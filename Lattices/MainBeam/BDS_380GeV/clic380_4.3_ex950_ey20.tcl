# Lattice design by Fabien Plassard
# L* = 4.3 m
#
# Optimised for the following entrance parameters 
# ex = 950 nm
# ey = 20 nm
# betax* = 8.2 mm
# betay* = 0.1 mm
# charge = 5.2e9
# sigma_z = 70 um
# e_spread = -1 (1% with square distribution)
#
# betax at the entrance of the BDS = 33.07266007 m
# betay at the entrance of the BDS = 8.962361942 m
# alpha_x 0.00
# alpha_y 0.00

Girder
Quadrupole -name "FQF" -synrad $quad_synrad -length 0.5 -strength [expr 0.0734950722*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "FDD" -length 5 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "FQD" -synrad $quad_synrad -length 0.5 -strength [expr -0.0812017998*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "FDD" -length 5 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "FQF2" -synrad $quad_synrad -length 0.5 -strength [expr -0.0780444232*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "FDD" -length 5 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "FQD2" -synrad $quad_synrad -length 0.5 -strength [expr 0.01289982894*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "TQF" -synrad $quad_synrad -length 0.5 -strength [expr 0.1149806684*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DD" -length 2.21875 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DD" -length 2.21875 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DD" -length 2.21875 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DD" -length 2.21875 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "TQD" -synrad $quad_synrad -length 1 -strength [expr -0.1179863805*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DD" -length 2.21875 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DD" -length 2.21875 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DD" -length 2.21875 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DD" -length 2.21875 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "TQF2A" -synrad $quad_synrad -length 0.5 -strength [expr 0.1033739537*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "TQF2B" -synrad $quad_synrad -length 0.5 -strength [expr 0.1033739537*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DD2" -length 1.024479167 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DD2" -length 1.024479167 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DD2" -length 1.024479167 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "TQD2A" -synrad $quad_synrad -length 0.5 -strength [expr -0.08499462268*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "TQD2B" -synrad $quad_synrad -length 0.5 -strength [expr -0.08499462268*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DD2" -length 1.024479167 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DD2" -length 1.024479167 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DD2" -length 1.024479167 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "TQF3A" -synrad $quad_synrad -length 0.5 -strength [expr 0.05660408062*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "TQF3B" -synrad $quad_synrad -length 0.5 -strength [expr 0.05660408062*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DD2" -length 1.024479167 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DD2" -length 1.024479167 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DD2" -length 1.024479167 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "TQD3A" -synrad $quad_synrad -length 0.5 -strength [expr -0.07925837696*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "TQD3B" -synrad $quad_synrad -length 0.5 -strength [expr -0.07925837696*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DD2" -length 1.024479167 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DD2" -length 1.024479167 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DD2" -length 1.024479167 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "TQF4A" -synrad $quad_synrad -length 0.5 -strength [expr 0.1039004288*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "TQF4B" -synrad $quad_synrad -length 0.5 -strength [expr 0.1039004288*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DD" -length 2.21875 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DD" -length 2.21875 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DD" -length 2.21875 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DD" -length 2.21875 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "TQD4A" -synrad $quad_synrad -length 0.5 -strength [expr -0.08989292421*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "TQD4B" -synrad $quad_synrad -length 0.5 -strength [expr -0.08989292421*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DD" -length 2.21875 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DD" -length 2.21875 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DD" -length 2.21875 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DD" -length 2.21875 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "TQF5A" -synrad $quad_synrad -length 0.5 -strength [expr 0.05764637677*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "TQF5B" -synrad $quad_synrad -length 0.5 -strength [expr 0.05764637677*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DD" -length 2.21875 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DD" -length 2.21875 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DD" -length 2.21875 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DD" -length 2.21875 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "TQD5" -synrad $quad_synrad -length 1 -strength [expr -0.1107619552*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DDL" -length 4.544391774 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DDL" -length 4.544391774 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DDL" -length 4.544391774 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DDL" -length 4.544391774 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "TQF6A" -synrad $quad_synrad -length 0.5 -strength [expr 0.02612552684*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "TQF6B" -synrad $quad_synrad -length 0.5 -strength [expr 0.02612552684*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DDL" -length 4.544391774 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DDL" -length 4.544391774 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DDL" -length 4.544391774 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DDL" -length 4.544391774 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "TQD6A" -synrad $quad_synrad -length 0.5 -strength [expr -0.02002336697*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "TQD6B" -synrad $quad_synrad -length 0.5 -strength [expr -0.02002336697*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DDL" -length 4.544391774 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DDL" -length 4.544391774 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DDL" -length 4.544391774 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DDL" -length 4.544391774 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "TQF7A" -synrad $quad_synrad -length 0.5 -strength [expr 0.0199547435*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "TQF7B" -synrad $quad_synrad -length 0.5 -strength [expr 0.0199547435*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DDL" -length 4.544391774 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DDL" -length 4.544391774 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DDL" -length 4.544391774 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DDL" -length 4.544391774 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "TQD7A" -synrad $quad_synrad -length 0.5 -strength [expr -0.0199547435*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "TQD7B" -synrad $quad_synrad -length 0.5 -strength [expr -0.0199547435*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DDL" -length 4.544391774 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DDL" -length 4.544391774 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DDL" -length 4.544391774 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DDL" -length 4.544391774 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "TQF8A" -synrad $quad_synrad -length 0.5 -strength [expr 0.0200862619*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "TQF8B" -synrad $quad_synrad -length 0.5 -strength [expr 0.0200862619*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DDL" -length 4.544391774 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DDL" -length 4.544391774 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DDL" -length 4.544391774 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DDL" -length 4.544391774 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "TQD8" -synrad $quad_synrad -length 1 -strength [expr -0.1038009056*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DDL89" -length 3.794391774 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "TQF9" -synrad $quad_synrad -length 1 -strength [expr -0.1524476254*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DDLE" -length 2.020466817 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "TQD9" -synrad $quad_synrad -length 1 -strength [expr 0.134790853*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
# WARNING: Multipole options not defined. Multipole type 0 with 0 strength added (tracked as a drift of length 0).
Girder
Multipole -name "SENTR" -synrad $mult_synrad -type 0 -length 0 -strength [expr 0.0*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DUTIL" -length 5 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QFEC" -synrad $quad_synrad -length 1.25 -strength [expr 0.002235658018*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm -name "BPMQ" -length 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QFEC" -synrad $quad_synrad -length 1.25 -strength [expr 0.002235658018*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "YGIRDER" -length 2.5 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 4.323916138e-05 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 4.323916138e-05 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 4.323916138e-05 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 4.323916138e-05 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 4.323916138e-05 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 4.323916138e-05 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM2" -length 0.3125 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DMM2" -length 0.3125 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 4.323916138e-05 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 4.323916138e-05 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 4.323916138e-05 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 4.323916138e-05 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 4.323916138e-05 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 4.323916138e-05 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.625 -strength [expr -0.01642511164*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.625 -strength [expr -0.01642511164*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.625 -strength [expr -0.01642511164*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.625 -strength [expr -0.01642511164*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QDEC" -synrad $quad_synrad -length 1.25 -strength [expr -0.002235658018*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm -name "BPMQ" -length 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QDEC" -synrad $quad_synrad -length 1.25 -strength [expr -0.002235658018*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DUTIL" -length 5 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "OCTEC3" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*463.8761985*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DUTIL" -length 5 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QDEC" -synrad $quad_synrad -length 1.25 -strength [expr -0.002235658018*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm -name "BPMQ" -length 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QDEC" -synrad $quad_synrad -length 1.25 -strength [expr -0.002235658018*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.625 -strength [expr -0.01642511164*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.625 -strength [expr -0.01642511164*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.625 -strength [expr -0.01642511164*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.625 -strength [expr -0.01642511164*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 -9.598727202e-06 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 -9.598727202e-06 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 -9.598727202e-06 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 -9.598727202e-06 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 -9.598727202e-06 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 -9.598727202e-06 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM2" -length 0.3125 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "ENGYSP" -length 0 -aperture_shape rectangular -aperture_x 1 -aperture_y 1
Girder
Drift -name "DMM2" -length 0.3125 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 -9.598727202e-06 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 -9.598727202e-06 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 -9.598727202e-06 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 -9.598727202e-06 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 -9.598727202e-06 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 -9.598727202e-06 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "SXEC2" -synrad $mult_synrad -type 3 -length 0.625 -strength [expr 0.005251846818*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "SXEC2" -synrad $mult_synrad -type 3 -length 0.625 -strength [expr 0.005251846818*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "SXEC2" -synrad $mult_synrad -type 3 -length 0.625 -strength [expr 0.005251846818*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "SXEC2" -synrad $mult_synrad -type 3 -length 0.625 -strength [expr 0.005251846818*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QFEC" -synrad $quad_synrad -length 1.25 -strength [expr 0.002235658018*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm -name "BPMQ" -length 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QFEC" -synrad $quad_synrad -length 1.25 -strength [expr 0.002235658018*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DUTIL" -length 5 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "ENGYAB" -length 0 -aperture_shape rectangular -aperture_x 1 -aperture_y 1
Girder
Drift -name "DUTIL" -length 5 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QFEC" -synrad $quad_synrad -length 1.25 -strength [expr 0.002235658018*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm -name "BPMQ" -length 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QFEC" -synrad $quad_synrad -length 1.25 -strength [expr 0.002235658018*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "SXEC2" -synrad $mult_synrad -type 3 -length 0.625 -strength [expr 0.005251846818*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "SXEC2" -synrad $mult_synrad -type 3 -length 0.625 -strength [expr 0.005251846818*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "SXEC2" -synrad $mult_synrad -type 3 -length 0.625 -strength [expr 0.005251846818*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "SXEC2" -synrad $mult_synrad -type 3 -length 0.625 -strength [expr 0.005251846818*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 -9.598727202e-06 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 -9.598727202e-06 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 -9.598727202e-06 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 -9.598727202e-06 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 -9.598727202e-06 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 -9.598727202e-06 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM2" -length 0.3125 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DMM2" -length 0.3125 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 -9.598727202e-06 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 -9.598727202e-06 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 -9.598727202e-06 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 -9.598727202e-06 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 -9.598727202e-06 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B4A" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 -9.598727202e-06 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B4B" -synrad $sbend_synrad -length 5.651041667 -angle -9.598727202e-06 -e0 $e0 -E1 0 -E2 -9.598727202e-06 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-9.598727202e-06*-9.598727202e-06/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.625 -strength [expr -0.01642511164*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.625 -strength [expr -0.01642511164*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.625 -strength [expr -0.01642511164*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.625 -strength [expr -0.01642511164*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QDEC" -synrad $quad_synrad -length 1.25 -strength [expr -0.002235658018*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm -name "BPMQ" -length 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QDEC" -synrad $quad_synrad -length 1.25 -strength [expr -0.002235658018*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DUTIL" -length 5 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "OCTEC4" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*-458.648884*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DUTIL" -length 5 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QDEC" -synrad $quad_synrad -length 1.25 -strength [expr -0.002235658018*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm -name "BPMQ" -length 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QDEC" -synrad $quad_synrad -length 1.25 -strength [expr -0.002235658018*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.625 -strength [expr -0.01642511164*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.625 -strength [expr -0.01642511164*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.625 -strength [expr -0.01642511164*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "SYEC2" -synrad $mult_synrad -type 3 -length 0.625 -strength [expr -0.01642511164*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 4.323916138e-05 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 4.323916138e-05 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 4.323916138e-05 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 4.323916138e-05 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 4.323916138e-05 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 4.323916138e-05 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM2" -length 0.3125 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DMM2" -length 0.3125 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 4.323916138e-05 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 4.323916138e-05 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 4.323916138e-05 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 4.323916138e-05 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 4.323916138e-05 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "B3A" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 4.323916138e-05 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "B3B" -synrad $sbend_synrad -length 5.651041667 -angle 4.323916138e-05 -e0 $e0 -E1 0 -E2 4.323916138e-05 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*4.323916138e-05*4.323916138e-05/5.651041667*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DSXL" -length 2.5 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DMM" -length 0.625 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QFEC" -synrad $quad_synrad -length 1.25 -strength [expr 0.002235658018*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm -name "BPMQ" -length 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QFEC" -synrad $quad_synrad -length 1.25 -strength [expr 0.002235658018*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DUTIL" -length 5 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "D1BC" -length 0.1 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QF1BC" -synrad $quad_synrad -length 0.5 -strength [expr 0.05165198607*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm -name "BPMQ" -length 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QF1BC" -synrad $quad_synrad -length 0.5 -strength [expr 0.05165198607*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DMM0" -length 0.125 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QF1BC" -synrad $quad_synrad -length 0.5 -strength [expr 0.05165198607*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm -name "BPMQ" -length 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QF1BC" -synrad $quad_synrad -length 0.5 -strength [expr 0.05165198607*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "D2BC" -length 0.125 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QD1BC" -synrad $quad_synrad -length 0.5 -strength [expr -0.04207740162*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm -name "BPMQ" -length 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QD1BC" -synrad $quad_synrad -length 0.5 -strength [expr -0.04207740162*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DMM0" -length 0.125 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QD1BC" -synrad $quad_synrad -length 0.5 -strength [expr -0.04207740162*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm -name "BPMQ" -length 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QD1BC" -synrad $quad_synrad -length 0.5 -strength [expr -0.04207740162*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DMM0" -length 0.125 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QD1BC" -synrad $quad_synrad -length 0.5 -strength [expr -0.04207740162*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm -name "BPMQ" -length 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QD1BC" -synrad $quad_synrad -length 0.5 -strength [expr -0.04207740162*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DMM0" -length 0.125 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QD1BC" -synrad $quad_synrad -length 0.5 -strength [expr -0.04207740162*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm -name "BPMQ" -length 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QD1BC" -synrad $quad_synrad -length 0.5 -strength [expr -0.04207740162*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "D3BC" -length 11.633515 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QF2BC" -synrad $quad_synrad -length 0.5 -strength [expr 0.01359339589*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm -name "BPMQ" -length 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QF2BC" -synrad $quad_synrad -length 0.5 -strength [expr 0.01359339589*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DMM0" -length 0.125 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QF2BC" -synrad $quad_synrad -length 0.5 -strength [expr 0.01359339589*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm -name "BPMQ" -length 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QF2BC" -synrad $quad_synrad -length 0.5 -strength [expr 0.01359339589*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "D4BC" -length 0.1925765 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "SEXIT" -synrad $mult_synrad -type 3 -length 0 -strength [expr -20*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "D3BCOL" -length 0.216005 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "PC" -length 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06066660139*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm -name "BPMQ" -length 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06066660139*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "D1BCOL" -length 17.58376 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "PC" -length 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.5 -strength [expr 0.04409515356*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm -name "BPMQ" -length 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.5 -strength [expr 0.04409515356*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "D2BCOL" -length 7.910975 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "PC" -length 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QDBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06789243904*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm -name "BPMQ" -length 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QDBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06789243904*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "YSP1" -length 0 -aperture_shape rectangular -aperture_x 1 -aperture_y 1
Girder
Drift -name "D2BCOL" -length 7.910975 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "XSP1" -length 0 -aperture_shape rectangular -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.5 -strength [expr 0.04409515356*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm -name "BPMQ" -length 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.5 -strength [expr 0.04409515356*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "D1BCOL" -length 17.58376 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "PC" -length 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06066660139*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm -name "BPMQ" -length 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06066660139*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "D3BCOL" -length 0.216005 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "D3BCOL" -length 0.216005 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "PC" -length 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06066660139*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm -name "BPMQ" -length 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06066660139*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "D1BCOL" -length 17.58376 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "XAB1" -length 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.5 -strength [expr 0.04409515356*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm -name "BPMQ" -length 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.5 -strength [expr 0.04409515356*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "D2BCOL" -length 7.910975 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "YAB1" -length 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QDBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06789243904*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm -name "BPMQ" -length 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QDBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06789243904*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "YSP2" -length 0 -aperture_shape rectangular -aperture_x 1 -aperture_y 1
Girder
Drift -name "D2BCOL" -length 7.910975 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "XSP2" -length 0 -aperture_shape rectangular -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.5 -strength [expr 0.04409515356*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm -name "BPMQ" -length 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.5 -strength [expr 0.04409515356*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "D1BCOL" -length 17.58376 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "PC" -length 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06066660139*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm -name "BPMQ" -length 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06066660139*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "D3BCOL" -length 0.216005 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "D3BCOL" -length 0.216005 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "PC" -length 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06066660139*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm -name "BPMQ" -length 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06066660139*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "D1BCOL" -length 17.58376 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "XAB2" -length 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.5 -strength [expr 0.04409515356*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm -name "BPMQ" -length 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.5 -strength [expr 0.04409515356*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "D2BCOL" -length 7.910975 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "YAB2" -length 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QDBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06789243904*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm -name "BPMQ" -length 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QDBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06789243904*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "YSP3" -length 0 -aperture_shape rectangular -aperture_x 1 -aperture_y 1
Girder
Drift -name "D2BCOL" -length 7.910975 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "XSP3" -length 0 -aperture_shape rectangular -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.5 -strength [expr 0.04409515356*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm -name "BPMQ" -length 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.5 -strength [expr 0.04409515356*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "D1BCOL" -length 17.58376 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "PC" -length 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06066660139*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm -name "BPMQ" -length 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06066660139*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "D3BCOL" -length 0.216005 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "D3BCOL" -length 0.216005 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "PC" -length 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06066660139*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm -name "BPMQ" -length 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06066660139*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "D1BCOL" -length 17.58376 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "XAB3" -length 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.5 -strength [expr 0.04409515356*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm -name "BPMQ" -length 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.5 -strength [expr 0.04409515356*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "D2BCOL" -length 7.910975 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "YAB3" -length 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QDBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06789243904*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm -name "BPMQ" -length 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QDBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06789243904*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "YSP4" -length 0 -aperture_shape rectangular -aperture_x 1 -aperture_y 1
Girder
Drift -name "D2BCOL" -length 7.910975 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "XSP4" -length 0 -aperture_shape rectangular -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.5 -strength [expr 0.04409515356*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm -name "BPMQ" -length 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.5 -strength [expr 0.04409515356*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "D1BCOL" -length 17.58376 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "PC" -length 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06066660139*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm -name "BPMQ" -length 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06066660139*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "D3BCOL" -length 0.216005 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "D3BCOL" -length 0.216005 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "PC" -length 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06066660139*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm -name "BPMQ" -length 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QCBCOL" -synrad $quad_synrad -length 0.5 -strength [expr -0.06066660139*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "D1BCOL" -length 17.58376 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "XAB4" -length 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.5 -strength [expr 0.04409515356*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Bpm -name "BPMQ" -length 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QFBCOL" -synrad $quad_synrad -length 0.5 -strength [expr 0.04409515356*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "D2BCOL" -length 7.910975 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "YAB4" -length 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "BTFD0" -length 0.02536345 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "BTFQ1" -synrad $quad_synrad -length 2.5 -strength [expr -0.1209993*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "BTFD1" -length 4.3563465 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "BTFQ2" -synrad $quad_synrad -length 2.5 -strength [expr 0.03044046*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "BTFD2" -length 9.99755 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "BTFQ3" -synrad $quad_synrad -length 2.5 -strength [expr 0.05048062*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "BTFD3" -length 9.98829 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "BTFQ4" -synrad $quad_synrad -length 2.5 -strength [expr -0.0666287*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "BTFD4" -length 0.02627595 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "LMD4" -length 6.968971864 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QMD11" -synrad $quad_synrad -length 1 -strength [expr 0.119570305*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QMD11" -synrad $quad_synrad -length 1 -strength [expr 0.119570305*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "LMD5" -length 4.244867663 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QMD12" -synrad $quad_synrad -length 1 -strength [expr -0.09180653875*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QMD12" -synrad $quad_synrad -length 1 -strength [expr -0.09180653875*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "LMD6" -length 7.2 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QMD13" -synrad $quad_synrad -length 1 -strength [expr 0.0840977218*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QMD13" -synrad $quad_synrad -length 1 -strength [expr 0.0840977218*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "LMD7" -length 7.2 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QMD14" -synrad $quad_synrad -length 1 -strength [expr -0.05527753679*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QMD14" -synrad $quad_synrad -length 1 -strength [expr -0.05527753679*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "LMD8" -length 6.9046 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QF8" -synrad $quad_synrad -length 0.9906 -strength [expr -0.008566825725*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QF8" -synrad $quad_synrad -length 0.9906 -strength [expr -0.008566825725*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "D8" -length 9.144 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QD7" -synrad $quad_synrad -length 0.9906 -strength [expr -0.004905805924*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QD7" -synrad $quad_synrad -length 0.9906 -strength [expr -0.004905805924*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "LX0" -length 0.3048 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "SFFB4" -synrad $sbend_synrad -length 2.10312 -angle 7.975818856e-06 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*7.975818856e-06*7.975818856e-06/2.10312*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB4" -synrad $sbend_synrad -length 2.10312 -angle 7.975818856e-06 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*7.975818856e-06*7.975818856e-06/2.10312*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB4" -synrad $sbend_synrad -length 2.10312 -angle 7.975818856e-06 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*7.975818856e-06*7.975818856e-06/2.10312*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB4" -synrad $sbend_synrad -length 2.10312 -angle 7.975818856e-06 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*7.975818856e-06*7.975818856e-06/2.10312*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB4" -synrad $sbend_synrad -length 2.10312 -angle 7.975818856e-06 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*7.975818856e-06*7.975818856e-06/2.10312*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB4" -synrad $sbend_synrad -length 2.10312 -angle 7.975818856e-06 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*7.975818856e-06*7.975818856e-06/2.10312*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB4" -synrad $sbend_synrad -length 2.10312 -angle 7.975818856e-06 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*7.975818856e-06*7.975818856e-06/2.10312*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB4" -synrad $sbend_synrad -length 2.10312 -angle 7.975818856e-06 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*7.975818856e-06*7.975818856e-06/2.10312*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB4" -synrad $sbend_synrad -length 2.10312 -angle 7.975818856e-06 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*7.975818856e-06*7.975818856e-06/2.10312*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB4" -synrad $sbend_synrad -length 2.10312 -angle 7.975818856e-06 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*7.975818856e-06*7.975818856e-06/2.10312*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "LX0" -length 0.3048 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QD6C" -synrad $quad_synrad -length 0.9906 -strength [expr -0.02322660034*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QD6C" -synrad $quad_synrad -length 0.9906 -strength [expr -0.02322660034*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "D7" -length 36.576 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QD6B" -synrad $quad_synrad -length 0.9906 -strength [expr -0.03312630653*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QD6B" -synrad $quad_synrad -length 0.9906 -strength [expr -0.03312630653*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "LX0" -length 0.3048 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QD6B" -synrad $quad_synrad -length 0.9906 -strength [expr -0.03312630653*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QD6B" -synrad $quad_synrad -length 0.9906 -strength [expr -0.03312630653*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "D6" -length 35.271456 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "SF6" -synrad $mult_synrad -type 3 -length 0.999744 -strength [expr 1.028260962*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "LX0" -length 0.3048 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QF5B" -synrad $quad_synrad -length 0.9906 -strength [expr 0.01804869847*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QF5B" -synrad $quad_synrad -length 0.9906 -strength [expr 0.01804869847*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "LX0" -length 0.3048 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QF5B" -synrad $quad_synrad -length 0.9906 -strength [expr 0.01804869847*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QF5B" -synrad $quad_synrad -length 0.9906 -strength [expr 0.01804869847*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "D5OD" -length 25.44568305 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "SD5L" -synrad $mult_synrad -type 3 -length 1 -strength [expr 0*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "SD5" -synrad $mult_synrad -type 3 -length 1 -strength [expr 10.34094733*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "SD5R" -synrad $mult_synrad -type 3 -length 1 -strength [expr 0*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "D5ODT" -length 17 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QF5A" -synrad $quad_synrad -length 0.9906 -strength [expr 0.01650632743*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QF5A" -synrad $quad_synrad -length 0.9906 -strength [expr 0.01650632743*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "LX0" -length 0.3048 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QF5A" -synrad $quad_synrad -length 0.9906 -strength [expr 0.01650632743*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QF5A" -synrad $quad_synrad -length 0.9906 -strength [expr 0.01650632743*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "LX0" -length 0.3048 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "SF5" -synrad $mult_synrad -type 3 -length 0.999744 -strength [expr -0.9286076461*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "D4OD" -length 8.817894544 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "OCTDRIFT" -length 2 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
# WARNING: Multipole options not defined. Multipole type 0 with 0 strength added (tracked as a drift of length 0).
Girder
Multipole -name "OCT1" -synrad $mult_synrad -type 0 -length 0 -strength [expr 0.0*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QD4B" -synrad $quad_synrad -length 0.9906 -strength [expr -0.01089460183*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QD4B" -synrad $quad_synrad -length 0.9906 -strength [expr -0.01089460183*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "LX0" -length 0.3048 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "SD4" -synrad $mult_synrad -type 3 -length 0.999744 -strength [expr 1.74353806*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "LX0" -length 0.3048 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QD4A" -synrad $quad_synrad -length 0.9906 -strength [expr -0.01094244259*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QD4A" -synrad $quad_synrad -length 0.9906 -strength [expr -0.01094244259*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "D3" -length 28.92624238 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "SFFB3" -synrad $sbend_synrad -length 8.9916 -angle -0.0001061013835 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-0.0001061013835*-0.0001061013835/8.9916*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB3" -synrad $sbend_synrad -length 8.9916 -angle -0.0001061013835 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-0.0001061013835*-0.0001061013835/8.9916*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB3" -synrad $sbend_synrad -length 8.9916 -angle -0.0001061013835 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-0.0001061013835*-0.0001061013835/8.9916*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB3" -synrad $sbend_synrad -length 8.9916 -angle -0.0001061013835 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-0.0001061013835*-0.0001061013835/8.9916*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB3" -synrad $sbend_synrad -length 8.9916 -angle -0.0001061013835 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-0.0001061013835*-0.0001061013835/8.9916*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB3" -synrad $sbend_synrad -length 8.9916 -angle -0.0001061013835 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-0.0001061013835*-0.0001061013835/8.9916*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB3" -synrad $sbend_synrad -length 8.9916 -angle -0.0001061013835 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-0.0001061013835*-0.0001061013835/8.9916*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB3" -synrad $sbend_synrad -length 8.9916 -angle -0.0001061013835 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-0.0001061013835*-0.0001061013835/8.9916*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB3" -synrad $sbend_synrad -length 8.9916 -angle -0.0001061013835 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-0.0001061013835*-0.0001061013835/8.9916*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB3" -synrad $sbend_synrad -length 8.9916 -angle -0.0001061013835 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-0.0001061013835*-0.0001061013835/8.9916*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "LX0" -length 0.3048 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QF3B" -synrad $quad_synrad -length 0.9906 -strength [expr 0.002669331782*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QF3B" -synrad $quad_synrad -length 0.9906 -strength [expr 0.002669331782*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "LX0" -length 0.3048 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "SFFB3B" -synrad $sbend_synrad -length 1.8288 -angle -2.15799424e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-2.15799424e-05*-2.15799424e-05/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB3B" -synrad $sbend_synrad -length 1.8288 -angle -2.15799424e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-2.15799424e-05*-2.15799424e-05/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB3B" -synrad $sbend_synrad -length 1.8288 -angle -2.15799424e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-2.15799424e-05*-2.15799424e-05/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB3B" -synrad $sbend_synrad -length 1.8288 -angle -2.15799424e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-2.15799424e-05*-2.15799424e-05/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB3B" -synrad $sbend_synrad -length 1.8288 -angle -2.15799424e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-2.15799424e-05*-2.15799424e-05/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB3B" -synrad $sbend_synrad -length 1.8288 -angle -2.15799424e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-2.15799424e-05*-2.15799424e-05/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB3B" -synrad $sbend_synrad -length 1.8288 -angle -2.15799424e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-2.15799424e-05*-2.15799424e-05/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB3B" -synrad $sbend_synrad -length 1.8288 -angle -2.15799424e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-2.15799424e-05*-2.15799424e-05/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB3B" -synrad $sbend_synrad -length 1.8288 -angle -2.15799424e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-2.15799424e-05*-2.15799424e-05/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB3B" -synrad $sbend_synrad -length 1.8288 -angle -2.15799424e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-2.15799424e-05*-2.15799424e-05/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "LX0" -length 0.3048 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QF3A" -synrad $quad_synrad -length 0.9906 -strength [expr 0.01098564386*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QF3A" -synrad $quad_synrad -length 0.9906 -strength [expr 0.01098564386*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "LX0" -length 0.3048 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "SFFB2" -synrad $sbend_synrad -length 1.8288 -angle -2.15799424e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-2.15799424e-05*-2.15799424e-05/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB2" -synrad $sbend_synrad -length 1.8288 -angle -2.15799424e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-2.15799424e-05*-2.15799424e-05/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB2" -synrad $sbend_synrad -length 1.8288 -angle -2.15799424e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-2.15799424e-05*-2.15799424e-05/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB2" -synrad $sbend_synrad -length 1.8288 -angle -2.15799424e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-2.15799424e-05*-2.15799424e-05/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB2" -synrad $sbend_synrad -length 1.8288 -angle -2.15799424e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-2.15799424e-05*-2.15799424e-05/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB2" -synrad $sbend_synrad -length 1.8288 -angle -2.15799424e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-2.15799424e-05*-2.15799424e-05/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB2" -synrad $sbend_synrad -length 1.8288 -angle -2.15799424e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-2.15799424e-05*-2.15799424e-05/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB2" -synrad $sbend_synrad -length 1.8288 -angle -2.15799424e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-2.15799424e-05*-2.15799424e-05/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB2" -synrad $sbend_synrad -length 1.8288 -angle -2.15799424e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-2.15799424e-05*-2.15799424e-05/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB2" -synrad $sbend_synrad -length 1.8288 -angle -2.15799424e-05 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-2.15799424e-05*-2.15799424e-05/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "LX0" -length 0.3048 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QD2" -synrad $quad_synrad -length 0.9906 -strength [expr -0.01294130587*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QD2" -synrad $quad_synrad -length 0.9906 -strength [expr -0.01294130587*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "LX0" -length 0.3048 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "SFFB1" -synrad $sbend_synrad -length 1.8288 -angle -6.483179788e-06 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB1" -synrad $sbend_synrad -length 1.8288 -angle -6.483179788e-06 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB1" -synrad $sbend_synrad -length 1.8288 -angle -6.483179788e-06 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB1" -synrad $sbend_synrad -length 1.8288 -angle -6.483179788e-06 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB1" -synrad $sbend_synrad -length 1.8288 -angle -6.483179788e-06 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB1" -synrad $sbend_synrad -length 1.8288 -angle -6.483179788e-06 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB1" -synrad $sbend_synrad -length 1.8288 -angle -6.483179788e-06 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB1" -synrad $sbend_synrad -length 1.8288 -angle -6.483179788e-06 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB1" -synrad $sbend_synrad -length 1.8288 -angle -6.483179788e-06 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFB1" -synrad $sbend_synrad -length 1.8288 -angle -6.483179788e-06 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "LX0" -length 0.3048 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.8288 -angle -6.483179788e-06 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.8288 -angle -6.483179788e-06 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.8288 -angle -6.483179788e-06 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.8288 -angle -6.483179788e-06 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.8288 -angle -6.483179788e-06 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.8288 -angle -6.483179788e-06 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.8288 -angle -6.483179788e-06 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.8288 -angle -6.483179788e-06 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.8288 -angle -6.483179788e-06 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.8288 -angle -6.483179788e-06 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "LX0" -length 0.3048 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.8288 -angle -6.483179788e-06 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.8288 -angle -6.483179788e-06 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.8288 -angle -6.483179788e-06 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.8288 -angle -6.483179788e-06 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.8288 -angle -6.483179788e-06 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.8288 -angle -6.483179788e-06 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.8288 -angle -6.483179788e-06 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.8288 -angle -6.483179788e-06 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.8288 -angle -6.483179788e-06 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.8288 -angle -6.483179788e-06 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "LX0" -length 0.3048 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.8288 -angle -6.483179788e-06 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.8288 -angle -6.483179788e-06 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.8288 -angle -6.483179788e-06 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.8288 -angle -6.483179788e-06 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.8288 -angle -6.483179788e-06 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.8288 -angle -6.483179788e-06 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.8288 -angle -6.483179788e-06 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.8288 -angle -6.483179788e-06 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.8288 -angle -6.483179788e-06 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.8288 -angle -6.483179788e-06 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "LX0" -length 0.3048 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.8288 -angle -6.483179788e-06 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.8288 -angle -6.483179788e-06 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.8288 -angle -6.483179788e-06 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.8288 -angle -6.483179788e-06 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.8288 -angle -6.483179788e-06 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.8288 -angle -6.483179788e-06 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.8288 -angle -6.483179788e-06 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.8288 -angle -6.483179788e-06 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.8288 -angle -6.483179788e-06 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1" -synrad $sbend_synrad -length 1.8288 -angle -6.483179788e-06 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "LX0" -length 0.3048 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "SFFSB1A" -synrad $sbend_synrad -length 1.8288 -angle -6.483179788e-06 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1A" -synrad $sbend_synrad -length 1.8288 -angle -6.483179788e-06 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1A" -synrad $sbend_synrad -length 1.8288 -angle -6.483179788e-06 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1A" -synrad $sbend_synrad -length 1.8288 -angle -6.483179788e-06 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1A" -synrad $sbend_synrad -length 1.8288 -angle -6.483179788e-06 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1A" -synrad $sbend_synrad -length 1.8288 -angle -6.483179788e-06 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1A" -synrad $sbend_synrad -length 1.8288 -angle -6.483179788e-06 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1A" -synrad $sbend_synrad -length 1.8288 -angle -6.483179788e-06 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1A" -synrad $sbend_synrad -length 1.8288 -angle -6.483179788e-06 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1A" -synrad $sbend_synrad -length 1.8288 -angle -6.483179788e-06 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "LX0" -length 0.3048 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Sbend -name "SFFSB1B" -synrad $sbend_synrad -length 1.8288 -angle -6.483179788e-06 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1B" -synrad $sbend_synrad -length 1.8288 -angle -6.483179788e-06 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1B" -synrad $sbend_synrad -length 1.8288 -angle -6.483179788e-06 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1B" -synrad $sbend_synrad -length 1.8288 -angle -6.483179788e-06 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1B" -synrad $sbend_synrad -length 1.8288 -angle -6.483179788e-06 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1B" -synrad $sbend_synrad -length 1.8288 -angle -6.483179788e-06 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1B" -synrad $sbend_synrad -length 1.8288 -angle -6.483179788e-06 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1B" -synrad $sbend_synrad -length 1.8288 -angle -6.483179788e-06 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1B" -synrad $sbend_synrad -length 1.8288 -angle -6.483179788e-06 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Sbend -name "SFFSB1B" -synrad $sbend_synrad -length 1.8288 -angle -6.483179788e-06 -e0 $e0 -E1 0 -E2 0 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
set e0 [expr $e0-14.1e-6*-6.483179788e-06*-6.483179788e-06/1.8288*$e0*$e0*$e0*$e0*$sbend_synrad]
Girder
Drift -name "D2OD" -length 14.30544669 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "OCTDRIFT" -length 2 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "SF1" -synrad $mult_synrad -type 3 -length 0.6096 -strength [expr -0.7311640987*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "OCTF1" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*0*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "LX0" -length 0.3048 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QF1" -synrad $quad_synrad -length 1.997964 -strength [expr 0.05369724494*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QF1" -synrad $quad_synrad -length 1.997964 -strength [expr 0.05369724494*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "D1OD" -length 1.048 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "OCTDRIFT" -length 2 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "DEC0" -length 0.6096 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "LX0" -length 0.3048 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "SD0" -synrad $mult_synrad -type 3 -length 0.6096 -strength [expr 2.640020845*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Multipole -name "OCTD0" -synrad $mult_synrad -type 4 -length 0 -strength [expr -1.0*0*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Drift -name "LX0" -length 0.3048 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QD0" -synrad $quad_synrad -length 1.6764 -strength [expr -0.1294435866*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Girder
Quadrupole -name "QD0" -synrad $quad_synrad -length 1.6764 -strength [expr -0.1294435866*$e0] -aperture_shape elliptic -aperture_x 1 -aperture_y 1
Dipole -name "IPDIPOLE" -length 0
Girder
Drift -name "D0" -length 4.29768 -aperture_shape elliptic -aperture_x 1 -aperture_y 1
