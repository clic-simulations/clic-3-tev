#!/usr/bin/tclsh

set ifile [lindex $argv 0]
set ofile [lindex $argv 1]

set f [open $ifile r]
set fo [open $ofile w]
#
# skip comment lines
#

for {set i 0} {$i<48} {incr i} {
    gets $f line
}

set comp_loss 1
set synrad 1

gets $f line

#
# Would need to add cavities here
#

while {![eof $f]} {
    set type [lindex $line 1]
#    if {[lindex $line 0]=="D1BC"} {break}
    puts $fo "Girder"
    switch $type {
	DRIFT {
	    puts $fo "Drift -l [lindex $line 3]"
	}
	SEXTUPOLE {
	    puts $fo "Multipole -synrad \$mult_synrad -type 3 -l [lindex $line 3] -strength \[expr [lindex $line 6]*\$e0\] -tilt [lindex $line 8]"
	}
	OCTUPOLE {
	    puts $fo "Multipole -synrad \$mult_synrad -type 4 -l [lindex $line 3] -strength \[expr -1.0*[lindex $line 7]*\$e0\]"
	}
	MULTIPOLE {
	    if {[expr abs([lindex $line 5])] > 0} {
		puts $fo "Multipole -synrad \$mult_synrad -type 2 -l [lindex $line 3] -strength \[expr -1.0*[lindex $line 5]*\$e0\]  -tilt [lindex $line 8]" }

	    if {[expr abs([lindex $line 6])] > 0} {
		puts $fo "Multipole -synrad \$mult_synrad -type 3 -l [lindex $line 3] -strength \[expr [lindex $line 6]*\$e0\]  -tilt [lindex $line 8]" }
	    if {[expr abs([lindex $line 7])] > 0} {
		puts $fo "Multipole -synrad 0 -type 4 -l [lindex $line 3] -strength \[expr -1.0*[lindex $line 7]*\$e0\] -tilt [lindex $line 8]" }
	    if {[expr abs([lindex $line 12])] > 0} {
		puts $fo "Multipole -synrad 0 -type 5 -l [lindex $line 3] -strength \[expr 1.0*[lindex $line 12]*\$e0\] -tilt [lindex $line 8]"}
#	    if {[expr abs([lindex $line 13])] > 0} {
#		puts $fo "Multipole -synrad 0 -type 6 -l [lindex $line 3] -strength \[expr -1.0*[lindex $line 13]*\$e0\] -tilt [lindex $line 8]"}

	#    if {[expr abs([lindex $line 13])] > 0} {
	#	puts $fo "Multipole -synrad 0 -type 2 -l [lindex $line 3] -strength \[expr 1.0*[lindex $line 13]*\$e0\]  -roll 0.785398163397448"}
	#}
	SBEND {
	    puts $fo "Sbend -synrad \$sbend_synrad -l [lindex $line 3] -angle [lindex $line 4] -e0 \$e0 -E1 [lindex $line 9] -E2 [lindex $line 10] -K \[expr [lindex $line 5]*\$e0\]"
	    set a_old [lindex $line 4]
	    set l_old [lindex $line 3]
	    if {$comp_loss} {
		puts $fo "set e0 \[expr \$e0-14.1e-6*[lindex $line 4]*[lindex $line 4]/[lindex $line 3]*\$e0*\$e0*\$e0*\$e0*\$sbend_synrad\]"
	    }
	}
	QUADRUPOLE {
	    puts $fo "Quadrupole -synrad \$quad_synrad -l [lindex $line 3] -strength \[expr [lindex $line 5]*\$e0\] -roll [lindex $line 8]"
	}
	MATRIX {
	    if {$comp_loss==0} {
		puts $fo "set e0 \[expr \$e0-\$synrad*14.1e-6*$a_old*$a_old/$l_old*\$e0*\$e0*\$e0*\$e0\]"
	    }
	}
	default {
	    puts $fo "Drift -l [lindex $line 3]"
	}
    }
    gets $f line
}


close $f
close $fo
