#!/afs/cern.ch/user/r/rtomas/lintrack/Python-2.5_32bit/Python-2.5_32bit/bin/python

# Just to make sure that the path to the libraires is defined 
#import sys
#sys.path.append('/afs/cern.ch/eng/sl/lintrack/Python_Classes4MAD/')


import math
#import copy
import sys
from os import system
#from simplex import Simplex
from mapclass25  import *
import random
from metaclass25 import *

betx=33.07266007
bety=8.962361942
gamma=489237.8342
ex=3000*1e-9
ey=40*1e-9



sigmaFFS=[sqrt(ex*betx/gamma), sqrt(ex/betx/gamma), sqrt(ey*bety/gamma), sqrt(ey/bety/gamma), 0.01]

file='fort.18'
map=Map(1,file)
#tw=twiss("twiss.scaled")
#IPi=tw.indx["IP"]
print "sigmax1=",sqrt(map.sigma('x',sigmaFFS)-map.offset('x',sigmaFFS)**2),";"
print "sigmay1=",sqrt(map.sigma('y',sigmaFFS)-map.offset('y',sigmaFFS)**2),";"
print "sigmapx1=",sqrt(map.sigma('px',sigmaFFS)-map.offset('px',sigmaFFS)**2),";"
print "sigmapy1=",sqrt(map.sigma('py',sigmaFFS)-map.offset('py',sigmaFFS)**2),";"

map=Map(2,file)
#tw=twiss("twiss")
#IPi=tw.indx["IP"]
print "sigmax2=",sqrt(map.sigma('x',sigmaFFS)-map.offset('x',sigmaFFS)**2),";"
print "sigmay2=",sqrt(map.sigma('y',sigmaFFS)-map.offset('y',sigmaFFS)**2),";"
print "sigmapx2=",sqrt(map.sigma('px',sigmaFFS)-map.offset('px',sigmaFFS)**2),";"
print "sigmapy2=",sqrt(map.sigma('py',sigmaFFS)-map.offset('py',sigmaFFS)**2),";"

map=Map(3,file)
#tw=twiss("twiss")
#IPi=tw.indx["IP"]
print "sigmax3=",sqrt(map.sigma('x',sigmaFFS)-map.offset('x',sigmaFFS)**2),";"
print "sigmay3=",sqrt(map.sigma('y',sigmaFFS)-map.offset('y',sigmaFFS)**2),";"
print "sigmapx3=",sqrt(map.sigma('px',sigmaFFS)-map.offset('px',sigmaFFS)**2),";"
print "sigmapy3=",sqrt(map.sigma('py',sigmaFFS)-map.offset('py',sigmaFFS)**2),";"

map=Map(4,file)
print "sigmax4=",sqrt(map.sigma('x',sigmaFFS)-map.offset('x',sigmaFFS)**2),";"
print "sigmay4=",sqrt(map.sigma('y',sigmaFFS)-map.offset('y',sigmaFFS)**2),";"
print "sigmapx4=",sqrt(map.sigma('px',sigmaFFS)-map.offset('px',sigmaFFS)**2),";"
print "sigmapy4=",sqrt(map.sigma('py',sigmaFFS)-map.offset('py',sigmaFFS)**2),";"

map=Map(5,file)
print "sigmax5=",sqrt(map.sigma('x',sigmaFFS)-map.offset('x',sigmaFFS)**2),";"
print "sigmay5=",sqrt(map.sigma('y',sigmaFFS)-map.offset('y',sigmaFFS)**2),";"
print "sigmapx5=",sqrt(map.sigma('px',sigmaFFS)-map.offset('px',sigmaFFS)**2),";"
print "sigmapy5=",sqrt(map.sigma('py',sigmaFFS)-map.offset('py',sigmaFFS)**2),";"

map=Map(6,file)
print "sigmax6=",sqrt(map.sigma('x',sigmaFFS)-map.offset('x',sigmaFFS)**2),";"
print "sigmay6=",sqrt(map.sigma('y',sigmaFFS)-map.offset('y',sigmaFFS)**2),";"
print "sigmapx6=",sqrt(map.sigma('px',sigmaFFS)-map.offset('px',sigmaFFS)**2),";"
print "sigmapy6=",sqrt(map.sigma('py',sigmaFFS)-map.offset('py',sigmaFFS)**2),";"

sys.exit()

#print "beta_x=",tw.BETX[IPi],";"
#print "beta_y=",tw.BETY[IPi],";"
#print "alfa_y=",tw.ALFY[IPi],";"
#print "D_y=",tw.DY[IPi],";"
print "corryx=",map.correlation('y','x',sigmaFFS)-(map.offset('y',sigmaFFS)*map.offset('x',sigmaFFS)),";"
print "corrypx=",map.correlation('py','x',sigmaFFS)-(map.offset('py',sigmaFFS)*map.offset('x',sigmaFFS)),";"
print "corrpyx=",map.correlation('y','px',sigmaFFS)-(map.offset('y',sigmaFFS)*map.offset('px',sigmaFFS)),";"
print "corrpypx=",map.correlation('py','px',sigmaFFS)-(map.offset('py',sigmaFFS)*map.offset('px',sigmaFFS)),";"
print "corryd=",map.correlation('y','d',sigmaFFS),";"
