
from metaclass25 import *


m=twiss('twiss.newbds')

brho=250*3.3

f=open("Apertures.EbE","w")
print >>f, 'NAME Pos[m]   Length[m]     Field/Gradient     Unit   Radius[mm]  PeakField[T]'
for i in range(len(m.NAME)):
  if  m.K1L[i] !=0 or m.ANGLE[i] !=0 or m.K2L[i] !=0:
    a=m.APER_1[i]
    peakf=m.ANGLE[i]/m.L[i]*brho + m.K1L[i]/m.L[i]*brho*a +  m.K2L[i]/m.L[i]*brho*a**2/2.
    if m.ANGLE[i] != 0: unit ="T"
    if m.K1L[i] != 0: unit ="T/m"
    if m.K2L[i] != 0: unit ="T/m^2"
    print >>f, '%8s  0.0  %5.1f  %8.3f  %10s  %5.2f  %5.4f' %  (m.NAME[i], m.L[i],m.ANGLE[i]/m.L[i]*brho + m.K1L[i]/m.L[i]*brho + m.K2L[i]/m.L[i]*brho , unit , a*1000, abs(peakf))



f.close()

   



