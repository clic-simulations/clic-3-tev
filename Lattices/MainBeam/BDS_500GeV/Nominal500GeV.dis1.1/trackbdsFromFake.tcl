set e_initial 500
set e0 $e_initial
#set script_dir /local/PLACET/CLIC_example/
set script_dir /afs/cern.ch/user/r/rtomas/w1/PLACET/CLIC_example

set synrad 1
set quad_synrad 1
set mult_synrad 1
set sbend_synrad 1
set dp 0.00


set scale 1.0
source $script_dir/clic_basic_single.tcl

set kod1 0
set kod2 0
set kod21 0
set kod3 0
set kod4 0

#source ../CreatPLACEtinputs/ff.tcl.Nominal
source bds.placet

proc save_beam {} {
    if {[EmittanceRun]} {
	BeamDump -file particles.out
    }
}

TclCall -script save_beam

BeamlineSet -name test




array set match {
alpha_x 0
alpha_y 0
beta_x  33.0726600
beta_y  8.962361942
}

set match(emitt_x)   6.8
set match(emitt_y)     0.2
set match(charge) 4e9
set charge $match(charge)
set match(sigma_z) 44.0
set match(phase) 0.0
set match(e_spread) -1.0

set n_slice 21
set n 7
set n_total 10000
source $script_dir/clic_beam.tcl


#make_beam_halo $e0 $match(e_spread) $n_total
make_beam_particles [expr $e0*(1+($dp))] $match(e_spread) $n_total


make_beam_many beam0 20 500 
BeamRead -file particles.in -beam beam0

FirstOrder 1
TestNoCorrection -beam beam0 -emitt_file emitt.dat -survey Zero


# H hist
Histogram hx -xmin -300 -xmax 300 -n 200
set f [open particles.out r]
gets $f line
while {![eof $f]} {
	hx add "[expr 1000.0*[lindex $line 1]] 1.0"
      	gets $f line
}
close $f
hx print hx.dat

# V hist
Histogram hy -xmin -10 -xmax 10 -n 200
set f [open particles.out r]
gets $f line
while {![eof $f]} {
	hy add "[expr 1000.0*[lindex $line 2]] 1.0"
      	gets $f line
}
close $f
hy print hy.dat

#exec gnuplot GaussFit.gp

#exit

exec AveSc particles.out > sizes_dp_${dp}_rad_${synrad}

#exit
#TwissPlot -beam beam0 -file twiss.dat
#set twiss [TwissPlot -beam beam0]



#Octave {

#     [E,Beam]=placet_test_no_correction("electron", "beam0", "Zero");
#    save -text emitt E
			
#	      }




source $script_dir/clic_guinea.tcl
set gp_param(n_x) 128
set gp_param(cut_x) 400.0
set gp_param(n_y) 256
set gp_param(cut_y) 15.0
set gp_param(sigmaz) $match(sigma_z)
set gp_param(particles) [expr 1e-10*$charge]
write_guinea_offset_angle 0.0 0.0
exec cp particles.out electron.ini
exec cp particles.out positron.ini

exec guinea default default default

exec  grep lumi_ee= default > lumi

exec  grep lumi_ee_high= default > lumi_high


exit
