#!/usr/local/bin/perl

if (@ARGV < 1) { die "usage:\tmadx2placet.pl FILE 1 (:beam parameters printed into the file) 1 (:take apertures from madX (0 apertures 1 m)) 1 (: for wakefield studies)\n"; }

open(FILE, $ARGV[0]) or die "Could not open file $ARGV[0]\n";

@names = ();
@line = ();

sub search_index  # $i = search_index("ENERGY"); $energy = $line[$i];
{
    my $name = $_[0];
    my $i;
    
    for ($i = 0; $i < @names; $i++)
    {
	if ($names[$i] eq $name)
	{
	    last;
	}
    }
    return $i;
}

sub search_value  # $energy = search_value("ENERGY");
{
    return $line[search_index($_[0])];
}


#$first_element = 1;
my $first_element = $ARGV[1];
my $aper = $ARGV[2];
my $coll = $ARGV[3];


while ($lines = <FILE>)
{

    if ($lines =~ /^\*/) {
	
	@line = split(" ", $lines);
	
	for ($i=1;$i<@line;$i++)
	{
	    push(@names, $line[$i]);
	}
	
    } elsif ($lines !~ /^[@\*\$]/) {
	
	@line = split(" ", $lines);
	
	if ($first_element == 1) {
	    
	    $first_element = 0;
	    
	    my $energy = search_value("ENERGY");
	    
	    print "set e0 $energy\n";
	    print "\n";

	    print "set sbend_synrad 0\n";
	    print "set quad_synrad 0\n";
	    print "set mult_synrad 0\n";
	    print "\n";
	    
	    my $beta_x = search_value("BETX");
	    my $beta_y = search_value("BETY");
	    my $alpha_x = search_value("ALFX");
	    my $alpha_y = search_value("ALFY");
	    
	    print "set match(beta_x) $beta_x\n";
	    print "set match(beta_y) $beta_y\n";
	    print "set match(alpha_x) $alpha_x\n";
	    print "set match(alpha_y) $alpha_y\n";
	    print "\n";
	    print "SetReferenceEnergy \$e0\n";
	}
	
	my $keyword = search_value("KEYWORD");
	my $length = search_value("L");
	my $name = search_value("NAME");
	
	my $apx = 1.0; # beware, this is in meters
	my $apy = 1.0; # beware, this is in meters	    
	if($aper == 1) {
	    $apx = search_value("APER_1"); # beware, this is in meters
	    $apy = search_value("APER_2"); # beware, this is in meters
        }
	if ($keyword =~ /DRIFT/)
	{
	    print "Girder\n";
	    print "Drift -name $name -length $length";
	    if($apx !=0 && $apy !=0){
		print " -aperture_shape elliptic -aperture_x $apx -aperture_y $apy\n";
	    }
	    elsif($apx !=0 && $apy ==0){
		print " -aperture_shape elliptic -aperture_x $apx -aperture_y $apx\n";	  
	    }
	    else {
		print " -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008\n";
	    }
	}
	elsif ($keyword =~ /QUADRUPOLE/)
	{
	    my $tilt = search_value("TILT");
	    my $k1l = search_value("K1L");
	    print "Girder\n"; 
	    print "Quadrupole -name $name -synrad \$quad_synrad -length $length -strength \[expr $k1l*\$e0\]";
	    if ($tilt != 0)
	    {
		print " -tilt $tilt";
	    }
	    if($apx !=0 && $apy !=0){
		print " -aperture_shape elliptic -aperture_x $apx -aperture_y $apy\n";
	    }
	    elsif($apx !=0 && $apy ==0){
		print " -aperture_shape elliptic -aperture_x $apx -aperture_y $apx\n";	  
	    }
	    else {
		print " -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008\n";
	    }
	}
	elsif ($keyword =~ /SEXTUPOLE/)
	{
	    my $tilt = -1.0 * search_value("TILT");
	    my $k2l = search_value("K2L");
	    print "Girder\n";
	    print "Multipole -name $name -synrad \$mult_synrad -type 3 -length $length -strength \[expr $k2l*\$e0\]";
	    if ($tilt != 0)
	    {
		print " -tilt $tilt";
	    }      
	    if($apx !=0 && $apy !=0){
		print " -aperture_shape elliptic -aperture_x $apx -aperture_y $apy\n";
	    }
	    elsif($apx !=0 && $apy ==0){
		print " -aperture_shape elliptic -aperture_x $apx -aperture_y $apx\n";	  	  
	    }
	    else {
		print " -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008\n";	  		
	    }	    
	}
	elsif ($keyword =~ /OCTUPOLE/)
	{
	    my $tilt = -1.0 * search_value("TILT");
	    my $k3l = search_value("K3L");
	    print "Girder\n";
	    print "Multipole -name $name -synrad \$mult_synrad -type 4 -length $length -strength \[expr -1.0*$k3l*\$e0\]";
	    if ($tilt != 0)
	    {
		print " -tilt $tilt";
	    }
	    if($apx !=0 && $apy !=0){
		print " -aperture_shape elliptic -aperture_x $apx -aperture_y $apy\n";
	    }
	    elsif($apx !=0 && $apy ==0){
		print " -aperture_shape elliptic -aperture_x $apx -aperture_y $apx\n"; 	  
	    }
	    else {
		print " -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008\n";	  		
	    }
	}
	elsif ($keyword =~ /MULTIPOLE/)
	{
	    my $tilt = -1.0 * search_value("TILT");
	    my $k0l = search_value("K0L");
	    my $k1l = search_value("K1L");
	    my $k2l = search_value("K2L");
	    my $k3l = search_value("K3L");
	    my $k4l = search_value("K4L");
	    if ($k0l != 0) {
		print "Girder\n";
		print "Dipole -name $name -synrad 0 -length $length -strength \[expr -1.0*$k0l*\$e0\]";
		if ($tilt != 0)
		{
		    print " -tilt $tilt";
		}
		if($apx !=0 && $apy !=0){
		    print " -aperture_shape elliptic -aperture_x $apx -aperture_y $apy\n";
		}
		elsif($apx !=0 && $apy ==0){
		    print " -aperture_shape elliptic -aperture_x $apx -aperture_y $apx\n"; 	    
		}
		else {
		    print " -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008\n";	  		
		}		
	    } elsif ($k1l != 0) {
		print "Girder\n";
		print "Quadrupole -name $name -synrad \$quad_synrad -length $length -strength \[expr -1.0*$k1l*\$e0\]"; # to be check
		if ($tilt != 0)
		{
		    print " -tilt $tilt";
		}
		if($apx !=0 && $apy !=0){
		    print " -aperture_shape elliptic -aperture_x $apx -aperture_y $apy\n";
		}
		elsif($apx !=0 && $apy ==0){
		    print " -aperture_shape elliptic -aperture_x $apx -aperture_y $apx\n"; 	    
		}
		else {
		    print " -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008\n";	  		
		}
	    } elsif ($k2l != 0) {
		print "Girder\n";
		print "Multipole -name $name -synrad \$mult_synrad -type 3 -length $length -strength \[expr $k2l*\$e0\]";
		if ($tilt != 0)
		{
		    print " -tilt $tilt";
		}
		if($apx !=0 && $apy !=0){	  
		    print " -aperture_shape elliptic -aperture_x $apx -aperture_y $apy\n";
		}
		elsif($apx !=0 && $apy ==0){
		    print " -aperture_shape elliptic -aperture_x $apx -aperture_y $apx\n";	      
		}
		else {
		    print " -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008\n";	  		
		}
	    } elsif ($k3l != 0) {
		print "Girder\n";
		print "Multipole -name $name -synrad \$mult_synrad -type 4 -length $length -strength \[expr -1.0*$k3l*\$e0\]";
		if ($tilt != 0)
		{
		    print " -tilt $tilt";
		}
		if($apx !=0 && $apy !=0){
		    print " -aperture_shape elliptic -aperture_x $apx -aperture_y $apy\n";
		}
		elsif($apx !=0 && $apy ==0){
		    print " -aperture_shape elliptic -aperture_x $apx -aperture_y $apx\n";	      
		}
		else {
		    print " -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008\n";	  		
		}
	    } elsif ($k4l != 0) {
		print "Girder\n";
		print "Multipole -name $name -synrad \$mult_synrad -type 5 -length $length -strength \[expr $k4l*\$e0\]";
		if ($tilt != 0)
		{
		    print " -tilt $tilt";
		}
		if($apx !=0 && $apy !=0){
		    print " -aperture_shape elliptic -aperture_x $apx -aperture_y $apy\n";
		}
		elsif($apx !=0 && $apy ==0){
		    print " -aperture_shape elliptic -aperture_x $apx -aperture_y $apx\n";	     
		}
		else {
		    print " -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008\n";	  		
		}
	    } elsif ($k5l != 0) {
		print "Girder\n";
		print "Multipole -name $name -synrad \$mult_synrad -type 6 -length $length -strength \[expr -1.0*$k5l*\$e0\]";
		if ($tilt != 0)
		{
		    print " -tilt $tilt";
		}
		if($apx !=0 && $apy !=0){
		    print " -aperture_shape elliptic -aperture_x $apx -aperture_y $apy\n";
		}
		elsif($apx !=0 && $apy ==0){
		    print " -aperture_shape elliptic -aperture_x $apx -aperture_y $apx\n";	     
		}
		else {
		    print " -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008\n";	  		
		}
	    } else {
		print "# WARNING: Multipole options not defined. Multipole type 0 with 0 strength added (tracked as a drift of length 0).";
		print "\n";
		print "Girder\n";
		print "Multipole -name $name -synrad \$mult_synrad -type 0 -length $length -strength \[expr 0.0*\$e0\]";
		if ($tilt != 0)
		{
		    print " -tilt $tilt";
		}
		if($apx !=0 && $apy !=0){
		    print " -aperture_shape elliptic -aperture_x $apx -aperture_y $apy\n";
		}
		elsif($apx !=0 && $apy ==0){
		    print " -aperture_shape elliptic -aperture_x $apx -aperture_y $apx\n";	     
		}
		else {
		    print " -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008\n";	  		
		}
	    } 
	}
	elsif ($keyword =~ /RBEND/)
	{
	    my $k1l = search_value("K1L");
	    my $angle = search_value("ANGLE");
	    my $half_angle = $angle * 0.5;
	    my $radius = $length / sin($half_angle) / 2;
	    my $arc_length = $angle * $radius;
	    print "# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2\n";
	    print "# WARNING: original length was $length\n";
	    print "Girder\n";
	    print "Sbend -name $name -synrad \$sbend_synrad -length $arc_length -angle $angle -e0 \$e0 -E1 $half_angle -E2 $half_angle";
	    if ($k1l != 0.0) {
		print " -K \[expr $k1l*\$e0\]";
	    }
	    if($apx !=0 && $apy !=0){
		print " -aperture_shape elliptic -aperture_x $apx -aperture_y $apy\n";
	    }
	    elsif($apx !=0 && $apy ==0){
		print " -aperture_shape elliptic -aperture_x $apx -aperture_y $apx\n";	     
	    }
	    else {
		print " -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008\n";	  		
	    }
	}
	elsif ($keyword =~ /SBEND/)
	{
	    my $k1l = search_value("K1L");
	    my $angle = search_value("ANGLE");
	    my $half_angle = $angle * 0.5;
	    print "Girder\n";
	    print "Sbend -name $name -synrad \$sbend_synrad -length $length -angle $angle -e0 \$e0 -E1 $half_angle -E2 $half_angle";
	    if ($k1l != 0.0) {
		print " -K \[expr $k1l*\$e0\]";
	    }
	    if($apx !=0 && $apy !=0){
		print " -aperture_shape elliptic -aperture_x $apx -aperture_y $apy\n";
	    }
	    elsif($apx !=0 && $apy ==0){
		print " -aperture_shape elliptic -aperture_x $apx -aperture_y $apx\n";	     
	    }
	    else {
		print " -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008\n";	  		
	    }
	    print "set e0 \[expr \$e0-14.1e-6*$angle*$angle/$length*\$e0*\$e0*\$e0*\$e0*\$sbend_synrad\]\n";
	}
	elsif ($keyword =~ /MATRIX/)
	{
#      if {$comp_loss==0} {
#        print "#set e0 \[expr \$e0-\$synrad*14.1e-6*$a_old*$a_old/$l_old*\$e0*\$e0*\$e0*\$e0\]\n";
#      }
	}
	elsif ($keyword =~ /LCAVITY/)
	{
	    print "Girder\n";
	    print "AccCavity -name $name -length $length\n";
#      if {$comp_loss==0} {
#        print "#set e0 \[expr \$e0-\$synrad*14.1e-6*$a_old*$a_old/$l_old*\$e0*\$e0*\$e0*\$e0\]\n";
#      }
	}
	elsif ($keyword =~ /RCOLLIMATOR/)
	{
	    print "Girder\n";
	    if($coll!=0){
		print "# WARNING: for the collimator wakefield studies you need to add more info ==> Adding a Collimator commented \n";
		print "# Collimator -name $name -length $length \n";
	    }
	    else {
		print "Drift -name $name -length $length";
		if($apx !=0 && $apy !=0){
		    print " -aperture_shape rectangular -aperture_x $apx -aperture_y $apy\n";
		}
		elsif($apx !=0 && $apy ==0){
		    print " -aperture_shape rectangular -aperture_x $apx -aperture_y $apx\n";	     
		}
		else {
		    print " -aperture_shape rectangular -aperture_x 0.008 -aperture_y 0.008\n";	  		
		}
	    }
	}
	elsif ($keyword =~ /ECOLLIMATOR/)
	{
	    print "Girder\n";
	    if($coll!=0){
		print "# WARNING: for the collimator wakefield studies you need to add more info ==> Adding a Collimator commented \n";
		print "# Collimator -name $name -length $length \n";
	    }
	    else {
		print "Drift -name $name -length $length";
		if($apx !=0 && $apy !=0){
		    print " -aperture_shape elliptic -aperture_x $apx -aperture_y $apy\n";
		}
		elsif($apx !=0 && $apy ==0){
		    print " -aperture_shape elliptic -aperture_x $apx -aperture_y $apx\n";	     
		}
		else {
		    print " -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008\n";	  		
		}
	    }
	}
	elsif ($keyword =~ /HKICKER/)
	{
	    print "Girder\n";
	    print "# HCORRECTOR -name $name -length $length\n";
	    if ($length != 0) {
		print "Drift -name $name -length $length";
		if($apx !=0 && $apy !=0){
		    print " -aperture_shape elliptic -aperture_x $apx -aperture_y $apy\n";
		}
		elsif($apx !=0 && $apy ==0){
		    print " -aperture_shape elliptic -aperture_x $apx -aperture_y $apx\n";	     
		}
		else {
		    print " -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008\n";	  		
		}
	    }
	}
	elsif ($keyword =~ /VKICKER/)
	{
	    print "Girder\n";
	    print "# VCORRECTOR -name $name -length $length\n";
	    if ($length != 0) {
		print "Drift -name $name -length $length";
		if($apx !=0 && $apy !=0){
		    print " -aperture_shape elliptic -aperture_x $apx -aperture_y $apy\n";
		}
		elsif($apx !=0 && $apy ==0){
		    print " -aperture_shape elliptic -aperture_x $apx -aperture_y $apx\n";	     
		}
		else {
		    print " -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008\n";	  		
		}
	    }
	}
	elsif ($keyword =~ /MARKER/)
	{
	    if ($length != 0) {
		print "Girder\n";
		print "Drift -name $name -length $length";
		if($apx !=0 && $apy !=0){
		    print " -aperture_shape elliptic -aperture_x $apx -aperture_y $apy\n";
		}
		elsif($apx !=0 && $apy ==0){
		    print " -aperture_shape elliptic -aperture_x $apx -aperture_y $apx\n";	     
		}
		else {
		    print " -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008\n";	  		
		}
	    }
	}
	elsif ($keyword =~ /MONITOR/)
	{
	    print "Girder\n";
	    print "Bpm -name $name -length $length";
	    if($apx !=0 && $apy !=0){
		print " -aperture_shape elliptic -aperture_x $apx -aperture_y $apy\n";
	    }
	    elsif($apx !=0 && $apy ==0){
		print " -aperture_shape elliptic -aperture_x $apx -aperture_y $apx\n";	     
	    }
	    else {
		print " -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008\n";	  		
	    }
	}
	else 
	{
	    print "# UNKNOWN: @line\n";
	    print "Girder\n";
	    print "Drift -name $name -length $length"; 
	    if($apx !=0 && $apy !=0){
		print " -aperture_shape elliptic -aperture_x $apx -aperture_y $apy\n";
	    }
	    elsif($apx !=0 && $apy ==0){
		print " -aperture_shape elliptic -aperture_x $apx -aperture_y $apx\n";	     
	    }
	    else {
		print " -aperture_shape elliptic -aperture_x 0.008 -aperture_y 0.008\n";	  		
	    }
	}
    } 
}

close(FILE);
