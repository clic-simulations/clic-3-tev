from metaclass25 import *
from numpy import *


def Rotate(ref,angle):
  ca=cos(angle)
  sa=sin(angle)
  R=array([[ca,sa],[-sa,ca]])
  for i in range(len(ref.NAME)):
    x=ref.X[i]
    z=ref.Z[i]
    theta=ref.THETA[i]
    vnew=dot(R,array([x,z]))
#    print vnew
    ref.X[i]=vnew[0]
    ref.Z[i]=vnew[1]
    ref.THETA[i]=theta+angle
    
  return ref


def Translate(ref,dx,dz):
  for i in range(len(ref.NAME)):
    ref.X[i]=ref.X[i]+dx
    ref.Z[i]=ref.Z[i]+dz
    ref.S[i]=ref.S[i]+dz
  return ref


x=twiss('survey')
IPindx=x.indx["IP"]

IPangle= x.THETA[IPindx]
IPx= x.X[IPindx]
IPz= x.Z[IPindx]

x=Translate(x,-IPx,-IPz)
x=Rotate(x,-IPangle+0.02)
print "IPx=",x.X[IPindx], x.THETA[IPindx]




f=open("survey.new","w")
for i in range(len(x.NAME)):
  print >>f, x.NAME[i], x.S[i],x.L[i],x.ANGLE[i],x.X[i], x.Y[i], x.Z[i], x.THETA[i]

f.close()



