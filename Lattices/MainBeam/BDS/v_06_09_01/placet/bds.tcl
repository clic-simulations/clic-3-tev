# Girder
Marker -name BDS\$START
Girder
Quadrupole -name LTCQ0 -synrad $quad_synrad -l 0.4525417 -strength [expr 0.03049761331*$e0] -roll 0
Girder
Drift -name DRIFT_0 -l 30.69051
Girder
Quadrupole -name LTCQ1 -synrad $quad_synrad -l 5 -strength [expr -0.1251003*$e0] -roll 0
Girder
Drift -name DRIFT_1 -l 3.551866
Girder
Quadrupole -name LTCQ2 -synrad $quad_synrad -l 5 -strength [expr 0.0994664*$e0] -roll 0
Girder
Drift -name DRIFT_2 -l 15.48765
Girder
Quadrupole -name LTCQ3 -synrad $quad_synrad -l 5 -strength [expr -0.2128077*$e0] -roll 0
Girder
Drift -name DRIFT_3 -l 0.6119686
Girder
Quadrupole -name LTCQ4 -synrad $quad_synrad -l 5 -strength [expr 0.10274365*$e0] -roll 0
Girder
Drift -name DRIFT_4 -l 0.7072719
# Girder
Marker -name TRANS
Girder
Drift -name DRIFT_5 -l 10
Girder
Quadrupole -name QFEC -synrad $quad_synrad -l 2.5 -strength [expr 0.001117829009*$e0] -roll 0
# Girder
Bpm -name BPMQ
Girder
Quadrupole -name QFEC -synrad $quad_synrad -l 2.5 -strength [expr 0.001117829009*$e0] -roll 0
Girder
Drift -name DRIFT_6 -l 7.500000003
Girder
Sbend -name B3A -synrad $sbend_synrad -l 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 2.521382369e-05 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0]
Girder
Sbend -name B3B -synrad $sbend_synrad -l 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 0 -E2 2.521382369e-05 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0]
Girder
Drift -name DRIFT_7 -l 1.249999967
Girder
Sbend -name B3A -synrad $sbend_synrad -l 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 2.521382369e-05 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0]
Girder
Sbend -name B3B -synrad $sbend_synrad -l 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 0 -E2 2.521382369e-05 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0]
Girder
Drift -name DRIFT_7 -l 1.249999967
Girder
Sbend -name B3A -synrad $sbend_synrad -l 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 2.521382369e-05 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0]
Girder
Sbend -name B3B -synrad $sbend_synrad -l 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 0 -E2 2.521382369e-05 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0]
Girder
Drift -name DRIFT_8 -l 1.250000067
Girder
Sbend -name B3A -synrad $sbend_synrad -l 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 2.521382369e-05 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0]
Girder
Sbend -name B3B -synrad $sbend_synrad -l 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 0 -E2 2.521382369e-05 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0]
Girder
Drift -name DRIFT_7 -l 1.249999967
Girder
Sbend -name B3A -synrad $sbend_synrad -l 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 2.521382369e-05 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0]
Girder
Sbend -name B3B -synrad $sbend_synrad -l 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 0 -E2 2.521382369e-05 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0]
Girder
Drift -name DRIFT_7 -l 1.249999967
Girder
Sbend -name B3A -synrad $sbend_synrad -l 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 2.521382369e-05 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0]
Girder
Sbend -name B3B -synrad $sbend_synrad -l 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 0 -E2 2.521382369e-05 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0]
Girder
Drift -name DRIFT_9 -l 0.6250000333
# Girder
Marker -name DUMMY
# Girder
Marker -name DUMMY
Girder
Drift -name DRIFT_9 -l 0.6250000333
Girder
Sbend -name B3A -synrad $sbend_synrad -l 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 2.521382369e-05 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0]
Girder
Sbend -name B3B -synrad $sbend_synrad -l 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 0 -E2 2.521382369e-05 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0]
Girder
Drift -name DRIFT_7 -l 1.249999967
Girder
Sbend -name B3A -synrad $sbend_synrad -l 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 2.521382369e-05 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0]
Girder
Sbend -name B3B -synrad $sbend_synrad -l 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 0 -E2 2.521382369e-05 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0]
Girder
Drift -name DRIFT_7 -l 1.249999967
Girder
Sbend -name B3A -synrad $sbend_synrad -l 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 2.521382369e-05 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0]
Girder
Sbend -name B3B -synrad $sbend_synrad -l 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 0 -E2 2.521382369e-05 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0]
Girder
Drift -name DRIFT_8 -l 1.250000067
Girder
Sbend -name B3A -synrad $sbend_synrad -l 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 2.521382369e-05 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0]
Girder
Sbend -name B3B -synrad $sbend_synrad -l 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 0 -E2 2.521382369e-05 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0]
Girder
Drift -name DRIFT_7 -l 1.249999967
Girder
Sbend -name B3A -synrad $sbend_synrad -l 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 2.521382369e-05 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0]
Girder
Sbend -name B3B -synrad $sbend_synrad -l 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 0 -E2 2.521382369e-05 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0]
Girder
Drift -name DRIFT_7 -l 1.249999967
Girder
Sbend -name B3A -synrad $sbend_synrad -l 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 2.521382369e-05 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0]
Girder
Sbend -name B3B -synrad $sbend_synrad -l 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 0 -E2 2.521382369e-05 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0]
Girder
Drift -name DRIFT_10 -l 1.250000033
Girder
Multipole -name SYEC2 -synrad $mult_synrad -type 3 -l 1.25 -strength [expr -0.0069456013*$e0] -tilt [expr -1.0*0]
Girder
Multipole -name SYEC2 -synrad $mult_synrad -type 3 -l 1.25 -strength [expr -0.0069456013*$e0] -tilt [expr -1.0*0]
Girder
Multipole -name SYEC2 -synrad $mult_synrad -type 3 -l 1.25 -strength [expr -0.0069456013*$e0] -tilt [expr -1.0*0]
Girder
Multipole -name SYEC2 -synrad $mult_synrad -type 3 -l 1.25 -strength [expr -0.0069456013*$e0] -tilt [expr -1.0*0]
Girder
Drift -name DRIFT_11 -l 1.25
Girder
Quadrupole -name QDEC -synrad $quad_synrad -l 2.5 -strength [expr -0.001117829009*$e0] -roll 0
# Girder
Bpm -name BPMQ
Girder
Quadrupole -name QDEC -synrad $quad_synrad -l 2.5 -strength [expr -0.001117829009*$e0] -roll 0
Girder
Drift -name DRIFT_5 -l 10
# Girder
Multipole -name OCTEC3 -synrad 0 -type 4 -l 0 -strength [expr 1.0*463.8761985*$e0] -tilt [expr -1.0*0]
Girder
Drift -name DRIFT_5 -l 10
Girder
Quadrupole -name QDEC -synrad $quad_synrad -l 2.5 -strength [expr -0.001117829009*$e0] -roll 0
# Girder
Bpm -name BPMQ
Girder
Quadrupole -name QDEC -synrad $quad_synrad -l 2.5 -strength [expr -0.001117829009*$e0] -roll 0
Girder
Drift -name DRIFT_11 -l 1.25
Girder
Multipole -name SYEC2 -synrad $mult_synrad -type 3 -l 1.25 -strength [expr -0.0069456013*$e0] -tilt [expr -1.0*0]
Girder
Multipole -name SYEC2 -synrad $mult_synrad -type 3 -l 1.25 -strength [expr -0.0069456013*$e0] -tilt [expr -1.0*0]
Girder
Multipole -name SYEC2 -synrad $mult_synrad -type 3 -l 1.25 -strength [expr -0.0069456013*$e0] -tilt [expr -1.0*0]
Girder
Multipole -name SYEC2 -synrad $mult_synrad -type 3 -l 1.25 -strength [expr -0.0069456013*$e0] -tilt [expr -1.0*0]
Girder
Drift -name DRIFT_10 -l 1.250000033
Girder
Sbend -name B4A -synrad $sbend_synrad -l 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 -5.597255072e-06 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0]
Girder
Sbend -name B4B -synrad $sbend_synrad -l 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 0 -E2 -5.597255072e-06 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0]
Girder
Drift -name DRIFT_7 -l 1.249999967
Girder
Sbend -name B4A -synrad $sbend_synrad -l 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 -5.597255072e-06 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0]
Girder
Sbend -name B4B -synrad $sbend_synrad -l 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 0 -E2 -5.597255072e-06 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0]
Girder
Drift -name DRIFT_7 -l 1.249999967
Girder
Sbend -name B4A -synrad $sbend_synrad -l 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 -5.597255072e-06 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0]
Girder
Sbend -name B4B -synrad $sbend_synrad -l 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 0 -E2 -5.597255072e-06 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0]
Girder
Drift -name DRIFT_8 -l 1.250000067
Girder
Sbend -name B4A -synrad $sbend_synrad -l 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 -5.597255072e-06 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0]
Girder
Sbend -name B4B -synrad $sbend_synrad -l 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 0 -E2 -5.597255072e-06 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0]
Girder
Drift -name DRIFT_7 -l 1.249999967
Girder
Sbend -name B4A -synrad $sbend_synrad -l 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 -5.597255072e-06 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0]
Girder
Sbend -name B4B -synrad $sbend_synrad -l 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 0 -E2 -5.597255072e-06 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0]
Girder
Drift -name DRIFT_7 -l 1.249999967
Girder
Sbend -name B4A -synrad $sbend_synrad -l 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 -5.597255072e-06 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0]
Girder
Sbend -name B4B -synrad $sbend_synrad -l 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 0 -E2 -5.597255072e-06 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0]
Girder
Drift -name DRIFT_9 -l 0.6250000333
# Girder
Marker -name COLL3ESP
# Girder
# Collimator -name ENGYSP -aperture rectangular -apx 0 -apy 
Girder
Drift -name DRIFT_9 -l 0.6250000333
Girder
Sbend -name B4A -synrad $sbend_synrad -l 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 -5.597255072e-06 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0]
Girder
Sbend -name B4B -synrad $sbend_synrad -l 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 0 -E2 -5.597255072e-06 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0]
Girder
Drift -name DRIFT_7 -l 1.249999967
Girder
Sbend -name B4A -synrad $sbend_synrad -l 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 -5.597255072e-06 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0]
Girder
Sbend -name B4B -synrad $sbend_synrad -l 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 0 -E2 -5.597255072e-06 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0]
Girder
Drift -name DRIFT_7 -l 1.249999967
Girder
Sbend -name B4A -synrad $sbend_synrad -l 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 -5.597255072e-06 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0]
Girder
Sbend -name B4B -synrad $sbend_synrad -l 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 0 -E2 -5.597255072e-06 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0]
Girder
Drift -name DRIFT_8 -l 1.250000067
Girder
Sbend -name B4A -synrad $sbend_synrad -l 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 -5.597255072e-06 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0]
Girder
Sbend -name B4B -synrad $sbend_synrad -l 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 0 -E2 -5.597255072e-06 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0]
Girder
Drift -name DRIFT_7 -l 1.249999967
Girder
Sbend -name B4A -synrad $sbend_synrad -l 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 -5.597255072e-06 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0]
Girder
Sbend -name B4B -synrad $sbend_synrad -l 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 0 -E2 -5.597255072e-06 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0]
Girder
Drift -name DRIFT_7 -l 1.249999967
Girder
Sbend -name B4A -synrad $sbend_synrad -l 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 -5.597255072e-06 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0]
Girder
Sbend -name B4B -synrad $sbend_synrad -l 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 0 -E2 -5.597255072e-06 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0]
Girder
Drift -name DRIFT_10 -l 1.250000033
Girder
Multipole -name SXEC2 -synrad $mult_synrad -type 3 -l 1.25 -strength [expr 0.002699291418*$e0] -tilt [expr -1.0*0]
Girder
Multipole -name SXEC2 -synrad $mult_synrad -type 3 -l 1.25 -strength [expr 0.002699291418*$e0] -tilt [expr -1.0*0]
Girder
Multipole -name SXEC2 -synrad $mult_synrad -type 3 -l 1.25 -strength [expr 0.002699291418*$e0] -tilt [expr -1.0*0]
Girder
Multipole -name SXEC2 -synrad $mult_synrad -type 3 -l 1.25 -strength [expr 0.002699291418*$e0] -tilt [expr -1.0*0]
Girder
Drift -name DRIFT_11 -l 1.25
Girder
Quadrupole -name QFEC -synrad $quad_synrad -l 2.5 -strength [expr 0.001117829009*$e0] -roll 0
# Girder
Bpm -name BPMQ
Girder
Quadrupole -name QFEC -synrad $quad_synrad -l 2.5 -strength [expr 0.001117829009*$e0] -roll 0
Girder
Drift -name DRIFT_5 -l 10
# Girder
# Collimator -name ENGYAB -aperture rectangular -apx 0 -apy 
# Girder
Marker -name COLL4EAB
Girder
Drift -name DRIFT_5 -l 10
Girder
Quadrupole -name QFEC -synrad $quad_synrad -l 2.5 -strength [expr 0.001117829009*$e0] -roll 0
# Girder
Bpm -name BPMQ
Girder
Quadrupole -name QFEC -synrad $quad_synrad -l 2.5 -strength [expr 0.001117829009*$e0] -roll 0
Girder
Drift -name DRIFT_11 -l 1.25
Girder
Multipole -name SXEC2 -synrad $mult_synrad -type 3 -l 1.25 -strength [expr 0.002699291418*$e0] -tilt [expr -1.0*0]
Girder
Multipole -name SXEC2 -synrad $mult_synrad -type 3 -l 1.25 -strength [expr 0.002699291418*$e0] -tilt [expr -1.0*0]
Girder
Multipole -name SXEC2 -synrad $mult_synrad -type 3 -l 1.25 -strength [expr 0.002699291418*$e0] -tilt [expr -1.0*0]
Girder
Multipole -name SXEC2 -synrad $mult_synrad -type 3 -l 1.25 -strength [expr 0.002699291418*$e0] -tilt [expr -1.0*0]
Girder
Drift -name DRIFT_10 -l 1.250000033
Girder
Sbend -name B4A -synrad $sbend_synrad -l 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 -5.597255072e-06 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0]
Girder
Sbend -name B4B -synrad $sbend_synrad -l 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 0 -E2 -5.597255072e-06 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0]
Girder
Drift -name DRIFT_7 -l 1.249999967
Girder
Sbend -name B4A -synrad $sbend_synrad -l 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 -5.597255072e-06 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0]
Girder
Sbend -name B4B -synrad $sbend_synrad -l 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 0 -E2 -5.597255072e-06 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0]
Girder
Drift -name DRIFT_7 -l 1.249999967
Girder
Sbend -name B4A -synrad $sbend_synrad -l 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 -5.597255072e-06 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0]
Girder
Sbend -name B4B -synrad $sbend_synrad -l 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 0 -E2 -5.597255072e-06 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0]
Girder
Drift -name DRIFT_8 -l 1.250000067
Girder
Sbend -name B4A -synrad $sbend_synrad -l 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 -5.597255072e-06 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0]
Girder
Sbend -name B4B -synrad $sbend_synrad -l 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 0 -E2 -5.597255072e-06 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0]
Girder
Drift -name DRIFT_7 -l 1.249999967
Girder
Sbend -name B4A -synrad $sbend_synrad -l 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 -5.597255072e-06 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0]
Girder
Sbend -name B4B -synrad $sbend_synrad -l 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 0 -E2 -5.597255072e-06 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0]
Girder
Drift -name DRIFT_7 -l 1.249999967
Girder
Sbend -name B4A -synrad $sbend_synrad -l 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 -5.597255072e-06 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0]
Girder
Sbend -name B4B -synrad $sbend_synrad -l 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 0 -E2 -5.597255072e-06 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0]
Girder
Drift -name DRIFT_9 -l 0.6250000333
# Girder
Marker -name DUMMY
# Girder
Marker -name DUMMY
Girder
Drift -name DRIFT_9 -l 0.6250000333
Girder
Sbend -name B4A -synrad $sbend_synrad -l 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 -5.597255072e-06 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0]
Girder
Sbend -name B4B -synrad $sbend_synrad -l 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 0 -E2 -5.597255072e-06 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0]
Girder
Drift -name DRIFT_7 -l 1.249999967
Girder
Sbend -name B4A -synrad $sbend_synrad -l 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 -5.597255072e-06 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0]
Girder
Sbend -name B4B -synrad $sbend_synrad -l 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 0 -E2 -5.597255072e-06 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0]
Girder
Drift -name DRIFT_7 -l 1.249999967
Girder
Sbend -name B4A -synrad $sbend_synrad -l 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 -5.597255072e-06 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0]
Girder
Sbend -name B4B -synrad $sbend_synrad -l 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 0 -E2 -5.597255072e-06 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0]
Girder
Drift -name DRIFT_8 -l 1.250000067
Girder
Sbend -name B4A -synrad $sbend_synrad -l 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 -5.597255072e-06 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0]
Girder
Sbend -name B4B -synrad $sbend_synrad -l 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 0 -E2 -5.597255072e-06 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0]
Girder
Drift -name DRIFT_7 -l 1.249999967
Girder
Sbend -name B4A -synrad $sbend_synrad -l 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 -5.597255072e-06 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0]
Girder
Sbend -name B4B -synrad $sbend_synrad -l 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 0 -E2 -5.597255072e-06 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0]
Girder
Drift -name DRIFT_12 -l 1.249999667
Girder
Sbend -name B4A -synrad $sbend_synrad -l 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 -5.597255072e-06 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0]
Girder
Sbend -name B4B -synrad $sbend_synrad -l 11.30208333 -angle -5.597255072e-06 -e0 $e0 -E1 0 -E2 -5.597255072e-06 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-5.597255072e-06*-5.597255072e-06/11.30208333*$e0*$e0*$e0*$e0]
Girder
Drift -name DRIFT_13 -l 1.249999333
Girder
Multipole -name SYEC2 -synrad $mult_synrad -type 3 -l 1.25 -strength [expr -0.0069456013*$e0] -tilt [expr -1.0*0]
Girder
Multipole -name SYEC2 -synrad $mult_synrad -type 3 -l 1.25 -strength [expr -0.0069456013*$e0] -tilt [expr -1.0*0]
Girder
Multipole -name SYEC2 -synrad $mult_synrad -type 3 -l 1.25 -strength [expr -0.0069456013*$e0] -tilt [expr -1.0*0]
Girder
Multipole -name SYEC2 -synrad $mult_synrad -type 3 -l 1.25 -strength [expr -0.0069456013*$e0] -tilt [expr -1.0*0]
Girder
Drift -name DRIFT_11 -l 1.25
Girder
Quadrupole -name QDEC -synrad $quad_synrad -l 2.5 -strength [expr -0.001117829009*$e0] -roll 0
# Girder
Bpm -name BPMQ
Girder
Quadrupole -name QDEC -synrad $quad_synrad -l 2.5 -strength [expr -0.001117829009*$e0] -roll 0
Girder
Drift -name DRIFT_5 -l 10
# Girder
Multipole -name OCTEC4 -synrad 0 -type 4 -l 0 -strength [expr 1.0*-458.648884*$e0] -tilt [expr -1.0*0]
Girder
Drift -name DRIFT_5 -l 10
Girder
Quadrupole -name QDEC -synrad $quad_synrad -l 2.5 -strength [expr -0.001117829009*$e0] -roll 0
# Girder
Bpm -name BPMQ
Girder
Quadrupole -name QDEC -synrad $quad_synrad -l 2.5 -strength [expr -0.001117829009*$e0] -roll 0
Girder
Drift -name DRIFT_11 -l 1.25
Girder
Multipole -name SYEC2 -synrad $mult_synrad -type 3 -l 1.25 -strength [expr -0.0069456013*$e0] -tilt [expr -1.0*0]
Girder
Multipole -name SYEC2 -synrad $mult_synrad -type 3 -l 1.25 -strength [expr -0.0069456013*$e0] -tilt [expr -1.0*0]
Girder
Multipole -name SYEC2 -synrad $mult_synrad -type 3 -l 1.25 -strength [expr -0.0069456013*$e0] -tilt [expr -1.0*0]
Girder
Multipole -name SYEC2 -synrad $mult_synrad -type 3 -l 1.25 -strength [expr -0.0069456013*$e0] -tilt [expr -1.0*0]
Girder
Drift -name DRIFT_14 -l 1.250000333
Girder
Sbend -name B3A -synrad $sbend_synrad -l 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 2.521382369e-05 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0]
Girder
Sbend -name B3B -synrad $sbend_synrad -l 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 0 -E2 2.521382369e-05 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0]
Girder
Drift -name DRIFT_15 -l 1.250000667
Girder
Sbend -name B3A -synrad $sbend_synrad -l 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 2.521382369e-05 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0]
Girder
Sbend -name B3B -synrad $sbend_synrad -l 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 0 -E2 2.521382369e-05 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0]
Girder
Drift -name DRIFT_12 -l 1.249999667
Girder
Sbend -name B3A -synrad $sbend_synrad -l 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 2.521382369e-05 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0]
Girder
Sbend -name B3B -synrad $sbend_synrad -l 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 0 -E2 2.521382369e-05 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0]
Girder
Drift -name DRIFT_12 -l 1.249999667
Girder
Sbend -name B3A -synrad $sbend_synrad -l 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 2.521382369e-05 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0]
Girder
Sbend -name B3B -synrad $sbend_synrad -l 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 0 -E2 2.521382369e-05 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0]
Girder
Drift -name DRIFT_15 -l 1.250000667
Girder
Sbend -name B3A -synrad $sbend_synrad -l 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 2.521382369e-05 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0]
Girder
Sbend -name B3B -synrad $sbend_synrad -l 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 0 -E2 2.521382369e-05 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0]
Girder
Drift -name DRIFT_12 -l 1.249999667
Girder
Sbend -name B3A -synrad $sbend_synrad -l 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 2.521382369e-05 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0]
Girder
Sbend -name B3B -synrad $sbend_synrad -l 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 0 -E2 2.521382369e-05 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0]
Girder
Drift -name DRIFT_16 -l 0.6249993333
# Girder
Marker -name DUMMY
# Girder
Marker -name DUMMY
Girder
Drift -name DRIFT_17 -l 0.6250003333
Girder
Sbend -name B3A -synrad $sbend_synrad -l 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 2.521382369e-05 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0]
Girder
Sbend -name B3B -synrad $sbend_synrad -l 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 0 -E2 2.521382369e-05 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0]
Girder
Drift -name DRIFT_15 -l 1.250000667
Girder
Sbend -name B3A -synrad $sbend_synrad -l 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 2.521382369e-05 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0]
Girder
Sbend -name B3B -synrad $sbend_synrad -l 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 0 -E2 2.521382369e-05 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0]
Girder
Drift -name DRIFT_12 -l 1.249999667
Girder
Sbend -name B3A -synrad $sbend_synrad -l 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 2.521382369e-05 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0]
Girder
Sbend -name B3B -synrad $sbend_synrad -l 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 0 -E2 2.521382369e-05 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0]
Girder
Drift -name DRIFT_12 -l 1.249999667
Girder
Sbend -name B3A -synrad $sbend_synrad -l 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 2.521382369e-05 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0]
Girder
Sbend -name B3B -synrad $sbend_synrad -l 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 0 -E2 2.521382369e-05 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0]
Girder
Drift -name DRIFT_15 -l 1.250000667
Girder
Sbend -name B3A -synrad $sbend_synrad -l 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 2.521382369e-05 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0]
Girder
Sbend -name B3B -synrad $sbend_synrad -l 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 0 -E2 2.521382369e-05 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0]
Girder
Drift -name DRIFT_12 -l 1.249999667
Girder
Sbend -name B3A -synrad $sbend_synrad -l 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 2.521382369e-05 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0]
Girder
Sbend -name B3B -synrad $sbend_synrad -l 11.30208333 -angle 2.521382369e-05 -e0 $e0 -E1 0 -E2 2.521382369e-05 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*2.521382369e-05*2.521382369e-05/11.30208333*$e0*$e0*$e0*$e0]
Girder
Drift -name DRIFT_18 -l 7.499999333
Girder
Quadrupole -name QFEC -synrad $quad_synrad -l 2.5 -strength [expr 0.001117829009*$e0] -roll 0
# Girder
Bpm -name BPMQ
Girder
Quadrupole -name QFEC -synrad $quad_synrad -l 2.5 -strength [expr 0.001117829009*$e0] -roll 0
Girder
Drift -name DRIFT_5 -l 10
# Girder
Marker -name TRANS
Girder
Drift -name DRIFT_19 -l 0.2
Girder
Quadrupole -name QF1BC -synrad $quad_synrad -l 1 -strength [expr 0.02582599304*$e0] -roll 0
# Girder
Bpm -name BPMQ
Girder
Quadrupole -name QF1BC -synrad $quad_synrad -l 1 -strength [expr 0.02582599304*$e0] -roll 0
Girder
Drift -name DRIFT_20 -l 0.25
Girder
Quadrupole -name QF1BC -synrad $quad_synrad -l 1 -strength [expr 0.02582599304*$e0] -roll 0
# Girder
Bpm -name BPMQ
Girder
Quadrupole -name QF1BC -synrad $quad_synrad -l 1 -strength [expr 0.02582599304*$e0] -roll 0
Girder
Drift -name DRIFT_20 -l 0.25
Girder
Quadrupole -name QD1BC -synrad $quad_synrad -l 1 -strength [expr -0.02103870081*$e0] -roll 0
# Girder
Bpm -name BPMQ
Girder
Quadrupole -name QD1BC -synrad $quad_synrad -l 1 -strength [expr -0.02103870081*$e0] -roll 0
Girder
Drift -name DRIFT_20 -l 0.25
Girder
Quadrupole -name QD1BC -synrad $quad_synrad -l 1 -strength [expr -0.02103870081*$e0] -roll 0
# Girder
Bpm -name BPMQ
Girder
Quadrupole -name QD1BC -synrad $quad_synrad -l 1 -strength [expr -0.02103870081*$e0] -roll 0
Girder
Drift -name DRIFT_20 -l 0.25
Girder
Quadrupole -name QD1BC -synrad $quad_synrad -l 1 -strength [expr -0.02103870081*$e0] -roll 0
# Girder
Bpm -name BPMQ
Girder
Quadrupole -name QD1BC -synrad $quad_synrad -l 1 -strength [expr -0.02103870081*$e0] -roll 0
Girder
Drift -name DRIFT_20 -l 0.25
Girder
Quadrupole -name QD1BC -synrad $quad_synrad -l 1 -strength [expr -0.02103870081*$e0] -roll 0
# Girder
Bpm -name BPMQ
Girder
Quadrupole -name QD1BC -synrad $quad_synrad -l 1 -strength [expr -0.02103870081*$e0] -roll 0
Girder
Drift -name DRIFT_21 -l 23.26703
Girder
Quadrupole -name QF2BC -synrad $quad_synrad -l 1 -strength [expr 0.006796697944*$e0] -roll 0
# Girder
Bpm -name BPMQ
Girder
Quadrupole -name QF2BC -synrad $quad_synrad -l 1 -strength [expr 0.006796697944*$e0] -roll 0
Girder
Drift -name DRIFT_20 -l 0.25
Girder
Quadrupole -name QF2BC -synrad $quad_synrad -l 1 -strength [expr 0.006796697944*$e0] -roll 0
# Girder
Bpm -name BPMQ
Girder
Quadrupole -name QF2BC -synrad $quad_synrad -l 1 -strength [expr 0.006796697944*$e0] -roll 0
Girder
Drift -name DRIFT_22 -l 0.385153
# Girder
Marker -name TRANS
Girder
Drift -name DRIFT_23 -l 0.43201
# Girder
# Collimator -name PC -aperture  elliptic -apx 0 -apy 
Girder
Quadrupole -name QCBCOL -synrad $quad_synrad -l 1 -strength [expr -0.03033330069*$e0] -roll 0
# Girder
Bpm -name BPMQ
Girder
Quadrupole -name QCBCOL -synrad $quad_synrad -l 1 -strength [expr -0.03033330069*$e0] -roll 0
Girder
Drift -name DRIFT_24 -l 35.16752
# Girder
# Collimator -name PC -aperture  elliptic -apx 0 -apy 
# Girder
Marker -name DUMMY
Girder
Quadrupole -name QFBCOL -synrad $quad_synrad -l 1 -strength [expr 0.02204757678*$e0] -roll 0
# Girder
Bpm -name BPMQ
Girder
Quadrupole -name QFBCOL -synrad $quad_synrad -l 1 -strength [expr 0.02204757678*$e0] -roll 0
Girder
Drift -name DRIFT_25 -l 15.82195
# Girder
# Collimator -name PC -aperture  elliptic -apx 0 -apy 
# Girder
Marker -name DUMMY
Girder
Quadrupole -name QDBCOL -synrad $quad_synrad -l 1 -strength [expr -0.03394621952*$e0] -roll 0
# Girder
Bpm -name BPMQ
Girder
Quadrupole -name QDBCOL -synrad $quad_synrad -l 1 -strength [expr -0.03394621952*$e0] -roll 0
# Girder
# Collimator -name YSP1 -aperture rectangular -apx 0 -apy 
# Girder
Marker -name COLL5BDSP
Girder
Drift -name DRIFT_25 -l 15.82195
# Girder
# Collimator -name XSP1 -aperture rectangular -apx 0 -apy 
# Girder
Marker -name COLL6BFSP
Girder
Quadrupole -name QFBCOL -synrad $quad_synrad -l 1 -strength [expr 0.02204757678*$e0] -roll 0
# Girder
Bpm -name BPMQ
Girder
Quadrupole -name QFBCOL -synrad $quad_synrad -l 1 -strength [expr 0.02204757678*$e0] -roll 0
Girder
Drift -name DRIFT_24 -l 35.16752
# Girder
# Collimator -name PC -aperture  elliptic -apx 0 -apy 
Girder
Quadrupole -name QCBCOL -synrad $quad_synrad -l 1 -strength [expr -0.03033330069*$e0] -roll 0
# Girder
Bpm -name BPMQ
Girder
Quadrupole -name QCBCOL -synrad $quad_synrad -l 1 -strength [expr -0.03033330069*$e0] -roll 0
Girder
Drift -name DRIFT_26 -l 0.86402
# Girder
# Collimator -name PC -aperture  elliptic -apx 0 -apy 
Girder
Quadrupole -name QCBCOL -synrad $quad_synrad -l 1 -strength [expr -0.03033330069*$e0] -roll 0
# Girder
Bpm -name BPMQ
Girder
Quadrupole -name QCBCOL -synrad $quad_synrad -l 1 -strength [expr -0.03033330069*$e0] -roll 0
Girder
Drift -name DRIFT_24 -l 35.16752
# Girder
# Collimator -name XAB1 -aperture  elliptic -apx 0 -apy 
# Girder
Marker -name COLL7BFAB
Girder
Quadrupole -name QFBCOL -synrad $quad_synrad -l 1 -strength [expr 0.02204757678*$e0] -roll 0
# Girder
Bpm -name BPMQ
Girder
Quadrupole -name QFBCOL -synrad $quad_synrad -l 1 -strength [expr 0.02204757678*$e0] -roll 0
Girder
Drift -name DRIFT_25 -l 15.82195
# Girder
# Collimator -name YAB1 -aperture  elliptic -apx 0 -apy 
# Girder
Marker -name COLL8BDAB
Girder
Quadrupole -name QDBCOL -synrad $quad_synrad -l 1 -strength [expr -0.03394621952*$e0] -roll 0
# Girder
Bpm -name BPMQ
Girder
Quadrupole -name QDBCOL -synrad $quad_synrad -l 1 -strength [expr -0.03394621952*$e0] -roll 0
# Girder
# Collimator -name YSP2 -aperture rectangular -apx 0 -apy 
# Girder
Marker -name COLL9BDSP
Girder
Drift -name DRIFT_25 -l 15.82195
# Girder
# Collimator -name XSP2 -aperture rectangular -apx 0 -apy 
# Girder
Marker -name COLL10BFSP
Girder
Quadrupole -name QFBCOL -synrad $quad_synrad -l 1 -strength [expr 0.02204757678*$e0] -roll 0
# Girder
Bpm -name BPMQ
Girder
Quadrupole -name QFBCOL -synrad $quad_synrad -l 1 -strength [expr 0.02204757678*$e0] -roll 0
Girder
Drift -name DRIFT_24 -l 35.16752
# Girder
# Collimator -name PC -aperture  elliptic -apx 0 -apy 
Girder
Quadrupole -name QCBCOL -synrad $quad_synrad -l 1 -strength [expr -0.03033330069*$e0] -roll 0
# Girder
Bpm -name BPMQ
Girder
Quadrupole -name QCBCOL -synrad $quad_synrad -l 1 -strength [expr -0.03033330069*$e0] -roll 0
Girder
Drift -name DRIFT_26 -l 0.86402
# Girder
# Collimator -name PC -aperture  elliptic -apx 0 -apy 
Girder
Quadrupole -name QCBCOL -synrad $quad_synrad -l 1 -strength [expr -0.03033330069*$e0] -roll 0
# Girder
Bpm -name BPMQ
Girder
Quadrupole -name QCBCOL -synrad $quad_synrad -l 1 -strength [expr -0.03033330069*$e0] -roll 0
Girder
Drift -name DRIFT_24 -l 35.16752
# Girder
# Collimator -name XAB2 -aperture  elliptic -apx 0 -apy 
# Girder
Marker -name COLL11BFAB
Girder
Quadrupole -name QFBCOL -synrad $quad_synrad -l 1 -strength [expr 0.02204757678*$e0] -roll 0
# Girder
Bpm -name BPMQ
Girder
Quadrupole -name QFBCOL -synrad $quad_synrad -l 1 -strength [expr 0.02204757678*$e0] -roll 0
Girder
Drift -name DRIFT_25 -l 15.82195
# Girder
# Collimator -name YAB2 -aperture  elliptic -apx 0 -apy 
# Girder
Marker -name COLL12BDAB
Girder
Quadrupole -name QDBCOL -synrad $quad_synrad -l 1 -strength [expr -0.03394621952*$e0] -roll 0
# Girder
Bpm -name BPMQ
Girder
Quadrupole -name QDBCOL -synrad $quad_synrad -l 1 -strength [expr -0.03394621952*$e0] -roll 0
# Girder
# Collimator -name YSP3 -aperture rectangular -apx 0 -apy 
# Girder
Marker -name COLL13BDSP
Girder
Drift -name DRIFT_25 -l 15.82195
# Girder
# Collimator -name XSP3 -aperture rectangular -apx 0 -apy 
# Girder
Marker -name COLL14BFSP
Girder
Quadrupole -name QFBCOL -synrad $quad_synrad -l 1 -strength [expr 0.02204757678*$e0] -roll 0
# Girder
Bpm -name BPMQ
Girder
Quadrupole -name QFBCOL -synrad $quad_synrad -l 1 -strength [expr 0.02204757678*$e0] -roll 0
Girder
Drift -name DRIFT_24 -l 35.16752
# Girder
# Collimator -name PC -aperture  elliptic -apx 0 -apy 
Girder
Quadrupole -name QCBCOL -synrad $quad_synrad -l 1 -strength [expr -0.03033330069*$e0] -roll 0
# Girder
Bpm -name BPMQ
Girder
Quadrupole -name QCBCOL -synrad $quad_synrad -l 1 -strength [expr -0.03033330069*$e0] -roll 0
Girder
Drift -name DRIFT_26 -l 0.86402
# Girder
# Collimator -name PC -aperture  elliptic -apx 0 -apy 
Girder
Quadrupole -name QCBCOL -synrad $quad_synrad -l 1 -strength [expr -0.03033330069*$e0] -roll 0
# Girder
Bpm -name BPMQ
Girder
Quadrupole -name QCBCOL -synrad $quad_synrad -l 1 -strength [expr -0.03033330069*$e0] -roll 0
Girder
Drift -name DRIFT_24 -l 35.16752
# Girder
# Collimator -name XAB3 -aperture  elliptic -apx 0 -apy 
# Girder
Marker -name COLL15BFAB
Girder
Quadrupole -name QFBCOL -synrad $quad_synrad -l 1 -strength [expr 0.02204757678*$e0] -roll 0
# Girder
Bpm -name BPMQ
Girder
Quadrupole -name QFBCOL -synrad $quad_synrad -l 1 -strength [expr 0.02204757678*$e0] -roll 0
Girder
Drift -name DRIFT_25 -l 15.82195
# Girder
# Collimator -name YAB3 -aperture  elliptic -apx 0 -apy 
# Girder
Marker -name COLL16BDAB
Girder
Quadrupole -name QDBCOL -synrad $quad_synrad -l 1 -strength [expr -0.03394621952*$e0] -roll 0
# Girder
Bpm -name BPMQ
Girder
Quadrupole -name QDBCOL -synrad $quad_synrad -l 1 -strength [expr -0.03394621952*$e0] -roll 0
# Girder
# Collimator -name YSP4 -aperture rectangular -apx 0 -apy 
# Girder
Marker -name COLL17BDSP
Girder
Drift -name DRIFT_25 -l 15.82195
# Girder
# Collimator -name XSP4 -aperture rectangular -apx 0 -apy 
# Girder
Marker -name COLL18BFSP
Girder
Quadrupole -name QFBCOL -synrad $quad_synrad -l 1 -strength [expr 0.02204757678*$e0] -roll 0
# Girder
Bpm -name BPMQ
Girder
Quadrupole -name QFBCOL -synrad $quad_synrad -l 1 -strength [expr 0.02204757678*$e0] -roll 0
Girder
Drift -name DRIFT_24 -l 35.16752
# Girder
# Collimator -name PC -aperture  elliptic -apx 0 -apy 
Girder
Quadrupole -name QCBCOL -synrad $quad_synrad -l 1 -strength [expr -0.03033330069*$e0] -roll 0
# Girder
Bpm -name BPMQ
Girder
Quadrupole -name QCBCOL -synrad $quad_synrad -l 1 -strength [expr -0.03033330069*$e0] -roll 0
Girder
Drift -name DRIFT_26 -l 0.86402
# Girder
# Collimator -name PC -aperture  elliptic -apx 0 -apy 
Girder
Quadrupole -name QCBCOL -synrad $quad_synrad -l 1 -strength [expr -0.03033330069*$e0] -roll 0
# Girder
Bpm -name BPMQ
Girder
Quadrupole -name QCBCOL -synrad $quad_synrad -l 1 -strength [expr -0.03033330069*$e0] -roll 0
Girder
Drift -name DRIFT_24 -l 35.16752
# Girder
# Collimator -name XAB4 -aperture  elliptic -apx 0 -apy 
# Girder
Marker -name COLL19BFAB
Girder
Quadrupole -name QFBCOL -synrad $quad_synrad -l 1 -strength [expr 0.02204757678*$e0] -roll 0
# Girder
Bpm -name BPMQ
Girder
Quadrupole -name QFBCOL -synrad $quad_synrad -l 1 -strength [expr 0.02204757678*$e0] -roll 0
Girder
Drift -name DRIFT_25 -l 15.82195
# Girder
# Collimator -name YAB4 -aperture  elliptic -apx 0 -apy 
# Girder
Marker -name COLL20BDAB
# Girder
Marker -name TRANS
Girder
Drift -name DRIFT_27 -l 0.050727
Girder
Quadrupole -name BTFQ1 -synrad $quad_synrad -l 5 -strength [expr -0.06049965*$e0] -roll 0
Girder
Drift -name DRIFT_28 -l 8.712693
Girder
Quadrupole -name BTFQ2 -synrad $quad_synrad -l 5 -strength [expr 0.01522023*$e0] -roll 0
Girder
Drift -name DRIFT_29 -l 19.9951
Girder
Quadrupole -name BTFQ3 -synrad $quad_synrad -l 5 -strength [expr 0.02524031*$e0] -roll 0
Girder
Drift -name DRIFT_30 -l 19.97658
Girder
Quadrupole -name BTFQ4 -synrad $quad_synrad -l 5 -strength [expr -0.03331435*$e0] -roll 0
Girder
Drift -name DRIFT_31 -l 0.052552
# Girder
Marker -name TRANS
Girder
Drift -name DRIFT_32 -l 6.968972
Girder
Quadrupole -name QMD11 -synrad $quad_synrad -l 1 -strength [expr 0.04686316795*$e0] -roll 0
Girder
Quadrupole -name QMD11 -synrad $quad_synrad -l 1 -strength [expr 0.04686316795*$e0] -roll 0
Girder
Drift -name DRIFT_33 -l 4.244868
Girder
Quadrupole -name QMD12 -synrad $quad_synrad -l 1 -strength [expr -0.057493608*$e0] -roll 0
Girder
Quadrupole -name QMD12 -synrad $quad_synrad -l 1 -strength [expr -0.057493608*$e0] -roll 0
Girder
Drift -name DRIFT_34 -l 7.2
Girder
Quadrupole -name QMD13 -synrad $quad_synrad -l 1 -strength [expr 0.04137622415*$e0] -roll 0
Girder
Quadrupole -name QMD13 -synrad $quad_synrad -l 1 -strength [expr 0.04137622415*$e0] -roll 0
Girder
Drift -name DRIFT_34 -l 7.2
Girder
Quadrupole -name QMD14 -synrad $quad_synrad -l 1 -strength [expr -0.03613413*$e0] -roll 0
Girder
Quadrupole -name QMD14 -synrad $quad_synrad -l 1 -strength [expr -0.03613413*$e0] -roll 0
Girder
Drift -name DRIFT_35 -l 6.9046
Girder
Quadrupole -name QF8 -synrad $quad_synrad -l 0.9906 -strength [expr 0.06361905186*$e0] -roll 0
# Girder
Marker -name FFIN
Girder
Quadrupole -name QF8 -synrad $quad_synrad -l 0.9906 -strength [expr 0.06361905186*$e0] -roll 0
Girder
Drift -name DRIFT_36 -l 9.144
Girder
Quadrupole -name QD7 -synrad $quad_synrad -l 0.9906 -strength [expr 0.0004203826483*$e0] -roll 0
Girder
Quadrupole -name QD7 -synrad $quad_synrad -l 0.9906 -strength [expr 0.0004203826483*$e0] -roll 0
Girder
Drift -name DRIFT_37 -l 0.3048
Girder
Sbend -name SFFB4 -synrad $sbend_synrad -l 2.1031 -angle 1.362531828e-06 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*1.362531828e-06*1.362531828e-06/2.1031*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFB4 -synrad $sbend_synrad -l 2.1031 -angle 1.362531828e-06 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*1.362531828e-06*1.362531828e-06/2.1031*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFB4 -synrad $sbend_synrad -l 2.1031 -angle 1.362531828e-06 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*1.362531828e-06*1.362531828e-06/2.1031*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFB4 -synrad $sbend_synrad -l 2.1031 -angle 1.362531828e-06 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*1.362531828e-06*1.362531828e-06/2.1031*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFB4 -synrad $sbend_synrad -l 2.1031 -angle 1.362531828e-06 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*1.362531828e-06*1.362531828e-06/2.1031*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFB4 -synrad $sbend_synrad -l 2.1031 -angle 1.362531828e-06 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*1.362531828e-06*1.362531828e-06/2.1031*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFB4 -synrad $sbend_synrad -l 2.1031 -angle 1.362531828e-06 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*1.362531828e-06*1.362531828e-06/2.1031*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFB4 -synrad $sbend_synrad -l 2.1031 -angle 1.362531828e-06 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*1.362531828e-06*1.362531828e-06/2.1031*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFB4 -synrad $sbend_synrad -l 2.1031 -angle 1.362531828e-06 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*1.362531828e-06*1.362531828e-06/2.1031*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFB4 -synrad $sbend_synrad -l 2.1031 -angle 1.362531828e-06 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*1.362531828e-06*1.362531828e-06/2.1031*$e0*$e0*$e0*$e0]
# Girder
Girder
Drift -name DRIFT_37 -l 0.3048
# Girder
Girder
Quadrupole -name QD6C -synrad $quad_synrad -l 0.9906 -strength [expr -0.0211453476*$e0] -roll 0
Girder
Quadrupole -name QD6C -synrad $quad_synrad -l 0.9906 -strength [expr -0.0211453476*$e0] -roll 0
Girder
Drift -name DRIFT_38 -l 31.806
Girder
Quadrupole -name QD6B -synrad $quad_synrad -l 0.9906 -strength [expr -0.03228436615*$e0] -roll 0
Girder
Quadrupole -name QD6B -synrad $quad_synrad -l 0.9906 -strength [expr -0.03228436615*$e0] -roll 0
Girder
Drift -name DRIFT_37 -l 0.3048
Girder
Quadrupole -name QD6B -synrad $quad_synrad -l 0.9906 -strength [expr -0.03228436615*$e0] -roll 0
Girder
Quadrupole -name QD6B -synrad $quad_synrad -l 0.9906 -strength [expr -0.03228436615*$e0] -roll 0
Girder
Drift -name DRIFT_39 -l 35.271456
Girder
Multipole -name SF6 -synrad $mult_synrad -type 3 -l 0.499872 -strength [expr 4.386738734*$e0] -tilt [expr -1.0*0]
Girder
Multipole -name SF6 -synrad $mult_synrad -type 3 -l 0.499872 -strength [expr 4.386738734*$e0] -tilt [expr -1.0*0]
Girder
Drift -name DRIFT_37 -l 0.3048
Girder
Quadrupole -name QF5B -synrad $quad_synrad -l 0.9906 -strength [expr 0.01804597647*$e0] -roll 0
Girder
Quadrupole -name QF5B -synrad $quad_synrad -l 0.9906 -strength [expr 0.01804597647*$e0] -roll 0
Girder
Drift -name DRIFT_37 -l 0.3048
Girder
Quadrupole -name QF5B -synrad $quad_synrad -l 0.9906 -strength [expr 0.01804597647*$e0] -roll 0
Girder
Quadrupole -name QF5B -synrad $quad_synrad -l 0.9906 -strength [expr 0.01804597647*$e0] -roll 0
Girder
Drift -name DRIFT_40 -l 27.445683
# Girder
Girder
Drift -name DRIFT_41 -l 18
Girder
Quadrupole -name QF5A -synrad $quad_synrad -l 0.9906 -strength [expr 0.01650584353*$e0] -roll 0
Girder
Quadrupole -name QF5A -synrad $quad_synrad -l 0.9906 -strength [expr 0.01650584353*$e0] -roll 0
Girder
Drift -name DRIFT_37 -l 0.3048
Girder
Quadrupole -name QF5A -synrad $quad_synrad -l 0.9906 -strength [expr 0.01650584353*$e0] -roll 0
# Girder
Multipole -name MULTIF5Q -synrad 0 -type 4 -l 0 -strength [expr 1.0*31.70639381*$e0] -tilt [expr -1.0*0]
Girder
Quadrupole -name QF5A -synrad $quad_synrad -l 0.9906 -strength [expr 0.01650584353*$e0] -roll 0
Girder
Drift -name DRIFT_37 -l 0.3048
Girder
Multipole -name SF5 -synrad $mult_synrad -type 3 -l 0.499872 -strength [expr -1.274004108*$e0] -tilt [expr -1.0*0]
# Girder
Multipole -name MULTIF5 -synrad 0 -type 5 -l 0 -strength [expr 1.0*-143436.6851*$e0] -tilt [expr -1.0*0]
Girder
Multipole -name SF5 -synrad $mult_synrad -type 3 -l 0.499872 -strength [expr -1.274004108*$e0] -tilt [expr -1.0*0]
Girder
Drift -name DRIFT_42 -l 10.817894
# Girder
Girder
Quadrupole -name QD4B -synrad $quad_synrad -l 0.9906 -strength [expr -0.01089762591*$e0] -roll 0
# Girder
Multipole -name MULTID4Q -synrad 0 -type 4 -l 0 -strength [expr 1.0*26.47074814*$e0] -tilt [expr -1.0*0]
Girder
Quadrupole -name QD4B -synrad $quad_synrad -l 0.9906 -strength [expr -0.01089762591*$e0] -roll 0
Girder
Drift -name DRIFT_37 -l 0.3048
Girder
Multipole -name SD4 -synrad $mult_synrad -type 3 -l 0.499872 -strength [expr 5.363742375*$e0] -tilt [expr -1.0*0]
# Girder
Multipole -name MULTID4 -synrad 0 -type 5 -l 0 -strength [expr 1.0*8644555.525*$e0] -tilt [expr -1.0*0]
Girder
Multipole -name SD4 -synrad $mult_synrad -type 3 -l 0.499872 -strength [expr 5.363742375*$e0] -tilt [expr -1.0*0]
Girder
Drift -name DRIFT_37 -l 0.3048
Girder
Quadrupole -name QD4A -synrad $quad_synrad -l 0.9906 -strength [expr -0.01094283878*$e0] -roll 0
Girder
Quadrupole -name QD4A -synrad $quad_synrad -l 0.9906 -strength [expr -0.01094283878*$e0] -roll 0
Girder
Drift -name DRIFT_43 -l 28.926242
Girder
Sbend -name SFFB3 -synrad $sbend_synrad -l 8.9916 -angle -2.083099212e-05 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-2.083099212e-05*-2.083099212e-05/8.9916*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFB3 -synrad $sbend_synrad -l 8.9916 -angle -2.083099212e-05 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-2.083099212e-05*-2.083099212e-05/8.9916*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFB3 -synrad $sbend_synrad -l 8.9916 -angle -2.083099212e-05 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-2.083099212e-05*-2.083099212e-05/8.9916*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFB3 -synrad $sbend_synrad -l 8.9916 -angle -2.083099212e-05 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-2.083099212e-05*-2.083099212e-05/8.9916*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFB3 -synrad $sbend_synrad -l 8.9916 -angle -2.083099212e-05 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-2.083099212e-05*-2.083099212e-05/8.9916*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFB3 -synrad $sbend_synrad -l 8.9916 -angle -2.083099212e-05 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-2.083099212e-05*-2.083099212e-05/8.9916*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFB3 -synrad $sbend_synrad -l 8.9916 -angle -2.083099212e-05 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-2.083099212e-05*-2.083099212e-05/8.9916*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFB3 -synrad $sbend_synrad -l 8.9916 -angle -2.083099212e-05 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-2.083099212e-05*-2.083099212e-05/8.9916*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFB3 -synrad $sbend_synrad -l 8.9916 -angle -2.083099212e-05 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-2.083099212e-05*-2.083099212e-05/8.9916*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFB3 -synrad $sbend_synrad -l 8.9916 -angle -2.083099212e-05 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-2.083099212e-05*-2.083099212e-05/8.9916*$e0*$e0*$e0*$e0]
# Girder
Girder
Drift -name DRIFT_37 -l 0.3048
Girder
Quadrupole -name QF3B -synrad $quad_synrad -l 0.9906 -strength [expr 0.002658475451*$e0] -roll 0
Girder
Quadrupole -name QF3B -synrad $quad_synrad -l 0.9906 -strength [expr 0.002658475451*$e0] -roll 0
Girder
Drift -name DRIFT_37 -l 0.3048
Girder
Sbend -name SFFB3B -synrad $sbend_synrad -l 1.8288 -angle -4.5857e-06 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-4.5857e-06*-4.5857e-06/1.8288*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFB3B -synrad $sbend_synrad -l 1.8288 -angle -4.5857e-06 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-4.5857e-06*-4.5857e-06/1.8288*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFB3B -synrad $sbend_synrad -l 1.8288 -angle -4.5857e-06 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-4.5857e-06*-4.5857e-06/1.8288*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFB3B -synrad $sbend_synrad -l 1.8288 -angle -4.5857e-06 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-4.5857e-06*-4.5857e-06/1.8288*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFB3B -synrad $sbend_synrad -l 1.8288 -angle -4.5857e-06 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-4.5857e-06*-4.5857e-06/1.8288*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFB3B -synrad $sbend_synrad -l 1.8288 -angle -4.5857e-06 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-4.5857e-06*-4.5857e-06/1.8288*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFB3B -synrad $sbend_synrad -l 1.8288 -angle -4.5857e-06 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-4.5857e-06*-4.5857e-06/1.8288*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFB3B -synrad $sbend_synrad -l 1.8288 -angle -4.5857e-06 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-4.5857e-06*-4.5857e-06/1.8288*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFB3B -synrad $sbend_synrad -l 1.8288 -angle -4.5857e-06 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-4.5857e-06*-4.5857e-06/1.8288*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFB3B -synrad $sbend_synrad -l 1.8288 -angle -4.5857e-06 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-4.5857e-06*-4.5857e-06/1.8288*$e0*$e0*$e0*$e0]
# Girder
Girder
Drift -name DRIFT_37 -l 0.3048
Girder
Quadrupole -name QF3A -synrad $quad_synrad -l 0.9906 -strength [expr 0.01100288185*$e0] -roll 0
Girder
Quadrupole -name QF3A -synrad $quad_synrad -l 0.9906 -strength [expr 0.01100288185*$e0] -roll 0
Girder
Drift -name DRIFT_37 -l 0.3048
Girder
Sbend -name SFFB2 -synrad $sbend_synrad -l 1.8288 -angle -1.606593544e-06 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-1.606593544e-06*-1.606593544e-06/1.8288*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFB2 -synrad $sbend_synrad -l 1.8288 -angle -1.606593544e-06 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-1.606593544e-06*-1.606593544e-06/1.8288*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFB2 -synrad $sbend_synrad -l 1.8288 -angle -1.606593544e-06 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-1.606593544e-06*-1.606593544e-06/1.8288*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFB2 -synrad $sbend_synrad -l 1.8288 -angle -1.606593544e-06 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-1.606593544e-06*-1.606593544e-06/1.8288*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFB2 -synrad $sbend_synrad -l 1.8288 -angle -1.606593544e-06 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-1.606593544e-06*-1.606593544e-06/1.8288*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFB2 -synrad $sbend_synrad -l 1.8288 -angle -1.606593544e-06 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-1.606593544e-06*-1.606593544e-06/1.8288*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFB2 -synrad $sbend_synrad -l 1.8288 -angle -1.606593544e-06 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-1.606593544e-06*-1.606593544e-06/1.8288*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFB2 -synrad $sbend_synrad -l 1.8288 -angle -1.606593544e-06 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-1.606593544e-06*-1.606593544e-06/1.8288*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFB2 -synrad $sbend_synrad -l 1.8288 -angle -1.606593544e-06 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-1.606593544e-06*-1.606593544e-06/1.8288*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFB2 -synrad $sbend_synrad -l 1.8288 -angle -1.606593544e-06 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-1.606593544e-06*-1.606593544e-06/1.8288*$e0*$e0*$e0*$e0]
# Girder
Girder
Drift -name DRIFT_37 -l 0.3048
Girder
Quadrupole -name QD2 -synrad $quad_synrad -l 0.9906 -strength [expr -0.01307771774*$e0] -roll 0
Girder
Quadrupole -name QD2 -synrad $quad_synrad -l 0.9906 -strength [expr -0.01307771774*$e0] -roll 0
Girder
Drift -name DRIFT_37 -l 0.3048
Girder
Sbend -name SFFB1 -synrad $sbend_synrad -l 1.8288 -angle -6.1359868e-08 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-6.1359868e-08*-6.1359868e-08/1.8288*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFB1 -synrad $sbend_synrad -l 1.8288 -angle -6.1359868e-08 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-6.1359868e-08*-6.1359868e-08/1.8288*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFB1 -synrad $sbend_synrad -l 1.8288 -angle -6.1359868e-08 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-6.1359868e-08*-6.1359868e-08/1.8288*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFB1 -synrad $sbend_synrad -l 1.8288 -angle -6.1359868e-08 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-6.1359868e-08*-6.1359868e-08/1.8288*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFB1 -synrad $sbend_synrad -l 1.8288 -angle -6.1359868e-08 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-6.1359868e-08*-6.1359868e-08/1.8288*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFB1 -synrad $sbend_synrad -l 1.8288 -angle -6.1359868e-08 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-6.1359868e-08*-6.1359868e-08/1.8288*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFB1 -synrad $sbend_synrad -l 1.8288 -angle -6.1359868e-08 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-6.1359868e-08*-6.1359868e-08/1.8288*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFB1 -synrad $sbend_synrad -l 1.8288 -angle -6.1359868e-08 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-6.1359868e-08*-6.1359868e-08/1.8288*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFB1 -synrad $sbend_synrad -l 1.8288 -angle -6.1359868e-08 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-6.1359868e-08*-6.1359868e-08/1.8288*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFB1 -synrad $sbend_synrad -l 1.8288 -angle -6.1359868e-08 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-6.1359868e-08*-6.1359868e-08/1.8288*$e0*$e0*$e0*$e0]
# Girder
Girder
Drift -name DRIFT_37 -l 0.3048
Girder
Sbend -name SFFSB1 -synrad $sbend_synrad -l 1.8288 -angle -1.386776425e-06 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-1.386776425e-06*-1.386776425e-06/1.8288*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFSB1 -synrad $sbend_synrad -l 1.8288 -angle -1.386776425e-06 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-1.386776425e-06*-1.386776425e-06/1.8288*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFSB1 -synrad $sbend_synrad -l 1.8288 -angle -1.386776425e-06 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-1.386776425e-06*-1.386776425e-06/1.8288*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFSB1 -synrad $sbend_synrad -l 1.8288 -angle -1.386776425e-06 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-1.386776425e-06*-1.386776425e-06/1.8288*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFSB1 -synrad $sbend_synrad -l 1.8288 -angle -1.386776425e-06 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-1.386776425e-06*-1.386776425e-06/1.8288*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFSB1 -synrad $sbend_synrad -l 1.8288 -angle -1.386776425e-06 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-1.386776425e-06*-1.386776425e-06/1.8288*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFSB1 -synrad $sbend_synrad -l 1.8288 -angle -1.386776425e-06 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-1.386776425e-06*-1.386776425e-06/1.8288*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFSB1 -synrad $sbend_synrad -l 1.8288 -angle -1.386776425e-06 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-1.386776425e-06*-1.386776425e-06/1.8288*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFSB1 -synrad $sbend_synrad -l 1.8288 -angle -1.386776425e-06 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-1.386776425e-06*-1.386776425e-06/1.8288*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFSB1 -synrad $sbend_synrad -l 1.8288 -angle -1.386776425e-06 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-1.386776425e-06*-1.386776425e-06/1.8288*$e0*$e0*$e0*$e0]
# Girder
Girder
Drift -name DRIFT_37 -l 0.3048
Girder
Sbend -name SFFSB1 -synrad $sbend_synrad -l 1.8288 -angle -1.386776425e-06 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-1.386776425e-06*-1.386776425e-06/1.8288*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFSB1 -synrad $sbend_synrad -l 1.8288 -angle -1.386776425e-06 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-1.386776425e-06*-1.386776425e-06/1.8288*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFSB1 -synrad $sbend_synrad -l 1.8288 -angle -1.386776425e-06 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-1.386776425e-06*-1.386776425e-06/1.8288*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFSB1 -synrad $sbend_synrad -l 1.8288 -angle -1.386776425e-06 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-1.386776425e-06*-1.386776425e-06/1.8288*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFSB1 -synrad $sbend_synrad -l 1.8288 -angle -1.386776425e-06 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-1.386776425e-06*-1.386776425e-06/1.8288*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFSB1 -synrad $sbend_synrad -l 1.8288 -angle -1.386776425e-06 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-1.386776425e-06*-1.386776425e-06/1.8288*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFSB1 -synrad $sbend_synrad -l 1.8288 -angle -1.386776425e-06 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-1.386776425e-06*-1.386776425e-06/1.8288*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFSB1 -synrad $sbend_synrad -l 1.8288 -angle -1.386776425e-06 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-1.386776425e-06*-1.386776425e-06/1.8288*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFSB1 -synrad $sbend_synrad -l 1.8288 -angle -1.386776425e-06 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-1.386776425e-06*-1.386776425e-06/1.8288*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFSB1 -synrad $sbend_synrad -l 1.8288 -angle -1.386776425e-06 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-1.386776425e-06*-1.386776425e-06/1.8288*$e0*$e0*$e0*$e0]
# Girder
Girder
Drift -name DRIFT_37 -l 0.3048
Girder
Sbend -name SFFSB1 -synrad $sbend_synrad -l 1.8288 -angle -1.386776425e-06 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-1.386776425e-06*-1.386776425e-06/1.8288*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFSB1 -synrad $sbend_synrad -l 1.8288 -angle -1.386776425e-06 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-1.386776425e-06*-1.386776425e-06/1.8288*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFSB1 -synrad $sbend_synrad -l 1.8288 -angle -1.386776425e-06 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-1.386776425e-06*-1.386776425e-06/1.8288*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFSB1 -synrad $sbend_synrad -l 1.8288 -angle -1.386776425e-06 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-1.386776425e-06*-1.386776425e-06/1.8288*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFSB1 -synrad $sbend_synrad -l 1.8288 -angle -1.386776425e-06 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-1.386776425e-06*-1.386776425e-06/1.8288*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFSB1 -synrad $sbend_synrad -l 1.8288 -angle -1.386776425e-06 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-1.386776425e-06*-1.386776425e-06/1.8288*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFSB1 -synrad $sbend_synrad -l 1.8288 -angle -1.386776425e-06 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-1.386776425e-06*-1.386776425e-06/1.8288*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFSB1 -synrad $sbend_synrad -l 1.8288 -angle -1.386776425e-06 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-1.386776425e-06*-1.386776425e-06/1.8288*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFSB1 -synrad $sbend_synrad -l 1.8288 -angle -1.386776425e-06 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-1.386776425e-06*-1.386776425e-06/1.8288*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFSB1 -synrad $sbend_synrad -l 1.8288 -angle -1.386776425e-06 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-1.386776425e-06*-1.386776425e-06/1.8288*$e0*$e0*$e0*$e0]
# Girder
Girder
Drift -name DRIFT_37 -l 0.3048
Girder
Sbend -name SFFSB1 -synrad $sbend_synrad -l 1.8288 -angle -1.386776425e-06 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-1.386776425e-06*-1.386776425e-06/1.8288*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFSB1 -synrad $sbend_synrad -l 1.8288 -angle -1.386776425e-06 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-1.386776425e-06*-1.386776425e-06/1.8288*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFSB1 -synrad $sbend_synrad -l 1.8288 -angle -1.386776425e-06 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-1.386776425e-06*-1.386776425e-06/1.8288*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFSB1 -synrad $sbend_synrad -l 1.8288 -angle -1.386776425e-06 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-1.386776425e-06*-1.386776425e-06/1.8288*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFSB1 -synrad $sbend_synrad -l 1.8288 -angle -1.386776425e-06 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-1.386776425e-06*-1.386776425e-06/1.8288*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFSB1 -synrad $sbend_synrad -l 1.8288 -angle -1.386776425e-06 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-1.386776425e-06*-1.386776425e-06/1.8288*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFSB1 -synrad $sbend_synrad -l 1.8288 -angle -1.386776425e-06 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-1.386776425e-06*-1.386776425e-06/1.8288*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFSB1 -synrad $sbend_synrad -l 1.8288 -angle -1.386776425e-06 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-1.386776425e-06*-1.386776425e-06/1.8288*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFSB1 -synrad $sbend_synrad -l 1.8288 -angle -1.386776425e-06 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-1.386776425e-06*-1.386776425e-06/1.8288*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFSB1 -synrad $sbend_synrad -l 1.8288 -angle -1.386776425e-06 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-1.386776425e-06*-1.386776425e-06/1.8288*$e0*$e0*$e0*$e0]
# Girder
Girder
Drift -name DRIFT_37 -l 0.3048
Girder
Sbend -name SFFSB1A -synrad $sbend_synrad -l 1.8288 -angle -1.386776425e-06 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-1.386776425e-06*-1.386776425e-06/1.8288*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFSB1A -synrad $sbend_synrad -l 1.8288 -angle -1.386776425e-06 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-1.386776425e-06*-1.386776425e-06/1.8288*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFSB1A -synrad $sbend_synrad -l 1.8288 -angle -1.386776425e-06 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-1.386776425e-06*-1.386776425e-06/1.8288*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFSB1A -synrad $sbend_synrad -l 1.8288 -angle -1.386776425e-06 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-1.386776425e-06*-1.386776425e-06/1.8288*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFSB1A -synrad $sbend_synrad -l 1.8288 -angle -1.386776425e-06 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-1.386776425e-06*-1.386776425e-06/1.8288*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFSB1A -synrad $sbend_synrad -l 1.8288 -angle -1.386776425e-06 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-1.386776425e-06*-1.386776425e-06/1.8288*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFSB1A -synrad $sbend_synrad -l 1.8288 -angle -1.386776425e-06 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-1.386776425e-06*-1.386776425e-06/1.8288*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFSB1A -synrad $sbend_synrad -l 1.8288 -angle -1.386776425e-06 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-1.386776425e-06*-1.386776425e-06/1.8288*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFSB1A -synrad $sbend_synrad -l 1.8288 -angle -1.386776425e-06 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-1.386776425e-06*-1.386776425e-06/1.8288*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFSB1A -synrad $sbend_synrad -l 1.8288 -angle -1.386776425e-06 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-1.386776425e-06*-1.386776425e-06/1.8288*$e0*$e0*$e0*$e0]
# Girder
Girder
Drift -name DRIFT_37 -l 0.3048
Girder
Sbend -name SFFSB1B -synrad $sbend_synrad -l 1.8288 -angle -1.386776425e-06 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-1.386776425e-06*-1.386776425e-06/1.8288*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFSB1B -synrad $sbend_synrad -l 1.8288 -angle -1.386776425e-06 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-1.386776425e-06*-1.386776425e-06/1.8288*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFSB1B -synrad $sbend_synrad -l 1.8288 -angle -1.386776425e-06 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-1.386776425e-06*-1.386776425e-06/1.8288*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFSB1B -synrad $sbend_synrad -l 1.8288 -angle -1.386776425e-06 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-1.386776425e-06*-1.386776425e-06/1.8288*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFSB1B -synrad $sbend_synrad -l 1.8288 -angle -1.386776425e-06 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-1.386776425e-06*-1.386776425e-06/1.8288*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFSB1B -synrad $sbend_synrad -l 1.8288 -angle -1.386776425e-06 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-1.386776425e-06*-1.386776425e-06/1.8288*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFSB1B -synrad $sbend_synrad -l 1.8288 -angle -1.386776425e-06 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-1.386776425e-06*-1.386776425e-06/1.8288*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFSB1B -synrad $sbend_synrad -l 1.8288 -angle -1.386776425e-06 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-1.386776425e-06*-1.386776425e-06/1.8288*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFSB1B -synrad $sbend_synrad -l 1.8288 -angle -1.386776425e-06 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-1.386776425e-06*-1.386776425e-06/1.8288*$e0*$e0*$e0*$e0]
# Girder
Girder
Sbend -name SFFSB1B -synrad $sbend_synrad -l 1.8288 -angle -1.386776425e-06 -e0 $e0 -E1 0 -E2 0 -K [expr 0*$e0]
set e0 [expr $e0-14.1e-6*-1.386776425e-06*-1.386776425e-06/1.8288*$e0*$e0*$e0*$e0]
# Girder
Girder
Drift -name DRIFT_44 -l 16.305447
Girder
Multipole -name SF1 -synrad $mult_synrad -type 3 -l 0.3048 -strength [expr -2.040155481*$e0] -tilt [expr -1.0*0]
# Girder
Multipole -name MULTIF1 -synrad 0 -type 5 -l 0 -strength [expr 1.0*-567541.2943*$e0] -tilt [expr -1.0*0]
Girder
Multipole -name SF1 -synrad $mult_synrad -type 3 -l 0.3048 -strength [expr -2.040155481*$e0] -tilt [expr -1.0*0]
# Girder
Marker -name M21
Girder
Drift -name DRIFT_37 -l 0.3048
Girder
Quadrupole -name QF1 -synrad $quad_synrad -l 1.997964 -strength [expr 0.05369982907*$e0] -roll 0
# Girder
# Girder
Multipole -name MULTIF1Q -synrad 0 -type 4 -l 0 -strength [expr 1.0*8.533669542*$e0] -tilt [expr -1.0*0]
Girder
Quadrupole -name QF1 -synrad $quad_synrad -l 1.997964 -strength [expr 0.05369982907*$e0] -roll 0
Girder
Drift -name DRIFT_45 -l 3.9624
Girder
Multipole -name SD0 -synrad $mult_synrad -type 3 -l 0.3048 -strength [expr 7.377168873*$e0] -tilt [expr -1.0*0]
# Girder
Multipole -name MULTID0 -synrad 0 -type 5 -l 0 -strength [expr 1.0*19517200.24*$e0] -tilt [expr -1.0*0]
Girder
Multipole -name SD0 -synrad $mult_synrad -type 3 -l 0.3048 -strength [expr 7.377168873*$e0] -tilt [expr -1.0*0]
Girder
Drift -name DRIFT_37 -l 0.3048
Girder
Quadrupole -name QD0 -synrad $quad_synrad -l 1.6764 -strength [expr -0.1294399512*$e0] -roll 0
# Girder
# Girder
Multipole -name MULTID0Q -synrad 0 -type 4 -l 0 -strength [expr 1.0*-9.990009875*$e0] -tilt [expr -1.0*0]
Girder
Quadrupole -name QD0 -synrad $quad_synrad -l 1.6764 -strength [expr -0.1294399512*$e0] -roll 0
Girder
Drift -name DRIFT_46 -l 4.29768
# Girder
Marker -name IP
# Girder
Marker -name ENDEFF1
# Girder
Marker -name BDS\$END
