set e0 1496.0
set e_initial $e0

set match(emitt_x)   6.6
set match(emitt_y)   0.2
set match(phase) 0.0
set charge 4e9
set match(charge) $charge

set match(beta_x) 64.171
set match(beta_y) 18.244
set match(alpha_x) -1.95133
set match(alpha_y) 0.605865
set match(sigma_z) 44.0
set match(e_spread) -1
