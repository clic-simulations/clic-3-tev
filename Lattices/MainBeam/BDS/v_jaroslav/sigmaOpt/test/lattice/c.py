#! python
import re, os, numpy as np

madxFile = 'job.madx'
assign = ':='

############
key = 'oct0'
values = np.arange(1,3,.1)
############



def createMadx(input, output, key, value):
   #change values in madxFile2 files       
    o = open(output,"w")
    data = open(input).read()
    o.write( re.sub(key + '.*' + assign + '.*' ,key + assign + value + ';',data)  )
    o.close()

for value in values:
    madxFile2 = madxFile + '_' + str(value) 
    createMadx(madxFile,madxFile2, key, str(value))



#if os.path.isfile (madxFile2): print "Success"

