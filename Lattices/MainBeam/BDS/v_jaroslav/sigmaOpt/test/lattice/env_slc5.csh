#if ( `arch` == "x86_64")  then
    set path=( /afs/cern.ch/eng/sl/clic-code/software/x86_64/slc5/bin $path )
    if ( $?LD_LIBRARY_PATH ) then
	set LD_LIBRARY_PATH=( /afs/cern.ch/eng/sl/clic-code/software/x86_64/slc5/gsl-1.9/lib:/afs/cern.ch/eng/sl/clic-code/software/x86_64/slc5/octave-3.0.1/lib:/afs/cern.ch/eng/sl/clic-code/software/x86_64/slc5/htgen/lib:/afs/cern.ch/eng/sl/clic-code/software/x86_64/slc5/fftw-2.1.5/lib $LD_LIBRARY_PATH )	
    else
	setenv LD_LIBRARY_PATH /afs/cern.ch/eng/sl/clic-code/software/x86_64/slc5/gsl-1.9/lib:/afs/cern.ch/eng/sl/clic-code/software/x86_64/slc5/octave-3.0.1/lib:/afs/cern.ch/eng/sl/clic-code/software/x86_64/slc5/htgen/lib:/afs/cern.ch/eng/sl/clic-code/software/x86_64/slc5/fftw-2.1.5/lib
    endif
#else
#    echo " no SLC5 installation for 32 bit machines "

#endif    
    
