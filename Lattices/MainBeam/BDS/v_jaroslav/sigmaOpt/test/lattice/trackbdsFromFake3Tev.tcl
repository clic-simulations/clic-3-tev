set e_initial 1500
set e0 $e_initial
#set script_dir /local/PLACET/CLIC_example/
set script_dir /afs/cern.ch/user/r/rtomas/w1/PLACET/CLIC_example

set synrad 1
set quad_synrad 1
set mult_synrad 1
set sbend_synrad 1
set dp 0.00


set scale 1.0
source $script_dir/clic_basic_single.tcl

#source eff1s3T.placetBD
#source bds3T.placetBD
source bds.placet
#source  ff3TL6.placet
#source  ff3TL6.placetBD

proc save_beam {} {
    if {[EmittanceRun]} {
	BeamDump -file particles.out
    }
}

TclCall -script save_beam

BeamlineSet -name test

#newbds condition 6m 3tev##################
	array set match {
	beta_x 66.14532014
	beta_y 17.92472388
	alpha_x 0
	alpha_y 0
	}
######################################6

#### eff1s condition
array set match {
#alpha_x  7.41685362e-07
#alpha_y 9.604936379e-06
#beta_x  64.9998842
#beta_y  17.99971386
}



#emittances unit is e-7
set match(emitt_x)  6.6
set match(emitt_y)  0.2
set match(charge) 3.72e9
set charge $match(charge)
set match(sigma_z) 44.0
set match(phase) 0.0
set match(e_spread) -1.0

set n_slice 30
set n 2000
set n_total [expr $n_slice * $n]
source $script_dir/clic_beam.tcl


#make_beam_halo $e0 $match(e_spread) $n_total
make_beam_particles [expr $e0*(1+($dp))] $match(e_spread) $n_total


make_beam_many beam0 $n_slice $n 
BeamRead -file particles.in -beam beam0

FirstOrder 1
TestNoCorrection -beam beam0 -emitt_file emitt.dat -survey Zero

# H hist
#Histogram hx -xmin -300 -xmax 300 -n 200
#set f [open particles.out r]
#gets $f line
#while {![eof $f]} {
#	hx add "[expr 1000.0*[lindex $line 1]] 1.0"
#      	gets $f line
#}
#close $f
#hx print hx.dat

# V hist
#Histogram hy -xmin -10 -xmax 10 -n 200
#set f [open particles.out r]
#gets $f line
#while {![eof $f]} {
#	hy add "[expr 1000.0*[lindex $line 2]] 1.0"
#      	gets $f line
#}
#close $f
#hy print hy.dat

#exec gnuplot GaussFit.gp

#exit

exec ./AveSc particles.out > sizes_dp_${dp}_rad_${synrad}

#exit
#TwissPlot -beam beam0 -file twiss.dat
#set twiss [TwissPlot -beam beam0]



#Octave {

#     [E,Beam]=placet_test_no_correction("electron", "beam0", "Zero");
#    save -text emitt E
			
#	      }



proc run_guinea {offx offy} {
      global gp_param
      set res [exec /afs/cern.ch/eng/sl/lintrack/PLACET/x86_64/bin/grid]
      set yoff [expr $offy-0.5*([lindex $res 2]+[lindex $res 3])]
      set xoff [expr $offx-0.5*([lindex $res 0]+[lindex $res 1])]
      set tx $gp_param(cut_x)
      set ty $gp_param(cut_y)
      if {[lindex $res 1]-[lindex $res 0]>2.0*$tx} {
              set gp_param(cut_x) [expr 0.5*([lindex $res 1]-[lindex $res 0])]
       }
      if {[lindex $res 3]-[lindex $res 2]>2.0*$ty} {
              set gp_param(cut_y) [expr 0.5*([lindex $res 3]-[lindex $res 2])]
       }
      puts "yoff $yoff"
      puts "xoff $xoff"
      puts "cut_x $gp_param(cut_x)"
      puts "cut_y $gp_param(cut_y)"
      puts "n_x $gp_param(n_x)"
      puts "n_y $gp_param(n_y)"		
      puts $res
      #
      # Modify this
      #
      #write_guinea_correct_angle_xy $xoff 0.0 $yoff 0.0
      write_guinea_correct $xoff $yoff
                                     
      exec guinea default default default
      set gp_param(cut_x) $tx
      set gp_param(cut_y) $ty
      return [get_results default]
       }
                                                            


#array set gp_param "
#    energy 249.3
#    particles [expr $match(charge)*1e-10]
#    sigmaz $match(sigma_z)
#    cut_x 200.0
#    cut_y 25.0
#    n_x 64
#    n_y 320
#    do_coherent 1
#    n_t 1
#    charge_sign -1.0"


array set gp_param "
    energy 1500.0
    particles [expr $match(charge)*1e-10]
    sigmaz $match(sigma_z)
    cut_x 400.0
    cut_y 15.0
    n_x 128
    n_y 256
    do_coherent 1
    n_t 1
    charge_sign -1.0"


                                      

source $script_dir/clic_guinea.tcl
exec cp particles.out electron.ini
exec cp particles.out positron.ini

set res [run_guinea 0.0 0.0]
set lumi [lindex $res 0]
set lumipeak [lindex $res 1]

puts "lumi $lumi"
puts "lumipeak $lumipeak"
exit
    



source $script_dir/clic_guinea.tcl
set gp_param(n_x) 128
set gp_param(cut_x) 400.0
set gp_param(n_y) 256
set gp_param(cut_y) 15.0
set gp_param(sigmaz) $match(sigma_z)
set gp_param(particles) [expr 1e-10*$charge]
write_guinea_offset_angle 0.0 0.0
exec cp particles.out electron.ini
exec cp particles.out positron.ini

exec guinea default default default

exec  grep lumi_ee= default > lumi

exec  grep lumi_ee_high= default > lumi_high


exit
