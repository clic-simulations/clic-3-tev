#! python
import re, os

madxFile = 'job.madx'
placetOut = 'placet.out_*'
sep = '\t'
valueSep ='_'
header = '# parameter'+sep+'value'+ sep +'lumi'+ sep +'lumipeak' 
# print header
def getFiles( arg, dirname, fnames ):
    for file in fnames:
        if re.search(placetOut,file):
            keyValue = file.split(valueSep)
            infile = open(file,"r")
            text = infile.read()
            infile.close()
            if text !="" :
                lumi = re.findall('lumi.*',text)[0].split()[1]
                lumipeak = re.findall('lumi.*',text)[1].split()[1]
                if lumi !="" and lumipeak!="":
                    print sep.join(keyValue[1:]) + sep + lumi + sep + lumipeak 

arglist = []
os.path.walk("./",getFiles,arglist)




