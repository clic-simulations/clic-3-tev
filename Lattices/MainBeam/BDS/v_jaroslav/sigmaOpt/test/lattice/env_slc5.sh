#if test `arch` = "x86_64" ; then
    export PATH=/afs/cern.ch/eng/sl/clic-code/software/x86_64/slc5/bin:$PATH
    if test -z "$LD_LIBRARY_PATH" ; then
	export LD_LIBRARY_PATH=/afs/cern.ch/eng/sl/clic-code/software/x86_64/slc5/gsl-1.9/lib:/afs/cern.ch/eng/sl/clic-code/software/x86_64/slc5/octave-3.0.1/lib:/afs/cern.ch/eng/sl/clic-code/software/x86_64/slc5/htgen/lib:/afs/cern.ch/eng/sl/clic-code/software/x86_64/slc5/fftw-2.1.5/lib
    else
	export LD_LIBRARY_PATH=/afs/cern.ch/eng/sl/clic-code/software/x86_64/slc5/gsl-1.9/lib:/afs/cern.ch/eng/sl/clic-code/software/x86_64/slc5/octave-3.0.1/lib:/afs/cern.ch/eng/sl/clic-code/software/x86_64/slc5/htgen/lib:/afs/cern.ch/eng/sl/clic-code/software/x86_64/slc5/fftw-2.1.5/lib:$LD_LIBRARY_PATH
    fi
#else
#    echo " no SLC5 installation for 32 bit machines "
#fi 
