#! python
import re, os, numpy as np

madxFile = 'job.madx'
assign = ':='
sep ='_'
############
key = np.array(['kocttest','kdectest'])
#scale = np.arange(0,10,1)
############

scriptFile = 'run_' + sep.join(key)

def cartesian(arrays, out=None):
    """
    Generate a cartesian product of input arrays.

    Parameters
    ----------
    arrays : list of array-like
        1-D arrays to form the cartesian product of.
    out : ndarray
        Array to place the cartesian product in.

    Returns
    -------
    out : ndarray
        2-D array of shape (M, len(arrays)) containing cartesian products
        formed of input arrays.

    Examples
    --------
    >>> cartesian(([1, 2, 3], [4, 5], [6, 7]))
    array([[1, 4, 6],
           [1, 4, 7],
           [1, 5, 6],
           [1, 5, 7],
           [2, 4, 6],
           [2, 4, 7],
           [2, 5, 6],
           [2, 5, 7],
           [3, 4, 6],
           [3, 4, 7],
           [3, 5, 6],
           [3, 5, 7]])

    """

    arrays = [np.asarray(x) for x in arrays]
    dtype = arrays[0].dtype

    n = np.prod([x.size for x in arrays])
    if out is None:
        out = np.zeros([n, len(arrays)], dtype=dtype)

    m = n / arrays[0].size
    out[:,0] = np.repeat(arrays[0], m)
    if arrays[1:]:
        cartesian(arrays[1:], out=out[0:m,1:])
        for j in xrange(1, arrays[0].size):
            out[j*m:(j+1)*m,1:] = out[0:m,1:]
    return out

def createMadx(input, output, key, value):
   # change values in madxFile2 files       
    o = open(output,"w")
    data = open(input).read()
    for i in range(0,len(key)): 
        o.write( re.sub(key[i] + '.*' + assign + '.*' ,key[i] + assign + str(value[i]) + ';',data)  )
    o.close()

def createScript(suffix):
    # create script file to compute luminosity
    o = open(scriptFile,"a")
    s = "\n\n # " + suffix + "\n\
    madx < " + "\"" + madxFile2 + "\"" + "\n\
    madXoptics.tcl ff4placet bds.placet\n\
    placet-development trackbdsFromFake3Tev.tcl > \"placet.out" + suffix + "\"\n\
    python a.py >> lumi.out"+ "\n\
    rm  \"placet.out" + suffix + "\""
    o.write(s)
    o.close()

def createHeader(scriptFile):
    o = open(scriptFile,"w")
    header = "#!/bin/bash\n\
    source /afs/cern.ch/eng/sl/clic-code/software/env.sh\n"
    o.write(header)
    o.close()

# cartesian product
##################
values = cartesian((np.arange(-2181,1819,100),np.arange(14453948,14493948,1000)))
##################

# Create run script
createHeader(scriptFile)
for value in values:
    suffix = sep + sep.join(key) + sep + sep + str(value[0]) + sep + str(value[1])
    madxFile2 = madxFile + suffix 
    
    createMadx(madxFile,madxFile2, key, value)
    createScript(suffix)
    
#if os.path.isfile (madxFile2): print "Success"

