#! python
import re, os, numpy as np

madxFile = 'job.madx'
assign = ':='

############
key = 'ksd4'
values = np.arange(3-.5,3+.5,.02)
############

scriptFile = 'run_' + key


def createMadx(input, output, key, value):
   #change values in madxFile2 files       
    o = open(output,"w")
    data = open(input).read()
    o.write( re.sub(key + '.*' + assign + '.*' ,key + assign + value + ';',data)  )
    o.close()

def createScript(suffix):
    # create script file to compute luminosity
    o = open(scriptFile,"a")
    s = "\n\n # " + suffix + "\n\
    madx < " + madxFile2 + " > out.log" + suffix + "\n\
    madXoptics.tcl ff4placet bds.placet\n\
    time placet-development trackbdsFromFake3Tev.tcl > placet.out" + suffix
    o.write(s)
    o.close()

def createHeader(scriptFile):
    o = open(scriptFile,"w")
    header = "#!/bin/bash\n\
    source /afs/cern.ch/eng/sl/clic-code/software/env.sh\n"
    o.write(header)
    o.close()

# Create run script
createHeader(scriptFile)
for value in values:
    suffix = '_'+ key + '_' + str(value) 
    madxFile2 = madxFile + suffix
    
    createMadx(madxFile,madxFile2, key, str(value))

    createScript(suffix)
    

#if os.path.isfile (madxFile2): print "Success"

