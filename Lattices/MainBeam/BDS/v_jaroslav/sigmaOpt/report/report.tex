%\documentclass[a4paper,11pt]{article}
\documentclass{abnote}
\title{Non-linear optimatization of the CLIC FFS}
\author{Jaroslav Vazny \\
  Masarykova Univerzita,\\
  Czech Republic \\
  \texttt{jaroslav.vazny@gmail.com}}
\date{\today}  

\documentlabel{AB-Note-2010-0XX (ABP)} 

\usepackage[pdftex]{graphicx}
\usepackage{sidecap}
\usepackage{listings}
\usepackage{color}
\usepackage{times}
\usepackage{amsmath}
\usepackage{booktabs}
\usepackage{url}


\begin{document}
\input{lst.tex}

\abstract{
  The aim of this work is to improve beam size and luminosity of the
  CLIC FFS using non-linear elements.
}



\maketitle


\thispagestyle{empty}
{\color{white} \tiny .} 
\newpage
\setcounter{page}{2}

\section{Project Targets}

The aim of this project was to use MAD~-~X \cite{bib:madx} and PLACET
\cite{bib:placet} program to optimize beam size and
luminosity. Luminosity is defined \cite{bib:cas} as :

\begin{equation}
  \label{eq:luminosity}
 \mathcal{L}  =   \frac{f_{rep}n_bN_b^2}{4\pi \sigma_x^* \sigma_y^*}H_D,
\end{equation}

\noindent where $N_b$ is colliding bunch population,$\sigma_x^*$,
$\sigma_y^*$ are the transverse r.m.s spot sizes at the IP, $H_D$ is
luminosity enhancement factor.



\section{Procedure}
Program for changes in the MAD~-~X input files was created as well as
program for analyzing the output files from PLACET.  This allowed
automatics scanning of the influence on the luminosity by individual
non-linear elements. 


The final output is in the figure \ref{fig:output}

\bigskip

\begin{figure}[hb]
  \centering
  \begin{lstlisting}[frame=single]
    kocttest kdectest -285 14471447 4.52194e+34 1.67292e+34
    kocttest kdectest -285 14471547 4.52194e+34 1.67292e+34
    .
    .
    kocttest kdectest -285 14471847 4.52194e+34 1.67292e+34
  \end{lstlisting} 
  \caption{Output of scanning process. First columns contain name of
    the element, followed by their value and resulting luminosity.}
  \label{fig:output}
  \vspace{10pt}
\end{figure}

%\clearpage

  % \begin{lstlisting}[frame=single]
  %   job.madx_kocttest_kdectest__-300_14471447
  %   job.madx_kocttest_kdectest__-300_14471547
  %   .
  %   .
  %   job.madx_kocttest_kdectest__-300_14471847
  % \end{lstlisting}


% \begin{lstlisting}[frame=single]
% #!/bin/bash
%     source /afs/cern.ch/eng/sl/clic-code/software/env.sh

%  # _kocttest_kdectest__-300_14471447
%     time MAD~-~X < "job.madx_kocttest_kdectest__-300_14471447" > "out.log_kocttest_kdectest__-300_14471447"
%     MAD~-~Xoptics.tcl ff4PLACET bds.PLACET
%     PLACET-development trackbdsFromFake3Tev.tcl > "PLACET.out_kocttest_kdectest__-300_14471447"
% \end{lstlisting}

% This program also generate shell script for automatic run of all
% files. The program can handle one or more parameters (Cartesian
% product is used for more than two parameters). But this is very
% limited due to problem with space (limited by 500~MiB on my profile)
% and time ($n^2$ dependency)

% Another program (a.py) is used for analyze the results. It searchs for
% lumi and lumipeak keyword in the PLACET output files and generate
% following file.

\subsection{Used Tools}

\begin{itemize}
  \item MAD~-~X \cite{bib:madx}
    is a tool for charged-particle optics in alternating-gradient accelerators and beam lines.
  \item PLACET \cite{bib:placet} (Program for Linear Accelerator Correction Efficiency Tests)
    was developed to simulate the different beam lines in CLIC. In
    this project it was used to compute luminosity.
  \item Python \cite{bib:python} is a general-purpose high-level programming language
    whose design philosophy emphasizes code readability. In this
    project it was used with combination with IPython and Emacs to
    automate the process of optimization.
  \item Git \cite{bib:git}
    is a distributed revision control system with an emphasis
    on speed. Git was initially designed and developed by Linus
    Torvalds for Linux kernel development.
  \item MAPCLASS \cite{bib:mapclass}
    MAPCLASS is a code written in PYTHON conceived to optimize
    the non-linear aberrations  of the Final Focus System of CLIC.
    MAPCLASS calls MADX-PTC to obtain the map coefficients
    and uses optimization algorithms like the Simplex to
    compensate the high order aberrations.
\end{itemize}



\section{Results}

Two no-linear elements were introduced into the lattice: octupole
(kocttest) and decapole (kdectest). The final values after
optimization are: -181.13~T/m$^3$ for kocttest and
14473947.93~T/m$^4$ for kdectest.

\subsection{Magnets strength}

In MAD~-~X\cite{bib:madx}  coefficients $k_x$ are used instead of physical
strength. For octupole Eq.\ref{eq:octupole} and Eq.\ref{eq:decapole} for
Decapole.  Values in Tesla can be obtained by integrating with respect
to x. For octupole we get Eq.\ref{eq:octupole_int} and Eq.\ref{eq:decapole_int}
for decapole.

\begin{equation}
  \label{eq:octupole}
  k_3 = \frac{1}{p/c} \frac{\partial^3 B_y}{\partial^3x} \hspace{5mm}[\textnormal{T/m}^3],
\end{equation}


\begin{equation}
  \label{eq:decapole}
k_4 = \frac{1}{p/c} \frac{\partial^4 B_y}{\partial^4x}\hspace{5mm}[\textnormal{T/m}^3],    
\end{equation}



\begin{equation}
  \label{eq:octupole_int}
B_{octupole}= k_3 \frac{p}{c} \frac{x^3}{6},
\end{equation}


\begin{equation}
  \label{eq:decapole_int}
B_{decapole} = k_4 \frac{p}{c} \frac{x^4}{24},
\end{equation}

\noindent Substituting values from \cite{bib:clic} we get
Eq.\ref{eq:octupole_int_v} for octupole and Eq.\ref{eq:decapole_int_v} for decapole.


\begin{equation}
  \label{eq:octupole_int_v}
B_{octupole} = k_3 \times 3.3 \times 1500 \frac{(10^{-2})^3}{24},
\approx  0.15 \textrm{T},
\end{equation}


\begin{equation}
  \label{eq:decapole_int_v}
B_{decapole} = k_4 \times 3.3 \times 1500 \frac{(10^{-2})^4}{24},
\approx 29.85 \textrm{T}.
\end{equation}




% \begin{align}
%   B_{kocttest} &= 0.15 \textrm{T} \\
%   B_{kdectest} &= 29.85 \textrm{T} 
% \end{align}

\noindent Figure \ref{fig:sigma} shows beam sizes versus order considered in
the simulation. Improvement in y-axis is noticeable on right chart.
Graphs on figure \ref{fig:magnet} and \ref{fig:magnet2} shows
dependency of the luminosity on strength of individual non-linear
elements. These graphs are the result of the optimization process.
Dependency on more than one element was tested using Cartesian product
of the values of two elements. Results is shown on figure \ref{fig:magnet3}


\begin{table}[!ht]
  \centering
  \begin{tabular}{l | r r r r}
    \toprule
    Parameter & Design  & Initial & Final & Improvements \\
    \midrule
    Horizontal beam size [nm]  & 40 & 41.50 & 41.50 & 0  \\
    Vertical beam size [nm]    & 1 & 1.36 & 1.20 &  0.16 \\
    Luminosity $[10^{34} cm^{-2} s^{-1}]$      & 3.69 & 4.49 & 4.54 &  0.04 \\
    Luminosity peak $[10^{34} cm^{-2} s^{-1}]$ & 1.25 & 1.66 & 1.68 &  0.01 \\
    \bottomrule
  \end{tabular}
  \vspace{40pt}
  \caption{Parameters summary. Values before, after optimization and designed values for 3~TeV.}
  \label{tab:summary}
\end{table}


\begin{figure}[!ht]
  \centering
  \includegraphics[scale = 0.6]{resultsigma}
  \caption{Beam size at the interaction point.}
  \label{fig:sigma}
  \vspace{-30pt}
\end{figure}


\begin{figure}[!ht]
  \centering
  \includegraphics[scale = 0.6]{magnetsOpt}
  \caption{Luminosity dependence on magnet strength.}
  \label{fig:magnet}
%  \vspace{30pt}
\end{figure}


\begin{figure}[!ht]
  \centering
  \includegraphics[scale = 0.6]{magnetsOpt2}
  \caption{Luminosity dependence on magnet strength.}
  \label{fig:magnet2}
  \vspace{-30pt}
\end{figure}


\begin{figure}[!ht]
  \centering
  \includegraphics[scale = 1.2]{kocttestKdectest}
  \caption{Luminosity dependence on two elements.}
  \label{fig:magnet3}
  \vspace{-30pt}
\end{figure}

\clearpage




% lumi 4.52132e+34
% lumipeak 1.64596e+34

% oct0 	:=280  
% lumi 4.53225e+34
% lumipeak 1.65989e+34

% dec1 	:=47437038
% lumi 4.53168e+34
% lumipeak 1.66477e+34

% kdectest :=14474272
% lumi 4.52437e+34
% lumipeak 1.6859e+34

% kocttest :=-270
% lumi 4.53574e+34
% lumipeak 1.67797e+34

% ksd0 	:= 10 ;
% lumi 1.29385e+32
% lumipeak 1.09442e+32


\section{Conclusion}
Automatization of the optimization process led to some improvements
in the luminosity and beam size. Individual tests takes 35
seconds on lxplus4 computer. Graph \ref{fig:magnet} and
\ref{fig:magnet2} contains 50 points. Graph \ref{fig:magnet3} contains
1600 points and the stimulation takes 14 hours.

\section*{Acknowledgments}
Thanks to my supervisor R.~Tom\'as and Guillermo Zamudio Ascencio for their
extraordinary help during my summer student program.
 
% \subsection{Automatization}
% The purpose of using computer in science is not only perform intensive
% computation and simulations but also reduce repetitive tasks. Anything
% what is done more than once can be automated in same way. This
% philosophy was used during this project.

% \subsection{Version Control}
%  Almost everything in physicist life is plain text
% (data, source codes, \LaTeX source) and therefore very suitable for
% version control. This alone can significantly improve quality of our
% work and simplify collaboration.

\begin{thebibliography}{99}
\bibitem{bib:madx} Herr, Werner ; Schmidt, F, A MAD~-~X Primer, 2004 
\bibitem{bib:cas} Neil, M., CERN Accelerator School, fifth general
  accelerator physics course, 1992
\bibitem{bib:git} GIT, \url{http://git-scm.com/}
\bibitem{bib:python} PYTHON, \url{http://www.python.org/}
\bibitem{bib:placet}
  PLACET,\url{http://dschulte.home.cern.ch/dschulte/placet.html}
\bibitem{bib:mapclass} R.~Tom\'as, MAPCLASS : a code to optimize high
  order aberrations,CERN-AB-Note-2006-017 \url{http://cdsweb.cern.ch/record/944769}
\bibitem{bib:clic} R. Mutzner  et al., Multi-Bunch eﬀect of resistive wall in the beam delivery system
    of the Compact Linear Collider, CLIC-note-818, (2010).

\end{thebibliography}

\end{document}
