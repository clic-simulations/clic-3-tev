#!/usr/opt/bin/bash
# Creates report.pdf 
gnuplot "plot.plt"
gnuplot "plot2.plt"
gnuplot "plotSigma.plt"
gnuplot "plot3d.plt"
epstopdf ../result/magnetsOpt.ps --outfile=magnetsOpt.pdf
epstopdf ../result/magnetsOpt2.ps --outfile=magnetsOpt2.pdf
epstopdf ../result/resultsigma.ps --outfile=resultsigma.pdf
epstopdf ../result/kocttestKdectest.ps --outfile=kocttestKdectest.pdf

pdflatex report.tex
