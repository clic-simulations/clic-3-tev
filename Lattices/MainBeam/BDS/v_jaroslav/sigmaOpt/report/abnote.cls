%  CERN AB Department Report documents in LaTeX format
%  Changed Division to Department 12/2/2004 (JMJ)
%
%  Trivial modification of slnote.cls, created 6/8/2003 which had the following history:
%
%  Version 1.01  (14/2/95)  JMJ
%  Adapted to latex2e from the old lepnote.sty, just a rather minor
%  modification of 12pt article class.
%
%  Modification:28/2/95 (changed name of organization)
%                2/2/96 fixed paper size. J.Poole
%                9/12/97 Update for electronic publication
%                           - uses times package
%                           - front page layout
%                           -
%                2/9/98 Fix ProvidesClass
%                       allow 10 and 11pt options, keep 12pt default
%                            - requires changes to all of maketitle stuff
%  Last Modification:6/8/2003
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
\def\fileversion{1.0}
\def\filedate{2003/08/06}
\def\docdate {2003/08/06}
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{abnote}[\filedate\space abnote class]

\newcommand\@slptsize{}

\DeclareOption{10pt}{\renewcommand\@slptsize{10pt}}
\DeclareOption{11pt}{\renewcommand\@slptsize{11pt}}
\DeclareOption{12pt}{\renewcommand\@slptsize{12pt}}


\DeclareOption*{\PassOptionsToClass{\CurrentOption}{article}}
\ExecuteOptions{12pt}
\ProcessOptions

\LoadClass[\@slptsize]{article}
\RequirePackage{times}
%
% DIMENSION OF TEXT:

\setlength{\textheight}{255mm}
\setlength{\textwidth}{174mm}
\setlength{\topmargin}{8mm}
\setlength{\oddsidemargin}{18mm}
\setlength{\evensidemargin}{18mm}

\hoffset -25.4mm
\voffset -25.4mm

\renewcommand{\topfraction}{.995}
\renewcommand{\bottomfraction}{.995}
\renewcommand{\textfraction}{0.01}

\typeout{Document Style `abnote' 1.0  6 August 2003}
%it uses the following info
%   \typist{..}  (may be filename) printed at top left of title page
%   \title{..}
%   \author{..}
%   \date{..}  optional
%   \documentlabel{..}  printed at top right of title page

% You also need to use \begin{document} and \end{document}

% \@startsection{NAME}{LEVEL}{INDENT}{BEFORESKIP}{AFTERSKIP}{STYLE}
%            optional * [ALTHEADING]{HEADING}
%    Generic command to start a section.
%    NAME       : e.g., 'subsection'
%    LEVEL      : a number, denoting depth of section -- e.g., section=1,
%                 subsection = 2, etc.  A section number will be printed if
%                 and only if LEVEL < or = the value of the secnumdepth
%                 counter.
%    INDENT     : Indentation of heading from left margin
%    BEFORESKIP : Absolute value = skip to leave above the heading.
%                 If negative, then paragraph indent of text following
%                 heading is suppressed.
%    AFTERSKIP  : if positive, then skip to leave below heading,
%                       else - skip to leave to right of run-in heading.
%    STYLE      : commands to set style
%  If '*' missing, then increments the counter.  If it is present, then
%  there should be no [ALTHEADING] argument.  A sectioning command
%  is normally defined to \@startsection + its first six arguments.

\def\section{\@startsection{section}{1}{\z@}{-3.5ex plus -1ex minus
    -.2ex}{2.3ex plus .2ex}{\large\bf}}

\def\subsection{\@startsection{subsection}{2}{\z@}{-3.25ex plus -1ex minus
   -.2ex}{1.5ex plus .2ex}{\normalsize\bf}}

\def\subsection{\@startsection{subsection}{2}{\z@}{-1.5ex  plus-1ex
    minus-.2ex}{1.5ex plus.2ex}{\normalsize\bf}}

\def\subsubsection{\@startsection{subsubsection}{3}{\z@}{-3.25ex plus
 -1ex minus -.2ex}{1.5ex plus .2ex}{\normalsize\bf}}

\def\paragraph{\@startsection
   {paragraph}{4}{\z@}{-2.25ex plus 1ex minus .2ex}{1em}{\normalsize\bf}}

\def\subparagraph{\@startsection
     {subparagraph}{4}{\parindent}{3.25ex plus 1ex minus
     .2ex}{-1em}{\normalsize\sl}}

% APPENDIX
%
% The \appendix command must do the following:
%    -- reset the section and subsection counters to zero
%    -- redefine the section counter to produce appendix numbers
%    -- redefine the \section command if appendix titles and headings are
%       to look different from section titles and headings.

\def\appendix{\par
  \setcounter{section}{0}
  \setcounter{subsection}{0}
  \def\thesection{\Alph{section}}}

%-----------------------------------------------------------------------
% THE REMAINDER OF THIS FILE CONSISTS OF ADDITIONS TO ART12.DOC RATHER
% THAN JUST MODIFICATIONS.

%************************************************************************
%************************** TITLE PAGE LAYOUT ***************************
%************************************************************************

% Copy following definition from ARTICLE.DOC so that we can set the
% whole title page the way we want.
\def\maketitle{\par
 \begingroup
   \def\thefootnote{\fnsymbol{footnote}}
   \def\@makefnmark{\hbox
       to 0pt{$^{\@thefnmark}$\hss}}
     \newpage
     \global\@topnum\z@    % Prevents figures from going at top of page.
     \@maketitle \thispagestyle{empty}
     \@thanks
 \newpage
 \endgroup
 \setcounter{footnote}{0}
 \let\maketitle\relax
 \let\@maketitle\relax
 \gdef\@textofabstract{}                       %just to save memory
 \gdef\@thanks{}\gdef\@author{}\gdef\@title{}\let\thanks\relax}

\def\@maketitle{
 \fontsize{12}{14.5}\selectfont
 \begin{center}   \textbf{ EUROPEAN ORGANIZATION FOR NUCLEAR RESEARCH}\\
 {\vskip 3mm}
                   CERN~~--~~AB DEPARTMENT
 \end{center}\par
 \vskip 3mm
%
% Here we put the typists name at top left and the document  label at the
% top right.  Defaults are an empty box for typist and "Preliminary
% draft" with date and time made for the label.
% Tabular environments are used to allow for the
% possibility of multiple line document labels - lines separated by \\
% in the argument of \documentlabel - and multiple line typists (which
% could have other uses ...
%
  \begin{tabular}{l} \footnotesize\@typist
  \end{tabular}  \hfill   \textbf{\begin{tabular}{r} {\bf \@documentlabel}
                          \end{tabular}}
  \vspace{\fill}                             %Vertical space above title.
 \begin{center}
   {\fontsize{20}{24.5}\selectfont\textbf{\@title}\\[1.5em] }
  {\fontsize{14}{17.0}\selectfont    % each author set in 14pt, in a
   \lineskip .5em                    % tabular environment
  \textbf{
   \begin{tabular}[t]{c}\@author
   \end{tabular}}\\[2em]}
  {\fontsize{12}{14.5}\selectfont
   \textbf{Abstract}}\\[.5ex]
 \end{center}
  \quotation\@textofabstract
  \vspace{\fill}
 \begin{center}
  {Geneva, Switzerland}\\[.5em]
  { \@date}
 \end{center}
 }

% compute the time in hours and minutes; make new variables \timehh and \timemm
%
\newcount\timehh\newcount\timemm
\timehh=\time
\divide\timehh by 60 \timemm=\time
\count255=\timehh\multiply\count255 by -60 \advance\timemm by \count255
%
\def\timeofday{\ifnum\timehh<10 0\fi\number\timehh:%
               \ifnum\timemm<10 0\fi\number\timemm
               }

\def\documentlabel#1{\gdef\@documentlabel{#1}}       %see \@maketitle
\gdef\@documentlabel{\tt Preliminary draft\ \timeofday\ \today}
%%                         Modified at 17:59:50 on 23 Sep 1988 by JOWETT

\def\typist#1{\gdef\@typist{#1}}          % see \@maketitle
\gdef\@typist{\hbox{}}
% The \documentlabel and \typist commands do not exist in ordinary LaTeX.

% Let abstract work as a declaration instead of an environment.
\long\def\abstract#1{\global\long\def\@textofabstract{#1}}%see \@maketitle
\global\long\def\@textofabstract{{\tt Please  supply a concise
                                      abstract to fill this space!}
                                }

\def\endabstract{}  % to kill the previously defined environment


\endinput




