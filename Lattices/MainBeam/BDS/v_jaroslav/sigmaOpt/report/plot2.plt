#! gnuplot
reset

#set title "Luminosity dependence on Oct0"
#set xrange [-3000:3000]
#set yrange [4.5e34:4.6e34]
#set y2tics

set xlabel "Strength of magnet"
set ylabel "Luminosity [10^{34} cm^{-2} s^{-1}]"

# Luminosity specification
sLumi = 5.9/1.6
sLumiPeak = 2/1.6

# Before optimalization
bLumi = 4.44632 
bLumiPeak = 1.6273

reset


set terminal postscript enhanced eps color solid lw 2 "Times-Roman" 28
#set terminal x11
set output "magnetsOpt2.ps"
set size 2,2
set origin 0.1,0
set multiplot

# ksd0
set xlabel "Strength of magnet ksd0 [T/m^2]."
set ylabel "L [10^{32} cm^{-2} s^{-1}]"

set format x "%1.0f"
set format y "%1.1f"
set format y2 "%1.1f"

set size 1,1
set origin 0.1,0.0
#set xtics 5e-5
set y2tics 0.6
set ytics 1

# set title "ksd0"
plot [6.5:7.5] "../result/lumi_ksd0.out" u 2:($3*1e-34) w l t "", \
"../result/lumi_ksd0.out" u 2:($4*1e-34) w l axes x1y2 t ""#, \
#bLumi t "Before opt." , bLumiPeak axes x1y2 t "Before opt. peak"#, \
#sLumi t "Designed", sLumiPeak axes x1y2 t "Designed peak"



reset

# ksd4
set xlabel "Strength of magnet ksd4 [T/m^2]."
#set ylabel "L [10^{34} cm^{-2} s^{-1}]"

set format x "%1.0f"
set format y "%1.1f"
set format y2 "%1.1f"

set size 1,1
set origin 1.1,0
# set title "ksd4"
set y2tics .5
set ytics 1
#set xtics 1500
set key at 1000,4

plot[2.5:3.5] "../result/lumi_ksd4.out" u 2:($3*1e-34) w l t "../result/luminostity", \
"../result/lumi_ksd4.out" u 2:($4*1e-34) w l axes x1y2 t "../result/luminosity peak"#, \
#bLumi t "Before opt." , bLumiPeak axes x1y2 t "Before opt. peak"#, \
#sLumi t "Designed", sLumiPeak axes x1y2 t "Designed peak"

# ksf1
reset
set xlabel "Strength of magnet ksf1 [T/m^2]."
set ylabel "L [10^{34} cm^{-2} s^{-1}]"

set format x "%1.0f"
set format y "%1.1f"
set format y2 "%1.1f"

set size 1,1
set origin 0.1,1
# set title "ksf1"
set y2tics 0.5
set ytics 1



plot [-3:-1] "../result/lumi_ksf1.out" u 2:($3*1e-34)w l t "", \
"../result/lumi_ksf1.out" u 2:($4*1e-34) w l axes x1y2 t ""#, \
#bLumi t "Before opt." , bLumiPeak axes x1y2 t "Before opt. peak"#, \
#sLumi t "Designed", sLumiPeak axes x1y2 t "Designed peak"

# ksf5
reset
set xlabel "Strength of magnet ksf5 [T/m^2]."
set ylabel ""

set format x "%1.1f"
set format y "%1.1f"
set format y2 "%1.1f"

set size 1,1
set origin 1.1,1
# set title "ksf5"
#set xtics 0.5e-4
set ytics 1
#set xrange [1.4473:1.4475]
set y2tics 0.5

plot [-2:0] "../result/lumi_ksf5.out" u 2:($3*1e-34) w l t "", \
"../result/lumi_ksf5.out" u 2:($4*1e-34)w l axes x1y2 t ""#, \
#bLumi t "Before opt." , bLumiPeak axes x1y2 t "Before opt. peak"#, \
#sLumi t "Designed", sLumiPeak axes x1y2 t "Designed peak"



unset multiplot

