#! gnuplot
reset

set title "Luminosity dependence on Oct0"
#set xrange [-3000:3000]
#set yrange [4.5e34:4.6e34]
set y2tics

# Luminosity specification
sLumi = 5.9e34/1.6
sLumiPeak = 2e34/1.6

# Before optimalization
bLumi = 4.44632e+34 
bLumiPeak = 1.6273e+34

reset

#set terminal postscript enhanced color 16 eps
set terminal x11
#set output "magnetsOpt.ps"
set size 1, 1
#set multiplot 
# dec1
#set size 0.5,0.5
#set origin 0,0
#set xtics 2e-4
set xtics 0.5e-4
set xrange [1.4473:1.4475]
#set xrange [1.4473:1.4475]
set title "dec1"
plot "lumi_kdectest.out" u ($2*1e-7):($3*1e-34) w l t "Luminostity", \
"lumi_kdectest.out" u  ($2*1e-7):($4*1e-34) w l axes x1y2 t "Luminosity peak"#, \
#bLumi t "Before opt." , bLumiPeak axes x1y2 t "Before opt. peak"#, \
#sLumi t "Designed", sLumiPeak axes x1y2 t "Designed peak"


