#! gnuplot
reset

#set title "../result/luminosity dependence on Oct0"
#set xrange [-3000:3000]
#set yrange [4.5e34:4.6e34]
#set y2tics

set xlabel "magnet strength"
set ylabel "luminosity [10^{34} cm^{-2} s^{-1}]"

# Luminosity specification
sLumi = 5.9/1.6
sLumiPeak = 2/1.6

# Before optimalization
bLumi = 4.44632 
bLumiPeak = 1.6273

reset


set terminal postscript enhanced eps color solid lw 2 "Times-Roman" 14
#set terminal x11
set output "kocttestKdectest.ps"
set size 1,1
set origin 0,0



set xlabel "kocttest [T/m^3]" offset -1,-1 
set ylabel "kdectest [T/m^4]" offset 2.2,-1
set zlabel "L [10^{34} cm^{-2} s^{-1}]" offset -.9 rotate by 90

set format x "%1.0f"
set format y "%1.0f"
set format z "%1.1f"


set size 1,1
set origin 0.1,0.0
set xtics 1e3
set ytics 2e4
set ztics 0.5

set xtics offset -5
set ytics offset +3



set dgrid3d
set contour base
#set view map
#set contour
set key outside


splot[-2500:2000] "../result/lumi3d.out" u 3:($4*1e0):($5*1e-34) t "" with pm3d
 