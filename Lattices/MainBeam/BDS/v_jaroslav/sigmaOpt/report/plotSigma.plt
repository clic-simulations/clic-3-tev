#!/bin/bash
# Gnuplot recommends setting the size and origin before going to
# multiplot mode
# This sets up bounding boxes and may be required on some terminals
# create ps and png graph from sgx and sgy files located in $resultDir
# modified script from Guillermo
# Jaroslav Vazny, summer student 2010

reset
set terminal postscript enhanced eps color solid lw 2 "Times-Roman" 26
#set terminal x11
set output "resultsigma.ps"
#set title "Sigmax results "
set size 2,1
set multiplot


set size 0.95,0.95
set origin 0,0
set xlabel"Maximum order considered"
set ylabel"{/Symbol s}_x [nm] at IP "
set key r

p "../result/sgx_my" u 1:($3*1e9) axes x1y1  t "" w l,  "../result/sgx_original" u 1:($3*1e9) axes x1y1  t "" w l


set size 0.95,0.95
set origin 1,0
set xlabel"Maximum order considered"
set ylabel"{/Symbol s}_y [nm] at IP "
set key r
set key at 8,1
p "../result/sgy_my" u 1:($3*1e9) axes x1y1  t"optimalized" w l,"../result/sgy_original" u 1:($3*1e9) axes x1y1  t"original" w l 

# On some terminals, nothing gets plotted until this command is issued
unset multiplot

# remove all customization
#reset
