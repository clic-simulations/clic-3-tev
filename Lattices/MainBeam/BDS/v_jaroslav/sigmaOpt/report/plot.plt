#! gnuplot
reset

## set title "Luminosity dependence on Oct0"
#set xrange [-3000:3000]
#set yrange [4.5e34:4.6e34]
#set y2tics

set xlabel "magnet strength"
set ylabel "Luminosity [10^{34} cm^{-2} s^{-1}]"

# Luminosity specification
sLumi = 5.9/1.6
sLumiPeak = 2/1.6

# Before optimalization
bLumi = 4.44632 
bLumiPeak = 1.6273

reset


set terminal postscript enhanced eps color solid lw 2 "Times-Roman" 26
#set terminal x11
set output "magnetsOpt.ps"
set size 2,2
set origin 0.1,0
set multiplot

# dec1
set xlabel "Strength of magnet dec1 [T/m^4]."
set ylabel "L [10^{34} cm^{-2} s^{-1}]"

set format x "%1.5f"
set format y "%1.2f"
set format y2 "%1.2f"

set size 1,1
set origin 0.1,0.0
set xtics 5e-5
set y2tics 0.03
set ytics 0.003

# set title "dec1"
plot "../result/lumi_dec1.out" u ($2*1e-7):($3*1e-34) w l t "", \
"../result/lumi_dec1.out" u ($2*1e-7):($4*1e-34) w l axes x1y2 t ""#, \
#bLumi t "Before opt." , bLumiPeak axes x1y2 t "Before opt. peak"#, \
#sLumi t "Designed", sLumiPeak axes x1y2 t "Designed peak"



reset

# oct0
set xlabel "Strength of magnet oct0 [T/m^3]."
#set ylabel "L [10^{34} cm^{-2} s^{-1}]"

set format x "%1.0f"
set format y "%1.2f"
set format y2 "%1.2f"

set size 1,1
set origin 1.1,0
# set title "oct0"
set y2tics 0.1
set ytics 0.3
set xtics 1500
set key at 1700,4

plot "../result/lumi_oct0.out" u 2:($3*1e-34) w l t "Luminostity", \
"../result/lumi_oct0.out" u 2:($4*1e-34) w l axes x1y2 t "Luminosity peak"#, \
#bLumi t "Before opt." , bLumiPeak axes x1y2 t "Before opt. peak"#, \
#sLumi t "Designed", sLumiPeak axes x1y2 t "Designed peak"

# kocttest
reset
set xlabel "Strength of magnet kocttest [T/m^3]."
set ylabel "L [10^{34} cm^{-2} s^{-1}]"

set format x "%1.0f"
set format y "%1.2f"
set format y2 "%1.2f"

set size 1,1
set origin 0.1,1
# set title "kocttest"
set y2tics 0.03
set ytics 0.05



plot "../result/lumi_kocttest.out" u ($2*1e0):($3*1e-34) w l t "", \
"../result/lumi_kocttest.out" u ($2*1e0):($4*1e-34) w l axes x1y2 t ""#, \
#bLumi t "Before opt." , bLumiPeak axes x1y2 t "Before opt. peak"#, \
#sLumi t "Designed", sLumiPeak axes x1y2 t "Designed peak"

# kdectest

set xlabel "Strength of magnet kdectest [T/m^4]."
set ylabel ""

set format x "%1.4f"
set format y "%1.2f"
set format y2 "%1.2f"

set size 1,1
set origin 1.1,1
# # set title "kdectest"
set xtics 0.5e-4
set ytics 0.005
set xrange [1.4473:1.4475]
set y2tics

plot "../result/lumi_kdectest.out" u ($2*1e-7):($3*1e-34) w l t "", \
"../result/lumi_kdectest.out" u ($2*1e-7):($4*1e-34) w l axes x1y2 t ""#, \
#bLumi t "Before opt." , bLumiPeak axes x1y2 t "Before opt. peak"#, \
#sLumi t "Designed", sLumiPeak axes x1y2 t "Designed peak"



unset multiplot

