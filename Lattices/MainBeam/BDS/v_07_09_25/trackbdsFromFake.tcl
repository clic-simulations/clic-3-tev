set e_initial 1496
set e0 $e_initial
#set script_dir /local/PLACET/CLIC_example/
set script_dir /afs/cern.ch/user/r/rtomas/w1/PLACET/CLIC_example

set synrad 1
set quad_synrad 1
set mult_synrad 1
set sbend_synrad 1

set scale 1.0
source $script_dir/clic_basic_single.tcl

set kod1 0
set kod2 0
set kod21 0
set kod3 0
set kod4 0

#source ../CreatPLACEtinputs/ff.tcl.Nominal
source bds.placet

proc save_beam {} {
    if {[EmittanceRun]} {
	BeamDump -file particles.out
    }
}

TclCall -script save_beam

BeamlineSet -name test




array set match {
alpha_x 0
alpha_y 0
beta_x  66.14532014 
beta_y  17.92472388
}

set match(emitt_x)   6.8
set match(emitt_y)     0.2
set match(charge) 4e9
set charge $match(charge)
set match(sigma_z) 44.0
set match(phase) 0.0
set match(e_spread) -1.0

set n_slice 21
set n 7
set n_total 10000
source $script_dir/clic_beam.tcl


#make_beam_halo $e0 $match(e_spread) $n_total
make_beam_particles $e0 $match(e_spread) $n_total


make_beam_many beam0 20 500 
BeamRead -file particles.in -beam beam0

FirstOrder 1
TestNoCorrection -beam beam0 -emitt_file emitt.dat -survey Zero

#TwissPlot -beam beam0 -file twiss.dat
#set twiss [TwissPlot -beam beam0]



#Octave {

#     [E,Beam]=placet_test_no_correction("electron", "beam0", "Zero");
#    save -text emitt E
			
#	      }









source $script_dir/clic_guinea.tcl
set gp_param(n_x) 128
set gp_param(cut_x) 400.0
set gp_param(n_y) 256
set gp_param(cut_y) 15.0
set gp_param(sigmaz) $match(sigma_z)
set gp_param(particles) [expr 1e-10*$charge]
write_guinea_offset_angle 0.0 0.0
exec cp particles.out electron.ini
exec cp particles.out positron.ini

exec guinea default default default

exec  grep lumi_ee= default > lumi

exec  grep lumi_ee_high= default > lumi_high


exit
