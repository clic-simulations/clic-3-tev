#!/usr/bin/env pythonafs

import math
import copy
import sys
from os import system
from simplex import Simplex
from mapclass import *


betx=66.14532014
bety=17.92472388
gamma=3e6
ex=68e-8
ey=2e-8
sigmaFFS=[sqrt(ex*betx/gamma), sqrt(ex/betx/gamma), sqrt(ey*bety/gamma), sqrt(ey/bety/gamma), 0.01]

file='fort.18'
map=Map(1,file)
print "sigmax=",sqrt(map.sigma('x',sigmaFFS)),";"
print "sigmay=",sqrt(map.sigma('y',sigmaFFS)),";"
print "sigmapx=",sqrt(map.sigma('px',sigmaFFS)),";"
print "sigmapy=",sqrt(map.sigma('py',sigmaFFS)),";"
map=Map(2,file)
print "sigmax=",sqrt(map.sigma('x',sigmaFFS)),";"
print "sigmay=",sqrt(map.sigma('y',sigmaFFS)),";"
print "sigmapx=",sqrt(map.sigma('px',sigmaFFS)),";"
print "sigmapy=",sqrt(map.sigma('py',sigmaFFS)),";"
map=Map(3,file)
print "sigmax=",sqrt(map.sigma('x',sigmaFFS)),";"
print "sigmay=",sqrt(map.sigma('y',sigmaFFS)),";"
print "sigmapx=",sqrt(map.sigma('px',sigmaFFS)),";"
print "sigmapy=",sqrt(map.sigma('py',sigmaFFS)),";"
map=Map(4,file)
print "sigmax=",sqrt(map.sigma('x',sigmaFFS)),";"
print "sigmay=",sqrt(map.sigma('y',sigmaFFS)),";"
print "sigmapx=",sqrt(map.sigma('px',sigmaFFS)),";"
print "sigmapy=",sqrt(map.sigma('py',sigmaFFS)),";"
map=Map(5,file)
print "sigmax=",sqrt(map.sigma('x',sigmaFFS)),";"
print "sigmay=",sqrt(map.sigma('y',sigmaFFS)),";"
print "sigmapx=",sqrt(map.sigma('px',sigmaFFS)),";"
print "sigmapy=",sqrt(map.sigma('py',sigmaFFS)),";"

