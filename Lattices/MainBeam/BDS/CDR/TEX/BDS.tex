%\documentclass[twoside,hyperef]{cernmono}

\documentclass{cernrep} 
\usepackage{texnames,color}
\usepackage[T1]{fontenc}%

%\usepackage{draftwatermark,color,ulem}
%\usepackage{clic}
%\usepackage[pdftex,colorlinks,bookmarksnumbered,linkcolor=blue]{hyperref}
%\usepackage{epic,eepic,pspicture,mathptmx,times,graphpap,epsf,titleref}
\pagestyle{empty}

\pagestyle{plain}
\pagenumbering{roman}
\cleardoublepage
\renewcommand{\bibname}{Bibliography}
%\pagestyle{fancy}

\pagenumbering{arabic}
\renewcommand{\floatpagefraction}{0.9}
\usepackage{hyperref}
%\SetWatermarkText{Draft}
 

\begin{document}
\title{Beam Delivery Systems}
 
\author{P.~Ambatu$^3$, {\bf D.~Angal-Kalinin}$^1$, J.~Barranco$^4$, G.~Blair$^2$, G.~Burt$^3$, H.~Burkhardt$^4$, B.~Dalena$^4$, L.~Deacon$^2$, A.~Dexter$^3$, H.M. Durand$^4$, J.L.~Fernandez~Hernando$^1$, L.~Gatignon$^4$, F.~Jackson$^1$, J.~Jones$^1$
E.~Mar\'in$^4$, A.~Latina$^4$, J.~Resta L\'opez$^2$, G.~Rumolo$^4$, J.~Snuverink$^4$, P.~Schuler$^5$, D.~Schulte$^4$, {\bf A.~Seryi}$^2$, {\bf R.~Tom\'as}$^4$, V.~Vlachoudis$^4$, R.~Veness$^4$ and G.~Zamudio$^4$}

\institute{ $^1$Cockroft Institute, $^2$John Adams Institute, $^3$Lancaster University, $^4$CERN,  $^5$DESY}
\maketitle % this produces the title block

\setcounter{section}{3}
\setcounter{subsection}{4}

\subsection{Beam Delivery Systems}\label{BDS}




\subsubsection{Overview}\label{BDS:Overview}

The CLIC Beam Delivery System (BDS) is responsible for transporting the e$^+$/e$^-$ beams
from the exit of the high energy linacs, focusing them to the sizes required to meet the CLIC
luminosity goals ($\sigma_x$~=~45~nm, $\sigma_y$~=~1~nm in the nominal parameters) and
bringing them into collision. In addition, the
BDS must perform several critical functions:
\begin{itemize}
\item[1.] Measure the linac beam and match it into the final focus.
\item[2.] Protect the beamline and detector against mis-steered beams from the main linacs.
\item[3.] Remove any large amplitude particles (beam-halo) from the linac to minimize background
in the detectors.
\item[4.] Measure and monitor the key physics parameters such as energy and polarization.
\end{itemize}
Functions 2. and 3. are accomplished by the collimators. Therefore the first collimator 
needs to survive to the impact of any mis-steered CLIC bunch train. This condition requires
large beam sizes at the first collimator, hence driving the length of the system.
The BDS must provide sufficient instrumentation, diagnostics and feedback systems to achieve
these goals. All the CLIC BDS lattices can be found in~\cite{CLICr}. 

\subsubsection{Beam parameters}\label{BDS:parameters}
Table~\ref{BDS:parameters:table} shows the key BDS parameters for the nominal 
configuration at 3~TeV CM.

\begin{table}
\caption{Key parameters of the BDS. The range of L$^*$, the distance from the final quadrupole to the IP, corresponds
to values considered for different detector and FFS concepts. 
%Effective values are given for the IP parameters.
}\label{BDS:parameters:table}
\begin{center}
\begin{tabular}{|l|c|c|}\hline
Parameter & Units & Value \\\hline\hline
Length (linac exit to IP distance)/side & m   & 2750 \\\hline
%Length of main (tune-up) extraction line& m   & 300 (467) \\\hline
Maximum Energy/beam     & TeV & 1.5 \\\hline
Distance from IP to first quad, L* & m & 3.5-6 \\\hline
Crossing angle at the IP &mrad& 20   \\\hline
Nominal core beam size at IP, $\sigma^*$, x/y & nm & 45/1  \\\hline
Nominal beam divergence at IP, $\theta^*$, x/y& $\mu$rad & 7.7/10.3  \\\hline
Nominal beta-function at IP, $\beta^*$, x/y & mm & 10/0.07  \\\hline
Nominal bunch length, $\sigma_z$ & $\mu$m & 44  \\\hline
Nominal disruption parameters, x/y &&   0.15/8.4   \\\hline
Nominal bunch population, N & & $3.7\times10^{9}$   \\\hline
Beam power in each beam &MW& 14  \\\hline
Preferred entrance train to train jitter & $\sigma$ & < 0.2  \\\hline
Preferred entrance bunch to bunch jitter & $\sigma$& < 0.05  \\\hline
Typical nominal collimation aperture, x/y & $\sigma_x/\sigma_y$& 15/55  \\\hline
Vacuum pressure level, near/far from IP &nTorr& 1000/1  \\\hline
\end{tabular}
\end{center}
\end{table}



\subsubsection{Subsystems}\label{BDS:Systems}
The main subsystems of the beam delivery starting from the exit of the main linacs are
the diagnostics region, the energy and betatron
collimation and the final focus. The layout of the
beam delivery system is shown in Fig.~\ref{3TeVlayout}.


\begin{figure}\vspace{-0.cm}
\begin{center}
\includegraphics[width=5.6cm, angle=-90]{layout3TeV.eps}
\end{center}\vspace{-0.cm}
\caption{CLIC 3~TeV layout. Dipoles, quadrupoles and collimators are shown in blue, red and black, respectively.
 The tune-up dump and its extraction line are also displayed.}\label{3TeVlayout}
\end{figure}



There is a single collision point with a 20 mrad crossing angle. 
The 20 mrad geometry provides space for separated spent beam lines and
requires crab cavities to rotate the bunches in the horizontal plane 
for head-on collisions. There are
two detectors in a common IR cavern complex (plus two garage caverns) 
which alternately occupy the single collision point, in a
so-called "push-pull" configuration, see Section~5.10.2.5 for more details. 

\subsubsubsection{Diagnostics}\label{BDS:Diagnostics}
The initial part of the BDS, from the end of the main linac to the start of the collimation system, is responsible for
measuring and correcting the properties of the beam before it enters the Collimation and Final Focus system.
The optics and the layout of the diagnostics section is shown in Fig.~\ref{diagnostics}. 
Starting at the exit of the main linac, the system includes the
skew correction section, emittance diagnostic section
and beta matching section.
The skew correction section contains 4 orthonormal skew
quadrupoles which provide complete and independent control of the 4 betatron coupling
parameters. This scheme allows correction of any arbitrary linearized coupled beam.



\begin{figure}\vspace{-0.cm}
\begin{center}
\includegraphics[width=6.8cm, angle=-90]{diagnostics3TeV.eps}
\end{center}\vspace{-0.cm}
\caption{Optics (top) and layout (bottom) of the CLIC diagnostics and energy collimation sections.}\label{diagnostics}
\end{figure}


The emittance diagnostic section contains 4 laser
wires which are capable of measuring horizontal and vertical RMS beam sizes down to 1~$\mu$m.
The wire scanners are separated by 45$^{\circ}$ in betatron phase to allow a complete measurement
of 2D transverse phase space and determination of the projected horizontal and vertical emittances.


The energy measurement has been designed to minimize the required space due
to the tight constraints on the CLIC total length. The deflection of the first dipole in the energy collimation 
section together with high precision BPM pairs at both sides of the dipole 
provides the most compact energy measurement. 
The integrated magnetic field is assumed to have a calibration error of
0.01\% and the BPM resolution must be 50~nm or better with a maximum calibration error of 0.1\%. 
This set-up provides a relative energy resolution below 0.1\%. 
Reference trajectories can be regularly established by zeroing the magnetic
field and safely disposing of the beam in the tune-up dump.



The BDS is equipped with a  polarization measurement station~\cite{schuler} in the energy collimation section. 
Figure~\ref{diagnostics} shows the location of the polarization laser IP.
At this location the beam travels  parallel to the beam direction at the e$^-$-e$^+$ IP and there is enough free space for the polarization laser.
The back scattered electrons (or positrons) will deviate from the 
main beam trajectory thanks to the bending dipoles. These lower energy particles
can be collected in a detector right before the energy spoiler. Excursions in the order
of 100~mm are expected for particles loosing about 95\% of the energy.
With current existing laser technology the polarization measurement
achieves a resolution better than  0.1\% if averaging over 60~seconds.
The systematic errors of the set-up will be analyzed in the forthcoming 
technical design phase.




\subsubsubsection{Tune-up extraction lines \& dumps}\label{BDS:tune-up}
During the commissioning of the main linacs 
 the presence of the beam is not desired in the collimation, final focus, or IR areas.
An extraction line right before the energy collimation allows to guide the
beam to a full-beam power water-filled dump.

 
\subsubsubsection{Collimation}\label{BDS:Collimation}
The CLIC collimation section fulfills two critical functions. (1) It protects the down-stream beam line and detector against mis-steered beams from the main linac and (2) removes the beam halo.
The most likely scenario for having mis-steered beams in the BDS
is the failure of some component of the accelerating RF in the 20~km linac,
resulting in a lower beam energy.
Therefore placing the energy collimation before the betatron collimation
guarantees the most efficient absorption of the errant beams.
The energy spoiler is designed to survive the impact of a full bunch
train. The spoiler survives medium depth impacts up to 2~mm~\cite{vasilis}.  
However recent simulations show that deep impacts may damage the
energy spoiler~\cite{Javier}. 
This is due to the increase in the collimator length for deeper impacts.
A solution would be to consider a hollow collimator. 
This should be investigated during the technical phase together with 
failure modes in the LINAC as the deep impacts might 
turn out improbable or associated to large emittance dilution.  


The transverse collimators, made of Ti, are sacrificial or consumable.
A collimator absorber is placed downstream the spoiler as shown
in Fig.~\ref{ffs} to stop the particles scattered 
at the spoiler. The full description of the BDS spoilers and absorbers is given in Table~\ref{colltab}. 


First activation and damage studies have been carried~\cite{vasilis} out for the energy collimation area
assuming that a fraction of 10$^{-6}$ beam particles hit the energy spoiler
and that CLIC operates 200 days per year. Activation levels after one year of operation
have been found acceptable for the energy spoiler and the elements downstream. 
The Displacement Per Atom (DPA) of the energy spoiler after one year of operation is in the 10$^{-6}$
level, representing a negligible damage.

 
%\begin{tabular}{|c|c|c|c|c|c|c|c|c|c|c|}
%\hline 
% Name & $\beta_{x}$ & $\beta_{y}$ & $D_x$ & $a_x$ & $a_y$ & $\theta_{\rm T}$ & L$_{\rm F}$ & L$_{\rm T}$  &  Material  \\
%  & [m] & [m] & [m] & [mm] & [mm] & [mrad] &[$X_0$] & [mm] & \\\hline \hline
%EYSP & 1406 & 70681 & 0.27 & 3.51 & 8 & 50 &0.05& 90 &Be \\
%EYAB & 3213 & 39271 & 0.42 & 5.41 & 8 &  & 20 & &Ti \\\hline
%\multicolumn{10}{|c|}{the following $\times$4}\\\hline
%YSP1 & 114 & 483 & 0 & 8 & 0.1 & 88 &0.2& 90 &Be? \\
%XSP1 & 270 & 101 & 0 & 0.12 & 8& 88  &0.2& 90 &Be? \\
%XAB1 & 270 & 81 & 0 & 1 & 1  &  & 20 & &Ti \\
%YAB1 & 114 & 483 & 0 & 1 & 1  &  & 20 & &Ti \\
%\hline 
%\end{tabular}
%\end{center}
%\caption{Optics parameters and geometry of the BDS spoilers and collimators. The geometry parameters are
%defined in Fig.~\ref{collgeom}.}\label{colltab}
%\end{table}






\begin{table}
\caption{Geometry of the BDS spoilers and absorbers.
The radiation lengths for Be and Ti are $X_0$=0.353~m and $X_0$=0.036~m, respectively.
The material (Ti-Cu) of the transverse spoilers (YSP and XSP) is Ti with a Cu coating.
}\label{colltab}
\begin{center}
\raisebox{-1cm}{\includegraphics[width=7.1cm, angle=-0]{collgeometry.eps}\hspace{-0.45cm}}
\begin{tabular}{|c|c|c|c|c|c|c|c|}
\hline 
 Name & a$_x$ & a$_y$ & $\theta_{\rm T}$ & L$_{\rm F}$ & L$_{\rm T}$  &  Mat.  \\
  & [mm] & [mm] & [mrad] &[$X_0$] & [mm] & \\\hline \hline
ESP & 3.51 & 8 & 50 &0.05& 90 &Be \\
EAB & 5.41 & 8 & 100   & 18 & 27 &Ti \\\hline
\multicolumn{7}{|c|}{the following $\times$4}\\\hline
YSP & 8 & 0.1 & 88 &0.2& 90 &  Ti-Cu \\
XSP & 0.12 & 8& 88  &0.2& 90 & Ti-Cu \\
XAB & 1 & 1  & 250  & 18 & 27 &Ti \\
YAB & 1 & 1  & 250  & 18 & 27 &Ti \\
\hline 
\end{tabular}
\end{center}

\end{table}



%\begin{figure}\vspace{-0.cm}
%\begin{center}
%\includegraphics[width=6.8cm, angle=-0]{collgeometry.eps}
%\end{center}\vspace{-0.cm}
%\caption{Parametrization of the BDS spoiler and absorber geometry.}\label{collgeom}
%\end{figure}


Particles in the beam halo produce backgrounds in the detector and must be removed in the
BDS collimation system. One of the design requirements for the CLIC BDS is that no particles
are lost in the last several hundred meters of beamline before the IP. Another requirement
is that all synchrotron radiation passes cleanly through the IP to the extraction line. The
BDS collimation must remove any particles in the beam halo which do not satisfy these
criteria. These requirements define a system where the collimators have very narrow gaps
and the system is designed to address the resulting machine protection, survivability and
beam emittance dilution issues.
The betatron collimation
system has four spoiler/absorber x/y pairs located as displayed in Fig.~\ref{ffs}.  These provide
collimation at each of the final doublet (FD) and IP betatron phases.
All spoilers and absorbers have adjustable gaps. 



\begin{figure}\vspace{-0.cm}
\begin{center}
\includegraphics[width=6.8cm, angle=-90]{ffs3TeV.eps}
\end{center}\vspace{-0.cm}
\caption{Optics (top) and layout (bottom) of the CLIC betatron collimation and final focus sections.}\label{ffs}
\end{figure}



There is a small probability (of the order of some $10^{-4}$) that high energy secondary muons are produced in the collimation of the halo particles which may reach the experimental cavern and detector.
This was studied by detailed tracking using PLACET~\cite{PLACET} with HTGEN~\cite{EUROTeV-Report-2008-076} for the halo modeling and BDSIM~\cite{Agapov2009708} for the study of muon production and tracking towards the detector.

The simulated muon tracks are used as input to detector simulations. It was found, that it will be important for the detector performance that the background muon rates from the machine are kept at a low level, aiming for not more than 5~muon per bunch crossing on average, integrated for both beams and over the cross section of the detector~\cite{Lucie}.

The simulations of halo particles are based on beam-gas scattering as the primary halo source. Under favorable beam and design vacuum conditions with residual gas pressure at the 0.1-1~nTorr level both in the LINAC and the BDS, we expected that only a very small fraction, of the order of $10^{-6}$-$10^{-5}$ of the beam particles would be lost at the collimators resulting into a muon flux which would not exceed the level of 1~muon per bunch crossing.






%Under favorable beam and vacuum conditions, we can expect muon fluxes of the order of $10^{4}$ per train into the detector region. For more conservative estimates, we assume that the collimation system would be hit by a fraction of $10^{-3}$ of the beam particles resulting in one to two orders of magnitude higher muons rates~\cite{Burkhardt:2010zz}



\subsubsubsection{Muon suppression}\label{BDS:Muon}

The actual fraction of halo particles in a realistic machine with imperfections is hard to predict and could be much higher than simulated by beam-gas scattering for the ideal machine.
For more conservative estimates, we assume that the collimation system would be hit by a fraction of $10^{-3}$ of the beam particles resulting in one to two orders of magnitude higher muons rates~\cite{Burkhardt:2010zz}  than targeted by the detector studies.

We have looked into the possibility to reduce the muon flux into the detector region using cylindrical magnetized iron shielding with an outer radius of 55 cm around the beam pipe at about 100~m downstream of the spoilers.
Based on our current tracking studies using BDSIM, we expect that a factor of ten reduction of the muon flux would require a 80~m long shielding. More detailed simulations are ongoing. It will be essential to reserve the space in the drift spaces of the BDS for the muon shield as shown in~Fig.~\ref{ffs}. The actual installation of the muon shield could be done in stages, as required by the actual beam conditions.

Other muon suppression systems might also be considered during the technical phase
as introducing absorbers downstream the collimators inside the beam pipe.






\subsubsubsection{Final Focus}\label{BDS:FFS}
The role of the Final Focus System (FFS) is to demagnify the beam to the required size ($\sigma_x=$45~nm
and $\sigma_y=$1~nm) at the IP. The FFS optics creates a large and almost parallel
beam at the entrance to the Final Doublet (FD) of strong quadrupoles. Since particles of
different energies have different focal points, even a relatively small energy spread of 0.1\%
significantly dilutes the beam size, unless adequate corrections are applied. The design of
the FFS is thus mainly driven by the need to cancel the chromaticity of the FD. The CLIC FFS has a baseline local chromaticity correction~\cite{Raimondi} using sextupoles next to the final doublets.
A bend upstream generates dispersion across the FD, which is required for the sextupoles and non-linear elements to
cancel the chromaticity. The dispersion at the IP is zero and the angular dispersion is about 1.4~mrad, i.e. small enough that it does not significantly increase the beam divergence. Half
of the total horizontal chromaticity of the final focus is generated upstream of the bend
in order for the sextupoles to simultaneously cancel the chromaticity and the second-order dispersion.
The horizontal and the vertical sextupoles are interleaved in this design, so they generate
third-order geometric aberrations. Additional sextupoles upstream and in proper phases
with the FD sextupoles partially cancel these third order aberrations. The residual higher order
aberrations are further minimized with octupoles and decapoles~\cite{Tomas}. The final focus optics is shown in Fig.~\ref{ffs}.






\subsubsubsection{Crab Cavity}\label{BDS:CrabCavity}
With a 20 mrad crossing angle, crab cavities are required to rotate the bunches so they collide
head on. They apply a z-dependent horizontal deflection to the bunch that zeroes at the center of the bunch. 
The crab cavity is located prior to the FD as shown in Fig.~\ref{ffs} but sufficiently
close to be at 90$^{\circ}$ degrees phase advance from the IP.

%{\color{red} 5\% loss with solution} 


\subsubsubsection{Alternative L$^*$}\label{BDS:Lstar}
In the nominal configuration with L*=3.5~m the last quadrupole
of the FD, QD0, sits inside the detector, see Section~2.6.1 for 
detailed illustrations. 
In order to alleviate 
the engineering and the stabilization of this set-up it has
been proposed, as a possible fallback solution, 
to move QD0 from the detector to the tunnel,
consequently increasing L$^*$. A collection of FF systems with
L$^*$ values between 3.5 and 8~m has been studied for CLIC. The performance
of these FFS is shown in Table~\ref{l*}. Both the total luminosity
and the luminosity in the energy peak degrade as the L$^*$ increases.
Only the cases with L$^*$ of 3.5 and 4.3~m meet the CLIC requirements 
with a 20\% margin for static and dynamic imperfections. 
The shortest L$^*$ that allows removing QD0 from the detector is  
6~m. The FFS with L$^*$=6~m meets the CLIC requirements with a tight
margin of 5\% for the imperfections~\cite{Guillermo}. The last case with L$^*$=8~m does 
not meet the CLIC requisites. 


\begin{table}
%\begin{minipage}[b]{0.5\linewidth}\centering

\caption{Total luminosity and luminosity in the 1\% energy peak
 for the various L* under consideration.}\label{l*}
\begin{center}
\begin{tabular}{|c|c|c|}\hline
L* & total luminosity & peak luminosity \\ \
 [m] & [$10^{34}$cm$^{-2}$s$^{-1}$] & [$10^{34}$cm$^{-2}$s$^{-1}$] \\\hline\hline
3.5  &  6.9 & {2.5}\\\hline
4.3  &  6.4 & {2.4}  \\\hline
6    &  5.0  & {2.1} \\\hline
8    &  4.0 & {1.7}  \\\hline
\end{tabular}
\end{center}
\end{table}
%\end{center}
%\end{minipage}
%\begin{minipage}[b]{0.5\linewidth}
%\centering
%\caption{Tuning performance assuming the prealignment static errors
%in the second column for the various L* under consideration. Relative success stands for the probability of reaching 80\% of the        maximum luminosity of that FFS. Absolute success stands for the probability of reaching 80\% of the CLIC design luminosity.             }\label{tuning}
%\begin{tabular}{|c|c|c|c|}\hline
%   &               &  relative & absolute \\
%L* & prealignment  & success & success  \\  \
% [m] &    [$\mu$m]  & [\%] & [\%]  \\\hline\hline
%3.5  & 10& 65 & {87}\\\hline
%4.3  & 10& 80 & {100}  \\\hline
%6    & {8} & 80 & {90} \\\hline
%8    & {2} & 80  & {46}  \\\hline
%\end{tabular}




%\end{minipage}

%\end{table}


\subsubsection{Accelerator physics issues}\label{BDS:issues}

\subsubsubsection{Synchrotron radiation and the detector solenoid}
Synchrotron radiation from all the BDS magnets causes a 22\% luminosity loss~\cite{Dalena}.
About 10\% comes from the FFS bending magnets and another 10\% originates at 
the FD quadrupoles. The CLIC vertical IP beta function is slightly below
the theoretical beta function that minimizes the Oide effect~\cite{Oide,CLIC08}.
These numbers do not take into account the effect of the detector solenoid as
this strongly depends on the final configuration of the IR.
Simulations in~\cite{barbSol} show that the luminosity loss due to the
solenoid ranges between 3\% and 25\%.  The right  adjustments of the length of
the antisolenoid, the L$^*$, the detector solenoid field and the crossing angle
should be explored to minimize this luminosity loss.


\subsubsubsection{Crab Cavity effects}
A proper simulation of the current crab cavity scheme
has shown a 5\% luminosity loss from ideal head-on collisions
without crab cavities~\cite{ian}. 
The source of this luminosity
loss is identified as the aberrations caused by the crabbed beam at 
the sextupoles downstream of the crab cavity.
Various solutions to avoid this luminosity loss  have already been
found as~\cite{JB}: (i) compensation with an extra crab cavity~\cite{AS}, (ii) change the location of the crab cavity, (iii) use the opposite beam crossing
scheme with opposite crab cavity voltage.



\subsubsubsection{Beam pipe aperture}
The aperture of the CLIC BDS beam pipe must be large enough
to contain the beam (14$\sigma_x$ and 55$\sigma_y$) and to avoid
the effects from the resistive wall wakefield~\cite{RaphaelMuntzner}.
It also must be small enough to guarantee the feasibility 
of the BDS magnets. In~\cite{RaphaelMuntzner} it is computed
that a reference beam pipe radius of 8~mm is acceptable
in terms of resistive wall effects. A larger aperture is used 
where the beam requires more space and a smaller aperture is used
where the magnet feasibility is challenged. A round aperture is assumed all along
the BDS, see Fig.~\ref{aper}.
%
\begin{figure}\vspace{-0.cm}
\begin{center}
\includegraphics[width=6.cm, angle=-90]{aper3TeV.eps}
\end{center}\vspace{-0.cm}
\caption{Beam pipe aperture radius along the BDS.}\label{aper}
\end{figure}


\subsubsubsection{Collimators wakefields}
Betatron spoilers are the main source of  emittance growth
due to wakefields in the BDS.
The effects of the
collimator wakefields on the luminosity have been evaluated for the design transverse
collimation apertures 15$\sigma_x$ and 55$\sigma_y$ and the materials as
given in Table~\ref{colltab}. Figure~\ref{collwakes} compares the relative luminosity
degradation as a function of initial vertical position offsets at the entrance of the BDS
with and without collimator wakefields. In this calculation the effect of all the BDS
collimators has been considered. For instance, for beam offsets of $\pm$0.4$\sigma_y$, the CLIC
luminosity loss was found to amount up to 20\% with collimator wakefields, and up to
10\% for the case with no wakefield effects.
%
\begin{figure}\vspace{-0.cm}
\begin{center}
\includegraphics[width=10.cm, angle=-0]{collwakes.eps}
\end{center}\vspace{-0.cm}
\caption{Relative CLIC luminosity versus initial beam offsets for the cases with
and without collimator wakefield effects.}\label{collwakes}
\end{figure}



\subsubsubsection{FFS tuning}
The biggest challenge faced by the BDS is the demonstration
of the  performance assuming realistic static and dynamic
imperfections. The diagnostics and the collimation sections
have demonstrated to be robust against misalignments 
(prealignment of 10~$\mu$m over 500~m as discussed in~\ref{BDS:Alignment}).
Standard orbit correction techniques, as the dispersion
free steering, guarantee the beam transport without 
blow-up in these regions. 
However these techniques fail in the FFS. 
The CLIC FFS is a very non-linear system with a pushed $\beta_y^*$
down to 0.07~mm. Many different approaches have been investigated
to tune the FFS in presence of realistic misalignments.
Currently the two most successful approaches follow:
\begin{itemize}
\item {\bf Luminosity optimization}: Maximizes the luminosity
      using all the available parameters in the FFS applying the Simplex algorithm.
\item {\bf Orthogonal knobs}: Maximizes the luminosity by scanning pre-computed
     arrangements of sextupole displacements (knobs) which target the IP beam
     correlations in an orthogonal fashion.
\end{itemize}



These approaches are simulated for 100 statistical realizations of the
CLIC FFS with misalignments. 
The final luminosity distribution and the number of iterations are shown in Fig.~\ref{tuning} for these two approaches in black and blue.
The number of iterations corresponds to the number of luminosity measurements.
A random error  up to 3\% has been assumed for the luminosity measurement.
Neither the Simplex approach, nor the orthogonal knobs reach a satisfactory
result in terms of luminosity. However being the orthogonal knobs
much faster it is possible to apply it after the  Simplex approach.
This corresponds to the magenta curves in Fig.~\ref{tuning}, showing
90\% probabily of reaching 90\% of the design luminosity and requiring 
a maximum of 18000 iterations. The achieved luminosity performance is close
enough to
the desired 90\% probability of reaching 110\% of the design luminosity
since new approaches or extensions will further improve the final luminosity,
e.g., non-linear knobs.
To convert the number of iterations into time it is required to know
how long a luminosity measurement will take. A conventional measurement
of luminosity takes between 7 and 70~minutes~\cite{lumimeas}, however faster 
indicators exist as different combinations of beamstrahlung 
signals~\cite{Eliasson} and hadronic events~\cite{BarbaLumi}.
These studies suggest that less than 10 bunch crossings should
be enough to obtain accurate signals for tuning.
Therefore 18000 iterations would take about an hour, which is 
reasonable for tuning the BDS from scratch.
 

\begin{figure}\vspace{-0.cm}
\begin{center}
\includegraphics[width=13.5cm, angle=0]{bds_tuning_3res_out.eps}\\
\includegraphics[width=13.5cm, angle=0]{bds_tuning_3res_iter.eps}
\end{center}\vspace{-0.cm}
\caption{Top: Luminosity performance for 100 statistical realizations of the CLIC FFS  after tuning using 3 different approaches. Bottom: Required number of luminosity measurements for the 3 different approaches.}\label{tuning}
\end{figure}


During the CLIC technical design phase special focus needs 
to be put in improved tuning algorithms taking into
account realistic errors in all BDS elements (e.g. the solenoid and
the crab cavity were excluded in this study). The e$^-$ and e$^+$ BDS lines
should be optimized simultaneously and more robust FFS designs could
be considered.



%\begin{table}
%\caption{Tuning performance assuming the prealignment static errors
%in the second column for the various L* under consideration. Relative success stands for the probability of reaching 80\% of the        maximum luminosity of that FFS. Absolute success stands for the probability of reaching 80\% of the CLIC design luminosity.             }\label{tuning}
%\begin{center}\vspace{-0cm}
%\begin{tabular}{|c|c|c|c|}\hline
%   &               &  relative & absolute \\
%L* & prealignment  & success & success  \\  \
% [m] &    [$\mu$m]  & [\%] & [\%]  \\\hline\hline
%3.5  & 10& 52 & {80}\\\hline
%4.3  & 10& 80 & {100}  \\\hline
%6    & {8} & 80 & {90} \\\hline
%8    & {2} & 80  & {46}  \\\hline
%\end{tabular}
%\end{center}
%\end{table}





\subsubsection{Component specifications}\label{BDS:components}

\subsubsubsection{Magnets}\label{BDS:magnets}

The CLIC BDS consists of 206~dipoles occupying 1.3~km,    
70~quadrupoles occupying  190~m and 18~sextupoles over 34~m.
Due to a lack of resources some BDS magnet lack an engineering design  
but no technical obstacle is observed a priori. Magnetic designs will be made during the next 
CLIC design phase.

The dipoles feature magnetic fields between 20 and 120~Gauss with
a field relative precision and jitter better than $10^{-4}$.
The sextupolar relative field error in the dipoles at 10~mm must be below $6\times10^{-4}$.
The baseline approach is to use normal conducting dipoles, however
superconducting dipoles have the advantage of naturally  shielding
stray fields.

The most challenging quadrupole in the BDS is the final quadrupole QD0.
Its specifications are given for the different L$^*$ FFS options in Table~\ref{qd0}.
The technical description of QD0 is given in Section~5.10.2.1.
\begin{table}
\begin{center}
\caption{Specifications of the FD QD0 quadrupole for the different L$^*$ cases.}\label{qd0}
\begin{tabular}{|l|c|r|r|r|r|}\hline
L$^*$        & m    & 3.5    & 4.3 & 6.0  &8.0    \\\hline\hline
Gradient  & T/m  & 575    & 382 & 200  & 211    \\\hline
Length    & m    & 2.7    & 3.3 & 4.7  & 4.2     \\\hline
Beam aperture  & mm   & 3.8    & 6.7 & 8 &   8.5  \\\hline
Jitter tolerance& nm & 0.15    & 0.15 & 0.2  &0.18 \\\hline
Gradient tol& $10^{-6}$  & 5 & 5 & - &3  \\\hline
Octupolar error  &10$^{-4}$@1mm & 7 & 7 & - & 3 \\\hline
Prealignment & $\mu$m & 10 & 10 & 8 &2  \\\hline
\end{tabular}
\end{center}
\end{table}

An antisolenoid is required to shield QD0 from the detector magnetic
field~\cite{antisolenoid} and to avoid the beam emittance blow-up.
A more detailed view of the antisolenoid is given in Section~2.6.3.2.

\subsubsubsection{Instrumentation}\label{BDS:instrumentation}
About 100~BPMs are assumed per BDS line (total of 200). Most of these BPMs
need between 20 and 50~nm resolution. Few FD BPMs require 3~nm resolution
in order to monitor and feedback the orbit at the FD.


Four horizontal and four vertical  beam size laser wires are assumed per
BDS line, see Fig.~\ref{diagnostics}. The vertical laser wires should resolve a 1~$\mu$m beam with 1\% resolution.

The polarization laser collides with the beam with a 10~mrad angle. 
It should have a wavelength of 532~nm and an IP spot size of 50~$\mu$m.
This set-up guarantees a resolution of 0.1\%.

Other required instrumentation as beam loss monitors, beam profile
monitors, etc, will be specified during the technical design phase.
 
%\subsubsubsection{Movers}\label{BDS:Movers}

%\subsubsubsection{Collimators}\label{BDS:collimators}

\subsubsubsection{Crab cavities}\label{BDS:CC}

The baseline crab cavities operate at 12 GHz
and require a phase stability of 0.02$^{\circ}$
and an amplitude stability of 2\% for a luminosity loss of 2\%. 
Crab cavities also need strong high order mode damping. 
Figure~\ref{CC} shows the current design
of the crab cavity~\cite{crab}.





\begin{figure}\vspace{-0.cm}
\begin{center}
\includegraphics[width=11.cm, angle=0]{CrabFigs/Racetrack_damped1.eps}
\end{center}\vspace{-0.cm}
\caption{12 cell crab cavity design including wakefield dampers (length $\approx$300~mm).}\label{CC}
\end{figure}





\subsubsubsection{Vacuum}\label{BDS:Vaccum}

The vacuum system for the BDS can be separated into 4 main types of systems, linked by common interfaces and requirements.

There are 206 dipole magnet chambers of 24 mm internal radius and 70 quadrupole magnet chambers with 8mm inner radius where the dimensions are constrained by the surrounding magnetic elements. These are separated by drift vacuum sections where dimensions and materials can be optimized for vacuum and mechanics. Finally there are a number of special vacuum sectors containing collimators and crab cavities which will have special requirements.

The requirements from the accelerator physics side are for an average pressure better than 1 nTorr~\cite{gio}. From the point of view of vacuum and surface physics, the pressure and surfaces must be designed to prevent pressure instabilities in the positron line. Additional requirements for the special vacuum sectors remain to be determined.

Preliminary pressure analysis suggests that the dipole chambers can be unbaked, with lumped ion pumps at both extremities. The small conductance of the quadrupole chambers means that a distributed pumping system along the chamber length will be required. This could use the same concept as for the main LINAC module chambers, i.e. ante-chambers with NEG pumping strips connected to the beam aperture~\cite{Garion}.



\subsubsubsection{Alignment}\label{BDS:Alignment}
All elements in the CLIC BDS are assumed to be prealigned
to 10~$\mu$m transversely over a distance of 500~m.
The longitudinal prealignment of the elements in the FFS
will be determined within $\pm$25~$\mu$m.
The determination of the transverse position of each element will follow the same strategy as for the main linac (see~Section~3.15). It will be performed by two different networks~\cite{Helene}: 
\begin{itemize}
\item A Metrological Reference Network (MRN), consisting of overlapping wires with a length of 500 m, linked by biaxial Wire Positioning Sensors (WPS) installed and measured on a common metrological plate. The objective of this network is to propagate the precision of a few micrometers over 500~m.

\item A Support Pre-alignment Network (SPN), framed by the MRN network, that associates sensors to each support to be aligned, will provide a few microns precision and accuracy over more than 10m. 
A third step will be required to link the support to the element to be aligned. This will be performed on a 3D Coordinate Machine Measurements (CMM), with an uncertainty of measurement of 0.3~$\mu$m + 1 ppm.
\end{itemize}
 
The longitudinal monitoring of the elements of Final Focus will be carried out using capacitive sensors coupled to each component. These sensors will measure with respect to targets located at the extremities of a carbon bar, independent from the components.

The solution of high precision and remote adjustment is the same as the one foreseen for the Main Beam quadrupoles (see Section~3.15.2.1), e.g. the use of eccentric cam movers. The only modification concerns the remote adjustment of the longitudinal axis in the final focus, which is not planned in the main linac. This additional adjustment will be performed by a stepper motor which will act on the blocking longitudinal point.




\begin{thebibliography}{99}
%
\bibitem{CLICr} \url{http://clicr.web.cern.ch/CLICr/MainBeam/BDS/}
%
\bibitem{schuler} P.~Schuler, ''Upstream polarimeter for CLIC'', CLIC08.
%
\bibitem{vasilis} V.~Vlachoudis et al, ``Energy deposition calculations on the BDS spoiler system'', CLIC-Note-932.
%
\bibitem{Javier} J.~Resta-L\'opez, D. Angal-Kalinin, B.~Dalena, J.~L.~Fernández-Hernando, F.~Jackson,
D.~Schulte, A.~Seryi and R.~Tom\'as, 
``Status report of the baseline collimation system of CLIC. Part II.'', CLIC-Note-883, 2011.
%
\bibitem{PLACET} D. Schulte, T.E.~D'Amico, G.~Guignard,  N.~Leros, "Simulation  Package based on PLACET", PAC2001, Chicago (2001); CERN/PS 2001-028 (AE), CLIC  Note 482 (2001)
%
\href{http://www.eurotev.org/reports__presentations/eurotev_reports/2008/e1533/ EUROTeV-Report-2008-076.pdf}{EUROTeV-Report-2008-076}
%
\bibitem{EUROTeV-Report-2008-076}
H.~Burkhardt, I.~Ahmed, M.~Fitterer, A.~Latina, L.~Neukermans, and D.~Schulte,
  ``Halo and tail generation computer model and studies for linear colliders''
%
\bibitem{Agapov2009708}
I.~Agapov, G.~Blair, S.~Malton, and L.~Deacon, ``BDSIM: A particle tracking
  code for accelerator beam-line simulations including particle-matter
  interactions,'' NIM-A {\bf 606} (2009), no.~3, 708 -- 712.
%
\bibitem{Lucie} L.~Linsen, ``CLIC detector status'' CLIC meeting June 17, 2011.
%
\bibitem{Burkhardt:2010zz} H.~Burkhardt, G.~A. Blair, and L.~Deacon, ``Muon Backgrounds in CLIC'',
\href{http://accelconf.web.cern.ch/AccelConf/IPAC10/papers/thpd014.pdf}{IPAC 2010} 
%\href{/Users/hbu/temp/IPAC/IPAC_2010_thpd014.pdf}{\lc}.
%
\bibitem{Raimondi}  P.~Raimondi and A.~Seryi, Phys. Rev. Lett. {\bf 86}, 3779-3782. 
%
\bibitem{Tomas} R.~Tom\'as,  Phys. Rev. ST Accel. Beams {\bf 9}, 081001 (2006).
%
\bibitem{Guillermo} G.~Zamudio and R.~Tom\'as, ``Optimization of the CLIC 500 GeV Final Focus System and design of a new 3 TeV Final Focus System with L$^*$=6.0~m'', CERN-OPEN-2011-035 and CLIC-Note-882, 2010.
%
\bibitem{Dalena} B.~Dalena, D.~Schulte, R.~Tom\'as, CERN, D.~Angal-Kalinin, 
``Solenoid and Synchrotron radiation effects in CLIC'', CERN-ATS-2009-083 and CLIC-Note-786, 2009. 
%
\bibitem{Oide} K.~Oide, Phys. Rev. Lett. {\bf 61}, 1713 - 1715 (1988).
%
\bibitem{CLIC08} R.~Tom\'as et al, ``Summary of the BDS and MDI CLIC08 Working Group'', CLIC-Note-776.
%
\bibitem{barbSol}  B.~Dalena, D.~Schulte, R.~Tom\'as, ``Impact of the experiment solenoid in the CLIC luminosity'', Proceedings of IPAC 2010.
%
\bibitem{ian} I.R.R.~Shinton and R.M.~Jones, ``Beam dynamics effects with Crab Cavities'', \href{http://indico.cern.ch/conferenceDisplay.py?confId=106655}{5$^{\rm th}$ CLIC-ILC BDS+MDI meeting}. 
%
\bibitem{JB} J.~Barranco, E.~Mar\'in and R.~Tom\'as,  ``Luminosity studies in a traveling focus scheme in the CLIC Final focus'', {\it to be published as CLIC Note}.
%
\bibitem{AS} A.~Seryi, ``Crab cavity effects on y-beam size for ILC'', January 2005 (revisited in the 
\href{http://indico.cern.ch/conferenceDisplay.py?confId=101200}{3$^{\rm rd}$ CLIC-ILC BDS+MDI meeting)}.
%
\bibitem{RaphaelMuntzner} R. Mutzner et al, ``Multi-Bunch effect of resistive wall in the CLIC BDS'' ,CLIC-Note-818.
%

\bibitem{lumimeas} D.~Schulte, ``New CLIC parameters, luminosity and background'', presented in the CLIC 08 workshop at CERN, 14-17 October 2008.
%
\bibitem{Eliasson} P.~Eliasson, M. Korostelev, D. Schulte and R. Tom\'as, ``Luminosity tuning at the interaction point'', Proceedings of EPAC 2006, Edinburgh, Scotland.
%
\bibitem{BarbaLumi} B.~Dalena, J. Barranco, A. Latina, E.~Mar\'in, J.~Pfingstner,
D.~Schulte, J.~Snuverink, R.~Tom\'as, and G.~Zamudio, ``BDS tuning and Luminosity Monitoring in CLIC'', CLIC-Note-931.
%
\bibitem{antisolenoid}  D.~Swoboda, B.~Dalena and R.~Tom\'as,  ``CLIC spectrometer magnet interference computation of transversal B-field on primary beam'', CERN-OPEN-2010-016 and CLIC-Note-815.
%
\bibitem{crab} A.~Dexter, G.~Burt, P.~Ambatu, I.~Shinton, R.~Jones, ``CLIC crab cavity specifications'', EuCARD-Milestone-M10.3.4-v7, 2010.
%
\bibitem{gio}   G.~Rumolo, ``Parameter Specification. Vacuum System for the CLIC Beam Delivery System.'', EDMS Id: 992781  v.2. 
%
\bibitem{Garion} C.~Garion and H.~Kos "Design of the CLIC Quadrupole Vacuum Chambers",
Proceedings of the PAC 2009. Vancouver, Canada.
%
\bibitem{Helene} H. Mainaud Durand et al., "CLIC active pre-alignment system: proposal for CDR and program for TDR", IWAA 2010, Hamburg, Germany, 2010.
\end{thebibliography}
\end{document}
