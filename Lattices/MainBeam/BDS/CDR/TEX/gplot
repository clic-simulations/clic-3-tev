

set terminal postscript enhanced color solid 25




set size 1.7,0.82
set output "layout3TeV.eps"


set key 0,2.2 samplen 1
set xlabel "s[m]"
set ylabel "x[m]"

set label "IP" at -15, -0.1
set label "Diagnostics" at -2750,0.2
set label "Energy collimation" at -2050, 0.2
set label "Transverse collimation" at -1120, 0.2
set label "Final focus" at -310, 0.2
set label "Tune-up dump" at -2330, -0.57
#set label "Post-" at 0,0.15
#set label "collision" at -15,0.35
#set label "lines" at -20,0.55
#set label "3TeV 20mrad" at -400,-0.5 textcolor lt 1
#set label "500GeV 18.6mrad" at -650,0.4 textcolor lt 3
#set label "linac (0.6mrad)" at -3000, 0.25 textcolor lt 2


! awk '$7>-2280 && $7<-2100{print }' /afs/cern.ch/user/r/rtomas/lintrack/CLICr/MainBeam/BDS/v_09_04_01/survey.zero > td
! grep Q /afs/cern.ch/user/r/rtomas/lintrack/CLICr/MainBeam/BDS/v_09_04_01/survey.zero | awk '{print $7-$3-0.4, $5;print $7,$5;print " "}' > quads
! grep -e MB -e B3 -e B4 -eB1 /afs/cern.ch/user/r/rtomas/lintrack/CLICr/MainBeam/BDS/v_09_04_01/survey.zero| awk '{print $7-$3, $5;print $7,$5;print " "}' > dipoles
! grep -e SP -e COLL -e AB /afs/cern.ch/user/r/rtomas/lintrack/CLICr/MainBeam/BDS/v_09_04_01/survey.zero  | awk '{print $7-$3-1.6, $5;print $7+1.5,$5;print " "}' > collimators 
! awk '/D2BCOL/{i++} /D2BCOL/ && i>1{print $7-$3+2, $5+0.0018;print $7-2.5,$5-0.0018;print " "}' /afs/cern.ch/user/r/rtomas/lintrack/CLICr/MainBeam/BDS/v_09_04_01/survey.zero > muonspoilers


set label "{/Symbol a}=0.6 mrad" at -2270, -0.07 left
unset key
p [-2890:50][-0.65:0.25]"/afs/cern.ch/user/r/rtomas/lintrack/CLICr/MainBeam/BDS/v_09_04_01/survey.zero" u 7:(-$5) t"3 Tev e^- BDS"  w l lt 7 lw 2,\
           "quads" u  ($1):(-$2-0.015) w l  lt 1 lw 14,\
           "quads" u  ($1):(-$2+0.015) w l  lt 1 lw 14,\
           "dipoles" u ($1):(-$2-0.015)  w l  lt 3 lw 14,\
           "dipoles" u ($1):(-$2+0.015)  w l  lt 3 lw 14,\
           "collimators" u ($1):(-$2+0.03)  w l  lt 7 lw 30,\
           "collimators" u ($1):(-$2-0.03)  w l  lt 7 lw 30,\
    "td" u  7:(-(0.187211212187+0.000004*($7+2325)**2)) t"tune-up dump" w l lt 7 lw 2,\
   "< echo -2700 0; echo -2300 0" w l lt 0,\
   "< echo -2355 0; echo -2360 -0.04; echo -2380 -0.08;echo -2400 -0.11" w l lt 0,\
   "-" u ($1*20-2125):($2*0.03-0.4) w l lw 3 lt 7
0 0
1 0
1 -1
2 -1
2 0
3 0
3 -2
0 -2
0 0
e


unset label

set output "diagnostics3TeV.eps"
set size 1.7,1
set multiplot
set size 1.7,0.55
set origin 0,0
set lmargin 8
set label "Coupling" at -2780,-0.1
set label "correction" at -2780, -0.2

set label "laser wires" at -2580, 0.1 tc lt 2
set label "Polarization laser IP" at -2190, -0.07 tc lt 8

set label "energy" at -1885, 0.03 center
set label "spoiler" at -1885, -0.06 center
set label "absorber" at -1759, -0.1 center

p [-2820:-1700][-0.4:0.25]"/afs/cern.ch/user/r/rtomas/lintrack/CLICr/MainBeam/BDS/v_09_04_01/survey.zero" u 7:(-$5) t"3 Tev e^- BDS"  w l lt 7 lw 2,\
           "quads" u  ($1):(-$2-0.015) w l  lt 1 lw 14,\
           "quads" u  ($1):(-$2+0.015) w l  lt 1 lw 14,\
           "dipoles" u ($1):(-$2-0.015)  w l  lt 3 lw 14,\
           "dipoles" u ($1):(-$2+0.015)  w l  lt 3 lw 14,\
           "collimators" u ($1):(-$2+0.03)  w l  lt 7 lw 30,\
           "collimators" u ($1):(-$2-0.03)  w l  lt 7 lw 30,\
           "LaserWires"   u ($1+2):(-$2+0.03)  w l  lt 2 lw 30,\
           "LaserWires"   u ($1+2):(-$2-0.03)  w l  lt 2 lw 30,\
           "< echo -2060 -0.28; echo -2050 -0.2" w l lt 8 lw 9,\
    "td" u  7:(-(0.187211212187+0.000004*($7+2325)**2)) t"tune-up dump" w l lt 7 lw 2


set origin 0,0.48
unset xlabel
set ylabel "{/Symbol b}_{x,y} [m]"
set key -2750,175 samplen 1
set size 0.792,0.55
set xtics 200
set ytics 50
unset label
p [-2820:-2390]"/afs/cern.ch/user/r/rtomas/lintrack/CLICr/MainBeam/BDS/v_09_04_01/twiss" u ($3-2795.934688):16 t"{/Symbol b}_{x}" w l lw 3 lt 1,\
  "" u ($3-2795.934688):17 t"{/Symbol b}_{y}" w l lt 3 lw 3


set origin 0.855,0.48
set size 0.849,0.55
unset key
set ylabel "{/Symbol b}_{x,y}[km], -D_{x}[cm]" 1
set lmargin 3
set rmargin 3
set ytics 20
set key -2175, 110

p [-2250:-1700]"/afs/cern.ch/user/r/rtomas/lintrack/CLICr/MainBeam/BDS/v_09_04_01/twiss" u ($3-2795.934688):($16/1000) t"{/Symbol b}_{x}" w l lw 3 lt 1,\
  "" u ($3-2795.934688):($17/1000) t"{/Symbol b}_{y}" w l lt 3 lw 3,\
  "" u ($3-2795.934688):($20*100) t"-D_{x}" w l lt 2 lw 3




reset
unset multiplot




set output "ffs3TeV.eps"
set size 1.7,1
set multiplot
set size 1.7,0.55
set origin 0,0
set lmargin 8


set label "FD" at -10, 0.1 center tc lt 1

set label "betatron collimators" at -750, 0. center tc lt 7
set label "muon spoilers" at -730, -0.33 left tc lt 9


set label "crab" at -20, -0.1 center tc lt 2
set label "cavity" at -27, -0.2 center tc lt 2




unset key
set xlabel "s[m]"
set ylabel "x[m]"
set mxtics 2
p [-1000:10][-0.4:0.25]"/afs/cern.ch/user/r/rtomas/lintrack/CLICr/MainBeam/BDS/v_09_04_01/survey.zero" u 7:(-$5) t"3 Tev e^- BDS"  w l lt 7 lw 2,\
           "quads" u  ($1):(-$2-0.015) w l  lt 1 lw 14,\
           "quads" u  ($1):(-$2+0.015) w l  lt 1 lw 14,\
           "dipoles" u ($1):(-$2-0.015)  w l  lt 3 lw 14,\
           "dipoles" u ($1):(-$2+0.015)  w l  lt 3 lw 14,\
           "collimators" u ($1):(-$2+0.035)  w l  lt 7 lw 30,\
           "collimators" u ($1):(-$2-0.035)  w l  lt 7 lw 30,\
           "muonspoilers" u ($1):(-$2-0.02)  w l  lt 9 lw 14,\
           "muonspoilers" u ($1):(-$2+0.015)  w l  lt 9 lw 14,\
           "< echo -20 0" w p pt 13 ps 2 lt 2



set origin 0,0.48
unset xlabel
set ylabel "{/Symbol b}_{x,y} [m]"
set key -2750,175 samplen 1
set size 0.956,0.55
set xtics 200
unset label
set ytics 100
p [-1000:-500]"/afs/cern.ch/user/r/rtomas/lintrack/CLICr/MainBeam/BDS/v_09_04_01/twiss" u ($3-2795.934688):16 t"{/Symbol b}_{x}" w l lw 3 lt 1,\
  "" u ($3-2795.934688):17 t"{/Symbol b}_{y}" w l lt 3 lw 3


set origin 1.017,0.48
set size 0.685,0.55
unset key
set ylabel "{/Symbol b}_{x,y}[km], -D_{x}[mm]" 0.8
set lmargin 3
set rmargin 3

set key -2200, 110
set ytics 50
set key -100, 250
p [-376:10]"/afs/cern.ch/user/r/rtomas/lintrack/CLICr/MainBeam/BDS/v_09_04_01/twiss" u ($3-2795.934688):($16/1000) t"{/Symbol b}_{x}" w l lw 3 lt 1,\
  "" u ($3-2795.934688):($17/1000) t"{/Symbol b}_{y}" w l lt 3 lw 3,\
  "" u ($3-2795.934688):($20*1000) t"-D_{x}" w l lt 2 lw 3

























#Combined plot
reset
unset multiplot

set terminal postscript enhanced color solid 29
set output "layouts500GeV3TeV.eps"


set key 0,2.2 samplen 1
set xlabel "s[m]"
set ylabel "x[m]"

set label "IP" at 0, -0.2 center
#set label "Post-" at 0,0.15
#set label "collision" at -15,0.35
#set label "lines" at -20,0.55
#set label "3TeV 20mrad" at -400,-0.5 textcolor lt 1
#set label "500GeV 18.6mrad" at -650,0.4 textcolor lt 3
#set label "linac (0.6mrad)" at -3000, 0.25 textcolor lt 2


set label "500 GeV" at -1000,-0.8
set label "3 TeV" at -1800, 0




set size 1.7,1

unset key



p [-2890:50][-1:0.25]\
           "500GeV/survey.500bds.new.more.bnd" u 7:(-$5) t"3 Tev e^- BDS"  w l lt 7 lw 2,\
           "500GeV/quads" u  ($1):(-$2-0.02) w l  lt 1 lw 14,\
           "500GeV/quads" u  ($1):(-$2+0.02) w l  lt 1 lw 14,\
           "500GeV/dipoles" u ($1):(-$2-0.02)  w l  lt 3 lw 14,\
           "500GeV/dipoles" u ($1):(-$2+0.02)  w l  lt 3 lw 14,\
           "500GeV/collimators" u ($1):(-$2+0.045)  w l  lt 7 lw 30,\
           "500GeV/collimators" u ($1):(-$2-0.045)  w l  lt 7 lw 30,\
           "/afs/cern.ch/user/r/rtomas/lintrack/CLICr/MainBeam/BDS/v_09_04_01/survey.zero" u 7:(-$5) t"3 Tev e^- BDS"  w l lt 7 lw 2,\
           "quads" u  ($1):(-$2-0.02) w l  lt 1 lw 14,\
           "quads" u  ($1):(-$2+0.02) w l  lt 1 lw 14,\
           "dipoles" u ($1):(-$2-0.02)  w l  lt 3 lw 14,\
           "dipoles" u ($1):(-$2+0.02)  w l  lt 3 lw 14,\
           "collimators" u ($1):(-$2+0.045)  w l  lt 7 lw 30,\
           "collimators" u ($1):(-$2-0.045)  w l  lt 7 lw 30,\
           "td" u  7:(-(0.187211212187+0.000004*($7+2325)**2)) t"tune-up dump" w l lt 7 lw 2,\
            "-" u ($1*20-2125):($2*0.03-0.4) w l lw 3 lt 7
0 0
1 0
1 -1
2 -1
2 0
3 0
3 -2
0 -2
0 0
e


! rm td quads dipoles collimators muonspoilers
#!









reset
unset multiplot


set terminal postscript enhanced color solid 25
set output "aper3TeV.eps"
set xlabel "Longitudinal location [km]"
set ylabel "Aperture radius [mm]"
unset key #r samplen 1
set size 1,0.84
p [-3:0.3]"< awk '$14>0 && $14>0.002{print }' /afs/cern.ch/eng/sl/lintrack/CLICr/MainBeam/BDS/v_10_01_25/ff4placet" u (($3-2795)/1000):($14*1000) t"3Tev"w l lw 3

#"<awk '$7>0 {print}' /afs/cern.ch/user/g/gzamudio/MADX/CLIC_500GEV_BDS/500Gevto3TevSMOOTH/Gradients.twiss" u (($2-1727)/1000):($7*1000)  t "500Gev" w l lw 3

















quit



