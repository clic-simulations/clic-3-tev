#
# Dump and matching of RTML to Main Linac
#
# beamparams=[betax,alphax,emitnx,betay,alphay,emitny,sigmaz,charge,
#             uncespread,echirp,energy,nslice,nmacro,nsigmabunch,nsigmawake]
# units=[m,rad,m*rad,m,rad,m*rad,m,C,1,1/m,eV,1,1,1,1]
#
# where necessary units were converted to placet units in main.tcl

proc lattice_dump_and_match_rtml_to_main_linac {bparray} {
upvar $bparray beamparams

set usesixdim 1
set numthinlenses 100
set quad_synrad 0

set refenergy $beamparams(meanenergy)
set q0 $beamparams(charge)


set kqm1  0.4069109223
set kqm2  0.09620017379
set kqm3 -0.7439161361
set kqm4  0.9431461545
set kqm5 -1.653611708

set lquadm 0.3

set ld1 0.60
set ld2 1.00
set ld3 4.00
set ld4 5.80
set ld5 2.90

SetReferenceEnergy $refenergy

Girder
Drift -length $ld1 -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm1*$lquadm*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ld2 -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm2*$lquadm*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ld3 -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm3*$lquadm*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ld4 -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm4*$lquadm*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ld5 -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -synrad $quad_synrad -length 0.175 -strength [expr $kqm5*0.175*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Quadrupole -synrad $quad_synrad -length 0.000001 -strength 0.0 -six_dim $usesixdim -thin_lens $numthinlenses
}
