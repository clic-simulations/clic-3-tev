#
# Diagnostics section 3
#
# beamparams=[betax,alphax,emitnx,betay,alphay,emitny,sigmaz,charge,
#             uncespread,echirp,energy,nslice,nmacro,nsigmabunch,nsigmawake]
# units=[m,rad,m*rad,m,rad,m*rad,m,C,1,1/m,eV,1,1,1,1]
#
# where necessary units were converted to placet units in main.tcl

proc lattice_diagnostics_3 {bparray} {
upvar $bparray beamparams

set usesixdim 1
set numthinlenses 100
set quad_synrad 0

set refenergy $beamparams(meanenergy)

set lquadm 0.2

set kqm1 0.3776567687
set kqm2 -0.3776567687

set ldm 10.0

SetReferenceEnergy $refenergy

Girder
for {set cell 1} {$cell<=4} {incr cell} {
  Bpm -length 0.0
  Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm1*$lquadm*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
  Drift -length $ldm -six_dim $usesixdim
  Bpm -length 0.0
  Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm2*$lquadm*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
  Drift -length $ldm -six_dim $usesixdim
}

}
