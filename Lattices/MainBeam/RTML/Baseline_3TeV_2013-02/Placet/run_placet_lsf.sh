#!/bin/bash
#
beam="$1"
wakes="$2"
isr="$3"

if [ "$wakes" != "1" ] ; then
wakes="0"
fi
if [ "$isr" != "1" ] ; then
isr="0"
fi
if [ "$beam" != "e+" ] ; then
beam="e-"
fi

echo "Job started: " $( date )
cd $LS_SUBCWD

afshome="/afs/cern.ch/user/f/fstulle/"
simdirname="${afshome}/scratch0/Work/RTML_WiP/Placet/simrun_${beam}_wakes=${wakes}_isr=${isr}"
test ! -d $simdirname && mkdir -p $simdirname

test -d $simdirname/Lattices_${beam} && rm -R $simdirname/Lattices_${beam}
test -d $simdirname/Parameters && rm -R $simdirname/Parameters
test -d $simdirname/Scripts && rm -R $simdirname/Scripts

cp -LR ./Lattices_${beam} $simdirname
cp -LR ./Parameters $simdirname
cp -LR ./Scripts $simdirname
cp ./main_${beam}.tcl $simdirname
cp ./tracking.tcl $simdirname

cd $simdirname

echo "
set beamparams(useisr) $isr
set beamparams(usewakefields) $wakes
" > inputparams.tcl

placet-octave main_${beam}.tcl &>placet.out
echo "Job finished: " $( date )
