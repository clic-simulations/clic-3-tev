#!/bin/bash
#
beam="$1"
lattice="$2"
wakes="$3"
csr="$4"
isr="$5"

if [ "$wakes" != "1" ] ; then
wakes="0"
fi
if [ "$csr" != "1" ] ; then
csr="0"
fi
if [ "$isr" != "1" ] ; then
isr="0"
fi
if [ "$lattice" != "e" -a "$lattice" != "csr" ] ; then
lattice="simple"
fi
if [ "$beam" != "e+" ] ; then
beam="e-"
fi

echo "Job started: " $( date )

# some MPI related stuff
export MPI=/afs/cern.ch/project/parc/mpi/
export MPIBIN=$MPI/bin
export MPILIB=$MPI/lib

export PATH=$MPIBIN:$PATH
export LD_LIBRARY_PATH=$MPILIB:$LD_LIBRARY_PATH

# set up some job related stuff
outputdir="/afs/cern.ch/user/f/fstulle/scratch0/Work/RTML_WiP/simrun_${beam}_${lattice}_wakes=${wakes}_csr=${csr}_isr=${isr}"
mkdir -p $outputdir

./create_rtml_lte.sh $beam $wakes $csr $isr
cp extractdata.sh $outputdir
mv rtml_${beam}_wakes=${wakes}_csr=${csr}_isr=${isr}.lte $outputdir/rtml.lte
cp rtml_${lattice}.ele $outputdir/rtml.ele
cp -L wake_bc1.sdds $outputdir
cp -L wake_bc2.sdds $outputdir
cp -L wake_booster.sdds $outputdir
cp mpihosts.txt $outputdir
cd $outputdir
#

# now we run the mpi job
mpirun --prefix $MPI -np 32 --hostfile ./mpihosts.txt Pelegant_lxbst rtml.ele > Pelegant.out 2> Pelegant.err

echo "Job finished: " $( date )
