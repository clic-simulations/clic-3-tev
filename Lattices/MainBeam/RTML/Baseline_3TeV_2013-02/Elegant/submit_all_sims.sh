#!/bin/bash
#
# e-
#
# simple lattice
bsub -q engpara -n 16 run_elegant_lsf.sh e- simple 0 0 0
#
bsub -q engpara -n 16 run_elegant_lsf.sh e- simple 1 0 0
#
# exact lattice
bsub -q engpara -n 16 run_elegant_lsf.sh e- e 0 0 0
#
bsub -q engpara -n 16 run_elegant_lsf.sh e- e 1 0 0
#
bsub -q engpara -n 16 run_elegant_lsf.sh e- e 0 0 1
#
bsub -q engpara -n 16 run_elegant_lsf.sh e- e 1 0 1
#
# csr lattice
bsub -q engpara -n 16 run_elegant_lsf.sh e- csr 0 0 0
#
bsub -q engpara -n 16 run_elegant_lsf.sh e- csr 1 0 0
#
bsub -q engpara -n 16 run_elegant_lsf.sh e- csr 0 0 1
#
bsub -q engpara -n 16 run_elegant_lsf.sh e- csr 1 0 1
#
bsub -q engpara -n 16 run_elegant_lsf.sh e- csr 0 1 0
#
bsub -q engpara -n 16 run_elegant_lsf.sh e- csr 1 1 0
#
bsub -q engpara -n 16 run_elegant_lsf.sh e- csr 0 1 1
#
bsub -q engpara -n 16 run_elegant_lsf.sh e- csr 1 1 1
#
#
# e+
#
# simple lattice
bsub -q engpara -n 16 run_elegant_lsf.sh e+ simple 0 0 0
#
bsub -q engpara -n 16 run_elegant_lsf.sh e+ simple 1 0 0
#
# exact lattice
bsub -q engpara -n 16 run_elegant_lsf.sh e+ e 0 0 0
#
bsub -q engpara -n 16 run_elegant_lsf.sh e+ e 1 0 0
#
bsub -q engpara -n 16 run_elegant_lsf.sh e+ e 0 0 1
#
bsub -q engpara -n 16 run_elegant_lsf.sh e+ e 1 0 1
#
# csr lattice
bsub -q engpara -n 16 run_elegant_lsf.sh e+ csr 0 0 0
#
bsub -q engpara -n 16 run_elegant_lsf.sh e+ csr 1 0 0
#
bsub -q engpara -n 16 run_elegant_lsf.sh e+ csr 0 0 1
#
bsub -q engpara -n 16 run_elegant_lsf.sh e+ csr 1 0 1
#
bsub -q engpara -n 16 run_elegant_lsf.sh e+ csr 0 1 0
#
bsub -q engpara -n 16 run_elegant_lsf.sh e+ csr 1 1 0
#
bsub -q engpara -n 16 run_elegant_lsf.sh e+ csr 0 1 1
#
bsub -q engpara -n 16 run_elegant_lsf.sh e+ csr 1 1 1
#
