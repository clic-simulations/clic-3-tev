#
# Booster Linac lattice
#
# beamparams=[betax,alphax,emitnx,betay,alphay,emitny,sigmaz,charge,
#             uncespread,echirp,energy,nslice,nmacro,nsigmabunch,nsigmawake]
# units=[m,rad,m*rad,m,rad,m*rad,m,C,1,1/m,eV,1,1,1,1]
#
# rfparams=[gradient,phase,lambda,a,g,l,delta,delta_g]
# units=[eV/m,Degree,m,m,m,m,m,m]
#
# where necessary units were converted to placet units in main.tcl

proc lattice_booster_linac {rfparray bparray} {
upvar $rfparray rfparams
upvar $bparray beamparams

#puts [array get rfparams]
#puts [array get beamparams]

set usesixdim 1
set numthinlenses 100
set quad_synrad 0

set gradient $rfparams(gradient)
set refenergy $beamparams(meanenergy)
set q0 $beamparams(charge)
set cavphase $rfparams(phase)

set lcav 1.5
set lquad 0.3

set kq1 0.3891746439
set kq2 -0.3695715001

set ld 0.3

#this is required to take into account the energy gain due to acceleration
#a phase error due to a longitudinal offset is corrected
set pi 3.14159265358979
set dphase [expr $beamparams(meanz)*1e-6*360/$rfparams(lambda)]
set deacc [expr $gradient*$lcav*cos(($cavphase-0*$dphase)/180.0*$pi)]
set cavphase [expr $cavphase+$dphase]

#energy loss due to wake fields
set dewake [expr $rfparams(dewake)*1e6/1e9*$lcav]

#this corrects the average energy loss induced by RF curvature
set sz [expr $beamparams(sigmaz)*1e-6*2*$pi/$rfparams(lambda)]
set mz [expr $beamparams(meanz)*1e-6*2*$pi/$rfparams(lambda)]
set decurve [expr -$gradient*$lcav*(1-exp(-$sz*$sz/2)*cos($mz*0))]

set de [expr $deacc+$dewake+$decurve]
#puts $de
set ebeam $refenergy

SetReferenceEnergy $ebeam

for {set sec 1} {$sec<=17} {incr sec} {
    Girder
    
    Cavity -length $lcav -gradient $gradient -phase $cavphase -type 0
    Cavity -length $lcav -gradient $gradient -phase $cavphase -type 0
    set ebeam [expr $ebeam+2*$de]
    SetReferenceEnergy $ebeam
    Drift -length $ld -six_dim $usesixdim
    Cavity -length $lcav -gradient $gradient -phase $cavphase -type 0
    Cavity -length $lcav -gradient $gradient -phase $cavphase -type 0
    set ebeam [expr $ebeam+2*$de]
    SetReferenceEnergy $ebeam
    Drift -length $ld -six_dim $usesixdim
    Cavity -length $lcav -gradient $gradient -phase $cavphase -type 0
    Cavity -length $lcav -gradient $gradient -phase $cavphase -type 0
    set ebeam [expr $ebeam+2*$de]
    SetReferenceEnergy $ebeam
    Drift -length $ld -six_dim $usesixdim
    Cavity -length $lcav -gradient $gradient -phase $cavphase -type 0
    Cavity -length $lcav -gradient $gradient -phase $cavphase -type 0
    set ebeam [expr $ebeam+2*$de]
    SetReferenceEnergy $ebeam
    Drift -length $ld -six_dim $usesixdim

    Bpm -length 0.0
    Quadrupole -synrad $quad_synrad -length $lquad -strength [expr $kq1*$lquad*$ebeam] -six_dim $usesixdim -thin_lens $numthinlenses
    Drift -length $ld -six_dim $usesixdim

    Cavity -length $lcav -gradient $gradient -phase $cavphase -type 0
    Cavity -length $lcav -gradient $gradient -phase $cavphase -type 0
    set ebeam [expr $ebeam+2*$de]
    SetReferenceEnergy $ebeam
    Drift -length $ld -six_dim $usesixdim
    Cavity -length $lcav -gradient $gradient -phase $cavphase -type 0
    Cavity -length $lcav -gradient $gradient -phase $cavphase -type 0
    set ebeam [expr $ebeam+2*$de]
    SetReferenceEnergy $ebeam
    Drift -length $ld -six_dim $usesixdim
    Cavity -length $lcav -gradient $gradient -phase $cavphase -type 0
    Cavity -length $lcav -gradient $gradient -phase $cavphase -type 0
    set ebeam [expr $ebeam+2*$de]
    SetReferenceEnergy $ebeam
    Drift -length $ld -six_dim $usesixdim
    Cavity -length $lcav -gradient $gradient -phase $cavphase -type 0
    Cavity -length $lcav -gradient $gradient -phase $cavphase -type 0
    set ebeam [expr $ebeam+2*$de]
    SetReferenceEnergy $ebeam
    Drift -length $ld -six_dim $usesixdim

    Bpm -length 0.0
    Quadrupole -synrad $quad_synrad -length $lquad -strength [expr $kq2*$lquad*$ebeam] -six_dim $usesixdim -thin_lens $numthinlenses
    Drift -length $ld -six_dim $usesixdim
}

Girder

Cavity -length $lcav -gradient $gradient -phase $cavphase -type 0
Cavity -length $lcav -gradient $gradient -phase $cavphase -type 0
set ebeam [expr $ebeam+2*$de]
SetReferenceEnergy $ebeam

#puts $ebeam
set beamparams(meanenergy) $ebeam
puts "Setting beamparams(meanenergy)=$beamparams(meanenergy)."
}
