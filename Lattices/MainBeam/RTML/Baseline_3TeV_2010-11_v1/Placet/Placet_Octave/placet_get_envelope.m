## -*- texinfo -*-
## @deftypefn {Function File} {} placet_get_envelope (@var{B})
## Return the beam envelope of beam `B', where `B' is a 6-columns matrix in guinea-pig format (returns radius of outermost particle), or a 17-columns matrix in sliced beam format (returns 3-sigma envelope radius)
## @end deftypefn

function r = placet_get_envelope(B)
  if nargin==1 && ismatrix(B) && columns(B)==6
    r = max(sqrt( B(:,2).^2 +  B(:,3).^2));
  elseif nargin==1 && ismatrix(B) && columns(B)==17
    n_sigma = 3.0;
    r = max(sqrt(( abs(B(:,4))+n_sigma*sqrt(B(:,8))).^2 + ( abs(B(:,6))+n_sigma*sqrt(B(:,11))).^2));
  else
    help placet_get_envelope
  endif
endfunction
