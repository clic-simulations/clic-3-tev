## -*- texinfo -*-
## @deftypefn {Function File} {} placet_get_response_matrix_attribute (@var{beamline}, @var{beam}, @var{bpms}, @var{direction}, @var{correctors}, @var{attribute}, @var{survey})
## Return the response matrix for 'beamline' and 'beam0', using selected 'bpms' and 'correctors'.
## @end deftypefn

function [R,b0]=placet_get_response_matrix_attribute(beamline, beam, B, direction, C, attribute, survey)
  if nargin==7 && ischar(beamline) && ischar(beam) && isvector(B) && ischar(direction) && isvector(C) && ischar(attribute) && ischar(survey)
    Res=placet_element_get_attribute(beamline, B, "resolution");
    placet_element_set_attribute(beamline, B, "resolution", 0.0);
    placet_test_no_correction(beamline, beam, survey);
    b0=placet_get_bpm_readings(beamline, B);
    R=zeros(rows(b0), columns(C));
    for i=1:columns(C)
      printf("Calculating response matrix, Column %d of %d...\n",i,columns(C));
      placet_element_vary_attribute(beamline, C(i), attribute, 0.1);
      placet_test_no_correction(beamline, beam, "None");
      placet_element_vary_attribute(beamline, C(i), attribute, -0.1);
      b=10*(placet_get_bpm_readings(beamline, B) - b0);
      if direction=="x"
        R(:,i)=b(:,1);
      else
        R(:,i)=b(:,2);
      endif
    endfor
    if direction=="x"
      b0=b0(:,1);
    else
      b0=b0(:,2);
    endif
    placet_element_set_attribute(beamline, B, "resolution", Res);
  else  
    help placet_get_response_matrix_attribute
  endif
endfunction
