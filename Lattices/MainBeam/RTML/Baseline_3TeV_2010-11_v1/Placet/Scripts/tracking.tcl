#
# the big tracking procedure
#
proc do_tracking {beamlinename beamname bparray stepnum} {
    global zoffset
    global placetunits
    upvar $bparray beamparams
    
    puts ""
    puts "Perform tracking step $stepnum using beamline $beamlinename and beam $beamname"
    puts ""
    
    set betax0 $beamparams(betax)
    set alphax0 $beamparams(alphax)
    set betay0 $beamparams(betay)
    set alphay0 $beamparams(alphay)
    
    #
    # plot Twiss functions
    #
    puts "Dumping Twiss data to file..."
    set outname [format "twiss_step_%d.dat" $stepnum]
#    TwissPlotStep -file $outname -beam $beamname -step 0.50
    TwissPlot -file $outname -beam $beamname
    puts "...Done!"

    #
    # dump energy spread along beam line
    #
    puts "Dumping energy spread to file..."
    set outname [format "e_spread_step_%d.dat" $stepnum]
    EnergySpreadPlot -file $outname -beam $beamname
    puts "...Done!"

    #
    # dump energy along beam line
    #
    puts "Dumping energy to file..."
    set outname [format "energy_step_%d.dat" $stepnum]
    BeamEnergyPlot -file $outname -beam $beamname
    puts "...Done!"


    
    #
    # dump AML file of lattice
    #
    BeamlineSaveAML -beamline $beamlinename -file [format "aml_lattice_%d.aml" $stepnum]

    #
    set quaderr 0.0e-1
    set caverr 0.0e-1
    set bpmerr 0.0e-1
    set benderr 0.0e-1
    
    SurveyErrorSet \
        -quadrupole_x $quaderr \
        -quadrupole_y $quaderr \
        -quadrupole_xp $quaderr \
        -quadrupole_yp $quaderr \
        -quadrupole_roll $quaderr \
        -cavity_x $caverr \
        -cavity_y $caverr \
        -cavity_xp $caverr \
        -cavity_yp $caverr \
        -bpm_x $bpmerr \
        -bpm_y $bpmerr \
        -bpm_xp $bpmerr \
        -bpm_yp $bpmerr \
        -bpm_roll $bpmerr \
        -sbend_x $benderr \
        -sbend_y $benderr \
        -sbend_xp $benderr \
        -sbend_yp $benderr \
        -sbend_roll $benderr \

    
    #TestNoCorrection -beam beam1 -survey none
    #TestQuadrupoleJitter -machines 100 -a0 0.0001 -a1 1.0 -steps 5 -beam beam1 -file emitt.dat
    #TestSimpleCorrectionDipole -machines 1 -binlength 100 -binoverlap 50 -bpm_resolution 0.1 -beam beam1 -testbeam beam1 -survey clic -emitt_fil emitt.dat
    
    #trajectorycorrection("rtml","beam1",10,"bpm.dat","dipole.dat","correction_statistics.dat");
    #[emitt,beamfin]=placet_test_no_correction("rtml","beam1","Zero");
    #beamfin=units_placet_to_std(beamfin);
    #deefin=beamfin(:,1)/mean(beamfin(:,1),1)-1;

    #
    # we do tracking, plotting, etc. in octave
    #
    Octave {
        format long;
        % read in some useful functions
        addpath("./Placet_Octave");
        scriptdir="./Scripts/";
        source([scriptdir,"octave_beam_statistics.m"]);
        source([scriptdir,"octave_correction_algorithms.m"]);
        
        % track beam
        [emitt,beamfin]=placet_test_no_correction("$beamlinename","$beamname","Clic");

        % convert from Placet units to more useful standard units
        % this is required for some calculations
        beamfin=units_placet_to_std(beamfin);

        % calculate new beam params for later use in TCL
        % this part is required for the correct connection to the next simulation step

        new_sigmas=std(beamfin(:,4),1);
        new_means=mean(beamfin(:,4),1);
        new_meanenergy=mean(beamfin(:,1),1);
        new_meanx=mean(beamfin(:,2),1);
        new_meanxp=mean(beamfin(:,5),1);
        new_meany=mean(beamfin(:,3),1);
        new_meanyp=mean(beamfin(:,6),1);
        new_gamma=new_meanenergy/(0.5109989*1e6);
        new_espread=std(beamfin(:,1),1)/new_meanenergy;
        new_lemit=emitproj(beamfin(:,4),beamfin(:,1)-new_meanenergy,1);
        new_uncespread=new_lemit/(new_sigmas*new_meanenergy);
        new_echirp=sqrt((new_espread^2-new_uncespread^2)/new_sigmas^2);
        [new_betax,new_alphax]=twiss(beamfin(:,2),beamfin(:,5),1);
        new_emitnx=new_gamma*emitproj(beamfin(:,2),beamfin(:,5),1);
        [new_betay,new_alphay]=twiss(beamfin(:,3),beamfin(:,6),1);
        new_emitny=new_gamma*emitproj(beamfin(:,3),beamfin(:,6),1);
        
        Tcl_SetVar("new_sigmas",new_sigmas);
        Tcl_SetVar("new_means",new_means);
        Tcl_SetVar("new_meanenergy",new_meanenergy);
        Tcl_SetVar("new_meanx",new_meanx);
        Tcl_SetVar("new_meanxp",new_meanxp);
        Tcl_SetVar("new_meany",new_meany);
        Tcl_SetVar("new_meanyp",new_meanyp);
        Tcl_SetVar("new_uncespread",new_uncespread);
        Tcl_SetVar("new_echirp",new_echirp);
        Tcl_SetVar("new_betax",new_betax);
        Tcl_SetVar("new_alphax",new_alphax);
        Tcl_SetVar("new_emitnx",new_emitnx);
        Tcl_SetVar("new_betay",new_betay);
        Tcl_SetVar("new_alphay",new_alphay);
        Tcl_SetVar("new_emitny",new_emitny);
        
        
        % the rest of the octave part is just dumping some phase space data and beam statistics,
        % i.e. it is not required for proper functioning of the simulations
        
        % use placet_evolve_beta_function to get Twiss from matrices
        elems=placet_get_name_number_list("$beamlinename","*");
        elemscount=size(elems,2);
        [tw_s,tw_betax,tw_betay,tw_alphax,tw_alphay,tw_mux,tw_muy,tw_dx,tw_dy]=placet_evolve_beta_function($betax0,$alphax0,$betay0,$alphay0,0,elemscount-1);
        outname=sprintf("twiss_test_step_%d.dat",$stepnum);
        fid=fopen(outname,"wt");
        fprintf(fid,"\# s betax alphax betay alphay mux muy dx dy\n");
        outdata=[tw_s',tw_betax,tw_alphax,tw_betay,tw_alphay,tw_mux',tw_muy',tw_dx',tw_dy']';
        fprintf(fid,"%20.12e %20.12e %20.12e %20.12e %20.12e %20.12e %20.12e %20.12e %20.12e\n",outdata);
        fclose(fid);
        
        % load the initial beam
        beamini=placet_get_beam("$beamname");
        beamini=units_placet_to_std(beamini);
        
        % clear outliers from initial beam
        w=ones(length(beamini),1);
        [xic,xpic,yic,ypic,sic,deic,wic]=clear_outliers_6d(beamini,w);
        beaminicore=[deic,xic,yic,sic,xpic,ypic];

        % clear outliers from final beam
        w=ones(length(beamfin),1);
        [xfc,xpfc,yfc,ypfc,sfc,defc,wfc]=clear_outliers_6d(beamfin,w);
        beamfincore=[defc,xfc,yfc,sfc,xpfc,ypfc];

        % dump some beam statistics to stdout and a file
        print_mean_std(beamini,beaminicore);
        print_mean_std(beamfin,beamfincore);
        if ($stepnum==4)
          save_mean_std(beamfin,beamfincore,"stat.txt","statcore.txt");
        end;
        
        % dump phasespace data of initial beam to file
        deeini=beamini(:,1)/mean(beamini(:,1),1)-1;
        outdata=[beamini(:,2),beamini(:,5),beamini(:,3),beamini(:,6),beamini(:,4),deeini]';
        outname=sprintf("phasespaces_initial_step_%d.dat",$stepnum);
        fid=fopen(outname,"wt");
        fprintf(fid,"\# x xp y yp s de\n");
        fprintf(fid,"%20.12e %20.12e %20.12e %20.12e %20.12e %20.12e\n",outdata);
        fclose(fid);
        
        % dump emittance data of final beam to file
        outdata=[emitt(:,1),emitt(:,2),emitt(:,3),emitt(:,4),emitt(:,5),emitt(:,6),emitt(:,7),emitt(:,8),emitt(:,9),emitt(:,10)]';
        outname=sprintf("emittance_step_%d.dat",$stepnum);
        fid=fopen(outname,"wt");
        fprintf(fid,"\# index avgepsx sigmaepsx avgtrajx 0 avgepsy sigmaepsy envelop 0 machinecount\n");
        fprintf(fid,"%20.12e %20.12e %20.12e %20.12e %20.12e %20.12e %20.12e %20.12e %20.12e %20.12e\n",outdata);
        fclose(fid);
        
        % dump phasespace data of final beam to file
        deefin=beamfin(:,1)/mean(beamfin(:,1),1)-1;
        outdata=[beamfin(:,2),beamfin(:,5),beamfin(:,3),beamfin(:,6),beamfin(:,4),deefin]';
        outname=sprintf("phasespaces_final_step_%d.dat",$stepnum);
        fid=fopen(outname,"wt");
        fprintf(fid,"\# x xp y yp s de\n");
        fprintf(fid,"%20.12e %20.12e %20.12e %20.12e %20.12e %20.12e\n",outdata);
        fclose(fid);
        
        % dump phasespace data of final core beam to file
        deefc=defc/mean(defc,1)-1;
        outdata=[xfc,xpfc,yfc,ypfc,sfc,deefc]';
        outname=sprintf("phasespaces_final_step_%d_core.dat",$stepnum);
        fid=fopen(outname,"wt");
        fprintf(fid,"\# x xp y yp s de\n");
        fprintf(fid,"%20.12e %20.12e %20.12e %20.12e %20.12e %20.12e\n",outdata);
        fclose(fid);
    }

    # end of tracking part
    
    # define the offset of the longitudinal coordinate z
    array set blinfo [BeamlineInfo]
    set deltaz $blinfo(length)
    set zoffset [expr $zoffset + $deltaz]
    set fid [open zoffset.dat a]
    set stmp [expr $stepnum+1]
    puts $fid "$stmp   $zoffset"
    close $fid
    
    # set new beamparams
    set beamparams(sigmaz) [expr $new_sigmas * $placetunits(xyz)]
    set beamparams(meanz) [expr $new_means * $placetunits(xyz)]
    set beamparams(meanenergy) [expr $new_meanenergy * $placetunits(energy)]
    set beamparams(meanx) [expr $new_meanx * $placetunits(xyz)]
    set beamparams(meanxp) [expr $new_meanxp * $placetunits(xpyp)]
    set beamparams(meany) [expr $new_meany * $placetunits(xyz)]
    set beamparams(meanyp) [expr $new_meanyp * $placetunits(xpyp)]
    set beamparams(startenergy) [expr $new_meanenergy * $placetunits(energy)]
    set beamparams(uncespread) $new_uncespread
    set beamparams(echirp) [expr $new_echirp/$placetunits(energy)]
    set beamparams(betax) $new_betax
    set beamparams(alphax) $new_alphax
    set beamparams(emitnx) [expr $new_emitnx * $placetunits(emittance)]
    set beamparams(betay) $new_betay
    set beamparams(alphay) $new_alphay
    set beamparams(emitny) [expr $new_emitny * $placetunits(emittance)]
    set beamparams(charge) $beamparams(charge)
}
