% some functions to calculate corrector settings
% i.e. dipole corrector strengths, quad positions,...
%
% errors are also applied here

%----------------------------------------
function [emitfinal,beamfinal]=trajectory_correction(beamlinename,beamname,correctiontype,correctortype,numberofsets,...
          bpmfilename,correctorsfilename,statfilename,...
          error_cavity_pos,error_cavity_gradient_rel,...
          error_bend_pos,error_bend_strength_rel,...
          error_quadrupole_pos,error_quadrupole_strength_rel,...
          error_multipole_pos,error_multipole_strength_rel,...
          error_bpm_pos,bpm_resolution,keepolderrors)

bpmfilename=[bpmfilename,"_",beamlinename,".txt"];
correctorsfilename=[correctorsfilename,"_",beamlinename,".txt"];
statfilename=[statfilename,"_",beamlinename,".txt"];

format long;
scriptdir="./Scripts/";
source([scriptdir,"octave_beam_statistics.m"]);
%
% some initialisations
if strcmpi(correctortype,"dipole")
  corrxattr="strength_x";
  corryattr="strength_y";
elseif strcmpi(correctortype,"quadrupole")
  corrxattr="x";
  corryattr="y";
else
  correctortype="quadrupole";
  corrxattr="x";
  corryattr="y";
end;

fid=fopen(bpmfilename,'wt');
fprintf(fid,"#BPM readings (in micrometer)\n");
fprintf(fid,"#%21s %22s %22s %22s\n","BPM X initial","BPM X final","BPM Y initial","BPM Y final");
fclose(fid);
fid=fopen(correctorsfilename,'wt');
if strcmpi(correctortype,"dipole")
  fprintf(fid,"#Corrector settings (in GeV/micrometer)\n");
  fprintf(fid,"#%21s %22s %22s %22s\n","Strength X initial","Strength X final","Strenght Y initial","Strength Y final");
end;
if strcmpi(correctortype,"quadrupole")
  fprintf(fid,"#Corrector settings (in micrometer)\n");
  fprintf(fid,"#%21s %22s %22s %22s\n","Offset X initial","Offset X final","Offset Y initial","Offset Y final");
end;
fclose(fid);
fid=fopen(statfilename,'wt');
fprintf(fid,"#Correction statistics (BPMs in micrometer, Emittance in m rad)\n");
fprintf(fid,"#%21s %22s %22s %22s %22s %22s %22s %22s %22s %22s %22s %22s %22s %22s %22s %22s %22s %22s %22s %22s\n",
            "STD BPM X initial","Mean BPM X initial","Emit X initial","Emit X core initial","STD BPM X final","Mean BPM X final","Emit X final","Emit X core final","Emit X perfect","Emit X core perfect",
            "STD BPM Y initial","Mean BPM Y initial","Emit Y initial","Emit Y core initial","STD BPM Y final","Mean BPM Y final","Emit Y final","Emit Y core final","Emit Y perfect","Emit Y core perfect");
fclose(fid);

% get lists of bpms and correctors
%
list_bpm=placet_get_number_list(beamlinename,"bpm");
list_corr=placet_get_number_list(beamlinename,correctortype);

numbpm=length(list_bpm);
numcorr=length(list_corr);

if (numbpm==0)||(numcorr==0)
  printf("No correctors or BPMs. Tracking will be performed without corrections.\n");
  [emitfinal,beamfinal]=placet_test_no_correction(beamlinename,beamname,"None");
  return;
end;

printf("Initializing %s correction...\n",correctiontype);

% Getting response matrices and calculating SVD matrices can take a rediculous amount of time.
% Hence, it is a good idea to make these calculations once, save the result and reuse that.
% If a file with the correct name already exists, this is used and no calculation is done.
Ax=0;
Ay=0;
SVDx=0;
SVDy=0;

reloaded=0;
datafile=sprintf("response_matrix_svd_%s.dat",beamlinename);
fid=fopen(datafile,"r");
if (fid!=-1)
    fclose(fid);
    printf("Reusing existing file for response matrices and SVD coefficients.\n");
    load(datafile);
    reloaded=1;
end;

if (rows(Ax)!=numbpm)||(columns(Ax)!=numcorr)||(rows(Ay)!=numbpm)||(columns(Ay)!=numcorr)
    if (reloaded!=0)
        printf("Number of BPMs or correctors does not match with re-loaded matrices. Re-calculating...\n");
        datafile=sprintf("response_matrix_svd_%s_new.dat",beamlinename);
    end;
    tic
    % get response matrix
    syscall=sprintf("rm -f %s",datafile);
    system(syscall);
    Ax=placet_get_response_matrix_attribute(beamlinename,beamname,list_bpm,"x",list_corr,corrxattr,"Zero");
    Ay=placet_get_response_matrix_attribute(beamlinename,beamname,list_bpm,"y",list_corr,corryattr,"Zero");
    toc
    tic
    %for correction using SVD
    [UxT,Sxinv,Vx]=mysvd(Ax,1);
    [UyT,Syinv,Vy]=mysvd(Ay,1);
    SVDx=Vx*Sxinv*UxT;
    SVDy=Vy*Syinv*UyT;
    save("-text",datafile,"Ax","Ay","SVDx","SVDy");
    toc
end;

printf("Starting correction of %d misalignment sets.\n",numberofsets);
for nset=1:numberofsets
  printf("Initializing beam line (Set %d of %d).\n",nset,numberofsets);
  % set everything to zero, i.e. start from perfect machine
  placet_element_set_attribute(beamlinename,list_corr,corrxattr,zeros(numcorr,1));
  placet_element_set_attribute(beamlinename,list_corr,corryattr,zeros(numcorr,1));
  
  corr_sx_initial=zeros(numcorr,1);
  corr_sy_initial=zeros(numcorr,1);

  [emitperfect,beamperfect]=placet_test_no_correction(beamlinename,beamname,"Zero");

  % induce errors, i.e. misalignment and strength errors, and set bpm resolution
  induce_beamline_errors(beamlinename,...
    error_cavity_pos,error_cavity_gradient_rel,...
    error_bend_pos,error_bend_strength_rel,...
    error_quadrupole_pos,error_quadrupole_strength_rel,...
    error_multipole_pos,error_multipole_strength_rel,...
    error_bpm_pos,bpm_resolution,keepolderrors);

  [emituncorrected,beamuncorrected]=placet_test_no_correction(beamlinename,beamname,"None");
  bpm_x_uncorrected=placet_element_get_attribute(beamlinename,list_bpm,"reading_x");
  bpm_y_uncorrected=placet_element_get_attribute(beamlinename,list_bpm,"reading_y");

  if strcmpi(correctiontype,"svd")
    trajectory_correction_svd(beamlinename,beamname,...
                              list_bpm,list_corr,corrxattr,corryattr,SVDx,SVDy);
  elseif strcmpi(correctiontype,"1to1")
    trajectory_correction_1to1(beamlinename,beamname,...
                              list_bpm,list_corr,corrxattr,corryattr,Ax,Ay);
  end;

  % do tracking without changing survey
  [emitfinal,beamfinal]=placet_test_no_correction(beamlinename,beamname,"None");
  
  bpm_x_final=placet_element_get_attribute(beamlinename,list_bpm,"reading_x");
  bpm_y_final=placet_element_get_attribute(beamlinename,list_bpm,"reading_y");
  
  corr_sx_final=placet_element_get_attribute(beamlinename,list_corr,corrxattr);
  corr_sy_final=placet_element_get_attribute(beamlinename,list_corr,corryattr);

  % dump data to files
  fid=fopen(bpmfilename,'at');
  fprintf(fid,"# Set %d\n",nset);
  for i=1:numbpm
    fprintf(fid,'%22.14e %22.14e %22.14e %22.14e\n',bpm_x_uncorrected(i),bpm_x_final(i),bpm_y_uncorrected(i),bpm_y_final(i));
  end;
  fclose(fid);

  fid=fopen(correctorsfilename,'at');
  fprintf(fid,"# Set %d\n",nset);
  for i=1:numcorr
    fprintf(fid,'%22.14e %22.14e %22.14e %22.14e\n',corr_sx_initial(i),corr_sx_final(i),corr_sy_initial(i),corr_sy_final(i));
  end;
  fclose(fid);


  beamuncorrected=units_placet_to_std(beamuncorrected);
  w=ones(length(beamuncorrected),1);
  [xc,xpc,yc,ypc,sc,dec,wc]=clear_outliers_6d(beamuncorrected,w);
  beamuncorrectedcore=[dec,xc,yc,sc,xpc,ypc];
  gammauncorrected=mean(beamuncorrected(:,1),1)/(0.5109989*1e6);
  gammauncorrectedcore=mean(beamuncorrectedcore(:,1),1)/(0.5109989*1e6);

  beamfinal=units_placet_to_std(beamfinal);
  w=ones(length(beamfinal),1);
  [xc,xpc,yc,ypc,sc,dec,wc]=clear_outliers_6d(beamfinal,w);
  beamfinalcore=[dec,xc,yc,sc,xpc,ypc];
  gammafinal=mean(beamfinal(:,1),1)/(0.5109989*1e6);
  gammafinalcore=mean(beamfinalcore(:,1),1)/(0.5109989*1e6);
  
  beamperfect=units_placet_to_std(beamperfect);
  w=ones(length(beamperfect),1);
  [xc,xpc,yc,ypc,sc,dec,wc]=clear_outliers_6d(beamperfect,w);
  beamperfectcore=[dec,xc,yc,sc,xpc,ypc];
  gammaperfect=mean(beamperfect(:,1),1)/(0.5109989*1e6);
  gammaperfectcore=mean(beamperfectcore(:,1),1)/(0.5109989*1e6);
  
  fid=fopen(statfilename,'at');
  fprintf(fid,"# Set %d\n",nset);
  fprintf(fid,"%22.14e %22.14e %22.14e %22.14e ",
          std(bpm_x_uncorrected,1),mean(bpm_x_uncorrected,1),
          gammauncorrected*emitproj(beamuncorrected(:,2),beamuncorrected(:,5),1),
          gammauncorrectedcore*emitproj(beamuncorrectedcore(:,2),beamuncorrectedcore(:,5),1));
  fprintf(fid,"%22.14e %22.14e %22.14e %22.14e %22.14e %22.14e ",
          std(bpm_x_final,1),mean(bpm_x_final,1),
          gammafinal*emitproj(beamfinal(:,2),beamfinal(:,5),1),
          gammafinalcore*emitproj(beamfinalcore(:,2),beamfinalcore(:,5),1),
          gammaperfect*emitproj(beamperfect(:,2),beamperfect(:,5),1),
          gammaperfectcore*emitproj(beamperfectcore(:,2),beamperfectcore(:,5),1));
  fprintf(fid,"%22.14e %22.14e %22.14e %22.14e ",
          std(bpm_y_uncorrected,1),mean(bpm_y_uncorrected,1),
          gammauncorrected*emitproj(beamuncorrected(:,3),beamuncorrected(:,6),1),
          gammauncorrectedcore*emitproj(beamuncorrectedcore(:,3),beamuncorrectedcore(:,6),1));
  fprintf(fid,"%22.14e %22.14e %22.14e %22.14e %22.14e %22.14e\n",
          std(bpm_y_final,1),mean(bpm_y_final,1),
          gammafinal*emitproj(beamfinal(:,3),beamfinal(:,6),1),
          gammafinalcore*emitproj(beamfinalcore(:,3),beamfinalcore(:,6),1),
          gammaperfect*emitproj(beamperfect(:,3),beamperfect(:,6),1),
          gammaperfectcore*emitproj(beamperfectcore(:,3),beamperfectcore(:,6),1));
  fclose(fid);
end;
endfunction;
%----------------------------------------


%----------------------------------------
% calculates the required corrector settings using SVD, i.e. global correction
function trajectory_correction_svd(beamlinename,beamname,list_bpm,list_corr,corrxattr,corryattr,SVDx,SVDy)

% one could iterate...
nimax=1;
for ni=1:nimax
    [emit,beaminitial]=placet_test_no_correction(beamlinename,beamname,"None");

    bpm_x_initial=(placet_element_get_attribute(beamlinename,list_bpm,"reading_x"))';
    bpm_y_initial=(placet_element_get_attribute(beamlinename,list_bpm,"reading_y"))';

    new_strength_x=-SVDx*bpm_x_initial;
    new_strength_y=-SVDy*bpm_y_initial;

    placet_element_vary_attribute(beamlinename,list_corr,corrxattr,new_strength_x);
    placet_element_vary_attribute(beamlinename,list_corr,corryattr,new_strength_y);
end;

endfunction;
%----------------------------------------


%----------------------------------------
% calculates the required corrector settings using 1-to-1 steering, i.e. local correction
function trajectory_correction_1to1(beamlinename,beamname,list_bpm,list_corr,corrxattr,corryattr,Ax,Ay)

% one could iterate...
nimax=3;
for ni=1:nimax
    [emit,beaminitial]=placet_test_no_correction(beamlinename,beamname,"None");

    bpm_x_initial=(placet_element_get_attribute(beamlinename,list_bpm,"reading_x"))';
    bpm_y_initial=(placet_element_get_attribute(beamlinename,list_bpm,"reading_y"))';

    new_strength_x=-Ax\bpm_x_initial;
    new_strength_y=-Ay\bpm_y_initial;

    placet_element_vary_attribute(beamlinename,list_corr,corrxattr,new_strength_x);
    placet_element_vary_attribute(beamlinename,list_corr,corryattr,new_strength_y);
end;

endfunction;
%----------------------------------------


%----------------------------------------
function [UT,Sinv,V]=mysvd(A,sd)

[U,S,V]=svd(A);

% octave svd returns simplified matrices missing rows and columns, which only contain zeros
sizeU=size(U);
sizeA=size(A);
if (sizeU(1)<sizeA(1))
  U=[U;zeros(sizeA(1)-sizeU(1),sizeU(2))];
end;
if (sizeU(2)<sizeA(2))
  U=[U,zeros(sizeU(1),sizeA(2)-sizeU(2))];
end;
sizeS=size(S);
if (sizeS(1)<sizeA(2))
  S=[S;zeros(sizeA(2)-sizeS(1),sizeS(2))];
end;
if (sizeS(2)<sizeA(2))
  S=[S,zeros(sizeS(1),sizeA(2)-sizeS(2))];
end;

if sd==1
  for i=1:length(S)
    if S(i,i)<0.00001
      S(i,i)=0.0;
    end;
  end;
end;


Sinv=pinv(S);
UT=U';

endfunction;
%----------------------------------------
