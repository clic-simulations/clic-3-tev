% some functions to calculate corrector settings
% i.e. dipole corrector strengths, quad positions,...
%
% errors are also applied here

%----------------------------------------
function [emitfinal,beamfinal]=trajectory_correction(beamlinename,beamname,correctiontype,correctortype,numberofsets,...
          bpmfilename,correctorsfilename,statfilename,...
          error_cavity_pos,error_cavity_gradient_rel,...
          error_bend_pos,error_bend_strength_rel,...
          error_quadrupole_pos,error_quadrupole_strength_rel,...
          error_multipole_pos,error_multipole_strength_rel,...
          error_bpm_pos,bpm_resolution,keepolderrors)

%bpmfilename=[bpmfilename,"_",beamlinename,".txt"];
%correctorsfilename=[correctorsfilename,"_",beamlinename,".txt"];
%statfilename=[statfilename,"_",beamlinename,".txt"];

format long;
%scriptdir="./Scripts/";
%source([scriptdir,"octave_beam_statistics.m"]);
%

% some initialisations
if strcmpi(correctortype,"dipole")
  corrxattr="strength_x";
  corryattr="strength_y";
elseif strcmpi(correctortype,"quadrupole")
  corrxattr="x";
  corryattr="y";
else
  correctortype="quadrupole";
  corrxattr="x";
  corryattr="y";
end;

fid=fopen(bpmfilename,"wt");
fprintf(fid,"#BPM readings (in micrometer)\n");
fprintf(fid,"#%21s %22s %22s %22s %22s %22s %22s %22s\n",
            "std BPM X initial","std BPM X final","mean BPM X initial","mean BPM X final",
            "std BPM Y initial","std BPM Y final","mean BPM Y initial","mean BPM Y final");
fclose(fid);

fid=fopen(correctorsfilename,"wt");
if strcmpi(correctortype,"dipole")
  fprintf(fid,"#Corrector settings (in GeV/micrometer)\n");
  fprintf(fid,"#%21s %22s %22s %22s %22s %22s %22s %22s\n",
              "std strength X ini","std strength X fin","mean strength X ini","mean strength X fin",
              "std strenght Y ini","std strength Y fin","mean strength Y ini","mean strength Y fin");
end;
if strcmpi(correctortype,"quadrupole")
  fprintf(fid,"#Corrector settings (in micrometer)\n");
  fprintf(fid,"#%21s %22s %22s %22s %22s %22s %22s %22s\n",
              "std offset X ini","std offset X fin","mean offset X ini","mean offset X fin",
              "std offset Y ini","std offset Y fin","mean offset Y ini","mean offset Y fin");
end;
fclose(fid);

fid=fopen(statfilename,"wt");
fprintf(fid,"# bunch train data\n");
fprintf(fid,"# %18s %20s %20s %20s %20s %20s %20s %20s ",
            "semit nx [nm rad]","semit nx [nm rad]","temit nx [nm rad]","temit nx [nm rad]",
            "semit ny [nm rad]","semit ny [nm rad]","temit ny [nm rad]","temit ny [nm rad]");
fprintf(fid,"%20s %20s %20s %20s %20s %20s %20s %20s %20s %20s %20s %20s ",
            "mean x [um]","mean x [um]","std x [um]","std x [um]","mean sx [um]","mean sx [um]",
            "mean xp [urad]","mean xp [urad]","std xp [urad]","std xp [urad]","mean sxp [um]","mean sxp [um]");
fprintf(fid,"%20s %20s %20s %20s %20s %20s %20s %20s %20s %20s %20s %20s ",
            "mean y [um]","mean y [um]","std y [um]","std y [um]","mean sy [um]","mean sy [um]",
            "mean yp [urad]","mean yp [urad]","std yp [urad]","std yp [urad]","mean syp [um]","mean syp [um]");
fprintf(fid,"%20s %20s %20s %20s %20s %20s %20s %20s %20s %20s\n",
            "mean s [um]","mean s [um]","std s [um]","std s [um]",
            "mean e [GeV]","mean e [GeV]","std e [GeV]","std e [GeV]","dee [1]","dee [1]");

fprintf(fid,"# %18s %20s %20s %20s %20s %20s %20s %20s ",
            "ini","fin","ini","fin","ini","fin","ini","fin");
fprintf(fid,"%20s %20s %20s %20s %20s %20s %20s %20s %20s %20s %20s %20s ",
            "ini","fin","ini","fin","ini","fin","ini","fin","ini","fin","ini","fin");
fprintf(fid,"%20s %20s %20s %20s %20s %20s %20s %20s %20s %20s %20s %20s ",
            "ini","fin","ini","fin","ini","fin","ini","fin","ini","fin","ini","fin");
fprintf(fid,"%20s %20s %20s %20s %20s %20s %20s %20s %20s %20s\n",
            "ini","fin","ini","fin","ini","fin","ini","fin","ini","fin");
fclose(fid);




% get lists of bpms and correctors
%
list_bpm=placet_get_number_list(beamlinename,"bpm");
list_corr=placet_get_number_list(beamlinename,correctortype);

numbpm=length(list_bpm);
numcorr=length(list_corr);

if (numbpm==0)||(numcorr==0)
  printf("No correctors or BPMs. Tracking will be performed without corrections.\n");
  [emitfinal,beamfinal]=placet_test_no_correction(beamlinename,beamname,"None");
  return;
end;

printf("Initializing %s correction...\n",correctiontype);

% Getting response matrices and calculating SVD matrices can take a rediculous amount of time.
% Hence, it is a good idea to make these calculations once, save the result and reuse that.
% If a file with the correct name already exists, this is used and no calculation is done.
Ax=0;
Ay=0;
SVDx=0;
SVDy=0;

reloaded=0;
datafile=sprintf("response_matrix_%s.dat",beamlinename);
fid=fopen(datafile,"r");
if (fid!=-1)
    fclose(fid);
    printf("Reusing existing file for response matrices and SVD coefficients.\n");
    load(datafile);
    reloaded=1;
end;

if (rows(Ax)!=numbpm)||(columns(Ax)!=numcorr)||(rows(Ay)!=numbpm)||(columns(Ay)!=numcorr)
    if (reloaded!=0)
        printf("Number of BPMs or correctors does not match with re-loaded matrices. Re-calculating...\n");
        datafile=sprintf("response_matrix_%s_new.dat",beamlinename);
    end;
    tic
    % get response matrix
    syscall=sprintf("rm -f %s",datafile);
    system(syscall);
    Ax=placet_get_response_matrix_attribute(beamlinename,beamname,list_bpm,"x",list_corr,corrxattr,"Zero");
    Ay=placet_get_response_matrix_attribute(beamlinename,beamname,list_bpm,"y",list_corr,corryattr,"Zero");
    toc
    tic
    %for correction using SVD
    [UxT,Sxinv,Vx]=mysvd(Ax,1);
    [UyT,Syinv,Vy]=mysvd(Ay,1);
    SVDx=Vx*Sxinv*UxT;
    SVDy=Vy*Syinv*UyT;
    save("-text",datafile,"Ax","Ay","SVDx","SVDy");
    toc
end;

pinvAx=pinv(Ax);
pinvAy=pinv(Ay);

if strcmpi(correctiontype,"dispfree")
    Ax_dep=0;
    Ay_dep=0;
    Ax_dem=0;
    Ay_dem=0;

    reloaded=0;
    datafile=sprintf("response_matrix_de_%s.dat",beamlinename);
    fid=fopen(datafile,"r");
    if (fid!=-1)
        fclose(fid);
        printf("Reusing existing file for response matrices of energy offset beams.\n");
        load(datafile);
        reloaded=1;
    end;

    if (rows(Ax_dep)!=numbpm)||(columns(Ax_dep)!=numcorr)||(rows(Ay_dep)!=numbpm)||(columns(Ay_dep)!=numcorr)||...
       (rows(Ax_dem)!=numbpm)||(columns(Ax_dem)!=numcorr)||(rows(Ay_dem)!=numbpm)||(columns(Ay_dem)!=numcorr)
        if (reloaded!=0)
            printf("Number of BPMs or correctors does not match with re-loaded matrices. Re-calculating...\n");
            datafile=sprintf("response_matrix_de_%s_new.dat",beamlinename);
        end;
        % get response matrix
        syscall=sprintf("rm -f %s",datafile);
        system(syscall);
        beamname_dep=sprintf("%s_dep",beamname);
        beamname_dem=sprintf("%s_dem",beamname);
        Ax_dep=placet_get_response_matrix_attribute(beamlinename,beamname_dep,list_bpm,"x",list_corr,corrxattr,"Zero")-Ax;
        Ay_dep=placet_get_response_matrix_attribute(beamlinename,beamname_dep,list_bpm,"y",list_corr,corryattr,"Zero")-Ay;
        Ax_dem=placet_get_response_matrix_attribute(beamlinename,beamname_dem,list_bpm,"x",list_corr,corrxattr,"Zero")-Ax;
        Ay_dem=placet_get_response_matrix_attribute(beamlinename,beamname_dem,list_bpm,"y",list_corr,corryattr,"Zero")-Ay;
        save("-text",datafile,"Ax_dep","Ay_dep","Ax_dem","Ay_dem");
    end;

    wgt=1;
    wgt_dep=150;
    wgt_dem=150;
    Ax_tot=[wgt*Ax;wgt_dep*Ax_dep;wgt_dem*Ax_dem];
    Ay_tot=[wgt*Ay;wgt_dep*Ay_dep;wgt_dem*Ay_dem];
    pinvAx_tot=pinv(Ax_tot);
    pinvAy_tot=pinv(Ay_tot);
    %for correction using SVD
    [UxT,Sxinv,Vx]=mysvd(Ax_tot,1);
    [UyT,Syinv,Vy]=mysvd(Ay_tot,1);
    SVDx_tot=Vx*Sxinv*UxT;
    SVDy_tot=Vy*Syinv*UyT;
end;



printf("Starting correction of %d misalignment sets.\n",numberofsets);
for nset=1:numberofsets
  printf("Initializing beam line (Set %d of %d).\n",nset,numberofsets);
  % set everything to zero, i.e. start from perfect machine
  placet_element_set_attribute(beamlinename,list_corr,corrxattr,zeros(numcorr,1));
  placet_element_set_attribute(beamlinename,list_corr,corryattr,zeros(numcorr,1));

  corr_sx_initial=zeros(numcorr,1);
  corr_sy_initial=zeros(numcorr,1);

  % induce errors, i.e. misalignment and strength errors, and set bpm resolution
  induce_beamline_errors(beamlinename,...
    error_cavity_pos,error_cavity_gradient_rel,...
    error_bend_pos,error_bend_strength_rel,...
    error_quadrupole_pos,error_quadrupole_strength_rel,...
    error_multipole_pos,error_multipole_strength_rel,...
    error_bpm_pos,bpm_resolution,keepolderrors);

  [emituncorrected,beamuncorrected]=placet_test_no_correction(beamlinename,beamname,"None");
  bpm_x_uncorrected=placet_element_get_attribute(beamlinename,list_bpm,"reading_x");
  bpm_y_uncorrected=placet_element_get_attribute(beamlinename,list_bpm,"reading_y");

  if strcmpi(correctiontype,"svd")
    trajectory_correction_svd(beamlinename,beamname,...
                              list_bpm,list_corr,corrxattr,corryattr,SVDx,SVDy);
  elseif strcmpi(correctiontype,"1to1")
    trajectory_correction_1to1(beamlinename,beamname,...
                               list_bpm,list_corr,corrxattr,corryattr,pinvAx,pinvAy);
  elseif strcmpi(correctiontype,"dispfree")
%    trajectory_correction_1to1(beamlinename,beamname,...
%                               list_bpm,list_corr,corrxattr,corryattr,pinvAx,pinvAy);

%    trajectory_correction_disp_free(beamlinename,beamname,...
%                                    list_bpm,list_corr,corrxattr,corryattr,pinvAx_tot,pinvAy_tot,wgt,wgt_dep,wgt_dem);
    trajectory_correction_disp_free_svd(beamlinename,beamname,...
                                    list_bpm,list_corr,corrxattr,corryattr,SVDx_tot,SVDy_tot,wgt,wgt_dep,wgt_dem);
  end;

  % do tracking without changing survey
  [emitfinal,beamfinal]=placet_test_no_correction(beamlinename,beamname,"None");
  
  bpm_x_final=placet_element_get_attribute(beamlinename,list_bpm,"reading_x");
  bpm_y_final=placet_element_get_attribute(beamlinename,list_bpm,"reading_y");

  corr_sx_final=placet_element_get_attribute(beamlinename,list_corr,corrxattr);
  corr_sy_final=placet_element_get_attribute(beamlinename,list_corr,corryattr);

  % dump data to files
  fid=fopen(bpmfilename,"at");
  fprintf(fid,"# Set %d\n",nset);
  fprintf(fid,"%22.14e %22.14e %22.14e %22.14e %22.14e %22.14e %22.14e %22.14e\n",
              std(bpm_x_uncorrected,1),std(bpm_x_final,1),
              mean(bpm_x_uncorrected,1),mean(bpm_x_final,1),
              std(bpm_y_uncorrected,1),std(bpm_y_final,1),
              mean(bpm_y_uncorrected,1),mean(bpm_y_final,1));
  fclose(fid);

  fid=fopen(correctorsfilename,"at");
  fprintf(fid,"# Set %d\n",nset);
  fprintf(fid,"%22.14e %22.14e %22.14e %22.14e %22.14e %22.14e %22.14e %22.14e\n",
              std(corr_sx_initial,1),std(corr_sx_final,1),
              mean(corr_sx_initial,1),mean(corr_sx_final,1),
              std(corr_sy_initial,1),std(corr_sy_final,1),
              mean(corr_sy_initial,1),mean(corr_sy_final,1));
  fclose(fid);

  % statistics of full bunch train
  bi=beamuncorrected;
  bf=beamfinal;

  mean_x_ini=mean(bi(:,4),bi(:,2));
  mean_x_fin=mean(bf(:,4),bf(:,2));
  mean_xp_ini=mean(bi(:,5),bi(:,2));
  mean_xp_fin=mean(bf(:,5),bf(:,2));

  mean_y_ini=mean(bi(:,6),bi(:,2));
  mean_y_fin=mean(bf(:,6),bf(:,2));
  mean_yp_ini=mean(bi(:,7),bi(:,2));
  mean_yp_fin=mean(bf(:,7),bf(:,2));

  std_x_ini=std(bi(:,4),bi(:,2));
  std_x_fin=std(bf(:,4),bf(:,2));
  std_xp_ini=std(bi(:,5),bi(:,2));
  std_xp_fin=std(bf(:,5),bf(:,2));

  std_y_ini=std(bi(:,6),bi(:,2));
  std_y_fin=std(bf(:,6),bf(:,2));
  std_yp_ini=std(bi(:,7),bi(:,2));
  std_yp_fin=std(bf(:,7),bf(:,2));

  mean_sx_ini=mean(sqrt(bi(:,8)),bi(:,2));
  mean_sx_fin=mean(sqrt(bf(:,8)),bf(:,2));
  mean_sxp_ini=mean(sqrt(bi(:,10)),bi(:,2));
  mean_sxp_fin=mean(sqrt(bf(:,10)),bf(:,2));

  mean_sy_ini=mean(sqrt(bi(:,11)),bi(:,2));
  mean_sy_fin=mean(sqrt(bf(:,11)),bf(:,2));
  mean_syp_ini=mean(sqrt(bi(:,13)),bi(:,2));
  mean_syp_fin=mean(sqrt(bf(:,13)),bf(:,2));

  mean_s_ini=mean(bi(:,1),bi(:,2));
  mean_s_fin=mean(bf(:,1),bf(:,2));
  mean_e_ini=mean(bi(:,3),bi(:,2));
  mean_e_fin=mean(bf(:,3),bf(:,2));

  std_s_ini=std(bi(:,1),bi(:,2));
  std_s_fin=std(bf(:,1),bf(:,2));
  std_e_ini=std(bi(:,3),bi(:,2));
  std_e_fin=std(bf(:,3),bf(:,2));

  dee_ini=std_e_ini/mean_e_ini;
  dee_fin=std_e_fin/mean_e_fin;

  bi(:,4)=bi(:,4)-mean(bi(:,4),bi(:,2));
  bi(:,5)=bi(:,5)-mean(bi(:,5),bi(:,2));
  bi(:,6)=bi(:,6)-mean(bi(:,6),bi(:,2));
  bi(:,7)=bi(:,7)-mean(bi(:,7),bi(:,2));
  bf(:,4)=bf(:,4)-mean(bf(:,4),bf(:,2));
  bf(:,5)=bf(:,5)-mean(bf(:,5),bf(:,2));
  bf(:,6)=bf(:,6)-mean(bf(:,6),bf(:,2));
  bf(:,7)=bf(:,7)-mean(bf(:,7),bf(:,2));

%  macroemit_x_ini=sqrt(bi(:,8).*bi(:,10) - bi(:,9).^2);
%  macroemit_x_fin=sqrt(bf(:,8).*bf(:,10) - bf(:,9).^2);

%  macroemit_y_ini=sqrt(bi(:,11).*bi(:,13) - bi(:,12).^2);
%  macroemit_y_fin=sqrt(bf(:,11).*bf(:,13) - bf(:,12).^2);

  old_s=bi(1,1);
  c=1;
  r_start=1;
  for i=1:rows(bi)
    s=bi(i,1);
    if (s!=old_s)||(i==rows(bi))
      r_end=i-1;
      if i==rows(bi)
        r_end=i;
      end;
      
      bip=bi(r_start:r_end,:);
      bfp=bf(r_start:r_end,:);

%      sce_x_ini(c)=sqrt(mean(bip(:,4).^2,bip(:,2))*mean(bip(:,5).^2,bip(:,2)) - mean(bip(:,4).*bip(:,5),bip(:,2))^2);
%      sce_x_fin(c)=sqrt(mean(bfp(:,4).^2,bfp(:,2))*mean(bfp(:,5).^2,bfp(:,2)) - mean(bfp(:,4).*bfp(:,5),bfp(:,2))^2);
%      sce_y_ini(c)=sqrt(mean(bip(:,6).^2,bip(:,2))*mean(bip(:,7).^2,bip(:,2)) - mean(bip(:,6).*bip(:,7),bip(:,2))^2);
%      sce_y_fin(c)=sqrt(mean(bfp(:,6).^2,bfp(:,2))*mean(bfp(:,7).^2,bfp(:,2)) - mean(bfp(:,6).*bfp(:,7),bfp(:,2))^2);
      
%      sme_x_ini(c)=sqrt(mean(bip(:,8),bip(:,2))*mean(bip(:,10),bip(:,2)) - mean(bip(:,9),bip(:,2))^2);
%      sme_x_fin(c)=sqrt(mean(bfp(:,8),bfp(:,2))*mean(bfp(:,10),bfp(:,2)) - mean(bfp(:,9),bfp(:,2))^2);
%      sme_y_ini(c)=sqrt(mean(bip(:,11),bip(:,2))*mean(bip(:,13),bip(:,2)) - mean(bip(:,12),bip(:,2))^2);
%      sme_y_fin(c)=sqrt(mean(bfp(:,11),bfp(:,2))*mean(bfp(:,13),bfp(:,2)) - mean(bfp(:,12),bfp(:,2))^2);

       se_x_ini(c)=sqrt(mean(bip(:,8)+bip(:,4).^2,bip(:,2))*mean(bip(:,10)+bip(:,5).^2,bip(:,2)) -...
                        mean(bip(:,9)+bip(:,4).*bip(:,5),bip(:,2))^2);
       se_x_fin(c)=sqrt(mean(bfp(:,8)+bfp(:,4).^2,bfp(:,2))*mean(bfp(:,10)+bfp(:,5).^2,bfp(:,2)) -...
                        mean(bfp(:,9)+bfp(:,4).*bfp(:,5),bfp(:,2))^2);
       se_y_ini(c)=sqrt(mean(bip(:,11)+bip(:,6).^2,bip(:,2))*mean(bip(:,13)+bip(:,7).^2,bip(:,2)) -...
                        mean(bip(:,12)+bip(:,6).*bip(:,7),bip(:,2))^2);
       se_y_fin(c)=sqrt(mean(bfp(:,11)+bfp(:,6).^2,bfp(:,2))*mean(bfp(:,13)+bfp(:,7).^2,bfp(:,2)) -...
                        mean(bfp(:,12)+bfp(:,6).*bfp(:,7),bfp(:,2))^2);

      s_weight_ini(c)=sum(bip(:,2));
      s_weight_fin(c)=sum(bfp(:,2));

      old_s=s;
      r_start=i;
      c=c+1;
    end;
  end;

%  se_x_ini=mean(bi(:,3),bi(:,2))/0.0005109989.*((1*sce_x_ini.^1+sme_x_ini.^1))/1000;
%  se_x_fin=mean(bf(:,3),bf(:,2))/0.0005109989.*((1*sce_x_fin.^1+sme_x_fin.^1))/1000;
%  se_y_ini=mean(bi(:,3),bi(:,2))/0.0005109989.*((1*sce_y_ini.^1+sme_y_ini.^1))/1000;
%  se_y_fin=mean(bf(:,3),bf(:,2))/0.0005109989.*((1*sce_y_fin.^1+sme_y_fin.^1))/1000;

  sliceemit_x_ini=mean(bip(:,3),bip(:,2))/0.0005109989*mean(se_x_ini,s_weight_ini)/1000;
  sliceemit_x_fin=mean(bfp(:,3),bfp(:,2))/0.0005109989*mean(se_x_fin,s_weight_fin)/1000;
  sliceemit_y_ini=mean(bip(:,3),bip(:,2))/0.0005109989*mean(se_y_ini,s_weight_ini)/1000;
  sliceemit_y_fin=mean(bfp(:,3),bfp(:,2))/0.0005109989*mean(se_y_fin,s_weight_fin)/1000;

%  sliceemit_x_ini=mean(bi(:,3),bi(:,2))/0.0005109989*...
%                  sqrt(mean(bi(:,8),bi(:,2))*mean(bi(:,10),bi(:,2)) - mean(bi(:,9),bi(:,2))^2)/1000;
%  sliceemit_x_fin=mean(bf(:,3),bf(:,2))/0.0005109989*...
%                  sqrt(mean(bf(:,8),bf(:,2))*mean(bf(:,10),bf(:,2)) - mean(bf(:,9),bf(:,2))^2)/1000;
%  sliceemit_y_ini=mean(bi(:,3),bi(:,2))/0.0005109989*...
%                  sqrt(mean(bi(:,11),bi(:,2))*mean(bi(:,13),bi(:,2)) - mean(bi(:,12),bi(:,2))^2)/1000;
%  sliceemit_y_fin=mean(bf(:,3),bf(:,2))/0.0005109989*...
%                  sqrt(mean(bf(:,11),bf(:,2))*mean(bf(:,13),bf(:,2)) - mean(bf(:,12),bf(:,2))^2)/1000;

  totalemit_x_ini=mean(bi(:,3),bi(:,2))/0.0005109989*...
                  sqrt(mean(bi(:,8)+bi(:,4).^2,bi(:,2))*mean(bi(:,10)+bi(:,5).^2,bi(:,2)) -...
                       mean(bi(:,9)+bi(:,4).*bi(:,5),bi(:,2))^2)/1000;
  totalemit_x_fin=mean(bf(:,3),bf(:,2))/0.0005109989*...
                  sqrt(mean(bf(:,8)+bf(:,4).^2,bf(:,2))*mean(bf(:,10)+bf(:,5).^2,bf(:,2)) -...
                       mean(bf(:,9)+bf(:,4).*bf(:,5),bf(:,2))^2)/1000;
  totalemit_y_ini=mean(bi(:,3),bi(:,2))/0.0005109989*...
                  sqrt(mean(bi(:,11)+bi(:,6).^2,bi(:,2))*mean(bi(:,13)+bi(:,7).^2,bi(:,2)) -...
                       mean(bi(:,12)+bi(:,6).*bi(:,7),bi(:,2))^2)/1000;
  totalemit_y_fin=mean(bf(:,3),bf(:,2))/0.0005109989*...
                  sqrt(mean(bf(:,11)+bf(:,6).^2,bf(:,2))*mean(bf(:,13)+bf(:,7).^2,bf(:,2)) -...
                       mean(bf(:,12)+bf(:,6).*bf(:,7),bf(:,2))^2)/1000;

%  coreemit_x_ini=mean(bi(:,3),bi(:,2))/0.0005109989*...
%                 sqrt(mean(bi(:,4).^2,bi(:,2))*mean(bi(:,5).^2,bi(:,2)) - mean(bi(:,4).*bi(:,5),bi(:,2))^2)/1000;
%  coreemit_x_fin=mean(bf(:,3),bf(:,2))/0.0005109989*...
%                 sqrt(mean(bf(:,4).^2,bf(:,2))*mean(bf(:,5).^2,bf(:,2)) - mean(bf(:,4).*bf(:,5),bf(:,2))^2)/1000;
%  coreemit_y_ini=mean(bi(:,3),bi(:,2))/0.0005109989*...
%                 sqrt(mean(bi(:,6).^2,bi(:,2))*mean(bi(:,7).^2,bi(:,2)) - mean(bi(:,6).*bi(:,7),bi(:,2))^2)/1000;
%  coreemit_y_fin=mean(bf(:,3),bf(:,2))/0.0005109989*...
%                 sqrt(mean(bf(:,6).^2,bf(:,2))*mean(bf(:,7).^2,bf(:,2)) - mean(bf(:,6).*bf(:,7),bf(:,2))^2)/1000;

%  coreemit_x_ini=mean(bi(:,3),bi(:,2))/0.0005109989*...
%                 sqrt(std(bi(:,4),bi(:,2))^2*std(bi(:,5),bi(:,2))^2 - mean(bi(:,4).*bi(:,5),bi(:,2))^2)/1000;
%  coreemit_x_fin=mean(bf(:,3),bf(:,2))/0.0005109989*...
%                 sqrt(std(bf(:,4),bf(:,2))^2*std(bf(:,5),bf(:,2))^2 - mean(bf(:,4).*bf(:,5),bf(:,2))^2)/1000;
%  coreemit_y_ini=mean(bi(:,3),bi(:,2))/0.0005109989*...
%                 sqrt(std(bi(:,6),bi(:,2))^2*std(bi(:,7),bi(:,2))^2 - mean(bi(:,6).*bi(:,7),bi(:,2))^2)/1000;
%  coreemit_y_fin=mean(bf(:,3),bf(:,2))/0.0005109989*...
%                 sqrt(std(bf(:,6),bf(:,2))^2*std(bf(:,7),bf(:,2))^2 - mean(bf(:,6).*bf(:,7),bf(:,2))^2)/1000;

  fid=fopen(statfilename,"at");
  if (fid!=-1)
    fprintf(fid,"%20.12e %20.12e %20.12e %20.12e %20.12e %20.12e %20.12e %20.12e ",
            sliceemit_x_ini,sliceemit_x_fin,totalemit_x_ini,totalemit_x_fin,
            sliceemit_y_ini,sliceemit_y_fin,totalemit_y_ini,totalemit_y_fin);
    fprintf(fid,"%20.12e %20.12e %20.12e %20.12e %20.12e %20.12e %20.12e %20.12e %20.12e %20.12e %20.12e %20.12e ",
            mean_x_ini,mean_x_fin,std_x_ini,std_x_fin,mean_sx_ini,mean_sx_fin,
            mean_xp_ini,mean_xp_fin,std_xp_ini,std_xp_fin,mean_sxp_ini,mean_sxp_fin);
    fprintf(fid,"%20.12e %20.12e %20.12e %20.12e %20.12e %20.12e %20.12e %20.12e %20.12e %20.12e %20.12e %20.12e ",
            mean_y_ini,mean_y_fin,std_y_ini,std_y_fin,mean_sy_ini,mean_sy_fin,
            mean_yp_ini,mean_yp_fin,std_yp_ini,std_yp_fin,mean_syp_ini,mean_syp_fin);
    fprintf(fid,"%20.12e %20.12e %20.12e %20.12e %20.12e %20.12e %20.12e %20.12e %20.12e %20.12e\n",
            mean_s_ini,mean_s_fin,std_s_ini,std_s_fin,mean_e_ini,mean_e_fin,std_e_ini,std_e_fin,dee_ini,dee_fin);
    fclose(fid);
  end;

end;
endfunction;
%----------------------------------------


%----------------------------------------
% calculates the required corrector settings using SVD, i.e. global correction
function trajectory_correction_svd(beamlinename,beamname,list_bpm,list_corr,corrxattr,corryattr,SVDx,SVDy)

% one could iterate...
nimax=3;
for ni=1:nimax
    [emit,beaminitial]=placet_test_no_correction(beamlinename,beamname,"None");

    bpm_x_initial=(placet_element_get_attribute(beamlinename,list_bpm,"reading_x"))';
    bpm_y_initial=(placet_element_get_attribute(beamlinename,list_bpm,"reading_y"))';

    new_strength_x=-SVDx*bpm_x_initial;
    new_strength_y=-SVDy*bpm_y_initial;

    placet_element_vary_attribute(beamlinename,list_corr,corrxattr,new_strength_x);
    placet_element_vary_attribute(beamlinename,list_corr,corryattr,new_strength_y);
end;

endfunction;
%----------------------------------------


%----------------------------------------
% calculates the required corrector settings using 1-to-1 steering, i.e. local correction
function trajectory_correction_1to1(beamlinename,beamname,list_bpm,list_corr,corrxattr,corryattr,pinvAx,pinvAy)

% one could iterate...
nimax=3;
for ni=1:nimax
    [emit,beaminitial]=placet_test_no_correction(beamlinename,beamname,"None");

    bpm_x_initial=(placet_element_get_attribute(beamlinename,list_bpm,"reading_x"))';
    bpm_y_initial=(placet_element_get_attribute(beamlinename,list_bpm,"reading_y"))';

%    new_strength_x=-Ax\bpm_x_initial;
%    new_strength_y=-Ay\bpm_y_initial;
    new_strength_x=-pinvAx*bpm_x_initial;
    new_strength_y=-pinvAy*bpm_y_initial;

    placet_element_vary_attribute(beamlinename,list_corr,corrxattr,new_strength_x);
    placet_element_vary_attribute(beamlinename,list_corr,corryattr,new_strength_y);
end;

endfunction;
%----------------------------------------


%----------------------------------------
% calculates the required corrector settings using dispersion-free steering
function trajectory_correction_disp_free(beamlinename,beamname,list_bpm,list_corr,corrxattr,corryattr,pinvAx_tot,pinvAy_tot,wgt,wgt_dep,wgt_dem)

beamname_dep=sprintf("%s_dep",beamname);
beamname_dem=sprintf("%s_dem",beamname);
% one could iterate...
nimax=3;
for ni=1:nimax
    placet_test_no_correction(beamlinename,beamname,"None");
    bpm_x_ini=(placet_element_get_attribute(beamlinename,list_bpm,"reading_x"))';
    bpm_y_ini=(placet_element_get_attribute(beamlinename,list_bpm,"reading_y"))';

    placet_test_no_correction(beamlinename,beamname_dep,"None");
    bpm_x_ini_dep=(placet_element_get_attribute(beamlinename,list_bpm,"reading_x"))'-bpm_x_ini;
    bpm_y_ini_dep=(placet_element_get_attribute(beamlinename,list_bpm,"reading_y"))'-bpm_y_ini;

    placet_test_no_correction(beamlinename,beamname_dem,"None");
    bpm_x_ini_dem=(placet_element_get_attribute(beamlinename,list_bpm,"reading_x"))'-bpm_x_ini;
    bpm_y_ini_dem=(placet_element_get_attribute(beamlinename,list_bpm,"reading_y"))'-bpm_y_ini;

    bpm_x_tot=[wgt*bpm_x_ini;wgt_dep*bpm_x_ini_dep;wgt_dem*bpm_x_ini_dem];
    bpm_y_tot=[wgt*bpm_y_ini;wgt_dep*bpm_y_ini_dep;wgt_dem*bpm_y_ini_dem];

    new_strength_x=-pinvAx_tot*bpm_x_tot;
    new_strength_y=-pinvAy_tot*bpm_y_tot;

    placet_element_vary_attribute(beamlinename,list_corr,corrxattr,new_strength_x);
    placet_element_vary_attribute(beamlinename,list_corr,corryattr,new_strength_y);
end;

endfunction;
%----------------------------------------

%----------------------------------------
% calculates the required corrector settings using dispersion-free steering and SVD
function trajectory_correction_disp_free_svd(beamlinename,beamname,list_bpm,list_corr,corrxattr,corryattr,SVDx_tot,SVDy_tot,wgt,wgt_dep,wgt_dem)

beamname_dep=sprintf("%s_dep",beamname);
beamname_dem=sprintf("%s_dem",beamname);
% one could iterate...
nimax=3;
for ni=1:nimax
    placet_test_no_correction(beamlinename,beamname,"None");
    bpm_x_ini=(placet_element_get_attribute(beamlinename,list_bpm,"reading_x"))';
    bpm_y_ini=(placet_element_get_attribute(beamlinename,list_bpm,"reading_y"))';

    placet_test_no_correction(beamlinename,beamname_dep,"None");
    bpm_x_ini_dep=(placet_element_get_attribute(beamlinename,list_bpm,"reading_x"))'-bpm_x_ini;
    bpm_y_ini_dep=(placet_element_get_attribute(beamlinename,list_bpm,"reading_y"))'-bpm_y_ini;

    placet_test_no_correction(beamlinename,beamname_dem,"None");
    bpm_x_ini_dem=(placet_element_get_attribute(beamlinename,list_bpm,"reading_x"))'-bpm_x_ini;
    bpm_y_ini_dem=(placet_element_get_attribute(beamlinename,list_bpm,"reading_y"))'-bpm_y_ini;

    bpm_x_tot=[wgt*bpm_x_ini;wgt_dep*bpm_x_ini_dep;wgt_dem*bpm_x_ini_dem];
    bpm_y_tot=[wgt*bpm_y_ini;wgt_dep*bpm_y_ini_dep;wgt_dem*bpm_y_ini_dem];

    new_strength_x=-SVDx_tot*bpm_x_tot;
    new_strength_y=-SVDy_tot*bpm_y_tot;

    placet_element_vary_attribute(beamlinename,list_corr,corrxattr,new_strength_x);
    placet_element_vary_attribute(beamlinename,list_corr,corryattr,new_strength_y);
end;

endfunction;
%----------------------------------------



%----------------------------------------
function [UT,Sinv,V]=mysvd(A,sd)

[U,S,V]=svd(A);

% octave svd returns simplified matrices missing rows and columns, which only contain zeros
sizeU=size(U);
sizeA=size(A);
if (sizeU(1)<sizeA(1))
  U=[U;zeros(sizeA(1)-sizeU(1),sizeU(2))];
end;
if (sizeU(2)<sizeA(2))
  U=[U,zeros(sizeU(1),sizeA(2)-sizeU(2))];
end;
sizeS=size(S);
if (sizeS(1)<sizeA(2))
  S=[S;zeros(sizeA(2)-sizeS(1),sizeS(2))];
end;
if (sizeS(2)<sizeA(2))
  S=[S,zeros(sizeS(1),sizeA(2)-sizeS(2))];
end;

if rows(S)<columns(S)
  maxi=rows(S);
 else
  maxi=columns(S);
end;

if sd==1
  for i=1:maxi
    if S(i,i)<0.00001
      S(i,i)=0.0;
    end;
  end;
end;


Sinv=pinv(S);
UT=U';

endfunction;
%----------------------------------------
