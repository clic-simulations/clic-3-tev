#
# define beam parameters at entrances of all sections
# if tracking starts in front of a section, the corresponding
# beam parameters as defined here should be used
# these values are close to what you would get a certain position by tracking from start 
# but the values are not corrected for wake field effects etc.
# i.e. they are not necessarily what a real simulation will give
#
# beamXXX=[betax,alphax,emitnx,betay,alphay,emitny,sigmaz,charge,
#          uncespr,echirp,energy]
# units=[m,rad,m*rad,m,rad,m*rad,m,C,1,1/m,eV]
#
# Attention: before usage the parameters must be converted to Placet units
#

# at entrance of section 0010 (Matching DR to RTML)
set beam0010(betax)    20.0
set beam0010(alphax)    0.0
set beam0010(emitnx)  500.0e-9
set beam0010(betay)     5.0
set beam0010(alphay)    0.0
set beam0010(emitny)    5.0e-9
set beam0010(sigmaz) 1600.0e-6
set beam0010(meanz)     0.0e-6
set beam0010(charge)    0.65e-9
set beam0010(uncespr)   1.3e-3
set beam0010(echirp)    0.0
set beam0010(energy)    2.86e9

# at entrance of section 0020 (Diagnostics section 1)
set beam0020(betax)     1.969809853458565e+01
set beam0020(alphax)    2.002449581405269e+00
set beam0020(emitnx)  500.0e-9
set beam0020(betay)     5.081051073345858e+00
set beam0020(alphay)   -5.430805110777391e-01
set beam0020(emitny)    5.0e-9
set beam0020(sigmaz) 1600.0e-6
set beam0020(meanz)     0.0e-6
set beam0020(charge)    0.65e-9
set beam0020(uncespr)   1.3e-3
set beam0020(echirp)    0.0
set beam0020(energy)    2.86e9

# at entrance of section 0030 (Dump and match diagnostics to electron spin rotator)
set beam0030(betax)     1.969809853453056e+01
set beam0030(alphax)   -2.002449581397243e+00
set beam0030(emitnx)  500.0e-9
set beam0030(betay)     5.081051073605066e+00
set beam0030(alphay)    5.430805110912666e-01
set beam0030(emitny)    5.0e-9
set beam0030(sigmaz) 1600.0e-6
set beam0030(meanz)     0.0e-6
set beam0030(charge)    0.65e-9
set beam0030(uncespr)   1.3e-3
set beam0030(echirp)    0.0
set beam0030(energy)    2.86e9

# at entrance of e+ section 0040 (Spin rotator)
set beam0040p(betax)     1.969809853472097e+01
set beam0040p(alphax)    2.002449581415385e+00
set beam0040p(emitnx)  500.0e-9
set beam0040p(betay)     5.081051073587772e+00
set beam0040p(alphay)   -5.430805111005579e-01
set beam0040p(emitny)    5.0e-9
set beam0040p(sigmaz) 1600.0e-6
set beam0040p(meanz)     0.0e-6
set beam0040p(charge)    0.65e-9
set beam0040p(uncespr)   1.3e-3
set beam0040p(echirp)    0.0
set beam0040p(energy)    2.86e9

# at entrance of e- section 0040 (Spin rotator)
set beam0040m(betax)     3.690229192612262e+00
set beam0040m(alphax)   -3.918263062634464e-01
set beam0040m(emitnx)  500.0e-9
set beam0040m(betay)     2.474923098762032e+01
set beam0040m(alphay)    2.314900500918652e+00
set beam0040m(emitny)    5.0e-9
set beam0040m(sigmaz) 1600.0e-6
set beam0040m(meanz)     0.0e-6
set beam0040m(charge)    0.65e-9
set beam0040m(uncespr)   1.3e-3
set beam0040m(echirp)    0.0
set beam0040m(energy)    2.86e9

# at entrance of e+ section 0050 (Match sr to BC1 RF)
set beam0050p(betax)     1.969809853458251e+01
set beam0050p(alphax)   -2.002449581395221e+00
set beam0050p(emitnx)  500.0e-9
set beam0050p(betay)     5.081051073389836e+00
set beam0050p(alphay)    5.430805110902255e-01
set beam0050p(emitny)    5.0e-9
set beam0050p(sigmaz) 1600.0e-6
set beam0050p(meanz)     0.0e-6
set beam0050p(charge)    0.65e-9
set beam0050p(uncespr)   1.3e-3
set beam0050p(echirp)    0.0
set beam0050p(energy)    2.86e9

# at entrance of e- section 0050 (Match sr to BC1 RF)
set beam0050m(betax)     5.237264781559220e+00
set beam0050m(alphax)   -7.981883362456368e-01
set beam0050m(emitnx)  500.0e-9
set beam0050m(betay)     1.916460163913579e+01
set beam0050m(alphay)    1.980883560165905e+00
set beam0050m(emitny)    5.0e-9
set beam0050m(sigmaz) 1600.0e-6
set beam0050m(meanz)     0.0e-6
set beam0050m(charge)    0.65e-9
set beam0050m(uncespr)   1.3e-3
set beam0050m(echirp)    0.0
set beam0050m(energy)    2.86e9

# at entrance of section 0060 (BC1 RF)
set beam0060(betax)    40.05625
set beam0060(alphax)    0.0375
set beam0060(emitnx)  500.0e-9
set beam0060(betay)    40.05625
set beam0060(alphay)    0.0375
set beam0060(emitny)    5.0e-9
set beam0060(sigmaz) 1600.0e-6
set beam0060(meanz)     0.0e-6
set beam0060(charge)    0.65e-9
set beam0060(uncespr)   1.3e-3
set beam0060(echirp)    0.0
set beam0060(energy)    2.86e9

# at entrance of section 0070 (Matching BC1 RF to Chicane)
set beam0070(betax)    40.05625
set beam0070(alphax)   -0.0375
set beam0070(emitnx)  500.0e-9
set beam0070(betay)    40.05625
set beam0070(alphay)   -0.0375
set beam0070(emitny)    5.0e-9
set beam0070(sigmaz) 1600.0e-6
set beam0070(meanz)     0.0e-6
set beam0070(charge)    0.65e-9
set beam0070(uncespr)   1.3e-3
set beam0070(echirp)    6.45
set beam0070(energy)    2.86e9

# at entrance of section 0080 (BC1 Chicane)
set beam0080(betax)   100.0
set beam0080(alphax)    2.8
set beam0080(emitnx)  500.0e-9
set beam0080(betay)    14.0
set beam0080(alphay)    0.0
set beam0080(emitny)    5.0e-9
set beam0080(sigmaz) 1600.0e-6
set beam0080(meanz)     0.0e-6
set beam0080(charge)    0.65e-9
set beam0080(uncespr)   1.3e-3
set beam0080(echirp)    6.45
set beam0080(energy)    2.86e9

# at entrance of section 0090 (Matching BC1 to Diagnostics section 2)
set beam0090(betax)     1.150525024464906e+01
set beam0090(alphax)    1.306294602657933e-01
set beam0090(emitnx)  500.0e-9
set beam0090(betay)     6.588028731789771e+01
set beam0090(alphay)   -1.464141924656770e+00
set beam0090(emitny)    5.0e-9
set beam0090(sigmaz)  300.0e-6
set beam0090(meanz)     0.0e-6
set beam0090(charge)    0.65e-9
set beam0090(uncespr)   6.93e-3
set beam0090(echirp)   25.84
set beam0090(energy)    2.86e9

# at entrance of section 0100 (Diagnostics section 2)
set beam0100(betax)     1.969810108121634e+01
set beam0100(alphax)    2.002449870169331e+00
set beam0100(emitnx)  500.0e-9
set beam0100(betay)     5.081050674189390e+00
set beam0100(alphay)    -5.430803605073287e-01
set beam0100(emitny)    5.0e-9
set beam0100(sigmaz)  300.0e-6
set beam0100(meanz)     0.0e-6
set beam0100(charge)    0.65e-9
set beam0100(uncespr)   6.93e-3
set beam0100(echirp)   25.84
set beam0100(energy)    2.86e9

# at entrance of section 0110 (Dump and match diag to booster)
set beam0110(betax)     1.969809759196183e+01
set beam0110(alphax)   -2.002449361815438e+00
set beam0110(emitnx)  500.0e-9
set beam0110(betay)     5.081051132647060e+00
set beam0110(alphay)    5.430803844347085e-01
set beam0110(emitny)    5.0e-9
set beam0110(sigmaz)  300.0e-6
set beam0110(meanz)     0.0e-6
set beam0110(charge)    0.65e-9
set beam0110(uncespr)   6.93e-3
set beam0110(echirp)   25.84
set beam0110(energy)    2.86e9

# at entrance of section 0120 (Booster Linac)
set beam0120(betax)     6.079099128010975e+00
set beam0120(alphax)   -4.068358870139384e-01
set beam0120(emitnx)  500.0e-9
set beam0120(betay)     4.794933025651348e+01
set beam0120(alphay)    2.701543519633918e+00
set beam0120(emitny)    5.0e-9
set beam0120(sigmaz)  300.0e-6
set beam0120(meanz)     0.0e-6
set beam0120(charge)    0.65e-9
set beam0120(uncespr)   6.93e-3
set beam0120(echirp)   25.84
set beam0120(energy)    2.86e9

# at entrance of section 0130 (Matching Booster to Central Arc)
set beam0130(betax)     1.024216598767133e+01
set beam0130(alphax)   -9.801517236518169e-01
set beam0130(emitnx)  500.0e-9
set beam0130(betay)     3.329109300863542e+01
set beam0130(alphay)    2.182329827618926e+00
set beam0130(emitny)    5.0e-9
set beam0130(sigmaz)  300.0e-6
set beam0130(meanz)     0.0e-6
set beam0130(charge)    0.65e-9
set beam0130(uncespr)   2.20e-3
set beam0130(echirp)    8.21
set beam0130(energy)    9.0e9

# at entrance of e+ section 0140 (Central Arc)
set beam0140p(betax)     5.521277310920350e+01
set beam0140p(alphax)   -1.244146800398738e+01
set beam0140p(emitnx)  500.0e-9
set beam0140p(betay)     4.760452119255813e+00
set beam0140p(alphay)    1.081536709457839e+00
set beam0140p(emitny)    5.0e-9
set beam0140p(sigmaz)  300.0e-6
set beam0140p(meanz)     0.0e-6
set beam0140p(charge)    0.65e-9
set beam0140p(uncespr)   2.20e-3
set beam0140p(echirp)    8.21
set beam0140p(energy)    9.0e9

# at entrance of e- section 0140 (Central Arc)
set beam0140m(betax)     3.691268915619407e+01
set beam0140m(alphax)    7.218484190972008e+00
set beam0140m(emitnx)  500.0e-9
set beam0140m(betay)     5.974111487926983e+00
set beam0140m(alphay)   -1.172087180546570e+00
set beam0140m(emitny)    5.0e-9
set beam0140m(sigmaz)  300.0e-6
set beam0140m(meanz)     0.0e-6
set beam0140m(charge)    0.65e-9
set beam0140m(uncespr)   2.20e-3
set beam0140m(echirp)    8.21
set beam0140m(energy)    9.0e9

# at entrance of e+ section 0150 (Vertical Transfer)
set beam0150p(betax)     5.521254666552343e+01
set beam0150p(alphax)   -1.244139130716687e+01
set beam0150p(emitnx)  500.0e-9
set beam0150p(betay)     4.760456950323459e+00
set beam0150p(alphay)    1.081532271901695e+00
set beam0150p(emitny)    5.0e-9
set beam0150p(sigmaz)  300.0e-6
set beam0150p(meanz)     0.0e-6
set beam0150p(charge)    0.65e-9
set beam0150p(uncespr)   2.20e-3
set beam0150p(echirp)    8.21
set beam0150p(energy)    9.0e9

# at entrance of e- section 0150 (Vertical Transfer)
set beam0150m(betax)     3.692750197920162e+01
set beam0150m(alphax)   -7.221528447518271e+00
set beam0150m(emitnx)  500.0e-9
set beam0150m(betay)     5.975176170904100e+00
set beam0150m(alphay)    1.172520226946776e+00
set beam0150m(emitny)    5.0e-9
set beam0150m(sigmaz)  300.0e-6
set beam0150m(meanz)     0.0e-6
set beam0150m(charge)    0.65e-9
set beam0150m(uncespr)   2.20e-3
set beam0150m(echirp)    8.21
set beam0150m(energy)    9.0e9

# at entrance of section 0160 (Match vertical transfer to long transfer line)
set beam0160(betax)     6.270730335363210e+01
set beam0160(alphax)   -6.919418042317589e+00
set beam0160(emitnx)  500.0e-9
set beam0160(betay)     1.536241962473385e+01
set beam0160(alphay)   -4.728273958766606e+00
set beam0160(emitny)    5.0e-9
set beam0160(sigmaz)  300.0e-6
set beam0160(meanz)     0.0e-6
set beam0160(charge)    0.65e-9
set beam0160(uncespr)   2.20e-3
set beam0160(echirp)    8.21
set beam0160(energy)    9.0e9

# at entrance of section 0170 (Long Transfer Line)
set beam0170(betax)     8.561241468163842e+02
set beam0170(alphax)   -1.496735512963458e+00
set beam0170(emitnx)  500.0e-9
set beam0170(betay)     3.825496344823510e+02
set beam0170(alphay)    6.692291814514501e-01
set beam0170(emitny)    5.0e-9
set beam0170(sigmaz)  300.0e-6
set beam0170(meanz)     0.0e-6
set beam0170(charge)    0.65e-9
set beam0170(uncespr)   2.20e-3
set beam0170(echirp)    8.21
set beam0170(energy)    9.0e9

# at entrance of section 0180 (Dump and Match Transfer Line to TAL)
set beam0180(betax)     8.561241581474796e+02
set beam0180(alphax)   -1.496735542320063e+00
set beam0180(emitnx)  500.0e-9
set beam0180(betay)     3.825496347048135e+02
set beam0180(alphay)    6.692291859769192e-01
set beam0180(emitny)    5.0e-9
set beam0180(sigmaz)  300.0e-6
set beam0180(meanz)     0.0e-6
set beam0180(charge)    0.65e-9
set beam0180(uncespr)   2.20e-3
set beam0180(echirp)    8.21
set beam0180(energy)    9.0e9

# at entrance of section 0190 (Turn Around Loop)
set beam0190(betax)     3.690629568070262e+01
set beam0190(alphax)    7.218310576379055e+00
set beam0190(emitnx)  500.0e-9
set beam0190(betay)     5.974176267738427e+00
set beam0190(alphay)   -1.172276165335175e+00
set beam0190(emitny)    5.0e-9
set beam0190(sigmaz)  300.0e-6
set beam0190(meanz)     0.0e-6
set beam0190(charge)    0.65e-9
set beam0190(uncespr)   2.20e-3
set beam0190(echirp)    8.21
set beam0190(energy)    9.0e9

# at entrance of section 0200 (Matching TAL to BC2 RF)
set beam0200(betax)     3.690627938705909e+01
set beam0200(alphax)   -7.218289085198253e+00
set beam0200(emitnx)  500.0e-9
set beam0200(betay)     5.974172370733092e+00
set beam0200(alphay)    1.172278094444008e+00
set beam0200(emitny)    5.0e-9
set beam0200(sigmaz)  300.0e-6
set beam0200(meanz)     0.0e-6
set beam0200(charge)    0.65e-9
set beam0200(uncespr)   2.20e-3
set beam0200(echirp)    8.21
set beam0200(energy)    9.0e9

# at entrance of section 0210 (BC2 RF)
set beam0230(betax)    40.0
set beam0230(alphax)    0.0
set beam0230(emitnx)  500.0e-9
set beam0230(betay)    40.0
set beam0230(alphay)    0.0
set beam0230(emitny)    5.0e-9
set beam0230(sigmaz)  300.0e-6
set beam0230(meanz)     0.0e-6
set beam0230(charge)    0.65e-9
set beam0230(uncespr)   2.20e-3
set beam0230(echirp)    8.21
set beam0230(energy)    9.0e9

# at entrance of section 0220 (Matching BC2 RF and Chicane 1)
set beam0240(betax)    40.0
set beam0240(alphax)    0.0
set beam0240(emitnx)  500.0e-9
set beam0240(betay)    40.0
set beam0240(alphay)    0.0
set beam0240(emitny)    5.0e-9
set beam0240(sigmaz)  300.0e-6
set beam0240(meanz)     0.0e-6
set beam0240(charge)    0.65e-9
set beam0240(uncespr)   2.20e-3
set beam0240(echirp)   49.53
set beam0240(energy)    9.0e9

# at entrance of section 0230 (BC2 Chicane 1)
set beam0250(betax)    70.0
set beam0250(alphax)    1.5
set beam0250(emitnx)  500.0e-9
set beam0250(betay)    20.0
set beam0250(alphay)    0.2
set beam0250(emitny)    5.0e-9
set beam0250(sigmaz)  300.0e-6
set beam0250(meanz)     0.0e-6
set beam0250(charge)    0.65e-9
set beam0250(uncespr)   2.20e-3
set beam0250(echirp)   49.53
set beam0250(energy)    9.0e9

# at entrance of section 0240 (Matching BC2 Chicanes 1 and 2)
set beam0260(betax)     2.178150310568769e+01
set beam0260(alphax)    1.062239896984802e-01
set beam0260(emitnx)  500.0e-9
set beam0260(betay)     5.371886834701682e+01
set beam0260(alphay)   -1.306672549450183e+00
set beam0260(emitny)    5.0e-9
set beam0260(sigmaz)  100.0e-6
set beam0260(meanz)     0.0e-6
set beam0260(charge)    0.65e-9
set beam0260(uncespr)   6.61e-3
set beam0260(echirp)  134.90
set beam0260(energy)    9.0e9

# at entrance of section 0250 (BC2 Chicane 2)
set beam0270(betax)    75.0
set beam0270(alphax)    1.8
set beam0270(emitnx)  500.0e-9
set beam0270(betay)    20.0
set beam0270(alphay)    0.5
set beam0270(emitny)    5.0e-9
set beam0270(sigmaz)  100.0e-6
set beam0270(meanz)     0.0e-6
set beam0270(charge)    0.65e-9
set beam0270(uncespr)   6.61e-3
set beam0270(echirp)  134.90
set beam0270(energy)    9.0e9

# at entrance of section 0260 (Matching BC2 to Diagnostics Section 3)
set beam0280(betax)     1.787856466656100e+01
set beam0280(alphax)    1.035722602962853e-01
set beam0280(emitnx)  600.0e-9
set beam0280(betay)     4.602643715762838e+01
set beam0280(alphay)   -1.362791302373187e+00
set beam0280(emitny)   10.0e-9
set beam0280(sigmaz)   44.0e-6
set beam0280(meanz)     0.0e-6
set beam0280(charge)    0.65e-9
set beam0280(uncespr)   1.50e-2
set beam0280(echirp)    0.0
set beam0280(energy)    9.0e9

# at entrance of section 0270 (Diagnostics Section 3)
set beam0290(betax)     1.969770799217144e+01
set beam0290(alphax)    2.002438803662253e+00
set beam0290(emitnx)  600.0e-9
set beam0290(betay)     5.075137359609719e+00
set beam0290(alphay)   -5.412520390986038e-01
set beam0290(emitny)   10.0e-9
set beam0290(sigmaz)   44.0e-6
set beam0290(meanz)     0.0e-6
set beam0290(charge)    0.65e-9
set beam0290(uncespr)   1.50e-2
set beam0290(echirp)    0.0
set beam0290(energy)    9.0e9

# at entrance of section 0280 (Dump and match RTML to Main Linac)
set beam0290(betax)     1.969760402886895e+01
set beam0290(alphax)    2.002374828102339e+00
set beam0290(emitnx)  600.0e-9
set beam0290(betay)     5.083963690843209e+00
set beam0290(alphay)   -5.449609668452444e-01
set beam0290(emitny)   10.0e-9
set beam0290(sigmaz)   44.0e-6
set beam0290(meanz)     0.0e-6
set beam0290(charge)    0.65e-9
set beam0290(uncespr)   1.50e-2
set beam0290(echirp)    0.0
set beam0290(energy)    9.0e9


