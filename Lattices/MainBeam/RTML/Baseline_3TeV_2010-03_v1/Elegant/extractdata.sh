#!/bin/bash
#
sdds2plaindata rtml.twi rtml.twi.txt -outputMode=ascii -separator=' ' -col=s -col=betax -col=alphax -col=psix -col=betay -col=alphay -col=ElementName -noRowCount
sdds2plaindata rtml.mat rtml.mat.txt -outputMode=ascii -separator=' ' -col=s -col=R16 -col=R52 -col=R56 -col=R55 -col=R36 -col=T566 -noRowCount
#sdds2plaindata rtml.mat rtml.mat.asc -outputMode=ascii -separator=' ' -col=s -col=ElementName -col=ElementOccurence -col=ElementType -noRowCount
sdds2plaindata rtml.s rtml.s.emit -outputMode=ascii -separator=' ' -col=s -col=enx -col=ecnx -col=eny -col=ecny -noRowCount
sdds2plaindata rtml.s rtml.s.txt2 -outputMode=ascii -separator=' ' -col=s -col=Ss -col=Sdelta -noRowCount
sddsprocess rtml.cen rtml.cen.new -def=col,pCentralGeV,"pCentral 0.00051099891 *" -def=col,CdeltaGeV,"Cdelta pCentral * 0.00051099891 *"
sdds2plaindata rtml.cen.new rtml.cen.txt -outputMode=ascii -separator=' ' -col=s -col=Cs -col=Cdelta -col=CdeltaGeV -col=pCentralGeV -noRowCount
#
sdds2plaindata rtml.flr rtml.flr.txt -outputMode=ascii -separator=' ' -noRowCount -col=s -col=X -col=Y -col=Z -col=theta -col=phi -col=psi -col=ElementName
#
#
#sddsprocess rtml.mat rtml_new.mat -define=param,Sdelta0,1.1e-3 -define=param,Ss0,1300e-6,units=m -define=col,Ss,"Sdelta0 R56 * sqr Ss0 R55 * sqr + sqrt",units=m
#sddsplot -col=s,Ss rtml.s rtml_new.mat -graph=line,vary &
