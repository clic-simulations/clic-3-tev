#!/bin/sh
# BC1 RF wakes
echo "cavitywake_new(\"wake_bc1.dat\",10001,11.0e-3,21.0e-3,25.0e-3,0.0,0.0)" | octave -q
plaindata2sdds wake_bc1.dat wake_bc1.sdds -inputMode=ascii -outputMode=ascii -separator=' ' -noRowCount -col=t,double,units=s -col=wl,double,units=V/C -col=wx,double,units=V/C/m -col=wy,double,units=V/C/m
#
# Booster wakes
echo "cavitywake_new(\"wake_booster.dat\",10001,11.0e-3,21.0e-3,25.0e-3,0.0,0.0)" | octave -q
plaindata2sdds wake_booster.dat wake_booster.sdds -inputMode=ascii -outputMode=ascii -separator=' ' -noRowCount -col=t,double,units=s -col=wl,double,units=V/C -col=wx,double,units=V/C/m -col=wy,double,units=V/C/m
#
# BC2RF wakes
echo "cavitywake_new(\"wake_bc2.dat\",10001,3.625e-3,8.5417e-3,10.417e-3,0.095,0.205e-3)" | octave -q
plaindata2sdds wake_bc2.dat wake_bc2.sdds -inputMode=ascii -outputMode=ascii -separator=' ' -noRowCount -col=t,double,units=s -col=wl,double,units=V/C -col=wx,double,units=V/C/m -col=wy,double,units=V/C/m
#
