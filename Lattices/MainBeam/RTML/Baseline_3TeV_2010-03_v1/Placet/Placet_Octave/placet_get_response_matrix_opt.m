## -*- texinfo -*-
## @deftypefn {Function File} {} placet_get_response_matrix_opt (@var{beamline}, @var{beam}, @var{survey}, @var{options})
## Return the response matrix for 'beamline' and 'beam0', using selected 'options'. 'options' is a structure containing the following items:
##
## @verbatim
##  options.B = placet_get_number_list("beamline", "bpm");
##  options.C = placet_get_number_list("beamline", "dipole");
##  options.scale = 1.0;
##  options.observable = "reading_y";
##  options.leverage = "strength_y";
## @end verbatim
## @end deftypefn

function [R,b0]=placet_get_response_matrix_opt(beamline, beam, survey, options)
  if nargin==4 && ischar(beamline) && ischar(beam) && ischar(survey) && ischaruct(options)
    Res=placet_element_get_attribute(beamline, options.B, "resolution");
    placet_element_set_attribute(beamline, options.B, "resolution", 0.0);
    placet_test_no_correction(beamline, beam, survey);
    b0=placet_element_get_attribute(beamline, options.B, options.observable);
    R=zeros(length(b0), length(options.C));
    for i=1:columns(options.C)
      placet_element_vary_attribute(beamline, options.C(i), options.leverage, options.scale);
      placet_test_no_correction(beamline, beam, "None");
      placet_element_vary_attribute(beamline, options.C(i), options.leverage, -options.scale);
      b=placet_element_get_attribute(beamline, options.B, options.observable) - b0;
      R(:,i)=b'/options.scale;
    endfor
    placet_element_set_attribute(beamline, options.B, "resolution", Res);
  else  
    help placet_get_response_matrix_opt
  endif
endfunction
