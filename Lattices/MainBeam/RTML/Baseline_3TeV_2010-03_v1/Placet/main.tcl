#
# CLIC Main Beam RTML
#
#
puts ""
puts "Setting up RTML tracking"
puts ""

set latticedir ./Lattices_e+
set scriptdir ./Scripts
set paramsdir ./Parameters

# define the array placetunits for unit conversions
# placetunits=[xyz,xpyp,energy,charge,emittance]
source $scriptdir/placet_units.tcl

#
# load some external files, i.e. parameters, lattices, scripts
#

# load standard beam parameters
# beamXXX=[betax,alphax,emitnx,betay,alphay,emitny,sigmaz,charge,
#          uncespr,echirp,energy]
# units=[m,rad,m*rad,m,rad,m*rad,m,C,1,1/m,eV]
# for use in Placet some of these untis have to be converted using placetunits
source $paramsdir/initial_beam_parameters.tcl

#
# load standard rf parameters
# rfparamsXXX=[gradient,gradientww,phase,wavelength,a,g,l]
# units=[eV/m,eV/m,Degree,m,m,m,m]
# for use in Placet some of these untis have to be converted using placetunits
source $paramsdir/rf_parameters.tcl

#
# load procedures to set up lattices
source $latticedir/0010_match_dr_to_rtml.tcl
source $latticedir/0020_diagnostics_1.tcl
source $latticedir/0030_dump_and_match_diag_to_sr.tcl
source $latticedir/0040_spin_rotator.tcl
source $latticedir/0050_match_sr_to_bc1_rf.tcl
source $latticedir/0060_bc1_rf.tcl
source $latticedir/0070_match_bc1_rf_to_chicane.tcl
source $latticedir/0080_bc1_chicane.tcl
source $latticedir/0090_match_bc1_to_diag.tcl
source $latticedir/0100_diagnostics_2.tcl
source $latticedir/0110_dump_and_match_diag_to_booster.tcl
source $latticedir/0120_booster_linac.tcl
source $latticedir/0130_dump_and_match_booster_to_ca.tcl
source $latticedir/0140_central_arc.tcl
source $latticedir/0150_vertical_transfer.tcl
source $latticedir/0160_match_vt_to_ltl.tcl
source $latticedir/0170_long_transfer_line.tcl
source $latticedir/0180_dump_and_match_ltl_to_tal.tcl
source $latticedir/0190_turn_around_loop.tcl
source $latticedir/0200_match_tal_to_sr.tcl
source $latticedir/0210_skip_spin_rotator.tcl
source $latticedir/0220_match_sr_to_bc2_rf.tcl
source $latticedir/0230_bc2_rf.tcl
source $latticedir/0240_match_bc2_rf_to_chicane_1.tcl
source $latticedir/0250_bc2_chicane_1.tcl
source $latticedir/0260_match_bc2_chicanes.tcl
source $latticedir/0270_bc2_chicane_2.tcl
source $latticedir/0280_match_bc2_to_diag.tcl
source $latticedir/0290_diagnostics_3.tcl

#
# load procedures to set up cavities and beams
source $scriptdir/cavitywakesetup.tcl
source $scriptdir/beamsetup.tcl

#
# load tracking procedures
source $scriptdir/tracking.tcl

#
# set some general parameters which should not change throughout tracking
set beamparams(nslice) 1001
set beamparams(nmacro) 150
set beamparams(nsigmabunch) 4
set beamparams(nsigmawake) 10

# this is needed to concatenate data at the correct z position since each step
# restarts at z=0.0
set zoffset 0.0
set fid [open zoffset.dat w]
puts $fid "# Step / Start Position"
puts $fid "1   $zoffset"
close $fid

#
# track through first part of RTML, i.e, up to entrance of booster linac
puts ""
puts "Setting up tracking step step1 using beamline rtmlpart1 and beam beam1"
puts ""

# general beam and rf parameters
#
# beamparams=[betax,alphax,emitnx,betay,alphay,emitny,sigmaz,charge,
#             uncespread,echirp,energy,nslice,nmacro,nsigma]
# units=[m,rad,m*rad,m,rad,m*rad,m,C,1,1/m,eV,1,1,1]
# hence, in the array the values are stored in Placet units
set beamparams(betax) $beam0010(betax)
set beamparams(alphax) $beam0010(alphax)
set beamparams(emitnx) [expr $beam0010(emitnx)*$placetunits(emittance)]
set beamparams(betay) $beam0010(betay)
set beamparams(alphay) $beam0010(alphay)
set beamparams(emitny) [expr $beam0010(emitny)*$placetunits(emittance)]
set beamparams(sigmaz) [expr $beam0010(sigmaz)*$placetunits(xyz)]
set beamparams(charge) [expr $beam0010(charge)*$placetunits(charge)]
set beamparams(uncespread) $beam0010(uncespr)
set beamparams(echirp) [expr $beam0010(echirp)/$placetunits(xyz)]
set beamparams(energy) [expr $beam0010(energy)*$placetunits(energy)]

set beamparams(energyafterbc1) [expr $beam0010(energy)*$placetunits(energy)]
set beamparams(energyafterbooster) [expr $beam0010(energy)*$placetunits(energy)]
set beamparams(energyafterbc2) [expr $beam0010(energy)*$placetunits(energy)]

set beamparams(useisr) 1
set beamparams(usewakefields) 1

if {$beamparams(usewakefields)==1} {
  set rfparamsbc1(gradient) [expr $rfparamsbc1(gradientww)*$placetunits(energy)]
  } else {
  set rfparamsbc1(gradient) [expr $rfparamsbc1(gradient)*$placetunits(energy)]
  }

#
#setup of lattice and beam
#
BeamlineNew
Girder
TclCall -script {BeamDump -file bunch_step_1a.dat}

lattice_match_dr_to_rtml beamparams
lattice_diagnostics_1 beamparams
lattice_dump_and_match_diag_to_sr beamparams
lattice_spin_rotator beamparams
lattice_match_sr_to_bc1_rf beamparams
lattice_bc1_rf rfparamsbc1 beamparams
lattice_match_bc1_rf_to_chicane beamparams
lattice_bc1_chicane beamparams
lattice_match_bc1_to_diag beamparams
lattice_diagnostics_2 beamparams
lattice_dump_and_match_diag_to_booster beamparams

TclCall -script {BeamDump -file bunch_step_1c.dat}
BeamlineSet -name rtmlpart1

FirstOrder 1

initialize_cavities rfparamsbc1 wakelong1
make_particle_beam beam1 beamparams rfparamsbc1 wake1.dat

# perform tracking
# set new values in beamparams
do_tracking rtmlpart1 beam1 beamparams 1


# end of first tracking part

#
# track through second part of RTML, i.e, up to entrance of BC2 RF
puts ""
puts "Setting up tracking step step2 using beamline rtmlpart2 and beam beam2"
puts ""

if {$beamparams(usewakefields)==1} {
  set rfparamsbooster(gradient) [expr $rfparamsbooster(gradientww)*$placetunits(energy)]
  } else {
  set rfparamsbooster(gradient) [expr $rfparamsbooster(gradient)*$placetunits(energy)]
  }

#
#setup of lattice and beam
#
BeamlineNew
Girder
TclCall -script {BeamDump -file bunch_step_2a.dat}

lattice_booster_linac rfparamsbooster beamparams
lattice_dump_and_match_booster_to_ca beamparams
lattice_central_arc beamparams
lattice_vertical_transfer beamparams
lattice_match_vt_to_ltl beamparams
lattice_long_transfer_line beamparams
lattice_dump_and_match_ltl_to_tal beamparams
lattice_turn_around_loop beamparams
lattice_match_tal_to_sr beamparams
lattice_skip_spin_rotator beamparams
lattice_match_sr_to_bc2_rf beamparams

TclCall -script {BeamDump -file bunch_step_2e.dat}
BeamlineSet -name rtmlpart2

FirstOrder 1

initialize_cavities rfparamsbooster wakelong2
reload_particle_beam beam2 beamparams rfparamsbooster bunch_step_1c.dat wake2.dat

# perform tracking
# set new values in beamparams
do_tracking rtmlpart2 beam2 beamparams 2


# end of second tracking part

#
# track through third part of RTML, i,e, up to entrance of main linac
puts ""
puts "Setting up tracking step step3 using beamline rtmlpart3 and beam beam3"
puts ""

if {$beamparams(usewakefields)==1} {
  set rfparamsbc2(gradient) [expr $rfparamsbc2(gradientww)*$placetunits(energy)]
  } else {
  set rfparamsbc2(gradient) [expr $rfparamsbc2(gradient)*$placetunits(energy)]
  }

#
#setup of lattice and beam
#
BeamlineNew
Girder
TclCall -script {BeamDump -file bunch_step_3a.dat}

lattice_bc2_rf rfparamsbc2 beamparams
lattice_match_bc2_rf_to_chicane_1 beamparams
lattice_bc2_chicane_1 beamparams
lattice_match_bc2_chicanes beamparams
lattice_bc2_chicane_2 beamparams
lattice_match_bc2_to_diag beamparams
lattice_diagnostics_3 beamparams

TclCall -script {BeamDump -file bunch_step_3d.dat}
BeamlineSet -name rtmlpart3

FirstOrder 1

initialize_cavities rfparamsbc2 wakelong3
reload_particle_beam beam3 beamparams rfparamsbc2 bunch_step_2e.dat wake3.dat

# perform tracking
# set new values in beamparams
do_tracking rtmlpart3 beam3 beamparams 3


# end of third tracking part

