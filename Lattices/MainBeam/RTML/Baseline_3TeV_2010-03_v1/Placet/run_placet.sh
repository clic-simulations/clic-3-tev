#!/bin/bash
#
echo "Job started: " $( date )

# set up some job related stuff
simdirname=./simrun
mkdir $simdirname
cp -R ./Lattices_common $simdirname
cp -R ./Lattices_e+ $simdirname
cp -R ./Lattices_e- $simdirname
cp -R ./Parameters $simdirname
cp -R ./Placet_Octave $simdirname
cp -R ./Scripts $simdirname
cp ./main.tcl $simdirname
cd $simdirname
#You have to use the name of your Placet binary. It must be the version with octave.
#The grep is used to get rid of some rather useless warnings which might come up in huge numbers.
#
#placet64-octave main.tcl 2>&1 | tee placet.out
#placet64-octave main.tcl 2> placet.err | grep --line-buffered -v "too large z" - | tee placet.out
placet-octave main.tcl 2> placet.err | grep --line-buffered -v "too large z" - | tee placet.out
#placet64-octave main.tcl | grep -v "too large z" - | grep -v "Cannot track a particle" - | tee placet.out
echo "Job finished: " $( date )
