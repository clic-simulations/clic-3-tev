#
# matching of BC1 RF and BC1 chicane
#
# beamparams=[betax,alphax,emitnx,betay,alphay,emitny,sigmaz,charge,
#             uncespread,echirp,energy,nslice,nmacro,nsigmabunch,nsigmawake]
# units=[m,rad,m*rad,m,rad,m*rad,m,C,1,1/m,eV,1,1,1,1]
#
# where necessary units were converted to placet units in main.tcl

proc lattice_bc1_match_rf_and_chicane {bparray} {
upvar $bparray beamparams

set usesixdim 1
set numthinlenses 100

set e0 $beamparams(energyafterbc1rf)

set lquadm 0.36

set kqm1 0.5325487934 
set kqm2 -0.2644057507
set kqm3 -0.3637870191
set kqm4 0.4875813132 

set ldm1 4.0 
set ldm2 3.79
set ldm3 4.0 
set ldm4 4.0 
set ldm5 0.6 


Girder
Drift -name BC1MATCHING -length 0.00000 -six_dim $usesixdim
Drift -name DM1 -length $ldm1 -six_dim $usesixdim
Quadrupole -name QM1 -length $lquadm -strength [expr $kqm1*$lquadm*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -name DM2 -length $ldm2 -six_dim $usesixdim
Quadrupole -name QM2 -length $lquadm -strength [expr $kqm2*$lquadm*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -name DM3 -length $ldm3 -six_dim $usesixdim
Quadrupole -name QM3 -length $lquadm -strength [expr $kqm3*$lquadm*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -name DM4 -length $ldm4 -six_dim $usesixdim
Quadrupole -name QM4 -length $lquadm -strength [expr $kqm4*$lquadm*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -name DM5 -length $ldm5 -six_dim $usesixdim
Drift -name BC1MATCHING -length 0.00000 -six_dim $usesixdim
}
