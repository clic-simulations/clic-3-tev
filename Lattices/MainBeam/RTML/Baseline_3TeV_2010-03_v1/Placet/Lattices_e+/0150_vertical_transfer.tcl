#
# Certical transfer
# includes the necessary transverse offsets and arcs to geometrically match
# the central arc to the long transfer line
# the optics matching is done in the following section
#
# beamparams=[betax,alphax,emitnx,betay,alphay,emitny,sigmaz,charge,
#             uncespread,echirp,energy,nslice,nmacro,nsigmabunch,nsigmawake]
# units=[m,rad,m*rad,m,rad,m*rad,m,C,1,1/m,eV,1,1,1,1]
# where necessary units were converted to placet units in main.tcl

proc lattice_vertical_transfer {bparray} {
upvar $bparray beamparams

set usesynrad $beamparams(useisr)
set usesixdim 1
set numthinlenses 100
set quad_synrad 0

set e0 $beamparams(energyafterbooster)

# match central arc to vertical arc
set lquadhv 0.3

set kqhv1 0.6107435442
set kqhv2 0.2321395202
set kqhv3 -1.187498499
set kqhv4 1.341241092
set kqhv5 -0.4699339275

set ldhv1 7.0
set ldhv2 8.0
set ldhv3 1.5
set ldhv4 0.8

Girder
Quadrupole -synrad $quad_synrad -length $lquadhv -strength [expr $kqhv1*$lquadhv*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ldhv1 -six_dim $usesixdim
Quadrupole -synrad $quad_synrad -length $lquadhv -strength [expr $kqhv2*$lquadhv*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ldhv2 -six_dim $usesixdim
Quadrupole -synrad $quad_synrad -length $lquadhv -strength [expr $kqhv3*$lquadhv*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ldhv3 -six_dim $usesixdim
Quadrupole -synrad $quad_synrad -length $lquadhv -strength [expr $kqhv4*$lquadhv*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ldhv4 -six_dim $usesixdim
Quadrupole -synrad $quad_synrad -length $lquadhv -strength [expr $kqhv5*$lquadhv*$e0] -six_dim $usesixdim -thin_lens $numthinlenses



#first vertical arc
set lbenda 1.5
set lquada 0.5
set lsexta 0.2

set pi 3.141592653589793

set theta [expr 0.65/180*$pi]
set rho [expr $lbenda/(2*sin($theta/2))]
set larc [expr $theta*$rho]

set ld01 0.96
set ld02 0.7
set ld03 4.0
set ld04 2.7
set ld05 1.0
set ld06 $ld05
set ld07 $ld04
set ld08 $ld03
set ld09 $ld02
set ld10 $ld01


set ld03a [expr $ld03-0.3]
set ld03b 0.1
set ld08a 0.1
set ld08b [expr $ld08-0.3]


set kq01 -1.429864551
set kq02 0.7260952542
set kq03 -0.9016411242
set kq04 0.2729779117
set kq05 $kq04
set kq06 $kq03
set kq07 $kq02
set kq08 $kq01


set ks01 [expr -98.0]
set ks02 [expr -98.0]

Girder

Drift -length $ld02 -six_dim $usesixdim
Sbend -length $larc -angle [expr $theta] -synrad $usesynrad -six_dim $usesixdim -thin_lens $numthinlenses -e0 $e0 -E1 [expr $theta/2.0] -E2 [expr $theta/2.0] -tilt [expr $pi/2.0]
Drift -length $ld03a -six_dim $usesixdim
Multipole -type 3 -length $lsexta -e0 $e0 -strength [expr $ks01*$e0*$lsexta] -thin_lens $numthinlenses -six_dim $usesixdim -tilt [expr $pi/2.0]
Drift -length $ld03b -six_dim $usesixdim
Quadrupole -synrad $quad_synrad -length $lquada -strength [expr $kq03*$lquada*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ld04 -six_dim $usesixdim
Quadrupole -synrad $quad_synrad -length $lquada -strength [expr $kq04*$lquada*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ld05 -six_dim $usesixdim
Sbend -length $larc -angle [expr $theta*2.0] -synrad $usesynrad -six_dim $usesixdim -thin_lens $numthinlenses -e0 $e0 -E1 [expr $theta*2.0/2.0] -E2 [expr $theta*2.0/2.0] -tilt [expr $pi/2.0]
Drift -length $ld06 -six_dim $usesixdim
Quadrupole -synrad $quad_synrad -length $lquada -strength [expr $kq05*$lquada*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ld07 -six_dim $usesixdim
Quadrupole -synrad $quad_synrad -length $lquada -strength [expr $kq06*$lquada*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ld08a -six_dim $usesixdim
Multipole -type 3 -length $lsexta -e0 $e0 -strength [expr $ks02*$e0*$lsexta] -thin_lens $numthinlenses -six_dim $usesixdim -tilt [expr $pi/2.0]
Drift -length $ld08b -six_dim $usesixdim
Sbend -length $larc -angle [expr $theta] -synrad $usesynrad -six_dim $usesixdim -thin_lens $numthinlenses -e0 $e0 -E1 [expr $theta/2.0] -E2 [expr $theta/2.0] -tilt [expr $pi/2.0]
Drift -length $ld09 -six_dim $usesixdim

# match first vertical arc to fodo
set lquadm 0.3

set ldm1 4.0
set ldm2 11.0
set ldm3 7.5
set ldm4 5.0

set kqm1 0.5102986995
set kqm2 -0.336823713
set kqm3 -0.2472896072
set kqm4 0.1830219261
set kqm5 0.02619084535

Girder
Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm1*$lquadm*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ldm1 -six_dim $usesixdim
Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm2*$lquadm*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ldm2 -six_dim $usesixdim
Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm3*$lquadm*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ldm3 -six_dim $usesixdim
Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm4*$lquadm*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ldm4 -six_dim $usesixdim
Quadrupole -synrad $quad_synrad -length [expr $lquadm/2.0] -strength [expr $kqm5*$lquadm/2.0*$e0] -six_dim $usesixdim -thin_lens $numthinlenses



# FODO
set lquadf 0.3
set kqf1 0.02619123669
set kqf2 -0.02619084535
set ldf 100.0

Girder
for {set cell 1} {$cell<=11} {incr cell} {
    Quadrupole -synrad $quad_synrad -length [expr $lquadf/2.0] -strength [expr $kqf1*$lquadf/2.0*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
    Drift -length $ldf -six_dim $usesixdim
    Quadrupole -synrad $quad_synrad -length $lquadf -strength [expr $kqf2*$lquadf*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
    Drift -length $ldf -six_dim $usesixdim
    Quadrupole -synrad $quad_synrad -length [expr $lquadf/2.0] -strength [expr $kqf1*$lquadf/2.0*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
}


# match fodo to second vertical arc

set lquadm 0.3

set ldm1 4.0
set ldm2 11.0
set ldm3 7.5
set ldm4 5.0

set kqm1 0.5102986995
set kqm2 -0.336823713
set kqm3 -0.2472896072
set kqm4 0.1830219261
set kqm5 0.02619084535

Girder
Quadrupole -synrad $quad_synrad -length [expr $lquadm/2.0] -strength [expr $kqm5*$lquadm/2.0*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ldm4 -six_dim $usesixdim
Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm4*$lquadm*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ldm3 -six_dim $usesixdim
Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm3*$lquadm*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ldm2 -six_dim $usesixdim
Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm2*$lquadm*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ldm1 -six_dim $usesixdim
Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm1*$lquadm*$e0] -six_dim $usesixdim -thin_lens $numthinlenses


#second vertical arc
set lbenda 1.5
set lquada 0.5
set lsexta 0.2

set pi 3.141592653589793

set theta [expr -0.65/180*$pi]
set rho [expr $lbenda/(2*sin($theta/2))]
set larc [expr $theta*$rho]

set ld01 0.96
set ld02 0.7
set ld03 4.0
set ld04 2.7
set ld05 1.0
set ld06 $ld05
set ld07 $ld04
set ld08 $ld03
set ld09 $ld02
set ld10 $ld01


set ld03a [expr $ld03-0.3]
set ld03b 0.1
set ld08a 0.1
set ld08b [expr $ld08-0.3]


set kq01 -1.429864551
set kq02 0.7260952542
set kq03 -0.9016411242
set kq04 0.2729779117
set kq05 $kq04
set kq06 $kq03
set kq07 $kq02
set kq08 $kq01


set ks01 [expr 98.0]
set ks02 [expr 98.0]

Girder

Drift -length $ld02 -six_dim $usesixdim
Sbend -length $larc -angle [expr $theta] -synrad $usesynrad -six_dim $usesixdim -thin_lens $numthinlenses -e0 $e0 -E1 [expr $theta/2.0] -E2 [expr $theta/2.0] -tilt [expr $pi/2.0]
Drift -length $ld03a -six_dim $usesixdim
Multipole -type 3 -length $lsexta -e0 $e0 -strength [expr $ks01*$e0*$lsexta] -thin_lens $numthinlenses -six_dim $usesixdim -tilt [expr $pi/2.0]
Drift -length $ld03b -six_dim $usesixdim
Quadrupole -synrad $quad_synrad -length $lquada -strength [expr $kq03*$lquada*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ld04 -six_dim $usesixdim
Quadrupole -synrad $quad_synrad -length $lquada -strength [expr $kq04*$lquada*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ld05 -six_dim $usesixdim
Sbend -length $larc -angle [expr $theta*2.0] -synrad $usesynrad -six_dim $usesixdim -thin_lens $numthinlenses -e0 $e0 -E1 [expr $theta*2.0/2.0] -E2 [expr $theta*2.0/2.0] -tilt [expr $pi/2.0]
Drift -length $ld06 -six_dim $usesixdim
Quadrupole -synrad $quad_synrad -length $lquada -strength [expr $kq05*$lquada*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ld07 -six_dim $usesixdim
Quadrupole -synrad $quad_synrad -length $lquada -strength [expr $kq06*$lquada*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ld08a -six_dim $usesixdim
Multipole -type 3 -length $lsexta -e0 $e0 -strength [expr $ks02*$e0*$lsexta] -thin_lens $numthinlenses -six_dim $usesixdim -tilt [expr $pi/2.0]
Drift -length $ld08b -six_dim $usesixdim
Sbend -length $larc -angle [expr $theta] -synrad $usesynrad -six_dim $usesixdim -thin_lens $numthinlenses -e0 $e0 -E1 [expr $theta/2.0] -E2 [expr $theta/2.0] -tilt [expr $pi/2.0]
Drift -length $ld09 -six_dim $usesixdim
}

