#
# matching of Turn Around Loop and spin rotator
#
# beamparams=[betax,alphax,emitnx,betay,alphay,emitny,sigmaz,charge,
#             uncespread,echirp,energy,nslice,nmacro,nsigmabunch,nsigmawake]
# units=[m,rad,m*rad,m,rad,m*rad,m,C,1,1/m,eV,1,1,1,1]
#
# where necessary units were converted to placet units in main.tcl

proc lattice_match_tal_to_sr {bparray} {
upvar $bparray beamparams

set usesixdim 1
set numthinlenses 100
set quad_synrad 0

set e0 $beamparams(energyafterbooster)

set lquadm 0.3

set kqm1 1.485783277
set kqm2 -0.8814511776
set kqm3 0.5502429574
set kqm4 -0.9915986806
set kqm5 0.4821717596
set kqm6 0.676808031

set ldm1 1.5
set ldm2 5.2
set ldm3 3.3
set ldm4 2.5
set ldm5 0.9


Girder
Quadrupole -synrad $quad_synrad -length [expr $lquadm/2.0] -strength [expr $kqm1*$lquadm/2.0*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ldm1 -six_dim $usesixdim
Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm2*$lquadm*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ldm2 -six_dim $usesixdim
Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm3*$lquadm*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ldm3 -six_dim $usesixdim
Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm4*$lquadm*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ldm4 -six_dim $usesixdim
Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm5*$lquadm*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ldm5 -six_dim $usesixdim
Quadrupole -synrad $quad_synrad -length [expr $lquadm/2.0] -strength [expr $kqm6*$lquadm/2.0*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
}
