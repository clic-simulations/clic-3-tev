#
# Skip positron spin rotator
#
# beamparams=[betax,alphax,emitnx,betay,alphay,emitny,sigmaz,charge,
#             uncespread,echirp,energy,nslice,nmacro,nsigmabunch,nsigmawake]
# units=[m,rad,m*rad,m,rad,m*rad,m,C,1,1/m,eV,1,1,1,1]
#
# where necessary units were converted to placet units in main.tcl

proc lattice_skip_spin_rotator {bparray} {
upvar $bparray beamparams

set usesixdim 1
set numthinlenses 100
set quad_synrad 0

set e0 $beamparams(energyafterbooster)

set lquadm 0.3

set kqm1 0.676808031
set kqm2 -0.8234632887

set ldm 3.0


Girder
Quadrupole -synrad $quad_synrad -length [expr $lquadm/2.0] -strength [expr $kqm1*$lquadm/2.0*$e0] -six_dim $usesixdim -thin_lens $numthinlenses

for {set cell 1} {$cell<=14} {incr cell} {
  Drift -length $ldm -six_dim $usesixdim
  Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm2*$lquadm*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
  Drift -length $ldm -six_dim $usesixdim
  Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm1*$lquadm*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
}

Drift -length $ldm -six_dim $usesixdim
Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm2*$lquadm*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ldm -six_dim $usesixdim
Quadrupole -synrad $quad_synrad -length [expr $lquadm/2.0] -strength [expr $kqm1*$lquadm/2.0*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
}
