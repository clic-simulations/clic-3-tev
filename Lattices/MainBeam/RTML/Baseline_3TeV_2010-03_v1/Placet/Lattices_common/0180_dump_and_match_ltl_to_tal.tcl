#
# Dump and match long transfer line to turn around loop
#
# beamparams=[betax,alphax,emitnx,betay,alphay,emitny,sigmaz,charge,
#             uncespread,echirp,energy,nslice,nmacro,nsigmabunch,nsigmawake]
# units=[m,rad,m*rad,m,rad,m*rad,m,C,1,1/m,eV,1,1,1,1]
#
# where necessary units were converted to placet units in main.tcl

proc lattice_dump_and_match_ltl_to_tal {bparray} {
upvar $bparray beamparams

set usesixdim 1
set numthinlenses 100
set quad_synrad 0

set e0 $beamparams(energyafterbooster)

set lquadm 0.3
set lquadx 0.36

set kqm1 0.009713020617
set kqm2 0.02490857395
set kqm3 0.08070684601
set kqm4 -0.2258579917
set kqm5 0.12222078
set kqm6 0.5475086249

set ldm1  5.0
set ldm2  6.4
set ldm3  8.0
set ldm4  8.5
set ldm5 35.0


Girder
Quadrupole -synrad $quad_synrad -length [expr $lquadx/2.0] -strength [expr $kqm1*$lquadx/2.0*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ldm1 -six_dim $usesixdim
Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm2*$lquadm*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ldm2 -six_dim $usesixdim
Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm3*$lquadm*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ldm3 -six_dim $usesixdim
Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm4*$lquadm*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ldm4 -six_dim $usesixdim
Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm5*$lquadm*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ldm5 -six_dim $usesixdim
Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm6*$lquadm*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
}
