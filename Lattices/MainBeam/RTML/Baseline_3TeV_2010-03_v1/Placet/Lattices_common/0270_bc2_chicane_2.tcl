#
# BC2 chicane 2 lattice
#
# beamparams=[betax,alphax,emitnx,betay,alphay,emitny,sigmaz,charge,
#             uncespread,echirp,energy,nslice,nmacro,nsigmabunch,nsigmawake]
# units=[m,rad,m*rad,m,rad,m*rad,m,C,1,1/m,eV,1,1,1,1]
# where necessary units were converted to placet units in main.tcl

proc lattice_bc2_chicane_2 {bparray} {
upvar $bparray beamparams

set usesynrad $beamparams(useisr)
set usesixdim 1
set numthinlenses 100

set pi 3.141592653589793

#set theta [expr 0.990/180*$pi]
set theta [expr 0.845/180*$pi]
set lbend 1.5
set rho [expr $lbend/sin($theta)]
set lbendarc [expr $theta*$rho]
set refenergy $beamparams(energyafterbc2rf)

set d12 [expr 11.5/cos($theta)]
set d23 1.0
set d34 [expr $d12]

Girder
Sbend -length $lbendarc -angle $theta -synrad $usesynrad -six_dim $usesixdim -thin_lens $numthinlenses -e0 $refenergy -E1 0.0 -E2 $theta
Drift -length $d12 -six_dim $usesixdim
Sbend -length $lbendarc -angle -$theta -synrad $usesynrad -six_dim $usesixdim -thin_lens $numthinlenses -e0 $refenergy -E1 -$theta -E2 0.0
Drift -length $d23 -six_dim $usesixdim
Sbend -length $lbendarc -angle -$theta -synrad $usesynrad -six_dim $usesixdim -thin_lens $numthinlenses -e0 $refenergy -E1 0.0 -E2 -$theta
Drift -length $d34 -six_dim $usesixdim
Sbend -length $lbendarc -angle $theta -synrad $usesynrad -six_dim $usesixdim -thin_lens $numthinlenses -e0 $refenergy -E1 $theta -E2 0.0
}
