#
# BC2 RF lattice
#
# beamparams=[betax,alphax,emitnx,betay,alphay,emitny,sigmaz,charge,
#             uncespread,echirp,energy,nslice,nmacro,nsigmabunch,nsigmawake]
# units=[m,rad,m*rad,m,rad,m*rad,m,C,1,1/m,eV,1,1,1,1]
#
# rfparams=[gradient,phase,lambda,a,g,l,delta,delta_g]
# units=[eV/m,Degree,m,m,m,m,m,m]
#
# where necessary units were converted to placet units in main.tcl

proc lattice_bc2_rf {rfparray bparray} {
upvar $rfparray rfparams
upvar $bparray beamparams

#puts [array get rfparams]
#puts [array get beamparams]

set usesixdim 1
set numthinlenses 100
set quad_synrad 0

set gradient $rfparams(gradient)
set e0 $beamparams(energyafterbooster)
set q0 $beamparams(charge)
set cavphase $rfparams(phase)
set lcav 0.25

set lquad 0.3
set kq1 0.1941956032
set kq2 -0.3873637676
set ldrift 0.6
set ldc 0.02

#this is required to take into account the energy gain due to acceleration
#
set de [expr $gradient*$lcav*cos($cavphase/180.0*3.14159265359)]
#
#this is required to take into account the energy gain due to acceleration
#and the energy loss due to wake fields
#you have to check your wake fields fiel to get the correct average energy loss
#
#set de [expr $gradient*$lcav*cos($cavphase/180.0*3.14159265359)-0.93*1e6/1e9*$lcav]
#puts $de

set ebeam $e0

for {set sec 1} {$sec<=6} {incr sec} {
    Girder
    Drift -length [expr $ldrift/2.0] -six_dim $usesixdim

    for {set cc 1} {$cc<=7} {incr cc} {
        Cavity -length $lcav -gradient $gradient -phase $cavphase -type 0
        set ebeam [expr $ebeam+$de]
        Drift -length $ldc -six_dim $usesixdim
    }
    Cavity -length $lcav -gradient $gradient -phase $cavphase -type 0

    Drift -length $ldrift -six_dim $usesixdim
    Quadrupole -synrad $quad_synrad -length $lquad -strength [expr $kq1*$lquad*$ebeam] -six_dim $usesixdim -thin_lens $numthinlenses
    Drift -length $ldrift -six_dim $usesixdim
    Quadrupole -synrad $quad_synrad -length $lquad -strength [expr $kq2*$lquad*$ebeam] -six_dim $usesixdim -thin_lens $numthinlenses
    Drift -length $ldrift -six_dim $usesixdim
    Quadrupole -synrad $quad_synrad -length $lquad -strength [expr $kq1*$lquad*$ebeam] -six_dim $usesixdim -thin_lens $numthinlenses
    Drift -length $ldrift -six_dim $usesixdim

    Cavity -length $lcav -gradient $gradient -phase $cavphase -type 0
    for {set cc 1} {$cc<=7} {incr cc} {
        Drift -length $ldc -six_dim $usesixdim
        Cavity -length $lcav -gradient $gradient -phase $cavphase -type 0
        set ebeam [expr $ebeam+$de]
    }

    Drift -length [expr $ldrift/2.0] -six_dim $usesixdim
}

set beamparams(energyafterbc2rf) $ebeam
puts "Setting beamparams(energyafterbc2rf)=$beamparams(energyafterbc2rf)."
}
