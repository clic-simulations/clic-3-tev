#
# Booster Linac lattice
#
# beamparams=[betax,alphax,emitnx,betay,alphay,emitny,sigmaz,charge,
#             uncespread,echirp,energy,nslice,nmacro,nsigmabunch,nsigmawake]
# units=[m,rad,m*rad,m,rad,m*rad,m,C,1,1/m,eV,1,1,1,1]
#
# rfparams=[gradient,phase,lambda,a,g,l,delta,delta_g]
# units=[eV/m,Degree,m,m,m,m,m,m]
#
# where necessary units were converted to placet units in main.tcl

proc lattice_booster_linac {rfparray bparray} {
upvar $rfparray rfparams
upvar $bparray beamparams

#puts [array get rfparams]
#puts [array get beamparams]

set usesixdim 1
set numthinlenses 100
set quad_synrad 0

set gradient $rfparams(gradient)
set e0 $beamparams(energyafterbc1rf)
set q0 $beamparams(charge)
set cavphase $rfparams(phase)

set lcav 3.0
set lquad 0.3
set lbpm 0.1

set kq1 0.3891746439
set kq2 -0.3695715001

set ld 0.3
set ldbpm 0.05

set de [expr $gradient*$lcav*cos($cavphase/180.0*3.14159265359)-0.0*1e6/1e9*$lcav]
set ebeam $e0

for {set sec 1} {$sec<=11} {incr sec} {
    Girder
    
    Cavity -length $lcav -gradient $gradient -phase $cavphase -type 0
    Drift -length $ld -six_dim $usesixdim
    Cavity -length $lcav -gradient $gradient -phase $cavphase -type 0
    Drift -length $ld -six_dim $usesixdim
    Cavity -length $lcav -gradient $gradient -phase $cavphase -type 0
    Drift -length $ld -six_dim $usesixdim
    Cavity -length $lcav -gradient $gradient -phase $cavphase -type 0
    Drift -length [expr $ld-$lbpm-$ldbpm] -six_dim $usesixdim
    
    set ebeam [expr $ebeam+4.0*$de]
#puts $ebeam

    Bpm -length $lbpm
    Drift -length $ldbpm -six_dim $usesixdim
    Quadrupole -synrad $quad_synrad -length $lquad -strength [expr $kq1*$lquad*$ebeam] -six_dim $usesixdim -thin_lens $numthinlenses
    Drift -length $ld -six_dim $usesixdim

    Cavity -length $lcav -gradient $gradient -phase $cavphase -type 0
    Drift -length $ld -six_dim $usesixdim
    Cavity -length $lcav -gradient $gradient -phase $cavphase -type 0
    Drift -length $ld -six_dim $usesixdim
    Cavity -length $lcav -gradient $gradient -phase $cavphase -type 0
    Drift -length $ld -six_dim $usesixdim
    Cavity -length $lcav -gradient $gradient -phase $cavphase -type 0
    Drift -length [expr $ld-$lbpm-$ldbpm] -six_dim $usesixdim
    
    set ebeam [expr $ebeam+4.0*$de]
#puts $ebeam

    Bpm -length $lbpm
    Drift -length $ldbpm -six_dim $usesixdim
    Quadrupole -synrad $quad_synrad -length $lquad -strength [expr $kq2*$lquad*$ebeam] -six_dim $usesixdim -thin_lens $numthinlenses
    Drift -length $ld -six_dim $usesixdim
}

Girder

Cavity -length $lcav -gradient $gradient -phase $cavphase -type 0
set ebeam [expr $ebeam+$de]

#puts $ebeam
set beamparams(energyafterbooster) $ebeam
puts "Setting beamparams(energyafterbooster)=$beamparams(energyafterbooster)."

#set l_cell [expr 2*($lquad+4*$lcav+$ld1+$ld2+3*$ld3)]
#array set twiss_match [MatchFodo -l1 $lquad -l2 $lquad -K1 [expr -$kq*$lquad] -K2 [expr $kq*$lquad] -L [expr $l_cell/2]]

#puts "Initial Twiss parameters:"
#puts "  beta_x: $twiss_match(beta_x)"
#puts "  beta_y: $twiss_match(beta_y)"
#puts "  alpha_x: $twiss_match(alpha_x)"
#puts "  alpha_y: $twiss_match(alpha_y)"
#puts ""

}
