#
# Turn Around Loop lattice
#
# beamparams=[betax,alphax,emitnx,betay,alphay,emitny,sigmaz,charge,
#             uncespread,echirp,energy,nslice,nmacro,nsigmabunch,nsigmawake]
# units=[m,rad,m*rad,m,rad,m*rad,m,C,1,1/m,eV,1,1,1,1]
# where necessary units were converted to placet units in main.tcl

proc lattice_turn_around_loop {bparray} {
upvar $bparray beamparams

set usesynrad $beamparams(useisr)
set usesixdim 1
set numthinlenses 100
set quad_synrad 0

set e0 $beamparams(energyafterbooster)

set lbend 2.0
set lquad 0.3
set lsext 0.2

set pi 3.141592653589793

# first bend to the left
set msign -1.0
set thetaa [expr $msign*1.3/180*$pi]
set rhoa [expr $lbend/(2*sin($thetaa/2))]
set larca [expr $thetaa*$rhoa]

set thetab [expr $msign*1.7/180*$pi]
set rhob [expr $lbend/(2*sin($thetab/2))]
set larcb [expr $thetab*$rhob]

set ld01 2.00
set ld02 0.60
set ld03 1.90
set ld04 2.35
set ld05 0.40
set ld06 0.30
set ld07 $ld05
set ld08 $ld04
set ld09 $ld03
set ld10 $ld02
set ld11 $ld01


set ld03a [expr $ld03-0.3]
set ld03b 0.1
set ld04a 0.1
set ld04b [expr $ld04-0.3]
set ld09a 0.1
set ld09b [expr $ld09-0.3]


set kq01  1.485783277
set kq02 -1.205627435
set kq03  2.026330565
set kq04 -0.5264630418
set kq05 $kq04
set kq06 $kq03
set kq07 $kq02
set kq08 $kq01

set kq08LQ0808  0.9096194233
set kq01RQ0101  0.9096194233

set ks01 [expr $msign*68.0]
set ks02 [expr $msign*67.7]

# left bending arc

for {set cell 1} {$cell<=10} {incr cell} {
    Girder
    
    if {$cell==1} {
    } else {
        Quadrupole -synrad $quad_synrad -length [expr $lquad/2] -strength [expr $kq01*$lquad/2*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
    }

    Drift -length $ld01 -six_dim $usesixdim
    Sbend -length $larca -angle [expr $thetaa] -synrad $usesynrad -six_dim $usesixdim -thin_lens $numthinlenses -e0 $e0 -E1 [expr $thetaa/2] -E2 [expr $thetaa/2]
    Drift -length $ld02 -six_dim $usesixdim
    Quadrupole -synrad $quad_synrad -length $lquad -strength [expr $kq02*$lquad*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
    Drift -length $ld03a -six_dim $usesixdim
    Multipole -type 3 -length $lsext -e0 $e0 -strength [expr $ks01*$e0*$lsext] -thin_lens $numthinlenses -six_dim $usesixdim
    Drift -length $ld03b -six_dim $usesixdim
    Quadrupole -synrad $quad_synrad -length $lquad -strength [expr $kq03*$lquad*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
    Drift -length $ld04 -six_dim $usesixdim
    Quadrupole -synrad $quad_synrad -length $lquad -strength [expr $kq04*$lquad*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
    Drift -length $ld05 -six_dim $usesixdim
    Sbend -length $larcb -angle [expr $thetab] -synrad $usesynrad -six_dim $usesixdim -thin_lens $numthinlenses -e0 $e0 -E1 [expr $thetab/2] -E2 [expr $thetab/2]
    Drift -length $ld06 -six_dim $usesixdim
    Sbend -length $larcb -angle [expr $thetab] -synrad $usesynrad -six_dim $usesixdim -thin_lens $numthinlenses -e0 $e0 -E1 [expr $thetab/2] -E2 [expr $thetab/2]
    Drift -length $ld07 -six_dim $usesixdim
    Quadrupole -synrad $quad_synrad -length $lquad -strength [expr $kq05*$lquad*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
    Drift -length $ld08 -six_dim $usesixdim
    Quadrupole -synrad $quad_synrad -length $lquad -strength [expr $kq06*$lquad*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
    Drift -length $ld09a -six_dim $usesixdim
    Multipole -type 3 -length $lsext -e0 $e0 -strength [expr $ks02*$e0*$lsext] -thin_lens $numthinlenses -six_dim $usesixdim
    Drift -length $ld09b -six_dim $usesixdim
    Quadrupole -synrad $quad_synrad -length $lquad -strength [expr $kq07*$lquad*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
    Drift -length $ld10 -six_dim $usesixdim
    Sbend -length $larca -angle [expr $thetaa] -synrad $usesynrad -six_dim $usesixdim -thin_lens $numthinlenses -e0 $e0 -E1 [expr $thetaa/2] -E2 [expr $thetaa/2]
    Drift -length $ld11 -six_dim $usesixdim
    
    if {$cell==10} {
    } else {
        Quadrupole -synrad $quad_synrad -length [expr $lquad/2] -strength [expr $kq08*$lquad/2*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
    }
}

# match left and right arcs
set ldm1 16.24
set ldm2 [expr 23.96-$ldm1]
set ldm3 10.9
set ldmc 20.0

set kqm1   0.853267766
set kqm2  -0.311868745
set kqm3   0.04794788438
set kqmc1  0.1853652768
set kqmc2 -0.2028929072

Girder
Quadrupole -synrad $quad_synrad -length $lquad -strength [expr $kqm1*$lquad*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ldm1 -six_dim $usesixdim
Quadrupole -synrad $quad_synrad -length $lquad -strength [expr $kqm2*$lquad*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ldm2 -six_dim $usesixdim
Quadrupole -synrad $quad_synrad -length $lquad -strength [expr $kqm3*$lquad*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ldm3 -six_dim $usesixdim

for {set cell 1} {$cell<=5} {incr cell} {
    Quadrupole -synrad $quad_synrad -length $lquad -strength [expr $kqmc1*$lquad*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
    Drift -length $ldmc -six_dim $usesixdim
    Quadrupole -synrad $quad_synrad -length $lquad -strength [expr $kqmc2*$lquad*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
    Drift -length $ldmc -six_dim $usesixdim
}

Quadrupole -synrad $quad_synrad -length $lquad -strength [expr $kqmc1*$lquad*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ldm3 -six_dim $usesixdim
Quadrupole -synrad $quad_synrad -length $lquad -strength [expr $kqm3*$lquad*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ldm2 -six_dim $usesixdim
Quadrupole -synrad $quad_synrad -length $lquad -strength [expr $kqm2*$lquad*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ldm1 -six_dim $usesixdim
Quadrupole -synrad $quad_synrad -length $lquad -strength [expr $kqm1*$lquad*$e0] -six_dim $usesixdim -thin_lens $numthinlenses

# right bending arc

for {set cell 1} {$cell<=40} {incr cell} {
    Girder
    
    if {$cell==1} {
    } else {
        Quadrupole -synrad $quad_synrad -length [expr $lquad/2] -strength [expr $kq01*$lquad/2*$e0] -thin_lens $numthinlenses -six_dim $usesixdim
    }
    Drift -length $ld01 -six_dim $usesixdim
    Sbend -length $larca -angle [expr -$thetaa] -six_dim $usesixdim -synrad $usesynrad -thin_lens $numthinlenses -e0 $e0 -E1 [expr -$thetaa/2] -E2 [expr -$thetaa/2]
    Drift -length $ld02 -six_dim $usesixdim
    Quadrupole -synrad $quad_synrad -length $lquad -strength [expr $kq02*$lquad*$e0] -thin_lens $numthinlenses -six_dim $usesixdim
    Drift -length $ld03a -six_dim $usesixdim
    Multipole -type 3 -length $lsext -e0 $e0 -strength [expr -$ks01*$e0*$lsext] -thin_lens $numthinlenses -six_dim $usesixdim
    Drift -length $ld03b -six_dim $usesixdim
    Quadrupole -synrad $quad_synrad -length $lquad -strength [expr $kq03*$lquad*$e0] -thin_lens $numthinlenses -six_dim $usesixdim
    Drift -length $ld04 -six_dim $usesixdim
    Quadrupole -synrad $quad_synrad -length $lquad -strength [expr $kq04*$lquad*$e0] -thin_lens $numthinlenses -six_dim $usesixdim
    Drift -length $ld05 -six_dim $usesixdim
    Sbend -length $larcb -angle [expr -$thetab] -six_dim $usesixdim -synrad $usesynrad -thin_lens $numthinlenses -e0 $e0 -E1 [expr -$thetab/2] -E2 [expr -$thetab/2]
    Drift -length $ld06 -six_dim $usesixdim
    Sbend -length $larcb -angle [expr -$thetab] -six_dim $usesixdim -synrad $usesynrad -thin_lens $numthinlenses -e0 $e0 -E1 [expr -$thetab/2] -E2 [expr -$thetab/2]
    Drift -length $ld07 -six_dim $usesixdim
    Quadrupole -synrad $quad_synrad -length $lquad -strength [expr $kq05*$lquad*$e0] -thin_lens $numthinlenses -six_dim $usesixdim
    Drift -length $ld08 -six_dim $usesixdim
    Quadrupole -synrad $quad_synrad -length $lquad -strength [expr $kq06*$lquad*$e0] -thin_lens $numthinlenses -six_dim $usesixdim
    Drift -length $ld09a -six_dim $usesixdim
    Multipole -type 3 -length $lsext -e0 $e0 -strength [expr -$ks02*$e0*$lsext] -thin_lens $numthinlenses -six_dim $usesixdim
    Drift -length $ld09b -six_dim $usesixdim
    Quadrupole -synrad $quad_synrad -length $lquad -strength [expr $kq07*$lquad*$e0] -thin_lens $numthinlenses -six_dim $usesixdim
    Drift -length $ld10 -six_dim $usesixdim
    Sbend -length $larca -angle [expr -$thetaa] -six_dim $usesixdim -synrad $usesynrad -thin_lens $numthinlenses -e0 $e0 -E1 [expr -$thetaa/2] -E2 [expr -$thetaa/2]
    Drift -length $ld11 -six_dim $usesixdim
    Quadrupole -synrad $quad_synrad -length [expr $lquad/2.0] -strength [expr $kq08*$lquad/2.0*$e0] -thin_lens $numthinlenses -six_dim $usesixdim
}


}
