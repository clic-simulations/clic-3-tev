#
# this file contains all procedure required to calculate longitudinal and transverse wake fields
# as well as setup for cavities
#
# rfparams=[gradient,phase,wavelength,a,g,l]
# units=[eV/m,Degree,m,m,m,m]
#


# initialize cavities and set longrange wake fields (not yet used)
proc initialize_cavities {rfparray wakelongrange} {
upvar $rfparray rfparams

WakeSet $wakelongrange {}
InjectorCavityDefine -lambda $rfparams(lambda) -wakelong $wakelongrange
}

#
# transverse wakefield
# s is given in micro metres
# return value is in V/pCm^2
#
proc w_transv {s sparray} {
    upvar $sparray structureparams
    set a $structureparams(a)
    set g $structureparams(g)
    set l $structureparams(l)
    set s0 [expr 0.169*pow($a,1.79)*pow($g,0.38)*pow($l,-1.17)]
    return [expr 4.0*377.0*3e8*$s0*1e-12/(acos(-1.0)*pow($a,4))*(1.0-(1.0+sqrt($s*1e-6/$s0))*exp(-sqrt($s*1e-6/$s0)))]
}

proc w_transv2 {s sparray} {
    upvar $sparray structureparams
    set a $structureparams(a)
    set g $structureparams(g)
    set l $structureparams(l)
    set tmp [expr $g/$l]
    set alpha [expr 1.0-0.4648*sqrt($tmp)-(1.0-2.0*0.4648)*$tmp]
    set s0 [expr $g/8.0*pow($a/($l*$alpha),2)]
    return [expr 4.0*377.0*3e8*$s0*1e-12/(acos(-1.0)*pow($a,4))*(1.0-(1.0+sqrt($s*1e-6/$s0))*exp(-sqrt($s*1e-6/$s0)))]
}

#
# longitudinal wakefield
# s is given in micro metres
# return value is in V/pCm
#
proc w_long {s sparray} {
    upvar $sparray structureparams
    set a $structureparams(a)
    set g $structureparams(g)
    set l $structureparams(l)
    set tmp [expr $g/$l]
    set alpha [expr 1.0-0.4648*sqrt($tmp)-(1.0-2.0*0.4648)*$tmp]
    set s0 [expr $g/8.0*pow($a/($l*$alpha),2)]
    return [expr 377.0*3e8*1e-12/(acos(-1.0)*$a*$a)*exp(-sqrt($s*1e-6/$s0))]
}


#
# routines to calculate the wakefields
# a file is created with name $filename that can be read when a beam is created
# z_list contains longitudinal positions and weights of the slices
#
# unit to store in the file is MV/pCm^2
# multiply charge with factor for pC and MV
#
proc cavity_wake_trans {charge z_list sparray} {
    upvar $sparray structureparams

    set tmp {}
    set z_l {}
    foreach l $z_list {
	lappend z_l $l
	set z0 [lindex $l 0]
	foreach j $z_l {
	    set z [lindex $j 0]
	    lappend tmp [expr 1.602177e-7*$charge*1e-6*[w_transv2 [expr $z0-$z] structureparams]]
	}
    }
    return $tmp
}

proc cavity_wake_long {charge z_list sparray} {
    upvar $sparray structureparams
    
    set z_l {}
    set n [llength $z_list]
    set tmp {}
    foreach l $z_list {
	set z0 [lindex $l 0]
	set wgt0 [expr $charge*[lindex $l 1]]
	set sum 0.0
	foreach j $z_l {
	    set z [lindex $j 0]
	    set wgt [expr [lindex $j 1]*$charge]
	    set sum [expr $sum+$wgt*[w_long [expr $z0-$z] structureparams]]
	}
	set sum [expr $sum+0.5*$wgt0*[w_long 0.0 structureparams]]
	lappend z_l $l
	set sum [expr -($sum*1.602177e-7*1e-6)]
	lappend tmp "$z0 $sum"
    }
    return $tmp
}

proc cavity_wake {filename charge sigmamin sigmamax sigmaz nslice rfparray} {
# create a list of longitudinal and transverse wake kicks
# required by the InjectorBeam command
    upvar $rfparray rfparams
    set structureparams(a) $rfparams(a)
    set structureparams(g) $rfparams(g)
    set structureparams(l) $rfparams(l)
    
    set file [open $filename w]
    puts $file $nslice
    set gausslong [GaussList -min $sigmamin -max $sigmamax -sigma $sigmaz -charge 1.0 -n_slices [expr 5*$nslice]]
    set wakelong [cavity_wake_long $charge $gausslong structureparams]
    set gausstrans [GaussList -min $sigmamin -max $sigmamax -sigma $sigmaz -charge 1.0 -n_slices [expr $nslice]]
    set waketrans [cavity_wake_trans $charge $gausstrans structureparams]
    set x [lindex $wakelong [expr int($nslice/2)*5+2]]
    puts $file [lindex $x 1]
    for {set i 0} {$i<$nslice} {incr i} {
	set x [lindex $wakelong [expr 2+5*$i]]
	puts $file "$x [lindex [lindex $gausstrans $i] 1]"
    }
    foreach x $waketrans {
	puts $file $x
    }
    close $file
}


proc cavity_wake_zero {filename sigmamin sigmamax sigmaz nslice} {
# create a list of longitudinal and transverse wake kicks
# required by the InjectorBeam command
# all kicks are set to 0
    set file [open $filename w]
    puts $file $nslice
    puts $file "1.0"
    set gl [GaussList -min $sigmamin -max $sigmamax -sigma $sigmaz -charge 1.0 -n_slices $nslice]
    foreach x $gl {
        puts $file "[lindex $x 0] 0 [lindex $x 1]"
    }
    set maxi [expr ($nslice+1)*$nslice/2]
    for {set i 0} {$i<$maxi} {incr i} {
        puts $file "0"
    }
    close $file
}

