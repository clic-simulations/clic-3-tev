#!/bin/bash
#
wakes="$1"
csr="$2"
isr="$3"

if $wakes!="1" ; then
wakes="0"
fi
if $csr!="1" ; then
csr="0"
fi
if $isr!="1" ; then
isr="0"
fi

latticedir="./Lattices_e+"
latticefile="rtml_e+.lte"

cp $latticedir/0000_start.lte $latticefile
echo "
% $csr sto global_csr
% $isr sto global_isr
" >> $latticefile
echo -e "\n\n\n!----------------------------------------------------------------------\n\n\n" >> $latticefile
sed "s/XXXX/0010/g" $latticedir/0010_match_dr_to_rtml.lte >> $latticefile
echo -e "\n\n\n!----------------------------------------------------------------------\n\n\n" >> $latticefile
sed "s/XXXX/0020/g" $latticedir/0020_diagnostics_1.lte >> $latticefile
echo -e "\n\n\n!----------------------------------------------------------------------\n\n\n" >> $latticefile
sed "s/XXXX/0030/g" $latticedir/0030_dump_and_match_diag_to_sr.lte >> $latticefile
echo -e "\n\n\n!----------------------------------------------------------------------\n\n\n" >> $latticefile
sed "s/XXXX/0040/g" $latticedir/0040_spin_rotator.lte >> $latticefile
echo -e "\n\n\n!----------------------------------------------------------------------\n\n\n" >> $latticefile
sed "s/XXXX/0050/g" $latticedir/0050_match_sr_to_bc1_rf.lte >> $latticefile
echo -e "\n\n\n!----------------------------------------------------------------------\n\n\n" >> $latticefile
if $wakes=="1" ; then
sed "s/XXXX/0060/g" $latticedir/0060_bc1_rf_with_wakes.lte >> $latticefile
else
sed "s/XXXX/0060/g" $latticedir/0060_bc1_rf.lte >> $latticefile
fi
echo -e "\n\n\n!----------------------------------------------------------------------\n\n\n" >> $latticefile
sed "s/XXXX/0070/g" $latticedir/0070_match_bc1_rf_to_chicane.lte >> $latticefile
echo -e "\n\n\n!----------------------------------------------------------------------\n\n\n" >> $latticefile
sed "s/XXXX/0080/g" $latticedir/0080_bc1_chicane.lte >> $latticefile
echo -e "\n\n\n!----------------------------------------------------------------------\n\n\n" >> $latticefile
sed "s/XXXX/0090/g" $latticedir/0090_match_bc1_to_diag.lte >> $latticefile
echo -e "\n\n\n!----------------------------------------------------------------------\n\n\n" >> $latticefile
sed "s/XXXX/0100/g" $latticedir/0100_diagnostics_2.lte >> $latticefile
echo -e "\n\n\n!----------------------------------------------------------------------\n\n\n" >> $latticefile
sed "s/XXXX/0110/g" $latticedir/0110_dump_and_match_diag_to_booster.lte >> $latticefile
echo -e "\n\n\n!----------------------------------------------------------------------\n\n\n" >> $latticefile
if $wakes=="1" ; then
sed "s/XXXX/0120/g" $latticedir/0120_booster_linac_with_wakes.lte >> $latticefile
else
sed "s/XXXX/0120/g" $latticedir/0120_booster_linac.lte >> $latticefile
fi
echo -e "\n\n\n!----------------------------------------------------------------------\n\n\n" >> $latticefile
sed "s/XXXX/0130/g" $latticedir/0130_dump_and_match_booster_to_ca.lte >> $latticefile
echo -e "\n\n\n!----------------------------------------------------------------------\n\n\n" >> $latticefile
sed "s/XXXX/0140/g" $latticedir/0140_central_arc.lte >> $latticefile
echo -e "\n\n\n!----------------------------------------------------------------------\n\n\n" >> $latticefile
sed "s/XXXX/0150/g" $latticedir/0150_vertical_transfer.lte >> $latticefile
echo -e "\n\n\n!----------------------------------------------------------------------\n\n\n" >> $latticefile
sed "s/XXXX/0160/g" $latticedir/0160_match_vt_to_ltl.lte >> $latticefile
echo -e "\n\n\n!----------------------------------------------------------------------\n\n\n" >> $latticefile
sed "s/XXXX/0170/g" $latticedir/0170_long_transfer_line.lte >> $latticefile
echo -e "\n\n\n!----------------------------------------------------------------------\n\n\n" >> $latticefile
sed "s/XXXX/0180/g" $latticedir/0180_dump_and_match_ltl_to_tal.lte >> $latticefile
echo -e "\n\n\n!----------------------------------------------------------------------\n\n\n" >> $latticefile
sed "s/XXXX/0190/g" $latticedir/0190_turn_around_loop.lte >> $latticefile
echo -e "\n\n\n!----------------------------------------------------------------------\n\n\n" >> $latticefile
sed "s/XXXX/0200/g" $latticedir/0200_match_tal_to_bc2_rf.lte >> $latticefile
echo -e "\n\n\n!----------------------------------------------------------------------\n\n\n" >> $latticefile
if $wakes=="1" ; then
sed "s/XXXX/0210/g" $latticedir/0210_bc2_rf_with_wakes.lte >> $latticefile
else
sed "s/XXXX/0210/g" $latticedir/0210_bc2_rf.lte >> $latticefile
fi
echo -e "\n\n\n!----------------------------------------------------------------------\n\n\n" >> $latticefile
sed "s/XXXX/0220/g" $latticedir/0220_match_bc2_rf_to_chicane_1.lte >> $latticefile
echo -e "\n\n\n!----------------------------------------------------------------------\n\n\n" >> $latticefile
sed "s/XXXX/0230/g" $latticedir/0230_bc2_chicane_1.lte >> $latticefile
echo -e "\n\n\n!----------------------------------------------------------------------\n\n\n" >> $latticefile
sed "s/XXXX/0240/g" $latticedir/0240_match_bc2_chicanes.lte >> $latticefile
echo -e "\n\n\n!----------------------------------------------------------------------\n\n\n" >> $latticefile
sed "s/XXXX/0250/g" $latticedir/0250_bc2_chicane_2.lte >> $latticefile
echo -e "\n\n\n!----------------------------------------------------------------------\n\n\n" >> $latticefile
sed "s/XXXX/0260/g" $latticedir/0260_match_bc2_to_diag.lte >> $latticefile
echo -e "\n\n\n!----------------------------------------------------------------------\n\n\n" >> $latticefile
sed "s/XXXX/0270/g" $latticedir/0270_diagnostics_3.lte >> $latticefile
echo -e "\n\n\n!----------------------------------------------------------------------\n\n\n" >> $latticefile
sed "s/XXXX/0280/g" $latticedir/0280_dump_and_match_rtml_to_main_linac.lte >> $latticefile
echo -e "\n\n\n!----------------------------------------------------------------------\n\n\n" >> $latticefile

echo "

% 0.65e-9 sto qbunch
% 250000 sto npart
% qbunch npart / sto qpart

Q0: CHARGE, TOTAL=\"qbunch\", PER_PARTICLE=\"qpart\"

W01: WATCH, FILENAME=\"watch_01.dat\"
W02: WATCH, FILENAME=\"watch_02.dat\"
W03: WATCH, FILENAME=\"watch_03.dat\"
W04: WATCH, FILENAME=\"watch_04.dat\"
W05: WATCH, FILENAME=\"watch_05.dat\"
W06: WATCH, FILENAME=\"watch_06.dat\"
W07: WATCH, FILENAME=\"watch_07.dat\"
W08: WATCH, FILENAME=\"watch_08.dat\"
W09: WATCH, FILENAME=\"watch_09.dat\"
W10: WATCH, FILENAME=\"watch_10.dat\"
W11: WATCH, FILENAME=\"watch_11.dat\"
W12: WATCH, FILENAME=\"watch_12.dat\"
W13: WATCH, FILENAME=\"watch_13.dat\"
W14: WATCH, FILENAME=\"watch_14.dat\"
W15: WATCH, FILENAME=\"watch_15.dat\"
W16: WATCH, FILENAME=\"watch_16.dat\"
W17: WATCH, FILENAME=\"watch_17.dat\"
W18: WATCH, FILENAME=\"watch_18.dat\"
W19: WATCH, FILENAME=\"watch_19.dat\"
W20: WATCH, FILENAME=\"watch_20.dat\"
W21: WATCH, FILENAME=\"watch_21.dat\"
W22: WATCH, FILENAME=\"watch_22.dat\"
W23: WATCH, FILENAME=\"watch_23.dat\"
W24: WATCH, FILENAME=\"watch_24.dat\"
W25: WATCH, FILENAME=\"watch_25.dat\"
W26: WATCH, FILENAME=\"watch_26.dat\"
W27: WATCH, FILENAME=\"watch_27.dat\"
W28: WATCH, FILENAME=\"watch_28.dat\"
W29: WATCH, FILENAME=\"watch_29.dat\"

RTML: LINE=(Q0,&
            LINE0010,LINE0020,LINE0030,LINE0040,LINE0050,LINE0060,&
            LINE0070,LINE0080,LINE0090,LINE0100,LINE0110,LINE0120,&
            LINE0130,LINE0140,LINE0150,LINE0160,LINE0170,LINE0180,&
            LINE0190,LINE0200,LINE0210,LINE0220,LINE0230,LINE0240,&
            LINE0250,LINE0260,LINE0270,LINE0280
           )

RTMLE: LINE=(Q0,&
             LINE0010E,LINE0020E,LINE0030E,LINE0040E,LINE0050E,LINE0060E,&
             LINE0070E,LINE0080E,LINE0090E,LINE0100E,LINE0110E,LINE0120E,&
             LINE0130E,LINE0140E,LINE0150E,LINE0160E,LINE0170E,LINE0180E,&
             LINE0190E,LINE0200E,LINE0210E,LINE0220E,LINE0230E,LINE0240E,&
             LINE0250E,LINE0260E,LINE0270E,LINE0280E
            )

RTMLCSR: LINE=(Q0,&
               LINE0010CSR,LINE0020CSR,LINE0030CSR,LINE0040CSR,LINE0050CSR,LINE0060CSR,&
               LINE0070CSR,LINE0080CSR,LINE0090CSR,LINE0100CSR,LINE0110CSR,LINE0120CSR,&
               LINE0130CSR,LINE0140CSR,LINE0150CSR,LINE0160CSR,LINE0170CSR,LINE0180CSR,&
               LINE0190CSR,LINE0200CSR,LINE0210CSR,LINE0220CSR,LINE0230CSR,LINE0240CSR,&
               LINE0250CSR,LINE0260CSR,LINE0270CSR,LINE0280CSR
              )

RTMLW: LINE=(Q0,&
             W01,LINE0010,W02,LINE0020,W03,LINE0030,W04,LINE0040,W05,LINE0050,W06,LINE0060,&
             W07,LINE0070,W08,LINE0080,W09,LINE0090,W10,LINE0100,W11,LINE0110,W12,LINE0120,&
             W13,LINE0130,W14,LINE0140,W15,LINE0150,W16,LINE0160,W17,LINE0170,W18,LINE0180,&
             W19,LINE0190,W20,LINE0200,W21,LINE0210,W22,LINE0220,W23,LINE0230,W24,LINE0240,&
             W25,LINE0250,W26,LINE0260,W27,LINE0270,W28,LINE0280,W29
            )

RTMLEW: LINE=(Q0,&
              W01,LINE0010E,W02,LINE0020E,W03,LINE0030E,W04,LINE0040E,W05,LINE0050E,W06,LINE0060E,&
              W07,LINE0070E,W08,LINE0080E,W09,LINE0090E,W10,LINE0100E,W11,LINE0110E,W12,LINE0120E,&
              W13,LINE0130E,W14,LINE0140E,W15,LINE0150E,W16,LINE0160E,W17,LINE0170E,W18,LINE0180E,&
              W19,LINE0190E,W20,LINE0200E,W21,LINE0210E,W22,LINE0220E,W23,LINE0230E,W24,LINE0240E,&
              W25,LINE0250E,W26,LINE0260E,W27,LINE0270E,W28,LINE0280E,W29
             )
RTMLCSRW: LINE=(Q0,&
                W01,LINE0010CSR,W02,LINE0020CSR,W03,LINE0030CSR,W04,LINE0040CSR,W05,LINE0050CSR,W06,LINE0060CSR,&
                W07,LINE0070CSR,W08,LINE0080CSR,W09,LINE0090CSR,W10,LINE0100CSR,W11,LINE0110CSR,W12,LINE0120CSR,&
                W13,LINE0130CSR,W14,LINE0140CSR,W15,LINE0150CSR,W16,LINE0160CSR,W17,LINE0170CSR,W18,LINE0180CSR,&
                W19,LINE0190CSR,W20,LINE0200CSR,W21,LINE0210CSR,W22,LINE0220CSR,W23,LINE0230CSR,W24,LINE0240CSR,&
                W25,LINE0250CSR,W26,LINE0260CSR,W27,LINE0270CSR,W28,LINE0280CSR,W29
               )

" >> $latticefile
