#!/bin/bash
#This script runs MPI simulations. Hence, you have to have an mpihosts.txt file containing  the correct nodes.
#
#You must specify the correct working directory.
#All required files will be copied there and it will also contain the output.
#e.g. outputdir=/afs/cern.ch/project/parc/scratch/users/fstulle/Elegant_RTML_Base_v21/output
#
outputdir=./simrun_e-
mkdir $outputdir
./create_rtml_e-_lte.sh
./create_rtml_e+_lte.sh
cp rtml_e+.lte $outputdir
cp rtml_e-.lte $outputdir
cp rtml.ele $outputdir/rtml.ele
cp wake_bc1.sdds $outputdir/wake_bc1.sdds
cp wake_bc2.sdds $outputdir/wake_bc2.sdds
cp wake_booster.sdds $outputdir/wake_booster.sdds
cp mpihosts_all.txt $outputdir/mpihosts.txt
cd $outputdir
#
#
#-----------------------------------------------------------------
#
elegant rtml.ele > Pelegant.out 2> Pelegant.err
#mpirun -np 16 --hostfile mpihosts.txt Pelegant rtml.ele > Pelegant.out 2> Pelegant.err
#
sdds2plaindata rtml.twi rtml.twi.txt -outputMode=ascii -separator=' ' -col=s -col=betax -col=alphax -col=psix -col=betay -col=alphay -col=ElementName -noRowCount
sdds2plaindata rtml.mat rtml.mat.txt -outputMode=ascii -separator=' ' -col=s -col=R16 -col=R52 -col=R56 -col=R55 -col=R36 -noRowCount
#sdds2plaindata rtml.mat rtml.mat.asc -outputMode=ascii -separator=' ' -col=s -col=ElementName -col=ElementOccurence -col=ElementType -noRowCount
sdds2plaindata rtml.s rtml.s.emit -outputMode=ascii -separator=' ' -col=s -col=enx -col=ecnx -col=eny -col=ecny -noRowCount
sdds2plaindata rtml.s rtml.s.txt2 -outputMode=ascii -separator=' ' -col=s -col=Ss -col=Sdelta -noRowCount
sddsprocess rtml.cen rtml.cen.new -def=col,pCentralGeV,"pCentral 0.00051099891 *"
sdds2plaindata rtml.cen.new rtml.cen.txt -outputMode=ascii -separator=' ' -col=s -col=Cs -col=Cdelta -col=pCentralGeV -noRowCount
#
sdds2plaindata rtml.flr rtml.flr.txt -outputMode=ascii -separator=' ' -noRowCount -col=s -col=X -col=Y -col=Z -col=theta -col=phi -col=psi -col=ElementName
#
#
#sddsprocess rtml.mat rtml_new.mat -define=param,Sdelta0,1.1e-3 -define=param,Ss0,1300e-6,units=m -define=col,Ss,"Sdelta0 R56 * sqr Ss0 R55 * sqr + sqrt",units=m
#sddsplot -col=s,Ss rtml.s rtml_new.mat -graph=line,vary &
