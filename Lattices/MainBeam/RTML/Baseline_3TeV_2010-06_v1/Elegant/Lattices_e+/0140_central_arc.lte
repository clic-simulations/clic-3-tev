!
! CLIC Main Beam RTML
! Central arc for positrons
! contains the first arc of the horizontal dog-leg which is required to
! compensate the horizontal offset with respect to the main tunnnel axis
!
! initial Twiss: betax=57.1, alphax=0.0, betay=4.6, alphay=0.0
! final Twiss: betax=55.21270616, alphax=-12.44143995,
!              betay=4.760445891, alphay=1.081532187

% global_csr sto XXXX_usecsr
% global_isr sto XXXX_useisr
% global_isr sto XXXX_usesyn

% 1 atan 4 * sto Pi

! general parameters of arc

% 2.0 sto XXXX_lbend
% 0.3 sto XXXX_lquad
% 0.2 sto XXXX_lsext

! dipoles

% -1.3 180 / Pi * sto XXXX_phia
% XXXX_lbend 2 / XXXX_phia 2 / sin / sto XXXX_r0a
% XXXX_phia XXXX_r0a * sto XXXX_larca

% -1.7 180 / Pi * sto XXXX_phib
% XXXX_lbend 2 / XXXX_phib 2 / sin / sto XXXX_r0b
% XXXX_phib XXXX_r0b * sto XXXX_larcb

XXXX_BA : SBEND, L="XXXX_larca", ANGLE="XXXX_phia 1.0 *",&
          E1 = "XXXX_phia 2.0 /", E2 = "XXXX_phia 2.0 /"


XXXX_BAE : CSBEND, L="XXXX_larca", ANGLE="XXXX_phia 1.0 *",&
           E1 = "XXXX_phia 2.0 /", E2 = "XXXX_phia 2.0 /",&
           INTEGRATION_ORDER = 4,ISR="XXXX_useisr",SYNCH_RAD="XXXX_usesyn",&
           USE_RAD_DIST=0

XXXX_BACSR : CSRCSBEND, L = "XXXX_larca", ANGLE = "XXXX_phia 1.0 *",&
             E1 = "XXXX_phia 2.0 /", E2 = "XXXX_phia 2.0 /",&
	     INTEGRATION_ORDER = 4,&
	     CSR = "XXXX_usecsr", ISR = "XXXX_useisr",SYNCH_RAD="XXXX_usesyn",&
             N_KICKS = 1000,&
	     BINS = 2000, SG_HALFWIDTH = 10,&
	     DERBENEV_CRITERION_MODE = "disable"
!	      OUTPUT_FILE = "wakes1.dat",&
!	      OUTPUT_INTERVAL= 10,&
!	      PARTICLE_OUTPUT_FILE = "part1.dat", &
!	      PARTICLE_OUTPUT_INTERVAL = 10,&
!	      SLICE_ANALYSIS_INTERVAL = 5


XXXX_BB : SBEND, L="XXXX_larcb", ANGLE="XXXX_phib 1.0 *",&
          E1 = "XXXX_phib 2.0 /", E2 = "XXXX_phib 2.0 /"

XXXX_BBE : CSBEND, L="XXXX_larcb", ANGLE="XXXX_phib 1.0 *",&
           E1 = "XXXX_phib 2.0 /", E2 = "XXXX_phib 2.0 /",&
           INTEGRATION_ORDER = 4,ISR="XXXX_useisr",SYNCH_RAD="XXXX_usesyn",&
           USE_RAD_DIST=0

XXXX_BBCSR : CSRCSBEND, L = "XXXX_larcb", ANGLE = "XXXX_phib 1.0 *",&
             E1 = "XXXX_phib 2.0 /", E2 = "XXXX_phib 2.0 /",&
	     INTEGRATION_ORDER = 4,&
	     CSR = "XXXX_usecsr", ISR = "XXXX_useisr",SYNCH_RAD="XXXX_usesyn",&
             N_KICKS = 1000,&
	     BINS = 2000, SG_HALFWIDTH = 10,&
	     DERBENEV_CRITERION_MODE = "disable"
!	      OUTPUT_FILE = "wakes1.dat",&
!	      OUTPUT_INTERVAL= 10,&
!	      PARTICLE_OUTPUT_FILE = "part1.dat", &
!	      PARTICLE_OUTPUT_INTERVAL = 10,&
!	      SLICE_ANALYSIS_INTERVAL = 5


! quadrupoles

% 1.485783277  sto XXXX_kq1
%-1.205627435  sto XXXX_kq2
% 2.026330565  sto XXXX_kq3
%-0.5264630418 sto XXXX_kq4

% XXXX_kq4 sto XXXX_kq5
% XXXX_kq3 sto XXXX_kq6
% XXXX_kq2 sto XXXX_kq7
% XXXX_kq1 sto XXXX_kq8

XXXX_Q1: QUAD, L="XXXX_lquad 2.0 /", K1="XXXX_kq1"
XXXX_Q2: QUAD, L="XXXX_lquad", K1="XXXX_kq2"
XXXX_Q3: QUAD, L="XXXX_lquad", K1="XXXX_kq3"
XXXX_Q4: QUAD, L="XXXX_lquad", K1="XXXX_kq4"
XXXX_Q5: QUAD, L="XXXX_lquad", K1="XXXX_kq5"
XXXX_Q6: QUAD, L="XXXX_lquad", K1="XXXX_kq6"
XXXX_Q7: QUAD, L="XXXX_lquad", K1="XXXX_kq7"
XXXX_Q8: QUAD, L="XXXX_lquad 2.0 /", K1="XXXX_kq8"

! drifts

% 2.00 sto XXXX_ld01
% 0.60 sto XXXX_ld02
% 1.90 sto XXXX_ld03
% 2.35 sto XXXX_ld04
% 0.40 sto XXXX_ld05
% 0.30 sto XXXX_ld06

% XXXX_ld05 sto XXXX_ld07
% XXXX_ld04 sto XXXX_ld08
% XXXX_ld03 sto XXXX_ld09
% XXXX_ld02 sto XXXX_ld10
% XXXX_ld01 sto XXXX_ld11

% XXXX_ld03 0.3 - sto XXXX_ld03a
% 0.10  sto XXXX_ld03b
% XXXX_ld03b sto XXXX_ld09a
% XXXX_ld03a sto XXXX_ld09b

XXXX_D01: DRIFT, L="XXXX_ld01"
XXXX_D02: DRIFT, L="XXXX_ld02"
XXXX_D03: DRIFT, L="XXXX_ld03"
XXXX_D03A: DRIFT, L="XXXX_ld03a"
XXXX_D03B: DRIFT, L="XXXX_ld03b"
XXXX_D04: DRIFT, L="XXXX_ld04"
XXXX_D05: DRIFT, L="XXXX_ld05"
XXXX_D06: DRIFT, L="XXXX_ld06"
XXXX_D07: DRIFT, L="XXXX_ld07"
XXXX_D08: DRIFT, L="XXXX_ld08"
XXXX_D09A: DRIFT, L="XXXX_ld09a"
XXXX_D09B: DRIFT, L="XXXX_ld09b"
XXXX_D10: DRIFT, L="XXXX_ld10"
XXXX_D11: DRIFT, L="XXXX_ld11"

XXXX_D01E: EDRIFT, L="XXXX_ld01"
XXXX_D02E: EDRIFT, L="XXXX_ld02"
XXXX_D03E: EDRIFT, L="XXXX_ld03"
XXXX_D03AE: EDRIFT, L="XXXX_ld03a"
XXXX_D03BE: EDRIFT, L="XXXX_ld03b"
XXXX_D04E: EDRIFT, L="XXXX_ld04"
XXXX_D05E: EDRIFT, L="XXXX_ld05"
XXXX_D06E: EDRIFT, L="XXXX_ld06"
XXXX_D07E: EDRIFT, L="XXXX_ld07"
XXXX_D08E: EDRIFT, L="XXXX_ld08"
XXXX_D09AE: EDRIFT, L="XXXX_ld09a"
XXXX_D09BE: EDRIFT, L="XXXX_ld09b"
XXXX_D10E: EDRIFT, L="XXXX_ld10"
XXXX_D11E: EDRIFT, L="XXXX_ld11"

XXXX_D01CSR: CSRDRIFT, L="XXXX_ld01", &
             CSR=0, DZ=0.01, USE_STUPAKOV=1
XXXX_D02CSR: CSRDRIFT, L="XXXX_ld02", &
             CSR=0, DZ=0.01, USE_STUPAKOV=1
XXXX_D03CSR: CSRDRIFT, L="XXXX_ld03", &
             CSR="XXXX_usecsr", DZ=0.01, USE_STUPAKOV=1
XXXX_D03ACSR: CSRDRIFT, L="XXXX_ld03a", &
              CSR="XXXX_usecsr", DZ=0.01, USE_STUPAKOV=1
XXXX_D03BCSR: CSRDRIFT, L="XXXX_ld03b", &
              CSR="XXXX_usecsr", DZ=0.01, USE_STUPAKOV=1
XXXX_D04CSR: CSRDRIFT, L="XXXX_ld04", &
             CSR="XXXX_usecsr", DZ=0.01, USE_STUPAKOV=1
XXXX_D05CSR: CSRDRIFT, L="XXXX_ld05", &
             CSR="XXXX_usecsr", DZ=0.01, USE_STUPAKOV=1
XXXX_D06CSR: CSRDRIFT, L="XXXX_ld06", &
             CSR="XXXX_usecsr", DZ=0.01, USE_STUPAKOV=1
XXXX_D07CSR: CSRDRIFT, L="XXXX_ld07", &
             CSR="XXXX_usecsr", DZ=0.01, USE_STUPAKOV=1
XXXX_D08CSR: CSRDRIFT, L="XXXX_ld08", &
             CSR="XXXX_usecsr", DZ=0.01, USE_STUPAKOV=1
XXXX_D09ACSR: CSRDRIFT, L="XXXX_ld09a", &
              CSR="XXXX_usecsr", DZ=0.01, USE_STUPAKOV=1
XXXX_D09BCSR: CSRDRIFT, L="XXXX_ld09b", &
              CSR="XXXX_usecsr", DZ=0.01, USE_STUPAKOV=1
XXXX_D10CSR: CSRDRIFT, L="XXXX_ld10", &
             CSR="XXXX_usecsr", DZ=0.01, USE_STUPAKOV=1
XXXX_D11CSR: CSRDRIFT, L="XXXX_ld11", &
             CSR="XXXX_usecsr", DZ=0.01, USE_STUPAKOV=1

! sextupoles

% -68.0 sto XXXX_ks1
% -67.7 sto XXXX_ks2


XXXX_SL1: SEXT, L="XXXX_lsext", K2="XXXX_ks1"
XXXX_SL2: SEXT, L="XXXX_lsext", K2="XXXX_ks2"

! arc definition

XXXX_ARCL: LINE=(XXXX_Q1,XXXX_D01,XXXX_BA,XXXX_D02,XXXX_Q2,XXXX_D03A,XXXX_SL1,XXXX_D03B,XXXX_Q3,XXXX_D04,XXXX_Q4,XXXX_D05,&
                 XXXX_BB,XXXX_D06,XXXX_BB,&
                 XXXX_D07,XXXX_Q5,XXXX_D08,XXXX_Q6,XXXX_D09A,XXXX_SL2,XXXX_D09B,XXXX_Q7,XXXX_D10,XXXX_BA,XXXX_D11)

XXXX_ARCLE: LINE=(XXXX_Q1,XXXX_D01E,XXXX_BAE,XXXX_D02E,XXXX_Q2,XXXX_D03AE,XXXX_SL1,XXXX_D03BE,XXXX_Q3,XXXX_D04E,XXXX_Q4,XXXX_D05E,&
                  XXXX_BBE,XXXX_D06E,XXXX_BBE,&
                  XXXX_D07E,XXXX_Q5,XXXX_D08E,XXXX_Q6,XXXX_D09AE,XXXX_SL2,XXXX_D09BE,XXXX_Q7,XXXX_D10E,XXXX_BAE,XXXX_D11E)

XXXX_ARCLCSR: LINE=(XXXX_Q1,XXXX_D01CSR,XXXX_BACSR,XXXX_D02CSR,XXXX_Q2,XXXX_D03ACSR,XXXX_SL1,XXXX_D03BCSR,XXXX_Q3,XXXX_D04CSR,XXXX_Q4,XXXX_D05CSR,&
                    XXXX_BBCSR,XXXX_D06CSR,XXXX_BBCSR,&
                    XXXX_D07CSR,XXXX_Q5,XXXX_D08CSR,XXXX_Q6,XXXX_D09ACSR,XXXX_SL2,XXXX_D09BCSR,XXXX_Q7,XXXX_D10CSR,XXXX_BACSR,XXXX_D11CSR)


LINEXXXX: LINE=(XXXX_ARCL)
LINEXXXXE: LINE=(XXXX_ARCLE)
LINEXXXXCSR: LINE=(XXXX_ARCLCSR)

