function rtmlplot

dataep=load('./simrun_e+/rtml.flr.txt');
dataem=load('./simrun_e-/rtml.flr.txt');
xp1=dataep(1:9204,2);
yp1=dataep(1:9204,3);
zp1=dataep(1:9204,4);
xp2=dataep(9205:24373,2);
yp2=dataep(9205:24373,3);
zp2=dataep(9205:24373,4);
xp3=dataep(24374:length(dataep),2);
yp3=dataep(24374:length(dataep),3);
zp3=dataep(24374:length(dataep),4);

xm1=dataem(1:17883,2);
ym1=dataem(1:17883,3);
zm1=dataem(1:17883,4);
xm2=dataem(17884:23052,2);
ym2=dataem(17884:23052,3);
zm2=dataem(17884:23052,4);
xm3=dataem(23053:length(dataem),2);
ym3=dataem(23053:length(dataem),3);
zm3=dataem(23053:length(dataem),4);

dltl=0;
zp2=zp2+dltl;
zp3=zp3+dltl;
zm2=zm2-dltl;
zm3=zm3-dltl;

lml=23800;
xp3=[xp3;xp3(length(xp3))];
zp3=[zp3;zp3(length(zp3))-lml];
xm3=[xm3;xm3(length(xm3))];
zm3=[zm3;zm3(length(zm3))+lml];

plot(zp1,xp1);
hold on;
plot(zp2,xp2);
plot(zp3,xp3);

plot(zm1,xm1);
plot(zm2,xm2);
plot(zm3,xm3);
hold off;
axis equal;
