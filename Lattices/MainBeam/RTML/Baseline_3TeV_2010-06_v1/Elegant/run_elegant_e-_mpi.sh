#!/bin/bash
#This script runs MPI simulations. Hence, you have to have an mpihosts.txt file containing  the correct nodes.
#
#You must specify the correct working directory.
#All required files will be copied there and it will also contain the output.
#e.g. outputdir=/afs/cern.ch/project/parc/scratch/users/fstulle/Elegant_RTML_Base_v21/output
#
outputdir=./simrun_e-
mkdir $outputdir
./create_rtml_e-_lte.sh
cp extractdata.sh $outputdir
cp rtml_e-.lte $outputdir
cp rtml_e-.ele $outputdir
cp wake_bc1.sdds $outputdir/wake_bc1.sdds
cp wake_bc2.sdds $outputdir/wake_bc2.sdds
cp wake_booster.sdds $outputdir/wake_booster.sdds
cp mpihosts_all.txt $outputdir/mpihosts.txt
cd $outputdir
#
#
#-----------------------------------------------------------------
#
#elegant rtml.ele
mpirun -np 16 --hostfile mpihosts.txt Pelegant_old rtml_e-.ele > Pelegant.out 2> Pelegant.err
#
