#!/bin/bash
#
simdirname="simrun_e+"
if test -f ./$simdirname/LSF_nodes_* ; then
  runhost=$( head -n 1 ./$simdirname/LSF_nodes_* );
 else
  runhost="lxclic01";
fi
echo $runhost
ssh $runhost "tail -n 30 ~/private/RTML_WiP/Elegant/$simdirname/Pelegant.out"
