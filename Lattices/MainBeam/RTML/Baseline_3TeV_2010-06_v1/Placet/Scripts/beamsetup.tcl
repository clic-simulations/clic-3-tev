#
# particle beam setup
#
# 
# general parameters have to be store before
# in the array beamparams=
# [betax,alphax,emitnx,betay,alphay,emitny,sigmaz,charge,
#  uncespread,echirp,energy,nslice,nmacro,nsigmabunch,nsigmawake]
# units=[m,rad,m*rad,m,rad,m*rad,m,C,1,1/m,eV,1,1,1,1]
#
# ATTENTION: The units used here do not match the units used by Placet internally!
#            Hence, at some places the units have to be matched.

proc create_particles {outfile bparray} {
    global placetunits
    upvar $bparray beamparams
    
    set bx $beamparams(betax)
    set ax $beamparams(alphax)
    set enx [expr $beamparams(emitnx)/$placetunits(emittance)]
    set by $beamparams(betay)
    set ay $beamparams(alphay)
    set eny [expr $beamparams(emitny)/$placetunits(emittance)]
    set sz [expr $beamparams(sigmaz)/$placetunits(xyz)]
    set q0 [expr $beamparams(charge)/$placetunits(charge)]
    set unces $beamparams(uncespread)
    set ec [expr $beamparams(echirp)*$placetunits(xyz)]
    set energy [expr $beamparams(startenergy)/$placetunits(energy)]
    set nslice $beamparams(nslice)
    set nmacro $beamparams(nmacro)
    set nsigma $beamparams(nsigmabunch)

    set pu_xyz $placetunits(xyz)
    set pu_xpyp $placetunits(xpyp)
    set pu_e $placetunits(energy)
    set pu_emit $placetunits(emittance)
    set pu_charge $placetunits(charge)
    
    set fid [open params.tmp w]
    puts $fid "$bx $ax $enx $by $ay $eny $sz $q0 $unces $ec $energy $nslice $nmacro $nsigma $pu_xyz $pu_xpyp $pu_e $pu_emit $pu_charge"
    close $fid

# for simplicity, to produce the particles I use octave
Octave {
% read in some useful functions
scriptdir="./Scripts/";
source([scriptdir,"octave_beam_statistics.m"]);

fid=fopen("params.tmp","rt");
[bx,ax,enx,by,ay,eny,sz,q0,unces,ec,energy,nslice,nmacro,nsigma,pu_xyz,pu_xpyp,pu_e,pu_emit,pu_charge]=fscanf(fid,"%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f","C");
fclose(fid);

ex=enx/energy*510998.9;
ey=eny/energy*510998.9;

sx=sqrt(ex*bx);
sxp=sqrt(ex/bx);
sy=sqrt(ey*by);
syp=sqrt(ey/by);
seunc=abs(unces)*energy;

xxpc=-ax/bx;
yypc=-ay/by;
    
n=nslice*nmacro;
rv=cutrandnv(n,nsigma);

x =sx*rv(1,:);
x =x-mean(x,1);
x =x*sx/rms(x,1);

xp=sxp*rv(2,:);
xp=xp-mean(xp,1)+x*1e3;

sumxp=0;
for i=1:length(xp)
  if (x(i)>=0)
    sumxp=sumxp+xp(i);
  end;
end;
signax=-sumxp/abs(sumxp);

ext=sqrt(rms(x,1)^2*rms(xp,1)^2-mean(x.*xp,1)^2);
dxpdx=sqrt(rms(xp,1)^2/rms(x,1)^2-ext^2/rms(x,1)^4);
xp=xp+x*signax*dxpdx;
xp=xp*sxp/rms(xp,1);


y =sy*rv(3,:);
y =y-mean(y,1);
y =y*sy/rms(y,1);

yp=syp*rv(4,:);
yp=yp-mean(yp,1)+y*1e3;

sumyp=0;
for i=1:length(yp)
  if (y(i)>=0)
    sumyp=sumyp+yp(i);
  end;
end;
signay=-sumyp/abs(sumyp);

eyt=sqrt(rms(y,1)^2*rms(yp,1)^2-mean(y.*yp,1)^2);
dypdy=sqrt(rms(yp,1)^2/rms(y,1)^2-eyt^2/rms(y,1)^4);
yp=yp+y*signay*dypdy;
yp=yp*syp/rms(yp,1);


z =sz*rv(5,:);
z =z-mean(z,1);
z =z*sz/rms(z,1);

e =seunc*rv(6,:);
e =e-mean(e,1);
e =e*seunc/rms(e,1);

xp=xp+x*xxpc;
yp=yp+y*yypc;
e =e+z*ec*energy+energy;

x=x*pu_xyz;
xp=xp*pu_xpyp;
y=y*pu_xyz;
yp=yp*pu_xpyp;
z=z*pu_xyz;
e=e*pu_e;

fid=fopen("particles.tmp","wt");
outdata=[e',x',y',z',xp',yp'];
fprintf(fid,"%20.12e %20.12e %20.12e %20.12e %20.12e %20.12e\n",outdata');
fclose(fid);
}
    
    exec cp particles.tmp $outfile
    exec rm particles.tmp
    exec rm params.tmp
}


proc make_particle_beam {name bparray rfparray wakefile} {
    upvar $bparray beamparams
    upvar $rfparray rfparams
#    puts [array get beamparams]

# the following is only required to set up the internal structure which stores the beam,
# by using BeamRead at the end all particle data is overwritten,
# however wake data persists
    set sgbunch $beamparams(nsigmabunch)
    set sgwake $beamparams(nsigmawake)
    set sigmaz $beamparams(sigmaz)
    set meanz $beamparams(meanz)
    set charge $beamparams(charge)
    set nslice $beamparams(nslice)

    create_particles particles.in beamparams

    set fid [open wakeparams.tmp w]
    puts $fid "$wakefile $charge [expr -1*$sgwake] $sgwake $sigmaz $meanz $nslice $rfparams(a) $rfparams(g) $rfparams(l) $rfparams(delta) $rfparams(delta_g) $beamparams(usewakefields) particles.in"
    close $fid
    Octave {
        format long;
        % read in some useful functions
        scriptdir="./Scripts/";
        source([scriptdir,"octave_cavitywakesetup.m"]);
        
        fid=fopen("wakeparams.tmp","rt");
        [filename,charge,sigmamin,sigmamax,sigmaz,meanz,nslice,a,g,l,delta,delta_g,usewakes,particlesfile]=fscanf(fid,"%s %f %f %f %f %f %f %f %f %f %f %f %d %s","C");
        fclose(fid);
        
        if usewakes==0
            cavity_wake_zero(filename,sigmamin,sigmamax,sigmaz,meanz,nslice);
          else
            cavity_wake(filename,charge,sigmamin,sigmamax,sigmaz,meanz,nslice,a,g,l,delta,delta_g,particlesfile);
        end;
    }
    
    InjectorBeam $name -bunches 1 \
	    -macroparticles $beamparams(nmacro) \
	    -particles [expr $beamparams(nslice)*$beamparams(nmacro)] \
	    -energyspread $beamparams(uncespread) \
	    -ecut $beamparams(nsigmawake) \
	    -e0 $beamparams(startenergy) \
	    -file $wakefile \
	    -chargelist {1.0} \
	    -charge $beamparams(charge) \
	    -phase 0.0 \
	    -overlapp 0.0 \
	    -distance 0.0 \
	    -beta_x $beamparams(betax) \
	    -alpha_x $beamparams(alphax) \
	    -emitt_x $beamparams(emitnx) \
	    -beta_y $beamparams(betay) \
	    -alpha_y $beamparams(alphay) \
	    -emitt_y $beamparams(emitny)
    
# now create and load the real particles

    BeamRead -file particles.in -beam $name
#    BeamRead -file bunch_test.dat -beam $name
}


proc reload_particle_beam {name bparray rfparray filename wakefile} {
    upvar $bparray beamparams
    upvar $rfparray rfparams
#    puts [array get beamparams]

# the following is only required to set up the internal structure which stores the beam,
# by using BeamRead at the end all particle data is overwritten,
# however wake data persists
    set sgbunch $beamparams(nsigmabunch)
    set sgwake $beamparams(nsigmawake)
    set sigmaz $beamparams(sigmaz)
    set meanz $beamparams(meanz)
    set charge $beamparams(charge)
    set nslice $beamparams(nslice)
    
    set fid [open wakeparams.tmp w]
    puts $fid "$wakefile $charge [expr -1*$sgwake] $sgwake $sigmaz $meanz $nslice $rfparams(a) $rfparams(g) $rfparams(l) $rfparams(delta) $rfparams(delta_g) $beamparams(usewakefields) $filename"
    close $fid
    Octave {
        format long;
        % read in some useful functions
        scriptdir="./Scripts/";
        source([scriptdir,"octave_cavitywakesetup.m"]);
        
        fid=fopen("wakeparams.tmp","rt");
        [filename,charge,sigmamin,sigmamax,sigmaz,meanz,nslice,a,g,l,delta,delta_g,usewakes,particlesfile]=fscanf(fid,"%s %f %f %f %f %f %f %f %f %f %f %f %d %s","C");
        fclose(fid);
        
        if usewakes==0
            cavity_wake_zero(filename,sigmamin,sigmamax,sigmaz,meanz,nslice);
          else
            cavity_wake(filename,charge,sigmamin,sigmamax,sigmaz,meanz,nslice,a,g,l,delta,delta_g,particlesfile);
        end;
    }
    
    InjectorBeam $name -bunches 1 \
	    -macroparticles $beamparams(nmacro) \
	    -particles [expr $beamparams(nslice)*$beamparams(nmacro)] \
	    -energyspread $beamparams(uncespread) \
	    -ecut $beamparams(nsigmawake) \
	    -e0 $beamparams(startenergy) \
	    -file $wakefile \
	    -chargelist {1.0} \
	    -charge $beamparams(charge) \
	    -phase 0.0 \
	    -overlapp 0.0 \
	    -distance 0.0 \
	    -beta_x $beamparams(betax) \
	    -alpha_x $beamparams(alphax) \
	    -emitt_x $beamparams(emitnx) \
	    -beta_y $beamparams(betay) \
	    -alpha_y $beamparams(alphay) \
	    -emitt_y $beamparams(emitny)
    
# load an existing particle beam
    BeamRead -file $filename -beam $name
}

