#
# matching of Turn Around Loop to BC2 RF
#
# beamparams=[betax,alphax,emitnx,betay,alphay,emitny,sigmaz,charge,
#             uncespread,echirp,energy,nslice,nmacro,nsigmabunch,nsigmawake]
# units=[m,rad,m*rad,m,rad,m*rad,m,C,1,1/m,eV,1,1,1,1]
#
# where necessary units were converted to placet units in main.tcl

proc lattice_match_tal_to_bc2_rf {bparray} {
upvar $bparray beamparams

set usesixdim 1
set numthinlenses 100
set quad_synrad 0

set refenergy $beamparams(meanenergy)

set lquadm 0.3

set kqm1 0.4083261303
set kqm2 0.03843216605
set kqm3 0.384279026
set kqm4 -0.4762374992
set kqm5 0.1050036

set ldm1 1.5
set ldm2 5.0
set ldm3 4.5
set ldm4 2.5

SetReferenceEnergy $refenergy

Girder
Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm1*$lquadm*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ldm1 -six_dim $usesixdim
Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm2*$lquadm*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ldm2 -six_dim $usesixdim
Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm3*$lquadm*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ldm3 -six_dim $usesixdim
Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm4*$lquadm*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ldm4 -six_dim $usesixdim
Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm5*$lquadm*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
}
