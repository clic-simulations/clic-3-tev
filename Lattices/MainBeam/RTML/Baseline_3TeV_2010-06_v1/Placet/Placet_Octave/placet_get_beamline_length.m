## -*- texinfo -*-
## @deftypefn {Function File} {} placet_get_beamline_length (@var{beamline})
## Return the length of 'beamline'.
## @end deftypefn

function s=placet_get_beamline_length(beamline)
  if nargin==1 && ischar(beamline)
    I=placet_get_name_number_list(beamline, "*");
    s=placet_element_get_attribute(beamline, I(end), "s");
  else
    help placet_get_beamline_length
  endif
endfunction
