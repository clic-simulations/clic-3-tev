## -*- texinfo -*-
## @deftypefn {Function File} {} placet_test_simple_correction (@var{beamline}, @var{beam}, @var{survey}, @var{bpms} @var{correctors}, @var{response})
## Tracks 'beam0' through 'beamline' and applies 1-to-1 correction, optionally using 'correctors' and their related 'response' matrix
## @end deftypefn

function E=placet_test_simple_correction(beamline, beam, survey, B, C, A)
  if nargin>=3 && ischar(beamline) && ischar(survey) && ischar(beam)
    if nargin<4
      B=placet_get_number_list(beamline, "bpm");
    endif
    if nargin<5
      C=placet_get_number_list(beamline, "quadrupole");
    endif
    if nargin<6
      A=placet_get_response_matrix(beamline, beam, B, C, "Zero");
    endif
    placet_test_no_correction(beamline, beam, survey, 1);
    b=placet_get_bpm_readings(beamline, B);
    b=b(:,2);
    c=-A\b;
    c=[zeros(size(c)),c];
    placet_vary_corrector(beamline, C, c);
    E=placet_test_no_correction(beamline, beam, "None", 1); 
  else  
    help placet_test_simple_correction
  endif
endfunction
