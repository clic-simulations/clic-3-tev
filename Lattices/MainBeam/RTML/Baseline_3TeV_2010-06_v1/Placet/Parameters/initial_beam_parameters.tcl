#
# define beam parameters at entrances of all sections
# if tracking starts in front of a section, the corresponding
# beam parameters as defined here should be used
# these values are close to what you would get a certain position by tracking from start 
# but the values are not corrected for wake field effects etc.
# i.e. they are not necessarily what a real simulation will give
#
# beamXXX=[betax,alphax,emitnx,betay,alphay,emitny,sigmaz,charge,
#          uncespr,echirp,energy]
# units=[m,rad,m*rad,m,rad,m*rad,m,C,1,1/m,eV]
#
# Attention: before usage the parameters must be converted to Placet units
#

# at entrance of section 0010 (Matching DR to RTML)
set beam0010(betax)    20.0
set beam0010(alphax)    0.0
set beam0010(emitnx)  500.0e-9
set beam0010(betay)     5.0
set beam0010(alphay)    0.0
set beam0010(emitny)    5.0e-9
set beam0010(sigmaz) 1600.0e-6
set beam0010(meanz)     0.0e-6
set beam0010(charge)    0.65e-9
set beam0010(uncespr)   1.3e-3
set beam0010(echirp)    0.0
set beam0010(energy)    2.86e9

# at entrance of section 0020 (Diagnostics section 1)
set beam0020(betax)    20.0
set beam0020(alphax)    0.0
set beam0020(emitnx)  500.0e-9
set beam0020(betay)     5.0
set beam0020(alphay)    0.0
set beam0020(emitny)    5.0e-9
set beam0020(sigmaz) 1600.0e-6
set beam0020(meanz)     0.0e-6
set beam0020(charge)    0.65e-9
set beam0020(uncespr)   1.3e-3
set beam0020(echirp)    0.0
set beam0020(energy)    2.86e9

# at entrance of section 0030 (Dump and match diagnostics to electron spin rotator)
set beam0030(betax)    20.0
set beam0030(alphax)    0.0
set beam0030(emitnx)  500.0e-9
set beam0030(betay)     5.0
set beam0030(alphay)    0.0
set beam0030(emitny)    5.0e-9
set beam0030(sigmaz) 1600.0e-6
set beam0030(meanz)     0.0e-6
set beam0030(charge)    0.65e-9
set beam0030(uncespr)   1.3e-3
set beam0030(echirp)    0.0
set beam0030(energy)    2.86e9

# at entrance of section 0040 (Spin rotator)
set beam0040(betax)    20.0
set beam0040(alphax)    0.0
set beam0040(emitnx)  500.0e-9
set beam0040(betay)     5.0
set beam0040(alphay)    0.0
set beam0040(emitny)    5.0e-9
set beam0040(sigmaz) 1600.0e-6
set beam0040(meanz)     0.0e-6
set beam0040(charge)    0.65e-9
set beam0040(uncespr)   1.3e-3
set beam0040(echirp)    0.0
set beam0040(energy)    2.86e9

# at entrance of section 0050 (Match sr to BC1 RF)
set beam0050(betax)    20.0
set beam0050(alphax)    0.0
set beam0050(emitnx)  500.0e-9
set beam0050(betay)     5.0
set beam0050(alphay)    0.0
set beam0050(emitny)    5.0e-9
set beam0050(sigmaz) 1600.0e-6
set beam0050(meanz)     0.0e-6
set beam0050(charge)    0.65e-9
set beam0050(uncespr)   1.3e-3
set beam0050(echirp)    0.0
set beam0050(energy)    2.86e9

# at entrance of section 0060 (BC1 RF)
set beam0060(betax)    40.05625
set beam0060(alphax)    0.0375
set beam0060(emitnx)  500.0e-9
set beam0060(betay)    40.05625
set beam0060(alphay)    0.0375
set beam0060(emitny)    5.0e-9
set beam0060(sigmaz) 1600.0e-6
set beam0060(meanz)     0.0e-6
set beam0060(charge)    0.65e-9
set beam0060(uncespr)   1.3e-3
set beam0060(echirp)    0.0
set beam0060(energy)    2.86e9

# at entrance of section 0070 (Matching BC1 RF to Chicane)
set beam0070(betax)    40.05625
set beam0070(alphax)   -0.0375
set beam0070(emitnx)  500.0e-9
set beam0070(betay)    40.05625
set beam0070(alphay)   -0.0375
set beam0070(emitny)    5.0e-9
set beam0070(sigmaz) 1600.0e-6
set beam0070(meanz)     0.0e-6
set beam0070(charge)    0.65e-9
set beam0070(uncespr)   1.3e-3
set beam0070(echirp)    6.45
set beam0070(energy)    2.86e9

# at entrance of section 0080 (BC1 Chicane)
set beam0080(betax)   100.0
set beam0080(alphax)    2.8
set beam0080(emitnx)  500.0e-9
set beam0080(betay)    14.0
set beam0080(alphay)    0.0
set beam0080(emitny)    5.0e-9
set beam0080(sigmaz) 1600.0e-6
set beam0080(meanz)     0.0e-6
set beam0080(charge)    0.65e-9
set beam0080(uncespr)   1.3e-3
set beam0080(echirp)    6.45
set beam0080(energy)    2.86e9

# at entrance of section 0090 (Matching BC1 to Diagnostics section 2)
set beam0090(betax)    11.505249698
set beam0090(alphax)    0.130629578
set beam0090(emitnx)  500.0e-9
set beam0090(betay)    65.880280008
set beam0090(alphay)   -1.464141689
set beam0090(emitny)    5.0e-9
set beam0090(sigmaz)  300.0e-6
set beam0090(meanz)     0.0e-6
set beam0090(charge)    0.65e-9
set beam0090(uncespr)   6.93e-3
set beam0090(echirp)   25.84
set beam0090(energy)    2.86e9

# at entrance of section 0100 (Diagnostics section 2)
set beam0100(betax)    20.0
set beam0100(alphax)    0.0
set beam0100(emitnx)  500.0e-9
set beam0100(betay)     5.0
set beam0100(alphay)    0.0
set beam0100(emitny)    5.0e-9
set beam0100(sigmaz)  300.0e-6
set beam0100(meanz)     0.0e-6
set beam0100(charge)    0.65e-9
set beam0100(uncespr)   6.93e-3
set beam0100(echirp)   25.84
set beam0100(energy)    2.86e9

# at entrance of section 0110 (Dump and match diag to booster)
set beam0110(betax)    20.0
set beam0110(alphax)    0.0
set beam0110(emitnx)  500.0e-9
set beam0110(betay)     5.0
set beam0110(alphay)    0.0
set beam0110(emitny)    5.0e-9
set beam0110(sigmaz)  300.0e-6
set beam0110(meanz)     0.0e-6
set beam0110(charge)    0.65e-9
set beam0110(uncespr)   6.93e-3
set beam0110(echirp)   25.84
set beam0110(energy)    2.86e9

# at entrance of section 0120 (Booster Linac)
set beam0120(betax)     6.079099189
set beam0120(alphax)   -0.406835759
set beam0120(emitnx)  500.0e-9
set beam0120(betay)    47.94933011
set beam0120(alphay)    2.701543378
set beam0120(emitny)    5.0e-9
set beam0120(sigmaz)  300.0e-6
set beam0120(meanz)     0.0e-6
set beam0120(charge)    0.65e-9
set beam0120(uncespr)   6.93e-3
set beam0120(echirp)   25.84
set beam0120(energy)    2.86e9

# at entrance of section 0130 (Matching Booster to Central Arc)
set beam0130(betax)    10.24214218
set beam0130(alphax)   -0.980136628
set beam0130(emitnx)  500.0e-9
set beam0130(betay)    33.29103264
set beam0130(alphay)    2.182328713
set beam0130(emitny)    5.0e-9
set beam0130(sigmaz)  300.0e-6
set beam0130(meanz)     0.0e-6
set beam0130(charge)    0.65e-9
set beam0130(uncespr)   2.20e-3
set beam0130(echirp)    8.21
set beam0130(energy)    9.0e9

# at entrance of section 0140 (Central Arc)
set beam0140(betax)    36.90663007
set beam0140(alphax)    7.218366603
set beam0140(emitnx)  500.0e-9
set beam0140(betay)     5.97415669
set beam0140(alphay)   -1.172272956
set beam0140(emitny)    5.0e-9
set beam0140(sigmaz)  300.0e-6
set beam0140(meanz)     0.0e-6
set beam0140(charge)    0.65e-9
set beam0140(uncespr)   2.20e-3
set beam0140(echirp)    8.21
set beam0140(energy)    9.0e9

# at entrance of section 0150 (Vertical Transfer)
set beam0150(betax)    36.90663007
set beam0150(alphax)   -7.218366603
set beam0150(emitnx)  500.0e-9
set beam0150(betay)     5.97415669
set beam0150(alphay)    1.172272956
set beam0150(emitny)    5.0e-9
set beam0150(sigmaz)  300.0e-6
set beam0150(meanz)     0.0e-6
set beam0150(charge)    0.65e-9
set beam0150(uncespr)   2.20e-3
set beam0150(echirp)    8.21
set beam0150(energy)    9.0e9

# at entrance of section 0160 (Match vertical transfer to long transfer line)
set beam0160(betax)    62.71420494
set beam0160(alphax)   -6.944518817
set beam0160(emitnx)  500.0e-9
set beam0160(betay)    15.35067861
set beam0160(alphay)   -4.725246434
set beam0160(emitny)    5.0e-9
set beam0160(sigmaz)  300.0e-6
set beam0160(meanz)     0.0e-6
set beam0160(charge)    0.65e-9
set beam0160(uncespr)   2.20e-3
set beam0160(echirp)    8.21
set beam0160(energy)    9.0e9

# at entrance of section 0170 (Long Transfer Line)
set beam0170(betax)   856.387062
set beam0170(alphax)    0.0
set beam0170(emitnx)  500.0e-9
set beam0170(betay)   382.4304501
set beam0170(alphay)    0.0
set beam0170(emitny)    5.0e-9
set beam0170(sigmaz)  300.0e-6
set beam0170(meanz)     0.0e-6
set beam0170(charge)    0.65e-9
set beam0170(uncespr)   2.20e-3
set beam0170(echirp)    8.21
set beam0170(energy)    9.0e9

# at entrance of section 0180 (Dump and Match Transfer Line to TAL)
set beam0180(betax)   856.387062
set beam0180(alphax)    0.0
set beam0180(emitnx)  500.0e-9
set beam0180(betay)   382.4304501
set beam0180(alphay)    0.0
set beam0180(emitny)    5.0e-9
set beam0180(sigmaz)  300.0e-6
set beam0180(meanz)     0.0e-6
set beam0180(charge)    0.65e-9
set beam0180(uncespr)   2.20e-3
set beam0180(echirp)    8.21
set beam0180(energy)    9.0e9

# at entrance of section 0190 (Turn Around Loop)
set beam0190(betax)    36.90663007
set beam0190(alphax)    7.218366603
set beam0190(emitnx)  500.0e-9
set beam0190(betay)     5.97415669
set beam0190(alphay)   -1.172272956
set beam0190(emitny)    5.0e-9
set beam0190(sigmaz)  300.0e-6
set beam0190(meanz)     0.0e-6
set beam0190(charge)    0.65e-9
set beam0190(uncespr)   2.20e-3
set beam0190(echirp)    8.21
set beam0190(energy)    9.0e9

# at entrance of section 0200 (Matching TAL to BC2 RF)
set beam0200(betax)    36.90663007
set beam0200(alphax)   -7.218366603
set beam0200(emitnx)  500.0e-9
set beam0200(betay)     5.97415669
set beam0200(alphay)    1.172272956
set beam0200(emitny)    5.0e-9
set beam0200(sigmaz)  300.0e-6
set beam0200(meanz)     0.0e-6
set beam0200(charge)    0.65e-9
set beam0200(uncespr)   2.20e-3
set beam0200(echirp)    8.21
set beam0200(energy)    9.0e9

# at entrance of section 0210 (BC2 RF)
set beam0230(betax)    40.0
set beam0230(alphax)    0.0
set beam0230(emitnx)  500.0e-9
set beam0230(betay)    40.0
set beam0230(alphay)    0.0
set beam0230(emitny)    5.0e-9
set beam0230(sigmaz)  300.0e-6
set beam0230(meanz)     0.0e-6
set beam0230(charge)    0.65e-9
set beam0230(uncespr)   2.20e-3
set beam0230(echirp)    8.21
set beam0230(energy)    9.0e9

# at entrance of section 0220 (Matching BC2 RF and Chicane 1)
set beam0240(betax)    40.0
set beam0240(alphax)    0.0
set beam0240(emitnx)  500.0e-9
set beam0240(betay)    40.0
set beam0240(alphay)    0.0
set beam0240(emitny)    5.0e-9
set beam0240(sigmaz)  300.0e-6
set beam0240(meanz)     0.0e-6
set beam0240(charge)    0.65e-9
set beam0240(uncespr)   2.20e-3
set beam0240(echirp)   49.53
set beam0240(energy)    9.0e9

# at entrance of section 0230 (BC2 Chicane 1)
set beam0250(betax)    70.0
set beam0250(alphax)    1.5
set beam0250(emitnx)  500.0e-9
set beam0250(betay)    20.0
set beam0250(alphay)    0.2
set beam0250(emitny)    5.0e-9
set beam0250(sigmaz)  300.0e-6
set beam0250(meanz)     0.0e-6
set beam0250(charge)    0.65e-9
set beam0250(uncespr)   2.20e-3
set beam0250(echirp)   49.53
set beam0250(energy)    9.0e9

# at entrance of section 0240 (Matching BC2 Chicanes 1 and 2)
set beam0260(betax)    21.7816664080
set beam0260(alphax)    0.1062590419
set beam0260(emitnx)  500.0e-9
set beam0260(betay)    53.7729142142
set beam0260(alphay)   -1.3093212699
set beam0260(emitny)    5.0e-9
set beam0260(sigmaz)  100.0e-6
set beam0260(meanz)     0.0e-6
set beam0260(charge)    0.65e-9
set beam0260(uncespr)   6.61e-3
set beam0260(echirp)  134.90
set beam0260(energy)    9.0e9

# at entrance of section 0250 (BC2 Chicane 2)
set beam0270(betax)    75.0
set beam0270(alphax)    1.8
set beam0270(emitnx)  500.0e-9
set beam0270(betay)    20.0
set beam0270(alphay)    0.5
set beam0270(emitny)    5.0e-9
set beam0270(sigmaz)  100.0e-6
set beam0270(meanz)     0.0e-6
set beam0270(charge)    0.65e-9
set beam0270(uncespr)   6.61e-3
set beam0270(echirp)  134.90
set beam0270(energy)    9.0e9

# at entrance of section 0260 (Matching BC2 to Diagnostics Section 3)
set beam0280(betax)    17.8783068863
set beam0280(alphax)    0.1035388021
set beam0280(emitnx)  600.0e-9
set beam0280(betay)    45.9615041604
set beam0280(alphay)   -1.3599770882
set beam0280(emitny)   10.0e-9
set beam0280(sigmaz)   44.0e-6
set beam0280(meanz)     0.0e-6
set beam0280(charge)    0.65e-9
set beam0280(uncespr)   1.50e-2
set beam0280(echirp)    0.0
set beam0280(energy)    9.0e9

# at entrance of section 0270 (Diagnostics Section 3)
set beam0290(betax)    20.0
set beam0290(alphax)    0.0
set beam0290(emitnx)  600.0e-9
set beam0290(betay)     5.0
set beam0290(alphay)    0.0
set beam0290(emitny)   10.0e-9
set beam0290(sigmaz)   44.0e-6
set beam0290(meanz)     0.0e-6
set beam0290(charge)    0.65e-9
set beam0290(uncespr)   1.50e-2
set beam0290(echirp)    0.0
set beam0290(energy)    9.0e9

# at entrance of section 0280 (Dump and match RTML to Main Linac)
set beam0290(betax)    20.0
set beam0290(alphax)    0.0
set beam0290(emitnx)  600.0e-9
set beam0290(betay)     5.0
set beam0290(alphay)    0.0
set beam0290(emitny)   10.0e-9
set beam0290(sigmaz)   44.0e-6
set beam0290(meanz)     0.0e-6
set beam0290(charge)    0.65e-9
set beam0290(uncespr)   1.50e-2
set beam0290(echirp)    0.0
set beam0290(energy)    9.0e9


