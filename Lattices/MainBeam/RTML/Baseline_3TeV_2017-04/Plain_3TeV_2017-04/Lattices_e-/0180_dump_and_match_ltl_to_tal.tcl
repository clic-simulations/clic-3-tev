#
# Dump and match long transfer line to turn around loop
#
# beamparams=[betax,alphax,emitnx,betay,alphay,emitny,sigmaz,charge,
#             uncespread,echirp,energy,nslice,nmacro,nsigmabunch,nsigmawake]
# units=[m,rad,m*rad,m,rad,m*rad,m,C,1,1/m,eV,1,1,1,1]
#
# where necessary units were converted to placet units in main.tcl

proc lattice_dump_and_match_ltl_to_tal {bparray} {
    upvar $bparray beamparams

    set usesixdim 1
    set numthinlenses 100
    set quad_synrad 0

    set refenergy $beamparams(meanenergy)

    SetReferenceEnergy $refenergy
    
    set lquadm 0.3
    set lquadx 0.36
    
    set kqm1 0.009713020617
    set kqm2 -0.01666831502
    set kqm3 0.02506010184
    set kqm4 0.0817375657
    set kqm5 -0.1208106861
    set kqm6 0.5742405616
    
    set ldm1  5.0
    set ldm2 10.0
    set ldm3 10.0
    set ldm4 13.0
    set ldm5 65.0

    if { 0 } {

	Girder
	Bpm -length 0.0
	Quadrupole -synrad $quad_synrad -length $lquadx -strength [expr $kqm1*$lquadx*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
	Drift -length $ldm1 -six_dim $usesixdim
	Bpm -length 0.0
	Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm2*$lquadm*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
	Drift -length $ldm2 -six_dim $usesixdim
	Bpm -length 0.0
	Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm3*$lquadm*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
	Drift -length $ldm3 -six_dim $usesixdim
	Bpm -length 0.0
	Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm4*$lquadm*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
	Drift -length $ldm4 -six_dim $usesixdim
	Bpm -length 0.0
	Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm5*$lquadm*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
	Drift -length $ldm5 -six_dim $usesixdim
	Bpm -length 0.0
	Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm6*$lquadm*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses

    } {
	
	set usesynrad $beamparams(useisr)
	set mult_synrad 0

	Drift -name "DRIFT_1" -length 1
	Sbend -name "BEND1" -synrad $usesynrad -length 0.8436627613 -angle 0.002812209204 -E1 0 -E2 0 -six_dim 1 -e0 $refenergy
	set e0 [expr $refenergy-14.1e-6*0.002812209204*0.002812209204/0.8436627613*$refenergy*$refenergy*$refenergy*$refenergy*$usesynrad]
	SetReferenceEnergy $refenergy
	Drift -name "DRIFT_2" -length 0.1
	Quadrupole -name "QM6" -synrad $quad_synrad -length 0.175 -strength [expr 0.01033514896*$refenergy] -e0 $refenergy
	Dipole -name "D180-1" -length 0.00000000000
	Quadrupole -name "QM6" -synrad $quad_synrad -length 0.175 -strength [expr 0.01033514896*$refenergy] -e0 $refenergy
	Dipole -name "D180-2" -length 0.00000000000
	Drift -name "DRIFT_3387" -length 18.21174984
	Quadrupole -name "QM7" -synrad $quad_synrad -length 0.175 -strength [expr -0.01666403078*$refenergy] -e0 $refenergy
	Dipole -name "D180-3" -length 0.00000000000
	Quadrupole -name "QM7" -synrad $quad_synrad -length 0.175 -strength [expr -0.01666403078*$refenergy] -e0 $refenergy
	Dipole -name "D180-4" -length 0.00000000000
	Drift -name "DRIFT_2" -length 0.1
	Sbend -name "BEND2" -synrad $usesynrad -length 0.8436627613 -angle -0.002812209204 -E1 0 -E2 0 -six_dim 1 -e0 $refenergy
	set e0 [expr $refenergy-14.1e-6*-0.002812209204*-0.002812209204/0.8436627613*$refenergy*$refenergy*$refenergy*$refenergy*$usesynrad]
	SetReferenceEnergy $refenergy
	Drift -name "DRIFT_3388" -length 24.43066582
	Quadrupole -name "QM1" -synrad $quad_synrad -length 0.175 -strength [expr 0.02183037577*$refenergy] -e0 $refenergy
	Dipole -name "D180-5" -length 0.00000000000
	Quadrupole -name "QM1" -synrad $quad_synrad -length 0.175 -strength [expr 0.02183037577*$refenergy] -e0 $refenergy
	Dipole -name "D180-6" -length 0.00000000000
	Drift -name "DRIFT_3389" -length 1.200000001
	Quadrupole -name "QM2" -synrad $quad_synrad -length 0.175 -strength [expr -0.02717745522*$refenergy] -e0 $refenergy
	Dipole -name "D180-7" -length 0.00000000000
	Quadrupole -name "QM2" -synrad $quad_synrad -length 0.175 -strength [expr -0.02717745522*$refenergy] -e0 $refenergy
	Dipole -name "D180-8" -length 0.00000000000
	Drift -name "DRIFT_3389" -length 1.200000001
	Quadrupole -name "QM1" -synrad $quad_synrad -length 0.175 -strength [expr 0.02183037577*$refenergy] -e0 $refenergy
	Dipole -name "D180-9" -length 0.00000000000
	Quadrupole -name "QM1" -synrad $quad_synrad -length 0.175 -strength [expr 0.02183037577*$refenergy] -e0 $refenergy
	Dipole -name "D180-10" -length 0.00000000000
	Drift -name "DRIFT_3388" -length 24.43066582
	Sbend -name "BEND2" -synrad $usesynrad -length 0.8436627613 -angle -0.002812209204 -E1 0 -E2 0 -six_dim 1 -e0 $refenergy
	set e0 [expr $refenergy-14.1e-6*-0.002812209204*-0.002812209204/0.8436627613*$refenergy*$refenergy*$refenergy*$refenergy*$usesynrad]
	SetReferenceEnergy $refenergy
	Drift -name "DRIFT_2" -length 0.1
	Quadrupole -name "QM7" -synrad $quad_synrad -length 0.175 -strength [expr -0.01666403078*$refenergy] -e0 $refenergy
	Dipole -name "D180-11" -length 0.00000000000
	Quadrupole -name "QM7" -synrad $quad_synrad -length 0.175 -strength [expr -0.01666403078*$refenergy] -e0 $refenergy
	Dipole -name "D180-12" -length 0.00000000000
	Drift -name "DRIFT_3387" -length 18.21174984
	Quadrupole -name "QM6" -synrad $quad_synrad -length 0.175 -strength [expr 0.01033514896*$refenergy] -e0 $refenergy
	Dipole -name "D180-13" -length 0.00000000000
	Quadrupole -name "QM6" -synrad $quad_synrad -length 0.175 -strength [expr 0.01033514896*$refenergy] -e0 $refenergy
	Dipole -name "D180-14" -length 0.00000000000
	Drift -name "DRIFT_2" -length 0.1
	Sbend -name "BEND1" -synrad $usesynrad -length 0.8436627613 -angle 0.002812209204 -E1 0 -E2 0 -six_dim 1 -e0 $refenergy
	set e0 [expr $refenergy-14.1e-6*0.002812209204*0.002812209204/0.8436627613*$refenergy*$refenergy*$refenergy*$refenergy*$usesynrad]
	SetReferenceEnergy $refenergy
	Drift -name "DRIFT_2" -length 0.1
	Quadrupole -name "QF1" -synrad $quad_synrad -length 0.175 -strength [expr 0.05488808199*$refenergy] -e0 $refenergy
	Dipole -name "D180-15" -length 0.00000000000
	Bpm -name "FF2XBPM1" -length 0
	Quadrupole -name "QF1" -synrad $quad_synrad -length 0.175 -strength [expr 0.05488808199*$refenergy] -e0 $refenergy
	Dipole -name "D180-16" -length 0.00000000000
	Drift -name "DRIFT_0" -length 12.65
	Quadrupole -name "QF2" -synrad $quad_synrad -length 0.175 -strength [expr -0.05488808199*$refenergy] -e0 $refenergy
	Dipole -name "D180-17" -length 0.00000000000
	Bpm -name "FF2YBPM1" -length 0
	Quadrupole -name "QF2" -synrad $quad_synrad -length 0.175 -strength [expr -0.05488808199*$refenergy] -e0 $refenergy
	Dipole -name "D180-18" -length 0.00000000000
	Drift -name "DRIFT_0" -length 12.65
	Quadrupole -name "QF1" -synrad $quad_synrad -length 0.175 -strength [expr 0.05488808199*$refenergy] -e0 $refenergy
	Dipole -name "D180-19" -length 0.00000000000
	Bpm -name "FF2XBPM2" -length 0
	Quadrupole -name "QF1" -synrad $quad_synrad -length 0.175 -strength [expr 0.05488808199*$refenergy] -e0 $refenergy
	Dipole -name "D180-20" -length 0.00000000000
	Drift -name "DRIFT_0" -length 12.65
	Quadrupole -name "QF2" -synrad $quad_synrad -length 0.175 -strength [expr -0.05488808199*$refenergy] -e0 $refenergy
	Dipole -name "D180-21" -length 0.00000000000
	Bpm -name "FF2YBPM2" -length 0
	Quadrupole -name "QF2" -synrad $quad_synrad -length 0.175 -strength [expr -0.05488808199*$refenergy] -e0 $refenergy
	Dipole -name "D180-22" -length 0.00000000000
	Drift -name "DRIFT_0" -length 12.65
	Quadrupole -name "QF1" -synrad $quad_synrad -length 0.175 -strength [expr 0.05488808199*$refenergy] -e0 $refenergy
	Dipole -name "D180-23" -length 0.00000000000
	Quadrupole -name "QF1" -synrad $quad_synrad -length 0.175 -strength [expr 0.05488808199*$refenergy] -e0 $refenergy
	Dipole -name "D180-24" -length 0.00000000000
	Drift -name "DRIFT_3390" -length 21.83150356
	Quadrupole -name "QM3" -synrad $quad_synrad -length 0.175 -strength [expr -0.06366381739*$refenergy] -e0 $refenergy
	Dipole -name "D180-25" -length 0.00000000000
	Quadrupole -name "QM3" -synrad $quad_synrad -length 0.175 -strength [expr -0.06366381739*$refenergy] -e0 $refenergy
	Dipole -name "D180-26" -length 0.00000000000
	Drift -name "DRIFT_3391" -length 6.401700061
        Drift -name "Marker-TAL1-start" -length 0.0
        Bpm -length 0
	Quadrupole -name "QM4" -synrad $quad_synrad -length 0.175 -strength [expr 0.06102719622*$refenergy] -e0 $refenergy
	Dipole -name "D180-27" -length 0.00000000000
        Bpm -length 0
	Quadrupole -name "QM4" -synrad $quad_synrad -length 0.175 -strength [expr 0.06102719622*$refenergy] -e0 $refenergy
	Dipole -name "D180-28" -length 0.00000000000
	Drift -name "DRIFT_3392" -length 8.849291577
        Bpm -length 0
	Quadrupole -name "QM5" -synrad $quad_synrad -length 0.175 -strength [expr 0.07445782338*$refenergy] -e0 $refenergy
	Dipole -name "D180-29" -length 0.00000000000
        Bpm -length 0
	Quadrupole -name "QM5" -synrad $quad_synrad -length 0.175 -strength [expr 0.07445782338*$refenergy] -e0 $refenergy
	Dipole -name "D180-30" -length 0.00000000000

	# install feed forward
	TclCall -name "FF2" -script "FF2"
	Drift -name "Marker-LTL-end" -length 0.0
    }
}
