#
# define beam parameters at entrances of all sections
# if tracking starts in front of a section, the corresponding
# beam parameters as defined here should be used
# these values are close to what you would get at a certain position by tracking from the start
# but the values are not corrected for wake field effects etc.
# i.e. they are not necessarily what a real simulation will give
#
# beamXXX=[betax,alphax,emitnx,betay,alphay,emitny,sigmaz,charge,
#          uncespr,echirp,energy]
# units=[m,rad,m*rad,m,rad,m*rad,m,C,1,1/m,eV]
#
# Attention: before usage the parameters must be converted to Placet units
#

# at entrance of section 0010 (Matching DR to RTML)
set beam0010(betax)    20.0
set beam0010(alphax)    0.0
set beam0010(emitnx)  500.0e-9
set beam0010(betay)     5.0
set beam0010(alphay)    0.0
set beam0010(emitny)    5.0e-9
set beam0010(sigmaz) 1800.0e-6
set beam0010(meanz)     0.0e-6
set beam0010(charge)    0.65e-9
set beam0010(uncespr)   1.1655e-3
set beam0010(echirp)    0.0
set beam0010(energy)    2.86e9

# at entrance of section 0020 (Diagnostics section 1)
set beam0020(betax)     1.969809853458563e+01
set beam0020(alphax)    2.002449581405267e+00
set beam0020(emitnx)  500.0e-9
set beam0020(betay)     5.081051073345856e+00
set beam0020(alphay)   -5.430805110777387e-01
set beam0020(emitny)    5.0e-9
set beam0020(sigmaz) 1800.0e-6
set beam0020(meanz)     0.0e-6
set beam0020(charge)    0.65e-9
set beam0020(uncespr)   1.1655e-3
set beam0020(echirp)    0.0
set beam0020(energy)    2.86e9

# at entrance of section 0030 (Dump and match diagnostics to electron spin rotator)
set beam0030(betax)     1.969809853453061e+01
set beam0030(alphax)   -2.002449581397254e+00
set beam0030(emitnx)  500.0e-9
set beam0030(betay)     5.081051073605058e+00
set beam0030(alphay)    5.430805110912621e-01
set beam0030(emitny)    5.0e-9
set beam0030(sigmaz) 1800.0e-6
set beam0030(meanz)     0.0e-6
set beam0030(charge)    0.65e-9
set beam0030(uncespr)   1.1655e-3
set beam0030(echirp)    0.0
set beam0030(energy)    2.86e9

# at entrance of section 0040 (Spin rotator)
set beam0040p(betax)     3.690229192612266e+00
set beam0040p(alphax)   -3.918263062634419e-01
set beam0040p(emitnx)  500.0e-9
set beam0040p(betay)     2.474923098762039e+01
set beam0040p(alphay)    2.314900500918660e+00
set beam0040p(emitny)    5.0e-9
set beam0040p(sigmaz) 1800.0e-6
set beam0040p(meanz)     0.0e-6
set beam0040p(charge)    0.65e-9
set beam0040p(uncespr)   1.1655e-3
set beam0040p(echirp)    0.0
set beam0040p(energy)    2.86e9

# at entrance of section 0050 (Match sr to BC1 RF)
set beam0050p(betax)     5.237273268560354e+00
set beam0050p(alphax)   -7.981919156476862e-01
set beam0050p(emitnx)  500.0e-9
set beam0050p(betay)     1.916487348288342e+01
set beam0050p(alphay)    1.980910704589770e+00
set beam0050p(emitny)    5.0e-9
set beam0050p(sigmaz) 1800.0e-6
set beam0050p(meanz)     0.0e-6
set beam0050p(charge)    0.65e-9
set beam0050p(uncespr)   1.1655e-3
set beam0050p(echirp)    0.0
set beam0050p(energy)    2.86e9

# at entrance of section 0060 (BC1 RF)
set beam0060(betax)     6.881653170749148e+00
set beam0060(alphax)   -5.464351124550523e-01
set beam0060(emitnx)  500.0e-9
set beam0060(betay)     2.560478765504324e+01
set beam0060(alphay)    1.960548357443838e+00
set beam0060(emitny)    5.0e-9
set beam0060(sigmaz) 1800.0e-6
set beam0060(meanz)     0.0e-6
set beam0060(charge)    0.65e-9
set beam0060(uncespr)   1.1655e-3
set beam0060(echirp)    0.0
set beam0060(energy)    2.86e9

# at entrance of section 0070 (Matching BC1 RF to Chicane)
set beam0070(betax)     2.569274507375324e+01
set beam0070(alphax)   -1.961710454653798e+00
set beam0070(emitnx)  500.0e-9
set beam0070(betay)     6.837577532675770e+00
set beam0070(alphay)    5.417463042924807e-01
set beam0070(emitny)    5.0e-9
set beam0070(sigmaz) 1800.0e-6
set beam0070(meanz)     0.0e-6
set beam0070(charge)    0.65e-9
set beam0070(uncespr)   1.1655e-3
set beam0070(echirp)    5.79
set beam0070(energy)    2.86e9

# at entrance of section 0080 (BC1 Chicane)
set beam0080(betax)   100.0
set beam0080(alphax)    2.8
set beam0080(emitnx)  500.0e-9
set beam0080(betay)    14.0
set beam0080(alphay)    0.0
set beam0080(emitny)    5.0e-9
set beam0080(sigmaz) 1800.0e-6
set beam0080(meanz)     0.0e-6
set beam0080(charge)    0.65e-9
set beam0080(uncespr)   1.1655e-3
set beam0080(echirp)    5.79
set beam0080(energy)    2.86e9

# at entrance of section 0090 (Matching BC1 to Diagnostics section 2)
set beam0090(betax)     1.149809845865557e+01
set beam0090(alphax)    1.281866790585585e-01
set beam0090(emitnx)  500.0e-9
set beam0090(betay)     6.430041554190029e+01
set beam0090(alphay)   -1.382544445807898e+00
set beam0090(emitny)    5.0e-9
set beam0090(sigmaz)  300.0e-6
set beam0090(meanz)     0.0e-6
set beam0090(charge)    0.65e-9
set beam0090(uncespr)   6.99e-3
set beam0090(echirp)   26.06
set beam0090(energy)    2.86e9

# at entrance of section 0100 (Diagnostics section 2)
set beam0100(betax)     1.969809853962024e+01
set beam0100(alphax)    2.002449581985366e+00
set beam0100(emitnx)  500.0e-9
set beam0100(betay)     5.081051071810490e+00
set beam0100(alphay)   -5.430805117808616e-01
set beam0100(emitny)    5.0e-9
set beam0100(sigmaz)  300.0e-6
set beam0100(meanz)     0.0e-6
set beam0100(charge)    0.65e-9
set beam0100(uncespr)   6.99e-3
set beam0100(echirp)   26.06
set beam0100(energy)    2.86e9

# at entrance of section 0110 (Dump and match diag to booster)
set beam0110(betax)     1.969809853248741e+01
set beam0110(alphax)   -2.002449580946175e+00
set beam0110(emitnx)  500.0e-9
set beam0110(betay)     5.081051077193772e+00
set beam0110(alphay)    5.430805120618252e-01
set beam0110(emitny)    5.0e-9
set beam0110(sigmaz)  300.0e-6
set beam0110(meanz)     0.0e-6
set beam0110(charge)    0.65e-9
set beam0110(uncespr)   6.99e-3
set beam0110(echirp)   26.06
set beam0110(energy)    2.86e9

# at entrance of section 0120 (Booster Linac)
set beam0120(betax)     6.881653166281068e+00
set beam0120(alphax)   -5.464351122646679e-01
set beam0120(emitnx)  500.0e-9
set beam0120(betay)     2.560478763032405e+01
set beam0120(alphay)    1.960548356245693e+00
set beam0120(emitny)    5.0e-9
set beam0120(sigmaz)  300.0e-6
set beam0120(meanz)     0.0e-6
set beam0120(charge)    0.65e-9
set beam0120(uncespr)   6.99e-3
set beam0120(echirp)   26.06
set beam0120(energy)    2.86e9

# at entrance of section 0130 (Matching Booster to Central Arc)
set beam0130(betax)     2.569421335532597e+01
set beam0130(alphax)   -1.962363139321195e+00
set beam0130(emitnx)  500.0e-9
set beam0130(betay)     6.838727837632369e+00
set beam0130(alphay)    5.415555907589521e-01
set beam0130(emitny)    5.0e-9
set beam0130(sigmaz)  300.0e-6
set beam0130(meanz)     0.0e-6
set beam0130(charge)    0.65e-9
set beam0130(uncespr)   2.22e-3
set beam0130(echirp)    8.28
set beam0130(energy)    9.0e9

# at entrance of section 0140 (Central Arc)
set beam0140p(betax)     3.690663006107827e+01
set beam0140p(alphax)    7.218366600806072e+00
set beam0140p(emitnx)  500.0e-9
set beam0140p(betay)     5.974156689951215e+00
set beam0140p(alphay)   -1.172272955890056e+00
set beam0140p(emitny)    5.0e-9
set beam0140p(sigmaz)  300.0e-6
set beam0140p(meanz)     0.0e-6
set beam0140p(charge)    0.65e-9
set beam0140p(uncespr)   2.22e-3
set beam0140p(echirp)    8.28
set beam0140p(energy)    9.0e9

# at entrance of section 0150 (Vertical Transfer)
set beam0150p(betax)     3.690662996539425e+01
set beam0150p(alphax)   -7.218366581141533e+00
set beam0150p(emitnx)  500.0e-9
set beam0150p(betay)     5.974156594218357e+00
set beam0150p(alphay)    1.172272916952053e+00
set beam0150p(emitny)    5.0e-9
set beam0150p(sigmaz)  300.0e-6
set beam0150p(meanz)     0.0e-6
set beam0150p(charge)    0.65e-9
set beam0150p(uncespr)   2.22e-3
set beam0150p(echirp)    8.28
set beam0150p(energy)    9.0e9

# at entrance of section 0160 (Match vertical transfer to long transfer line)
set beam0160(betax)     6.271420480270881e+01
set beam0160(alphax)   -6.944518807463906e+00
set beam0160(emitnx)  500.0e-9
set beam0160(betay)     1.535067833470247e+01
set beam0160(alphay)   -4.725246359659966e+00
set beam0160(emitny)    5.0e-9
set beam0160(sigmaz)  300.0e-6
set beam0160(meanz)     0.0e-6
set beam0160(charge)    0.65e-9
set beam0160(uncespr)   2.22e-3
set beam0160(echirp)    8.28
set beam0160(energy)    9.0e9

# at entrance of section 0170 (Long Transfer Line)
set beam0170(betax)     8.561176173690139e+02
set beam0170(alphax)   -1.496734681832926e+00
set beam0170(emitnx)  500.0e-9
set beam0170(betay)     3.825508931661166e+02
set beam0170(alphay)    6.692309491263281e-01
set beam0170(emitny)    5.0e-9
set beam0170(sigmaz)  300.0e-6
set beam0170(meanz)     0.0e-6
set beam0170(charge)    0.65e-9
set beam0170(uncespr)   2.22e-3
set beam0170(echirp)    8.28
set beam0170(energy)    9.0e9

# at entrance of section 0180 (Dump and Match Transfer Line to TAL)
set beam0180(betax)     8.561176173655683e+02
set beam0180(alphax)   -1.496734681820911e+00
set beam0180(emitnx)  500.0e-9
set beam0180(betay)     3.825508931796965e+02
set beam0180(alphay)    6.692309491691935e-01
set beam0180(emitny)    5.0e-9
set beam0180(sigmaz)  300.0e-6
set beam0180(meanz)     0.0e-6
set beam0180(charge)    0.65e-9
set beam0180(uncespr)   2.22e-3
set beam0180(echirp)    8.28
set beam0180(energy)    9.0e9

# at entrance of section 0190 (Turn Around Loop)
set beam0190(betax)     3.690663027081168e+01
set beam0190(alphax)    7.218366636170233e+00
set beam0190(emitnx)  500.0e-9
set beam0190(betay)     5.974156838401393e+00
set beam0190(alphay)   -1.172273014130560e+00
set beam0190(emitny)    5.0e-9
set beam0190(sigmaz)  300.0e-6
set beam0190(meanz)     0.0e-6
set beam0190(charge)    0.65e-9
set beam0190(uncespr)   2.22e-3
set beam0190(echirp)    8.28
set beam0190(energy)    9.0e9

# at entrance of section 0200 (Matching TAL to BC2 RF)
set beam0200(betax)     3.690663027740090e+01
set beam0200(alphax)   -7.218366646866695e+00
set beam0200(emitnx)  500.0e-9
set beam0200(betay)     5.974156901895229e+00
set beam0200(alphay)    1.172272983062067e+00
set beam0200(emitny)    5.0e-9
set beam0200(sigmaz)  300.0e-6
set beam0200(meanz)     0.0e-6
set beam0200(charge)    0.65e-9
set beam0200(uncespr)   2.22e-3
set beam0200(echirp)    8.28
set beam0200(energy)    9.0e9

# at entrance of section 0210 (BC2 RF)
set beam0230(betax)     2.140612660553167e+00
set beam0230(alphax)   -6.943597450790081e-01
set beam0230(emitnx)  500.0e-9
set beam0230(betay)     6.353828171609992e+00
set beam0230(alphay)    1.843724909101989e+00
set beam0230(emitny)    5.0e-9
set beam0230(sigmaz)  300.0e-6
set beam0230(meanz)     0.0e-6
set beam0230(charge)    0.65e-9
set beam0230(uncespr)   2.22e-3
set beam0230(echirp)    8.28
set beam0230(energy)    9.0e9

# at entrance of section 0220 (Matching BC2 RF and Chicane 1)
set beam0240(betax)     6.353831437923118e+00
set beam0240(alphax)   -1.843723409244628e+00
set beam0240(emitnx)  500.0e-9
set beam0240(betay)     2.140609544070128e+00
set beam0240(alphay)    6.943568329826921e-01
set beam0240(emitny)    5.0e-9
set beam0240(sigmaz)  300.0e-6
set beam0240(meanz)     0.0e-6
set beam0240(charge)    0.65e-9
set beam0240(uncespr)   2.22e-3
set beam0240(echirp)   49.96
set beam0240(energy)    9.0e9

# at entrance of section 0230 (BC2 Chicane 1)
set beam0250(betax)    70.0
set beam0250(alphax)    1.5
set beam0250(emitnx)  500.0e-9
set beam0250(betay)    20.0
set beam0250(alphay)    0.2
set beam0250(emitny)    5.0e-9
set beam0250(sigmaz)  300.0e-6
set beam0250(meanz)     0.0e-6
set beam0250(charge)    0.65e-9
set beam0250(uncespr)   2.22e-3
set beam0250(echirp)   49.96
set beam0250(energy)    9.0e9

# at entrance of section 0240 (Matching BC2 Chicanes 1 and 2)
set beam0260(betax)     2.178148471820744e+01
set beam0260(alphax)    1.062218325460853e-01
set beam0260(emitnx)  500.0e-9
set beam0260(betay)     5.373181527463874e+01
set beam0260(alphay)   -1.307307771786528e+00
set beam0260(emitny)    5.0e-9
set beam0260(sigmaz)  100.0e-6
set beam0260(meanz)     0.0e-6
set beam0260(charge)    0.65e-9
set beam0260(uncespr)   6.66e-3
set beam0260(echirp)  136.06
set beam0260(energy)    9.0e9

# at entrance of section 0250 (BC2 Chicane 2)
set beam0270(betax)    75.0
set beam0270(alphax)    1.8
set beam0270(emitnx)  500.0e-9
set beam0270(betay)    20.0
set beam0270(alphay)    0.5
set beam0270(emitny)    5.0e-9
set beam0270(sigmaz)  100.0e-6
set beam0270(meanz)     0.0e-6
set beam0270(charge)    0.65e-9
set beam0270(uncespr)   6.66e-3
set beam0270(echirp)  136.06
set beam0270(energy)    9.0e9

# at entrance of section 0260 (Matching BC2 to Diagnostics Section 3)
set beam0280(betax)     1.787826616070926e+01
set beam0280(alphax)    1.035279479152752e-01
set beam0280(emitnx)  600.0e-9
set beam0280(betay)     4.595467276672003e+01
set beam0280(alphay)   -1.359621865950975e+00
set beam0280(emitny)   10.0e-9
set beam0280(sigmaz)   44.0e-6
set beam0280(meanz)     0.0e-6
set beam0280(charge)    0.65e-9
set beam0280(uncespr)   1.52e-2
set beam0280(echirp)    0.0
set beam0280(energy)    9.0e9

# at entrance of section 0270 (Diagnostics Section 3)
set beam0290(betax)     3.969038431111105e+01
set beam0290(alphax)   -1.498310794232509e+00
set beam0290(emitnx)  600.0e-9
set beam0290(betay)     1.789978887234360e+01
set beam0290(alphay)    6.807489722771100e-01
set beam0290(emitny)   10.0e-9
set beam0290(sigmaz)   44.0e-6
set beam0290(meanz)     0.0e-6
set beam0290(charge)    0.65e-9
set beam0290(uncespr)   1.52e-2
set beam0290(echirp)    0.0
set beam0290(energy)    9.0e9

# at entrance of section 0280 (Dump and match RTML to Main Linac)
set beam0290(betax)     3.969038431111105e+01
set beam0290(alphax)   -1.498310794232508e+00
set beam0290(emitnx)  600.0e-9
set beam0290(betay)     1.789978887234355e+01
set beam0290(alphay)    6.807489722771091e-01
set beam0290(emitny)   10.0e-9
set beam0290(sigmaz)   44.0e-6
set beam0290(meanz)     0.0e-6
set beam0290(charge)    0.65e-9
set beam0290(uncespr)   1.52e-2
set beam0290(echirp)    0.0
set beam0290(energy)    9.0e9


