#
# define RF parameters
#
# rfparams=[gradient,gradientww,phase,wavelength,a,g,l,delta,delta_g]
# units=[eV/m,eV/m,Degree,m,m,m,m,?,?]
#
# Attention: before usage the parameters must be converted to Placet units
#

# no wakes
set rfparamsbc1(gradient) [expr 13.14e6]
# with wakes
set rfparamsbc1(gradientww) [expr 13.238e6]
set rfparamsbc1(phase) 90.0
set rfparamsbc1(lambda) [expr 299792458/(2.0e9)]
set rfparamsbc1(a) 17.0e-3
set rfparamsbc1(g) 42.0e-3
set rfparamsbc1(l) 50.0e-3
set rfparamsbc1(delta) 0.0
set rfparamsbc1(delta_g) 0.0


# no wakes
set rfparamsbooster(gradient) [expr 14.832e6]
# with wakes
set rfparamsbooster(gradientww) [expr 14.881e6]
set rfparamsbooster(phase) 0.0
set rfparamsbooster(lambda) [expr 299792458/(2.0e9)]
set rfparamsbooster(a) 17.0e-3
set rfparamsbooster(g) 42.0e-3
set rfparamsbooster(l) 50.0e-3
set rfparamsbooster(delta) 0.0
set rfparamsbooster(delta_g) 0.0


# no wakes
set rfparamsbc2(gradient) [expr 90.0e6]
# with wakes
set rfparamsbc2(gradientww) [expr 94.0e6]
set rfparamsbc2(phase) 90.0
set rfparamsbc2(lambda) [expr 299792458/(12.0e9)]
set rfparamsbc2(a) [expr 1.5*3.625e-3]
set rfparamsbc2(g) 8.5417e-3
set rfparamsbc2(l) 10.417e-3
set rfparamsbc2(delta) 0.095
set rfparamsbc2(delta_g) 0.205e-3



