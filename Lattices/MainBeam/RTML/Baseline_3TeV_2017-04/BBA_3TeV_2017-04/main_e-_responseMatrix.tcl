# CLIC Main Beam RTML

##### If No wakefield file, run the create_wake.tcl script
#source create_wake.tcl
######

set tcl_precision 15
ParallelThreads -num 4

proc FF1 {} {}
proc FF2 {} {}

set latticedir ./Lattices_e-
set scriptdir ./Scripts
set paramsdir ./Parameters
set matrixdir ./ResponseMatrix

source $scriptdir/placet_units.tcl
source $paramsdir/initial_beam_parameters_e-.tcl
source $paramsdir/rf_parameters_e-.tcl
source $scriptdir/lattice_fun.tcl
source $scriptdir/beamsetup.tcl
source $scriptdir/cavitywakesetup.tcl
source $scriptdir/octave_fun.tcl
source $scriptdir/setParam.tcl
source $matrixdir/getResponseMatrix-sr.tcl
source $matrixdir/getResponseMatrix-bc1.tcl
source $matrixdir/getResponseMatrix-boo.tcl
source $matrixdir/getResponseMatrix-ca.tcl
source $matrixdir/getResponseMatrix-ltl.tcl
source $matrixdir/getResponseMatrix-tal1.tcl
source $matrixdir/getResponseMatrix-tal2.tcl
source $matrixdir/getResponseMatrix-bc2.tcl
source $matrixdir/disperseEnergy-responseMatrix.tcl
source $scriptdir/plainTracking.tcl

if {$beamparams(usewakefields)==1} {
   source rf_parameteres_ww.tcl
}

create_particles_file particles.in beamparams
create_zero_wakes_file zero_wake.dat beamparams rfparamsbc1 particles.in

BeamlineNew
source $scriptdir/beamline_and_wake.tcl
BeamlineSet -name rtml

FirstOrder 1

make_particle_beam beam1 beamparams particles.in zero_wake.dat
make_particle_beam beam2 beamparams particles.in zero_wake.dat
make_particle_beam beam3 beamparams particles.in zero_wake.dat
make_particle_beam beam4 beamparams particles.in zero_wake.dat
make_particle_beam beam5 beamparams particles.in zero_wake.dat
make_particle_beam beam6 beamparams particles.in zero_wake.dat
make_particle_beam beam7 beamparams particles.in zero_wake.dat
make_particle_beam beam8 beamparams particles.in zero_wake.dat
make_particle_beam beam9 beamparams particles.in zero_wake.dat
make_particle_beam beamDFS beamparams particles.in zero_wake.dat

Octave {
    Start = placet_get_name_number_list("rtml","Marker-SR-start");
    End = placet_get_name_number_list("rtml","Marker-BC1-start");
    [E,B] = placet_test_no_correction("rtml", "beam1", "None", 1, 0, End);
    placet_set_beam("beam2",B);

    Start = placet_get_name_number_list("rtml","Marker-BC1-start");
    End = placet_get_name_number_list("rtml","Marker-BOO-start");
    [E,B] = placet_test_no_correction("rtml", "beam2", "None", 1, Start, End);
    placet_set_beam("beam3",B);

    Start = placet_get_name_number_list("rtml","Marker-BOO-start");
    End = placet_get_name_number_list("rtml","Marker-CA-start");
    [E,B] = placet_test_no_correction("rtml", "beam3", "None", 1, Start, End);
    placet_set_beam("beam4",B);

    Start = placet_get_name_number_list("rtml","Marker-CA-start");
    End = placet_get_name_number_list("rtml","Marker-VT-start");
    [E,B] = placet_test_no_correction("rtml", "beam4", "None", 1, Start, End);
    placet_set_beam("beam5",B);

    Start = placet_get_name_number_list("rtml","Marker-VT-start");
    End = placet_get_name_number_list("rtml","Marker-LTL-start");
    [E,B] = placet_test_no_correction("rtml", "beam5", "None", 1, Start, End);
    placet_set_beam("beam6",B);

    Start = placet_get_name_number_list("rtml","Marker-LTL-start");
    End = placet_get_name_number_list("rtml","Marker-TAL1-start");
    [E,B] = placet_test_no_correction("rtml", "beam6", "None", 1, Start, End);
    placet_set_beam("beam7",B);

    Start = placet_get_name_number_list("rtml","Marker-TAL1-start");
    End = placet_get_name_number_list("rtml","Marker-TAL2-start");
    [E,B] = placet_test_no_correction("rtml", "beam7", "None", 1, Start, End);
    placet_set_beam("beam8",B);

    Start = placet_get_name_number_list("rtml","Marker-TAL2-start");
    End = placet_get_name_number_list("rtml","Marker-BC2-start");
    [E,B] = placet_test_no_correction("rtml", "beam8", "None", 1, Start, End);
    placet_set_beam("beam9",B);

    Start = placet_get_name_number_list("rtml","Marker-BOO-start");
    End = placet_get_name_number_list("rtml","Marker-CA-start");
    decrease_grad("rtml",$BOOde,Start,End);
    decrease_strength("rtml",$BOOde,Start,End);
    [E,B] = placet_test_no_correction("rtml", "beam3", "None", 1, Start, End);
    reset_grad("rtml",$BOOde,Start,End);
    reset_strength("rtml",$BOOde,Start,End);
    placet_set_beam("beamDFS",B);


}

getResponseMatrix-sr SR rtml Marker-SR-start Marker-SR-end beam1 $SRde
getResponseMatrix-bc1 BC1 rtml Marker-BC1-start Marker-BC1-end beam2 $BC1de
getResponseMatrix-boo BOO rtml Marker-BOO-start Marker-BOO-end beam3 $BOOde
getResponseMatrix-ca CA rtml Marker-CA-start Marker-VT-end beam4 $CAde
getResponseMatrix-ltl LTL rtml Marker-LTL-start Marker-LTL-end beam6 $LTLde
getResponseMatrix-tal1 TAL1 rtml Marker-TAL1-start Marker-TAL1-end beam7 $TAL1de
getResponseMatrix-tal2 TAL2 rtml Marker-TAL2-start Marker-TAL2-end beam8 $TAL2de
getResponseMatrix-bc2 BC2 rtml Marker-BC2-start Marker-BC2-end beam9 $BC2de
