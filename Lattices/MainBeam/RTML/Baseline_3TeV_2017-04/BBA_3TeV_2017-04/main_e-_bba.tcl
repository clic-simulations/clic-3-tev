# CLIC Main Beam RTML

##### If No wakefield file, run the create_wake.tcl script
#source create_wake.tcl
######

set tcl_precision 15
ParallelThreads -num 4

proc FF1 {} {}
proc FF2 {} {}

set latticedir ./Lattices_e-
set scriptdir ./Scripts
set paramsdir ./Parameters

source $scriptdir/placet_units.tcl
source $paramsdir/initial_beam_parameters_e-.tcl
source $paramsdir/rf_parameters_e-.tcl
source $scriptdir/lattice_fun.tcl
source $scriptdir/beamsetup.tcl
source $scriptdir/cavitywakesetup.tcl
source $scriptdir/octave_fun.tcl
source $scriptdir/setParam.tcl
source $scriptdir/getResponseMatrix.tcl
source $scriptdir/disperseEnergy.tcl
source $scriptdir/survey.tcl
source $scriptdir/splitBin.tcl
source $scriptdir/plainTracking.tcl
source $scriptdir/OTO.tcl
source $scriptdir/DFS.tcl
source $scriptdir/DFS-bc1.tcl
source $scriptdir/DFS-boo.tcl
source $scriptdir/DFS-ca.tcl
source $scriptdir/DFS-bc2.tcl
source $scriptdir/DFS-tal.tcl

if {$beamparams(usewakefields)==1} {
   source rf_parameteres_ww.tcl
}

create_particles_file particles.in beamparams
create_zero_wakes_file zero_wake.dat beamparams rfparamsbc1 particles.in

BeamlineNew
source $scriptdir/beamline_and_wake.tcl
BeamlineSet -name rtml

FirstOrder 1

make_particle_beam beam1 beamparams particles.in zero_wake.dat
make_particle_beam beam2 beamparams particles.in zero_wake.dat
make_particle_beam beam3 beamparams particles.in zero_wake.dat
make_particle_beam beam4 beamparams particles.in zero_wake.dat
make_particle_beam beam5 beamparams particles.in zero_wake.dat
make_particle_beam beam6 beamparams particles.in zero_wake.dat
make_particle_beam beam7 beamparams particles.in zero_wake.dat
make_particle_beam beam8 beamparams particles.in zero_wake.dat
make_particle_beam beam9 beamparams particles.in zero_wake.dat
make_particle_beam beamDFS beamparams particles.in zero_wake.dat


#Octave {
#    Start = placet_get_name_number_list("rtml","Marker-SR-start");
#    End = placet_get_name_number_list("rtml","Marker-BC1-start");
#    [E,B] = placet_test_no_correction("rtml", "beam1", "None", 1, 0, End);
#    placet_set_beam("beam2",B);
#
#}


my_survey rtml

if { [file exists beam-$machine-2.dat] } {
    Octave {
    load 'beam-$machine-2.dat'
    placet_set_beam("beam2",B);
    }
} else {
    getResponseMatrix SR rtml Marker-SR-start Marker-SR-end beam1 $SRde
    splitBin rtml SRncarray Marker-SR-start Marker-SR-end
    plainTracking SR rtml Marker-SR-start Marker-SR-end beam1
    otoCorrection SR rtml Marker-SR-start Marker-SR-end beam1 $SRbeta0 $SROTOnloop
    dfsCorrection SR rtml Marker-SR-start Marker-SR-end beam1 $SRde $SRbeta1 $SRwgt $SRDFSnloop
    
    Octave {
        Start = placet_get_name_number_list("rtml","Marker-SR-start");
        End = placet_get_name_number_list("rtml","Marker-BC1-start");
        [E,B] = placet_test_no_correction("rtml", "beam1", "None", 1, Start, End);
        placet_set_beam("beam2",B);
    
        save -text beam-$machine-2.dat B;
        Bins = Bins(1);
    }
}


if { [file exists beam-$machine-3.dat] } {
    Octave {
    load 'beam-$machine-3.dat'
    placet_set_beam("beam3",B);
    }
} else {
    getResponseMatrix BC1 rtml Marker-BC1-start Marker-BC1-end beam2 $BC1de
    splitBin rtml BC1ncarray Marker-BC1-start Marker-BC1-end
    plainTracking BC1 rtml Marker-BC1-start Marker-BC1-end beam2
    otoCorrection BC1 rtml Marker-BC1-start Marker-BC1-end beam2 $BC1beta0 $BC1OTOnloop
    dfsCorrection-bc1 BC1 rtml Marker-BC1-start Marker-BC1-end beam2 $BC1de $BC1beta1 $BC1wgt $BC1DFSnloop
    
    Octave {
        Start = placet_get_name_number_list("rtml","Marker-BC1-start");
        End = placet_get_name_number_list("rtml","Marker-BOO-start");
        [E,B] = placet_test_no_correction("rtml", "beam2", "None", 1, Start, End);
        placet_set_beam("beam3",B);
    
        save -text beam-$machine-3.dat B;
    
        Bins = Bins(1);
    }
}


if { [file exists beam-$machine-4.dat] } {
    Octave {
    load 'beam-$machine-4.dat'
    placet_set_beam("beam4",B);
    }
} else {
    getResponseMatrix BOO rtml Marker-BOO-start Marker-BOO-end beam3 $BOOde
    splitBin rtml BOOncarray Marker-BOO-start Marker-BOO-end
    plainTracking BOO rtml Marker-BOO-start Marker-BOO-end beam3
    otoCorrection BOO rtml Marker-BOO-start Marker-BOO-end beam3 $BOObeta0 $BOOOTOnloop
    dfsCorrection-boo BOO rtml Marker-BOO-start Marker-BOO-end beam3 $BOOde $BOObeta1 $BOOwgt $BOODFSnloop
    
    Octave {
        Start = placet_get_name_number_list("rtml","Marker-BOO-start");
        End = placet_get_name_number_list("rtml","Marker-CA-start");
        [E,B] = placet_test_no_correction("rtml", "beam3", "None", 1, Start, End);
        placet_set_beam("beam4",B);
        save -text beam-$machine-4.dat B;
        Bins = Bins(1);
    }
}


if { [file exists beam-$machine-5.dat] } {
    Octave {
    load 'beam-$machine-5.dat'
    placet_set_beam("beam5",B);
    }
} else {

    Octave {
        Start = placet_get_name_number_list("rtml","Marker-BOO-start");
        End = placet_get_name_number_list("rtml","Marker-CA-start");
        decrease_grad("rtml",$BOOde,Start,End);
	decrease_strength("rtml",$BOOde,Start,End);
        [E,B] = placet_test_no_correction("rtml", "beam3", "None", 1, Start, End);
        reset_grad("rtml",$BOOde,Start,End);
	reset_strength("rtml",$BOOde,Start,End);
        placet_set_beam("beamDFS",B);
    }

    getResponseMatrix CA rtml Marker-CA-start Marker-VT-end beam4 $CAde
    splitBin rtml CAncarray1 Marker-CA-start Marker-VT-end
    plainTracking CA rtml Marker-CA-start Marker-VT-end beam4
    otoCorrection CA rtml Marker-CA-start Marker-VT-end beam4 $CAbeta0 $CAOTOnloop
    dfsCorrection-ca CA rtml Marker-CA-start Marker-VT-end beam4 $CAde $CAbeta1 $CAwgt $CADFSnloop


    Octave {
        Start = placet_get_name_number_list("rtml","Marker-CA-start");
        End = placet_get_name_number_list("rtml","Marker-LTL-start");
        [E,B] = placet_test_no_correction("rtml", "beam4", "None", 1, Start, End);
        placet_set_beam("beam6",B);
        save -text beam-$machine-6.dat B;
    
        Bins = Bins(1);
    }
}


#if { [file exists beam-$machine-6.dat] } {
#    Octave {
#    load 'beam-$machine-6.dat'
#    placet_set_beam("beam6",B);
#    }
#} else {
#
#    Octave {
#        Start = placet_get_name_number_list("rtml","Marker-BOO-start");
#        End = placet_get_name_number_list("rtml","Marker-VT-start");
#        decrease_grad("rtml",$BOOde,Start,End);
#        decrease_strength("rtml",$BOOde,Start,End);
#        [E,B] = placet_test_no_correction("rtml", "beam3", "None", 1, Start, End);
#        reset_grad("rtml",$BOOde,Start,End);
#        reset_strength("rtml",$BOOde,Start,End);
#        placet_set_beam("beamDFS",B);
#    }
#
#    getResponseMatrix VT rtml Marker-VT-start Marker-VT-end beam5 $VTde
#    splitBin rtml VTncarray Marker-VT-start Marker-VT-end
#    plainTracking VT rtml Marker-VT-start Marker-VT-end beam5
#    otoCorrection VT rtml Marker-VT-start Marker-VT-end beam5 $VTbeta0 $VTOTOnloop
#    dfsCorrection VT rtml Marker-VT-start Marker-VT-end beam5 $VTde $VTbeta1 $VTwgt $VTDFSnloop
#    
#    Octave {
#        Start = placet_get_name_number_list("rtml","Marker-VT-start");
#        End = placet_get_name_number_list("rtml","Marker-LTL-start");
#        [E,B] = placet_test_no_correction("rtml", "beam5", "None", 1, Start, End);
#        placet_set_beam("beam6",B);
#        save -text beam-$machine-6.dat B;
#        Bins = Bins(1);
#    }
#}


if { [file exists beam-$machine-7.dat] } {
    Octave {
    load 'beam-$machine-7.dat'
    placet_set_beam("beam7",B);
    }
} else {
    getResponseMatrix LTL rtml Marker-LTL-start Marker-LTL-end beam6 $LTLde
    splitBin rtml LTLncarray Marker-LTL-start Marker-LTL-end
    plainTracking LTL rtml Marker-LTL-start Marker-LTL-end beam6
    otoCorrection LTL rtml Marker-LTL-start Marker-LTL-end beam6 $LTLbeta0 $LTLOTOnloop
    dfsCorrection LTL rtml Marker-LTL-start Marker-LTL-end beam6 $LTLde $LTLbeta1 $LTLwgt $LTLDFSnloop
    
    Octave {
        Start = placet_get_name_number_list("rtml","Marker-LTL-start");
        End = placet_get_name_number_list("rtml","Marker-TAL1-start");
        [E,B] = placet_test_no_correction("rtml", "beam6", "None", 1, Start, End);
        placet_set_beam("beam7",B);
        save -text beam-$machine-7.dat B;
        Bins = Bins(1);
    }
}



if { [file exists beam-$machine-8.dat] } {
    Octave {
    load 'beam-$machine-8.dat'
    placet_set_beam("beam8",B);
    }
} else {
    getResponseMatrix TAL1 rtml Marker-TAL1-start Marker-TAL1-end beam7 $TAL1de
    splitBin rtml TAL1ncarray Marker-TAL1-start Marker-TAL1-end
    plainTracking TAL1 rtml Marker-TAL1-start Marker-TAL1-end beam7
    otoCorrection TAL1 rtml Marker-TAL1-start Marker-TAL1-end beam7 $TAL1beta0 $TAL1OTOnloop
    dfsCorrection TAL1 rtml Marker-TAL1-start Marker-TAL1-end beam7 $TAL1de $TAL1beta1 $TAL1wgt $TAL1DFSnloop
    Octave {
        Start = placet_get_name_number_list("rtml","Marker-TAL1-start");
        End = placet_get_name_number_list("rtml","Marker-TAL2-start");
        [E,B] = placet_test_no_correction("rtml", "beam7", "None", 1, Start, End);
        placet_set_beam("beam8",B);
        save -text beam-$machine-8.dat B;
        Bins = Bins(1);
    }
}


if { [file exists beam-$machine-9.dat] } {
    Octave {
    load 'beam-$machine-9.dat'
    placet_set_beam("beam9",B);
    }
} else {
    getResponseMatrix TAL2 rtml Marker-TAL2-start Marker-TAL2-end beam8 $TAL2de
    splitBin rtml TAL2ncarray Marker-TAL2-start Marker-TAL2-end
    plainTracking TAL2 rtml Marker-TAL2-start Marker-TAL2-end beam8
    otoCorrection TAL2 rtml Marker-TAL2-start Marker-TAL2-end beam8 $TAL2beta0 $TAL2OTOnloop
    dfsCorrection TAL2 rtml Marker-TAL2-start Marker-TAL2-end beam8 $TAL2de $TAL2beta1 $TAL2wgt $TAL2DFSnloop


    Octave {
        Start = placet_get_name_number_list("rtml","Marker-TAL2-start");
        End = placet_get_name_number_list("rtml","Marker-BC2-start");
        [E,B] = placet_test_no_correction("rtml", "beam8", "None", 1, Start, End);
        placet_set_beam("beam9",B);
        save -text beam-$machine-9.dat B;
        Bins = Bins(1);
    }
}


if { [file exists beam-$machine-10.dat] } {
    Octave {
    load 'beam-$machine-10.dat'
    }
} else {
    getResponseMatrix BC2 rtml Marker-BC2-start Marker-BC2-end beam9 $BC2de
    splitBin rtml BC2ncarray Marker-BC2-start Marker-BC2-end
    plainTracking BC2 rtml Marker-BC2-start Marker-BC2-end beam9
    otoCorrection BC2 rtml Marker-BC2-start Marker-BC2-end beam9 $BC2beta0 $BC2OTOnloop
    dfsCorrection-bc2 BC2 rtml Marker-BC2-start Marker-BC2-end beam9 $BC2de $BC2beta1 $BC2wgt $BC2DFSnloop

    Octave {
        Start = placet_get_name_number_list("rtml","Marker-BC2-start");
        End = placet_get_name_number_list("rtml","Marker-BC2-end");
        [E,B] = placet_test_no_correction("rtml", "beam9", "None", 1, Start, End);
        save -text beam-$machine-10.dat B;
        Bins = Bins(1);
    }
}

plainTracking ALL rtml Marker-SR-start Marker-BC2-end beam1

source Scripts/save.tcl
