proc plainTracking {name beamline Start End beam} {
  global sigma
  global machine
    Octave {
            disp("TRACKING...");
	    Start = placet_get_name_number_list("$beamline","$Start");
	    End = placet_get_name_number_list("$beamline","$End");
            [E,B] = placet_test_no_correction("$beamline", "$beam", "None",1,Start,End);
	    printf("*****************************************\n");
	    printf("The emittance of X is %e\n", E(end,2));
	    printf("The emittance of Y is %e\n", E(end,6));
	    printf("*****************************************\n");
	    save(['Emitt0-' "$name-$beamline-" num2str($sigma) '-' num2str($machine) '.dat'], 'E')
    }
}
