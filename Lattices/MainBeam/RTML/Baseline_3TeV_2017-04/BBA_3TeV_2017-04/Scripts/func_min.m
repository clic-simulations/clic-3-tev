function demitt = correction1(X)

  global counter;
  global MaxIter

  counter += 1
  signal = 0.01;
  location='correction1';

  load Machine.dat;
  save X_ca.dat X location counter;

  ret = system(['placet -s main_e-_ca.tcl machine ' num2str(machine)])

  if( ret == 0 )
        sumx = load('result_ca_sumx.dat');
        sumy = load('result_ca_sumy.dat');
        sum_sigma = -1 * sum((15 * sumx .* (1+signal*randn(4,1))+ sumy .* (1+signal*randn(4,1)) ));
        demitt = sum_sigma;
        save sum_sigma.dat sum_sigma;

  else
      demitt = 100000000;
  end

end

function demitt = correction2(X)

  global counter;
  global MaxIter
  counter += 1;
  signal = 0.01;

  location='correction2';

  save X_tal.dat X location counter;
  load Machine.dat;

  ret = system(['placet -s main_e-_tal.tcl machine ' num2str(machine)])

  if( ret == 0 )
        sumx = load('result_sumx.dat');
        sumy = load('result_sumy.dat');
        sum_sigma = -1 * sum((15*sumx .* (1+signal*randn(4,1))+ sumy .* (1+signal*randn(4,1)) ));
        demitt = sum_sigma;
        save sum_sigma.dat sum_sigma;

  else
      demitt = 100000000;
  end
end

function demitt = correction3(X)

  global counter;
  global MaxIter
  counter += 1;

  signal = 0.01;

  if (counter > MaxIter)
    exit
  end

  location='correction3';

  save X_tal.dat X location counter;
  load Machine.dat;

  ret = system(['placet -s main_e-_tal.tcl machine ' num2str(machine)])

  if( ret == 0 )
        sumx = load('result_sumx.dat');
        sumy = load('result_sumy.dat');
        sum_sigma = -1 * sum((50*sumx .* (1+signal*randn(4,1))+ sumy .* (1+signal*randn(4,1)) ));
        demitt = sum_sigma;
        save sum_sigma.dat sum_sigma;

  else
      demitt = 100000000;
  end
end

