set wakelongrangedata(nmodes) 0
set wakelongrangedata(modes) {}
initialize_cavities rfparamsbc1 wakelong1 wakelongrangedata

Girder
lattice_match_dr_to_rtml beamparams
lattice_diagnostics_1 beamparams
lattice_dump_and_match_diag_to_sr beamparams
lattice_spin_rotator beamparams
lattice_match_sr_to_bc1_rf beamparams
TclCall -script {
        Octave {
                B0=placet_get_beam();
		B0=sortrows(B0,4);
		placet_set_beam(B0);

                meanz = mean(B0(:,4));
                CAV = placet_get_name_number_list('rtml','060C');

                lambda_bc1 = placet_element_get_attribute("rtml", CAV(1),"lambda");
                dphase = meanz*1e-6*360/lambda_bc1;

                placet_element_vary_attribute("rtml",CAV,'phase',dphase);
		placet_element_set_attribute("rtml",CAV,"six_dim",true);
	}
}
if {$beamparams(usewakefields)==1} {
	set rfparamsbc1(gradient) [expr $rfparamsbc1(gradientww)*$placetunits(energy)]
	set rfparamsbc1(dewake) $energy_wake_bc1
	source $scriptdir/set_wake_bc1.tcl
} else {
	set rfparamsbc1(gradient) [expr $rfparamsbc1(gradient)*$placetunits(energy)]
	set rfparamsbc1(dewake) 0
}
lattice_bc1_rf rfparamsbc1 beamparams
TclCall -script {
        Octave {
               placet_element_vary_attribute("rtml",CAV,'phase',-dphase);
        }
}
lattice_match_bc1_rf_to_chicane beamparams
lattice_bc1_chicane beamparams
lattice_match_bc1_to_diag beamparams
lattice_diagnostics_2 beamparams
lattice_dump_and_match_diag_to_booster beamparams

if {$beamparams(usewakefields)==1} {
	set rfparamsbooster(gradient) [expr $rfparamsbooster(gradientww)*$placetunits(energy)]
	set rfparamsbooster(dewake) $energy_wake_booster
	source $scriptdir/set_wake_booster.tcl
} else {
	set rfparamsbooster(gradient) [expr $rfparamsbooster(gradient)*$placetunits(energy)]
	set rfparamsbooster(dewake) 0
}
TclCall -script {
        Octave {
                B0=placet_get_beam();
                B0 = sortrows(B0,4);
                placet_set_beam(B0);
                meanz = mean(B0(:,4));
                CAV = placet_get_name_number_list('rtml','120C');

		lambda_booster = placet_element_get_attribute("rtml", CAV(1),"lambda");
                dphase = meanz*1e-6*360/lambda_booster;

                placet_element_vary_attribute("rtml",CAV,'phase',dphase);
                placet_element_set_attribute("rtml",CAV,"six_dim",true);
	}
}
set beamparams(sigmaz) 300
lattice_booster_linac rfparamsbooster beamparams
TclCall -script {
        Octave {
               placet_element_vary_attribute("rtml",CAV,'phase',-dphase);
        }
}
lattice_dump_and_match_booster_to_ca beamparams
lattice_central_arc beamparams
lattice_vertical_transfer beamparams
lattice_match_vt_to_ltl beamparams
lattice_long_transfer_line beamparams
lattice_dump_and_match_ltl_to_tal beamparams
lattice_turn_around_loop beamparams
lattice_match_tal_to_bc2_rf beamparams

TclCall -script {
        Octave {
                B0 = placet_get_beam();
                B0=sortrows(B0,4);
                placet_set_beam(B0);
                meanz = mean(B0(:,4));

                CAV = placet_get_name_number_list('rtml','210C');
		placet_element_set_attribute("rtml",CAV,"six_dim",true);
		placet_element_set_attribute("rtml",CAV,"frequency",12);

		lambda_bc2 = placet_element_get_attribute("rtml", CAV(1),"lambda");
                dphase = meanz*1e-6*360/lambda_bc2;           
                placet_element_vary_attribute("rtml",CAV,'phase',dphase);
        }
}
if {$beamparams(usewakefields)==1} {
	set rfparamsbc2(gradient) [expr $rfparamsbc2(gradientww)*$placetunits(energy)]
	set rfparamsbc2(dewake) $energy_wake_bc2
	source $scriptdir/set_wake_bc2.tcl
} else {
	set rfparamsbc2(gradient) [expr $rfparamsbc2(gradient)*$placetunits(energy)]
	set rfparamsbc2(dewake) 0
}
lattice_bc2_rf rfparamsbc2 beamparams
TclCall -script {
        Octave {
               placet_element_vary_attribute("rtml",CAV,'phase',-dphase);
        }
}
lattice_match_bc2_rf_to_chicane_1 beamparams
lattice_bc2_chicane_1 beamparams
lattice_match_bc2_chicanes beamparams
lattice_bc2_chicane_2 beamparams
lattice_match_bc2_to_diag beamparams
lattice_diagnostics_3 beamparams
lattice_dump_and_match_rtml_to_main_linac beamparams
