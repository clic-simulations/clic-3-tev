TclCall -script {
  Octave {
      std_y_perfect = [3.912599;3.507969;3.284353;3.694345].*(1+0.01*randn(4,1))*0.8;
      B0 = placet_get_beam();
      y = B0(:,3);

      meany = mean(y);
      sumy = 0;
      for j = 1:length(y)
          sumy +=  normpdf(y(j),meany,std_y_perfect(measure_index));
      end

      fid = fopen('result_sumy.dat',"a+");
      fprintf(fid,"%f\n",sumy);
      fclose(fid);

  }
}

