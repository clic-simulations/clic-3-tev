TclCall -script {
    Octave {

        R_match_x = [-0.0034364  , 4.6044507;  -0.2175248 ,  0.4604451];
        R_match_y =[ 0.28982,   3.88048;  -0.18629,   0.95612];
        M_fodo_x =[0.642788,   7.660444;  -0.076604,  0.642788];
        M_fodo_half_y =[   1.422618,   2.692375;  -0.066338,  0.577382];

        B0 = placet_get_beam();
        x = B0(:,2);
        y = B0(:,3);
        xp = B0(:,5);
        yp = B0(:,6);

        X =  transpose(R_match_x * transpose([x xp]));
        Y =  transpose(R_match_y * transpose([y yp]));

        x = X(:,1);
        xp = X(:,2);
        y = Y(:,1);
        yp = Y(:,2);

        #std_x = std(x);
        #std_y = std(y);

        #B.x = x;
        #B.y = y;
        #B.xp = xp;
        #B.yp = yp;
        #B.std_x = std_x;
        #B.std_y = std_y;


        #save beam-after-match-0.dat B;

        #std_x_perfect = 11.496;
        #std_y_perfect = 1.5689;

        Y =  transpose(M_fodo_half_y * transpose([y yp]));
        y = Y(:,1);
        yp = Y(:,2);



        sum_sigma = 0;
        for i = 1:4
        std_x_perfect = [17.730;17.730;17.730;17.730].*(1+0.000*randn(4,1));
        std_y_perfect = [1.8227;1.8227;1.8227;1.8227].*(1+0.0*randn(4,1));
	X =  transpose(M_fodo_x * transpose([x xp]));
        Y =  transpose(M_fodo_x * transpose([y yp]));

        x = X(:,1);
        xp = X(:,2);
        y = Y(:,1);
        yp = Y(:,2);

        std_x = std(x);
        std_y = std(y);

        #B.x = x;
        #B.y = y;
        #B.xp = xp;
        #B.yp = yp;
        #B.std_x = std_x;
        #B.std_y = std_y;

        #save(['beam-after-match' num2str(i) '.dat'], 'B');


        meanx = mean(x);
        meany = mean(y);
        sumx = 0;
        sumy = 0;
	for j = 1:length(x)
            if ( unifrnd(0,1) < exp(-0.5*((x(j)-meanx)/std_x_perfect(i))^2 - 0.5*((y(j)-meany)/std_y_perfect(i))^2 ))
               sumx += 1;
            end
	end

        sum_sigma += -1 * (sumx + sumy);
        #sum_sigma += 20*(std_x_perfect(i)^2/std_x^2 + std_x^2/std_x_perfect(i)^2 -2)^2 + (std_y_perfect(i)^2/std_y^2 + std_y^2/std_y_perfect(i)^2 -2)^2;
        end

        save sum_sigma.dat sum_sigma
        
    }
}
