#
# BC2 RF lattice
#
# beamparams=[betax,alphax,emitnx,betay,alphay,emitny,sigmaz,charge,
#             uncespread,echirp,energy,nslice,nmacro,nsigmabunch,nsigmawake]
# units=[m,rad,m*rad,m,rad,m*rad,m,C,1,1/m,eV,1,1,1,1]
#
# rfparams=[gradient,phase,lambda,a,g,l,delta,delta_g]
# units=[eV/m,Degree,m,m,m,m,m,m]
#
# where necessary units were converted to placet units in main.tcl

proc lattice_bc2_rf {rfparray bparray} {
upvar $rfparray rfparams
upvar $bparray beamparams

#puts [array get rfparams]
#puts [array get beamparams]

set usesixdim 1
set numthinlenses 100
set quad_synrad 0

set gradient $rfparams(gradient)
set refenergy $beamparams(meanenergy)
set q0 $beamparams(charge)
set cavphase $rfparams(phase)
set lcav 0.23

set lbpm 0.08
set lquad 0.35
set kq1 -1.65361171
set kq2 1.65361175


# This is required to take into account the energy gain due to the cavities.
# Attention! Since there will be a longitudinal offset of the bunch in these cavities
# they will induce a small acceleration.
set pi 3.14159265358979
#set dphase [expr $beamparams(meanz)*1e-6*360/$rfparams(lambda)]
set dphase 0
set deacc [expr $gradient*$lcav*cos(($cavphase-0*$dphase)/180.0*$pi)]
set cavphase [expr $cavphase+$dphase]
#energy loss due to wake fields
#set dewake [expr $rfparams(dewake)*1e6/1e9*$lcav]
set dewake $rfparams(dewake)

set de [expr $deacc+$dewake]


set ebeam $refenergy
SetReferenceEnergy $ebeam

Girder
#Bpm -length $lbpm
#Drift -length 0.04 -six_dim $usesixdim
# now this is in 0200 ...
#Quadrupole -name 210Q -synrad $quad_synrad -length [expr $lquad/2.0] -strength [expr $kq1*$lquad/2.0*$ebeam] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length 0.02 -six_dim $usesixdim

for {set cc 1} {$cc<=3} {incr cc} {
    Drift -length 0.04 -six_dim $usesixdim
    Cavity  -name "210C" -length $lcav -gradient $gradient -phase $cavphase -type 0
    set ebeam [expr $ebeam+$de]
    SetReferenceEnergy $ebeam
    Cavity  -name "210C" -length $lcav -gradient $gradient -phase $cavphase -type 0
    set ebeam [expr $ebeam+$de]
    SetReferenceEnergy $ebeam
}

Drift -length 0.02 -six_dim $usesixdim

for {set sec 1} {$sec<=6} {incr sec} {
    Girder
    #Bpm -length $lbpm
    Drift -length $lbpm
    Drift -length 0.04 -six_dim $usesixdim
    Dipole -name "D210-$sec-1" -length 0.00000000000
    Bpm -length 0.0
    Quadrupole -name "210Q" -synrad $quad_synrad -length $lquad -strength [expr $kq2*$lquad*$ebeam] -six_dim $usesixdim -thin_lens $numthinlenses
    Drift -length 0.02 -six_dim $usesixdim

    for {set cc 1} {$cc<=3} {incr cc} {
	Drift -length 0.04 -six_dim $usesixdim
        Cavity -name "210C" -length $lcav -gradient $gradient -phase $cavphase -type 0
	set ebeam [expr $ebeam+$de]
        SetReferenceEnergy $ebeam
	Cavity -name "210C" -length $lcav -gradient $gradient -phase $cavphase -type 0
        set ebeam [expr $ebeam+$de]
	SetReferenceEnergy $ebeam
    }

    Drift -length 0.02 -six_dim $usesixdim

    Girder
    #Bpm -length $lbpm
    Drift -length $lbpm
    Drift -length 0.04 -six_dim $usesixdim
    Dipole -name "D210-$sec-2" -length 0.00000000000
    Bpm -length 0.0
    Quadrupole -name "210Q" -synrad $quad_synrad -length $lquad -strength [expr $kq1*$lquad*$ebeam] -six_dim $usesixdim -thin_lens $numthinlenses
    Drift -length 0.02 -six_dim $usesixdim

    for {set cc 1} {$cc<=3} {incr cc} {
	Drift -length 0.04 -six_dim $usesixdim
        Cavity -name "210C" -length $lcav -gradient $gradient -phase $cavphase -type 0
	set ebeam [expr $ebeam+$de]
        SetReferenceEnergy $ebeam
	Cavity -name "210C" -length $lcav -gradient $gradient -phase $cavphase -type 0
        set ebeam [expr $ebeam+$de]
	SetReferenceEnergy $ebeam
    }

    Drift -length 0.02 -six_dim $usesixdim
}

#Bpm -length $lbpm
Drift -length $lbpm
Drift -length 0.04 -six_dim $usesixdim
set beamparams(meanenergy) $ebeam
#puts "Setting beamparams(meanenergy)=$beamparams(meanenergy)."
}
