#step 1 : generate wakefield file
placet create_wake.tcl 

#step 2: generate (or copy) response matrix
placet main_e-_responseMatrix.tcl
(cp ResponseMatrix/R_* .)

#step 3: Apply the BBA for different random seed
octave BBA.m
