%	
% convert output of Placet to CSRTrack fmt3



% --------------------------------------------------------------------
function convert_placet_to_fmt3(inputbasename,outputbasename)

global elegantdata;
global qtot;
global npart;
global g0;

mkdir("csrtdata");
cd("csrtdata");
fidlog=fopen("log.txt","wt");

qtot=0.65e-9

outputfile=[outputbasename,"_0001.fmt3"]
inputfile=["../",inputbasename,"_step_1a.dat"];
conv_to_fmt3_b(outputfile,inputfile);
fprintf(fidlog,"  monitor=%s\n",outputfile);

outputfile=[outputbasename,"_0002.fmt3"]
inputfile=["../",inputbasename,"_step_1b.dat"];
conv_to_fmt3_b(outputfile,inputfile);
fprintf(fidlog,"  monitor=%s\n",outputfile);

outputfile=[outputbasename,"_0003.fmt3"]
inputfile=["../",inputbasename,"_step_1c.dat"];
conv_to_fmt3_b(outputfile,inputfile);
fprintf(fidlog,"  monitor=%s\n",outputfile);

outputfile=[outputbasename,"_0004.fmt3"]
inputfile=["../",inputbasename,"_step_1d.dat"];
conv_to_fmt3_b(outputfile,inputfile);
fprintf(fidlog,"  monitor=%s\n",outputfile);

outputfile=[outputbasename,"_0005.fmt3"]
inputfile=["../",inputbasename,"_step_1e.dat"];
conv_to_fmt3_b(outputfile,inputfile);
fprintf(fidlog,"  monitor=%s\n",outputfile);

outputfile=[outputbasename,"_0006.fmt3"]
inputfile=["../",inputbasename,"_step_2a.dat"];
conv_to_fmt3_b(outputfile,inputfile);
fprintf(fidlog,"  monitor=%s\n",outputfile);

outputfile=[outputbasename,"_0007.fmt3"]
inputfile=["../",inputbasename,"_step_2b.dat"];
conv_to_fmt3_b(outputfile,inputfile);
fprintf(fidlog,"  monitor=%s\n",outputfile);

outputfile=[outputbasename,"_0008.fmt3"]
inputfile=["../",inputbasename,"_step_2c.dat"];
conv_to_fmt3_b(outputfile,inputfile);
fprintf(fidlog,"  monitor=%s\n",outputfile);

outputfile=[outputbasename,"_0009.fmt3"]
inputfile=["../",inputbasename,"_step_2d.dat"];
conv_to_fmt3_b(outputfile,inputfile);
fprintf(fidlog,"  monitor=%s\n",outputfile);

outputfile=[outputbasename,"_0010.fmt3"]
inputfile=["../",inputbasename,"_step_2e.dat"];
conv_to_fmt3_b(outputfile,inputfile);
fprintf(fidlog,"  monitor=%s\n",outputfile);

outputfile=[outputbasename,"_0011.fmt3"]
inputfile=["../",inputbasename,"_step_2f.dat"];
conv_to_fmt3_b(outputfile,inputfile);
fprintf(fidlog,"  monitor=%s\n",outputfile);

outputfile=[outputbasename,"_0012.fmt3"]
inputfile=["../",inputbasename,"_step_2g.dat"];
conv_to_fmt3_b(outputfile,inputfile);
fprintf(fidlog,"  monitor=%s\n",outputfile);

outputfile=[outputbasename,"_0013.fmt3"]
inputfile=["../",inputbasename,"_step_3a.dat"];
conv_to_fmt3_b(outputfile,inputfile);
fprintf(fidlog,"  monitor=%s\n",outputfile);

outputfile=[outputbasename,"_0014.fmt3"]
inputfile=["../",inputbasename,"_step_3b.dat"];
conv_to_fmt3_b(outputfile,inputfile);
fprintf(fidlog,"  monitor=%s\n",outputfile);

outputfile=[outputbasename,"_0015.fmt3"]
inputfile=["../",inputbasename,"_step_3c.dat"];
conv_to_fmt3_b(outputfile,inputfile);
fprintf(fidlog,"  monitor=%s\n",outputfile);

outputfile=[outputbasename,"_0016.fmt3"]
inputfile=["../",inputbasename,"_step_3d.dat"];
conv_to_fmt3_b(outputfile,inputfile);
fprintf(fidlog,"  monitor=%s\n",outputfile);



fclose(fidlog);
cd("../");
%done

endfunction;

% --------------------------------------------------------------------
% --------------------------------------------------------------------

function conv_to_fmt3(outfile,infile)

global qtot;
global npart;
global g0;

% extract data
% x xp y yp s de
indata=load(infile);
npart=length(indata)
% first row : t,g0,0,0,0,0,0,ss,sx,sy
% second row: ref x,ref xp,ref y,ref yp,ref s,ref de,ref q,ref ws,ref wx,ref wy
% other rows: x,xp,y,yp,s,de,q,ws,wx,wy
%
% Attention: my x,y != CSRTrack x,y, but my x,y, = CSRTrack h,v

c_x =indata(:,1);
c_xp=indata(:,2);
c_y =indata(:,3);
c_yp=indata(:,4);
c_s =-indata(:,5);
c_de=indata(:,6);
c_q =ones(npart,1)*qtot/npart;
c_ws=zeros(npart,1);
c_wx=zeros(npart,1);
c_wy=zeros(npart,1);

mean_s=sum(c_s)/npart;
%c_s=-(c_s-mean_s)*299792458;
mean_de=sum(c_de)/npart;
%c_de=(c_de-g0)/g0;

mean_x=sum(c_x)/npart;
mean_xp=sum(c_xp)/npart;
mean_y=sum(c_y)/npart;
mean_yp=sum(c_yp)/npart;

c=[c_x,c_xp,c_y,c_yp,c_s,c_de,c_q,c_ws,c_wx,c_wy];

r1=[mean_s,g0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0];
r2=[mean_x,mean_xp,mean_y,mean_yp,mean_s,mean_de,0.0,0.0,0.0,0.0];

fmt3data=[r1;r2;c];

fid=fopen(outfile,"wt");
fprintf(fid,"%18.10e %18.10e %18.10e %18.10e %18.10e %18.10e %18.10e %3.1f %3.1f %3.1f\n",fmt3data.');
fclose(fid);

endfunction;


% --------------------------------------------------------------------
% --------------------------------------------------------------------

function conv_to_fmt3_b(outfile,infile)

global qtot;
global npart;
global g0;

% extract data
% x xp y yp s de
indata=load(infile);
npart=length(indata)
% first row : t,g0,0,0,0,0,0,ss,sx,sy
% second row: ref x,ref xp,ref y,ref yp,ref s,ref de,ref q,ref ws,ref wx,ref wy
% other rows: x,xp,y,yp,s,de,q,ws,wx,wy
%
% Attention: my x,y != CSRTrack x,y, but my x,y, = CSRTrack h,v

c_x =indata(:,2)*1e-6;
c_xp=indata(:,5)*1e-6;
c_y =indata(:,3)*1e-6;
c_yp=indata(:,6)*1e-6;
c_s =-indata(:,4)*1e-6;
c_de=indata(:,1)*1e9;
mean_e=sum(c_de)/npart;
g0=mean_e/510998.9
c_de=c_de/mean_e-1;
c_q =ones(npart,1)*qtot/npart;
c_ws=zeros(npart,1);
c_wx=zeros(npart,1);
c_wy=zeros(npart,1);

mean_s=sum(c_s)/npart;
%c_s=-(c_s-mean_s)*299792458;
mean_de=sum(c_de)/npart;
%c_de=(c_de-g0)/g0;

mean_x=sum(c_x)/npart;
mean_xp=sum(c_xp)/npart;
mean_y=sum(c_y)/npart;
mean_yp=sum(c_yp)/npart;

c=[c_x,c_xp,c_y,c_yp,c_s,c_de,c_q,c_ws,c_wx,c_wy];

r1=[mean_s,g0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0];
r2=[-mean_x,-mean_xp,-mean_y,-mean_yp,-mean_s,-mean_de,0.0,0.0,0.0,0.0];

fmt3data=[r1;r2;c];

fid=fopen(outfile,"wt");
fprintf(fid,"%18.10e %18.10e %18.10e %18.10e %18.10e %18.10e %18.10e %3.1f %3.1f %3.1f\n",fmt3data.');
fclose(fid);

endfunction;


% --------------------------------------------------------------------
% --------------------------------------------------------------------

