#
# the big tracking procedure
#
proc do_tracking {beamlinename beamname bparray stepnum} {
    global zoffset
    global placetunits
    upvar $bparray beamparams
    
    puts ""
    puts "Perform tracking step $stepnum using beamline $beamlinename and beam $beamname"
    puts ""
    
    set betax0 $beamparams(betax)
    set alphax0 $beamparams(alphax)
    set betay0 $beamparams(betay)
    set alphay0 $beamparams(alphay)
    
    #
    # store phase space distribution in file
    #
    puts "Dumping beam to file..."
    set outname [format "bunch_initial_step_%d.dat" $stepnum]
    BeamDump -beam $beamname -file $outname
    puts "...Done!"

    #
    # plot Twiss functions
    #
    puts "Dumping Twiss data to file..."
    set outname [format "twiss_step_%d.dat" $stepnum]
#    TwissPlotStep -file $outname -beam $beamname -step 0.50
    TwissPlot -file $outname -beam $beamname
    puts "...Done!"

    #
    # dump energy spread along beam line
    #
    puts "Dumping energy spread to file..."
    set outname [format "e_spread_step_%d.dat" $stepnum]
    EnergySpreadPlot -file $outname -beam $beamname
    puts "...Done!"

    #
    # dump energy along beam line
    #
    puts "Dumping energy to file..."
    set outname [format "energy_step_%d.dat" $stepnum]
    BeamEnergyPlot -file $outname -beam $beamname
    puts "...Done!"


    
    #
    # dump AML file of lattice
    #
    BeamlineSaveAML -beamline $beamlinename -file [format "aml_lattice_%d.aml" $stepnum]

    #
    SurveyErrorSet \
        -quadrupole_x 0.0 \
        -quadrupole_y 0.0 \
        -quadrupole_xp 0.00 \
        -quadrupole_yp 0.00 \
        -quadrupole_roll 0.0 \
        -cavity_x 0.0 \
        -cavity_y 0.0 \
        -cavity_xp 0.0 \
        -cavity_yp 0.0 \
        -bpm_x 0 \
        -bpm_y 0

    
    #
    # we do tracking, plotting, etc. in octave
    #
    Octave {
        format long;
        % read in some useful functions
        addpath("./Placet_Octave");
        scriptdir="./Scripts/";
        source([scriptdir,"octave_beam_statistics.m"]);
        source([scriptdir,"octave_correction_algorithms.m"]);
        
        % try placet_evolve_beta_function
        elems=placet_get_name_number_list("$beamlinename","*");
        elemscount=size(elems,2);
        [tw_s,tw_betax,tw_betay,tw_alphax,tw_alphay,tw_mux,tw_muy,tw_dx,tw_dy]=placet_evolve_beta_function($betax0,$alphax0,$betay0,$alphay0,0,elemscount-1);
        outname=sprintf("twiss_test_step_%d.dat",$stepnum);
        fid=fopen(outname,"wt");
        fprintf(fid,"\# s betax alphax betay alphay mux muy dx dy\n");
        outdata=[tw_s',tw_betax,tw_alphax,tw_betay,tw_alphay,tw_mux',tw_muy',tw_dx',tw_dy']';
        fprintf(fid,"%20.12e %20.12e %20.12e %20.12e %20.12e %20.12e %20.12e %20.12e %20.12e\n",outdata);
        fclose(fid);
        
        % read in the initial Placet beam for usage in Octave
        beamini=placet_get_beam("$beamname");
        beamini=units_placet_to_std(beamini);
        deeini=beamini(:,1)/mean(beamini(:,1),1)-1;
        
        outname=sprintf("phasespaces_initial_step_%d.dat",$stepnum);
        fid=fopen(outname,"wt");
        fprintf(fid,"\# x xp y yp s de\n");
        outdata=[beamini(:,2),beamini(:,5),beamini(:,3),beamini(:,6),beamini(:,4),deeini]';
        fprintf(fid,"%20.12e %20.12e %20.12e %20.12e %20.12e %20.12e\n",outdata);
        fclose(fid);
        
        % track beam
        [emitt,beamfin]=placet_test_no_correction("$beamlinename","$beamname","Zero");
        beamfin=units_placet_to_std(beamfin);
        deefin=beamfin(:,1)/mean(beamfin(:,1),1)-1;
        print_mean_rms(beamini);
        print_mean_rms(beamfin);
        save_mean_rms(beamfin,"stat.txt");
        
        outname=sprintf("emittance_step_%d.dat",$stepnum);
        fid=fopen(outname,"wt");
        fprintf(fid,"\# index avgepsx sigmaepsx avgtrajx 0 avgepsy sigmaepsy envelop 0 machinecount\n");
        outdata=[emitt(:,1),emitt(:,2),emitt(:,3),emitt(:,4),emitt(:,5),emitt(:,6),emitt(:,7),emitt(:,8),emitt(:,9),emitt(:,10)]';
        fprintf(fid,"%20.12e %20.12e %20.12e %20.12e %20.12e %20.12e %20.12e %20.12e %20.12e %20.12e\n",outdata);
        fclose(fid);
        
        outname=sprintf("phasespaces_final_step_%d.dat",$stepnum);
        fid=fopen(outname,"wt");
        fprintf(fid,"\# x xp y yp s de\n");
        outdata=[beamfin(:,2),beamfin(:,5),beamfin(:,3),beamfin(:,6),beamfin(:,4),deefin]';
        fprintf(fid,"%20.12e %20.12e %20.12e %20.12e %20.12e %20.12e\n",outdata);
        fclose(fid);
        
        w=ones(length(beamfin),1);
        [xn,xpn,yn,ypn,sn,den,wn]=clear_outliers_6d(beamfin,w);
        deecore=den/mean(den,1)-1;
        outname=sprintf("phasespaces_final_step_%d_core.dat",$stepnum);
        fid=fopen(outname,"wt");
        fprintf(fid,"\# x xp y yp s de\n");
        outdata=[xn,xpn,yn,ypn,sn,deecore]';
        fprintf(fid,"%20.12e %20.12e %20.12e %20.12e %20.12e %20.12e\n",outdata);
        fclose(fid);

        
        % calculate new beam params for later use in TCL

        new_rmss=rms(beamfin(:,4),1);
        new_means=mean(beamfin(:,4),1);
        new_meanenergy=mean(beamfin(:,1),1);
        new_gamma=new_meanenergy/(0.5109989*1e6);
        new_espread=rms(beamfin(:,1),1)/new_meanenergy;
        new_lemit=emitproj(beamfin(:,4),beamfin(:,1)-new_meanenergy,1);
        new_uncespread=new_lemit/(new_rmss*new_meanenergy);
        new_echirp=sqrt((new_espread^2-new_uncespread^2)/new_rmss^2);
        [new_betax,new_alphax]=twiss(beamfin(:,2),beamfin(:,5),1);
        new_emitnx=new_gamma*emitproj(beamfin(:,2),beamfin(:,5),1);
        [new_betay,new_alphay]=twiss(beamfin(:,3),beamfin(:,6),1);
        new_emitny=new_gamma*emitproj(beamfin(:,3),beamfin(:,6),1);
        
        Tcl_SetVar("new_rmss",new_rmss);
        Tcl_SetVar("new_means",new_means);
        Tcl_SetVar("new_meanenergy",new_meanenergy);
        Tcl_SetVar("new_uncespread",new_uncespread);
        Tcl_SetVar("new_echirp",new_echirp);
        Tcl_SetVar("new_betax",new_betax);
        Tcl_SetVar("new_alphax",new_alphax);
        Tcl_SetVar("new_emitnx",new_emitnx);
        Tcl_SetVar("new_betay",new_betay);
        Tcl_SetVar("new_alphay",new_alphay);
        Tcl_SetVar("new_emitny",new_emitny);
        
    }
    #TestNoCorrection -beam beam1 -survey none
    #TestQuadrupoleJitter -machines 100 -a0 0.0001 -a1 1.0 -steps 5 -beam beam1 -file emitt.dat
    #TestSimpleCorrectionDipole -machines 1 -binlength 100 -binoverlap 50 -bpm_resolution 0.1 -beam beam1 -testbeam beam1 -survey clic -emitt_fil emitt.dat
    
    #trajectorycorrection("rtml","beam1",10,"bpm.dat","dipole.dat","correction_statistics.dat");
    #[emitt,beamfin]=placet_test_no_correction("rtml","beam1","Zero");
    #beamfin=units_placet_to_std(beamfin);
    #deefin=beamfin(:,1)/mean(beamfin(:,1),1)-1;

    # end of tracking part
    
    # define the offset of the longitudinal coordinate z
    array set blinfo [BeamlineInfo]
    set deltaz $blinfo(length)
    set zoffset [expr $zoffset + $deltaz]
    set fid [open zoffset.dat a]
    set stmp [expr $stepnum+1]
    puts $fid "$stmp   $zoffset"
    close $fid
    
    # set new beamparams
    set beamparams(sigmaz) [expr $new_rmss * $placetunits(xyz)]
    set beamparams(meanz) [expr $new_means * $placetunits(xyz)]
    set beamparams(meanenergy) [expr $new_meanenergy * $placetunits(energy)]
    set beamparams(startenergy) [expr $new_meanenergy * $placetunits(energy)]
    set beamparams(uncespread) $new_uncespread
    set beamparams(echirp) [expr $new_echirp/$placetunits(energy)]
    set beamparams(betax) $new_betax
    set beamparams(alphax) $new_alphax
    set beamparams(emitnx) [expr $new_emitnx * $placetunits(emittance)]
    set beamparams(betay) $new_betay
    set beamparams(alphay) $new_alphay
    set beamparams(emitny) [expr $new_emitny * $placetunits(emittance)]
    set beamparams(charge) $beamparams(charge)
}
