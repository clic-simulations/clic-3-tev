#
# particle beam setup
#
# 
# general parameters have to be store before
# in the array beamparams=
# [betax,alphax,emitnx,betay,alphay,emitny,sigmaz,charge,
#  uncespread,echirp,energy,nslice,nmacro,nsigmabunch,nsigmawake]
# units=[m,rad,m*rad,m,rad,m*rad,m,C,1,1/m,eV,1,1,1,1]
#

proc create_particles_file {outfile bparray} {
    global placetunits
    upvar $bparray beamparams

    set bx $beamparams(betax)
    set ax $beamparams(alphax)
    set enx $beamparams(emitnx)
    set by $beamparams(betay)
    set ay $beamparams(alphay)
    set eny $beamparams(emitny)
    set rmsz $beamparams(sigmaz)
    set meanz $beamparams(meanz)
    set espread $beamparams(uncespread)
    set echirp $beamparams(echirp)
    set energy $beamparams(startenergy)
    set nslice $beamparams(nslice)
    set nmacro $beamparams(nmacro)
    set nsigma $beamparams(nsigmabunch)

    set npart [expr $nmacro*$nslice]

    set pu_xyz $placetunits(xyz)
    set pu_xpyp $placetunits(xpyp)
    set pu_e $placetunits(energy)
    set pu_emit $placetunits(emittance)

    # the creation of the particle distribution is done in Octave
    Octave {
    format long;
    % read in some required functions
    scriptdir="./Scripts/";
    source([scriptdir,"octave_beam_statistics.m"]);
    source([scriptdir,"octave_beam_creation.m"]);

    create_particle_distribution("$outfile",$bx,$ax,$enx,$by,$ay,$eny,$rmsz,$meanz,$espread,$echirp,$energy,$nsigma,$npart,$pu_xyz,$pu_xpyp,$pu_e,$pu_emit);
    }
    # end of Octave part
}


proc make_particle_beam {name bparray particlesfile wakefile} {
    upvar $bparray beamparams

# the following is only required to set up the internal structure which stores the beam,
# by using BeamRead at the end all particle data is overwritten.

    InjectorBeam $name -bunches 1 \
	    -macroparticles $beamparams(nmacro) \
	    -particles [expr $beamparams(nslice)*$beamparams(nmacro)] \
	    -energyspread $beamparams(uncespread) \
	    -ecut $beamparams(nsigmawake) \
	    -e0 $beamparams(startenergy) \
	    -file $wakefile \
	    -chargelist {1.0} \
	    -charge $beamparams(charge) \
	    -phase 0.0 \
	    -overlapp 0.0 \
	    -distance 0.0 \
	    -beta_x $beamparams(betax) \
	    -alpha_x $beamparams(alphax) \
	    -emitt_x $beamparams(emitnx) \
	    -beta_y $beamparams(betay) \
	    -alpha_y $beamparams(alphay) \
	    -emitt_y $beamparams(emitny)

# now load the real particles
    BeamRead -file $particlesfile -beam $name
}
