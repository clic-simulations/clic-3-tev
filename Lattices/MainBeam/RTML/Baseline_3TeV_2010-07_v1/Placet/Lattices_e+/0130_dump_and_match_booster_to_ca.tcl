#
# Dump and matching of booster to central arc
#
# beamparams=[betax,alphax,emitnx,betay,alphay,emitny,sigmaz,charge,
#             uncespread,echirp,energy,nslice,nmacro,nsigmabunch,nsigmawake]
# units=[m,rad,m*rad,m,rad,m*rad,m,C,1,1/m,eV,1,1,1,1]
#
# where necessary units were converted to placet units in main.tcl

proc lattice_dump_and_match_booster_to_ca {bparray} {
upvar $bparray beamparams

set usesixdim 1
set numthinlenses 100
set quad_synrad 0

set refenergy $beamparams(meanenergy)

set lquadm 0.3

set kqm1 -0.3405916589
set kqm2 1.298511695
set kqm3 -0.9757512869
set kqm4 0.2607184639
set kqm5 1.485783277

set ldm1 0.7
set ldm2 2.8
set ldm3 5.0
set ldm4 1.15
set ldm5 1.2

SetReferenceEnergy $refenergy

Girder
Drift -length $ldm1 -six_dim $usesixdim
Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm1*$lquadm*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ldm2 -six_dim $usesixdim
Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm2*$lquadm*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ldm3 -six_dim $usesixdim
Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm3*$lquadm*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ldm4 -six_dim $usesixdim
Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm4*$lquadm*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ldm5 -six_dim $usesixdim
Quadrupole -synrad $quad_synrad -length [expr $lquadm/2.0] -strength [expr $kqm5*$lquadm/2.0*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
}
