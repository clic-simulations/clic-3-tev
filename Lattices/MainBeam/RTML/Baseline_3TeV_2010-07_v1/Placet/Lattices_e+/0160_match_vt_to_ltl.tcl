#
# matching vertical transfer to long transfer line
#
# beamparams=[betax,alphax,emitnx,betay,alphay,emitny,sigmaz,charge,
#             uncespread,echirp,energy,nslice,nmacro,nsigmabunch,nsigmawake]
# units=[m,rad,m*rad,m,rad,m*rad,m,C,1,1/m,eV,1,1,1,1]
#
# where necessary units were converted to placet units in main.tcl

proc lattice_match_vt_to_ltl {bparray} {
upvar $bparray beamparams

set usesixdim 1
set numthinlenses 100
set quad_synrad 0

set pi 3.141592653589793
set c 299792458
set q0 1.6021765e-19 
set eps0 [expr 1/(4e-7*$pi*$c*$c)]

set refenergy $beamparams(meanenergy)

set lquadm 0.3

set kqm1 -0.4699339275
set kqm2 1.341241092
set kqm3 -1.187498499
set kqm4 0.2321395202
set kqm5 0.6107435442

set ldm1 0.8
set ldm2 1.5
set ldm3 8.0
set ldm4 7.0

SetReferenceEnergy $refenergy

Girder
Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm1*$lquadm*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ldm1 -six_dim $usesixdim
Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm2*$lquadm*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ldm2 -six_dim $usesixdim
Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm3*$lquadm*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ldm3 -six_dim $usesixdim
Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm4*$lquadm*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ldm4 -six_dim $usesixdim
Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm5*$lquadm*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses


# second horizontal arc of dog-leg
set usesynrad $beamparams(useisr)
set usesixdim 1
set numthinlenses 100
set quad_synrad 0

set lbend 2.0
set lquad 0.3
set lsext 0.2

# bend to the right
set msign 1.0
set thetaa [expr $msign*1.3/180*$pi]
set rhoa [expr $lbend/(2*sin($thetaa/2))]
set larca [expr $thetaa*$rhoa]

set thetab [expr $msign*1.7/180*$pi]
set rhob [expr $lbend/(2*sin($thetab/2))]
set larcb [expr $thetab*$rhob]

set ld01 2.00
set ld02 0.60
set ld03 1.90
set ld04 2.35
set ld05 0.40
set ld06 0.30
set ld07 $ld05
set ld08 $ld04
set ld09 $ld03
set ld10 $ld02
set ld11 $ld01

set ld03a [expr $ld03-0.3]
set ld03b 0.1
set ld09a 0.1
set ld09b [expr $ld09-0.3]

set kq01 1.485783277
set kq02 -1.205627435
set kq03 2.026330565
set kq04 -0.5264630418
set kq05 $kq04
set kq06 $kq03
set kq07 $kq02
set kq08 $kq01

set ks01 [expr $msign*68.0]
set ks02 [expr $msign*67.7]

SetReferenceEnergy $refenergy

Girder
Drift -length $ld01 -six_dim $usesixdim
Sbend -length $larca -angle $thetaa -synrad $usesynrad -six_dim $usesixdim -thin_lens $numthinlenses -e0 $refenergy -E1 [expr $thetaa/2.0] -E2 [expr $thetaa/2.0]
set g0 [expr $refenergy/0.000510999]
set de [expr 1/(6*$pi*$eps0)*$q0*$q0*$c*$g0*$g0*$g0*$g0/($rhoa*$rhoa)*$larca/$c]
set refenergy [expr $refenergy-$de/$q0/1e9*$usesynrad]
SetReferenceEnergy $refenergy
Drift -length $ld02 -six_dim $usesixdim
Quadrupole -synrad $quad_synrad -length $lquad -strength [expr $kq02*$lquad*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ld03a -six_dim $usesixdim
Multipole -type 3 -length $lsext -e0 $refenergy -strength [expr $ks01*$refenergy*$lsext] -thin_lens $numthinlenses -six_dim $usesixdim
Drift -length $ld03b -six_dim $usesixdim
Quadrupole -synrad $quad_synrad -length $lquad -strength [expr $kq03*$lquad*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ld04 -six_dim $usesixdim
Quadrupole -synrad $quad_synrad -length $lquad -strength [expr $kq04*$lquad*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ld05 -six_dim $usesixdim
Sbend -length $larcb -angle $thetab -synrad $usesynrad -six_dim $usesixdim -thin_lens $numthinlenses -e0 $refenergy -E1 [expr $thetab/2.0] -E2 [expr $thetab/2.0]
set g0 [expr $refenergy/0.000510999]
set de [expr 1/(6*$pi*$eps0)*$q0*$q0*$c*$g0*$g0*$g0*$g0/($rhob*$rhob)*$larcb/$c]
set refenergy [expr $refenergy-$de/$q0/1e9*$usesynrad]
SetReferenceEnergy $refenergy
Drift -length $ld06 -six_dim $usesixdim
Sbend -length $larcb -angle $thetab -synrad $usesynrad -six_dim $usesixdim -thin_lens $numthinlenses -e0 $refenergy -E1 [expr $thetab/2.0] -E2 [expr $thetab/2.0]
set g0 [expr $refenergy/0.000510999]
set de [expr 1/(6*$pi*$eps0)*$q0*$q0*$c*$g0*$g0*$g0*$g0/($rhob*$rhob)*$larcb/$c]
set refenergy [expr $refenergy-$de/$q0/1e9*$usesynrad]
SetReferenceEnergy $refenergy
Drift -length $ld07 -six_dim $usesixdim
Quadrupole -synrad $quad_synrad -length $lquad -strength [expr $kq05*$lquad*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ld08 -six_dim $usesixdim
Quadrupole -synrad $quad_synrad -length $lquad -strength [expr $kq06*$lquad*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ld09a -six_dim $usesixdim
Multipole -type 3 -length $lsext -e0 $refenergy -strength [expr $ks02*$refenergy*$lsext] -thin_lens $numthinlenses -six_dim $usesixdim
Drift -length $ld09b -six_dim $usesixdim
Quadrupole -synrad $quad_synrad -length $lquad -strength [expr $kq07*$lquad*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ld10 -six_dim $usesixdim
Sbend -length $larca -angle $thetaa -synrad $usesynrad -six_dim $usesixdim -thin_lens $numthinlenses -e0 $refenergy -E1 [expr $thetaa/2.0] -E2 [expr $thetaa/2.0]
set g0 [expr $refenergy/0.000510999]
set de [expr 1/(6*$pi*$eps0)*$q0*$q0*$c*$g0*$g0*$g0*$g0/($rhoa*$rhoa)*$larca/$c]
set refenergy [expr $refenergy-$de/$q0/1e9*$usesynrad]
SetReferenceEnergy $refenergy
Drift -length $ld11 -six_dim $usesixdim

# match to ltl
set lquadm 0.3
set lquadx 0.36

set kqm1 0.5475086249
set kqm2 0.12222078
set kqm3 -0.2258579917
set kqm4 0.08070684601
set kqm5 0.02490857395
set kqm6 0.009713020617

set ldm1 35.0
set ldm2 8.5
set ldm3 8.0
set ldm4 6.4
set ldm5 5.0

SetReferenceEnergy $refenergy

Girder
Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm1*$lquadm*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ldm1 -six_dim $usesixdim
Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm2*$lquadm*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ldm2 -six_dim $usesixdim
Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm3*$lquadm*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ldm3 -six_dim $usesixdim
Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm4*$lquadm*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ldm4 -six_dim $usesixdim
Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm5*$lquadm*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ldm5 -six_dim $usesixdim
Quadrupole -synrad $quad_synrad -length [expr $lquadx/2.0] -strength [expr $kqm6*$lquadx/2.0*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses

set beamparams(meanenergy) $refenergy
puts "Setting beamparams(meanenergy)=$beamparams(meanenergy)."
}
