## -*- texinfo -*-
## @deftypefn {Function File} {} placet_test_simple_correction_opt (@var{beamline}, @var{beam}, @var{survey}, @var{options})
## Tracks 'beam0' through 'beamline' and applies 1-to-1 correction, using selected 'options'. 'options' is a structure containing the following items:
##
## @verbatim
##  options.B = placet_get_number_list("beamline", "bpm");
##  options.C = placet_get_number_list("beamline", "dipole");
##  options.observable = "reading_y";
##  options.leverage = "strength_y";
##  [options.R, options.T] = placet_get_response_matrix_opt("beamline", "beam0", "Zero", options);
##  options.gain = 1.0;
##  options.pinv_tol = svd(options.R)(1)*1e-3;
## @end verbatim
## @end deftypefn

function E=placet_test_simple_correction_opt(beamline, beam, survey, options)
  if nargin==4 && ischar(beamline) && ischar(beam) && ischar(survey) && ischaruct(options)
    placet_test_no_correction(beamline, beam, survey, 1);
    bpms=placet_element_get_attribute(beamline, options.B, options.observable) - options.T;
    if options.pinv_tol==0.0
      correction=-options.R\bpms';
    else
      correction=-pinv(options.R, options.pinv_tol)*bpms';
    endif
    placet_element_vary_attribute(beamline, options.C, options.leverage, options.gain * correction);
    E=placet_test_no_correction(beamline, beam, "None", 1); 
  else  
    help placet_test_simple_correction_opt
  endif
endfunction
