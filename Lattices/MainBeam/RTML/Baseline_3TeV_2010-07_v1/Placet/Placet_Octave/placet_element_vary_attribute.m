## -*- texinfo -*-
## @deftypefn {Function File} {} placet_element_vary_attribute ("beamline", index, "attribute", value)
## Vary the attribute of an element (new value
## 
## Usage:
##      x=placet_element_set_attribute("beamline", index, "attribute", value)
## 
## Input:
## @verbatim
##      beamline:    beamline name [STRING]
##      index:       index position (or vector of indexes) of the element [INTEGER]
##      attribute:   attribute name [STRING]
##      value:       variation(s) to be added to the current value(s) [DOUBLE]
## @end verbatim
## @end deftypefn

function placet_element_vary_attribute(beamline, index, attribute, value)
  if nargin==4 && ischar(beamline) && ismatrix(index) && ischar(attribute) && ismatrix(value)
    x0=placet_element_get_attribute(beamline, index, attribute);
    if !isscalar(value)
      if size(x0)!=size(value)
        value=value';
      endif
    endif
    placet_element_set_attribute(beamline, index, attribute, x0+value);
  else
    help placet_element_vary_attribute
  endif
endfunction
