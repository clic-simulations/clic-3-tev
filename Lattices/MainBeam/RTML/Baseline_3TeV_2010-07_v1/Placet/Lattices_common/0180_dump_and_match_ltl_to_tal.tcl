#
# Dump and match long transfer line to turn around loop
#
# beamparams=[betax,alphax,emitnx,betay,alphay,emitny,sigmaz,charge,
#             uncespread,echirp,energy,nslice,nmacro,nsigmabunch,nsigmawake]
# units=[m,rad,m*rad,m,rad,m*rad,m,C,1,1/m,eV,1,1,1,1]
#
# where necessary units were converted to placet units in main.tcl

proc lattice_dump_and_match_ltl_to_tal {bparray} {
upvar $bparray beamparams

set usesixdim 1
set numthinlenses 100
set quad_synrad 0

set refenergy $beamparams(meanenergy)

set lquadm 0.3
set lquadx 0.36

set kqm1 0.009713020617
set kqm2 -0.01666831502
set kqm3 0.02506010184
set kqm4 0.0817375657
set kqm5 -0.1208106861
set kqm6 0.5742405616

set ldm1  5.0
set ldm2 10.0
set ldm3 10.0
set ldm4 13.0
set ldm5 65.0

SetReferenceEnergy $refenergy

Girder
Quadrupole -synrad $quad_synrad -length [expr $lquadx/2.0] -strength [expr $kqm1*$lquadx/2.0*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ldm1 -six_dim $usesixdim
Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm2*$lquadm*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ldm2 -six_dim $usesixdim
Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm3*$lquadm*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ldm3 -six_dim $usesixdim
Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm4*$lquadm*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ldm4 -six_dim $usesixdim
Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm5*$lquadm*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ldm5 -six_dim $usesixdim
Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm6*$lquadm*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
}
