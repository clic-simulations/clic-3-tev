#
# Central arc lattice
#
# beamparams=[betax,alphax,emitnx,betay,alphay,emitny,sigmaz,charge,
#             uncespread,echirp,energy,nslice,nmacro,nsigmabunch,nsigmawake]
# units=[m,rad,m*rad,m,rad,m*rad,m,C,1,1/m,eV,1,1,1,1]
# where necessary units were converted to placet units in main.tcl

proc lattice_central_arc {bparray} {
upvar $bparray beamparams

set usesynrad $beamparams(useisr)
set usesixdim 1
set numthinlenses 100
set quad_synrad 0

set pi 3.141592653589793
set c 299792458
set q0 1.6021765e-19 
set eps0 [expr 1/(4e-7*$pi*$c*$c)]

set refenergy $beamparams(meanenergy)

#puts $refenergy
set lbend 2.0
set lquad 0.3
set lsext 0.2

# bend to the left
set msign -1.0
set thetaa [expr $msign*1.2/180*$pi]
set rhoa [expr $lbend/(2*sin($thetaa/2))]
set larca [expr $thetaa*$rhoa]

set thetab [expr $msign*1.2/180*$pi]
set rhob [expr $lbend/(2*sin($thetab/2))]
set larcb [expr $thetab*$rhob]

set ld01 1.15
set ld02 2.99
set ld03 1.57
set ld04 1.94
set ld05 1.94
set ld06 0.30
set ld07 $ld05
set ld08 $ld04
set ld09 $ld03
set ld10 $ld02
set ld11 $ld01

set ld03a [expr $ld03-0.3]
set ld03b 0.1
set ld09a 0.1
set ld09b [expr $ld09-0.3]

set ld02a [expr $ld02-0.3]
set ld02b 0.1
set ld10a 0.1
set ld10b [expr $ld10-0.3]

set kq01 1.291951194
set kq02 -1.212050561
set kq03 1.733959844
set kq04 -0.3453705052
set kq05 $kq04
set kq06 $kq03
set kq07 $kq02
set kq08 $kq01

set ks01 [expr $msign*28.81111869]
set ks02 [expr $msign*28.61111867]
set ks03 [expr $msign*-39.72965294]
set ks04 [expr $msign*-36.72965291]

SetReferenceEnergy $refenergy

for {set cell 1} {$cell<=30} {incr cell} {
    Girder

    if {$cell==1} {
    } else {
        Quadrupole -synrad $quad_synrad -length [expr $lquad/2.0] -strength [expr $kq01*$lquad/2.0*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
    }
    Drift -length $ld01 -six_dim $usesixdim
    Sbend -length $larca -angle $thetaa -synrad $usesynrad -six_dim $usesixdim -thin_lens $numthinlenses -e0 $refenergy -E1 [expr $thetaa/2.0] -E2 [expr $thetaa/2.0]
    set g0 [expr $refenergy/0.000510999]
    set de [expr 1/(6*$pi*$eps0)*$q0*$q0*$c*$g0*$g0*$g0*$g0/($rhoa*$rhoa)*$larca/$c]
    set refenergy [expr $refenergy-$de/$q0/1e9*$usesynrad]
    SetReferenceEnergy $refenergy
    Drift -length $ld02a -six_dim $usesixdim
    Multipole -type 3 -length $lsext -e0 $refenergy -strength [expr $ks03*$refenergy*$lsext] -thin_lens $numthinlenses -six_dim $usesixdim
    Drift -length $ld02b -six_dim $usesixdim
    Quadrupole -synrad $quad_synrad -length $lquad -strength [expr $kq02*$lquad*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
    Drift -length $ld03a -six_dim $usesixdim
    Multipole -type 3 -length $lsext -e0 $refenergy -strength [expr $ks01*$refenergy*$lsext] -thin_lens $numthinlenses -six_dim $usesixdim
    Drift -length $ld03b -six_dim $usesixdim
    Quadrupole -synrad $quad_synrad -length $lquad -strength [expr $kq03*$lquad*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
    Drift -length $ld04 -six_dim $usesixdim
    Quadrupole -synrad $quad_synrad -length $lquad -strength [expr $kq04*$lquad*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
    Drift -length $ld05 -six_dim $usesixdim
    Sbend -length $larcb -angle $thetab -synrad $usesynrad -six_dim $usesixdim -thin_lens $numthinlenses -e0 $refenergy -E1 [expr $thetab/2.0] -E2 [expr $thetab/2.0]
    set g0 [expr $refenergy/0.000510999]
    set de [expr 1/(6*$pi*$eps0)*$q0*$q0*$c*$g0*$g0*$g0*$g0/($rhob*$rhob)*$larcb/$c]
    set refenergy [expr $refenergy-$de/$q0/1e9*$usesynrad]
    SetReferenceEnergy $refenergy
    Drift -length $ld06 -six_dim $usesixdim
    Sbend -length $larcb -angle $thetab -synrad $usesynrad -six_dim $usesixdim -thin_lens $numthinlenses -e0 $refenergy -E1 [expr $thetab/2.0] -E2 [expr $thetab/2.0]
    set g0 [expr $refenergy/0.000510999]
    set de [expr 1/(6*$pi*$eps0)*$q0*$q0*$c*$g0*$g0*$g0*$g0/($rhob*$rhob)*$larcb/$c]
    set refenergy [expr $refenergy-$de/$q0/1e9*$usesynrad]
    SetReferenceEnergy $refenergy
    Drift -length $ld06 -six_dim $usesixdim
    Sbend -length $larcb -angle $thetab -synrad $usesynrad -six_dim $usesixdim -thin_lens $numthinlenses -e0 $refenergy -E1 [expr $thetab/2.0] -E2 [expr $thetab/2.0]
    set g0 [expr $refenergy/0.000510999]
    set de [expr 1/(6*$pi*$eps0)*$q0*$q0*$c*$g0*$g0*$g0*$g0/($rhob*$rhob)*$larcb/$c]
    set refenergy [expr $refenergy-$de/$q0/1e9*$usesynrad]
    SetReferenceEnergy $refenergy
    Drift -length $ld07 -six_dim $usesixdim
    Quadrupole -synrad $quad_synrad -length $lquad -strength [expr $kq05*$lquad*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
    Drift -length $ld08 -six_dim $usesixdim
    Quadrupole -synrad $quad_synrad -length $lquad -strength [expr $kq06*$lquad*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
    Drift -length $ld09a -six_dim $usesixdim
    Multipole -type 3 -length $lsext -e0 $refenergy -strength [expr $ks02*$refenergy*$lsext] -thin_lens $numthinlenses -six_dim $usesixdim
    Drift -length $ld09b -six_dim $usesixdim
    Quadrupole -synrad $quad_synrad -length $lquad -strength [expr $kq07*$lquad*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
    Drift -length $ld10a -six_dim $usesixdim
    Multipole -type 3 -length $lsext -e0 $refenergy -strength [expr $ks04*$refenergy*$lsext] -thin_lens $numthinlenses -six_dim $usesixdim
    Drift -length $ld10b -six_dim $usesixdim
    Sbend -length $larca -angle $thetaa -synrad $usesynrad -six_dim $usesixdim -thin_lens $numthinlenses -e0 $refenergy -E1 [expr $thetaa/2.0] -E2 [expr $thetaa/2.0]
    set g0 [expr $refenergy/0.000510999]
    set de [expr 1/(6*$pi*$eps0)*$q0*$q0*$c*$g0*$g0*$g0*$g0/($rhoa*$rhoa)*$larca/$c]
    set refenergy [expr $refenergy-$de/$q0/1e9*$usesynrad]
    SetReferenceEnergy $refenergy
    Drift -length $ld11 -six_dim $usesixdim
    
    if {$cell==30} {
    } else {
        Quadrupole -synrad $quad_synrad -length [expr $lquad/2.0] -strength [expr $kq08*$lquad/2.0*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
    }
    
}

# single arc for dog-leg
set msign -1.0
set thetaa [expr $msign*1.2/180*$pi]
set rhoa [expr $lbend/(2*sin($thetaa/2))]
set larca [expr $thetaa*$rhoa]

set thetab [expr $msign*1.2/180*$pi]
set rhob [expr $lbend/(2*sin($thetab/2))]
set larcb [expr $thetab*$rhob]

set ld01 1.15
set ld02 2.99
set ld03 1.57
set ld04 1.94
set ld05 1.94
set ld06 0.30
set ld07 $ld05
set ld08 $ld04
set ld09 $ld03
set ld10 $ld02
set ld11 $ld01

set ld03a [expr $ld03-0.3]
set ld03b 0.1
set ld09a 0.1
set ld09b [expr $ld09-0.3]

set ld02a [expr $ld02-0.3]
set ld02b 0.1
set ld10a 0.1
set ld10b [expr $ld10-0.3]

set kq01 1.291951194
set kq02 -1.212050561
set kq03 1.733959844
set kq04 -0.3453705052
set kq05 $kq04
set kq06 $kq03
set kq07 $kq02
set kq08 $kq01

set ks01 [expr $msign*33.2]
set ks02 [expr $msign*33.0]
set ks03 [expr $msign*-43.6]
set ks04 [expr $msign*-40.6]

SetReferenceEnergy $refenergy

Girder
Quadrupole -synrad $quad_synrad -length [expr $lquad] -strength [expr $kq01*$lquad*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ld01 -six_dim $usesixdim
Sbend -length $larca -angle $thetaa -synrad $usesynrad -six_dim $usesixdim -thin_lens $numthinlenses -e0 $refenergy -E1 [expr $thetaa/2.0] -E2 [expr $thetaa/2.0]
set g0 [expr $refenergy/0.000510999]
set de [expr 1/(6*$pi*$eps0)*$q0*$q0*$c*$g0*$g0*$g0*$g0/($rhoa*$rhoa)*$larca/$c]
set refenergy [expr $refenergy-$de/$q0/1e9*$usesynrad]
SetReferenceEnergy $refenergy
Drift -length $ld02a -six_dim $usesixdim
Multipole -type 3 -length $lsext -e0 $refenergy -strength [expr $ks03*$refenergy*$lsext] -thin_lens $numthinlenses -six_dim $usesixdim
Drift -length $ld02b -six_dim $usesixdim
Quadrupole -synrad $quad_synrad -length $lquad -strength [expr $kq02*$lquad*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ld03a -six_dim $usesixdim
Multipole -type 3 -length $lsext -e0 $refenergy -strength [expr $ks01*$refenergy*$lsext] -thin_lens $numthinlenses -six_dim $usesixdim
Drift -length $ld03b -six_dim $usesixdim
Quadrupole -synrad $quad_synrad -length $lquad -strength [expr $kq03*$lquad*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ld04 -six_dim $usesixdim
Quadrupole -synrad $quad_synrad -length $lquad -strength [expr $kq04*$lquad*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ld05 -six_dim $usesixdim
Sbend -length $larcb -angle $thetab -synrad $usesynrad -six_dim $usesixdim -thin_lens $numthinlenses -e0 $refenergy -E1 [expr $thetab/2.0] -E2 [expr $thetab/2.0]
set g0 [expr $refenergy/0.000510999]
set de [expr 1/(6*$pi*$eps0)*$q0*$q0*$c*$g0*$g0*$g0*$g0/($rhob*$rhob)*$larcb/$c]
set refenergy [expr $refenergy-$de/$q0/1e9*$usesynrad]
SetReferenceEnergy $refenergy
Drift -length $ld06 -six_dim $usesixdim
Sbend -length $larcb -angle $thetab -synrad $usesynrad -six_dim $usesixdim -thin_lens $numthinlenses -e0 $refenergy -E1 [expr $thetab/2.0] -E2 [expr $thetab/2.0]
set g0 [expr $refenergy/0.000510999]
set de [expr 1/(6*$pi*$eps0)*$q0*$q0*$c*$g0*$g0*$g0*$g0/($rhob*$rhob)*$larcb/$c]
set refenergy [expr $refenergy-$de/$q0/1e9*$usesynrad]
SetReferenceEnergy $refenergy
Drift -length $ld06 -six_dim $usesixdim
Sbend -length $larcb -angle $thetab -synrad $usesynrad -six_dim $usesixdim -thin_lens $numthinlenses -e0 $refenergy -E1 [expr $thetab/2.0] -E2 [expr $thetab/2.0]
set g0 [expr $refenergy/0.000510999]
set de [expr 1/(6*$pi*$eps0)*$q0*$q0*$c*$g0*$g0*$g0*$g0/($rhob*$rhob)*$larcb/$c]
set refenergy [expr $refenergy-$de/$q0/1e9*$usesynrad]
SetReferenceEnergy $refenergy
Drift -length $ld07 -six_dim $usesixdim
Quadrupole -synrad $quad_synrad -length $lquad -strength [expr $kq05*$lquad*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ld08 -six_dim $usesixdim
Quadrupole -synrad $quad_synrad -length $lquad -strength [expr $kq06*$lquad*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ld09a -six_dim $usesixdim
Multipole -type 3 -length $lsext -e0 $refenergy -strength [expr $ks02*$refenergy*$lsext] -thin_lens $numthinlenses -six_dim $usesixdim
Drift -length $ld09b -six_dim $usesixdim
Quadrupole -synrad $quad_synrad -length $lquad -strength [expr $kq07*$lquad*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ld10a -six_dim $usesixdim
Multipole -type 3 -length $lsext -e0 $refenergy -strength [expr $ks04*$refenergy*$lsext] -thin_lens $numthinlenses -six_dim $usesixdim
Drift -length $ld10b -six_dim $usesixdim
Sbend -length $larca -angle $thetaa -synrad $usesynrad -six_dim $usesixdim -thin_lens $numthinlenses -e0 $refenergy -E1 [expr $thetaa/2.0] -E2 [expr $thetaa/2.0]
set g0 [expr $refenergy/0.000510999]
set de [expr 1/(6*$pi*$eps0)*$q0*$q0*$c*$g0*$g0*$g0*$g0/($rhoa*$rhoa)*$larca/$c]
set refenergy [expr $refenergy-$de/$q0/1e9*$usesynrad]
SetReferenceEnergy $refenergy
Drift -length $ld11 -six_dim $usesixdim

set beamparams(meanenergy) $refenergy
puts "Setting beamparams(meanenergy)=$beamparams(meanenergy)."
}
