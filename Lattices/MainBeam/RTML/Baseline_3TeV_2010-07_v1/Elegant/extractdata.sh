#!/bin/bash
#
echo "twi:"
sdds2plaindata rtml.twi rtml.twi.txt -outputMode=ascii -separator=' ' -col=s -col=betax -col=alphax -col=psix -col=betay -col=alphay -col=ElementName -noRowCount
echo "mat:"
sdds2plaindata rtml.mat rtml.mat.txt -outputMode=ascii -separator=' ' -col=s -col=R16 -col=R52 -col=R56 -col=R55 -col=R36 -col=T566 -noRowCount
#sdds2plaindata rtml.mat rtml.mat.asc -outputMode=ascii -separator=' ' -col=s -col=ElementName -col=ElementOccurence -col=ElementType -noRowCount
echo "s:"
sdds2plaindata rtml.s rtml.s.emit -outputMode=ascii -separator=' ' -col=s -col=enx -col=ecnx -col=eny -col=ecny -noRowCount
echo "s 2:"
sdds2plaindata rtml.s rtml.s.txt2 -outputMode=ascii -separator=' ' -col=s -col=Ss -col=Sdelta -noRowCount
echo "cen process:"
sddsprocess rtml.cen rtml.cen.new -def=col,pCentralGeV,"pCentral 0.00051099891 *" -def=col,CdeltaGeV,"Cdelta pCentral * 0.00051099891 *" -def=col,Cds,"s Cs -"
echo "cen:"
sdds2plaindata rtml.cen.new rtml.cen.txt -outputMode=ascii -separator=' ' -col=s -col=Cs -col=Cds -col=Cdelta -col=CdeltaGeV -col=pCentralGeV -noRowCount
#
echo "flr:"
sdds2plaindata rtml.flr rtml.flr.txt -outputMode=ascii -separator=' ' -noRowCount -col=s -col=X -col=Y -col=Z -col=theta -col=phi -col=psi
#
echo "fin:"
sdds2plaindata rtml.fin rtml.fin.txt -outputMode=ascii -separator=' ' -noRowCount -par=Cx -par=Sx -par=Cxp -par=Sxp -par=Cy -par=Sy -par=Cyp -par=Syp -par=Cs -par=Ss -par=Cdelta -par=Sdelta
#
echo "out process:"
sddsprocess rtml.out rtml.out.new -process=t,average,avgt -def=col,s,"t avgt - 299792458 * 1 *" -def=col,de,"p pCentral / 1 -"
echo "out:"
sdds2plaindata rtml.out.new rtml.out.txt -outputMode=ascii -separator=' ' -noRowCount -col=s -col=de -col=x -col=xp -col=y -col=yp
#
echo "bun process:"
sddsprocess rtml.bun rtml.bun.new -process=t,average,avgt -def=col,s,"t avgt - 299792458 * 1 *" -def=col,de,"p pCentral / 1 -"
echo "bun:"
sdds2plaindata rtml.bun.new rtml.bun.txt -outputMode=ascii -separator=' ' -noRowCount -col=s -col=de -col=x -col=xp -col=y -col=yp
#
#
#sddsprocess rtml.mat rtml_new.mat -define=param,Sdelta0,1.1e-3 -define=param,Ss0,1300e-6,units=m -define=col,Ss,"Sdelta0 R56 * sqr Ss0 R55 * sqr + sqrt",units=m
#sddsplot -col=s,Ss rtml.s rtml_new.mat -graph=line,vary &
