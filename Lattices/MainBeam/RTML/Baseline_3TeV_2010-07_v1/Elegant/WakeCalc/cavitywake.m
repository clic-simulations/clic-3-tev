%
function cavitywake(wfilename,nslices,a,g,l)
    sstep=l/(nslices-1);
    
    wfid=fopen(wfilename,"wt");
    
    for s=0.0:sstep:l
        wl=w_long(s,a,g,l)*l;
        wt=w_trans(s,a,g,l)*l;
        wx=wt;
        wy=wt;
        fprintf(wfid,"%22.18e %22.18e %22.18e %22.18e\n",s/299792458,wl,wx,wy);
    end;
    fclose(wfid);
endfunction;


%
% longitudinal wakefield
% return value is in V/Cm
%
function wl=w_long(s,a,g,l)
    tmp=g/l;
    alpha=1.0-0.4648*sqrt(tmp)-(1.0-2.0*0.4648)*tmp;
    s0=g/8.0*(a/(l*alpha))^2;
    wl=377.0*299792458/(pi*a*a)*exp(-sqrt(s/s0));
endfunction;

%
% transverse wakefield
% return value is in V/Cm^2
%
function wt=w_trans(s,a,g,l)
    tmp=g/l;
    alpha=1.0-0.4648*sqrt(tmp)-(1.0-2.0*0.4648)*tmp;
    s0=g/8.0*(a/(l*alpha))^2;
    wt=4.0*377.0*299792458*s0/(pi*a*a*a*a)*(1.0-(1.0+sqrt(s/s0))*exp(-sqrt(s/s0)));
endfunction;

