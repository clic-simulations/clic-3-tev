%	
% convert output of Elegant to CSRTrack fmt3



% --------------------------------------------------------------------
function convert_elegant_to_fmt3(inputbasename,outputbasename)

global elegantdata;
global qtot;
global npart;
global g0;

mkdir("csrtdata");
cd("csrtdata");
fidlog=fopen("log.txt","wt");


outputfile=[outputbasename,"_0001.fmt3"]
inputfile=["../",inputbasename,"_01.dat"]
extractparams(inputfile);
qtot=0.65e-9
npart
g0
extractdata(inputfile);
conv_to_fmt3(outputfile,elegantdata);
fprintf(fidlog,"  monitor=%s\n",outputfile);

outputfile=[outputbasename,"_0002.fmt3"]
inputfile=["../",inputbasename,"_02.dat"]
extractparams(inputfile);
qtot=0.65e-9
npart
g0
extractdata(inputfile);
conv_to_fmt3(outputfile,elegantdata);
fprintf(fidlog,"  monitor=%s\n",outputfile);

outputfile=[outputbasename,"_0003.fmt3"]
inputfile=["../",inputbasename,"_03.dat"]
extractparams(inputfile);
qtot=0.65e-9
npart
g0
extractdata(inputfile);
conv_to_fmt3(outputfile,elegantdata);
fprintf(fidlog,"  monitor=%s\n",outputfile);

outputfile=[outputbasename,"_0004.fmt3"]
inputfile=["../",inputbasename,"_04.dat"]
extractparams(inputfile);
qtot=0.65e-9
npart
g0
extractdata(inputfile);
conv_to_fmt3(outputfile,elegantdata);
fprintf(fidlog,"  monitor=%s\n",outputfile);

outputfile=[outputbasename,"_0005.fmt3"]
inputfile=["../",inputbasename,"_05.dat"]
extractparams(inputfile);
qtot=0.65e-9
npart
g0
extractdata(inputfile);
conv_to_fmt3(outputfile,elegantdata);
fprintf(fidlog,"  monitor=%s\n",outputfile);

outputfile=[outputbasename,"_0006.fmt3"]
inputfile=["../",inputbasename,"_06.dat"]
extractparams(inputfile);
qtot=0.65e-9
npart
g0
extractdata(inputfile);
conv_to_fmt3(outputfile,elegantdata);
fprintf(fidlog,"  monitor=%s\n",outputfile);

outputfile=[outputbasename,"_0007.fmt3"]
inputfile=["../",inputbasename,"_07.dat"]
extractparams(inputfile);
qtot=0.65e-9
npart
g0
extractdata(inputfile);
conv_to_fmt3(outputfile,elegantdata);
fprintf(fidlog,"  monitor=%s\n",outputfile);

outputfile=[outputbasename,"_0008.fmt3"]
inputfile=["../",inputbasename,"_08.dat"]
extractparams(inputfile);
qtot=0.65e-9
npart
g0
extractdata(inputfile);
conv_to_fmt3(outputfile,elegantdata);
fprintf(fidlog,"  monitor=%s\n",outputfile);

outputfile=[outputbasename,"_0009.fmt3"]
inputfile=["../",inputbasename,"_09.dat"]
extractparams(inputfile);
qtot=0.65e-9
npart
g0
extractdata(inputfile);
conv_to_fmt3(outputfile,elegantdata);
fprintf(fidlog,"  monitor=%s\n",outputfile);

outputfile=[outputbasename,"_0010.fmt3"]
inputfile=["../",inputbasename,"_10.dat"]
extractparams(inputfile);
qtot=0.65e-9
npart
g0
extractdata(inputfile);
conv_to_fmt3(outputfile,elegantdata);
fprintf(fidlog,"  monitor=%s\n",outputfile);

outputfile=[outputbasename,"_0011.fmt3"]
inputfile=["../",inputbasename,"_11.dat"]
extractparams(inputfile);
qtot=0.65e-9
npart
g0
extractdata(inputfile);
conv_to_fmt3(outputfile,elegantdata);
fprintf(fidlog,"  monitor=%s\n",outputfile);

outputfile=[outputbasename,"_0012.fmt3"]
inputfile=["../",inputbasename,"_12.dat"]
extractparams(inputfile);
qtot=0.65e-9
npart
g0
extractdata(inputfile);
conv_to_fmt3(outputfile,elegantdata);
fprintf(fidlog,"  monitor=%s\n",outputfile);

outputfile=[outputbasename,"_0013.fmt3"]
inputfile=["../",inputbasename,"_13.dat"]
extractparams(inputfile);
qtot=0.65e-9
npart
g0
extractdata(inputfile);
conv_to_fmt3(outputfile,elegantdata);
fprintf(fidlog,"  monitor=%s\n",outputfile);

outputfile=[outputbasename,"_0014.fmt3"]
inputfile=["../",inputbasename,"_14.dat"]
extractparams(inputfile);
qtot=0.65e-9
npart
g0
extractdata(inputfile);
conv_to_fmt3(outputfile,elegantdata);
fprintf(fidlog,"  monitor=%s\n",outputfile);

outputfile=[outputbasename,"_0015.fmt3"]
inputfile=["../",inputbasename,"_15.dat"]
extractparams(inputfile);
qtot=0.65e-9
npart
g0
extractdata(inputfile);
conv_to_fmt3(outputfile,elegantdata);
fprintf(fidlog,"  monitor=%s\n",outputfile);

outputfile=[outputbasename,"_0016.fmt3"]
inputfile=["../",inputbasename,"_16.dat"]
extractparams(inputfile);
qtot=0.65e-9
npart
g0
extractdata(inputfile);
conv_to_fmt3(outputfile,elegantdata);
fprintf(fidlog,"  monitor=%s\n",outputfile);


fclose(fidlog);
cd("../");
%done

endfunction;

% --------------------------------------------------------------------
% --------------------------------------------------------------------

function extractparams(inputname)

global qtot;
global npart;
global g0;

system(["sdds2plaindata ",inputname," tmp1234.dat -outputMode=ascii -para=Charge -para=Particles -para=pCentral"]);
data=load("tmp1234.dat");
qtot=data(1);
npart=data(2);
g0=data(3);
system("rm tmp1234.dat");

endfunction;


% --------------------------------------------------------------------
% --------------------------------------------------------------------

function extractdata(datafilename)

global elegantdata;
tmpdata="tmpdata.dat";

syscmd=["sdds2plaindata ",datafilename," ",tmpdata," -outputMode=ascii -noRowCount -separator=' ' -col=x -col=xp -col=y -col=yp -col=t -col=p"];
system(syscmd);
elegantdata=load(tmpdata);
system(["rm ",tmpdata]);

endfunction;


% --------------------------------------------------------------------
% --------------------------------------------------------------------

function conv_to_fmt3(outfile,data)

global qtot;
global npart;
global g0;

% first row : t,g0,0,0,0,0,0,ss,sx,sy
% second row: ref x,ref xp,ref y,ref yp,ref s,ref de,ref q,ref ws,ref wx,ref wy
% other rows: x,xp,y,yp,s,de,q,ws,wx,wy
%
% Attention: my x,y != CSRTrack x,y, but my x,y, = CSRTrack h,v

ld=length(data);
if (ld~=npart)
    disp "length(data) != npart. Stopping."
    return;
end;

c_x =data(:,1);
c_xp=data(:,2);
c_y =data(:,3);
c_yp=data(:,4);
c_s =data(:,5);
c_de=data(:,6);
c_q =ones(ld,1)*qtot/npart;
c_ws=zeros(ld,1);
c_wx=zeros(ld,1);
c_wy=zeros(ld,1);

mean_st=sum(c_s)/npart;
c_s=-(c_s-mean_st)*299792458;
mean_s=sum(c_s)/npart;
mean_de=sum(c_de)/npart;
c_de=(c_de-g0)/g0;
mean_de=sum(c_de)/npart;

mean_x=sum(c_x)/npart;
mean_xp=sum(c_xp)/npart;
mean_y=sum(c_y)/npart;
mean_yp=sum(c_yp)/npart;

c=[c_x,c_xp,c_y,c_yp,c_s,c_de,c_q,c_ws,c_wx,c_wy];

r1=[mean_st*299792458,g0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0];
r2=[-mean_x,-mean_xp,-mean_y,-mean_yp,-mean_s,-mean_de,0.0,0.0,0.0,0.0];

fmt3data=[r1;r2;c];

fid=fopen(outfile,"wt");
fprintf(fid,"%18.10e %18.10e %18.10e %18.10e %18.10e %18.10e %18.10e %3.1f %3.1f %3.1f\n",fmt3data.');
fclose(fid);

endfunction;


% --------------------------------------------------------------------
% --------------------------------------------------------------------

