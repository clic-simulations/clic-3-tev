#!/bin/bash
#
simdirname="simrun_e+_e_wakes=1_csr=0_isr=1"
if test -f ./$simdirname/LSF_nodes_* ; then
  runhost=$( head -n 1 ./$simdirname/LSF_nodes_* );
 else
  runhost="lxclic01";
fi
echo $runhost
ssh $runhost "tail -n 30 ~/scratch0/Work/RTML_WiP/Elegant/$simdirname/Pelegant.out"
