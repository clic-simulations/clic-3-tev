# CLIC Main Beam RTML

##### If No wakefield file, run the create_wake.tcl script
#source create_wake.tcl
######

set tcl_precision 15
ParallelThreads -num 4

proc FF1 {} {}
proc FF2 {} {}

set latticedir ./Lattices_e+
set scriptdir ./Scripts
set paramsdir ./Parameters

source $scriptdir/placet_units.tcl
source $paramsdir/initial_beam_parameters_e+.tcl
source $paramsdir/rf_parameters_e+.tcl
source $scriptdir/lattice_fun.tcl
source $scriptdir/beamsetup.tcl
source $scriptdir/cavitywakesetup.tcl
source $scriptdir/octave_fun.tcl
source $scriptdir/setParam.tcl

if {$beamparams(usewakefields)==1} {
   source rf_parameteres_ww.tcl
}

create_particles_file particles.in beamparams
create_zero_wakes_file zero_wake.dat beamparams rfparamsbc1 particles.in

BeamlineNew
source $scriptdir/beamline_and_wake.tcl
BeamlineSet -name rtml

FirstOrder 1

make_particle_beam beam1 beamparams particles.in zero_wake.dat

source $scriptdir/setCSR.m

Octave {
    [E,B] = placet_test_no_correction("rtml", "beam1", "None", 1);
 
    save -text Emit-bc2.dat E;
    save -text beam-bc2.dat B;
}
