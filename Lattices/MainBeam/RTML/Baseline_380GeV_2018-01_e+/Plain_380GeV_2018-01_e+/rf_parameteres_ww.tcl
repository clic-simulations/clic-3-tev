set rfparamsbooster(gradientww)   1.489874671511e+07
set rfparamsbc1(gradientww)   1.327180977838e+07
set rfparamsbc2(gradientww)   7.184218893705e+07
set energy_wake_bc1  -3.528469883725e-05
set energy_wake_booster  -5.772051969600e-05
set energy_wake_bc2  -7.425861716771e-05
