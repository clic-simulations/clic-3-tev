In the file 'elements_table.txt' the last column, called Strength, contains:

* for quadrupoles: the integrated strength in T
* for sector bends (sbends): the integrated strength in Tm
* for structures (cavities): the integrated voltage in MV
* for kickers (dipole correctors) the max strength given in integrated transverse voltage in Tm
* for sextupoles the integrated strength in T/m
* for solenoids the max field in T (the solenoids will normally operate at a lower field - that number is the field required to achieve a 45 degrees spin rotation in just one solenoid.)

