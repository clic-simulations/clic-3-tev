#
# BC1 chicane lattice
#
# beamparams=[betax,alphax,emitnx,betay,alphay,emitny,sigmaz,charge,
#             uncespread,echirp,energy,nslice,nmacro,nsigmabunch,nsigmawake]
# units=[m,rad,m*rad,m,rad,m*rad,m,C,1,1/m,eV,1,1,1,1]
# where necessary units were converted to placet units in main.tcl

proc lattice_bc1_chicane {bparray} {
upvar $bparray beamparams

set usecsr 0
set usesynrad $beamparams(useisr)
set usesixdim 1
set numthinlenses 100

set pi 3.141592653589793

set theta [expr 4.416/180*$pi]
set lbend 1.5
set rho [expr $lbend/sin($theta)]
set lbendarc [expr $theta*$rho]
set refenergy $beamparams(meanenergy)

set c 299792458
set q0 1.6021765e-19
set eps0 [expr 1/(4e-7*$pi*$c*$c)]

set d12 [expr 11.5/cos($theta)]
set d23 1.0
set d34 [expr $d12]

SetReferenceEnergy $refenergy

Girder
Sbend -length $lbendarc -angle $theta -csr $usecsr -synrad $usesynrad -six_dim $usesixdim -thin_lens $numthinlenses -e0 $refenergy -E1 0.0 -E2 $theta
set g0 [expr $refenergy/0.000510999]
set de [expr 1/(6*$pi*$eps0)*$q0*$q0*$c*$g0*$g0*$g0*$g0/($rho*$rho)*$lbendarc/$c]
set refenergy [expr $refenergy-$de/$q0/1e9*$usesynrad]
SetReferenceEnergy $refenergy
Drift -length $d12 -six_dim $usesixdim
Sbend -length $lbendarc -angle -$theta -csr $usecsr -synrad $usesynrad -six_dim $usesixdim -thin_lens $numthinlenses -e0 $refenergy -E1 -$theta -E2 0.0
set g0 [expr $refenergy/0.000510999]
set de [expr 1/(6*$pi*$eps0)*$q0*$q0*$c*$g0*$g0*$g0*$g0/($rho*$rho)*$lbendarc/$c]
set refenergy [expr $refenergy-$de/$q0/1e9*$usesynrad]
SetReferenceEnergy $refenergy
Drift -length $d23 -six_dim $usesixdim
Sbend -length $lbendarc -angle -$theta -csr $usecsr -synrad $usesynrad -six_dim $usesixdim -thin_lens $numthinlenses -e0 $refenergy -E1 0.0 -E2 -$theta
set g0 [expr $refenergy/0.000510999]
set de [expr 1/(6*$pi*$eps0)*$q0*$q0*$c*$g0*$g0*$g0*$g0/($rho*$rho)*$lbendarc/$c]
set refenergy [expr $refenergy-$de/$q0/1e9*$usesynrad]
SetReferenceEnergy $refenergy
Drift -length $d34 -six_dim $usesixdim
Sbend -length $lbendarc -angle $theta -csr $usecsr -synrad $usesynrad -six_dim $usesixdim -thin_lens $numthinlenses -e0 $refenergy -E1 $theta -E2 0.0
set g0 [expr $refenergy/0.000510999]
set de [expr 1/(6*$pi*$eps0)*$q0*$q0*$c*$g0*$g0*$g0*$g0/($rho*$rho)*$lbendarc/$c]
set refenergy [expr $refenergy-$de/$q0/1e9*$usesynrad]
SetReferenceEnergy $refenergy

set beamparams(meanenergy) $refenergy
puts "Setting beamparams(meanenergy)=$beamparams(meanenergy)."
}
