#!/bin/bash
#
beam="$1"
wakes="$2"
isr="$3"

if [ "$wakes" != "0" ] ; then
wakes="1"
fi
if [ "$isr" != "0" ] ; then
isr="1"
fi
if [ "$beam" != "e+" ] ; then
beam="e-"
fi

echo "Job started: " $( date )
simdirname=./simrun_${beam}
rm -rf $simdirname
mkdir $simdirname
rsync -aC ../Lattices_${beam} $simdirname
rsync -aC ../Lattices_common/* $simdirname/Lattices_${beam}
rsync -aC ../Parameters $simdirname
rsync -aC ../Scripts $simdirname
cp ./main_${beam}.tcl $simdirname
cp ./tracking.tcl $simdirname
cd $simdirname

cat << EOF > inputparams.tcl
set beamparams(useisr) $isr
set beamparams(usewakefields) $wakes
EOF

placet main_${beam}.tcl 2> placet.err | grep --line-buffered -v "too large z" - | tee placet.out
echo "Job finished: " $( date )
