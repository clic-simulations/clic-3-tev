#
# the big tracking procedure
#
proc do_tracking {beamlinename beamname bparray stepnum} {
    global zoffset
    global placetunits
    upvar $bparray beamparams
    
    puts ""
    puts "Perform tracking step $stepnum using beamline $beamlinename and beam $beamname"
    puts ""
    
    set betax0 $beamparams(betax)
    set alphax0 $beamparams(alphax)
    set betay0 $beamparams(betay)
    set alphay0 $beamparams(alphay)
    set nbunches $beamparams(nbunches)
    set nmacro $beamparams(nmacro)
    set nslice $beamparams(nslice)
    
    BeamlineSaveFootprint -beamline $beamlinename -file "footprint_${stepnum}.dat"

    #
    # plot Twiss functions
    #
#     puts "Dumping Twiss data to file..."
#     set outname [format "twiss_step_%d.dat" $stepnum]
# #    TwissPlotStep -file $outname -beam $beamname -step 0.05
#     TwissPlot -file $outname -beam $beamname
#     puts "...Done!"

    #
    # dump energy spread along beam line
    #
    # puts "Dumping energy spread to file..."
    # set outname [format "e_spread_step_%d.dat" $stepnum]
    # EnergySpreadPlot -file $outname -beam $beamname
    # puts "...Done!"

    #
    # dump energy along beam line
    #
    # puts "Dumping energy to file..."
    # set outname [format "energy_step_%d.dat" $stepnum]
    # BeamEnergyPlot -file $outname -beam $beamname
    # puts "...Done!"


    
    #
    # dump AML file of lattice
    #
#    BeamlineSaveAML -beamline $beamlinename -file [format "aml_lattice_%d.aml" $stepnum]

    #
    # we do tracking, plotting, etc. in octave
    #
    Octave {
        format long;
        
%        persistent beamini;
%        persistent beaminio;

        beamlinename="$beamlinename";
        beamname="$beamname";

        if ($stepnum==1)
          beaminio=placet_get_beam(beamname);
          beamini=units_placet_to_std(beaminio);
          beamnormalini=transform_to_normal_coords(beamini);
        end;
        
        % track beam
        [emitt,beamfino]=placet_test_no_correction(beamlinename,beamname,"None");
        list_bpm=placet_get_number_list(beamlinename,"bpm");
        bpmx=placet_element_get_attribute(beamlinename,list_bpm,"reading_x");
        bpmy=placet_element_get_attribute(beamlinename,list_bpm,"reading_y");
        bpms=placet_element_get_attribute(beamlinename,list_bpm,"s");

        % convert from Placet units to more useful standard units
        % this is required for some calculations
        beamfin=units_placet_to_std(beamfino);

        % calculate new beam params for later use in TCL
        % this part is required for the correct connection to the next simulation step

        new_rmss=rms(beamfin(:,4),1);
        new_means=mean(beamfin(:,4),1);
        new_meanenergy=mean(beamfin(:,1),1);
        new_meanx=mean(beamfin(:,2),1);
        new_meanxp=mean(beamfin(:,5),1);
        new_meany=mean(beamfin(:,3),1);
        new_meanyp=mean(beamfin(:,6),1);
        new_gamma=new_meanenergy/(0.5109989*1e6);
        new_espread=rms(beamfin(:,1),1)/new_meanenergy;
        new_lemit=emitproj(beamfin(:,4),beamfin(:,1)-new_meanenergy,1);
        new_uncespread=new_lemit/(new_rmss*new_meanenergy);
        new_echirp=sqrt((new_espread^2-new_uncespread^2)/new_rmss^2);
        [new_betax,new_alphax]=twiss(beamfin(:,2),beamfin(:,5),1);
        new_emitnx=new_gamma*emitproj(beamfin(:,2),beamfin(:,5),1);
        [new_betay,new_alphay]=twiss(beamfin(:,3),beamfin(:,6),1);
        new_emitny=new_gamma*emitproj(beamfin(:,3),beamfin(:,6),1);
        
        Tcl_SetVar("new_rmss",new_rmss);
        Tcl_SetVar("new_means",new_means);
        Tcl_SetVar("new_meanenergy",new_meanenergy);
        Tcl_SetVar("new_meanx",new_meanx);
        Tcl_SetVar("new_meanxp",new_meanxp);
        Tcl_SetVar("new_meany",new_meany);
        Tcl_SetVar("new_meanyp",new_meanyp);
        Tcl_SetVar("new_uncespread",new_uncespread);
        Tcl_SetVar("new_echirp",new_echirp);
        Tcl_SetVar("new_betax",new_betax);
        Tcl_SetVar("new_alphax",new_alphax);
        Tcl_SetVar("new_emitnx",new_emitnx);
        Tcl_SetVar("new_betay",new_betay);
        Tcl_SetVar("new_alphay",new_alphay);
        Tcl_SetVar("new_emitny",new_emitny);
        
        
        % the rest of the octave part is just dumping some phase space data and beam statistics,
        % i.e. it is not required for proper functioning of the simulations
        
        % dump some beam statistics to stdout and a file
        print_mean_std(beamini,beamfin);
        if ($stepnum==4)
          save_statistics(beamini,beamfin,"statistics.dat");
          beamnormalfin=transform_to_normal_coords(beamfin);
          save_statistics(beamnormalini,beamnormalfin,"statistics_normal.dat");
        end;
        
        % save emittances along beamline
        fid=fopen("emit_${stepnum}.dat","w");
        for i=1:rows(emitt)
            fprintf(fid,"%20.12e %20.12e %20.12e %20.12e\n",emitt(i,1),emitt(i,2)*100,emitt(i,6)*100,emitt(i,4));
        end;
        fclose(fid);

        % save bpm data along beamline
        fid=fopen("bpm.dat.tmp","at");
        for i=1:columns(bpmx)
            fprintf(fid,"%20.12e %20.12e %20.12e\n",bpms(i)+$zoffset,bpmx(i),bpmy(i));
        end;
        fclose(fid);
    }

    # end of tracking part
    
    # define the offset of the longitudinal coordinate z
    array set blinfo [BeamlineInfo]
    set deltaz $blinfo(length)
    set zoffset [expr $zoffset + $deltaz]
    set fid [open zoffset.dat a]
    set stmp [expr $stepnum+1]
    puts $fid "$stmp   $zoffset"
    close $fid
    
    # set new beamparams
    set beamparams(sigmaz) [expr $new_rmss * $placetunits(xyz)]
    set beamparams(meanz) [expr $new_means * $placetunits(xyz)]
    set beamparams(meanenergy) [expr $new_meanenergy * $placetunits(energy)]
    set beamparams(meanx) [expr $new_meanx * $placetunits(xyz)]
    set beamparams(meanxp) [expr $new_meanxp * $placetunits(xpyp)]
    set beamparams(meany) [expr $new_meany * $placetunits(xyz)]
    set beamparams(meanyp) [expr $new_meanyp * $placetunits(xpyp)]
    set beamparams(startenergy) [expr $new_meanenergy * $placetunits(energy)]
    set beamparams(uncespread) $new_uncespread
    set beamparams(echirp) [expr $new_echirp/$placetunits(energy)]
    set beamparams(betax) $new_betax
    set beamparams(alphax) $new_alphax
    set beamparams(emitnx) [expr $new_emitnx * $placetunits(emittance)]
    set beamparams(betay) $new_betay
    set beamparams(alphay) $new_alphay
    set beamparams(emitny) [expr $new_emitny * $placetunits(emittance)]
    set beamparams(charge) $beamparams(charge)
}
