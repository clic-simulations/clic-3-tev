#
# matching of Damping Rings and BC1 RF
#
# beamparams=[betax,alphax,emitnx,betay,alphay,emitny,sigmaz,charge,
#             uncespread,echirp,energy,nslice,nmacro,nsigmabunch,nsigmawake]
# units=[m,rad,m*rad,m,rad,m*rad,m,C,1,1/m,eV,1,1,1,1]
#
# where necessary units were converted to placet units in main.tcl

proc lattice_match_dr_and_bc1 {bparray} {
upvar $bparray beamparams

set usesixdim 1
set numthinlenses 100

set quad_synrad 0
set e0 $beamparams(energy)

set lquadm 0.36

set kqm1 -0.00323172199
set kqm2 0.00507988736
set kqm3 -0.2397034835
set kqm4 0.2778479176

set ldm1 3.477518021
set ldm2 12.98510007
set ldm3 10.66482094
set ldm4 3.155495239
set ldm5 1.195470237


Girder
Drift -name BC1MATCHING -length 0.00000 -six_dim $usesixdim
Drift -name DM1 -length $ldm1 -six_dim $usesixdim
Quadrupole -name QM1 -synrad $quad_synrad -length $lquadm -strength [expr $kqm1*$lquadm*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -name DM2 -length $ldm2 -six_dim $usesixdim
Quadrupole -name QM2 -synrad $quad_synrad -length $lquadm -strength [expr $kqm2*$lquadm*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -name DM3 -length $ldm3 -six_dim $usesixdim
Quadrupole -name QM3 -synrad $quad_synrad -length $lquadm -strength [expr $kqm3*$lquadm*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -name DM4 -length $ldm4 -six_dim $usesixdim
Quadrupole -name QM4 -synrad $quad_synrad -length $lquadm -strength [expr $kqm4*$lquadm*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -name DM5 -length $ldm5 -six_dim $usesixdim
Drift -name BC1MATCHING -length 0.00000 -six_dim $usesixdim
}
