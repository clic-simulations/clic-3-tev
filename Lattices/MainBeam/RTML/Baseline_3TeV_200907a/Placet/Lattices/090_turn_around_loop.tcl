#
# Turn Around Loop lattice
#
# beamparams=[betax,alphax,emitnx,betay,alphay,emitny,sigmaz,charge,
#             uncespread,echirp,energy,nslice,nmacro,nsigmabunch,nsigmawake]
# units=[m,rad,m*rad,m,rad,m*rad,m,C,1,1/m,eV,1,1,1,1]
# where necessary units were converted to placet units in main.tcl

proc lattice_turn_around_loop {bparray} {
upvar $bparray beamparams

set usesynrad $beamparams(useisr)
set usesixdim 1
set numthinlenses 100
set e0 $beamparams(energyafterbooster)

set lbend 2.0
set lquad 0.5
set lsext 0.2
set lcorr 0.05

set pi 3.141592653589793

set theta [expr 1.5/180*$pi]
set rho [expr $lbend/(2*sin($theta/2))]
set larc [expr $theta*$rho]

set ld01 0.50
set ld02 0.55
set ld03 6.38
set ld04 1.17
set ld05 1.30
set ld06 0.50
set ld07 $ld05
set ld08 $ld04
set ld09 $ld03
set ld10 $ld02
set ld11 $ld01


set ld03a [expr $ld03-0.3]
set ld03b 0.1
set ld04a 0.1
set ld04b [expr $ld04-0.3]
set ld09a 0.1
set ld09b [expr $ld09-0.3]


set kq01 1.580449653  
set kq02 -0.7605336329
set kq03 0.9155124479 
set kq04 -0.4598785488
set kq05 $kq04
set kq06 $kq03
set kq07 $kq02
set kq08 $kq01

set kq08LQ0808  0.9096194233
set kq01RQ0101  0.9096194233

set ks01 [expr -22.0]
set ks02 [expr -22.0]

# left bending arc

for {set cell 1} {$cell<=8} {incr cell} {
    set cns [format "LC%02dSTART" $cell]
    set cne [format "LC%02dEND" $cell]
    
    set qn01 [format "LQ%02d01" $cell]
    set qn02 [format "LQ%02d02" $cell]
    set qn03 [format "LQ%02d03" $cell]
    set qn04 [format "LQ%02d04" $cell]
    set qn05 [format "LQ%02d05" $cell]
    set qn06 [format "LQ%02d06" $cell]
    set qn07 [format "LQ%02d07" $cell]
    set qn08 [format "LQ%02d08" $cell]
    
    set bn01 [format "LB%02d01" $cell]
    set bn02 [format "LB%02d02" $cell]
    set bn03 [format "LB%02d03" $cell]
    set bn04 [format "LB%02d04" $cell]
    set bn05 [format "LB%02d05" $cell]
    
    set sn01 [format "LS%02d01" $cell]
    set sn02 [format "LS%02d02" $cell]

    set cn01 [format "LC%02d01" $cell]
    set cn02 [format "LC%02d02" $cell]
    set cn03 [format "LC%02d03" $cell]
    set cn04 [format "LC%02d04" $cell]
    set cn05 [format "LC%02d05" $cell]
    set cn06 [format "LC%02d06" $cell]
    set cn07 [format "LC%02d07" $cell]
    set cn08 [format "LC%02d08" $cell]
    set cn09 [format "LC%02d09" $cell]
    
    Girder
    Begin6d
    Drift -name $cns -length 0.00000
    
    QuadBpm -name $qn01 -length [expr $lquad/2] -strength [expr $kq01*$lquad/2*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
    Drift -length [expr $ld01-0.10-$lcorr] -six_dim $usesixdim
    Dipole -name $cn01 -length $lcorr -e0 $e0 -strength_x 0.0 -strength_y 0.0 -six_dim $usesixdim -synrad $usesynrad
    Drift -length 0.10 -six_dim $usesixdim
    QuadBpm -name $qn02 -length $lquad -strength [expr $kq02*$lquad*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
    Drift -length $ld02 -six_dim $usesixdim
    Sbend -name $bn01 -length $larc -angle [expr -$theta] -synrad $usesynrad -six_dim $usesixdim -thin_lens $numthinlenses -e0 $e0 -E1 [expr -$theta/2] -E2 [expr -$theta/2]
    Drift -length [expr $ld03a-0.10-$lcorr] -six_dim $usesixdim
    Dipole -name $cn02 -length $lcorr -e0 $e0 -strength_x 0.0 -strength_y 0.0 -six_dim $usesixdim -synrad $usesynrad
    Drift -length [expr 0.10-0.05-0.025] -six_dim $usesixdim
    Bpm -length 0.05
    Drift -length [expr 0.10-0.05-0.025] -six_dim $usesixdim
    Multipole -name $sn01 -type 3 -length $lsext -e0 $e0 -strength [expr $ks01*$e0*$lsext] -thin_lens $numthinlenses
    Drift -length [expr $ld03b-0.025-$lcorr] -six_dim $usesixdim
    Dipole -name $cn03 -length $lcorr -e0 $e0 -strength_x 0.0 -strength_y 0.0 -six_dim $usesixdim -synrad $usesynrad
    Drift -length 0.025 -six_dim $usesixdim
    QuadBpm -name $qn03 -length $lquad -strength [expr $kq03*$lquad*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
    Drift -length [expr $ld04-0.10-$lcorr] -six_dim $usesixdim
    Dipole -name $cn04 -length $lcorr -e0 $e0 -strength_x 0.0 -strength_y 0.0 -six_dim $usesixdim -synrad $usesynrad
    Drift -length 0.10 -six_dim $usesixdim
    QuadBpm -name $qn04 -length $lquad -strength [expr $kq04*$lquad*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
    Drift -length $ld05 -six_dim $usesixdim
    Sbend -name $bn02 -length $larc -angle [expr -$theta] -synrad $usesynrad -six_dim $usesixdim -thin_lens $numthinlenses -e0 $e0 -E1 [expr -$theta/2] -E2 [expr -$theta/2]
    Drift -length $ld06 -six_dim $usesixdim
    Sbend -name $bn03 -length $larc -angle [expr -$theta] -synrad $usesynrad -six_dim $usesixdim -thin_lens $numthinlenses -e0 $e0 -E1 [expr -$theta/2] -E2 [expr -$theta/2]
    Drift -length $ld06 -six_dim $usesixdim
    Sbend -name $bn04 -length $larc -angle [expr -$theta] -synrad $usesynrad -six_dim $usesixdim -thin_lens $numthinlenses -e0 $e0 -E1 [expr -$theta/2] -E2 [expr -$theta/2]
    Drift -length [expr $ld07-0.10-$lcorr] -six_dim $usesixdim
    Dipole -name $cn05 -length $lcorr -e0 $e0 -strength_x 0.0 -strength_y 0.0 -six_dim $usesixdim -synrad $usesynrad
    Drift -length 0.10 -six_dim $usesixdim
    QuadBpm -name $qn05 -length $lquad -strength [expr $kq05*$lquad*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
    Drift -length [expr $ld08-0.10-$lcorr] -six_dim $usesixdim
    Dipole -name $cn06 -length $lcorr -e0 $e0 -strength_x 0.0 -strength_y 0.0 -six_dim $usesixdim -synrad $usesynrad
    Drift -length 0.10 -six_dim $usesixdim
    QuadBpm -name $qn06 -length $lquad -strength [expr $kq06*$lquad*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
    Drift -length [expr $ld09a-0.025-$lcorr] -six_dim $usesixdim
    Dipole -name $cn07 -length $lcorr -e0 $e0 -strength_x 0.0 -strength_y 0.0 -six_dim $usesixdim -synrad $usesynrad
    Drift -length 0.025 -six_dim $usesixdim
    Multipole -name $sn02 -type 3 -length $lsext -e0 $e0 -strength [expr $ks02*$e0*$lsext] -thin_lens $numthinlenses
    Drift -length 0.05 -six_dim $usesixdim
    Bpm -length 0.05
    Drift -length [expr $ld09b-0.05-0.05] -six_dim $usesixdim
    Sbend -name $bn05 -length $larc -angle [expr -$theta] -synrad $usesynrad -six_dim $usesixdim -thin_lens $numthinlenses -e0 $e0 -E1 [expr -$theta/2] -E2 [expr -$theta/2]
    Drift -length [expr $ld10-0.10-$lcorr] -six_dim $usesixdim
    Dipole -name $cn08 -length $lcorr -e0 $e0 -strength_x 0.0 -strength_y 0.0 -six_dim $usesixdim -synrad $usesynrad
    Drift -length 0.10 -six_dim $usesixdim
    QuadBpm -name $qn07 -length $lquad -strength [expr $kq07*$lquad*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
    Drift -length [expr $ld11-0.10-$lcorr] -six_dim $usesixdim
    Dipole -name $cn09 -length $lcorr -e0 $e0 -strength_x 0.0 -strength_y 0.0 -six_dim $usesixdim -synrad $usesynrad
    Drift -length 0.10 -six_dim $usesixdim
    
    if {$cell==8} {
        QuadBpm -name $qn08 -length [expr $lquad] -strength [expr $kq08LQ0808*$lquad*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
    } else {
        Quadrupole -name $qn08 -length [expr $lquad/2] -strength [expr $kq08*$lquad/2*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
    }
    
    Drift -name $cne -length 0.00000 -six_dim $usesixdim
    End6d
}

# match left and right arcs
set ldm1 14.91552397
set ldm2 [expr 24.584-$ldm1]
set ldmc 13.4

set kqm1  -0.1572892074
set kqm2   0.2136304422
set kqmc1  0.090098406704
set kqmc2 -0.093624668432

Girder
Begin6d
Drift -length 0.0
Drift -length [expr $ldm1-0.10-$lcorr]
Dipole -name MCM1 -length $lcorr -e0 $e0 -strength_x 0.0 -strength_y 0.0 -six_dim $usesixdim -synrad $usesynrad
Drift -length 0.10
QuadBpm -name MQM1 -length $lquad -strength [expr $kqm1*$lquad*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length [expr $ldm2-0.10-$lcorr]
Dipole -name MCM2 -length $lcorr -e0 $e0 -strength_x 0.0 -strength_y 0.0 -six_dim $usesixdim -synrad $usesynrad
Drift -length 0.10
QuadBpm -name MQM2 -length $lquad -strength [expr $kqm2*$lquad*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length [expr $ldmc-0.10-$lcorr]
Dipole -name MCM3 -length $lcorr -e0 $e0 -strength_x 0.0 -strength_y 0.0 -six_dim $usesixdim -synrad $usesynrad
Drift -length 0.10
QuadBpm -name MQM3 -length $lquad -strength [expr $kqmc2*$lquad*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length [expr $ldmc-0.10-$lcorr]
Dipole -name MCM4 -length $lcorr -e0 $e0 -strength_x 0.0 -strength_y 0.0 -six_dim $usesixdim -synrad $usesynrad
Drift -length 0.10

for {set cell 1} {$cell<=6} {incr cell} {
    set dn01 [format "MD%02d1" $cell]
    set dn02 [format "MD%02d2" $cell]
    set qn01 [format "MQ%02d1" $cell]
    set qn02 [format "MQ%02d2" $cell]
    set cn01 [format "MC%02d1" $cell]
    set cn02 [format "MC%02d2" $cell]
    
    QuadBpm -name $qn01 -length $lquad -strength [expr $kqmc1*$lquad*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
    Drift -length [expr $ldmc-0.10-$lcorr]
    Dipole -name $cn01 -length $lcorr -e0 $e0 -strength_x 0.0 -strength_y 0.0 -six_dim $usesixdim -synrad $usesynrad
    Drift -length 0.10
    QuadBpm -name $qn02 -length $lquad -strength [expr $kqmc2*$lquad*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
    Drift -length [expr $ldmc-0.10-$lcorr]
    Dipole -name $cn02 -length $lcorr -e0 $e0 -strength_x 0.0 -strength_y 0.0 -six_dim $usesixdim -synrad $usesynrad
    Drift -length 0.10
}
QuadBpm -name MQM4 -length $lquad -strength [expr $kqm2*$lquad*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length [expr $ldm2-0.10-$lcorr]
Dipole -name MCM5 -length $lcorr -e0 $e0 -strength_x 0.0 -strength_y 0.0 -six_dim $usesixdim -synrad $usesynrad
Drift -length 0.10
QuadBpm -name MQM5 -length $lquad -strength [expr $kqm1*$lquad*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length [expr $ldm1-0.10-$lcorr]
Dipole -name MCM6 -length $lcorr -e0 $e0 -strength_x 0.0 -strength_y 0.0 -six_dim $usesixdim -synrad $usesynrad
Drift -length 0.10
Drift -length 0.0
End6d

# right bending arc

for {set cell 1} {$cell<=32} {incr cell} {
    set cns [format "RC%02dSTART" $cell]
    set cne [format "RC%02dEND" $cell]
    
    set qn01 [format "RQ%02d01" $cell]
    set qn02 [format "RQ%02d02" $cell]
    set qn03 [format "RQ%02d03" $cell]
    set qn04 [format "RQ%02d04" $cell]
    set qn05 [format "RQ%02d05" $cell]
    set qn06 [format "RQ%02d06" $cell]
    set qn07 [format "RQ%02d07" $cell]
    set qn08 [format "RQ%02d08" $cell]
    
    set bn01 [format "RB%02d01" $cell]
    set bn02 [format "RB%02d02" $cell]
    set bn03 [format "RB%02d03" $cell]
    set bn04 [format "RB%02d04" $cell]
    set bn05 [format "RB%02d04" $cell]
    
    set sn01 [format "RS%02d01" $cell]
    set sn02 [format "RS%02d02" $cell]
    
    set cn01 [format "RC%02d01" $cell]
    set cn02 [format "RC%02d02" $cell]
    set cn03 [format "RC%02d03" $cell]
    set cn04 [format "RC%02d04" $cell]
    set cn05 [format "RC%02d05" $cell]
    set cn06 [format "RC%02d06" $cell]
    set cn07 [format "RC%02d07" $cell]
    set cn08 [format "RC%02d08" $cell]
    set cn09 [format "RC%02d09" $cell]
    
    Girder
    Begin6d
    Drift -name $cns -length 0.00000
    
    if {$cell==1} {
        QuadBpm -name $qn01 -length $lquad -strength [expr $kq01RQ0101*$lquad*$e0] -thin_lens $numthinlenses
    } else {
        Quadrupole -name $qn01 -length [expr $lquad/2] -strength [expr $kq01*$lquad/2*$e0] -thin_lens $numthinlenses
    }
    Drift -length [expr $ld01-0.10-$lcorr]
    Dipole -name $cn01 -length $lcorr -e0 $e0 -strength_x 0.0 -strength_y 0.0 -synrad $usesynrad
    Drift -length 0.10
    QuadBpm -name $qn02 -length $lquad -strength [expr $kq02*$lquad*$e0] -thin_lens $numthinlenses
    Drift -length $ld02
    Sbend -name $bn01 -length $larc -angle [expr $theta] -synrad $usesynrad -thin_lens $numthinlenses -e0 $e0 -E1 [expr $theta/2] -E2 [expr $theta/2]
    Drift -length [expr $ld03a-0.10-$lcorr]
    Dipole -name $cn02 -length $lcorr -e0 $e0 -strength_x 0.0 -strength_y 0.0 -synrad $usesynrad
    Drift -length [expr 0.10-0.05-0.025]
    Bpm -length 0.05
    Drift -length [expr 0.10-0.05-0.025]
    Multipole -name $sn01 -type 3 -length $lsext -e0 $e0 -strength [expr -$ks01*$e0*$lsext] -thin_lens $numthinlenses
    Drift -length [expr $ld03b-0.025-$lcorr]
    Dipole -name $cn03 -length $lcorr -e0 $e0 -strength_x 0.0 -strength_y 0.0 -synrad $usesynrad
    Drift -length 0.025
    QuadBpm -name $qn03 -length $lquad -strength [expr $kq03*$lquad*$e0] -thin_lens $numthinlenses
    Drift -length [expr $ld04-0.10-$lcorr]
    Dipole -name $cn04 -length $lcorr -e0 $e0 -strength_x 0.0 -strength_y 0.0 -synrad $usesynrad
    Drift -length 0.10
    QuadBpm -name $qn04 -length $lquad -strength [expr $kq04*$lquad*$e0] -thin_lens $numthinlenses
    Drift -length $ld05
    Sbend -name $bn02 -length $larc -angle [expr $theta] -synrad $usesynrad -thin_lens $numthinlenses -e0 $e0 -E1 [expr $theta/2] -E2 [expr $theta/2]
    Drift -length $ld06
    Sbend -name $bn03 -length $larc -angle [expr $theta] -synrad $usesynrad -thin_lens $numthinlenses -e0 $e0 -E1 [expr $theta/2] -E2 [expr $theta/2]
    Drift -length $ld06
    Sbend -name $bn04 -length $larc -angle [expr $theta] -synrad $usesynrad -thin_lens $numthinlenses -e0 $e0 -E1 [expr $theta/2] -E2 [expr $theta/2]
    Drift -length [expr $ld07-0.10-$lcorr]
    Dipole -name $cn05 -length $lcorr -e0 $e0 -strength_x 0.0 -strength_y 0.0 -synrad $usesynrad
    Drift -length 0.10
    QuadBpm -name $qn05 -length $lquad -strength [expr $kq05*$lquad*$e0] -thin_lens $numthinlenses
    Drift -length [expr $ld08-0.10-$lcorr]
    Dipole -name $cn06 -length $lcorr -e0 $e0 -strength_x 0.0 -strength_y 0.0 -synrad $usesynrad
    Drift -length 0.10
    QuadBpm -name $qn06 -length $lquad -strength [expr $kq06*$lquad*$e0] -thin_lens $numthinlenses
    Drift -length [expr $ld09a-0.025-$lcorr]
    Dipole -name $cn07 -length $lcorr -e0 $e0 -strength_x 0.0 -strength_y 0.0 -synrad $usesynrad
    Drift -length 0.025
    Multipole -name $sn02 -type 3 -length $lsext -e0 $e0 -strength [expr -$ks02*$e0*$lsext] -thin_lens $numthinlenses
    Drift -length 0.05
    Bpm -length 0.05
    Drift -length [expr $ld09b-0.05-0.05]
    Sbend -name $bn05 -length $larc -angle [expr $theta] -synrad $usesynrad -thin_lens $numthinlenses -e0 $e0 -E1 [expr $theta/2] -E2 [expr $theta/2]
    Drift -length [expr $ld10-0.10-$lcorr]
    Dipole -name $cn08 -length $lcorr -e0 $e0 -strength_x 0.0 -strength_y 0.0 -synrad $usesynrad
    Drift -length 0.10
    QuadBpm -name $qn07 -length $lquad -strength [expr $kq07*$lquad*$e0] -thin_lens $numthinlenses
    Drift -length [expr $ld11-0.10-$lcorr]
    Dipole -name $cn09 -length $lcorr -e0 $e0 -strength_x 0.0 -strength_y 0.0 -synrad $usesynrad
    Drift -length 0.10
    QuadBpm -name $qn08 -length [expr $lquad/2] -strength [expr $kq08*$lquad/2*$e0] -thin_lens $numthinlenses
    
    Drift -name $cne -length 0.00000
    End6d
}


}
