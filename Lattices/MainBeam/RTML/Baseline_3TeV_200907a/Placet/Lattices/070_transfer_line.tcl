#
# Transfer Line Lattice
#
# beamparams=[betax,alphax,emitnx,betay,alphay,emitny,sigmaz,charge,
#             uncespread,echirp,energy,nslice,nmacro,nsigmabunch,nsigmawake]
# units=[m,rad,m*rad,m,rad,m*rad,m,C,1,1/m,eV,1,1,1,1]
#
# where necessary units were converted to placet units in main.tcl

proc lattice_transfer_line {bparray} {
upvar $bparray beamparams

set usesixdim 1
set numthinlenses 100

set e0 $beamparams(energyafterbooster)

set lquadm 0.36

set kqmx 0.009713020617
set kqm1 $kqmx
set kqm2 [expr $kqmx * -1.0]
set kqm3 $kqmx
set kqm4 [expr $kqmx * -1.0]

set ldm1 [expr 219.0 - 0.36]
set ldm2 $ldm1
set ldm3 $ldm1
set ldm4 $ldm1

Girder
Drift -length 0.00000 -six_dim $usesixdim
Quadrupole -length [expr $lquadm / 2.0] -strength [expr $kqm1*$lquadm / 2.0 *$e0] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ldm1 -six_dim $usesixdim
Quadrupole -length $lquadm -strength [expr $kqm2*$lquadm*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ldm2 -six_dim $usesixdim
Quadrupole -length $lquadm -strength [expr $kqm3*$lquadm*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ldm3 -six_dim $usesixdim
Quadrupole -length $lquadm -strength [expr $kqm4*$lquadm*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ldm4 -six_dim $usesixdim
Drift -length 0.00000 -six_dim $usesixdim

for {set sec 1} {$sec<=23} {incr sec} {
    Girder
    Drift -length 0.00000 -six_dim $usesixdim
    Quadrupole -length $lquadm -strength [expr $kqm1*$lquadm*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
    Drift -length $ldm1 -six_dim $usesixdim
    Quadrupole -length $lquadm -strength [expr $kqm2*$lquadm*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
    Drift -length $ldm2 -six_dim $usesixdim
    Quadrupole -length $lquadm -strength [expr $kqm3*$lquadm*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
    Drift -length $ldm3 -six_dim $usesixdim
    Quadrupole -length $lquadm -strength [expr $kqm4*$lquadm*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
    Drift -length $ldm4 -six_dim $usesixdim
    Drift -length 0.00000 -six_dim $usesixdim
    }
}
