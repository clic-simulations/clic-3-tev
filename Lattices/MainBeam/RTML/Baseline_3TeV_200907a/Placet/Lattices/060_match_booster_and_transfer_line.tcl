#
# matching of Booster Linac and Transfer Line
#
# beamparams=[betax,alphax,emitnx,betay,alphay,emitny,sigmaz,charge,
#             uncespread,echirp,energy,nslice,nmacro,nsigmabunch,nsigmawake]
# units=[m,rad,m*rad,m,rad,m*rad,m,C,1,1/m,eV,1,1,1,1]
#
# where necessary units were converted to placet units in main.tcl

proc lattice_match_booster_and_transfer_line {bparray} {
upvar $bparray beamparams

set usesixdim 1
set numthinlenses 100

set e0 $beamparams(energyafterbooster)

set lquadm 0.36

set kqm1 -0.01766477867
set kqm2 0.2900141676  
set kqm3 -0.4655233622 
set kqm4 0.1754976538  

set ldm1 16.24783632
set ldm2 38.51420496
set ldm3 0.989885515
set ldm4 1.533483253
set ldm5 45.03637893


Girder
Drift -name BC1MATCHING -length 0.00000 -six_dim $usesixdim
Drift -name DM1 -length $ldm1 -six_dim $usesixdim
Quadrupole -name QM1 -length $lquadm -strength [expr $kqm1*$lquadm*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -name DM2 -length $ldm2 -six_dim $usesixdim
Quadrupole -name QM2 -length $lquadm -strength [expr $kqm2*$lquadm*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -name DM3 -length $ldm3 -six_dim $usesixdim
Quadrupole -name QM3 -length $lquadm -strength [expr $kqm3*$lquadm*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -name DM4 -length $ldm4 -six_dim $usesixdim
Quadrupole -name QM4 -length $lquadm -strength [expr $kqm4*$lquadm*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -name DM5 -length $ldm5 -six_dim $usesixdim
Drift -name BC1MATCHING -length 0.00000 -six_dim $usesixdim
}
