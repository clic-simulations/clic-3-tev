#
# define RF parameters
#
# rfparams=[gradient,phase,wavelength,a,g,l]
# units=[eV/m,Degree,m,m,m,m]
#
# Attention: before usage the parameters must be converted to Placet units
#

# no wakes
set rfparamsbc1(gradient) [expr 18.89e6]
# with wakes
#set rfparamsbc1(gradient) [expr 19.0489e6]
set rfparamsbc1(phase) 90.0
set rfparamsbc1(lambda) [expr 299792458/(4.0e9)]
set rfparamsbc1(a) 11.0e-3
set rfparamsbc1(g) 21.0e-3
set rfparamsbc1(l) 25.0e-3


# no wakes
set rfparamsbooster(gradient) [expr 23.48e6]
# with wakes
#set rfparamsbooster(gradient) [expr 23.549e6]
set rfparamsbooster(phase) 0.0
set rfparamsbooster(lambda) [expr 299792458/(4.0e9)]
set rfparamsbooster(a) 11.0e-3
set rfparamsbooster(g) 21.0e-3
set rfparamsbooster(l) 25.0e-3


# no wakes
set rfparamsbc2(gradient) [expr 72.29e6]
# with wakes
#set rfparamsbc2(gradient) [expr 79.8e6]
set rfparamsbc2(phase) 90.0
set rfparamsbc2(lambda) [expr 299792458/(12.0e9)]
set rfparamsbc2(a) 3.625e-3
set rfparamsbc2(g) 8.5417e-3
set rfparamsbc2(l) 10.417e-3

