% some functions to calculate corrector settings
% i.e. dipole corrector strengths, quad positions,...
%

%----------------------------------------
% calculates the required corrector settings
function trajectorycorrection(beamlinename,beamname,numberofsets,bpmfilename,correctorsfilename,statfilename,bpmres)
format long;
scriptdir="./Scripts/";
source([scriptdir,"octave_beam_statistics.m"]);
%
% some initialisations
fid=fopen(bpmfilename,'wt');
fprintf(fid,"#BPM readings (in micrometer)\n");
fprintf(fid,"#%21s %22s %22s %22s\n","BPM X initial","BPM X final","BPM Y initial","BPM Y final");
fclose(fid);
fid=fopen(correctorsfilename,'wt');
fprintf(fid,"#Corrector settings (in GeV/micrometer)\n");
fprintf(fid,"#%21s %22s %22s %22s\n","Strength X initial","Strength X final","Strenght Y initial","Strength Y final");
fclose(fid);
fid=fopen(statfilename,'wt');
fprintf(fid,"#Correction statistics (BPMs in micrometer, Emittance in m rad)\n");
fprintf(fid,"#%21s %22s %22s %22s %22s %22s %22s %22s %22s %22s %22s %22s %22s %22s %22s %22s %22s %22s\n",
            "RMS BPM X initial","Mean BPM X initial","Emit X initial","Emit X core initial","RMS BPM X final","Mean BPM X final","Emit X final","Emit X core final","Emit X perfect",
            "RMS BPM Y initial","Mean BPM Y initial","Emit Y initial","Emit Y core initial","RMS BPM Y final","Mean BPM Y final","Emit Y final","Emit Y core final","Emit Y perfect");
fclose(fid);

% may be one should set misalignments to zero????



% get lists of bpms and correctors (dipoles)
%
list_bpm=placet_get_number_list(beamlinename,"bpm");
list_corr=placet_get_number_list(beamlinename,"dipole");

numbpm=length(list_bpm)
numcorr=length(list_corr)

bpmspersector=1;
numsec=1;
secbpm(numsec)=bpmspersector;
while secbpm(numsec)<numbpm
  numsec++;
  secbpm(numsec)=secbpm(numsec-1)+bpmspersector;
end;
secbpm(numsec)=numbpm;
secbpm
numsec

printf("Initializing correction.\n");
% get response matrix
%Ax=placet_get_response_matrix_attribute(beamlinename,beamname,list_bpm,"x",list_corr,"strength_x","Zero");
%Ay=placet_get_response_matrix_attribute(beamlinename,beamname,list_bpm,"y",list_corr,"strength_y","Zero");

%correction using simple matrix inversion (will not always work)
%pinvAx=pinv(Ax);
%pinvAy=pinv(Ay);
%new_strength_x=-pinvAx(:,1:length(bpm_x_initial))*bpm_x_initial';

%for correction using SVD
%[UxT,Sxinv,Vx]=mysvd(Ax);
%[UyT,Syinv,Vy]=mysvd(Ay);

%save svd_v52test4.dat Ax Ay;

%load pinvrx_tal_with_corr.dat;
%load pinvry_tal_with_corr.dat;
%load pinvrx_v3.dat;
%load pinvry_v3.dat;
%load svd_v52test.dat;
load svd_v52test4.dat;

SVDxM=zeros(numsec,numcorr,numbpm);
SVDyM=zeros(numsec,numcorr,numbpm);

for isec=1:numsec
    printf("Section %d...\n",isec);
    secend=secbpm(isec);

    [UxT,Sxinv,Vx]=mysvd(Ax(1:secend,:),1);
    SVDxM(isec,:,1:secend)=Vx*Sxinv*UxT;

    [UyT,Syinv,Vy]=mysvd(Ay(1:secend,:),1);
    SVDyM(isec,:,1:secend)=Vy*Syinv*UyT;
end;



% define BPM resolution [um]
placet_element_set_attribute(beamlinename,list_bpm,"resolution",bpmres);

printf("Starting correction for %d misalignment sets.\n",numberofsets);
for step=1:numberofsets
  printf("Initializing beam line (Step %d).\n",step);
  % set everything to zero, i.e. start from perfect machine
  placet_element_set_attribute(beamlinename,list_corr,"strength_x",zeros(numcorr,1));
  placet_element_set_attribute(beamlinename,list_corr,"strength_y",zeros(numcorr,1));
  
  [emitperfect,beamperfect]=placet_test_no_correction(beamlinename,beamname,"Zero");
  
%  corr_sx_initial=placet_element_get_attribute(beamlinename,list_corr,"strength_x");
%  corr_sy_initial=placet_element_get_attribute(beamlinename,list_corr,"strength_y");
  corr_sx_initial=zeros(numcorr,1);
  corr_sy_initial=zeros(numcorr,1);

  % misalign the lattice
  [emituncorrected,beamuncorrected]=placet_test_no_correction(beamlinename,beamname,"Clic");
  bpm_x_uncorrected=placet_element_get_attribute(beamlinename,list_bpm,"reading_x");
  bpm_y_uncorrected=placet_element_get_attribute(beamlinename,list_bpm,"reading_y");
  
for ni=1:1
  % correction of sections
  printf("Starting sector correction.\n");

  for isec=1:numsec
    secend=secbpm(isec);
    SVDx=zeros(numcorr,secend);
    SVDy=zeros(numcorr,secend);

    if (isec==1)&&(bpmspersector==1)
      SVDx(:,:)=SVDxM(isec,:,1:secend)';
      SVDy(:,:)=SVDyM(isec,:,1:secend)';
     else
      SVDx(:,:)=SVDxM(isec,:,1:secend);
      SVDy(:,:)=SVDyM(isec,:,1:secend);
    end;
niimax=1;
for nii=1:niimax
    printf("Section %d...\n",isec);
    [emit,beaminitial]=placet_test_no_correction(beamlinename,beamname,"None",1,1,list_bpm(secend));
    
    bpm_x_initial=(placet_element_get_attribute(beamlinename,list_bpm(1:secend),"reading_x"))';
    bpm_y_initial=(placet_element_get_attribute(beamlinename,list_bpm(1:secend),"reading_y"))';
    
    % calculate correction and apply
    new_strength_x=-SVDx*bpm_x_initial;
    new_strength_y=-SVDy*bpm_y_initial;
    
    placet_element_vary_attribute(beamlinename,list_corr,"strength_x",new_strength_x/niimax*nii);
    placet_element_vary_attribute(beamlinename,list_corr,"strength_y",new_strength_y/niimax*nii);
    
end;
  end;

  
  printf("Starting correction of full beam line.\n");
  % correction of full beam line
  [emit,beaminitial]=placet_test_no_correction(beamlinename,beamname,"None");
  
  bpm_x_initial=(placet_element_get_attribute(beamlinename,list_bpm,"reading_x"))';
  bpm_y_initial=(placet_element_get_attribute(beamlinename,list_bpm,"reading_y"))';
  % calculate correction and apply
%  new_strength_x=-pinvAx*bpm_x_initial
%  new_strength_y=-pinvAy*bpm_y_initial
  [UxT,Sxinv,Vx]=mysvd(Ax,1);
  [UyT,Syinv,Vy]=mysvd(Ay,1);
  new_strength_x=-Vx*Sxinv*(UxT*bpm_x_initial);
  new_strength_y=-Vy*Syinv*(UyT*bpm_y_initial);
  
  placet_element_vary_attribute(beamlinename,list_corr,"strength_x",new_strength_x);
  placet_element_vary_attribute(beamlinename,list_corr,"strength_y",new_strength_y);
end;  
  % do tracking without changing survey
  [emit,beamfinal]=placet_test_no_correction(beamlinename,beamname,"None");
  
  [xn,xpn,yn,ypn]=delete_residual_dispersion(beamfinal);
  beamfinal(:,2)=xn;
  beamfinal(:,5)=xpn;
  beamfinal(:,3)=yn;
  beamfinal(:,6)=ypn;
  
  
  bpm_x_final=placet_element_get_attribute(beamlinename,list_bpm,"reading_x");
  bpm_y_final=placet_element_get_attribute(beamlinename,list_bpm,"reading_y");
  
  corr_sx_final=placet_element_get_attribute(beamlinename,list_corr,"strength_x");
  corr_sy_final=placet_element_get_attribute(beamlinename,list_corr,"strength_y");

  % dump data to files
  fid=fopen(bpmfilename,'at');
  fprintf(fid,"# Step %d\n",step);
  for i=1:numbpm
    fprintf(fid,'%22.14e %22.14e %22.14e %22.14e\n',bpm_x_uncorrected(i),bpm_x_final(i),bpm_y_uncorrected(i),bpm_y_final(i));
  end;
  fclose(fid);

  fid=fopen(correctorsfilename,'at');
  fprintf(fid,"# Step %d\n",step);
  for i=1:numcorr
    fprintf(fid,'%22.14e %22.14e %22.14e %22.14e\n',corr_sx_initial(i),corr_sx_final(i),corr_sy_initial(i),corr_sy_final(i));
  end;
  fclose(fid);
  
  
  beamuncorrected=units_placet_to_std(beamuncorrected);
  gammauncorrected=mean(beamuncorrected(:,1),1)/(0.5109989*1e6);
  gammauncorrected5s=mean5s(beamuncorrected(:,1),1)/(0.5109989*1e6);

  beamfinal=units_placet_to_std(beamfinal);
  gammafinal=mean(beamfinal(:,1),1)/(0.5109989*1e6);
  gammafinal5s=mean5s(beamfinal(:,1),1)/(0.5109989*1e6);
  
  beamperfect=units_placet_to_std(beamperfect);
  gammaperfect=mean(beamperfect(:,1),1)/(0.5109989*1e6);
  
  fid=fopen(statfilename,'at');
  fprintf(fid,"# Step %d\n",step);
  fprintf(fid,"%22.14e %22.14e %22.14e %22.14e ",
          rms(bpm_x_uncorrected,1),mean(bpm_x_uncorrected,1),
          gammauncorrected*emitproj(beamuncorrected(:,2),beamuncorrected(:,5),1),
          gammauncorrected5s*emitproj5s(beamuncorrected(:,2),beamuncorrected(:,5),1));
  fprintf(fid,"%22.14e %22.14e %22.14e %22.14e %22.14e ",
          rms(bpm_x_final,1),mean(bpm_x_final,1),
          gammafinal*emitproj(beamfinal(:,2),beamfinal(:,5),1),
          gammafinal5s*emitproj5s(beamfinal(:,2),beamfinal(:,5),1),
          gammaperfect*emitproj(beamperfect(:,2),beamperfect(:,5),1));
  fprintf(fid,"%22.14e %22.14e %22.14e %22.14e ",
          rms(bpm_y_uncorrected,1),mean(bpm_y_uncorrected,1),
          gammauncorrected*emitproj(beamuncorrected(:,3),beamuncorrected(:,6),1),
          gammauncorrected5s*emitproj5s(beamuncorrected(:,3),beamuncorrected(:,6),1));
  fprintf(fid,"%22.14e %22.14e %22.14e %22.14e %22.14e\n",
          rms(bpm_y_final,1),mean(bpm_y_final,1),
          gammafinal*emitproj(beamfinal(:,3),beamfinal(:,6),1),
          gammafinal5s*emitproj5s(beamfinal(:,3),beamfinal(:,6),1),
          gammaperfect*emitproj(beamperfect(:,3),beamperfect(:,6),1));
  fclose(fid);
end;
placet_element_set_attribute(beamlinename,list_corr,"strength_x",zeros(numcorr,1));
placet_element_set_attribute(beamlinename,list_corr,"strength_y",zeros(numcorr,1));
endfunction;
%----------------------------------------


%----------------------------------------
function [UT,Sinv,V]=mysvd(A,sd)

[U,S,V]=svd(A);

% octave svd returns simplified matrices missing rows and columns, which only contain zeros
sizeU=size(U);
sizeA=size(A);
if (sizeU(1)<sizeA(1))
  U=[U;zeros(sizeA(1)-sizeU(1),sizeU(2))];
end;
if (sizeU(2)<sizeA(2))
  U=[U,zeros(sizeU(1),sizeA(2)-sizeU(2))];
end;
sizeS=size(S);
if (sizeS(1)<sizeA(2))
  S=[S;zeros(sizeA(2)-sizeS(1),sizeS(2))];
end;
if (sizeS(2)<sizeA(2))
  S=[S,zeros(sizeS(1),sizeA(2)-sizeS(2))];
end;

if sd==1
  for i=1:length(S)
    if S(i,i)<0.00001
      S(i,i)=0.0;
    end;
  end;
end;


Sinv=pinv(S);
UT=U';

endfunction;
%----------------------------------------
