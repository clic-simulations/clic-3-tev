This is the CLIC Mainbeam RTML Baseline Lattice from Juli 2009.
It is provided for the codes Placet and Elegant.
All required files and scrips are included.
Hence, it should be easy to run simulations if you have the respective binaries of the codes.
Have a look at "run_elegant_mpi.sh" for the Elegant simulations and "run_placet.sh" for the Placet simulations.


