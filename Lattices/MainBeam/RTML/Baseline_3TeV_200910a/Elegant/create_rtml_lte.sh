#!/bin/bash
#
cp ./Lattices/000_start.lte rtml.lte
cat ./Lattices/010_bc1_rf.lte >> rtml.lte
cat ./Lattices/020_bc1_match_rf_and_chicane.lte >> rtml.lte
cat ./Lattices/030_bc1_chicane.lte >> rtml.lte
cat ./Lattices/040_match_bc1_and_booster.lte >> rtml.lte
cat ./Lattices/050_booster.lte >> rtml.lte
cat ./Lattices/060_match_booster_and_transfer_line.lte >> rtml.lte
cat ./Lattices/06X_skip_transfer_line.lte >> rtml.lte
cat ./Lattices/070_transfer_line.lte >> rtml.lte
cat ./Lattices/080_match_transfer_line_and_tal.lte >> rtml.lte
cat ./Lattices/090_turn_around_loop.lte >> rtml.lte
cat ./Lattices/100_match_tal_and_bc2.lte >> rtml.lte
cat ./Lattices/110_bc2_rf.lte >> rtml.lte
cat ./Lattices/120_bc2_match_rf_and_chicane.lte >> rtml.lte
cat ./Lattices/130_bc2_chicane_1.lte >> rtml.lte
cat ./Lattices/132_bc2_match_chicanes.lte >> rtml.lte
cat ./Lattices/134_bc2_chicane_2.lte >> rtml.lte

echo "

% 0.65e-9 sto qbunch
% 200000 sto npart
% qbunch npart / sto qpart

Q0: CHARGE, TOTAL=\"qbunch\", PER_PARTICLE=\"qpart\"

W01: WATCH, FILENAME=\"watch_01.dat\"
W02: WATCH, FILENAME=\"watch_02.dat\"
W03: WATCH, FILENAME=\"watch_03.dat\"
W04: WATCH, FILENAME=\"watch_04.dat\"
W05: WATCH, FILENAME=\"watch_05.dat\"
W06: WATCH, FILENAME=\"watch_06.dat\"
W07: WATCH, FILENAME=\"watch_07.dat\"
W08: WATCH, FILENAME=\"watch_08.dat\"
W09: WATCH, FILENAME=\"watch_09.dat\"
W10: WATCH, FILENAME=\"watch_10.dat\"
W11: WATCH, FILENAME=\"watch_11.dat\"
W12: WATCH, FILENAME=\"watch_12.dat\"
W13: WATCH, FILENAME=\"watch_13.dat\"
W14: WATCH, FILENAME=\"watch_14.dat\"
W15: WATCH, FILENAME=\"watch_15.dat\"
W16: WATCH, FILENAME=\"watch_16.dat\"


RTML: LINE=(Q0,W01,LINE010,W02,LINE020,W03,LINE030,W04,LINE040,W05,LINE050,W06,LINE060,&
            W07,LINE070,W08,LINE080,W09,LINE090,W10,LINE100,W11,LINE110,W12,LINE120,W13,LINE130,W14,&
            LINE132,W15,LINE134,W16)

RTMLE: LINE=(Q0,W01,LINE010E,W02,LINE020E,W03,LINE030E,W04,LINE040E,W05,LINE050E,W06,LINE060E,&
             W07,LINE070E,W08,LINE080E,W09,LINE090E,W10,LINE100E,W11,LINE110E,W12,LINE120E,W13,LINE130E,W14,&
             LINE132E,W15,LINE134E,W16)

RTMLCSR: LINE=(Q0,W01,LINE010CSR,W02,LINE020CSR,W03,LINE030CSR,W04,LINE040CSR,W05,LINE050CSR,W06,LINE060CSR,&
               W07,LINE070CSR,W08,LINE080CSR,W09,LINE090CSR,W10,LINE100CSR,W11,LINE110CSR,W12,LINE120CSR,W13,LINE130CSR,W14,&
               LINE132CSR,W15,LINE134CSR,W16)

" >> rtml.lte
