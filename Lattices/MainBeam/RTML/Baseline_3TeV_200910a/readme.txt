This is the CLIC Mainbeam RTML Baseline Lattice.
It is provided for the codes Placet and Elegant.

All required files and scripts are included, except for the respective binaries of the codes.
Have a look at "run_elegant_mpi.sh" for the Elegant simulations and "run_placet.sh" for the Placet simulations.


Version 2009 10a
Changes:
Update of the short range cavity wake field functions
Correction of a bug in the Turn Around Loop Lattice (too short matching between left bending and right bending arcs)
Cleanup of several files

Version 2009 07a
Initial baseline lattice

