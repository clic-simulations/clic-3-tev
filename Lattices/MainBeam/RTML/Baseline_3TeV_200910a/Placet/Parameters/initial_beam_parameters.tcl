#
# define beam parameters at entrances of all sections
# if tracking starts in front of a section, the corresponding
# beam parameters as defined here should be used
#
# these parameters might not be corrected for wake field effects etc.
# i.e. they are not necessarily what a real simulation will give
#
# beamXXX=[betax,alphax,emitnx,betay,alphay,emitny,sigmaz,charge,
#          uncespr,echirp,energy]
# units=[m,rad,m*rad,m,rad,m*rad,m,C,1,1/m,eV]
#
# Attention: before usage the parameters must be converted to Placet units
#

# at entrance of section 001 (Matching DR and BC1 RF)
set beam001(betax)     0.0
set beam001(alphax)    0.0
set beam001(emitnx)  500.0e-9
set beam001(betay)     0.0
set beam001(alphay)    0.0
set beam001(emitny)    5.0e-9
set beam001(sigmaz) 1400.0e-6
set beam001(charge)    0.65e-9
set beam001(uncespr)   1.0e-3
set beam001(echirp)    0.0
set beam001(energy)    2.86e9

# at entrance of section 010 (BC1 RF)
set beam010(betax)    40.0
set beam010(alphax)    0.0
set beam010(emitnx)  500.0e-9
set beam010(betay)    40.0
set beam010(alphay)    0.0
set beam010(emitny)    5.0e-9
set beam010(sigmaz) 1400.0e-6
set beam010(charge)    0.65e-9
set beam010(uncespr)   1.0e-3
set beam010(echirp)    0.0
set beam010(energy)    2.86e9

# at entrance of section 020 (Matching BC1 RF and Chicane)
set beam020(betax)    40.0
set beam020(alphax)    0.0
set beam020(emitnx)  500.0e-9
set beam020(betay)    40.0
set beam020(alphay)    0.0
set beam020(emitny)    5.0e-9
set beam020(sigmaz) 1400.0e-6
set beam020(charge)    0.65e-9
set beam020(uncespr)   1.0e-3
set beam020(echirp)    4.949
set beam020(energy)    2.86e9

# at entrance of section 030 (BC1 Chicane)
set beam030(betax)   100.0
set beam030(alphax)    2.4
set beam030(emitnx)  500.0e-9
set beam030(betay)    33.4
set beam030(alphay)    2.0
set beam030(emitny)    5.0e-9
set beam030(sigmaz) 1400.0e-6
set beam030(charge)    0.65e-9
set beam030(uncespr)   1.0e-3
set beam030(echirp)    4.949
set beam030(energy)    2.86e9

# at entrance of section 040 (Matching BC1 and Booster)
set beam040(betax)    16.68004848
set beam040(alphax)    0.3571712156
set beam040(emitnx)  500.0e-9
set beam040(betay)    50.93527278
set beam040(alphay)   -2.482120204
set beam040(emitny)    5.0e-9
set beam040(sigmaz)  300.0e-6
set beam040(charge)    0.65e-9
set beam040(uncespr)   4.67e-3
set beam040(echirp)   17.39
set beam040(energy)    2.86e9

# at entrance of section 050 (Booster Linac)
set beam050(betax)     5.26359
set beam050(alphax)    0.0
set beam050(emitnx)  500.0e-9
set beam050(betay)    53.5488
set beam050(alphay)    0.0
set beam050(emitny)    5.0e-9
set beam050(sigmaz)  300.0e-6
set beam050(charge)    0.65e-9
set beam050(uncespr)   4.67e-3
set beam050(echirp)   17.39
set beam050(energy)    2.86e9

# at entrance of section 060 (Matching Booster and Transfer Line)
set beam060(betax)     9.4694
set beam060(alphax)   -0.923194
set beam060(emitnx)  500.0e-9
set beam060(betay)    33.7166
set beam060(alphay)    2.42885
set beam060(emitny)    5.0e-9
set beam060(sigmaz)  300.0e-6
set beam060(charge)    0.65e-9
set beam060(uncespr)   1.67e-3
set beam060(echirp)    6.22
set beam060(energy)    8.0e9

# at entrance of section 070 (Transfer Line)
set beam070(betax)   856.387062
set beam070(alphax)    0.0
set beam070(emitnx)  500.0e-9
set beam070(betay)   382.4304501
set beam070(alphay)    0.0
set beam070(emitny)    5.0e-9
set beam070(sigmaz)  300.0e-6
set beam070(charge)    0.65e-9
set beam070(uncespr)   1.67e-3
set beam070(echirp)    6.22
set beam070(energy)    8.0e9

# at entrance of section 080 (Matching Transfer Line and TAL)
set beam080(betax)   382.4304501
set beam080(alphax)    0.0
set beam080(emitnx)  500.0e-9
set beam080(betay)   856.387062
set beam080(alphay)    0.0
set beam080(emitny)    5.0e-9
set beam080(sigmaz)  300.0e-6
set beam080(charge)    0.65e-9
set beam080(uncespr)   1.67e-3
set beam080(echirp)    6.22
set beam080(energy)    8.0e9

# at entrance of section 090 (Turn Around Loop)
set beam090(betax)    73.5
set beam090(alphax)    0.0
set beam090(emitnx)  500.0e-9
set beam090(betay)    35.2
set beam090(alphay)    0.0
set beam090(emitny)    5.0e-9
set beam090(sigmaz)  300.0e-6
set beam090(charge)    0.65e-9
set beam090(uncespr)   1.67e-3
set beam090(echirp)    6.22
set beam090(energy)    8.0e9

# at entrance of section 100 (Matching TAL and BC2)
set beam100(betax)    73.5
set beam100(alphax)    0.0
set beam100(emitnx)  500.0e-9
set beam100(betay)    35.2
set beam100(alphay)    0.0
set beam100(emitny)    5.0e-9
set beam100(sigmaz)  300.0e-6
set beam100(charge)    0.65e-9
set beam100(uncespr)   1.67e-3
set beam100(echirp)    6.22
set beam100(energy)    8.0e9

# at entrance of section 110 (BC2 RF)
set beam110(betax)    40.0
set beam110(alphax)    0.0
set beam110(emitnx)  500.0e-9
set beam110(betay)    40.0
set beam110(alphay)    0.0
set beam110(emitny)    5.0e-9
set beam110(sigmaz)  300.0e-6
set beam110(charge)    0.65e-9
set beam110(uncespr)   1.67e-3
set beam110(echirp)    6.22
set beam110(energy)    8.0e9

# at entrance of section 120 (Matching BC2 RF and Chicane)
set beam120(betax)    40.0
set beam120(alphax)    0.0
set beam120(emitnx)  500.0e-9
set beam120(betay)    40.0
set beam120(alphay)    0.0
set beam120(emitny)    5.0e-9
set beam120(sigmaz)  300.0e-6
set beam120(charge)    0.65e-9
set beam120(uncespr)   1.67e-3
set beam120(echirp)   37.51
set beam120(energy)    8.0e9

# at entrance of section 130 (BC2 Chicane 1)
set beam130(betax)   100.0
set beam130(alphax)    2.6
set beam130(emitnx)  500.0e-9
set beam130(betay)    10.91
set beam130(alphay)   -0.02
set beam130(emitny)    5.0e-9
set beam130(sigmaz)  300.0e-6
set beam130(charge)    0.65e-9
set beam130(uncespr)   1.67e-3
set beam130(echirp)   37.51
set beam130(energy)    8.0e9

# at entrance of section 132 (Matching BC2 Chicanes 1 and 2c)
set beam132(betax)    13.81853162
set beam132(alphax)    0.2689201629
set beam132(emitnx)  500.0e-9
set beam132(betay)    91.65203792
set beam132(alphay)   -2.58772864
set beam132(emitny)    5.0e-9
set beam132(sigmaz)  100.0e-6
set beam132(charge)    0.65e-9
set beam132(uncespr)   5.0e-3
set beam132(echirp)  102.15
set beam132(energy)    8.0e9

# at entrance of section 134 (BC2 Chicane 2)
set beam134(betax)    70.0
set beam134(alphax)    1.8
set beam134(emitnx)  500.0e-9
set beam134(betay)    30.0
set beam134(alphay)    0.05
set beam134(emitny)    5.0e-9
set beam134(sigmaz)  100.0e-6
set beam134(charge)    0.65e-9
set beam134(uncespr)   5.0e-3
set beam134(echirp)   102.15
set beam134(energy)    8.0e9

# at entrance of section 140 (Matching BC2 and Main Linac)
set beam140(betax)    16.51469174
set beam140(alphax)   -0.01784576236
set beam140(emitnx)  600.0e-9
set beam140(betay)    56.15712121
set beam140(alphay)   -0.9140013203
set beam140(emitny)   10.0e-9
set beam140(sigmaz)   44.0e-6
set beam140(charge)    0.65e-9
set beam140(uncespr)   1.14e-2
set beam140(echirp)    0.0
set beam140(energy)    8.0e9


