#
# matching of BC2 and Main Linac
#
# beamparams=[betax,alphax,emitnx,betay,alphay,emitny,sigmaz,charge,
#             uncespread,echirp,energy,nslice,nmacro,nsigmabunch,nsigmawake]
# units=[m,rad,m*rad,m,rad,m*rad,m,C,1,1/m,eV,1,1,1,1]
#
# where necessary units were converted to placet units in main.tcl

proc lattice_match_bc2_and_main_linac {bparray} {
upvar $bparray beamparams

set usesixdim 1
set numthinlenses 100

set quad_synrad 0
set e0 $beamparams(energyafterbc2rf)
set q0 $beamparams(charge)


set kqm1  0.300
set kqm2 -0.200
set kqm3 -0.310
set kqm4  0.395
set lquadm 0.36

Girder
Drift -name BC2MATCHING -length 0.00000 -six_dim $usesixdim
Drift -name DM1 -length 1.000000 -six_dim $usesixdim
Quadrupole -name QM1 -synrad $quad_synrad -length $lquadm -strength [expr $kqm1*$lquadm*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -name DM2 -length 5.000000 -six_dim $usesixdim
Quadrupole -name QM2 -synrad $quad_synrad -length $lquadm -strength [expr $kqm2*$lquadm*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -name DM3 -length 15.00000 -six_dim $usesixdim
Quadrupole -name QM3 -synrad $quad_synrad -length $lquadm -strength [expr $kqm3*$lquadm*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -name DM4 -length 2.800000 -six_dim $usesixdim
Quadrupole -name QM4 -synrad $quad_synrad -length $lquadm -strength [expr $kqm4*$lquadm*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -name DM5 -length 1.000000 -six_dim $usesixdim
Drift -name BC2MATCHING -length 0.00000 -six_dim $usesixdim
}
