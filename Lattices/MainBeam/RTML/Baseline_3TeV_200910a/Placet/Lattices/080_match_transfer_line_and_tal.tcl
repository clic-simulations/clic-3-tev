#
# matching of Transfer Line and Turn Around Loop
#
# beamparams=[betax,alphax,emitnx,betay,alphay,emitny,sigmaz,charge,
#             uncespread,echirp,energy,nslice,nmacro,nsigmabunch,nsigmawake]
# units=[m,rad,m*rad,m,rad,m*rad,m,C,1,1/m,eV,1,1,1,1]
#
# where necessary units were converted to placet units in main.tcl

proc lattice_match_transfer_line_and_tal {bparray} {
upvar $bparray beamparams

set usesixdim 1
set numthinlenses 100

set e0 $beamparams(energyafterbooster)

set lquadm 0.36

set kqm1 0.009713020617
set kqm2 -0.09572004234
set kqm3 0.07934611709 
set kqm4 0.02585011811 
set kqm5 -0.07603614877

set ldm1 44.37884029
set ldm2 5.810105391
set ldm3 28.27003087
set ldm4 79.11879376
set ldm5 36.42672925


Girder
Drift -length 0.00000 -six_dim $usesixdim
Quadrupole -length [expr $lquadm/2.0] -strength [expr $kqm1*$lquadm/2.0*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ldm1 -six_dim $usesixdim
Quadrupole -length $lquadm -strength [expr $kqm2*$lquadm*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ldm2 -six_dim $usesixdim
Quadrupole -length $lquadm -strength [expr $kqm3*$lquadm*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ldm3 -six_dim $usesixdim
Quadrupole -length $lquadm -strength [expr $kqm4*$lquadm*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ldm4 -six_dim $usesixdim
Quadrupole -length $lquadm -strength [expr $kqm5*$lquadm*$e0] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ldm5 -six_dim $usesixdim
Drift -length 0.00000 -six_dim $usesixdim
}
