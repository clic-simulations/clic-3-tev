#
# BC1 RF lattice
#
# beamparams=[betax,alphax,emitnx,betay,alphay,emitny,sigmaz,charge,
#             uncespread,echirp,energy,nslice,nmacro,nsigmabunch,nsigmawake]
# units=[m,rad,m*rad,m,rad,m*rad,m,C,1,1/m,eV,1,1,1,1]
#
# rfparams=[gradient,phase,lambda,a,g,l,delta,delta_g]
# units=[eV/m,Degree,m,m,m,m,m,m]
#
# where necessary units were converted to placet units in main.tcl

proc lattice_bc1_rf {rfparray bparray} {
upvar $rfparray rfparams
upvar $bparray beamparams

#puts [array get rfparams]
#puts [array get beamparams]

set usesixdim 1
set numthinlenses 100

set gradient $rfparams(gradient)
set e0 $beamparams(energy)
set q0 $beamparams(charge)
set cavphase $rfparams(phase)
set lcav 3.0

set lquad 0.36
set kq1  0.1562498419
set kq2 -0.3116367758
set ldrift 0.6

#this is required to take into account the energy gain due to acceleration
#
set de [expr $gradient*$lcav*cos($cavphase/180.0*3.14159265359)]
#
#this is required to take into account the energy gain due to acceleration
#and the energy loss due to wake fields
#you have to check your wake fields fiel to get the correct average energy loss
#
#set de [expr $gradient*$lcav*cos($cavphase/180.0*3.14159265359)-0.0652*1e6/1e9*$lcav]
#puts $de

set ebeam $e0

for {set sec 1} {$sec<=2} {incr sec} {
    set sn [format "SEC%02d" $sec]
    set cn [format "ACC%02d" $sec]
    set dn1 [format "DR%02d1" $sec]
    set dn2 [format "DR%02d2" $sec]
    set dn3 [format "DR%02d3" $sec]
    set dn4 [format "DR%02d4" $sec]
    set qn1 [format "QF%02d1" $sec]
    set qn2 [format "QD%02d2" $sec]
    set qn3 [format "QF%02d3" $sec]

    Girder
    Drift -name $sn -length 0.00000 -six_dim $usesixdim

    Cavity -name $cn -length $lcav -gradient $gradient -phase $cavphase -type 0
    set ebeam [expr $ebeam+$de]
    Drift -name $dn1 -length $ldrift -six_dim $usesixdim
    Quadrupole -name $qn1 -length $lquad -strength [expr $kq1*$lquad*$ebeam] -six_dim $usesixdim -thin_lens $numthinlenses
    Drift -name $dn2 -length $ldrift -six_dim $usesixdim
    Quadrupole -name $qn2 -length $lquad -strength [expr $kq2*$lquad*$ebeam] -six_dim $usesixdim -thin_lens $numthinlenses
    Drift -name $dn3 -length $ldrift -six_dim $usesixdim
    Quadrupole -name $qn3 -length $lquad -strength [expr $kq1*$lquad*$ebeam] -six_dim $usesixdim -thin_lens $numthinlenses
    Drift -name $dn4 -length $ldrift -six_dim $usesixdim
    Drift -name $sn -length 0.00000 -six_dim $usesixdim
}
set cn [format "ACC%02d" [expr $sec+1]]
Cavity -name $cn -length $lcav -gradient $gradient -phase $cavphase -type 0
set ebeam [expr $ebeam+$de]

set beamparams(energyafterbc1rf) $ebeam
puts "Setting beamparams(energyafterbc1rf)=$beamparams(energyafterbc1rf)."
}
