#
# CLIC Main Beam RTML
#
#
puts ""
puts "Setting up RTML tracking"
puts ""

set latticedir ./Lattices
set scriptdir ./Scripts
set paramsdir ./Parameters

# define the array placetunits for unit conversions
# placetunits=[xyz,xpyp,energy,charge,emittance]
source $scriptdir/placet_units.tcl

#
# load some external files, i.e. parameters, lattices, scripts
#

# load standard beam parameters
# beamXXX=[betax,alphax,emitnx,betay,alphay,emitny,sigmaz,charge,
#          uncespr,echirp,energy]
# units=[m,rad,m*rad,m,rad,m*rad,m,C,1,1/m,eV]
# for use in Placet some of these untis have to be converted using placetunits
source $paramsdir/initial_beam_parameters.tcl

#
# load standard rf parameters
# rfparamsXXX=[gradient,phase,wavelength,a,g,l]
# units=[eV/m,Degree,m,m,m,m]
# for use in Placet some of these untis have to be converted using placetunits
source $paramsdir/rf_parameters.tcl

#
# load procedures to set up lattices
source $latticedir/010_bc1_rf.tcl
source $latticedir/020_bc1_match_rf_and_chicane.tcl
source $latticedir/030_bc1_chicane.tcl
source $latticedir/040_match_bc1_and_booster.tcl
source $latticedir/050_booster.tcl
source $latticedir/060_match_booster_and_transfer_line.tcl
source $latticedir/070_transfer_line.tcl
source $latticedir/080_match_transfer_line_and_tal.tcl
source $latticedir/090_turn_around_loop.tcl
source $latticedir/100_match_tal_and_bc2.tcl
source $latticedir/110_bc2_rf.tcl
source $latticedir/120_bc2_match_rf_and_chicane.tcl
source $latticedir/130_bc2_chicane_1.tcl
source $latticedir/132_bc2_match_chicanes.tcl
source $latticedir/134_bc2_chicane_2.tcl

#
# load procedures to set up cavities and beams
source $scriptdir/cavitywakesetup.tcl
source $scriptdir/beamsetup.tcl

#
# load tracking procedures
source $scriptdir/tracking.tcl

#
# set some general parameters which should not change throughout tracking
set beamparams(nslice) 4001
set beamparams(nmacro) 51
set beamparams(nsigmabunch) 4
set beamparams(nsigmawake) 10

# this is needed to concatenate data at the correct z position since each step
# restarts at z=0.0
set zoffset 0.0
set fid [open zoffset.dat w]
puts $fid "# Step / Start Position"
puts $fid "1   $zoffset"
close $fid

#
# track through first part of RTML, i.e, up to entrance of booster linac
puts ""
puts "Setting up tracking step step1 using beamline rtmlpart1 and beam beam1"
puts ""

# general beam and rf parameters
#
# beamparams=[betax,alphax,emitnx,betay,alphay,emitny,sigmaz,charge,
#             uncespread,echirp,energy,nslice,nmacro,nsigma]
# units=[m,rad,m*rad,m,rad,m*rad,m,C,1,1/m,eV,1,1,1]
# hence, in the array the values are stored in Placet units
set beamparams(betax) $beam010(betax)
set beamparams(alphax) $beam010(alphax)
set beamparams(emitnx) [expr $beam010(emitnx)*$placetunits(emittance)]
set beamparams(betay) $beam010(betay)
set beamparams(alphay) $beam010(alphay)
set beamparams(emitny) [expr $beam010(emitny)*$placetunits(emittance)]
set beamparams(sigmaz) [expr $beam010(sigmaz)*$placetunits(xyz)]
set beamparams(charge) [expr $beam010(charge)*$placetunits(charge)]
set beamparams(uncespread) $beam010(uncespr)
set beamparams(echirp) [expr $beam010(echirp)/$placetunits(xyz)]
set beamparams(energy) [expr $beam010(energy)*$placetunits(energy)]

set beamparams(energyafterbc1) [expr $beam010(energy)*$placetunits(energy)]
set beamparams(energyafterbooster) [expr $beam010(energy)*$placetunits(energy)]
set beamparams(energyafterbc2) [expr $beam010(energy)*$placetunits(energy)]

set beamparams(useisr) 0
set beamparams(usewakefields) 0

set rfparamsbc1(gradient) [expr $rfparamsbc1(gradient)*$placetunits(energy)]

#
#setup of lattice and beam
#
BeamlineNew
Girder
TclCall -script {BeamDump -file bunch_step_1a.dat}
lattice_bc1_rf rfparamsbc1 beamparams
TclCall -script {BeamDump -file bunch_step_1b.dat}
lattice_bc1_match_rf_and_chicane beamparams
lattice_bc1_chicane beamparams
lattice_match_bc1_and_booster beamparams
TclCall -script {BeamDump -file bunch_step_1c.dat}
BeamlineSet -name rtmlpart1

FirstOrder 1

initialize_cavities rfparamsbc1 wakelong1
make_particle_beam beam1 beamparams rfparamsbc1 wake1.dat

# perform tracking
# set new values in beamparams
do_tracking rtmlpart1 beam1 beamparams 1


# end of first tracking part

#
# track through second part of RTML, i.e, up to entrance of BC2 RF
puts ""
puts "Setting up tracking step step2 using beamline rtmlpart2 and beam beam2"
puts ""

# general beam and rf parameters
#
# beamparams=[betax,alphax,emitnx,betay,alphay,emitny,sigmaz,charge,
#             uncespread,echirp,energy,nslice,nmacro,nsigma]
# units=[m,rad,m*rad,m,rad,m*rad,m,C,1,1/m,eV,1,1,1]
# hence, in the array the values are stored in Placet units
#set beamparams(betax) $beam050(betax)
#set beamparams(alphax) $beam050(alphax)
#set beamparams(emitnx) [expr $beam050(emitnx)*$placetunits(emittance)]
#set beamparams(betay) $beam050(betay)
#set beamparams(alphay) $beam050(alphay)
#set beamparams(emitny) [expr $beam050(emitny)*$placetunits(emittance)]
#set beamparams(sigmaz) [expr $beam050(sigmaz)*$placetunits(xyz)]
#set beamparams(charge) [expr $beam050(charge)*$placetunits(charge)]
#set beamparams(uncespread) $beam050(uncespr)
#set beamparams(echirp) [expr $beam050(echirp)/$placetunits(xyz)]
#set beamparams(energy) [expr $beam050(energy)*$placetunits(energy)]
#set beamparams(energy) $beamparams(energyafterbc1)

#set beamparams(energyafterbc1) [expr $beam010(energy)*$placetunits(energy)]
#set beamparams(energyafterbooster) [expr $beam050(energy)*$placetunits(energy)]
#set beamparams(energyafterbc2) [expr $beam050(energy)*$placetunits(energy)]

#set beamparams(useisr) 0
#set beamparams(usewakefields) 1

set rfparamsbooster(gradient) [expr $rfparamsbooster(gradient)*$placetunits(energy)]

#
#setup of lattice and beam
#
BeamlineNew
Girder
TclCall -script {BeamDump -file bunch_step_2a.dat}
lattice_booster rfparamsbooster beamparams
TclCall -script {BeamDump -file bunch_step_2b.dat}
lattice_match_booster_and_transfer_line beamparams
lattice_transfer_line beamparams
lattice_match_transfer_line_and_tal beamparams
TclCall -script {BeamDump -file bunch_step_2c.dat}
lattice_turn_around_loop beamparams
TclCall -script {BeamDump -file bunch_step_2d.dat}
lattice_match_tal_and_bc2 beamparams
TclCall -script {BeamDump -file bunch_step_2e.dat}
BeamlineSet -name rtmlpart2

FirstOrder 1

initialize_cavities rfparamsbooster wakelong2
reload_particle_beam beam2 beamparams rfparamsbooster bunch_step_1c.dat wake2.dat

# perform tracking
# set new values in beamparams
do_tracking rtmlpart2 beam2 beamparams 2


# end of second tracking part

#
# track through third part of RTML, i,e, up to entrance of main linac
puts ""
puts "Setting up tracking step step3 using beamline rtmlpart3 and beam beam3"
puts ""

# general beam and rf parameters
#
# beamparams=[betax,alphax,emitnx,betay,alphay,emitny,sigmaz,charge,
#             uncespread,echirp,energy,nslice,nmacro,nsigma]
# units=[m,rad,m*rad,m,rad,m*rad,m,C,1,1/m,eV,1,1,1]
# hence, in the array the values are stored in Placet units
#set beamparams(betax) $beam050(betax)
#set beamparams(alphax) $beam050(alphax)
#set beamparams(emitnx) [expr $beam050(emitnx)*$placetunits(emittance)]
#set beamparams(betay) $beam050(betay)
#set beamparams(alphay) $beam050(alphay)
#set beamparams(emitny) [expr $beam050(emitny)*$placetunits(emittance)]
#set beamparams(sigmaz) [expr $beam050(sigmaz)*$placetunits(xyz)]
#set beamparams(charge) [expr $beam050(charge)*$placetunits(charge)]
#set beamparams(uncespread) $beam050(uncespr)
#set beamparams(echirp) [expr $beam050(echirp)/$placetunits(xyz)]
#set beamparams(energy) [expr $beam050(energy)*$placetunits(energy)]
#set beamparams(energy) $beamparams(energyafterbc1)

#set beamparams(energyafterbc1) [expr $beam010(energy)*$placetunits(energy)]
#set beamparams(energyafterbooster) [expr $beam050(energy)*$placetunits(energy)]
#set beamparams(energyafterbc2) [expr $beam050(energy)*$placetunits(energy)]

#set beamparams(useisr) 0
#set beamparams(usewakefields) 1

set rfparamsbc2(gradient) [expr $rfparamsbc2(gradient)*$placetunits(energy)]

#
#setup of lattice and beam
#
BeamlineNew
Girder
TclCall -script {BeamDump -file bunch_step_3a.dat}
lattice_bc2_rf rfparamsbc2 beamparams
TclCall -script {BeamDump -file bunch_step_3b.dat}
lattice_bc2_match_rf_and_chicane beamparams
lattice_bc2_chicane_1 beamparams
TclCall -script {BeamDump -file bunch_step_3c.dat}
lattice_bc2_match_chicanes beamparams
lattice_bc2_chicane_2 beamparams
TclCall -script {BeamDump -file bunch_step_3d.dat}
BeamlineSet -name rtmlpart3

FirstOrder 1

initialize_cavities rfparamsbc2 wakelong3
reload_particle_beam beam3 beamparams rfparamsbc2 bunch_step_2e.dat wake3.dat

# perform tracking
# set new values in beamparams
do_tracking rtmlpart3 beam3 beamparams 3


# end of third tracking part

