#
# the big tracking procedure
#
proc do_tracking {beamlinename beamname bparray stepnum} {
    global zoffset
    global placetunits
    upvar $bparray beamparams
    
    puts ""
    puts "Perform tracking step $stepnum using beamline $beamlinename and beam $beamname"
    puts ""
    
    #
    # store phase space distribution in file
    #
    puts "Dumping beam to file..."
    set outname [format "bunch_initial_step_%d.dat" $stepnum]
    BeamDump -beam $beamname -file $outname
    puts "...Done!"

    #
    # plot Twiss functions
    #
    puts "Dumping Twiss data to file..."
    set outname [format "twiss_step_%d.dat" $stepnum]
#    TwissPlotStep -file $outname -beam $beamname -step 0.05
    puts "...Done!"

    #
    SurveyErrorSet \
        -quadrupole_x 0.0 \
        -quadrupole_y 0.0 \
        -quadrupole_xp 0.00 \
        -quadrupole_yp 0.00 \
        -quadrupole_roll 0.0 \
        -cavity_x 0.0 \
        -cavity_y 0.0 \
        -cavity_xp 0.0 \
        -cavity_yp 0.0 \
        -bpm_x 0 \
        -bpm_y 0

    
    #
    # to hand over parameters to Octave, we store them in a file
    #
    set fid [open trackingparams.tmp w]
    puts $fid "$beamlinename $beamname $stepnum $zoffset"
    close $fid
    
    #
    # we do tracking, plotting, etc. in octave
    #
    Octave {
        format long;
        % read in some useful functions
        addpath("./Placet_Octave");
        scriptdir="./Scripts/";
        source([scriptdir,"octave_beam_statistics.m"]);
        source([scriptdir,"octave_correction_algorithms.m"]);
        
        fid=fopen("trackingparams.tmp","rt");
        [beamlinename,beamname,stepnum,zoffset]=fscanf(fid,"%s %s %d %f","C");
        fclose(fid);

        % read in the initial Placet beam for usage in Octave
        beamini=placet_get_beam(beamname);
        beamini=units_placet_to_std(beamini);
        deeini=beamini(:,1)/mean(beamini(:,1),1)-1;
        
        outname=sprintf("phasespaces_initial_step_%d.dat",stepnum);
        fid=fopen(outname,"wt");
        fprintf(fid,"\# x xp y yp s de\n");
        outdata=[beamini(:,2),beamini(:,5),beamini(:,3),beamini(:,6),beamini(:,4),deeini]';
        fprintf(fid,"%20.12e %20.12e %20.12e %20.12e %20.12e %20.12e\n",outdata);
        fclose(fid);
        
        % track beam
        [emitt,beamfin]=placet_test_no_correction(beamlinename,beamname,"Zero");
        beamfin=units_placet_to_std(beamfin);
        deefin=beamfin(:,1)/mean(beamfin(:,1),1)-1;
        print_mean_rms(beamini);
        print_mean_rms(beamfin);
        save_mean_rms(beamfin,"stat.txt");
        
        outname=sprintf("phasespaces_final_step_%d.dat",stepnum);
        fid=fopen(outname,"wt");
        fprintf(fid,"\# x xp y yp s de\n");
        outdata=[beamfin(:,2),beamfin(:,5),beamfin(:,3),beamfin(:,6),beamfin(:,4),deefin]';
        fprintf(fid,"%20.12e %20.12e %20.12e %20.12e %20.12e %20.12e\n",outdata);
        fclose(fid);
        
        w=ones(length(beamfin),1);
        [xn,xpn,yn,ypn,sn,den,wn]=clear_outliers_6d(beamfin,w);
        deecore=den/mean(den,1)-1;
        outname=sprintf("phasespaces_final_step_%d_core.dat",stepnum);
        fid=fopen(outname,"wt");
        fprintf(fid,"\# x xp y yp s de\n");
        outdata=[xn,xpn,yn,ypn,sn,deecore]';
        fprintf(fid,"%20.12e %20.12e %20.12e %20.12e %20.12e %20.12e\n",outdata);
        fclose(fid);
        
        % save new beam params to file for later use in TCL
        outname=sprintf("tracking_beam_params.tmp");
        save_beam_params(beamfin,outname);
    }
    #TestNoCorrection -beam beam1 -survey none
    #TestQuadrupoleJitter -machines 100 -a0 0.0001 -a1 1.0 -steps 5 -beam beam1 -file emitt.dat
    #TestSimpleCorrectionDipole -machines 1 -binlength 100 -binoverlap 50 -bpm_resolution 0.1 -beam beam1 -testbeam beam1 -survey clic -emitt_fil emitt.dat
    
    #trajectorycorrection("rtml","beam1",10,"bpm.dat","dipole.dat","correction_statistics.dat");
    #[emitt,beamfin]=placet_test_no_correction("rtml","beam1","Zero");
    #beamfin=units_placet_to_std(beamfin);
    #deefin=beamfin(:,1)/mean(beamfin(:,1),1)-1;

    # end of tracking part
    
    # define the offset of the longitudinal coordinate z
    array set blinfo [BeamlineInfo]
    set deltaz $blinfo(length)
    set zoffset [expr $zoffset + $deltaz]
    set fid [open zoffset.dat a]
    set stmp [expr $stepnum+1]
    puts $fid "$stmp   $zoffset"
    close $fid
    
    # set new beamparams
    set fid [open tracking_beam_params.tmp r]
    gets $fid new_rmss
    gets $fid new_meanenergy
    gets $fid new_uncespread
    gets $fid new_echirp
    gets $fid new_betax
    gets $fid new_alphax
    gets $fid new_emitnx
    gets $fid new_betay
    gets $fid new_alphay
    gets $fid new_emitny
    close $fid
    
    set beamparams(sigmaz) [expr $new_rmss * $placetunits(xyz)]
    set beamparams(energy) [expr $new_meanenergy * $placetunits(energy)]
    set beamparams(uncespread) $new_uncespread
    set beamparams(echirp) [expr $new_echirp/$placetunits(energy)]
    set beamparams(betax) $new_betax
    set beamparams(alphax) $new_alphax
    set beamparams(emitnx) [expr $new_emitnx * $placetunits(emittance)]
    set beamparams(betay) $new_betay
    set beamparams(alphay) $new_alphay
    set beamparams(emitny) [expr $new_emitny * $placetunits(emittance)]
    
    set beamparams(charge) $beamparams(charge)
}
