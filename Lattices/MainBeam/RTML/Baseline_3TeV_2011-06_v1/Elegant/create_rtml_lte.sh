#!/bin/bash
#
beam="$1"
wakes="$2"
csr="$3"
isr="$4"

if [ "$beam" != "e+" ] ; then
beam="e-"
fi
if [ "$wakes" != "1" ] ; then
wakes="0"
fi
if [ "$csr" != "1" ] ; then
csr="0"
fi
if [ "$isr" != "1" ] ; then
isr="0"
fi

latticedir="./Lattices_${beam}"
latticefile="rtml_${beam}_wakes=${wakes}_csr=${csr}_isr=${isr}.lte"

cp $latticedir/0000_start.lte $latticefile
echo "
% $csr sto global_csr
% $isr sto global_isr
" >> $latticefile
echo -e "\n\n\n!----------------------------------------------------------------------\n\n\n" >> $latticefile
sed "s/XXXX/0010/g" $latticedir/0010_match_dr_to_rtml.lte >> $latticefile
echo -e "\n\n\n!----------------------------------------------------------------------\n\n\n" >> $latticefile
sed "s/XXXX/0020/g" $latticedir/0020_diagnostics_1.lte >> $latticefile
echo -e "\n\n\n!----------------------------------------------------------------------\n\n\n" >> $latticefile
sed "s/XXXX/0030/g" $latticedir/0030_dump_and_match_diag_to_sr.lte >> $latticefile
echo -e "\n\n\n!----------------------------------------------------------------------\n\n\n" >> $latticefile
sed "s/XXXX/0040/g" $latticedir/0040_spin_rotator.lte >> $latticefile
echo -e "\n\n\n!----------------------------------------------------------------------\n\n\n" >> $latticefile
sed "s/XXXX/0050/g" $latticedir/0050_match_sr_to_bc1_rf.lte >> $latticefile
echo -e "\n\n\n!----------------------------------------------------------------------\n\n\n" >> $latticefile
if [ $wakes == "1" ] ; then
sed "s/XXXX/0060/g" $latticedir/0060_bc1_rf_with_wakes.lte >> $latticefile
else
sed "s/XXXX/0060/g" $latticedir/0060_bc1_rf.lte >> $latticefile
fi
echo -e "\n\n\n!----------------------------------------------------------------------\n\n\n" >> $latticefile
sed "s/XXXX/0070/g" $latticedir/0070_match_bc1_rf_to_chicane.lte >> $latticefile
echo -e "\n\n\n!----------------------------------------------------------------------\n\n\n" >> $latticefile
sed "s/XXXX/0080/g" $latticedir/0080_bc1_chicane.lte >> $latticefile
echo -e "\n\n\n!----------------------------------------------------------------------\n\n\n" >> $latticefile
sed "s/XXXX/0090/g" $latticedir/0090_match_bc1_to_diag.lte >> $latticefile
echo -e "\n\n\n!----------------------------------------------------------------------\n\n\n" >> $latticefile
sed "s/XXXX/0100/g" $latticedir/0100_diagnostics_2.lte >> $latticefile
echo -e "\n\n\n!----------------------------------------------------------------------\n\n\n" >> $latticefile
sed "s/XXXX/0110/g" $latticedir/0110_dump_and_match_diag_to_booster.lte >> $latticefile
echo -e "\n\n\n!----------------------------------------------------------------------\n\n\n" >> $latticefile
if [ $wakes == "1" ]; then
sed "s/XXXX/0120/g" $latticedir/0120_booster_linac_with_wakes.lte >> $latticefile
else
sed "s/XXXX/0120/g" $latticedir/0120_booster_linac.lte >> $latticefile
fi
echo -e "\n\n\n!----------------------------------------------------------------------\n\n\n" >> $latticefile
sed "s/XXXX/0130/g" $latticedir/0130_dump_and_match_booster_to_ca.lte >> $latticefile
echo -e "\n\n\n!----------------------------------------------------------------------\n\n\n" >> $latticefile
sed "s/XXXX/0140/g" $latticedir/0140_central_arc.lte >> $latticefile
echo -e "\n\n\n!----------------------------------------------------------------------\n\n\n" >> $latticefile
sed "s/XXXX/0150/g" $latticedir/0150_vertical_transfer.lte >> $latticefile
echo -e "\n\n\n!----------------------------------------------------------------------\n\n\n" >> $latticefile
sed "s/XXXX/0160/g" $latticedir/0160_match_vt_to_ltl.lte >> $latticefile
echo -e "\n\n\n!----------------------------------------------------------------------\n\n\n" >> $latticefile
sed "s/XXXX/0170/g" $latticedir/0170_long_transfer_line.lte >> $latticefile
echo -e "\n\n\n!----------------------------------------------------------------------\n\n\n" >> $latticefile
sed "s/XXXX/0180/g" $latticedir/0180_dump_and_match_ltl_to_tal.lte >> $latticefile
echo -e "\n\n\n!----------------------------------------------------------------------\n\n\n" >> $latticefile
sed "s/XXXX/0190/g" $latticedir/0190_turn_around_loop.lte >> $latticefile
echo -e "\n\n\n!----------------------------------------------------------------------\n\n\n" >> $latticefile
sed "s/XXXX/0200/g" $latticedir/0200_match_tal_to_bc2_rf.lte >> $latticefile
echo -e "\n\n\n!----------------------------------------------------------------------\n\n\n" >> $latticefile
if [ $wakes == "1" ] ; then
sed "s/XXXX/0210/g" $latticedir/0210_bc2_rf_with_wakes.lte >> $latticefile
else
sed "s/XXXX/0210/g" $latticedir/0210_bc2_rf.lte >> $latticefile
fi
echo -e "\n\n\n!----------------------------------------------------------------------\n\n\n" >> $latticefile
sed "s/XXXX/0220/g" $latticedir/0220_match_bc2_rf_to_chicane_1.lte >> $latticefile
echo -e "\n\n\n!----------------------------------------------------------------------\n\n\n" >> $latticefile
sed "s/XXXX/0230/g" $latticedir/0230_bc2_chicane_1.lte >> $latticefile
echo -e "\n\n\n!----------------------------------------------------------------------\n\n\n" >> $latticefile
sed "s/XXXX/0240/g" $latticedir/0240_match_bc2_chicanes.lte >> $latticefile
echo -e "\n\n\n!----------------------------------------------------------------------\n\n\n" >> $latticefile
sed "s/XXXX/0250/g" $latticedir/0250_bc2_chicane_2.lte >> $latticefile
echo -e "\n\n\n!----------------------------------------------------------------------\n\n\n" >> $latticefile
sed "s/XXXX/0260/g" $latticedir/0260_match_bc2_to_diag.lte >> $latticefile
echo -e "\n\n\n!----------------------------------------------------------------------\n\n\n" >> $latticefile
sed "s/XXXX/0270/g" $latticedir/0270_diagnostics_3.lte >> $latticefile
echo -e "\n\n\n!----------------------------------------------------------------------\n\n\n" >> $latticefile
sed "s/XXXX/0280/g" $latticedir/0280_dump_and_match_rtml_to_main_linac.lte >> $latticefile
echo -e "\n\n\n!----------------------------------------------------------------------\n\n\n" >> $latticefile

echo "

% 0.65e-9 sto qbunch
% 100000 sto npart
% qbunch npart / sto qpart

Q0: CHARGE, TOTAL=\"qbunch\", PER_PARTICLE=\"qpart\"

RTML: LINE=(Q0,&
            LINE0010,LINE0020,LINE0030,LINE0040,LINE0050,LINE0060,&
            LINE0070,LINE0080,LINE0090,LINE0100,LINE0110,LINE0120,&
            LINE0130,LINE0140,LINE0150,LINE0160,LINE0170,LINE0180,&
            LINE0190,LINE0200,LINE0210,LINE0220,LINE0230,LINE0240,&
            LINE0250,LINE0260,LINE0270,LINE0280
           )

WW: WATCH, FILENAME=\"beam.sdds\"

RTMLT: LINE=(Q0,&
            LINE0010,LINE0020,LINE0030,LINE0040,LINE0050,WW,LINE0060,&
            LINE0070,LINE0080,LINE0090,LINE0100,LINE0110,LINE0120
            )
!RTMLT: LINE=(Q0,&
!            LINE0210,LINE0220,LINE0230,LINE0240,&
!            LINE0250,LINE0260,LINE0270,LINE0280)

RTMLE: LINE=(Q0,&
             LINE0010E,LINE0020E,LINE0030E,LINE0040E,LINE0050E,LINE0060E,&
             LINE0070E,LINE0080E,LINE0090E,LINE0100E,LINE0110E,LINE0120E,&
             LINE0130E,LINE0140E,LINE0150E,LINE0160E,LINE0170E,LINE0180E,&
             LINE0190E,LINE0200E,LINE0210E,LINE0220E,LINE0230E,LINE0240E,&
             LINE0250E,LINE0260E,LINE0270E,LINE0280E
            )

RTMLCSR: LINE=(Q0,&
               LINE0010CSR,LINE0020CSR,LINE0030CSR,LINE0040CSR,LINE0050CSR,LINE0060CSR,&
               LINE0070CSR,LINE0080CSR,LINE0090CSR,LINE0100CSR,LINE0110CSR,LINE0120CSR,&
               LINE0130CSR,LINE0140CSR,LINE0150CSR,LINE0160CSR,LINE0170CSR,LINE0180CSR,&
               LINE0190CSR,LINE0200CSR,LINE0210CSR,LINE0220CSR,LINE0230CSR,LINE0240CSR,&
               LINE0250CSR,LINE0260CSR,LINE0270CSR,LINE0280CSR
              )

" >> $latticefile
