#
# Dump and matching of diagnostics to booster linac
#
# beamparams=[betax,alphax,emitnx,betay,alphay,emitny,sigmaz,charge,
#             uncespread,echirp,energy,nslice,nmacro,nsigmabunch,nsigmawake]
# units=[m,rad,m*rad,m,rad,m*rad,m,C,1,1/m,eV,1,1,1,1]
#
# where necessary units were converted to placet units in main.tcl

proc lattice_dump_and_match_diag_to_booster {bparray} {
upvar $bparray beamparams

set usesixdim 1
set numthinlenses 100
set quad_synrad 0

set refenergy $beamparams(meanenergy)

set lquadm 0.3

set kqm1  0.6768080310
set kqm2 -0.7041803427
set kqm3  0.603093147
set kqm4 -0.1244938245
set kqm5 -0.511172383


set ldm1 1.45
set ldm2 2.70
set ldm3 4.05
set ldm4 1.35


SetReferenceEnergy $refenergy

Girder
Bpm -length 0.0
Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm1*$lquadm*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ldm1 -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm2*$lquadm*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ldm2 -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm3*$lquadm*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ldm3 -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm4*$lquadm*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ldm4 -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -synrad $quad_synrad -length [expr $lquadm] -strength [expr $kqm5*$lquadm*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
}
