#
# Certical transfer
# includes the necessary transverse offsets and arcs to geometrically match
# the central arc to the long transfer line
# the optics matching is done in the following section
#
# beamparams=[betax,alphax,emitnx,betay,alphay,emitny,sigmaz,charge,
#             uncespread,echirp,energy,nslice,nmacro,nsigmabunch,nsigmawake]
# units=[m,rad,m*rad,m,rad,m*rad,m,C,1,1/m,eV,1,1,1,1]
# where necessary units were converted to placet units in main.tcl

proc lattice_vertical_transfer {bparray} {
upvar $bparray beamparams

set usesynrad $beamparams(useisr)
set usesixdim 1
set numthinlenses 100
set quad_synrad 0

set pi 3.141592653589793
set c 299792458
set q0 1.6021765e-19 
set eps0 [expr 1/(4e-7*$pi*$c*$c)]

set refenergy $beamparams(meanenergy)

# match central arc to vertical arc
set lquadhv 0.3

set kqhv1 0.3443745591
set kqhv2 0.3674979027
set kqhv3 -1.280057996
set kqhv4 0.9689887412

set ldhv1 1.3
set ldhv2 5.0
set ldhv3 6.4
set ldhv4 1.6

SetReferenceEnergy $refenergy

Girder
Drift -length $ldhv1 -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -synrad $quad_synrad -length $lquadhv -strength [expr $kqhv1*$lquadhv*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ldhv2 -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -synrad $quad_synrad -length $lquadhv -strength [expr $kqhv2*$lquadhv*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ldhv3 -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -synrad $quad_synrad -length $lquadhv -strength [expr $kqhv3*$lquadhv*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ldhv4 -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -synrad $quad_synrad -length $lquadhv -strength [expr $kqhv4*$lquadhv*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses



#first vertical arc
set lbenda 1.5
set lquada 0.5
set lsexta 0.2

set theta [expr 1.0/180*$pi]
set rho [expr $lbenda/(2*sin($theta/2))]
set larc [expr $theta*$rho]

set ld01 0.96
set ld02 0.7
set ld03 4.0
set ld04 2.7
set ld05 1.0
set ld06 $ld05
set ld07 $ld04
set ld08 $ld03
set ld09 $ld02
set ld10 $ld01


set ld03a [expr $ld03-0.3]
set ld03b 0.1
set ld08a 0.1
set ld08b [expr $ld08-0.3]


set kq01 -1.430485608
set kq02 0.7270291277
set kq03 -0.9016579497
set kq04 0.2729845753
set kq05 $kq04
set kq06 $kq03
set kq07 $kq02
set kq08 $kq01


set ks01 [expr -63.0]
set ks02 [expr -63.0]

SetReferenceEnergy $refenergy

Girder
Drift -length $ld02 -six_dim $usesixdim
Sbend -length $larc -angle [expr $theta] -synrad $usesynrad -six_dim $usesixdim -thin_lens $numthinlenses -e0 $refenergy -E1 [expr $theta/2.0] -E2 [expr $theta/2.0] -tilt [expr $pi/2.0]
set g0 [expr $refenergy/0.000510999]
set de [expr 1/(6*$pi*$eps0)*$q0*$q0*$c*$g0*$g0*$g0*$g0/($rho*$rho)*$larc/$c]
set refenergy [expr $refenergy-$de/$q0/1e9*$usesynrad]
SetReferenceEnergy $refenergy
Drift -length $ld03a -six_dim $usesixdim
Multipole -type 3 -length $lsexta -e0 $refenergy -strength [expr $ks01*$refenergy*$lsexta] -thin_lens $numthinlenses -six_dim $usesixdim -tilt [expr $pi/2.0]
Drift -length $ld03b -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -synrad $quad_synrad -length $lquada -strength [expr $kq03*$lquada*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ld04 -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -synrad $quad_synrad -length $lquada -strength [expr $kq04*$lquada*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ld05 -six_dim $usesixdim
Sbend -length $larc -angle [expr $theta*2.0] -synrad $usesynrad -six_dim $usesixdim -thin_lens $numthinlenses -e0 $refenergy -E1 [expr $theta*2.0/2.0] -E2 [expr $theta*2.0/2.0] -tilt [expr $pi/2.0]
set g0 [expr $refenergy/0.000510999]
set de [expr 1/(6*$pi*$eps0)*$q0*$q0*$c*$g0*$g0*$g0*$g0/(($rho/2)*($rho/2))*$larc/$c]
set refenergy [expr $refenergy-$de/$q0/1e9*$usesynrad]
SetReferenceEnergy $refenergy
Drift -length $ld06 -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -synrad $quad_synrad -length $lquada -strength [expr $kq05*$lquada*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ld07 -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -synrad $quad_synrad -length $lquada -strength [expr $kq06*$lquada*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ld08a -six_dim $usesixdim
Multipole -type 3 -length $lsexta -e0 $refenergy -strength [expr $ks02*$refenergy*$lsexta] -thin_lens $numthinlenses -six_dim $usesixdim -tilt [expr $pi/2.0]
Drift -length $ld08b -six_dim $usesixdim
Sbend -length $larc -angle [expr $theta] -synrad $usesynrad -six_dim $usesixdim -thin_lens $numthinlenses -e0 $refenergy -E1 [expr $theta/2.0] -E2 [expr $theta/2.0] -tilt [expr $pi/2.0]
set g0 [expr $refenergy/0.000510999]
set de [expr 1/(6*$pi*$eps0)*$q0*$q0*$c*$g0*$g0*$g0*$g0/($rho*$rho)*$larc/$c]
set refenergy [expr $refenergy-$de/$q0/1e9*$usesynrad]
SetReferenceEnergy $refenergy
Drift -length $ld09 -six_dim $usesixdim

# match first vertical arc to fodo
set lquadm 0.3

set ldm1 4.0
set ldm2 11.0
set ldm3 7.5
set ldm4 5.0

set kqm1 0.511743716
set kqm2 -0.3371687368
set kqm3 -0.2472233781
set kqm4 0.1830131008

SetReferenceEnergy $refenergy

Girder
Bpm -length 0.0
Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm1*$lquadm*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ldm1 -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm2*$lquadm*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ldm2 -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm3*$lquadm*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ldm3 -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm4*$lquadm*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ldm4 -six_dim $usesixdim


# FODO
set lquadf 0.3
set kqf1 0.02619123669
set kqf2 -0.02619084535
set ldf 100.0

SetReferenceEnergy $refenergy

Girder
for {set cell 1} {$cell<=7} {incr cell} {
    Bpm -length 0.0
    Quadrupole -synrad $quad_synrad -length $lquadf -strength [expr $kqf1*$lquadf*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
    Drift -length $ldf -six_dim $usesixdim
    Bpm -length 0.0
    Quadrupole -synrad $quad_synrad -length $lquadf -strength [expr $kqf2*$lquadf*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
    Drift -length $ldf -six_dim $usesixdim
}
Bpm -length 0.0
Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqf1*$lquadm*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses


# match fodo to second vertical arc

set lquadm 0.3

set ldm1 4.0
set ldm2 11.0
set ldm3 7.5
set ldm4 5.0

set kqm1 0.511743716
set kqm2 -0.3371687368
set kqm3 -0.2472233781
set kqm4 0.1830131008

SetReferenceEnergy $refenergy

Girder
Drift -length $ldm4 -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm4*$lquadm*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ldm3 -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm3*$lquadm*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ldm2 -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm2*$lquadm*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ldm1 -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm1*$lquadm*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses


#second vertical arc
set lbenda 1.5
set lquada 0.5
set lsexta 0.2

set theta [expr 1.0/180*$pi]
set rho [expr $lbenda/(2*sin($theta/2))]
set larc [expr $theta*$rho]

set ld01 0.96
set ld02 0.7
set ld03 4.0
set ld04 2.7
set ld05 1.0
set ld06 $ld05
set ld07 $ld04
set ld08 $ld03
set ld09 $ld02
set ld10 $ld01


set ld03a [expr $ld03-0.3]
set ld03b 0.1
set ld08a 0.1
set ld08b [expr $ld08-0.3]


set kq01 -1.430485608
set kq02 0.7270291277
set kq03 -0.9016579497
set kq04 0.2729845753
set kq05 $kq04
set kq06 $kq03
set kq07 $kq02
set kq08 $kq01


set ks01 [expr 63.0]
set ks02 [expr 63.0]

SetReferenceEnergy $refenergy

Girder
Drift -length $ld02 -six_dim $usesixdim
Sbend -length $larc -angle [expr -$theta] -synrad $usesynrad -six_dim $usesixdim -thin_lens $numthinlenses -e0 $refenergy -E1 [expr -$theta/2.0] -E2 [expr -$theta/2.0] -tilt [expr $pi/2.0]
set g0 [expr $refenergy/0.000510999]
set de [expr 1/(6*$pi*$eps0)*$q0*$q0*$c*$g0*$g0*$g0*$g0/($rho*$rho)*$larc/$c]
set refenergy [expr $refenergy-$de/$q0/1e9*$usesynrad]
SetReferenceEnergy $refenergy
Drift -length $ld03a -six_dim $usesixdim
Multipole -type 3 -length $lsexta -e0 $refenergy -strength [expr $ks01*$refenergy*$lsexta] -thin_lens $numthinlenses -six_dim $usesixdim -tilt [expr $pi/2.0]
Drift -length $ld03b -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -synrad $quad_synrad -length $lquada -strength [expr $kq03*$lquada*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ld04 -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -synrad $quad_synrad -length $lquada -strength [expr $kq04*$lquada*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ld05 -six_dim $usesixdim
Sbend -length $larc -angle [expr -$theta*2.0] -synrad $usesynrad -six_dim $usesixdim -thin_lens $numthinlenses -e0 $refenergy -E1 [expr -$theta*2.0/2.0] -E2 [expr -$theta*2.0/2.0] -tilt [expr $pi/2.0]
set g0 [expr $refenergy/0.000510999]
set de [expr 1/(6*$pi*$eps0)*$q0*$q0*$c*$g0*$g0*$g0*$g0/(($rho/2)*($rho/2))*$larc/$c]
set refenergy [expr $refenergy-$de/$q0/1e9*$usesynrad]
SetReferenceEnergy $refenergy
Drift -length $ld06 -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -synrad $quad_synrad -length $lquada -strength [expr $kq05*$lquada*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ld07 -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -synrad $quad_synrad -length $lquada -strength [expr $kq06*$lquada*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ld08a -six_dim $usesixdim
Multipole -type 3 -length $lsexta -e0 $refenergy -strength [expr $ks02*$refenergy*$lsexta] -thin_lens $numthinlenses -six_dim $usesixdim -tilt [expr $pi/2.0]
Drift -length $ld08b -six_dim $usesixdim
Sbend -length $larc -angle [expr -$theta] -synrad $usesynrad -six_dim $usesixdim -thin_lens $numthinlenses -e0 $refenergy -E1 [expr -$theta/2.0] -E2 [expr -$theta/2.0] -tilt [expr $pi/2.0]
set g0 [expr $refenergy/0.000510999]
set de [expr 1/(6*$pi*$eps0)*$q0*$q0*$c*$g0*$g0*$g0*$g0/($rho*$rho)*$larc/$c]
set refenergy [expr $refenergy-$de/$q0/1e9*$usesynrad]
SetReferenceEnergy $refenergy
Drift -length $ld09 -six_dim $usesixdim

set beamparams(meanenergy) $refenergy
puts "Setting beamparams(meanenergy)=$beamparams(meanenergy)."
}

