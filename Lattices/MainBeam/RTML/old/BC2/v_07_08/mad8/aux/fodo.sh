#!/bin/bash

for section in `seq 1 4`; do
  for subsection in `seq -f "%02g" 1 16`; do

cat << EOF
! ***** section $section-$subsection

 AC${section}${subsection} :  CAVC,  L=LACC, DELTAE=GECAV*LACC;

 DR${section}${subsection}1 :  DRIFT, L=LDMID;
 QF${section}${subsection}1 :  QUADRUPOLE, L=LQS2, k1=KQSLE; ! QS quadrupole
 DR${section}${subsection}2 :  DRIFT, L=LDMID;
 QD${section}${subsection}2 :  QUADRUPOLE, L=LQS2, k1=-KQSLM; ! QS quadrupole
 DR${section}${subsection}3 :  DRIFT, L=LDMID;
 QF${section}${subsection}3 :  QUADRUPOLE, L=LQS2, k1=KQSLE; ! QS quadrupole
 DR${section}${subsection}4 :  DRIFT, L=LDMID;
 
 SEC${section}${subsection} : LINE = (AC${section}${subsection}, DR${section}${subsection}1, &
  QF${section}${subsection}1, DR${section}${subsection}2, &
  QD${section}${subsection}2, DR${section}${subsection}3, &
  QF${section}${subsection}3, DR${section}${subsection}4)

EOF

done

cat << EOF
 LINAC${section} : LINE = (SEC${section}01,SEC${section}02,SEC${section}03,SEC${section}04,SEC${section}05, &
                  SEC${section}06,SEC${section}07,SEC${section}08,SEC${section}09,SEC${section}10, &
                  SEC${section}11,SEC${section}12,SEC${section}13,SEC${section}14,SEC${section}15, SEC${section}16)

EOF

done
