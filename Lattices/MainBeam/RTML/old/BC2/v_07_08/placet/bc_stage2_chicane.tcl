# the command line was:
#
# /home/clic/afs/placet/tools/xsif2placet/xsif2placet --deck bc2.mad -particles=2.56e9 -energy0=9
#
Begin6d
SetReferenceEnergy 9
Drift -length 1
Sbend -synrad 1 -length 2.000133 -e0 9.0 -angle 0.01999696 -E2 0.01999696
Drift -length 15.50310
Sbend -synrad 1 -length 2.000133 -e0 9.0 -angle -0.01999696 -E1 -0.01999696
Drift -length 1
Sbend -synrad 1 -length 2.000133 -e0 9.0 -angle -0.01999696 -E2 -0.01999696
Drift -length 15.50310
Sbend -synrad 1 -length 2.000133 -e0 9.0 -angle 0.01999696 -E1 0.01999696
End6d
