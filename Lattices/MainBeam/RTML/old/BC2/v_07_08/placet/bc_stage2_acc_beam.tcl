set match(emitt_x) 4.32
set match(emitt_y) 0.043
set match(alpha_x) 0.0
set match(alpha_y) 0.0
set match(beta_x) 40
set match(beta_y) 40
set match(e_spread) 0.3232
set match(charge) 4.4e9
set charge $match(charge)
set match(sigma_z) 175.0
set match(phase) 0.0

set charge $match(charge)
