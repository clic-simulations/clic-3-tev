Drift -name LINAC1 -length 0.00000
Drift -name SEC101 -length 0.00000
Cavity -6d 1 -length 1.0 -gradient $gradient -phase 90 -type 0 -name AC101 -length 0.00000
Drift -name DR1011 -length 0.600000
Quadrupole -name QF1011 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR1012 -length 0.600000
Quadrupole -name QD1012 -synrad $quad_synrad -length 0.360000 -strength [expr -0.846389E-01*$e0] -roll 0.00000
Drift -name DR1013 -length 0.600000
Quadrupole -name QF1013 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR1014 -length 0.600000
Drift -name SEC101 -length 0.00000
Drift -name SEC102 -length 0.00000
Cavity -6d 1 -length 1.0 -gradient $gradient -phase 90 -type 0 -name AC102 -length 0.00000
Drift -name DR1021 -length 0.600000
Quadrupole -name QF1021 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR1022 -length 0.600000
Quadrupole -name QD1022 -synrad $quad_synrad -length 0.360000 -strength [expr -0.846389E-01*$e0] -roll 0.00000
Drift -name DR1023 -length 0.600000
Quadrupole -name QF1023 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR1024 -length 0.600000
Drift -name SEC102 -length 0.00000
Drift -name SEC103 -length 0.00000
Cavity -6d 1 -length 1.0 -gradient $gradient -phase 90 -type 0 -name AC103 -length 0.00000
Drift -name DR1031 -length 0.600000
Quadrupole -name QF1031 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR1032 -length 0.600000
Quadrupole -name QD1032 -synrad $quad_synrad -length 0.360000 -strength [expr -0.846389E-01*$e0] -roll 0.00000
Drift -name DR1033 -length 0.600000
Quadrupole -name QF1033 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR1034 -length 0.600000
Drift -name SEC103 -length 0.00000
Drift -name SEC104 -length 0.00000
Cavity -6d 1 -length 1.0 -gradient $gradient -phase 90 -type 0 -name AC104 -length 0.00000
Drift -name DR1041 -length 0.600000
Quadrupole -name QF1041 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR1042 -length 0.600000
Quadrupole -name QD1042 -synrad $quad_synrad -length 0.360000 -strength [expr -0.846389E-01*$e0] -roll 0.00000
Drift -name DR1043 -length 0.600000
Quadrupole -name QF1043 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR1044 -length 0.600000
Drift -name SEC104 -length 0.00000
Drift -name SEC105 -length 0.00000
Cavity -6d 1 -length 1.0 -gradient $gradient -phase 90 -type 0 -name AC105 -length 0.00000
Drift -name DR1051 -length 0.600000
Quadrupole -name QF1051 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR1052 -length 0.600000
Quadrupole -name QD1052 -synrad $quad_synrad -length 0.360000 -strength [expr -0.846389E-01*$e0] -roll 0.00000
Drift -name DR1053 -length 0.600000
Quadrupole -name QF1053 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR1054 -length 0.600000
Drift -name SEC105 -length 0.00000
Drift -name SEC106 -length 0.00000
Cavity -6d 1 -length 1.0 -gradient $gradient -phase 90 -type 0 -name AC106 -length 0.00000
Drift -name DR1061 -length 0.600000
Quadrupole -name QF1061 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR1062 -length 0.600000
Quadrupole -name QD1062 -synrad $quad_synrad -length 0.360000 -strength [expr -0.846389E-01*$e0] -roll 0.00000
Drift -name DR1063 -length 0.600000
Quadrupole -name QF1063 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR1064 -length 0.600000
Drift -name SEC106 -length 0.00000
Drift -name SEC107 -length 0.00000
Cavity -6d 1 -length 1.0 -gradient $gradient -phase 90 -type 0 -name AC107 -length 0.00000
Drift -name DR1071 -length 0.600000
Quadrupole -name QF1071 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR1072 -length 0.600000
Quadrupole -name QD1072 -synrad $quad_synrad -length 0.360000 -strength [expr -0.846389E-01*$e0] -roll 0.00000
Drift -name DR1073 -length 0.600000
Quadrupole -name QF1073 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR1074 -length 0.600000
Drift -name SEC107 -length 0.00000
Drift -name SEC108 -length 0.00000
Cavity -6d 1 -length 1.0 -gradient $gradient -phase 90 -type 0 -name AC108 -length 0.00000
Drift -name DR1081 -length 0.600000
Quadrupole -name QF1081 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR1082 -length 0.600000
Quadrupole -name QD1082 -synrad $quad_synrad -length 0.360000 -strength [expr -0.846389E-01*$e0] -roll 0.00000
Drift -name DR1083 -length 0.600000
Quadrupole -name QF1083 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR1084 -length 0.600000
Drift -name SEC108 -length 0.00000
Drift -name SEC109 -length 0.00000
Cavity -6d 1 -length 1.0 -gradient $gradient -phase 90 -type 0 -name AC109 -length 0.00000
Drift -name DR1091 -length 0.600000
Quadrupole -name QF1091 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR1092 -length 0.600000
Quadrupole -name QD1092 -synrad $quad_synrad -length 0.360000 -strength [expr -0.846389E-01*$e0] -roll 0.00000
Drift -name DR1093 -length 0.600000
Quadrupole -name QF1093 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR1094 -length 0.600000
Drift -name SEC109 -length 0.00000
Drift -name SEC110 -length 0.00000
Cavity -6d 1 -length 1.0 -gradient $gradient -phase 90 -type 0 -name AC110 -length 0.00000
Drift -name DR1101 -length 0.600000
Quadrupole -name QF1101 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR1102 -length 0.600000
Quadrupole -name QD1102 -synrad $quad_synrad -length 0.360000 -strength [expr -0.846389E-01*$e0] -roll 0.00000
Drift -name DR1103 -length 0.600000
Quadrupole -name QF1103 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR1104 -length 0.600000
Drift -name SEC110 -length 0.00000
Drift -name SEC111 -length 0.00000
Cavity -6d 1 -length 1.0 -gradient $gradient -phase 90 -type 0 -name AC111 -length 0.00000
Drift -name DR1111 -length 0.600000
Quadrupole -name QF1111 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR1112 -length 0.600000
Quadrupole -name QD1112 -synrad $quad_synrad -length 0.360000 -strength [expr -0.846389E-01*$e0] -roll 0.00000
Drift -name DR1113 -length 0.600000
Quadrupole -name QF1113 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR1114 -length 0.600000
Drift -name SEC111 -length 0.00000
Drift -name SEC112 -length 0.00000
Cavity -6d 1 -length 1.0 -gradient $gradient -phase 90 -type 0 -name AC112 -length 0.00000
Drift -name DR1121 -length 0.600000
Quadrupole -name QF1121 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR1122 -length 0.600000
Quadrupole -name QD1122 -synrad $quad_synrad -length 0.360000 -strength [expr -0.846389E-01*$e0] -roll 0.00000
Drift -name DR1123 -length 0.600000
Quadrupole -name QF1123 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR1124 -length 0.600000
Drift -name SEC112 -length 0.00000
Drift -name SEC113 -length 0.00000
Cavity -6d 1 -length 1.0 -gradient $gradient -phase 90 -type 0 -name AC113 -length 0.00000
Drift -name DR1131 -length 0.600000
Quadrupole -name QF1131 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR1132 -length 0.600000
Quadrupole -name QD1132 -synrad $quad_synrad -length 0.360000 -strength [expr -0.846389E-01*$e0] -roll 0.00000
Drift -name DR1133 -length 0.600000
Quadrupole -name QF1133 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR1134 -length 0.600000
Drift -name SEC113 -length 0.00000
Drift -name SEC114 -length 0.00000
Cavity -6d 1 -length 1.0 -gradient $gradient -phase 90 -type 0 -name AC114 -length 0.00000
Drift -name DR1141 -length 0.600000
Quadrupole -name QF1141 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR1142 -length 0.600000
Quadrupole -name QD1142 -synrad $quad_synrad -length 0.360000 -strength [expr -0.846389E-01*$e0] -roll 0.00000
Drift -name DR1143 -length 0.600000
Quadrupole -name QF1143 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR1144 -length 0.600000
Drift -name SEC114 -length 0.00000
Drift -name SEC115 -length 0.00000
Cavity -6d 1 -length 1.0 -gradient $gradient -phase 90 -type 0 -name AC115 -length 0.00000
Drift -name DR1151 -length 0.600000
Quadrupole -name QF1151 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR1152 -length 0.600000
Quadrupole -name QD1152 -synrad $quad_synrad -length 0.360000 -strength [expr -0.846389E-01*$e0] -roll 0.00000
Drift -name DR1153 -length 0.600000
Quadrupole -name QF1153 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR1154 -length 0.600000
Drift -name SEC115 -length 0.00000
Drift -name SEC116 -length 0.00000
Cavity -6d 1 -length 1.0 -gradient $gradient -phase 90 -type 0 -name AC116 -length 0.00000
Drift -name DR1161 -length 0.600000
Quadrupole -name QF1161 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR1162 -length 0.600000
Quadrupole -name QD1162 -synrad $quad_synrad -length 0.360000 -strength [expr -0.846389E-01*$e0] -roll 0.00000
Drift -name DR1163 -length 0.600000
Quadrupole -name QF1163 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR1164 -length 0.600000
Drift -name SEC116 -length 0.00000
Drift -name LINAC1 -length 0.00000
Drift -name LINAC2 -length 0.00000
Drift -name SEC201 -length 0.00000
Cavity -6d 1 -length 1.0 -gradient $gradient -phase 90 -type 0 -name AC201 -length 0.00000
Drift -name DR2011 -length 0.600000
Quadrupole -name QF2011 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR2012 -length 0.600000
Quadrupole -name QD2012 -synrad $quad_synrad -length 0.360000 -strength [expr -0.846389E-01*$e0] -roll 0.00000
Drift -name DR2013 -length 0.600000
Quadrupole -name QF2013 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR2014 -length 0.600000
Drift -name SEC201 -length 0.00000
Drift -name SEC202 -length 0.00000
Cavity -6d 1 -length 1.0 -gradient $gradient -phase 90 -type 0 -name AC202 -length 0.00000
Drift -name DR2021 -length 0.600000
Quadrupole -name QF2021 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR2022 -length 0.600000
Quadrupole -name QD2022 -synrad $quad_synrad -length 0.360000 -strength [expr -0.846389E-01*$e0] -roll 0.00000
Drift -name DR2023 -length 0.600000
Quadrupole -name QF2023 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR2024 -length 0.600000
Drift -name SEC202 -length 0.00000
Drift -name SEC203 -length 0.00000
Cavity -6d 1 -length 1.0 -gradient $gradient -phase 90 -type 0 -name AC203 -length 0.00000
Drift -name DR2031 -length 0.600000
Quadrupole -name QF2031 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR2032 -length 0.600000
Quadrupole -name QD2032 -synrad $quad_synrad -length 0.360000 -strength [expr -0.846389E-01*$e0] -roll 0.00000
Drift -name DR2033 -length 0.600000
Quadrupole -name QF2033 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR2034 -length 0.600000
Drift -name SEC203 -length 0.00000
Drift -name SEC204 -length 0.00000
Cavity -6d 1 -length 1.0 -gradient $gradient -phase 90 -type 0 -name AC204 -length 0.00000
Drift -name DR2041 -length 0.600000
Quadrupole -name QF2041 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR2042 -length 0.600000
Quadrupole -name QD2042 -synrad $quad_synrad -length 0.360000 -strength [expr -0.846389E-01*$e0] -roll 0.00000
Drift -name DR2043 -length 0.600000
Quadrupole -name QF2043 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR2044 -length 0.600000
Drift -name SEC204 -length 0.00000
Drift -name SEC205 -length 0.00000
Cavity -6d 1 -length 1.0 -gradient $gradient -phase 90 -type 0 -name AC205 -length 0.00000
Drift -name DR2051 -length 0.600000
Quadrupole -name QF2051 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR2052 -length 0.600000
Quadrupole -name QD2052 -synrad $quad_synrad -length 0.360000 -strength [expr -0.846389E-01*$e0] -roll 0.00000
Drift -name DR2053 -length 0.600000
Quadrupole -name QF2053 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR2054 -length 0.600000
Drift -name SEC205 -length 0.00000
Drift -name SEC206 -length 0.00000
Cavity -6d 1 -length 1.0 -gradient $gradient -phase 90 -type 0 -name AC206 -length 0.00000
Drift -name DR2061 -length 0.600000
Quadrupole -name QF2061 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR2062 -length 0.600000
Quadrupole -name QD2062 -synrad $quad_synrad -length 0.360000 -strength [expr -0.846389E-01*$e0] -roll 0.00000
Drift -name DR2063 -length 0.600000
Quadrupole -name QF2063 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR2064 -length 0.600000
Drift -name SEC206 -length 0.00000
Drift -name SEC207 -length 0.00000
Cavity -6d 1 -length 1.0 -gradient $gradient -phase 90 -type 0 -name AC207 -length 0.00000
Drift -name DR2071 -length 0.600000
Quadrupole -name QF2071 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR2072 -length 0.600000
Quadrupole -name QD2072 -synrad $quad_synrad -length 0.360000 -strength [expr -0.846389E-01*$e0] -roll 0.00000
Drift -name DR2073 -length 0.600000
Quadrupole -name QF2073 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR2074 -length 0.600000
Drift -name SEC207 -length 0.00000
Drift -name SEC208 -length 0.00000
Cavity -6d 1 -length 1.0 -gradient $gradient -phase 90 -type 0 -name AC208 -length 0.00000
Drift -name DR2081 -length 0.600000
Quadrupole -name QF2081 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR2082 -length 0.600000
Quadrupole -name QD2082 -synrad $quad_synrad -length 0.360000 -strength [expr -0.846389E-01*$e0] -roll 0.00000
Drift -name DR2083 -length 0.600000
Quadrupole -name QF2083 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR2084 -length 0.600000
Drift -name SEC208 -length 0.00000
Drift -name SEC209 -length 0.00000
Cavity -6d 1 -length 1.0 -gradient $gradient -phase 90 -type 0 -name AC209 -length 0.00000
Drift -name DR2091 -length 0.600000
Quadrupole -name QF2091 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR2092 -length 0.600000
Quadrupole -name QD2092 -synrad $quad_synrad -length 0.360000 -strength [expr -0.846389E-01*$e0] -roll 0.00000
Drift -name DR2093 -length 0.600000
Quadrupole -name QF2093 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR2094 -length 0.600000
Drift -name SEC209 -length 0.00000
Drift -name SEC210 -length 0.00000
Cavity -6d 1 -length 1.0 -gradient $gradient -phase 90 -type 0 -name AC210 -length 0.00000
Drift -name DR2101 -length 0.600000
Quadrupole -name QF2101 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR2102 -length 0.600000
Quadrupole -name QD2102 -synrad $quad_synrad -length 0.360000 -strength [expr -0.846389E-01*$e0] -roll 0.00000
Drift -name DR2103 -length 0.600000
Quadrupole -name QF2103 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR2104 -length 0.600000
Drift -name SEC210 -length 0.00000
Drift -name SEC211 -length 0.00000
Cavity -6d 1 -length 1.0 -gradient $gradient -phase 90 -type 0 -name AC211 -length 0.00000
Drift -name DR2111 -length 0.600000
Quadrupole -name QF2111 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR2112 -length 0.600000
Quadrupole -name QD2112 -synrad $quad_synrad -length 0.360000 -strength [expr -0.846389E-01*$e0] -roll 0.00000
Drift -name DR2113 -length 0.600000
Quadrupole -name QF2113 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR2114 -length 0.600000
Drift -name SEC211 -length 0.00000
Drift -name SEC212 -length 0.00000
Cavity -6d 1 -length 1.0 -gradient $gradient -phase 90 -type 0 -name AC212 -length 0.00000
Drift -name DR2121 -length 0.600000
Quadrupole -name QF2121 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR2122 -length 0.600000
Quadrupole -name QD2122 -synrad $quad_synrad -length 0.360000 -strength [expr -0.846389E-01*$e0] -roll 0.00000
Drift -name DR2123 -length 0.600000
Quadrupole -name QF2123 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR2124 -length 0.600000
Drift -name SEC212 -length 0.00000
Drift -name SEC213 -length 0.00000
Cavity -6d 1 -length 1.0 -gradient $gradient -phase 90 -type 0 -name AC213 -length 0.00000
Drift -name DR2131 -length 0.600000
Quadrupole -name QF2131 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR2132 -length 0.600000
Quadrupole -name QD2132 -synrad $quad_synrad -length 0.360000 -strength [expr -0.846389E-01*$e0] -roll 0.00000
Drift -name DR2133 -length 0.600000
Quadrupole -name QF2133 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR2134 -length 0.600000
Drift -name SEC213 -length 0.00000
Drift -name SEC214 -length 0.00000
Cavity -6d 1 -length 1.0 -gradient $gradient -phase 90 -type 0 -name AC214 -length 0.00000
Drift -name DR2141 -length 0.600000
Quadrupole -name QF2141 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR2142 -length 0.600000
Quadrupole -name QD2142 -synrad $quad_synrad -length 0.360000 -strength [expr -0.846389E-01*$e0] -roll 0.00000
Drift -name DR2143 -length 0.600000
Quadrupole -name QF2143 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR2144 -length 0.600000
Drift -name SEC214 -length 0.00000
Drift -name SEC215 -length 0.00000
Cavity -6d 1 -length 1.0 -gradient $gradient -phase 90 -type 0 -name AC215 -length 0.00000
Drift -name DR2151 -length 0.600000
Quadrupole -name QF2151 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR2152 -length 0.600000
Quadrupole -name QD2152 -synrad $quad_synrad -length 0.360000 -strength [expr -0.846389E-01*$e0] -roll 0.00000
Drift -name DR2153 -length 0.600000
Quadrupole -name QF2153 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR2154 -length 0.600000
Drift -name SEC215 -length 0.00000
Drift -name SEC216 -length 0.00000
Cavity -6d 1 -length 1.0 -gradient $gradient -phase 90 -type 0 -name AC216 -length 0.00000
Drift -name DR2161 -length 0.600000
Quadrupole -name QF2161 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR2162 -length 0.600000
Quadrupole -name QD2162 -synrad $quad_synrad -length 0.360000 -strength [expr -0.846389E-01*$e0] -roll 0.00000
Drift -name DR2163 -length 0.600000
Quadrupole -name QF2163 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR2164 -length 0.600000
Drift -name SEC216 -length 0.00000
Drift -name LINAC2 -length 0.00000
Drift -name LINAC3 -length 0.00000
Drift -name SEC301 -length 0.00000
Cavity -6d 1 -length 1.0 -gradient $gradient -phase 90 -type 0 -name AC301 -length 0.00000
Drift -name DR3011 -length 0.600000
Quadrupole -name QF3011 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR3012 -length 0.600000
Quadrupole -name QD3012 -synrad $quad_synrad -length 0.360000 -strength [expr -0.846389E-01*$e0] -roll 0.00000
Drift -name DR3013 -length 0.600000
Quadrupole -name QF3013 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR3014 -length 0.600000
Drift -name SEC301 -length 0.00000
Drift -name SEC302 -length 0.00000
Cavity -6d 1 -length 1.0 -gradient $gradient -phase 90 -type 0 -name AC302 -length 0.00000
Drift -name DR3021 -length 0.600000
Quadrupole -name QF3021 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR3022 -length 0.600000
Quadrupole -name QD3022 -synrad $quad_synrad -length 0.360000 -strength [expr -0.846389E-01*$e0] -roll 0.00000
Drift -name DR3023 -length 0.600000
Quadrupole -name QF3023 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR3024 -length 0.600000
Drift -name SEC302 -length 0.00000
Drift -name SEC303 -length 0.00000
Cavity -6d 1 -length 1.0 -gradient $gradient -phase 90 -type 0 -name AC303 -length 0.00000
Drift -name DR3031 -length 0.600000
Quadrupole -name QF3031 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR3032 -length 0.600000
Quadrupole -name QD3032 -synrad $quad_synrad -length 0.360000 -strength [expr -0.846389E-01*$e0] -roll 0.00000
Drift -name DR3033 -length 0.600000
Quadrupole -name QF3033 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR3034 -length 0.600000
Drift -name SEC303 -length 0.00000
Drift -name SEC304 -length 0.00000
Cavity -6d 1 -length 1.0 -gradient $gradient -phase 90 -type 0 -name AC304 -length 0.00000
Drift -name DR3041 -length 0.600000
Quadrupole -name QF3041 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR3042 -length 0.600000
Quadrupole -name QD3042 -synrad $quad_synrad -length 0.360000 -strength [expr -0.846389E-01*$e0] -roll 0.00000
Drift -name DR3043 -length 0.600000
Quadrupole -name QF3043 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR3044 -length 0.600000
Drift -name SEC304 -length 0.00000
Drift -name SEC305 -length 0.00000
Cavity -6d 1 -length 1.0 -gradient $gradient -phase 90 -type 0 -name AC305 -length 0.00000
Drift -name DR3051 -length 0.600000
Quadrupole -name QF3051 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR3052 -length 0.600000
Quadrupole -name QD3052 -synrad $quad_synrad -length 0.360000 -strength [expr -0.846389E-01*$e0] -roll 0.00000
Drift -name DR3053 -length 0.600000
Quadrupole -name QF3053 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR3054 -length 0.600000
Drift -name SEC305 -length 0.00000
Drift -name SEC306 -length 0.00000
Cavity -6d 1 -length 1.0 -gradient $gradient -phase 90 -type 0 -name AC306 -length 0.00000
Drift -name DR3061 -length 0.600000
Quadrupole -name QF3061 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR3062 -length 0.600000
Quadrupole -name QD3062 -synrad $quad_synrad -length 0.360000 -strength [expr -0.846389E-01*$e0] -roll 0.00000
Drift -name DR3063 -length 0.600000
Quadrupole -name QF3063 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR3064 -length 0.600000
Drift -name SEC306 -length 0.00000
Drift -name SEC307 -length 0.00000
Cavity -6d 1 -length 1.0 -gradient $gradient -phase 90 -type 0 -name AC307 -length 0.00000
Drift -name DR3071 -length 0.600000
Quadrupole -name QF3071 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR3072 -length 0.600000
Quadrupole -name QD3072 -synrad $quad_synrad -length 0.360000 -strength [expr -0.846389E-01*$e0] -roll 0.00000
Drift -name DR3073 -length 0.600000
Quadrupole -name QF3073 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR3074 -length 0.600000
Drift -name SEC307 -length 0.00000
Drift -name SEC308 -length 0.00000
Cavity -6d 1 -length 1.0 -gradient $gradient -phase 90 -type 0 -name AC308 -length 0.00000
Drift -name DR3081 -length 0.600000
Quadrupole -name QF3081 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR3082 -length 0.600000
Quadrupole -name QD3082 -synrad $quad_synrad -length 0.360000 -strength [expr -0.846389E-01*$e0] -roll 0.00000
Drift -name DR3083 -length 0.600000
Quadrupole -name QF3083 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR3084 -length 0.600000
Drift -name SEC308 -length 0.00000
Drift -name SEC309 -length 0.00000
Cavity -6d 1 -length 1.0 -gradient $gradient -phase 90 -type 0 -name AC309 -length 0.00000
Drift -name DR3091 -length 0.600000
Quadrupole -name QF3091 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR3092 -length 0.600000
Quadrupole -name QD3092 -synrad $quad_synrad -length 0.360000 -strength [expr -0.846389E-01*$e0] -roll 0.00000
Drift -name DR3093 -length 0.600000
Quadrupole -name QF3093 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR3094 -length 0.600000
Drift -name SEC309 -length 0.00000
Drift -name SEC310 -length 0.00000
Cavity -6d 1 -length 1.0 -gradient $gradient -phase 90 -type 0 -name AC310 -length 0.00000
Drift -name DR3101 -length 0.600000
Quadrupole -name QF3101 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR3102 -length 0.600000
Quadrupole -name QD3102 -synrad $quad_synrad -length 0.360000 -strength [expr -0.846389E-01*$e0] -roll 0.00000
Drift -name DR3103 -length 0.600000
Quadrupole -name QF3103 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR3104 -length 0.600000
Drift -name SEC310 -length 0.00000
Drift -name SEC311 -length 0.00000
Cavity -6d 1 -length 1.0 -gradient $gradient -phase 90 -type 0 -name AC311 -length 0.00000
Drift -name DR3111 -length 0.600000
Quadrupole -name QF3111 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR3112 -length 0.600000
Quadrupole -name QD3112 -synrad $quad_synrad -length 0.360000 -strength [expr -0.846389E-01*$e0] -roll 0.00000
Drift -name DR3113 -length 0.600000
Quadrupole -name QF3113 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR3114 -length 0.600000
Drift -name SEC311 -length 0.00000
Drift -name SEC312 -length 0.00000
Cavity -6d 1 -length 1.0 -gradient $gradient -phase 90 -type 0 -name AC312 -length 0.00000
Drift -name DR3121 -length 0.600000
Quadrupole -name QF3121 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR3122 -length 0.600000
Quadrupole -name QD3122 -synrad $quad_synrad -length 0.360000 -strength [expr -0.846389E-01*$e0] -roll 0.00000
Drift -name DR3123 -length 0.600000
Quadrupole -name QF3123 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR3124 -length 0.600000
Drift -name SEC312 -length 0.00000
Drift -name SEC313 -length 0.00000
Cavity -6d 1 -length 1.0 -gradient $gradient -phase 90 -type 0 -name AC313 -length 0.00000
Drift -name DR3131 -length 0.600000
Quadrupole -name QF3131 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR3132 -length 0.600000
Quadrupole -name QD3132 -synrad $quad_synrad -length 0.360000 -strength [expr -0.846389E-01*$e0] -roll 0.00000
Drift -name DR3133 -length 0.600000
Quadrupole -name QF3133 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR3134 -length 0.600000
Drift -name SEC313 -length 0.00000
Drift -name SEC314 -length 0.00000
Cavity -6d 1 -length 1.0 -gradient $gradient -phase 90 -type 0 -name AC314 -length 0.00000
Drift -name DR3141 -length 0.600000
Quadrupole -name QF3141 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR3142 -length 0.600000
Quadrupole -name QD3142 -synrad $quad_synrad -length 0.360000 -strength [expr -0.846389E-01*$e0] -roll 0.00000
Drift -name DR3143 -length 0.600000
Quadrupole -name QF3143 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR3144 -length 0.600000
Drift -name SEC314 -length 0.00000
Drift -name SEC315 -length 0.00000
Cavity -6d 1 -length 1.0 -gradient $gradient -phase 90 -type 0 -name AC315 -length 0.00000
Drift -name DR3151 -length 0.600000
Quadrupole -name QF3151 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR3152 -length 0.600000
Quadrupole -name QD3152 -synrad $quad_synrad -length 0.360000 -strength [expr -0.846389E-01*$e0] -roll 0.00000
Drift -name DR3153 -length 0.600000
Quadrupole -name QF3153 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR3154 -length 0.600000
Drift -name SEC315 -length 0.00000
Drift -name SEC316 -length 0.00000
Cavity -6d 1 -length 1.0 -gradient $gradient -phase 90 -type 0 -name AC316 -length 0.00000
Drift -name DR3161 -length 0.600000
Quadrupole -name QF3161 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR3162 -length 0.600000
Quadrupole -name QD3162 -synrad $quad_synrad -length 0.360000 -strength [expr -0.846389E-01*$e0] -roll 0.00000
Drift -name DR3163 -length 0.600000
Quadrupole -name QF3163 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR3164 -length 0.600000
Drift -name SEC316 -length 0.00000
Drift -name LINAC3 -length 0.00000
Drift -name LINAC4 -length 0.00000
Drift -name SEC401 -length 0.00000
Cavity -6d 1 -length 1.0 -gradient $gradient -phase 90 -type 0 -name AC401 -length 0.00000
Drift -name DR4011 -length 0.600000
Quadrupole -name QF4011 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR4012 -length 0.600000
Quadrupole -name QD4012 -synrad $quad_synrad -length 0.360000 -strength [expr -0.846389E-01*$e0] -roll 0.00000
Drift -name DR4013 -length 0.600000
Quadrupole -name QF4013 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR4014 -length 0.600000
Drift -name SEC401 -length 0.00000
Drift -name SEC402 -length 0.00000
Cavity -6d 1 -length 1.0 -gradient $gradient -phase 90 -type 0 -name AC402 -length 0.00000
Drift -name DR4021 -length 0.600000
Quadrupole -name QF4021 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR4022 -length 0.600000
Quadrupole -name QD4022 -synrad $quad_synrad -length 0.360000 -strength [expr -0.846389E-01*$e0] -roll 0.00000
Drift -name DR4023 -length 0.600000
Quadrupole -name QF4023 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR4024 -length 0.600000
Drift -name SEC402 -length 0.00000
Drift -name SEC403 -length 0.00000
Cavity -6d 1 -length 1.0 -gradient $gradient -phase 90 -type 0 -name AC403 -length 0.00000
Drift -name DR4031 -length 0.600000
Quadrupole -name QF4031 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR4032 -length 0.600000
Quadrupole -name QD4032 -synrad $quad_synrad -length 0.360000 -strength [expr -0.846389E-01*$e0] -roll 0.00000
Drift -name DR4033 -length 0.600000
Quadrupole -name QF4033 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR4034 -length 0.600000
Drift -name SEC403 -length 0.00000
Drift -name SEC404 -length 0.00000
Cavity -6d 1 -length 1.0 -gradient $gradient -phase 90 -type 0 -name AC404 -length 0.00000
Drift -name DR4041 -length 0.600000
Quadrupole -name QF4041 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR4042 -length 0.600000
Quadrupole -name QD4042 -synrad $quad_synrad -length 0.360000 -strength [expr -0.846389E-01*$e0] -roll 0.00000
Drift -name DR4043 -length 0.600000
Quadrupole -name QF4043 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR4044 -length 0.600000
Drift -name SEC404 -length 0.00000
Drift -name SEC405 -length 0.00000
Cavity -6d 1 -length 1.0 -gradient $gradient -phase 90 -type 0 -name AC405 -length 0.00000
Drift -name DR4051 -length 0.600000
Quadrupole -name QF4051 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR4052 -length 0.600000
Quadrupole -name QD4052 -synrad $quad_synrad -length 0.360000 -strength [expr -0.846389E-01*$e0] -roll 0.00000
Drift -name DR4053 -length 0.600000
Quadrupole -name QF4053 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR4054 -length 0.600000
Drift -name SEC405 -length 0.00000
Drift -name SEC406 -length 0.00000
Cavity -6d 1 -length 1.0 -gradient $gradient -phase 90 -type 0 -name AC406 -length 0.00000
Drift -name DR4061 -length 0.600000
Quadrupole -name QF4061 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR4062 -length 0.600000
Quadrupole -name QD4062 -synrad $quad_synrad -length 0.360000 -strength [expr -0.846389E-01*$e0] -roll 0.00000
Drift -name DR4063 -length 0.600000
Quadrupole -name QF4063 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR4064 -length 0.600000
Drift -name SEC406 -length 0.00000
Drift -name SEC407 -length 0.00000
Cavity -6d 1 -length 1.0 -gradient $gradient -phase 90 -type 0 -name AC407 -length 0.00000
Drift -name DR4071 -length 0.600000
Quadrupole -name QF4071 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR4072 -length 0.600000
Quadrupole -name QD4072 -synrad $quad_synrad -length 0.360000 -strength [expr -0.846389E-01*$e0] -roll 0.00000
Drift -name DR4073 -length 0.600000
Quadrupole -name QF4073 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR4074 -length 0.600000
Drift -name SEC407 -length 0.00000
Drift -name SEC408 -length 0.00000
Cavity -6d 1 -length 1.0 -gradient $gradient -phase 90 -type 0 -name AC408 -length 0.00000
Drift -name DR4081 -length 0.600000
Quadrupole -name QF4081 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR4082 -length 0.600000
Quadrupole -name QD4082 -synrad $quad_synrad -length 0.360000 -strength [expr -0.846389E-01*$e0] -roll 0.00000
Drift -name DR4083 -length 0.600000
Quadrupole -name QF4083 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR4084 -length 0.600000
Drift -name SEC408 -length 0.00000
Drift -name SEC409 -length 0.00000
Cavity -6d 1 -length 1.0 -gradient $gradient -phase 90 -type 0 -name AC409 -length 0.00000
Drift -name DR4091 -length 0.600000
Quadrupole -name QF4091 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR4092 -length 0.600000
Quadrupole -name QD4092 -synrad $quad_synrad -length 0.360000 -strength [expr -0.846389E-01*$e0] -roll 0.00000
Drift -name DR4093 -length 0.600000
Quadrupole -name QF4093 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR4094 -length 0.600000
Drift -name SEC409 -length 0.00000
Drift -name SEC410 -length 0.00000
Cavity -6d 1 -length 1.0 -gradient $gradient -phase 90 -type 0 -name AC410 -length 0.00000
Drift -name DR4101 -length 0.600000
Quadrupole -name QF4101 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR4102 -length 0.600000
Quadrupole -name QD4102 -synrad $quad_synrad -length 0.360000 -strength [expr -0.846389E-01*$e0] -roll 0.00000
Drift -name DR4103 -length 0.600000
Quadrupole -name QF4103 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR4104 -length 0.600000
Drift -name SEC410 -length 0.00000
Drift -name SEC411 -length 0.00000
Cavity -6d 1 -length 1.0 -gradient $gradient -phase 90 -type 0 -name AC411 -length 0.00000
Drift -name DR4111 -length 0.600000
Quadrupole -name QF4111 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR4112 -length 0.600000
Quadrupole -name QD4112 -synrad $quad_synrad -length 0.360000 -strength [expr -0.846389E-01*$e0] -roll 0.00000
Drift -name DR4113 -length 0.600000
Quadrupole -name QF4113 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR4114 -length 0.600000
Drift -name SEC411 -length 0.00000
Drift -name SEC412 -length 0.00000
Cavity -6d 1 -length 1.0 -gradient $gradient -phase 90 -type 0 -name AC412 -length 0.00000
Drift -name DR4121 -length 0.600000
Quadrupole -name QF4121 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR4122 -length 0.600000
Quadrupole -name QD4122 -synrad $quad_synrad -length 0.360000 -strength [expr -0.846389E-01*$e0] -roll 0.00000
Drift -name DR4123 -length 0.600000
Quadrupole -name QF4123 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR4124 -length 0.600000
Drift -name SEC412 -length 0.00000
Drift -name SEC413 -length 0.00000
Cavity -6d 1 -length 1.0 -gradient $gradient -phase 90 -type 0 -name AC413 -length 0.00000
Drift -name DR4131 -length 0.600000
Quadrupole -name QF4131 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR4132 -length 0.600000
Quadrupole -name QD4132 -synrad $quad_synrad -length 0.360000 -strength [expr -0.846389E-01*$e0] -roll 0.00000
Drift -name DR4133 -length 0.600000
Quadrupole -name QF4133 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR4134 -length 0.600000
Drift -name SEC413 -length 0.00000
Drift -name SEC414 -length 0.00000
Cavity -6d 1 -length 1.0 -gradient $gradient -phase 90 -type 0 -name AC414 -length 0.00000
Drift -name DR4141 -length 0.600000
Quadrupole -name QF4141 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR4142 -length 0.600000
Quadrupole -name QD4142 -synrad $quad_synrad -length 0.360000 -strength [expr -0.846389E-01*$e0] -roll 0.00000
Drift -name DR4143 -length 0.600000
Quadrupole -name QF4143 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR4144 -length 0.600000
Drift -name SEC414 -length 0.00000
Drift -name SEC415 -length 0.00000
Cavity -6d 1 -length 1.0 -gradient $gradient -phase 90 -type 0 -name AC415 -length 0.00000
Drift -name DR4151 -length 0.600000
Quadrupole -name QF4151 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR4152 -length 0.600000
Quadrupole -name QD4152 -synrad $quad_synrad -length 0.360000 -strength [expr -0.846389E-01*$e0] -roll 0.00000
Drift -name DR4153 -length 0.600000
Quadrupole -name QF4153 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR4154 -length 0.600000
Drift -name SEC415 -length 0.00000
Drift -name SEC416 -length 0.00000
Cavity -6d 1 -length 1.0 -gradient $gradient -phase 90 -type 0 -name AC416 -length 0.00000
Drift -name DR4161 -length 0.600000
Quadrupole -name QF4161 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR4162 -length 0.600000
Quadrupole -name QD4162 -synrad $quad_synrad -length 0.360000 -strength [expr -0.846389E-01*$e0] -roll 0.00000
Drift -name DR4163 -length 0.600000
Quadrupole -name QF4163 -synrad $quad_synrad -length 0.360000 -strength [expr 0.423969E-01*$e0] -roll 0.00000
Drift -name DR4164 -length 0.600000
Drift -name SEC416 -length 0.00000
Drift -name LINAC4 -length 0.00000
Drift -name MATCHING -length 0.00000
Drift -name DRL1 -length 0.600000
Quadrupole -name QFL -synrad $quad_synrad -length 0.360000 -strength [expr -0.315579E-03*$e0] -roll 0.00000
Drift -name DRL2 -length 0.600000
Quadrupole -name QD0 -synrad $quad_synrad -length 0.360000 -strength [expr 0.478690E-03*$e0] -roll 0.00000
Drift -name DRR1 -length 0.600000
Quadrupole -name QFR -synrad $quad_synrad -length 0.360000 -strength [expr -0.315579E-03*$e0] -roll 0.00000
Drift -name DRR2 -length 0.600000
Drift -name MATCHING -length 0.00000
Drift -name LINAC -length 0.00000
