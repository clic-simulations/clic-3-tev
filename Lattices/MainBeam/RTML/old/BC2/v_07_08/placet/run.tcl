set scripts /afs/cern.ch/user/a/alatina/CLIC/scripts
set script_dir /afs/cern.ch/user/a/alatina/CLIC/scripts

source ./clic_basic_single.tcl

set e_initial 9.0
set e0 $e_initial

BeamlineNew
Girder
SetReferenceEnergy $e0
SlicesToParticles
TclCall -script {BeamDump -file beam0.dump}
set quad_synrad 0
set sbend_synrad 1
set gradient 0.0388603125
source bc_stage2_acc.tcl
source bc_stage2_chicane.tcl
TclCall -script { BeamDump -file beam.dump }
TclCall -script {
  set t [open "beam.dat" "w"]
  puts $t [BeamMeasure]
  close $t
}
BeamlineSet

source bc_stage2_acc_beam.tcl

set n_total 31000
set n_slice 31
set n 1000

source $scripts/clic_beam.tcl

make_beam_slice beam0 $n_slice $n

TestNoCorrection -beam beam0 -emitt_file dummy -machines 1 -survey Zero -bpm_res 0.0

Octave
