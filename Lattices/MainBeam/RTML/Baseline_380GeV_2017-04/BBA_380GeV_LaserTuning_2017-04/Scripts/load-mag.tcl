Octave {
load mag-$machine.dat;
magnets = placet_get_number_list("rtml", "quadrupole");
magnets = [ magnets placet_get_number_list("rtml", "sbend") ];
magnets = [ magnets placet_get_number_list("rtml", "multipole") ];
placet_element_set_attribute("rtml", magnets, "x", mag.x);
placet_element_set_attribute("rtml", magnets, "y", mag.y);
placet_element_set_attribute("rtml", magnets, "xp", mag.xp);
placet_element_set_attribute("rtml", magnets, "yp", mag.yp);
placet_element_set_attribute("rtml", magnets, "roll", mag.roll);

magnets = placet_get_number_list("rtml", "quadrupole");
magnets = [ magnets placet_get_number_list("rtml", "multipole") ];
placet_element_set_attribute("rtml", magnets, "strength", mag.strength);

magnets = placet_get_number_list("rtml", "e0");
placet_element_set_attribute("rtml", magnets, "angle", mag.e0);


BI = placet_get_number_list("rtml","bpm");
placet_element_set_attribute("rtml", BI, "x", Bpm.x);
placet_element_set_attribute("rtml", BI, "y", Bpm.y);
placet_element_set_attribute("rtml", BI, "xp", Bpm.xp);
placet_element_set_attribute("rtml", BI, "yp", Bpm.yp);
placet_element_set_attribute("rtml", BI, "roll", Bpm.roll);
placet_element_set_attribute("rtml",BI, "resolution",Bpm.res);
}

Octave {
load dipole-$machine.dat
Corrector_all = placet_get_number_list("rtml","dipole");
placet_element_set_attribute("rtml", Corrector_all, 'strength_x',dipole_x);
placet_element_set_attribute("rtml", Corrector_all, 'strength_y',dipole_y);
}

