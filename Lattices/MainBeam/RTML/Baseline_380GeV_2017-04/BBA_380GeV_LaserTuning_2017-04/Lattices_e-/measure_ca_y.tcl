TclCall -script {
  Octave {
      std_y_perfect = [3.858741;3.729007;5.681786;1.372056].*(1+0.01*randn(4,1)) * 0.8;
      B0 = placet_get_beam();
      y = B0(:,3);

      meany = mean(y);
      sumy = 0;
      for j = 1:length(y)
          sumy +=  normpdf(y(j),meany,std_y_perfect(measure_index));
      end

      fid = fopen('result_ca_sumy.dat',"a+");
      fprintf(fid,"%f\n",sumy);
      fclose(fid);

  }
}
