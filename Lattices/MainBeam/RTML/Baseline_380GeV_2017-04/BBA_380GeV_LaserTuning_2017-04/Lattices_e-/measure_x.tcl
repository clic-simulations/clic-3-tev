TclCall -script {
  Octave {
      measure_index++;
      std_x_perfect = [34.753476;34.453494;36.703348;37.019890].*(1+0.0001*randn(4,1))*0.8;
      B0 = placet_get_beam();
      x = B0(:,2);

      meanx = mean(x);
      sumx = 0;
      for j = 1:length(x)
          sumx +=  normpdf(x(j),meanx,std_x_perfect(measure_index));
      end

      fid = fopen('result_sumx.dat',"a+");
      fprintf(fid,"%f\n",sumx);
      fclose(fid);

  }
}

