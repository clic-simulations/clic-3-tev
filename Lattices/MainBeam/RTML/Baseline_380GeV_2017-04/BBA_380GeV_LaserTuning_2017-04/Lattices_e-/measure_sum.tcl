TclCall -script {
        Octave {
        sumx = load('result_sumx.dat');
        sumy = load('result_sumy.dat');
        sum_sigma = 0;
        sum_sigma = -1 * sum((25*sumx .* (1+0.005*randn(4,1))+ sumy .* (1+0.005*randn(4,1)) ));
        save sum_sigma.dat sum_sigma;
        }
}

