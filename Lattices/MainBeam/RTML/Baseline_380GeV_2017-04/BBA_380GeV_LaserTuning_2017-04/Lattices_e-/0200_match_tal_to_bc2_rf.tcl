#
# matching of Turn Around Loop to BC2 RF
#
# beamparams=[betax,alphax,emitnx,betay,alphay,emitny,sigmaz,charge,
#             uncespread,echirp,energy,nslice,nmacro,nsigmabunch,nsigmawake]
# units=[m,rad,m*rad,m,rad,m*rad,m,C,1,1/m,eV,1,1,1,1]
#
# where necessary units were converted to placet units in main.tcl

proc lattice_match_tal_to_bc2_rf {bparray} {
    upvar $bparray beamparams

    set usesixdim 1
    set numthinlenses 100
    set quad_synrad 0

    set refenergy $beamparams(meanenergy)

    SetReferenceEnergy $refenergy

    set lquadm 0.3
    
    set kqm1 0.9977955408
    set kqm2 -0.9378162817
    set kqm3 0.8448493311
    set kqm4 0.643791675
    set kqm5 -1.65361
    
    set ldm1 6.8
    set ldm2 3.4
    set ldm3 1.65
    set ldm4 1.4

    if { 0 } {

	Girder
	Bpm -length 0.0
	Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm1*$lquadm*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
	Drift -length $ldm1 -six_dim $usesixdim
	Bpm -length 0.0
	Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm2*$lquadm*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
	Drift -length $ldm2 -six_dim $usesixdim
	Bpm -length 0.0
	Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm3*$lquadm*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
	Drift -length $ldm3 -six_dim $usesixdim
	Bpm -length 0.0
	Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm4*$lquadm*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
	Drift -length $ldm4 -six_dim $usesixdim
	Bpm -length 0.0
	Quadrupole -synrad $quad_synrad -length 0.175 -strength [expr $kqm5*0.175*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
	# this was in 0210 ....
	#Quadrupole -synrad $quad_synrad -length 0.175 -strength [expr $kqm5*0.175*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses

    } {

	Drift -name "DRIFT_99" -length 3.647078968
        Dipole -name "DIQ-match-1" -length 0.00000000000
        Bpm -length 0
	Quadrupole -name "QM6" -synrad $quad_synrad -length 0.175 -strength [expr 0.139008664*$refenergy] -e0 $refenergy
        Dipole -name "DIQ-match-2" -length 0.00000000000
        Bpm -length 0
	Quadrupole -name "QM6" -synrad $quad_synrad -length 0.175 -strength [expr 0.139008664*$refenergy] -e0 $refenergy
	Drift -name "DRIFT_99" -length 3.647078968
        Dipole -name "DIQ-match-3" -length 0.00000000000
        Bpm -length 0
	Quadrupole -name "QM5" -synrad $quad_synrad -length 0.175 -strength [expr -0.1185026319*$refenergy] -e0 $refenergy
        Dipole -name "DIQ-match-4" -length 0.00000000000
        Bpm -length 0
	Quadrupole -name "QM5" -synrad $quad_synrad -length 0.175 -strength [expr -0.1185026319*$refenergy] -e0 $refenergy
	Drift -name "DRIFT_99" -length 3.647078968
        Dipole -name "DIQ-match-5" -length 0.00000000000
        Bpm -length 0
	Quadrupole -name "QM4" -synrad $quad_synrad -length 0.175 -strength [expr -0.06627433522*$refenergy] -e0 $refenergy
        Dipole -name "DIQ-match-6" -length 0.00000000000
        Bpm -length 0
	Quadrupole -name "QM4" -synrad $quad_synrad -length 0.175 -strength [expr -0.06627433522*$refenergy] -e0 $refenergy
	Drift -name "DRIFT_99" -length 3.647078968
        Dipole -name "DIQ-match-7" -length 0.00000000000
        Bpm -length 0
	Quadrupole -name "QF1" -synrad $quad_synrad -length 0.175 -strength [expr 0.2258577615*$refenergy] -e0 $refenergy
        Bpm -length 0
	Quadrupole -name "QF1" -synrad $quad_synrad -length 0.175 -strength [expr 0.2258577615*$refenergy] -e0 $refenergy
        Dipole -name "DIQ-match-8" -length 0.00000000000
	Drift -name "DRIFT_0" -length 2.9
        Dipole -name "DIQ-match-9" -length 0.00000000000
        Bpm -length 0
	Quadrupole -name "QF2" -synrad $quad_synrad -length 0.175 -strength [expr -0.2258577615*$refenergy] -e0 $refenergy
	Dipole -name "FF2YKICK1" -length 0
        Dipole -name "DIQ-match-10" -length 0.00000000000
        Bpm -length 0
	Quadrupole -name "QF2" -synrad $quad_synrad -length 0.175 -strength [expr -0.2258577615*$refenergy] -e0 $refenergy
	Drift -name "DRIFT_0" -length 2.9
        Dipole -name "DIQ-match-11" -length 0.00000000000
        Bpm -length 0
	Quadrupole -name "QF1" -synrad $quad_synrad -length 0.175 -strength [expr 0.2258577615*$refenergy] -e0 $refenergy
	Dipole -name "FF2XKICK1" -length 0
        Dipole -name "DIQ-match-12" -length 0.00000000000
        Bpm -length 0
	Quadrupole -name "QF1" -synrad $quad_synrad -length 0.175 -strength [expr 0.2258577615*$refenergy] -e0 $refenergy
	Drift -name "DRIFT_0" -length 2.9
        Dipole -name "DIQ-match-13" -length 0.00000000000
        Bpm -length 0
	Quadrupole -name "QF2" -synrad $quad_synrad -length 0.175 -strength [expr -0.2258577615*$refenergy] -e0 $refenergy
	Dipole -name "FF2YKICK2" -length 0
        Dipole -name "DIQ-match-14" -length 0.00000000000
        Bpm -length 0
	Quadrupole -name "QF2" -synrad $quad_synrad -length 0.175 -strength [expr -0.2258577615*$refenergy] -e0 $refenergy
	Drift -name "DRIFT_0" -length 2.9
        Dipole -name "DIQ-match-15" -length 0.00000000000
        Bpm -length 0
	Quadrupole -name "QF1" -synrad $quad_synrad -length 0.175 -strength [expr 0.2258577615*$refenergy] -e0 $refenergy
	Dipole -name "FF2XKICK2" -length 0
        Dipole -name "DIQ-match-16" -length 0.00000000000
        Bpm -length 0
	Quadrupole -name "QF1" -synrad $quad_synrad -length 0.175 -strength [expr 0.2258577615*$refenergy] -e0 $refenergy
	Drift -name "DRIFT_43" -length 0.5652677159
        Dipole -name "DIQ-match-17" -length 0.00000000000
        Bpm -length 0
	Quadrupole -name "QM3" -synrad $quad_synrad -length 0.175 -strength [expr 0.06762795992*$refenergy] -e0 $refenergy
	Drift -name "Marker-BC2-start" -length 0.0
        Dipole -name "DIQ-match-18" -length 0.00000000000
        Bpm -length 0
	Quadrupole -name "QM3" -synrad $quad_synrad -length 0.175 -strength [expr 0.06762795992*$refenergy] -e0 $refenergy
	Drift -name "DRIFT_43" -length 0.5652677159
        Dipole -name "DIQ-match-19" -length 0.00000000000
        Bpm -length 0
	Quadrupole -name "QM2" -synrad $quad_synrad -length 0.175 -strength [expr -0.0748101827*$refenergy] -e0 $refenergy
        Dipole -name "DIQ-match-20" -length 0.00000000000
        Bpm -length 0
	Quadrupole -name "QM2" -synrad $quad_synrad -length 0.175 -strength [expr -0.0748101827*$refenergy] -e0 $refenergy
	Drift -name "DRIFT_43" -length 0.5652677159
        Dipole -name "DIQ-match-21" -length 0.00000000000
        Bpm -length 0
	Quadrupole -name "QM1" -synrad $quad_synrad -length 0.175 -strength [expr -0.2222357097*$refenergy] -e0 $refenergy
        Dipole -name "DIQ-match-22" -length 0.00000000000
        Bpm -length 0
	Quadrupole -name "QM1" -synrad $quad_synrad -length 0.175 -strength [expr -0.2222357097*$refenergy] -e0 $refenergy
	Drift -name "DRIFT_43" -length 0.5652677159
	Drift -name "Marker-TAL2-end" -length 0.0
    }
}
