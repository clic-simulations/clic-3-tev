TclCall -script {
    Octave {

        R_match_x = [2.978776, 13.466372; 0.223619, 1.346637 ]; 
        R_match_y = [-0.175991, 3.569814; -0.319546, 0.799577 ]; 
        M_fodo_x =[0.707107,  7.071068; -0.070711, 0.707107 ]; 
        M_fodo_y = [0.707107, 3.156968; -0.158380, 0.707107 ]; 
        M_fodo_half_y = [1.382683, 2.557009; -0.057273, 0.617317 ]; 

	R_match_f_x =[1.346637,  13.466372; -0.297878, -2.236186 ]; 
	R_match_f_y = [0.799577, 3.569814; 0.039419, 1.426652 ]; 
 
        M_fodo_second_half_y = [0.617317, 2.557009; -0.057273, 1.382683 ];



        B0 = placet_get_beam();
        save beam-1.dat B0;
        x = B0(:,2);
        y = B0(:,3);
        xp = B0(:,5);
        yp = B0(:,6);

        X =  transpose(R_match_x * transpose([x xp]));
        Y =  transpose(R_match_y * transpose([y yp]));
      

        x = X(:,1);
        xp = X(:,2);
        y = Y(:,1);
        yp = Y(:,2);

        B0(:,2) = x;
        B0(:,3) = y;
        B0(:,5) = xp;
        B0(:,6) = yp;
        save beam-2.dat B0;

        Y =  transpose(M_fodo_half_y * transpose([y yp]));
        y = Y(:,1);
        yp = Y(:,2);



        sum_sigma = 0;
        for i = 1:4
            std_x_perfect = [17.248;17.248;17.248;17.248].*(1+0.000*randn(4,1));
            std_y_perfect = [1.7860;1.7860;1.7860;1.7860].*(1+0.0*randn(4,1));
            X =  transpose(M_fodo_x * transpose([x xp]));
            Y =  transpose(M_fodo_x * transpose([y yp]));
    
            x = X(:,1);
            xp = X(:,2);
            y = Y(:,1);
            yp = Y(:,2);
    
            std_x = std(x);
            std_y = std(y);
    
    
            meanx = mean(x);
            meany = mean(y);
            sumx = 0;
            sumy = 0;
                for j = 1:length(x)
                    if ( unifrnd(0,1) < exp(-0.5*((x(j)-meanx)/std_x_perfect(i))^2 - 0.5*((y(j)-meany)/std_y_perfect(i))^2 ))
                        sumx += 1;
                    end
                end
    
            sum_sigma += -1 * (sumx + sumy);
        end

        save sum_sigma.dat sum_sigma


	X =  transpose(R_match_f_x * M_fodo_x * transpose([x xp]));
        Y =  transpose(R_match_f_y * M_fodo_second_half_y * transpose([y yp]));

        x = X(:,1);
        xp = X(:,2);
        y = Y(:,1);
        yp = Y(:,2);

        B0(:,2) = x;
        B0(:,3) = y;
        B0(:,5) = xp;
        B0(:,6) = yp;
        
        placet_set_beam(B0);
    }
}
