#
# Dump and matching of booster to central arc
#
# beamparams=[betax,alphax,emitnx,betay,alphay,emitny,sigmaz,charge,
#             uncespread,echirp,energy,nslice,nmacro,nsigmabunch,nsigmawake]
# units=[m,rad,m*rad,m,rad,m*rad,m,C,1,1/m,eV,1,1,1,1]
#
# where necessary units were converted to placet units in main.tcl

proc lattice_dump_and_match_booster_to_ca {bparray} {
    upvar $bparray beamparams

    set usesixdim 1
    set numthinlenses 100
    set quad_synrad 0

    set refenergy $beamparams(meanenergy)

    SetReferenceEnergy $refenergy
    
    set lquadm 0.3
    
    set kqm1 0.4218379525
    set kqm2 0.1281436211
    set kqm3 -0.673853586
    set kqm4 0.8274547417
    
    set ldm1 2.25
    set ldm2 3.40
    set ldm3 1.90
    set ldm4 8.00
    
    if { 0 } {

	Girder
	Drift -length $ldm1 -six_dim $usesixdim
	Bpm -length 0.0
	Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm1*$lquadm*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
	Drift -length $ldm2 -six_dim $usesixdim
	Bpm -length 0.0
	Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm2*$lquadm*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
	Drift -length $ldm3 -six_dim $usesixdim
	Bpm -length 0.0
	Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm3*$lquadm*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
	Drift -length $ldm4 -six_dim $usesixdim
	Bpm -length 0.0
	Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm4*$lquadm*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses

    } {

	Drift -name "DRIFT_19" -length 2.120694192
	Quadrupole -name "130QM1" -synrad $quad_synrad -length 0.35 -strength [expr -0.07461181132*$refenergy*2] -e0 $refenergy
  Dipole -name "DIQ-Boomatch-1" -length 0.00000000000
	Bpm -length 0.0
	Drift -name "DRIFT_19" -length 2.120694192
	Quadrupole -name "130QM2" -synrad $quad_synrad -length 0.35 -strength [expr 0.1485688888*$refenergy*2] -e0 $refenergy
  Dipole -name "DIQ-Boomatch-3" -length 0.00000000000
	Bpm -length 0.0
	Drift -name "DRIFT_19" -length 2.120694192
	Quadrupole -name "130QM3" -synrad $quad_synrad -length 0.35 -strength [expr -0.1464805333*$refenergy*2] -e0 $refenergy
  Dipole -name "DIQ-Boomatch-5" -length 0.00000000000
	Bpm -length 0.0
	Drift -name "DRIFT_19" -length 2.120694192
	Quadrupole -name "130QF1" -synrad $quad_synrad -length 0.35 -strength [expr 0.05488808199*$refenergy*2] -e0 $refenergy
  Dipole -name "DIQ-Boomatch-6" -length 0.00000000000
	Bpm -name "FF1XBPM1" -length 0
	Drift -name "DRIFT_0" -length 12.65
	Quadrupole -name "130QF2" -synrad $quad_synrad -length 0.35 -strength [expr -0.05488808199*$refenergy*2] -e0 $refenergy
  Dipole -name "DIQ-Boomatch-8" -length 0.00000000000
	Bpm -name "FF1YBPM1" -length 0
	Drift -name "DRIFT_0" -length 12.65
	Quadrupole -name "130QF1" -synrad $quad_synrad -length 0.35 -strength [expr 0.05488808199*$refenergy*2] -e0 $refenergy
  Dipole -name "DIQ-Boomatch-10" -length 0.00000000000
	Bpm -name "FF1XBPM2" -length 0
	Drift -name "DRIFT_0" -length 12.65
	Quadrupole -name "130QF2" -synrad $quad_synrad -length 0.35 -strength [expr -0.05488808199*$refenergy*2] -e0 $refenergy
  Dipole -name "DIQ-Boomatch-12" -length 0.00000000000
	Bpm -name "FF1YBPM2" -length 0
	Drift -name "DRIFT_0" -length 12.65
	Quadrupole -name "130QF1" -synrad $quad_synrad -length 0.35 -strength [expr 0.05488808199*$refenergy*2] -e0 $refenergy
  Dipole -name "DIQ-Boomatch-14" -length 0.00000000000
	Bpm -length 0.0
	Drift -name "DRIFT_646" -length 21.82960853
	Quadrupole -name "130QM4" -synrad $quad_synrad -length 0.35 -strength [expr -0.06369228931*$refenergy*2] -e0 $refenergy
  Dipole -name "DIQ-Boomatch-16" -length 0.00000000000

        Drift -name "Marker-CA-start" -length 0.0

	Bpm -length 0.0
	Drift -name "DRIFT_647" -length 6.39411488
	Quadrupole -name "130QM5" -synrad $quad_synrad -length 0.35 -strength [expr 0.0610312928*$refenergy*2] -e0 $refenergy
  Dipole -name "DIQ-Boomatch-18" -length 0.00000000000
	Bpm -length 0.0
	Drift -name "DRIFT_648" -length 8.85455555
	Quadrupole -name "130QM6" -synrad $quad_synrad -length 0.35 -strength [expr 0.07451466375*$refenergy*2] -e0 $refenergy
  Dipole -name "DIQ-Boomatch-20" -length 0.00000000000
	Bpm -length 0.0

	# install feed forward
	TclCall -name "FF1" -script "FF1"
        Drift -name "Marker-BOO-end" -length 0.0
    }
}
