#
# Turn Around Loop lattice
#
# beamparams=[betax,alphax,emitnx,betay,alphay,emitny,sigmaz,charge,
#             uncespread,echirp,energy,nslice,nmacro,nsigmabunch,nsigmawake]
# units=[m,rad,m*rad,m,rad,m*rad,m,C,1,1/m,eV,1,1,1,1]
# where necessary units were converted to placet units in main.tcl

proc lattice_turn_around_loop {bparray} {
upvar $bparray beamparams

set usesynrad $beamparams(useisr)
set usesynrad_lattice $beamparams(useisr_lattice)
set usesixdim 1
set numthinlenses 100
set quad_synrad 0

set pi 3.141592653589793
set c 299792458
set q0 1.6021765e-19 
set eps0 [expr 1/(4e-7*$pi*$c*$c)]

set refenergy $beamparams(meanenergy)

set lbend 2.0
set lquad 0.3
set lsext 0.2

# first bend to the left
set msign -1.0
set thetaa [expr $msign*1.2/180*$pi]
set rhoa [expr $lbend/(2*sin($thetaa/2))]
set larca [expr $thetaa*$rhoa]

set thetab [expr $msign*1.2/180*$pi]
set rhob [expr $lbend/(2*sin($thetab/2))]
set larcb [expr $thetab*$rhob]

set ld01 1.15
set ld02 2.99
set ld03 1.57
set ld04 1.94
set ld05 1.94
set ld06 0.30
set ld07 $ld05
set ld08 $ld04
set ld09 $ld03
set ld10 $ld02
set ld11 $ld01


set ld03a [expr $ld03-0.3]
set ld03b 0.1
set ld09a 0.1
set ld09b [expr $ld09-0.3]

set ld02a [expr $ld02-0.3]
set ld02b 0.1
set ld10a 0.1
set ld10b [expr $ld10-0.3]

set kq01  1.291951194
set kq02 -1.212050561
set kq03  1.733959844
set kq04 -0.3453705052
set kq05 $kq04
set kq06 $kq03
set kq07 $kq02
set kq08 $kq01

set ks01 [expr $msign*28.81111869]
set ks02 [expr $msign*28.61111867]
set ks03 [expr $msign*-39.72965294]
set ks04 [expr $msign*-36.72965291]

# left bending arc

SetReferenceEnergy $refenergy

for {set cell 1} {$cell<=10} {incr cell} {
    Girder
   
    Drift -length 0 -name "CELL_7TAL_SPLIT1_$cell"
 
    Drift -name Drift-left-$cell-1 -length $ld01 -six_dim $usesixdim
    Sbend -name Sbend-left-$cell-1 -length $larca -angle [expr $thetaa] -synrad $usesynrad -six_dim $usesixdim -thin_lens $numthinlenses -e0 $refenergy -E1 [expr $thetaa/2] -E2 [expr $thetaa/2]
    set g0 [expr $refenergy/0.000510999]
    set de [expr 1/(6*$pi*$eps0)*$q0*$q0*$c*$g0*$g0*$g0*$g0/($rhoa*$rhoa)*$larca/$c]
    set refenergy [expr $refenergy-$de/$q0/1e9*$usesynrad*$usesynrad_lattice]
    SetReferenceEnergy $refenergy
    Drift -name Drift-left-$cell-2 -length $ld02a -six_dim $usesixdim
    Multipole -name Multipole-left-$cell-1 -type 3 -length $lsext -e0 $refenergy -strength [expr $ks03*$refenergy*$lsext] -thin_lens $numthinlenses -six_dim $usesixdim
    Drift -name Drift-left-$cell-3 -length $ld02b -six_dim $usesixdim
    Dipole -name "DIQ-left-$cell-1" -length 0.00000000000
    Bpm -length 0.0
    Quadrupole -name Quadrupole-left-$cell-1 -synrad $quad_synrad -length $lquad -strength [expr $kq02*$lquad*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
    Drift -name Drift-left-$cell-4 -length $ld03a -six_dim $usesixdim
    Multipole -name Multipole-left-$cell-2 -type 3 -length $lsext -e0 $refenergy -strength [expr $ks01*$refenergy*$lsext] -thin_lens $numthinlenses -six_dim $usesixdim
    Drift -name Drift-left-$cell-5 -length $ld03b -six_dim $usesixdim
    Dipole -name "DIQ-left-$cell-2" -length 0.00000000000
    Bpm -length 0.0
    Quadrupole -name Quadrupole-left-$cell-2 -synrad $quad_synrad -length $lquad -strength [expr $kq03*$lquad*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
    Drift -name Drift-left-$cell-6 -length $ld04 -six_dim $usesixdim
    Dipole -name "DIQ-left-$cell-3" -length 0.00000000000
    Bpm -length 0.0
    Quadrupole -name Quadrupole-left-$cell-3 -synrad $quad_synrad -length $lquad -strength [expr $kq04*$lquad*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
    Drift -name Drift-left-$cell-7 -length $ld05 -six_dim $usesixdim
    Sbend -name Sbend-left-$cell-2 -length $larcb -angle [expr $thetab] -synrad $usesynrad -six_dim $usesixdim -thin_lens $numthinlenses -e0 $refenergy -E1 [expr $thetab/2] -E2 [expr $thetab/2]
    set g0 [expr $refenergy/0.000510999]
    set de [expr 1/(6*$pi*$eps0)*$q0*$q0*$c*$g0*$g0*$g0*$g0/($rhob*$rhob)*$larcb/$c]
    set refenergy [expr $refenergy-$de/$q0/1e9*$usesynrad*$usesynrad_lattice]
    SetReferenceEnergy $refenergy
    Drift -name Drift-left-$cell-8 -length $ld06 -six_dim $usesixdim
    Sbend -name Sbend-left-$cell-3 -length $larcb -angle [expr $thetab] -synrad $usesynrad -six_dim $usesixdim -thin_lens $numthinlenses -e0 $refenergy -E1 [expr $thetab/2] -E2 [expr $thetab/2]
    set g0 [expr $refenergy/0.000510999]
    set de [expr 1/(6*$pi*$eps0)*$q0*$q0*$c*$g0*$g0*$g0*$g0/($rhob*$rhob)*$larcb/$c]
    set refenergy [expr $refenergy-$de/$q0/1e9*$usesynrad*$usesynrad_lattice]
    SetReferenceEnergy $refenergy
    Drift -name Drift-left-$cell-9 -length $ld06 -six_dim $usesixdim
    Sbend -name Sbend-left-$cell-4 -length $larcb -angle [expr $thetab] -synrad $usesynrad -six_dim $usesixdim -thin_lens $numthinlenses -e0 $refenergy -E1 [expr $thetab/2] -E2 [expr $thetab/2]
    set g0 [expr $refenergy/0.000510999]
    set de [expr 1/(6*$pi*$eps0)*$q0*$q0*$c*$g0*$g0*$g0*$g0/($rhob*$rhob)*$larcb/$c]
    set refenergy [expr $refenergy-$de/$q0/1e9*$usesynrad*$usesynrad_lattice]
    SetReferenceEnergy $refenergy
    Drift -name Drift-left-$cell-10 -length $ld07 -six_dim $usesixdim
    Dipole -name "DIQ-left-$cell-4" -length 0.00000000000
    Bpm -length 0.0
    Quadrupole -name Quadrupole-left-$cell-4 -synrad $quad_synrad -length $lquad -strength [expr $kq05*$lquad*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
    Drift -name Drift-left-$cell-11 -length $ld08 -six_dim $usesixdim
    Dipole -name "DIQ-left-$cell-5" -length 0.00000000000
    Bpm -length 0.0
    Quadrupole -name Quadrupole-left-$cell-5 -synrad $quad_synrad -length $lquad -strength [expr $kq06*$lquad*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
    Drift -name Drift-left-$cell-12 -length $ld09a -six_dim $usesixdim
    Multipole -name Multipole-left-$cell-3 -type 3 -length $lsext -e0 $refenergy -strength [expr $ks02*$refenergy*$lsext] -thin_lens $numthinlenses -six_dim $usesixdim
    Drift -name Drift-left-$cell-13 -length $ld09b -six_dim $usesixdim
    Dipole -name "DIQ-left-$cell-6" -length 0.00000000000
    Bpm -length 0.0
    Quadrupole -name Quadrupole-left-$cell-6 -synrad $quad_synrad -length $lquad -strength [expr $kq07*$lquad*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
    Drift -name Drift-left-$cell-14 -length $ld10a -six_dim $usesixdim
    Multipole -name Multipole-left-$cell-4 -type 3 -length $lsext -e0 $refenergy -strength [expr $ks04*$refenergy*$lsext] -thin_lens $numthinlenses -six_dim $usesixdim
    Drift -name Drift-left-$cell-15 -length $ld10b -six_dim $usesixdim
    Sbend -name Sbend-left-$cell-5 -length $larca -angle [expr $thetaa] -synrad $usesynrad -six_dim $usesixdim -thin_lens $numthinlenses -e0 $refenergy -E1 [expr $thetaa/2] -E2 [expr $thetaa/2]
    set g0 [expr $refenergy/0.000510999]
    set de [expr 1/(6*$pi*$eps0)*$q0*$q0*$c*$g0*$g0*$g0*$g0/($rhoa*$rhoa)*$larca/$c]
    set refenergy [expr $refenergy-$de/$q0/1e9*$usesynrad*$usesynrad_lattice]
    SetReferenceEnergy $refenergy
    Drift -name Drift-left-$cell-16 -length $ld11 -six_dim $usesixdim
    
    if {$cell==10} {
    } else {
        Dipole -name "DIQ-left-$cell-7" -length 0.00000000000
        Bpm -length 0.0
        Quadrupole -name Quadrupole-left-$cell-7 -synrad $quad_synrad -length $lquad -strength [expr $kq08*$lquad*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
    }
}

Drift -name "Marker-TAL2-start" -length 0.0

# match left and right arcs
set ldm1 17.39
set ldm2 [expr 21.16-$ldm1]
set ldm3 10.9
set ldmc 20.0

set kqm1   0.8652551102
set kqm2  -0.449800556
set kqm3   0.3081363082
set kqmc1  0.1848254644
set kqmc2 -0.2054362023

SetReferenceEnergy $refenergy

Girder

Drift -length 0 -name "CELL_7TAL_SPLIT2_1"

Dipole -name "DIQ-middle-1" -length 0.00000000000
Bpm -length 0.0
Quadrupole -name Quadrupole-middle-1 -synrad $quad_synrad -length $lquad -strength [expr $kqm1*$lquad*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -name Drift-middle-1 -length $ldm1 -six_dim $usesixdim
Dipole -name "DIQ-middle-2" -length 0.00000000000
Bpm -length 0.0
Quadrupole -name Quadrupole-middle-2 -synrad $quad_synrad -length $lquad -strength [expr $kqm2*$lquad*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -name Drift-middle-2 -length $ldm2 -six_dim $usesixdim
Dipole -name "DIQ-middle-3" -length 0.00000000000
Bpm -length 0.0
Quadrupole -name Quadrupole-middle-3 -synrad $quad_synrad -length $lquad -strength [expr $kqm3*$lquad*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -name Drift-middle-3 -length $ldm3 -six_dim $usesixdim

for {set cell 1} {$cell<=7} {incr cell} {
    Dipole -name "DIQ-middle-$cell-1" -length 0.00000000000
    Bpm -length 0.0
    Quadrupole -name Quadrupole-middle-$cell-1 -synrad $quad_synrad -length $lquad -strength [expr $kqmc1*$lquad*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
    Drift -name Drift-middle-$cell-1 -length $ldmc -six_dim $usesixdim
    Dipole -name "DIQ-middle-$cell-2" -length 0.00000000000
    Bpm -length 0.0
    Quadrupole -name Quadrupole-middle-$cell-2 -synrad $quad_synrad -length $lquad -strength [expr $kqmc2*$lquad*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
    Drift -name Drift-middle-$cell-2 -length $ldmc -six_dim $usesixdim
}

Dipole -name "DIQ-middle-4" -length 0.00000000000
Bpm -length 0.0
Quadrupole -name Quadrupole-middle-4 -synrad $quad_synrad -length $lquad -strength [expr $kqmc1*$lquad*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -name Drift-middle-4 -length $ldm3 -six_dim $usesixdim
Dipole -name "DIQ-middle-5" -length 0.00000000000
Bpm -length 0.0
Quadrupole -name Quadrupole-middle-5 -synrad $quad_synrad -length $lquad -strength [expr $kqm3*$lquad*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -name Drift-middle-5 -length $ldm2 -six_dim $usesixdim
Dipole -name "DIQ-middle-6" -length 0.00000000000
Bpm -length 0.0
Quadrupole -name Quadrupole-middle-6 -synrad $quad_synrad -length $lquad -strength [expr $kqm2*$lquad*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -name Drift-middle-6 -length $ldm1 -six_dim $usesixdim
Bpm -length 0.0
Dipole -name "DIQ-middle-7" -length 0.00000000000
Quadrupole -name Quadrupole-middle-7 -synrad $quad_synrad -length $lquad -strength [expr $kqm1*$lquad*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses

Drift -name "Marker-TAL1-end" -length 0.0
# right bending arc

SetReferenceEnergy $refenergy

for {set cell 1} {$cell<=40} {incr cell} {
    Girder

    Drift -length 0 -name "CELL_7TAL_SPLIT3_$cell"

    Drift -name Drift-right-$cell-1 -length $ld01 -six_dim $usesixdim
    Sbend -name Sbend-right-$cell-1 -length $larca -angle [expr -$thetaa] -six_dim $usesixdim -synrad $usesynrad -thin_lens $numthinlenses -e0 $refenergy -E1 [expr -$thetaa/2] -E2 [expr -$thetaa/2]
    set g0 [expr $refenergy/0.000510999]
    set de [expr 1/(6*$pi*$eps0)*$q0*$q0*$c*$g0*$g0*$g0*$g0/($rhoa*$rhoa)*$larca/$c]
    set refenergy [expr $refenergy-$de/$q0/1e9*$usesynrad*$usesynrad_lattice]
    SetReferenceEnergy $refenergy
    Drift -name Drift-right-$cell-2 -length $ld02a -six_dim $usesixdim
    Multipole -name Multipole-right-$cell-1 -type 3 -length $lsext -e0 $refenergy -strength [expr -$ks03*$refenergy*$lsext] -thin_lens $numthinlenses -six_dim $usesixdim
    Drift -name Drift-right-$cell-3 -length $ld02b -six_dim $usesixdim
    Dipole -name "DIQ-right-$cell-1" -length 0.00000000000
    Bpm -length 0.0
    Quadrupole -name Quadrupole-right-$cell-1 -synrad $quad_synrad -length $lquad -strength [expr $kq02*$lquad*$refenergy] -thin_lens $numthinlenses -six_dim $usesixdim
    Drift -name Drift-right-$cell-4 -length $ld03a -six_dim $usesixdim
    Multipole -name Multipole-right-$cell-2 -type 3 -length $lsext -e0 $refenergy -strength [expr -$ks01*$refenergy*$lsext] -thin_lens $numthinlenses -six_dim $usesixdim
    Drift -name Drift-right-$cell-5 -length $ld03b -six_dim $usesixdim
    Bpm -length 0.0
    Dipole -name "DIQ-right-$cell-2" -length 0.00000000000
    Quadrupole -name Quadrupole-right-$cell-2 -synrad $quad_synrad -length $lquad -strength [expr $kq03*$lquad*$refenergy] -thin_lens $numthinlenses -six_dim $usesixdim
    Drift -name Drift-right-$cell-6 -length $ld04 -six_dim $usesixdim
    Dipole -name "DIQ-right-$cell-3" -length 0.00000000000
    Bpm -length 0.0
    Quadrupole -name Quadrupole-right-$cell-3 -synrad $quad_synrad -length $lquad -strength [expr $kq04*$lquad*$refenergy] -thin_lens $numthinlenses -six_dim $usesixdim
    Drift -name Drift-right-$cell-7 -length $ld05 -six_dim $usesixdim
    Sbend -name Sbend-right-$cell-2 -length $larcb -angle [expr -$thetab] -six_dim $usesixdim -synrad $usesynrad -thin_lens $numthinlenses -e0 $refenergy -E1 [expr -$thetab/2] -E2 [expr -$thetab/2]
    set g0 [expr $refenergy/0.000510999]
    set de [expr 1/(6*$pi*$eps0)*$q0*$q0*$c*$g0*$g0*$g0*$g0/($rhob*$rhob)*$larcb/$c]
    set refenergy [expr $refenergy-$de/$q0/1e9*$usesynrad*$usesynrad_lattice]
    SetReferenceEnergy $refenergy
    Drift -name Drift-right-$cell-8 -length $ld06 -six_dim $usesixdim
    Sbend -name Sbend-right-$cell-3 -length $larcb -angle [expr -$thetab] -six_dim $usesixdim -synrad $usesynrad -thin_lens $numthinlenses -e0 $refenergy -E1 [expr -$thetab/2] -E2 [expr -$thetab/2]
    set g0 [expr $refenergy/0.000510999]
    set de [expr 1/(6*$pi*$eps0)*$q0*$q0*$c*$g0*$g0*$g0*$g0/($rhob*$rhob)*$larcb/$c]
    set refenergy [expr $refenergy-$de/$q0/1e9*$usesynrad*$usesynrad_lattice]
    SetReferenceEnergy $refenergy
    Drift -name Drift-right-$cell-9 -length $ld06 -six_dim $usesixdim
    Sbend -name Sbend-right-$cell-4 -length $larcb -angle [expr -$thetab] -six_dim $usesixdim -synrad $usesynrad -thin_lens $numthinlenses -e0 $refenergy -E1 [expr -$thetab/2] -E2 [expr -$thetab/2]
    set g0 [expr $refenergy/0.000510999]
    set de [expr 1/(6*$pi*$eps0)*$q0*$q0*$c*$g0*$g0*$g0*$g0/($rhob*$rhob)*$larcb/$c]
    set refenergy [expr $refenergy-$de/$q0/1e9*$usesynrad*$usesynrad_lattice]
    SetReferenceEnergy $refenergy
    Drift -name Drift-right-$cell-10 -length $ld07 -six_dim $usesixdim
    Dipole -name "DIQ-right-$cell-4" -length 0.00000000000
    Bpm -length 0.0
    Quadrupole -name Quadrupole-right-$cell-4 -synrad $quad_synrad -length $lquad -strength [expr $kq05*$lquad*$refenergy] -thin_lens $numthinlenses -six_dim $usesixdim
    Drift -name Drift-right-$cell-11 -length $ld08 -six_dim $usesixdim
    Dipole -name "DIQ-right-$cell-5" -length 0.00000000000
    Bpm -length 0.0
    Quadrupole -name Quadrupole-right-$cell-5 -synrad $quad_synrad -length $lquad -strength [expr $kq06*$lquad*$refenergy] -thin_lens $numthinlenses -six_dim $usesixdim
    Drift -name Drift-right-$cell-12 -length $ld09a -six_dim $usesixdim
    Multipole -name Multipole-right-$cell-3 -type 3 -length $lsext -e0 $refenergy -strength [expr -$ks02*$refenergy*$lsext] -thin_lens $numthinlenses -six_dim $usesixdim
    Drift -name Drift-right-$cell-13 -length $ld09b -six_dim $usesixdim
    Dipole -name "DIQ-right-$cell-6" -length 0.00000000000
    Bpm -length 0.0
    Quadrupole -name Quadrupole-right-$cell-6 -synrad $quad_synrad -length $lquad -strength [expr $kq07*$lquad*$refenergy] -thin_lens $numthinlenses -six_dim $usesixdim
    Drift -name Drift-right-$cell-14 -length $ld10a -six_dim $usesixdim
    Multipole -name Multipole-right-$cell-4 -type 3 -length $lsext -e0 $refenergy -strength [expr -$ks04*$refenergy*$lsext] -thin_lens $numthinlenses -six_dim $usesixdim
    Drift -name Drift-right-$cell-15 -length $ld10b -six_dim $usesixdim
    Sbend -name Sbend-right-$cell-5 -length $larca -angle [expr -$thetaa] -six_dim $usesixdim -synrad $usesynrad -thin_lens $numthinlenses -e0 $refenergy -E1 [expr -$thetaa/2] -E2 [expr -$thetaa/2]
    set g0 [expr $refenergy/0.000510999]
    set de [expr 1/(6*$pi*$eps0)*$q0*$q0*$c*$g0*$g0*$g0*$g0/($rhoa*$rhoa)*$larca/$c]
    set refenergy [expr $refenergy-$de/$q0/1e9*$usesynrad*$usesynrad_lattice]
    SetReferenceEnergy $refenergy
    Drift -name Drift-right-$cell-16 -length $ld11 -six_dim $usesixdim

    if {$cell==40} {
    } else {
        Dipole -name "DIQ-right-$cell-7" -length 0.00000000000
        Bpm -length 0.0
        Quadrupole -name Quadrupole-right-$cell-7 -synrad $quad_synrad -length $lquad -strength [expr $kq08*$lquad*$refenergy] -thin_lens $numthinlenses -six_dim $usesixdim
    }
}
set beamparams(meanenergy) $refenergy


puts "Setting beamparams(meanenergy)=$beamparams(meanenergy)."
}
