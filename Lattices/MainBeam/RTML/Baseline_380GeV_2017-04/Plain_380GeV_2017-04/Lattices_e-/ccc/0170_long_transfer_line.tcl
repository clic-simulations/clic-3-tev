#
# Long Transfer Line Lattice
#
# beamparams=[betax,alphax,emitnx,betay,alphay,emitny,sigmaz,charge,
#             uncespread,echirp,energy,nslice,nmacro,nsigmabunch,nsigmawake]
# units=[m,rad,m*rad,m,rad,m*rad,m,C,1,1/m,eV,1,1,1,1]
#
# where necessary units were converted to placet units in main.tcl

proc lattice_long_transfer_line {bparray} {
upvar $bparray beamparams

set usesixdim 1
set numthinlenses 100
set quad_synrad 0

set refenergy $beamparams(meanenergy)

set lquadm 0.36

set kqmx 0.009713020617
set kqm1 $kqmx
set kqm2 [expr $kqmx * -1.0]
set kqm3 $kqmx
set kqm4 [expr $kqmx * -1.0]

set ldm1 [expr 219.0 - 0.36]
set ldm2 $ldm1
set ldm3 $ldm1
set ldm4 $ldm1

SetReferenceEnergy $refenergy

Girder
Bpm -length 0.0
Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm1*$lquadm*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Dipole -name "D170-1" -length 0.00000000000
Drift -length $ldm1 -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm2*$lquadm*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Dipole -name "D170-2" -length 0.00000000000
Drift -length $ldm2 -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm3*$lquadm*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Dipole -name "D170-3" -length 0.00000000000
Drift -length $ldm3 -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm4*$lquadm*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Dipole -name "D170-4" -length 0.00000000000
Drift -length $ldm4 -six_dim $usesixdim

for {set sec 1} {$sec<=5} {incr sec} {
    Girder
    Bpm -length 0.0
    Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm1*$lquadm*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
    Dipole -name "D170-$sec-1" -length 0.00000000000
    Drift -length $ldm1 -six_dim $usesixdim
    Bpm -length 0.0
    Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm2*$lquadm*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
    Dipole -name "D170-$sec-2" -length 0.00000000000
    Drift -length $ldm2 -six_dim $usesixdim
    Bpm -length 0.0
    Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm3*$lquadm*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
    Dipole -name "D170-$sec-3" -length 0.00000000000
    Drift -length $ldm3 -six_dim $usesixdim
    Bpm -length 0.0
    Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm4*$lquadm*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
    Dipole -name "D170-$sec-4" -length 0.00000000000
    Drift -length $ldm4 -six_dim $usesixdim
    }
}
