#
# Spin rotator
#
# beamparams=[betax,alphax,emitnx,betay,alphay,emitny,sigmaz,charge,
#             uncespread,echirp,energy,nslice,nmacro,nsigmabunch,nsigmawake]
# units=[m,rad,m*rad,m,rad,m*rad,m,C,1,1/m,eV,1,1,1,1]
#
# where necessary units were converted to placet units in main.tcl

proc lattice_spin_rotator {bparray} {
upvar $bparray beamparams

set usecsr 0
set usesynrad $beamparams(useisr)
set usesixdim 1
set numthinlenses 100
set quad_synrad 0

set pi 3.141592653589793

set theta 0.04033666667
set lbendarc 1.0
set rho [expr $lbendarc/$theta]
set refenergy $beamparams(meanenergy)

set c 299792458
set q0 1.6021765e-19
set eps0 [expr 1/(4e-7*$pi*$c*$c)]


SetReferenceEnergy $refenergy

Girder

Solenoid -name "SOLENOID1" -length 1.3
Drift -name "D1S" -length 3.7 -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -name "SR" -name "QF01" -synrad $quad_synrad -length 0.36 -strength [expr 0.2530370303*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Dipole -name "DIQ-SR-1" -length 0.00000000000
Drift -name "D1" -length 5 -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -name "SR" -name "QD01" -synrad $quad_synrad -length 0.36 -strength [expr -0.1865980792*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Dipole -name "DIQ-SR-2" -length 0.00000000000
Drift -name "D1" -length 5 -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -name "SR" -name "QF1" -synrad $quad_synrad -length 0.36 -strength [expr 0.2530366575*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Dipole -name "DIQ-SR-3" -length 0.00000000000
Drift -name "D1" -length 5 -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -name "SR" -name "QD1" -synrad $quad_synrad -length 0.36 -strength [expr -0.1865980528*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Dipole -name "DIQ-SR-4" -length 0.00000000000
Drift -name "D1" -length 5 -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -name "SR" -name "QF1" -synrad $quad_synrad -length 0.36 -strength [expr 0.2530366575*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Dipole -name "DIQ-SR-5" -length 0.00000000000
Drift -name "D1" -length 5 -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -name "SR" -name "QD1" -synrad $quad_synrad -length 0.36 -strength [expr -0.1865980528*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Dipole -name "DIQ-SR-6" -length 0.00000000000
Drift -name "D1" -length 5 -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -name "SR" -name "QF1" -synrad $quad_synrad -length 0.36 -strength [expr 0.2530366575*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Dipole -name "DIQ-SR-7" -length 0.00000000000
Drift -name "D1" -length 5 -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -name "SR" -name "QD1" -synrad $quad_synrad -length 0.36 -strength [expr -0.1865980528*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Dipole -name "DIQ-SR-8" -length 0.00000000000
Solenoid -name "SOLENOID1" -length 1.3
Drift -name "M1D1" -length 1 -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -name "SR" -name "M1QF1" -synrad $quad_synrad -length 0.36 -strength [expr 0.5178288905*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Dipole -name "DIQ-SR-9" -length 0.00000000000
Drift -name "M1D2" -length 1 -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -name "SR" -name "M1QD2" -synrad $quad_synrad -length 0.36 -strength [expr -0.545118223*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Dipole -name "DIQ-SR-10" -length 0.00000000000
Drift -name "M1D3" -length 1 -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -name "SR" -name "M1QF3" -synrad $quad_synrad -length 0.36 -strength [expr 0.5425750504*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Dipole -name "DIQ-SR-11" -length 0.00000000000
Drift -name "M1D4" -length 1 -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -name "SR" -name "M1QD4" -synrad $quad_synrad -length 0.36 -strength [expr -0.3062493475*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Dipole -name "DIQ-SR-12" -length 0.00000000000
Drift -name "D2S" -length 2 -six_dim $usesixdim
Sbend -name "BEND1" -length $lbendarc -angle $theta -e0 $refenergy -E1 [expr 0*$theta/2] -E2 [expr 0*$theta/2] -csr $usecsr -synrad $usesynrad -six_dim $usesixdim -thin_lens $numthinlenses
set g0 [expr $refenergy/0.000510999]
set de [expr 1/(6*$pi*$eps0)*$q0*$q0*$c*$g0*$g0*$g0*$g0/($rho*$rho)*$lbendarc/$c]
set refenergy [expr $refenergy-$de/$q0*0e9*$usesynrad]
SetReferenceEnergy $refenergy
Drift -name "D2S" -length 2 -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -name "SR" -name "QF2" -synrad $quad_synrad -length 0.36 -strength [expr 0.3295122006*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Dipole -name "DIQ-SR-13" -length 0.00000000000
Drift -name "D2S" -length 2 -six_dim $usesixdim
Sbend -name "BEND1" -length $lbendarc -angle $theta -e0 $refenergy -E1 [expr 0*$theta/2] -E2 [expr 0*$theta/2] -csr $usecsr -synrad $usesynrad -six_dim $usesixdim -thin_lens $numthinlenses
set g0 [expr $refenergy/0.000510999]
set de [expr 1/(6*$pi*$eps0)*$q0*$q0*$c*$g0*$g0*$g0*$g0/($rho*$rho)*$lbendarc/$c]
set refenergy [expr $refenergy-$de/$q0*0e9*$usesynrad]
SetReferenceEnergy $refenergy
Drift -name "D2S" -length 2 -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -name "SR" -name "QD2" -synrad $quad_synrad -length 0.36 -strength [expr -0.3305696261*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Dipole -name "DIQ-SR-14" -length 0.00000000000
Drift -name "D2S" -length 2 -six_dim $usesixdim
Sbend -name "BEND1" -length $lbendarc -angle $theta -e0 $refenergy -E1 [expr 0*$theta/2] -E2 [expr 0*$theta/2] -csr $usecsr -synrad $usesynrad -six_dim $usesixdim -thin_lens $numthinlenses
set g0 [expr $refenergy/0.000510999]
set de [expr 1/(6*$pi*$eps0)*$q0*$q0*$c*$g0*$g0*$g0*$g0/($rho*$rho)*$lbendarc/$c]
set refenergy [expr $refenergy-$de/$q0*0e9*$usesynrad]
SetReferenceEnergy $refenergy
Drift -name "D2S" -length [expr 2.0 - 0.2] -six_dim $usesixdim
Multipole -type 3 -length 0.2 -e0 $refenergy -strength [expr 2.92*$refenergy*0.2] -thin_lens $numthinlenses -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -name "SR" -name "QF2" -synrad $quad_synrad -length 0.36 -strength [expr 0.3295122006*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Dipole -name "DIQ-SR-15" -length 0.00000000000
Drift -name "D2S" -length 2 -six_dim $usesixdim
Sbend -name "BEND1" -length $lbendarc -angle $theta -e0 $refenergy -E1 [expr 0*$theta/2] -E2 [expr 0*$theta/2] -csr $usecsr -synrad $usesynrad -six_dim $usesixdim -thin_lens $numthinlenses
set g0 [expr $refenergy/0.000510999]
set de [expr 1/(6*$pi*$eps0)*$q0*$q0*$c*$g0*$g0*$g0*$g0/($rho*$rho)*$lbendarc/$c]
set refenergy [expr $refenergy-$de/$q0*0e9*$usesynrad]
SetReferenceEnergy $refenergy
Drift -name "D2S" -length 2 -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -name "SR" -name "QD2" -synrad $quad_synrad -length 0.36 -strength [expr -0.3305696261*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Dipole -name "DIQ-SR-16" -length 0.00000000000
Drift -name "D2S" -length 2 -six_dim $usesixdim
Sbend -name "BEND1" -length $lbendarc -angle $theta -e0 $refenergy -E1 [expr 0*$theta/2] -E2 [expr 0*$theta/2] -csr $usecsr -synrad $usesynrad -six_dim $usesixdim -thin_lens $numthinlenses
set g0 [expr $refenergy/0.000510999]
set de [expr 1/(6*$pi*$eps0)*$q0*$q0*$c*$g0*$g0*$g0*$g0/($rho*$rho)*$lbendarc/$c]
set refenergy [expr $refenergy-$de/$q0*0e9*$usesynrad]
SetReferenceEnergy $refenergy
Drift -name "D2S" -length 2 -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -name "SR" -name "QF2" -synrad $quad_synrad -length 0.36 -strength [expr 0.3295122006*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Dipole -name "DIQ-SR-17" -length 0.00000000000
Drift -name "D2S" -length 2 -six_dim $usesixdim
Sbend -name "BEND1" -length $lbendarc -angle $theta -e0 $refenergy -E1 [expr 0*$theta/2] -E2 [expr 0*$theta/2] -csr $usecsr -synrad $usesynrad -six_dim $usesixdim -thin_lens $numthinlenses
set g0 [expr $refenergy/0.000510999]
set de [expr 1/(6*$pi*$eps0)*$q0*$q0*$c*$g0*$g0*$g0*$g0/($rho*$rho)*$lbendarc/$c]
set refenergy [expr $refenergy-$de/$q0*0e9*$usesynrad]
SetReferenceEnergy $refenergy
Drift -name "D2S" -length 2 -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -name "SR" -name "QD2" -synrad $quad_synrad -length 0.36 -strength [expr -0.3305696261*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Dipole -name "DIQ-SR-18" -length 0.00000000000
Drift -name "M2D1" -length 1 -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -name "SR" -name "M2QF1" -synrad $quad_synrad -length 0.36 -strength [expr 0.05035268685*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Dipole -name "DIQ-SR-19" -length 0.00000000000
Drift -name "M2D2" -length 1 -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -name "SR" -name "M2QD2" -synrad $quad_synrad -length 0.36 -strength [expr 0.2872202639*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Dipole -name "DIQ-SR-20" -length 0.00000000000
Drift -name "M2D3" -length 1 -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -name "SR" -name "M2QF3" -synrad $quad_synrad -length 0.36 -strength [expr 0.2064250276*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Dipole -name "DIQ-SR-21" -length 0.00000000000
Drift -name "M2D4" -length 1 -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -name "SR" -name "M2QD4" -synrad $quad_synrad -length 0.36 -strength [expr -0.3232044214*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Dipole -name "DIQ-SR-22" -length 0.00000000000
Solenoid -name "SOLENOID2" -length 1.3
Drift -name "D1S" -length 3.7 -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -name "SR" -name "QF1" -synrad $quad_synrad -length 0.36 -strength [expr 0.2530366575*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Dipole -name "DIQ-SR-23" -length 0.00000000000
Drift -name "D1" -length 5 -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -name "SR" -name "QD1" -synrad $quad_synrad -length 0.36 -strength [expr -0.1865980528*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Dipole -name "DIQ-SR-24" -length 0.00000000000
Drift -name "D1" -length 5 -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -name "SR" -name "QF1" -synrad $quad_synrad -length 0.36 -strength [expr 0.2530366575*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Dipole -name "DIQ-SR-25" -length 0.00000000000
Drift -name "D1" -length 5 -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -name "SR" -name "QD1" -synrad $quad_synrad -length 0.36 -strength [expr -0.1865980528*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Dipole -name "DIQ-SR-26" -length 0.00000000000
Drift -name "D1" -length 5 -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -name "SR" -name "QF1" -synrad $quad_synrad -length 0.36 -strength [expr 0.2530366575*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Dipole -name "DIQ-SR-27" -length 0.00000000000
Drift -name "D1" -length 5 -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -name "SR" -name "QD1" -synrad $quad_synrad -length 0.36 -strength [expr -0.1865980528*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Dipole -name "DIQ-SR-28" -length 0.00000000000
Drift -name "D1" -length 5 -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -name "SR" -name "QF1" -synrad $quad_synrad -length 0.36 -strength [expr 0.2530366575*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Dipole -name "DIQ-SR-29" -length 0.00000000000
Drift -name "D1" -length 5 -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -name "SR" -name "QD1" -synrad $quad_synrad -length 0.36 -strength [expr -0.1865980528*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Dipole -name "DIQ-SR-30" -length 0.00000000000
Solenoid -name "SOLENOID2" -length 1.3
set beamparams(meanenergy) $refenergy
#puts "Setting beamparams(meanenergy)=$beamparams(meanenergy)."
}
