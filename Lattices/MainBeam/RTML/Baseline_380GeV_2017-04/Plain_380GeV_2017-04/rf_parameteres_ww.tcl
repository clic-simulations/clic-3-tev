set rfparamsbooster(gradientww)   1.489874673924e+07
set rfparamsbc1(gradientww)   1.327200053774e+07
set rfparamsbc2(gradientww)   7.190473029005e+07
set energy_wake_bc1  -3.527322178331e-05
set energy_wake_booster  -5.769866861638e-05
set energy_wake_bc2  -7.433900726715e-05
