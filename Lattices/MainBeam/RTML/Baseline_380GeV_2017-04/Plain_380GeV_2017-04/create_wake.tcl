source Parameters/rf_parameters_e-.tcl
source Parameters/initial_beam_parameters_e-.tcl
source Scripts/wake_init.tcl

Octave {
energy_start = $beam0010(energy)/1e9;
energy_end = $beam0290(energy)/1e9;
energy_increase_booster = energy_end - energy_start;
N_rf_booster = 276;
Length_rf_booster = 1.5;
gradient_booster = energy_increase_booster / (N_rf_booster * Length_rf_booster);

###### ISR energy loss --- Central Arc and TAL
E = energy_end;  % Average
m_electron = 0.000510999; % GeV
charge_electron = 1.6021765e-19;
epsilon_0 = 8.854187817e-12;
c = 299792458;
length_sbend = 2.0;
theta = 1.2 * pi /180; % radian
rho = length_sbend / ( 2 * sin(theta/2));
length_arc = rho * theta;
P_ISR = charge_electron^2 * c * (E/m_electron)^4 / (6 * pi * epsilon_0 * rho^2);
N_sbend_per_cell = 5;
N_cell_arc = 30;
N_cell_tal = 50;
length_total = length_arc * N_sbend_per_cell * (N_cell_tal);
E_ISR = P_ISR * length_total /c / charge_electron/1e9; % GeV

%%%%%%%%%%%%%%%%%%%%% BC1 and BC2 sbend 

E_bc1 = energy_start;
theta_bc1 = 4.538575 / 180 * pi;
length_sbend_bc1 = 1.5;
rho_bc1 = length_sbend_bc1 / sin(theta_bc1);
length_arc_bc1 = theta_bc1 * rho_bc1;
P_ISR_bc1 = charge_electron^2 * c * (E_bc1/m_electron)^4 / (6 * pi * epsilon_0 * rho_bc1^2);
E_ISR_bc1 = P_ISR_bc1 * length_arc_bc1 * 4 /c / charge_electron/1e9; % GeV

E_bc2_1 = energy_end;
theta_bc2_1 = 1.733801 / 180 * pi;
length_sbend_bc2_1 = 1.5;
rho_bc2_1 = length_sbend_bc2_1/sin(theta_bc2_1);
length_arc_bc2_1 = theta_bc2_1 * rho_bc2_1;
P_ISR_bc2_1 = charge_electron^2 * c * (E_bc2_1/m_electron)^4 / (6 * pi * epsilon_0 * rho_bc2_1^2);
E_ISR_bc2_1 = P_ISR_bc2_1 * length_arc_bc2_1 * 4 /c / charge_electron/1e9; % GeV

E_bc2_2 = energy_end;
theta_bc2_2 = 0.326512 / 180 * pi;
length_sbend_bc2_2 = 1.5;
rho_bc2_2 = length_sbend_bc2_2/sin(theta_bc2_2);
length_arc_bc2_2 = theta_bc2_2 * rho_bc2_2;
P_ISR_bc2_2 = charge_electron^2 * c * (E_bc2_2/m_electron)^4 / (6 * pi * epsilon_0 * rho_bc2_2^2);
E_ISR_bc2_2 = P_ISR_bc2_2 * length_arc_bc2_2 * 4 /c / charge_electron/1e9; % GeV


%%%%%%%%%%%%%%%%%%%%% Vertical Transfer
E_vt = energy_end;
theta_vt = 1.0 / 180 * pi;
length_vt = 1.5;
rho_vt =  length_vt / (2*sin(theta_vt/2));
length_arc_vt = theta_vt * rho_vt;
P_ISR_vt = charge_electron^2 * c * (E_vt/m_electron)^4 / (6 * pi * epsilon_0 * rho_vt^2);
E_ISR_vt = P_ISR_bc2_1 * length_arc_vt * 6 / c / charge_electron/1e9; % GeV; 

gradient_booster +=  (E_ISR + E_ISR_bc1 + E_ISR_bc2_1 + E_ISR_bc2_2 + E_ISR_vt) / (N_rf_booster * Length_rf_booster);

####### Wakefield Energy loss
sigma_bc1 = 1800; % um
sigma_booster = 250 ;
sigma_bc2 = 250;
N_slice = 512;
charge = 850; % pC
Lcav_bc1 = 1.5; % m
Lcav_booster = 1.5; % m
Lcav_bc2 = 0.23; % m
N_rf_bc1 = 20;
N_rf_booster = 276;
N_rf_bc2 = 78;


## BC1
Z_bc1 = linspace(0, sigma_bc1 * 10, N_slice) * 1e-6; % m
Z_booster = linspace(0, sigma_booster * 10, N_slice) * 1e-6; % m
Z_bc2 = linspace(0, sigma_bc2 * 10, N_slice)* 1e-6; % m

Wt_bc1 = W_transv(Z_bc1,$rfparamsbc1(a),$rfparamsbc1(g),$rfparamsbc1(l),$rfparamsbc1(delta),$rfparamsbc1(delta_g));
Wl_bc1 = W_long(Z_bc1,$rfparamsbc1(a),$rfparamsbc1(g),$rfparamsbc1(l),$rfparamsbc1(delta),$rfparamsbc1(delta_g));

Wl_bc1_new = Wl_bc1; Wl_bc1_new(1) /= 2.0;

charge_Z_bc1 = sort(normrnd(0,sigma_bc1 * 1e-6,[100000,1])); % m
stepW = Z_bc1(2) - Z_bc1(1); % m
edge = charge_Z_bc1(1):stepW:charge_Z_bc1(end); % m

distri_charge_Z = histc(charge_Z_bc1,edge);
distri_charge_Z = distri_charge_Z/sum(distri_charge_Z);

lambda = distri_charge_Z * charge; % pC
loss = conv(lambda,Wl_bc1_new); % V/pC/m * pC = V/m

mean_loss_bc1 = distri_charge_Z'* loss(1:length(distri_charge_Z))' * Lcav_bc1 * N_rf_bc1 * 1e-9;

#slope_bc1 = (sum(distri_charge_Z' .* (edge .* loss(1:length(distri_charge_Z)))) - sum(distri_charge_Z' .* edge) * sum(distri_charge_Z' .* loss(1:length(distri_charge_Z)))) /( sum(distri_charge_Z' .* (edge .**2)) - (sum(distri_charge_Z' .* edge))**2);

index_bin_start = round((-sigma_bc1*1e-6 - edge(1))/stepW);
index_bin_end = round((sigma_bc1*1e-6 - edge(1))/stepW);
P = polyfit(edge(index_bin_start:index_bin_end),loss(index_bin_start:index_bin_end),1);

slope_bc1 = P(1);

gradient_bc1 = $rfparamsbc1(gradient)/1e9 + slope_bc1/1e9 / (2 * pi / $rfparamsbc1(lambda));


filet = fopen('Wt_bc1.dat', 'w');
filel = fopen('Wl_bc1.dat', 'w');

fprintf(filet, ' %.15g %.15g\n', [ Z_bc1 ; Wt_bc1 ] );
fprintf(filel, ' %.15g %.15g\n', [ Z_bc1 ; Wl_bc1 ] );

fclose(filet);
fclose(filel);

## Booster


Wt_booster = W_transv(Z_booster,$rfparamsbooster(a),$rfparamsbooster(g),$rfparamsbooster(l),$rfparamsbooster(delta),$rfparamsbooster(delta_g));
Wl_booster = W_long(Z_booster,$rfparamsbooster(a),$rfparamsbooster(g),$rfparamsbooster(l),$rfparamsbooster(delta),$rfparamsbooster(delta_g));

Wl_booster_new = Wl_booster; Wl_booster_new(1) /= 2.0;

charge_Z_booster = sort(normrnd(0,sigma_booster * 1e-6,[100000,1])); % m
stepW = Z_booster(2) - Z_booster(1); % m
edge = charge_Z_booster(1):stepW:charge_Z_booster(end); % m

distri_charge_Z = histc(charge_Z_booster,edge);
distri_charge_Z = distri_charge_Z/sum(distri_charge_Z);

lambda = distri_charge_Z * charge; % pC
loss = conv(lambda,Wl_booster_new); % V/pC/m * pC = V/m

mean_loss_booster = distri_charge_Z'* loss(1:length(distri_charge_Z))' * Lcav_booster * N_rf_booster * 1e-9;

filet = fopen('Wt_booster.dat', 'w');
filel = fopen('Wl_booster.dat', 'w');

fprintf(filet, ' %.15g %.15g\n', [ Z_booster ; Wt_booster ] );
fprintf(filel, ' %.15g %.15g\n', [ Z_booster ; Wl_booster ] );

fclose(filet);
fclose(filel);


## BC2
Wt_bc2 = W_transv(Z_bc2,$rfparamsbc2(a),$rfparamsbc2(g),$rfparamsbc2(l),$rfparamsbc2(delta),$rfparamsbc2(delta_g));
Wl_bc2 = W_long(Z_bc2,$rfparamsbc2(a),$rfparamsbc2(g),$rfparamsbc2(l),$rfparamsbc2(delta),$rfparamsbc2(delta_g));

Wl_bc2_new = Wl_bc2; Wl_bc2_new(1) /= 2.0;

charge_Z_bc2 = sort(normrnd(0,sigma_bc2 * 1e-6,[100000,1])); % m
stepW = Z_bc2(2) - Z_bc2(1); % m
edge = charge_Z_bc2(1):stepW:charge_Z_bc2(end); % m

distri_charge_Z = histc(charge_Z_bc2,edge);
distri_charge_Z = distri_charge_Z/sum(distri_charge_Z);

lambda = distri_charge_Z * charge; % pC
loss = conv(lambda,Wl_bc2_new); % V/pC/m * pC = V/m

mean_loss_bc2 = distri_charge_Z'* loss(1:length(distri_charge_Z))' * Lcav_bc2 * N_rf_bc2 * 1e-9;


#slope_bc2 = (sum(distri_charge_Z' .* (edge .* loss(1:length(distri_charge_Z)))) - sum(distri_charge_Z' .* edge) * sum(distri_charge_Z' .* loss(1:length(distri_charge_Z)))) /( sum(distri_charge_Z' .* (edge .**2)) - (sum(distri_charge_Z' .* edge))**2)

index_bin_start = round((-sigma_bc2*0.6*1e-6 - edge(1))/stepW);
index_bin_end = round((sigma_bc2*0.6*1e-6 - edge(1))/stepW);
P = polyfit(edge(index_bin_start:index_bin_end),loss(index_bin_start:index_bin_end),1);

slope_bc2 = P(1) * 1.2;

gradient_bc2 = $rfparamsbc2(gradient)/1e9 + slope_bc2/1e9 / (2 * pi / $rfparamsbc2(lambda));

filet = fopen('Wt_bc2.dat', 'w');
filel = fopen('Wl_bc2.dat', 'w');

fprintf(filet, ' %.15g %.15g\n', [ Z_bc2 ; Wt_bc2 ] );
fprintf(filel, ' %.15g %.15g\n', [ Z_bc2 ; Wl_bc2 ] );

fclose(filet);
fclose(filel);

Energy_loss_wake = mean_loss_bc1 + mean_loss_booster + mean_loss_bc2;
gradient_booster += Energy_loss_wake / (N_rf_booster * Length_rf_booster);

ofile = fopen('rf_parameteres_ww.tcl','w');
fprintf(ofile, "set rfparamsbooster(gradientww) %20.12e\n", gradient_booster*1e9);
fprintf(ofile, "set rfparamsbc1(gradientww) %20.12e\n", gradient_bc1*1e9);
fprintf(ofile, "set rfparamsbc2(gradientww) %20.12e\n", gradient_bc2*1e9);
fprintf(ofile, "set energy_wake_bc1 %20.12e\n", -1 * mean_loss_bc1 / N_rf_bc1);
fprintf(ofile, "set energy_wake_booster %20.12e\n", -1 * mean_loss_booster / N_rf_booster);
fprintf(ofile, "set energy_wake_bc2 %20.12e\n", -1 * mean_loss_bc2 / N_rf_bc2);
fclose(ofile);



}
