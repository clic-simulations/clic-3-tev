Octave {

      Sbends  = placet_get_name_number_list("rtml", "080S");
      Sbends = [Sbends placet_get_name_number_list("rtml", "230S")]
      Sbends = [Sbends placet_get_name_number_list("rtml", "250S")]
      SI = Sbends;

      placet_element_set_attribute("rtml", SI, "thin_lens", 0);
      placet_element_set_attribute("rtml", SI, "csr", true);
      placet_element_set_attribute("rtml", SI, "csr_charge", $charge);
      placet_element_set_attribute("rtml", SI, "csr_nbins", 510);
      placet_element_set_attribute("rtml", SI, "csr_nsectors", 100);
      placet_element_set_attribute("rtml", SI, "csr_filterorder", 1);
      placet_element_set_attribute("rtml", SI, "csr_nhalffilter", 10);
      #placet_element_set_attribute("rtml", SI, "csr_savesectors", logical(1));
      placet_element_set_attribute("rtml", SI, "csr_enforce_steady_state", true);


      #placet_element_set_attribute("rtml", SI, "csr_shielding",true);
      #placet_element_set_attribute("rtml", SI, "csr_shielding_n_images",1);
      #placet_element_set_attribute("rtml", SI, "csr_shielding_height",0.01);
      #placet_element_set_attribute("rtml", SI, "enable_csr_shielding_width",true);
      #placet_element_set_attribute("rtml", SI, "csr_shielding_width",0.01);

}
