# CLIC Main Beam RTML

##### If No wakefield file, run the create_wake.tcl script
#source create_wake.tcl
######

set tcl_precision 15
ParallelThreads -num 4

proc FF1 {} {}
proc FF2 {} {}

set latticedir ./Lattices_e-
set scriptdir ./Scripts
set paramsdir ./Parameters

source $scriptdir/placet_units.tcl
source $paramsdir/initial_beam_parameters_e-.tcl
source $paramsdir/rf_parameters_e-.tcl
source $scriptdir/lattice_fun.tcl
source $scriptdir/beamsetup.tcl
source $scriptdir/cavitywakesetup.tcl
source $scriptdir/octave_fun.tcl
source $scriptdir/setParam.tcl

if {$beamparams(usewakefields)==1} {
   source rf_parameteres_ww.tcl
}

create_particles_file particles.in beamparams
create_zero_wakes_file zero_wake.dat beamparams rfparamsbc1 particles.in

BeamlineNew
source $scriptdir/beamline_and_wake.tcl
BeamlineSet -name rtml

Octave {

    global COLS_NAME
    COLS_NAME = { "S" ; "L" ; "E0" ; "K1L" ; "K1SL" ; "K2L" ; "K2SL" ; "KS" ; "ANGLE" ; "E1" ; "E2" ; "Strength" }; % plus "NAME" ; "KEYWORD"
    ALL = placet_element_get_attributes("rtml");
    TABLE_NAME = [];
    TABLE_KEY  = [];
    TABLE_DATA = [];
    function T = append_to_row(T, name, value)
        global COLS_NAME
        T(find(strcmp(COLS_NAME, name))) = value;
    end

    %% element by element
    sigma_quad = 30; % um, rms misalignment of quadrupoles
    last_quad_K1L = 0.0; % 1/m
    
    index = 1;    
    for i=1:size(ALL,1);
        T = zeros(1, size(COLS_NAME,1));
        T = append_to_row(T, "S", ALL{i}.s);
        T = append_to_row(T, "L", ALL{i}.length);
        T = append_to_row(T, "E0", ALL{i}.e0);
        switch ALL{i}.type_name
          case "quadrupole"
            T = append_to_row(T, "K1L", ALL{i}.strength / ALL{i}.e0);
            T = append_to_row(T, "Strength", ALL{i}.strength / 0.299792458); % GV/c/m = (1 / 0.299792458) T
            last_quad_K1L = ALL{i}.strength / ALL{i}.e0; % 1/m
         case "sbend"
            T = append_to_row(T, "ANGLE", ALL{i}.angle);
            T = append_to_row(T, "E1", ALL{i}.E1);
            T = append_to_row(T, "E2", ALL{i}.E2);
            T = append_to_row(T, "Strength", ALL{i}.e0 * ALL{i}.angle / 0.299792458); % GV/c = (1 / 0.299792458) T*m
         case "multipole"
            ALL{i}.type_name = "sextupole";
            T = append_to_row(T, "K2L", ALL{i}.strength / ALL{i}.e0);
            T = append_to_row(T, "Strength", ALL{i}.strength / 0.299792458); % GV/c/m^2 = (1 / 0.299792458) T/m
          case "bpm"
          case "dipole"
            ALL{i}.type_name = "kicker";
            max_strength = 3*sigma_quad*abs(last_quad_K1L)*ALL{i}.e0*1e3; % V, 3 * sigma_quad * last_quad_K1L * ALL{i}.e0
            T = append_to_row(T, "Strength", max_strength / 299792458); % T*m
          case "cavity"
            T = append_to_row(T, "Strength", 1e3 * ALL{i}.gradient * ALL{i}.length); % MV
            T = append_to_row(T, "ANGLE", ALL{i}.phase / 180.0 * pi); % rad
          case "solenoid"
            T = append_to_row(T, "KS", 0.29979246 * ALL{i}.bz); % solenoid strength from T/(GeV/c) to 1/m
            T = append_to_row(T, "Strength", 2.9); % T
          case "drift"
            if strfind(ALL{i}.name, "MARKER")
              ALL{i}.type_name = "marker";
            endif
        end
        
        %% for all types
        switch ALL{i}.type_name
          case { "quadrupole" , "sbend", "sextupole", "solenoid", "bpm" , "kicker" , "cavity", "marker" }
            TABLE_DATA(index,:) = T;
            TABLE_NAME{index}   = ALL{i}.name;
            TABLE_KEY {index}   = toupper(ALL{i}.type_name);
            index++;
        end
    end
    
    %% save on disk
    fid = fopen("elements_table.txt", 'w');
    if fid != -1
        fprintf(fid, '* NAME\tKEYWORD\t%s\n', strjoin (COLS_NAME, '\t'));
        fprintf(fid, '* <STR> <STR> [m] [m] [GeV] [m^-1] [m^-1] [m^-2] [m^-2] [m^-1] [rad] [rad] [rad]\n');
        for i=1:size(TABLE_DATA,1)
            fprintf(fid, [ '\"%s\"\t\"%s\"\t%.3f\t' repmat('%g\t', 1, size(TABLE_DATA,2)-1) ], TABLE_NAME{i}, TABLE_KEY{i}, TABLE_DATA(i,:));
            fprintf(fid, '\n');
        end
        fclose(fid);
    end
}
