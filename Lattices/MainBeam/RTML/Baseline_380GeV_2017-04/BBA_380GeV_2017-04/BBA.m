for machine=START_MACHINE:END_MACHINE
source Scripts/func_min.m
save Machine.dat machine

global counter = 0;
global MaxIter = 200;
options = [0;1e-3;0;0;1;0;0;0;0;MaxIter];

X=[0;0;0;0;0;0;0;0;0;0];
save X_ca.dat X;
save X_tal.dat X;


##########   OTO and DFS
system(['placet -s main_e-_bba.tcl machine ' num2str(machine) ' >& placet.out'])

system(['placet -s main_e-_boo.tcl machine ' num2str(machine)])
[CAk, cor_min] = fminsearch("correction1", X,options)

X=CAk;

save X_ca.dat X;

counter = 0;

X=[0;0;0;0;0;0;0;0;0;0];
save X_tal.dat X;
system(['placet -s main_e-_ltl.tcl machine ' num2str(machine)])
[CAk, cor_min] = fminsearch("correction2", X,options)

X=CAk;

save X_tal.dat X;

system(['placet -s main_e-_tal.tcl machine ' num2str(machine)])


###################################################################
counter = 0;
load X_tal.dat;
system(['placet -s main_e-_ltl.tcl machine ' num2str(machine)])
[CAk, cor_min] = fminsearch("correction3", X,options)

X=CAk;

save X_tal.dat X;

system(['placet -s main_e-_tal.tcl machine ' num2str(machine)])


end
