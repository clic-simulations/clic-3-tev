proc getResponseMatrix-boo {name beamline Start End beam ds} {
  Octave {
      if exist('R_parm_${name}.dat', "file")
          load 'R_parm_${name}.dat';
      else
          Response.Corrector_all = placet_get_number_list("$beamline","dipole");
          Response.Bpms_all = placet_get_number_list("$beamline","bpm");

          Response.Start = placet_get_name_number_list("$beamline","$Start");
          Response.End = placet_get_name_number_list("$beamline","$End");

          Response.Corrector = Response.Corrector_all((Response.Corrector_all > Response.Start) & (Response.Corrector_all < Response.End));
          Response.Bpms = Response.Bpms_all((Response.Bpms_all > Response.Start) & (Response.Bpms_all < Response.End));
  
          # picks all correctors preceeding the last bpm, and all bpms following the first corrector
          Response.Corrector = Response.Corrector(Response.Corrector < Response.Bpms(end));
          Response.Bpms = Response.Bpms(Response.Bpms > max(Response.Corrector(1)));
  
  
          if exist('R_parm_${name}_1.dat', "file")
              load 'R_parm_${name}_1.dat';
          else
              placet_test_no_correction("$beamline", "$beam", "Zero",1,Response.Start,Response.End);
              Response.B0 = placet_get_bpm_readings("$beamline", Response.Bpms);
              printf("We are in B0\n"); 
              Response.R0x = placet_get_response_matrix_attribute_local("$beamline", "$beam", Response.Bpms, "x", Response.Corrector, "strength_x", "Zero",Response.Start,Response.End);
              printf("We are in R0x\n"); 
              Response.R0y = placet_get_response_matrix_attribute_local("$beamline", "$beam", Response.Bpms, "y", Response.Corrector, "strength_y", "Zero",Response.Start,Response.End);
              printf("We are in R0y\n"); 
              save -text 'R_parm_${name}_1.dat' Response;
          end

          decrease_grad("$beamline",$ds,Response.Start,Response.End);
          placet_test_no_correction("$beamline", "$beam", "Zero",1,Response.Start,Response.End);
          Response.B1 = placet_get_bpm_readings("$beamline", Response.Bpms) - Response.B0;
  
          Response.R1x = placet_get_response_matrix_attribute_local("$beamline", "$beam", Response.Bpms, "x", Response.Corrector, "strength_x", "Zero",Response.Start,Response.End) - Response.R0x;
          printf("We are in R1x\n"); 
          Response.R1y = placet_get_response_matrix_attribute_local("$beamline", "$beam", Response.Bpms, "y", Response.Corrector, "strength_y", "Zero",Response.Start,Response.End) - Response.R0y;
          printf("We are in R1y\n"); 
  
          reset_grad("$beamline",$ds,Response.Start,Response.End);
  
          save -text 'R_parm_${name}.dat' Response;
      end
  }
}
