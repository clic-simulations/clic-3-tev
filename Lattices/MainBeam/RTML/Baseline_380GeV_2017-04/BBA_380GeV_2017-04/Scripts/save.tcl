Octave {
    magnets = placet_get_number_list("rtml", "quadrupole");
    magnets = [ magnets placet_get_number_list("rtml", "sbend") ];
    magnets = [ magnets placet_get_number_list("rtml", "multipole") ];
    mag.x = placet_element_get_attribute("rtml", magnets, "x");
    mag.y = placet_element_get_attribute("rtml", magnets, "y" );
    mag.xp = placet_element_get_attribute("rtml", magnets, "xp");
    mag.yp = placet_element_get_attribute("rtml", magnets, "yp");
    mag.roll = placet_element_get_attribute("rtml", magnets, "roll");

    magnets = placet_get_number_list("rtml", "quadrupole");
    magnets = [ magnets placet_get_number_list("rtml", "multipole") ];
    STRENGTH = placet_element_get_attribute("rtml", magnets, "strength");
    mag.strength = STRENGTH;

    magnets = placet_get_number_list("rtml", "sbend");
    ANGLE = placet_element_get_attribute("rtml", magnets, "e0");
    mag.e0 = ANGLE;

    BI = placet_get_number_list("rtml","bpm");
    Bpm.x = placet_element_get_attribute("rtml",BI, "x");
    Bpm.y = placet_element_get_attribute("rtml",BI, "y");
    Bpm.xp = placet_element_get_attribute("rtml",BI, "xp");
    Bpm.yp = placet_element_get_attribute("rtml",BI, "yp");
    Bpm.roll = placet_element_get_attribute("rtml",BI, "roll");
    Bpm.res = placet_element_get_attribute("rtml",BI, "resolution");

    save -text mag-$machine.dat mag Bpm;
}

Octave {
Corrector_all = placet_get_number_list("rtml","dipole");
dipole_x = placet_element_get_attribute("rtml", Corrector_all, 'strength_x');
dipole_y = placet_element_get_attribute("rtml", Corrector_all, 'strength_y');
save -text dipole-$machine.dat dipole_x dipole_y;
}
