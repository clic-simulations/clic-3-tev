function demitt = correction1(X)


  global counter;
  global MaxIter

  counter += 1

  #if (counter > MaxIter)
  #  exit
  #end
  location='correction1'

  load Machine.dat;
  save X_ca.dat X location counter;

  ret = system(['placet -s main_e-_ca.tcl machine ' num2str(machine)])

  if( ret == 0 )
      R = load(['Emitt0-CA-rtml-30-' num2str(machine) '.dat']).E;
      emitx = mean(R(end-5:end,2))*(1+0.01*randn());
      emity = mean(R(end-10:end-5,6))*(1+0.01*randn());
      demitt = emitx/7 + emity/0.05;
      #if ( emity < 0.062 && emitx < 5.3)
      #   counter = MaxIter
      #end 
  else
      demitt = 100000000;
  end

  #fid = fopen('out1.dat','a');
  #fprintf(fid,'%e %e %e %e %e\n',k1,k2,k3,k4,demitt);
  #fclose(fid);

end

function demitt = correction2(X)

  global counter;
  global MaxIter
  counter += 1

  if (counter > MaxIter)
    exit
  end

  location='correction2'

  save X_tal.dat X location counter;
  load Machine.dat;

  ret = system(['placet -s main_e-_tal.tcl machine ' num2str(machine)])

  if( ret == 0 )
      R = load(['Emitt0-ALL-rtml-30-' num2str(machine) '.dat']).E;
      emitx = mean(R(end-5:end,2))*(1+0.01*randn());
      emity = mean(R(end-10:end-5,6))*(1+0.01*randn());
      demitt = emitx/10 + emity/0.05;
      if ( emity < 0.076 && emitx < 8.1)
         counter = MaxIter;
      end
  else
      demitt = 100000000;
  end
  #fid = fopen('out2.dat','a');
  #fprintf(fid,'%e %e %e %e %e\n',k1,k2,k3,k4,demitt);
  #fclose(fid);
end

function demitt = correction3(X)

  global counter;
  global MaxIter
  counter += 1

  if (counter > MaxIter)
    exit
  end

  location='correction3'

  save X_tal.dat X location counter;
  load Machine.dat;

  ret = system(['placet -s main_e-_tal.tcl machine ' num2str(machine)])

  if( ret == 0 )
      R = load(['Emitt0-ALL-rtml-30-' num2str(machine) '.dat']).E;
      emitx = mean(R(end-5:end,2))*(1+0.01*randn());
      emity = mean(R(end-10:end-5,6))*(1+0.01*randn());
      demitt = 5*emitx/7 + emity/0.05;
      if ( emity < 0.076 && emitx < 8.15)
         counter = MaxIter;
      end
  else
      demitt = 100000000;
  end
  #fid = fopen('out2.dat','a');
  #fprintf(fid,'%e %e %e %e %e\n',k1,k2,k3,k4,demitt);
  #fclose(fid);
end


function demitt = correctionSlove(X)
  demitt = zeros(4,1);
  k1 = X(1);
  k2 = X(2);
  k3 = X(3);
  k4 = X(4);

  l = 0.3;

  load 'B_cov.dat';

  HALFPI = [0,1,0,0;-1,0,0,0;0,0,0,1;0,0,-1,0];
  PI = [-1,0,0,0;0,-1,0,0;0,0,-1,0;0,0,0,-1];
  PIHALFPI = [-1,0,0,0;0,-1,0,0;0,0,0,1;0,0,-1,0];
  ZeroHALFPI = [1,0,0,0;0,1,0,0;0,0,0,1;0,0,-1,0];
  M1 = [1,0,0,0;0,1,-k1*l,0;0,0,1,0;-k1*l,0,0,1];
  M2 = [1,0,0,0;0,1,-k2*l,0;0,0,1,0;-k2*l,0,0,1];
  M3 = [1,0,0,0;0,1,-k3*l,0;0,0,1,0;-k3*l,0,0,1];
  M4 = [1,0,0,0;0,1,-k4*l,0;0,0,1,0;-k4*l,0,0,1];

  M_trans = ZeroHALFPI*M4*HALFPI*M3*PIHALFPI*M2*HALFPI*M1;
  M_beam = B_cov;
  M_beam = M_trans * M_beam * transpose(M_trans);

#emitt_x = sqrt(det(M_beam(1:2,1:2)));
#emitt_y = sqrt(det(M_beam(3:4,3:4)));


  demitt(1) = M_beam(1,3);
  demitt(2) = M_beam(1,4);
  demitt(3) = M_beam(2,3);
  demitt(4) = M_beam(2,4);
end

function demitt = correctionSearch(X)
  k1 = X(1);
  k2 = X(2);
  k3 = X(3);
  k4 = X(4);
  load 'B_cov.dat';
  M_beam = B_cov;
  l = 0.3;

  HALFPI = [0,1,0,0;-1,0,0,0;0,0,0,1;0,0,-1,0];
  PI = [-1,0,0,0;0,-1,0,0;0,0,-1,0;0,0,0,-1];
  PIHALFPI = [-1,0,0,0;0,-1,0,0;0,0,0,1;0,0,-1,0];
  ZeroHALFPI = [1,0,0,0;0,1,0,0;0,0,0,1;0,0,-1,0];
  M1 = [1,0,0,0;0,1,-k1,0;0,0,1,0;-k1,0,0,1];
  M2 = [1,0,0,0;0,1,-k2,0;0,0,1,0;-k2,0,0,1];
  M3 = [1,0,0,0;0,1,-k3,0;0,0,1,0;-k3,0,0,1];
  M4 = [1,0,0,0;0,1,-k4,0;0,0,1,0;-k4,0,0,1];

  M_trans = ZeroHALFPI*M4*HALFPI*M3*PIHALFPI*M2*HALFPI*M1;
  M_beam = M_trans * M_beam * transpose(M_trans);
  demitt = M_beam(1,3)^2 + M_beam(1,4)^2 + M_beam(2,3)^2 + M_beam(2,4)^2;
end

