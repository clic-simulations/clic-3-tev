array set args {
    machine 1
    sigma 30
}
array set args $argv
set machine $args(machine)
set sigma $args(sigma)

set beamparams(nbunches) 1
set beamparams(nslice) 100
set beamparams(nmacro) 100
set beamparams(nsigmabunch) 4
set beamparams(nsigmawake) 5
set beamparams(useisr) 1
set beamparams(useisr_lattice) 0
set beamparams(usewakefields) 1
set flag 1
set bpmres 1
set charge 0.85e-9

#Octave {
#        randn("seed", 876 * 1e4);
#}


# this is needed to concatenate data at the correct z position since each step
# restarts at z=0.0
set zoffset 0.0
set fid [open zoffset.dat w]
close $fid
#
# beamparams=[betax,alphax,emitnx,betay,alphay,emitny,sigmaz,charge,
#             uncespread,echirp,energy,nslice,nmacro,nsigma]
# units=[m,rad,m*rad,m,rad,m*rad,m,C,1,1/m,eV,1,1,1]
# hence, in the array the values are stored in Placet units
set beamparams(betax) $beam0010(betax)
set beamparams(alphax) $beam0010(alphax)
set beamparams(emitnx) [expr $beam0010(emitnx)*$placetunits(emittance)]
set beamparams(betay) $beam0010(betay)
set beamparams(alphay) $beam0010(alphay)
set beamparams(emitny) [expr $beam0010(emitny)*$placetunits(emittance)]
set beamparams(sigmaz) [expr $beam0010(sigmaz)*$placetunits(xyz)]
set beamparams(meanz) [expr $beam0010(meanz)*$placetunits(xyz)]
set beamparams(charge) [expr $beam0010(charge)*$placetunits(charge)]
set beamparams(uncespread) $beam0010(uncespr)
set beamparams(echirp) [expr $beam0010(echirp)/$placetunits(xyz)]
set beamparams(energy) [expr $beam0010(energy)*$placetunits(energy)]

set beamparams(startenergy) [expr $beam0010(energy)*$placetunits(energy)]
set beamparams(meanenergy) [expr $beam0010(energy)*$placetunits(energy)]

# random misalignment of beam
set gamma [expr $beamparams(startenergy)/0.0005109989]
set emitx [expr $beamparams(emitnx)/($placetunits(emittance)*$gamma)]
set emity [expr $beamparams(emitny)/($placetunits(emittance)*$gamma)]
set betax $beamparams(betax)
set alphax $beamparams(alphax)
set betay $beamparams(betay)
set alphay $beamparams(alphay)
Octave {
    format long;
    ex=$emitx;
    bx=$betax;
    ax=$alphax;
    ey=$emity;
    by=$betay;
    ay=$alphay;

    xn =0.0*sqrt(ex)*randn(1,1);
    xpn=0.0*sqrt(ex)*randn(1,1);
    yn =0.0*sqrt(ey)*randn(1,1);
    ypn=0.0*sqrt(ey)*randn(1,1);

    x  =(sqrt(bx)*xn)*1e6;
    xp =(-ax/sqrt(bx)*xn+xpn/sqrt(bx))*1e6;
    y  =(sqrt(by)*yn)*1e6;
    yp =(-ay/sqrt(by)*yn+ypn/sqrt(by))*1e6;
    Tcl_SetVar("bm_x",x);
    Tcl_SetVar("bm_xp",xp);
    Tcl_SetVar("bm_y",y);
    Tcl_SetVar("bm_yp",yp);
}
set beamparams(meanx) $bm_x
set beamparams(meanxp) $bm_xp
set beamparams(meany) $bm_y
set beamparams(meanyp) $bm_yp



########### SR 
set SRncarray(1,1)   40
set SRncarray(1,2)   0.5

set SRde 0.05
set SRnbins 1
set SRbeta0 5
set SRbeta1 3
set SRwgt 30
set SROTOnloop 3
set SRDFSnloop 3
###############


############# BC1
set BC1ncarray(1,1)   100
set BC1ncarray(1,2)   0.5

set BC1de 0.05
set BC1beta0 4
set BC1beta1 5
set BC1wgt 30
set BC1OTOnloop 3
set BC1DFSnloop 3
###############

############ Boost Linac
set BOOncarray(1,1)   100
set BOOncarray(1,2)   0.5

set BOOde 0.05
set BOOnbins 1
set BOObeta0 7
set BOObeta1 2
set BOOwgt 30
set BOOOTOnloop 3
set BOODFSnloop 3
###############

########### Central Arc
set CAncarray1(1,1)   40
set CAncarray1(1,2)   0.5
set CAncarray2(1,1)   20
set CAncarray2(1,2)   0.5

set CAde 0.05
set CAbeta0 4
set CAbeta1 1
set CAbeta11 3
set CAwgt 30
set CAOTOnloop 3
set CADFSnloop 5
###############

########### Vertical Transfer
set VTncarray(1,1)   50
set VTncarray(1,2)   0.5

set VTde 0.05
set VTbeta0 4
set VTbeta1 1
set VTwgt 30
set VTOTOnloop 3
set VTDFSnloop 3
###############

########## Long Transfer Line
set LTLncarray(1,1)   50
set LTLncarray(1,2)   0.5

set LTLde 0.05
set LTLbeta0 5
set LTLbeta1 3
set LTLwgt 30
set LTLOTOnloop 3
set LTLDFSnloop 5
###############


######## Turn Around Loop 
set TAL1ncarray(1,1)   40
set TAL1ncarray(1,2)   0.5

set TAL1de 0.05
set TAL1beta0 3
set TAL1beta1 1
set TAL1wgt 30
set TAL1OTOnloop 3
set TAL1DFSnloop 5
###############
#
######### Turn Around Loop 
set TAL2ncarray(1,1)   40
set TAL2ncarray(1,2)   0.5

set TAL2de 0.05
set TAL2beta0 4
set TAL2beta1 1
set TAL2beta11 3
set TAL2wgt 30
set TAL2OTOnloop 3
set TAL2DFSnloop 5
###############



########## BC2
set BC2ncarray(1,1)   30
set BC2ncarray(1,2)   0.5

set BC2de 0.1
set BC2beta0 2
set BC2beta1 1
set BC2wgt 30
set BC2OTOnloop 3
set BC2DFSnloop 5
###############

