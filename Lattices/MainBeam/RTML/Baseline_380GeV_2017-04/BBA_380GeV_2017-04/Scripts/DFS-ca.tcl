proc dfsCorrection-ca {name beamline Start End beam de beta1 wgt nloop} {
  global sigma
  global machine

  Octave {
  disp("DFS CORRECTION");

  Start = placet_get_name_number_list("$beamline","$Start");
  End = placet_get_name_number_list("$beamline","$End");

  beta11 = $beta1;
  for bin = 1:length(Bins)
    printf("Now is bin %e\n",bin);
    Bin = Bins(bin);
    nCorrector = length(Bin.Corrector);
    R0x = Response.R0x(Bin.Bpms,Bin.Corrector);
    R0y = Response.R0y(Bin.Bpms,Bin.Corrector);
    R1x = Response.R1x(Bin.Bpms,Bin.Corrector);
    R1y = Response.R1y(Bin.Bpms,Bin.Corrector);
    placet_test_no_correction("$beamline", "$beam", "None", 1, Start, Response.Bpms(Bin.Bpms(end)));
    for i=1:$nloop
  
      B0 = placet_get_bpm_readings("$beamline", Response.Bpms);
      placet_test_no_correction("$beamline", "beamDFS", "None", 1, Start, Response.Bpms(Bin.Bpms(end)));
      B1 = placet_get_bpm_readings("$beamline", Response.Bpms) - B0;
      B0 -= Response.B0;
      B1 -= Response.B1;
      B0 = B0(Bin.Bpms,:);
      B1 = B1(Bin.Bpms,:);
  
      CorrectorX = -[ R0x ; $wgt * R1x ; beta11 * eye(nCorrector) ] \ [ B0(:,1) ; $wgt * B1(:,1) ; zeros(nCorrector,1) ];
      CorrectorY = -[ R0y ; $wgt * R1y ; beta11 * eye(nCorrector) ] \ [ B0(:,2) ; $wgt * B1(:,2) ; zeros(nCorrector,1) ];
      
      placet_element_vary_attribute("$beamline", Response.Corrector(Bin.Corrector), "strength_x", CorrectorX);
      placet_element_vary_attribute("$beamline", Response.Corrector(Bin.Corrector), "strength_y", CorrectorY);
      [E,B] = placet_test_no_correction("$beamline", "$beam", "None", 1, Start, Bin.last);
    end
         printf("The emittance of X is %e\n", E(end,2));
         printf("The emittance of Y is %e\n", E(end,6));
  end
          [E,B] = placet_test_no_correction("$beamline", "$beam", "None",1, Start, Bin.last);
          printf("*****************************************\n");
          printf("The emittance of X is %e\n", E(end,2));
          printf("The emittance of Y is %e\n", E(end,6));
          printf("*****************************************\n");
	  save(['Emitt2-' "$name-$beamline-" num2str($sigma) '-' num2str($machine) '.dat'], 'E')
  }
}
