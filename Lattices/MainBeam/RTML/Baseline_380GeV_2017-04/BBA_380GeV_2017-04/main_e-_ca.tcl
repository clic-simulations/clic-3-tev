# CLIC Main Beam RTML

##### If No wakefield file, run the create_wake.tcl script
#source create_wake.tcl
######

set tcl_precision 15
#ParallelThreads -num 2

proc FF1 {} {}
proc FF2 {} {}

set latticedir ./Lattices_e-
set scriptdir ./Scripts
set paramsdir ./Parameters

source $scriptdir/placet_units.tcl
source $paramsdir/initial_beam_parameters_e-.tcl
source $paramsdir/rf_parameters_e-.tcl
source $scriptdir/lattice_fun.tcl
source $scriptdir/beamsetup.tcl
source $scriptdir/cavitywakesetup.tcl
source $scriptdir/octave_fun.tcl
source $scriptdir/setParam.tcl
source $scriptdir/plainTracking.tcl

if {$beamparams(usewakefields)==1} {
   source rf_parameteres_ww.tcl
}

create_particles_file particles.in beamparams
create_zero_wakes_file zero_wake.dat beamparams rfparamsbc1 particles.in

BeamlineNew
source $scriptdir/beamline_and_wake.tcl
BeamlineSet -name rtml

FirstOrder 1

make_particle_beam beam4 beamparams particles.in zero_wake.dat

source $scriptdir/setCSR.m

Octave {
load mag-$machine.dat;
magnets = placet_get_number_list("rtml", "quadrupole");
magnets = [ magnets placet_get_number_list("rtml", "sbend") ];
magnets = [ magnets placet_get_number_list("rtml", "multipole") ];
placet_element_set_attribute("rtml", magnets, "x", mag.x);
placet_element_set_attribute("rtml", magnets, "y", mag.y);
placet_element_set_attribute("rtml", magnets, "xp", mag.xp);
placet_element_set_attribute("rtml", magnets, "yp", mag.yp);
placet_element_set_attribute("rtml", magnets, "roll", mag.roll);

magnets = placet_get_number_list("rtml", "quadrupole");
magnets = [ magnets placet_get_number_list("rtml", "multipole") ];
placet_element_set_attribute("rtml", magnets, "strength", mag.strength);

magnets = placet_get_number_list("rtml", "sbend");
placet_element_set_attribute("rtml", magnets, "e0", mag.e0);


BI = placet_get_number_list("rtml","bpm");
placet_element_set_attribute("rtml", BI, "x", Bpm.x);
placet_element_set_attribute("rtml", BI, "y", Bpm.y);
placet_element_set_attribute("rtml", BI, "xp", Bpm.xp);
placet_element_set_attribute("rtml", BI, "yp", Bpm.yp);
placet_element_set_attribute("rtml", BI, "roll", Bpm.roll);
placet_element_set_attribute("rtml",BI, "resolution",Bpm.res);
}

Octave {
load dipole-$machine.dat
Corrector_all = placet_get_number_list("rtml","dipole");
placet_element_set_attribute("rtml", Corrector_all, 'strength_x',dipole_x);
placet_element_set_attribute("rtml", Corrector_all, 'strength_y',dipole_y);
}

Octave {
    load 'beam-$machine-4.dat';
    placet_set_beam("beam4",B);
}

plainTracking CA rtml Marker-CA-start Marker-LTL-end beam4
