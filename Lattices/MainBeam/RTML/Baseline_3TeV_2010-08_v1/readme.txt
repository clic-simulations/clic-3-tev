This is the CLIC Mainbeam RTML Baseline Lattice.
It has been created by Frank Stulle (frank.stulle@cern.ch) with the help
by several others and builds on some previous work.

Lattices are provided for the codes Placet and Elegant.
A simplified MAD-X lattice is converted from Placet (useful only for twiss and survey, no acceleration).

All required files and scripts are included, except for the respective binaries of the codes.
Have a look at "run_elegant_mpi.sh" for the Elegant simulations and "run_placet.sh" for the Placet simulations.



Version 2010-08 v1
Finally, the e- spin rotator is in. But path length difference between e+ and e- is still not matched to the correct value
and central arcs do not give the correct horizontal offset.
- Included the e- spin rotator
- Clean up of many lattices, e.g. remove half-quads
- Several other improvements


Version 2010-07 v1
This is just a bug-fix release. Some errors are corrected in the wake setup for elegant and placet.
The lattices are unchanged.


Version 2010-06 v1
A lot closer to the CDR baseline. Unfortunately, the spin rotator is still missing. The lengths of e- and e+ beam lines are
not matched to compensate the static timing offset. Central arcs do not really correct the horizontal offset.
Changes:
- Revision of the turn around loop and the central e- arc. This was required to improve its error acceptance.
- some clean-up, bug fixing,...
- A MAD-X lattice is included which has been converted from Placet. RF cavities are off. Thus it is useful only for
  Twiss output and survey.


Version 2010-03 v1
This is not yet the final version which will be used for the CDR baseline! But it is close to it.
Changes:
- Major revision of all parts to make the footprint match civil engineering and to correct several bugs.
- Lattices are now provided for e- and e+ separately since spin rotators are at different locations and the central arcs are different.
- This is the first version which claims some completeness. But still at a few locations dummy lattices are used,
  e.g. spin rotators and diagnostics sections.


Version 2009-10 v1
Changes:
- Update of the short range cavity wake field functions.
- Correction of a bug in the Turn Around Loop Lattice (too short matching between left bending and right bending arcs).
- Cleanup of several files.


Version 2009-07 v1
- Initial baseline lattice.

