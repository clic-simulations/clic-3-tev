#
# Spin rotator
#
# beamparams=[betax,alphax,emitnx,betay,alphay,emitny,sigmaz,charge,
#             uncespread,echirp,energy,nslice,nmacro,nsigmabunch,nsigmawake]
# units=[m,rad,m*rad,m,rad,m*rad,m,C,1,1/m,eV,1,1,1,1]
#
# where necessary units were converted to placet units in main.tcl

proc lattice_spin_rotator {bparray} {
upvar $bparray beamparams

set usecsr 0
set usesynrad $beamparams(useisr)
set usesixdim 1
set numthinlenses 100
set quad_synrad 0

set pi 3.141592653589793

set theta 0.04033666667
set lbendarc 1.0
set rho [expr $lbendarc/$theta]
set refenergy $beamparams(meanenergy)

set c 299792458
set q0 1.6021765e-19
set eps0 [expr 1/(4e-7*$pi*$c*$c)]


SetReferenceEnergy $refenergy

Girder

Drift -name "SOLENOID1" -length 1.3 -six_dim $usesixdim
Drift -name "D1S" -length 3.7 -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -name "QF01" -synrad $quad_synrad -length 0.36 -strength [expr 0.2530370303*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -name "D1" -length 5 -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -name "QD01" -synrad $quad_synrad -length 0.36 -strength [expr -0.1865980792*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -name "D1" -length 5 -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -name "QF1" -synrad $quad_synrad -length 0.36 -strength [expr 0.2530366575*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -name "D1" -length 5 -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -name "QD1" -synrad $quad_synrad -length 0.36 -strength [expr -0.1865980528*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -name "D1" -length 5 -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -name "QF1" -synrad $quad_synrad -length 0.36 -strength [expr 0.2530366575*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -name "D1" -length 5 -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -name "QD1" -synrad $quad_synrad -length 0.36 -strength [expr -0.1865980528*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -name "D1" -length 5 -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -name "QF1" -synrad $quad_synrad -length 0.36 -strength [expr 0.2530366575*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -name "D1" -length 5 -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -name "QD1" -synrad $quad_synrad -length 0.36 -strength [expr -0.1865980528*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -name "SOLENOID1" -length 1.3 -six_dim $usesixdim
Drift -name "M1D1" -length 1 -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -name "M1QF1" -synrad $quad_synrad -length 0.36 -strength [expr 0.5178288905*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -name "M1D2" -length 1 -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -name "M1QD2" -synrad $quad_synrad -length 0.36 -strength [expr -0.545118223*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -name "M1D3" -length 1 -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -name "M1QF3" -synrad $quad_synrad -length 0.36 -strength [expr 0.5425750504*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -name "M1D4" -length 1 -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -name "M1QD4" -synrad $quad_synrad -length 0.36 -strength [expr -0.3062493475*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -name "D2S" -length 2 -six_dim $usesixdim
Sbend -name "BEND1" -length $lbendarc -angle $theta -e0 $refenergy -E1 [expr 0*$theta/2] -E2 [expr 0*$theta/2] -csr $usecsr -synrad $usesynrad -six_dim $usesixdim -thin_lens $numthinlenses
set g0 [expr $refenergy/0.000510999]
set de [expr 1/(6*$pi*$eps0)*$q0*$q0*$c*$g0*$g0*$g0*$g0/($rho*$rho)*$lbendarc/$c]
set refenergy [expr $refenergy-$de/$q0/1e9*$usesynrad]
SetReferenceEnergy $refenergy
Drift -name "D2S" -length 2 -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -name "QF2" -synrad $quad_synrad -length 0.36 -strength [expr 0.3295122006*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -name "D2S" -length 2 -six_dim $usesixdim
Sbend -name "BEND1" -length $lbendarc -angle $theta -e0 $refenergy -E1 [expr 0*$theta/2] -E2 [expr 0*$theta/2] -csr $usecsr -synrad $usesynrad -six_dim $usesixdim -thin_lens $numthinlenses
set g0 [expr $refenergy/0.000510999]
set de [expr 1/(6*$pi*$eps0)*$q0*$q0*$c*$g0*$g0*$g0*$g0/($rho*$rho)*$lbendarc/$c]
set refenergy [expr $refenergy-$de/$q0/1e9*$usesynrad]
SetReferenceEnergy $refenergy
Drift -name "D2S" -length 2 -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -name "QD2" -synrad $quad_synrad -length 0.36 -strength [expr -0.3305696261*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -name "D2S" -length 2 -six_dim $usesixdim
Sbend -name "BEND1" -length $lbendarc -angle $theta -e0 $refenergy -E1 [expr 0*$theta/2] -E2 [expr 0*$theta/2] -csr $usecsr -synrad $usesynrad -six_dim $usesixdim -thin_lens $numthinlenses
set g0 [expr $refenergy/0.000510999]
set de [expr 1/(6*$pi*$eps0)*$q0*$q0*$c*$g0*$g0*$g0*$g0/($rho*$rho)*$lbendarc/$c]
set refenergy [expr $refenergy-$de/$q0/1e9*$usesynrad]
SetReferenceEnergy $refenergy
Drift -name "D2S" -length [expr 2.0 - 0.2] -six_dim $usesixdim
Multipole -type 3 -length 0.2 -e0 $refenergy -strength [expr 2.83*$refenergy*0.2] -thin_lens $numthinlenses -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -name "QF2" -synrad $quad_synrad -length 0.36 -strength [expr 0.3295122006*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -name "D2S" -length 2 -six_dim $usesixdim
Sbend -name "BEND1" -length $lbendarc -angle $theta -e0 $refenergy -E1 [expr 0*$theta/2] -E2 [expr 0*$theta/2] -csr $usecsr -synrad $usesynrad -six_dim $usesixdim -thin_lens $numthinlenses
set g0 [expr $refenergy/0.000510999]
set de [expr 1/(6*$pi*$eps0)*$q0*$q0*$c*$g0*$g0*$g0*$g0/($rho*$rho)*$lbendarc/$c]
set refenergy [expr $refenergy-$de/$q0/1e9*$usesynrad]
SetReferenceEnergy $refenergy
Drift -name "D2S" -length 2 -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -name "QD2" -synrad $quad_synrad -length 0.36 -strength [expr -0.3305696261*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -name "D2S" -length 2 -six_dim $usesixdim
Sbend -name "BEND1" -length $lbendarc -angle $theta -e0 $refenergy -E1 [expr 0*$theta/2] -E2 [expr 0*$theta/2] -csr $usecsr -synrad $usesynrad -six_dim $usesixdim -thin_lens $numthinlenses
set g0 [expr $refenergy/0.000510999]
set de [expr 1/(6*$pi*$eps0)*$q0*$q0*$c*$g0*$g0*$g0*$g0/($rho*$rho)*$lbendarc/$c]
set refenergy [expr $refenergy-$de/$q0/1e9*$usesynrad]
SetReferenceEnergy $refenergy
Drift -name "D2S" -length 2 -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -name "QF2" -synrad $quad_synrad -length 0.36 -strength [expr 0.3295122006*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -name "D2S" -length 2 -six_dim $usesixdim
Sbend -name "BEND1" -length $lbendarc -angle $theta -e0 $refenergy -E1 [expr 0*$theta/2] -E2 [expr 0*$theta/2] -csr $usecsr -synrad $usesynrad -six_dim $usesixdim -thin_lens $numthinlenses
set g0 [expr $refenergy/0.000510999]
set de [expr 1/(6*$pi*$eps0)*$q0*$q0*$c*$g0*$g0*$g0*$g0/($rho*$rho)*$lbendarc/$c]
set refenergy [expr $refenergy-$de/$q0/1e9*$usesynrad]
SetReferenceEnergy $refenergy
Drift -name "D2S" -length 2 -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -name "QD2" -synrad $quad_synrad -length 0.36 -strength [expr -0.3305696261*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -name "M2D1" -length 1 -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -name "M2QF1" -synrad $quad_synrad -length 0.36 -strength [expr 0.05035268685*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -name "M2D2" -length 1 -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -name "M2QD2" -synrad $quad_synrad -length 0.36 -strength [expr 0.2872202639*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -name "M2D3" -length 1 -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -name "M2QF3" -synrad $quad_synrad -length 0.36 -strength [expr 0.2064250276*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -name "M2D4" -length 1 -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -name "M2QD4" -synrad $quad_synrad -length 0.36 -strength [expr -0.3232044214*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -name "SOLENOID2" -length 1.3 -six_dim $usesixdim
Drift -name "D1S" -length 3.7 -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -name "QF1" -synrad $quad_synrad -length 0.36 -strength [expr 0.2530366575*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -name "D1" -length 5 -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -name "QD1" -synrad $quad_synrad -length 0.36 -strength [expr -0.1865980528*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -name "D1" -length 5 -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -name "QF1" -synrad $quad_synrad -length 0.36 -strength [expr 0.2530366575*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -name "D1" -length 5 -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -name "QD1" -synrad $quad_synrad -length 0.36 -strength [expr -0.1865980528*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -name "D1" -length 5 -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -name "QF1" -synrad $quad_synrad -length 0.36 -strength [expr 0.2530366575*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -name "D1" -length 5 -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -name "QD1" -synrad $quad_synrad -length 0.36 -strength [expr -0.1865980528*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -name "D1" -length 5 -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -name "QF1" -synrad $quad_synrad -length 0.36 -strength [expr 0.2530366575*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -name "D1" -length 5 -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -name "QD1" -synrad $quad_synrad -length 0.36 -strength [expr -0.1865980528*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -name "SOLENOID2" -length 1.3 -six_dim $usesixdim

set beamparams(meanenergy) $refenergy
puts "Setting beamparams(meanenergy)=$beamparams(meanenergy)."
}
