#
# particle beam setup
#
# 
# general parameters have to be store before
# in the array beamparams=
# [betax,alphax,emitnx,betay,alphay,emitny,sigmaz,charge,
#  uncespread,echirp,energy,nslice,nmacro,nsigmabunch,nsigmawake]
# units=[m,rad,m*rad,m,rad,m*rad,m,C,1,1/m,eV,1,1,1,1]
#

proc create_particles_file {outfile bparray} {
    global placetunits
    upvar $bparray beamparams

    set bx $beamparams(betax)
    set ax $beamparams(alphax)
    set enx $beamparams(emitnx)
    set by $beamparams(betay)
    set ay $beamparams(alphay)
    set eny $beamparams(emitny)
    set rmsz $beamparams(sigmaz)
    set meanz $beamparams(meanz)
    set espread $beamparams(uncespread)
    set echirp $beamparams(echirp)
    set energy $beamparams(startenergy)
    set nslice $beamparams(nslice)
    set nmacro $beamparams(nmacro)
    set nsigma $beamparams(nsigmabunch)

    set meanx $beamparams(meanx)
    set meanxp $beamparams(meanxp)
    set meany $beamparams(meany)
    set meanyp $beamparams(meanyp)

    set npart [expr $nmacro*$nslice]

    set pu_xyz $placetunits(xyz)
    set pu_xpyp $placetunits(xpyp)
    set pu_e $placetunits(energy)
    set pu_emit $placetunits(emittance)

    # the creation of the particle distribution is done in Octave
    Octave {
    format long;
    % read in some required functions
    scriptdir="./Scripts/";
    source([scriptdir,"octave_beam_statistics.m"]);
    source([scriptdir,"octave_beam_creation.m"]);

    create_particle_distribution("$outfile",$bx,$ax,$enx,$by,$ay,$eny,$rmsz,$meanz,$espread,$echirp,$energy,$meanx,$meanxp,$meany,$meanyp,$nsigma,$npart,$pu_xyz,$pu_xpyp,$pu_e,$pu_emit);
    }
    # end of Octave part
}


proc make_particle_beam {name bparray particlesfile wakefile} {
    upvar $bparray beamparams

# the following is only required to set up the internal structure which stores the beam,
# by using BeamRead at the end all particle data is overwritten.

    InjectorBeam $name -bunches 1 \
	    -macroparticles $beamparams(nmacro) \
	    -particles [expr $beamparams(nslice)*$beamparams(nmacro)] \
	    -energyspread $beamparams(uncespread) \
	    -ecut $beamparams(nsigmawake) \
	    -e0 $beamparams(startenergy) \
	    -file $wakefile \
	    -chargelist {1.0} \
	    -charge $beamparams(charge) \
	    -phase 0.0 \
	    -overlapp 0.0 \
	    -distance 0.0 \
	    -beta_x $beamparams(betax) \
	    -alpha_x $beamparams(alphax) \
	    -emitt_x $beamparams(emitnx) \
	    -beta_y $beamparams(betay) \
	    -alpha_y $beamparams(alphay) \
	    -emitt_y $beamparams(emitny)

# now load the real particles
    BeamRead -file $particlesfile -beam $name
}


proc make_bunch_train {name bparray rfparray wakefile} {
    upvar $bparray beamparams
    upvar $rfparray rfparams

# This creates a train of bunches each consisting of slices.
# It is useful, e.g., for long range wake studies.
    set e0 $beamparams(startenergy)
    set uncespr $beamparams(uncespread)
    set echirp $beamparams(echirp)
    set rmss $beamparams(sigmaz)
    set de [expr $e0*sqrt($uncespr*$uncespr+$rmss*$rmss*$echirp*$echirp)]

    InjectorBeam $name -bunches $beamparams(nbunches) \
	    -macroparticles $beamparams(nmacro) \
	    -slices $beamparams(nslice) \
	    -energyspread $de \
	    -ecut $beamparams(nsigmawake) \
	    -e0 $e0 \
	    -file $wakefile \
	    -chargelist {1.0} \
	    -charge $beamparams(charge) \
	    -phase 0.0 \
            -last_wgt 1.0 \
	    -overlapp [expr -$rfparams(lambda)] \
	    -distance [expr $rfparams(lambda)] \
	    -beta_x $beamparams(betax) \
	    -alpha_x $beamparams(alphax) \
	    -emitt_x $beamparams(emitnx) \
	    -beta_y $beamparams(betay) \
	    -alpha_y $beamparams(alphay) \
	    -emitt_y $beamparams(emitny)

    set l {}
    lappend l {1.0 0.0 0.0}
    SetRfGradientSingle $name 0 $l
}
