% since multipoles cannot be misaligned by SurveyErrorSet
% and since the random seed is always the same when
% restarting Placet, we do misalignment the hard way
function induce_beamline_errors(beamlinename,...
          error_cavity_pos,error_cavity_gradient_rel,...
          error_bend_pos,error_bend_strength_rel,...
          error_quadrupole_pos,error_quadrupole_strength_rel,...
          error_multipole_pos,error_multipole_strength_rel,...
          error_bpm_pos,bpm_resolution,keepolderrors)

id_cav=placet_get_number_list(beamlinename,"cavity");
id_bend=placet_get_number_list(beamlinename,"sbend");
id_quad=placet_get_number_list(beamlinename,"quadrupole");
id_multi=placet_get_number_list(beamlinename,"multipole");
id_bpm=placet_get_number_list(beamlinename,"bpm");

if (keepolderrors!=0)
  keepolderrors=1;
end;

persistent old_err_str_cav=0;
id=id_cav;
pos_err=error_cavity_pos;
str_err=error_cavity_gradient_rel;
if (length(id)>0)
	old_x=placet_element_get_attribute("$beamlinename",id,"x");
	old_xp=placet_element_get_attribute("$beamlinename",id,"xp");
	old_y=placet_element_get_attribute("$beamlinename",id,"y");
	old_yp=placet_element_get_attribute("$beamlinename",id,"yp");
	old_roll=placet_element_get_attribute("$beamlinename",id,"roll");
	old_str=placet_element_get_attribute("$beamlinename",id,"gradient");
	
	err_x=pos_err*cutrandnv(1,length(id),3);
	err_xp=pos_err*cutrandnv(1,length(id),3);
	err_y=pos_err*cutrandnv(1,length(id),3);
	err_yp=pos_err*cutrandnv(1,length(id),3);
	err_roll=pos_err*cutrandnv(1,length(id),3);
	err_str=str_err*cutrandnv(1,length(id),3);
	
	placet_element_set_attribute("$beamlinename",id,"x",keepolderrors*old_x+err_x);
	placet_element_set_attribute("$beamlinename",id,"xp",keepolderrors*old_xp+err_xp);
	placet_element_set_attribute("$beamlinename",id,"y",keepolderrors*old_y+err_y);
	placet_element_set_attribute("$beamlinename",id,"yp",keepolderrors*old_yp+err_yp);
	placet_element_set_attribute("$beamlinename",id,"roll",keepolderrors*old_roll+err_roll);
	placet_element_set_attribute("$beamlinename",id,"gradient",old_str.*(1+err_str)./(1+!keepolderrors*old_err_str_cav));
	old_err_str_cav=err_str;
end;


persistent old_err_angle_bend=0;
id=id_bend;
pos_err=error_bend_pos;
str_err=error_bend_strength_rel;
if (length(id)>0)
	old_x=placet_element_get_attribute("$beamlinename",id,"x");
	old_xp=placet_element_get_attribute("$beamlinename",id,"xp");
	old_y=placet_element_get_attribute("$beamlinename",id,"y");
	old_yp=placet_element_get_attribute("$beamlinename",id,"yp");
	old_roll=placet_element_get_attribute("$beamlinename",id,"roll");
	old_angle=placet_element_get_attribute("$beamlinename",id,"angle");
	
	err_x=pos_err*cutrandnv(1,length(id),3);
	err_xp=pos_err*cutrandnv(1,length(id),3);
	err_y=pos_err*cutrandnv(1,length(id),3);
	err_yp=pos_err*cutrandnv(1,length(id),3);
	err_roll=pos_err*cutrandnv(1,length(id),3);
	err_angle=str_err*cutrandnv(1,length(id),3);
	
	placet_element_set_attribute("$beamlinename",id,"x",keepolderrors*old_x+err_x);
	placet_element_set_attribute("$beamlinename",id,"xp",keepolderrors*old_xp+err_xp);
	placet_element_set_attribute("$beamlinename",id,"y",keepolderrors*old_y+err_y);
	placet_element_set_attribute("$beamlinename",id,"yp",keepolderrors*old_yp+err_yp);
	placet_element_set_attribute("$beamlinename",id,"roll",keepolderrors*old_roll+err_roll);
	placet_element_set_attribute("$beamlinename",id,"angle",old_angle.*(1+err_angle)./(1+!keepolderrors*old_err_angle_bend));
	old_err_angle_bend=err_angle;
end;


persistent old_err_str_quad=0;
id=id_quad;
pos_err=error_quadrupole_pos;
str_err=error_quadrupole_strength_rel;
if (length(id)>0)
	old_x=placet_element_get_attribute(beamlinename,id,"x");
	old_xp=placet_element_get_attribute(beamlinename,id,"xp");
	old_y=placet_element_get_attribute(beamlinename,id,"y");
	old_yp=placet_element_get_attribute(beamlinename,id,"yp");
	old_roll=placet_element_get_attribute(beamlinename,id,"roll");
	old_str=placet_element_get_attribute(beamlinename,id,"strength");
	
	err_x=pos_err*cutrandnv(1,length(id),3);
	err_xp=pos_err*cutrandnv(1,length(id),3);
	err_y=pos_err*cutrandnv(1,length(id),3);
	err_yp=pos_err*cutrandnv(1,length(id),3);
	err_roll=pos_err*cutrandnv(1,length(id),3);
	err_str=str_err*cutrandnv(1,length(id),3);
	
	placet_element_set_attribute("$beamlinename",id,"x",keepolderrors*old_x+err_x);
	placet_element_set_attribute("$beamlinename",id,"xp",keepolderrors*old_xp+err_xp);
	placet_element_set_attribute("$beamlinename",id,"y",keepolderrors*old_y+err_y);
	placet_element_set_attribute("$beamlinename",id,"yp",keepolderrors*old_yp+err_yp);
	placet_element_set_attribute("$beamlinename",id,"roll",keepolderrors*old_roll+err_roll);
	placet_element_set_attribute("$beamlinename",id,"strength",old_str.*(1+err_str)./(1+!keepolderrors*old_err_str_quad));
	old_err_str_quad=err_str;
end;


persistent old_err_str_multi=0;
id=id_multi;
pos_err=error_multipole_pos;
str_err=error_multipole_strength_rel;
if (length(id)>0)
	old_x=placet_element_get_attribute(beamlinename,id,"x");
	old_xp=placet_element_get_attribute(beamlinename,id,"xp");
	old_y=placet_element_get_attribute(beamlinename,id,"y");
	old_yp=placet_element_get_attribute(beamlinename,id,"yp");
	old_roll=placet_element_get_attribute(beamlinename,id,"roll");
	old_str=placet_element_get_attribute(beamlinename,id,"strength");
	
	err_x=pos_err*cutrandnv(1,length(id),3);
	err_xp=pos_err*cutrandnv(1,length(id),3);
	err_y=pos_err*cutrandnv(1,length(id),3);
	err_yp=pos_err*cutrandnv(1,length(id),3);
	err_roll=pos_err*cutrandnv(1,length(id),3);
	err_str=str_err*cutrandnv(1,length(id),3);
	
	placet_element_set_attribute("$beamlinename",id,"x",keepolderrors*old_x+err_x);
	placet_element_set_attribute("$beamlinename",id,"xp",keepolderrors*old_xp+err_xp);
	placet_element_set_attribute("$beamlinename",id,"y",keepolderrors*old_y+err_y);
	placet_element_set_attribute("$beamlinename",id,"yp",keepolderrors*old_yp+err_yp);
	placet_element_set_attribute("$beamlinename",id,"roll",keepolderrors*old_roll+err_roll);
	placet_element_set_attribute("$beamlinename",id,"strength",old_str.*(1+err_str)./(1+!keepolderrors*old_err_str_multi));
	old_err_str_multi=err_str;
end;


id=id_bpm;
pos_err=error_bpm_pos;
if (length(id)>0)
	old_x=placet_element_get_attribute("$beamlinename",id,"x");
	old_xp=placet_element_get_attribute("$beamlinename",id,"xp");
	old_y=placet_element_get_attribute("$beamlinename",id,"y");
	old_yp=placet_element_get_attribute("$beamlinename",id,"yp");
	old_roll=placet_element_get_attribute("$beamlinename",id,"roll");
	
	err_x=pos_err*cutrandnv(1,length(id),3);
	err_xp=pos_err*cutrandnv(1,length(id),3);
	err_y=pos_err*cutrandnv(1,length(id),3);
	err_yp=pos_err*cutrandnv(1,length(id),3);
	err_roll=pos_err*cutrandnv(1,length(id),3);
	
	placet_element_set_attribute("$beamlinename",id,"x",keepolderrors*old_x+err_x);
	placet_element_set_attribute("$beamlinename",id,"xp",keepolderrors*old_xp+err_xp);
	placet_element_set_attribute("$beamlinename",id,"y",keepolderrors*old_y+err_y);
	placet_element_set_attribute("$beamlinename",id,"yp",keepolderrors*old_yp+err_yp);
	placet_element_set_attribute("$beamlinename",id,"roll",keepolderrors*old_roll+err_roll);
	placet_element_set_attribute("$beamlinename",id,"resolution",bpm_resolution);
end;

endfunction;
