% some functions to calculate corrector settings
% i.e. dipole corrector strengths, quad positions,...
%
% errors are also applied here

%----------------------------------------
function [emitfinal,beamfinal]=trajectory_correction(beamlinename,beamname,correctiontype,correctortype,numberofsets,...
          bpmfilename,correctorsfilename,statfilename,...
          error_cavity_pos,error_cavity_gradient_rel,...
          error_bend_pos,error_bend_strength_rel,...
          error_quadrupole_pos,error_quadrupole_strength_rel,...
          error_multipole_pos,error_multipole_strength_rel,...
          error_bpm_pos,bpm_resolution,keepolderrors)

%bpmfilename=[bpmfilename,"_",beamlinename,".txt"];
%correctorsfilename=[correctorsfilename,"_",beamlinename,".txt"];
%statfilename=[statfilename,"_",beamlinename,".txt"];

format long;
%scriptdir="./Scripts/";
%source([scriptdir,"octave_beam_statistics.m"]);
%

% some initialisations
if strcmpi(correctortype,"dipole")
  corrxattr="strength_x";
  corryattr="strength_y";
elseif strcmpi(correctortype,"quadrupole")
  corrxattr="x";
  corryattr="y";
else
  correctortype="quadrupole";
  corrxattr="x";
  corryattr="y";
end;

fid=fopen(bpmfilename,"wt");
fprintf(fid,"#BPM readings (in micrometer)\n");
fprintf(fid,"#%21s %22s %22s %22s %22s %22s %22s %22s\n",
            "rms BPM X initial","rms BPM X final","mean BPM X initial","mean BPM X final",
            "rms BPM Y initial","rms BPM Y final","mean BPM Y initial","mean BPM Y final");
fclose(fid);

fid=fopen(correctorsfilename,"wt");
if strcmpi(correctortype,"dipole")
  fprintf(fid,"#Corrector settings (in GeV/micrometer)\n");
  fprintf(fid,"#%21s %22s %22s %22s %22s %22s %22s %22s\n",
              "rms strength X ini","rms strength X fin","mean strength X ini","mean strength X fin",
              "rms strenght Y ini","rms strength Y fin","mean strength Y ini","mean strength Y fin");
end;
if strcmpi(correctortype,"quadrupole")
  fprintf(fid,"#Corrector settings (in micrometer)\n");
  fprintf(fid,"#%21s %22s %22s %22s %22s %22s %22s %22s\n",
              "rms offset X ini","rms offset X fin","mean offset X ini","mean offset X fin",
              "rms offset Y ini","rms offset Y fin","mean offset Y ini","mean offset Y fin");
end;
fclose(fid);

fid=fopen(statfilename,"wt");
fprintf(fid,"# bunch train data\n");
fprintf(fid,"# %18s %20s %20s %20s %20s %20s %20s %20s %20s %20s %20s %20s %20s %20s %20s %20s %20s %20s %20s %20s %20s\n",
            "semit nx [nm rad]","cemit nx [nm rad]","semit ny [nm rad]","cemit ny [nm rad]",
            "mean x [um]","rms x [um]","mean sx [um]","mean xp [urad]","rms xp [urad]","mean sxp [um]",
            "mean y [um]","rms y [um]","mean sy [um]","mean yp [urad]","rms yp [urad]","mean syp [um]",
            "mean s [um]","rms s [um]","mean e [GeV]","rms e [GeV]","dee [1]");
fclose(fid);




% get lists of bpms and correctors
%
list_bpm=placet_get_number_list(beamlinename,"bpm");
list_corr=placet_get_number_list(beamlinename,correctortype);

numbpm=length(list_bpm);
numcorr=length(list_corr);

if (numbpm==0)||(numcorr==0)
  printf("No correctors or BPMs. Tracking will be performed without corrections.\n");
  [emitfinal,beamfinal]=placet_test_no_correction(beamlinename,beamname,"None");
  return;
end;

printf("Initializing %s correction...\n",correctiontype);

% Getting response matrices and calculating SVD matrices can take a rediculous amount of time.
% Hence, it is a good idea to make these calculations once, save the result and reuse that.
% If a file with the correct name already exists, this is used and no calculation is done.
Ax=0;
Ay=0;
SVDx=0;
SVDy=0;

reloaded=0;
datafile=sprintf("response_matrix_svd_%s.dat",beamlinename);
fid=fopen(datafile,"r");
if (fid!=-1)
    fclose(fid);
    printf("Reusing existing file for response matrices and SVD coefficients.\n");
    load(datafile);
    reloaded=1;
end;

if (rows(Ax)!=numbpm)||(columns(Ax)!=numcorr)||(rows(Ay)!=numbpm)||(columns(Ay)!=numcorr)
    if (reloaded!=0)
        printf("Number of BPMs or correctors does not match with re-loaded matrices. Re-calculating...\n");
        datafile=sprintf("response_matrix_svd_%s_new.dat",beamlinename);
    end;
    tic
    % get response matrix
    syscall=sprintf("rm -f %s",datafile);
    system(syscall);
    Ax=placet_get_response_matrix_attribute(beamlinename,beamname,list_bpm,"x",list_corr,corrxattr,"Zero");
    Ay=placet_get_response_matrix_attribute(beamlinename,beamname,list_bpm,"y",list_corr,corryattr,"Zero");
    toc
    tic
    %for correction using SVD
    [UxT,Sxinv,Vx]=mysvd(Ax,1);
    [UyT,Syinv,Vy]=mysvd(Ay,1);
    SVDx=Vx*Sxinv*UxT;
    SVDy=Vy*Syinv*UyT;
    save("-text",datafile,"Ax","Ay","SVDx","SVDy");
    toc
end;

printf("Starting correction of %d misalignment sets.\n",numberofsets);
for nset=1:numberofsets
  printf("Initializing beam line (Set %d of %d).\n",nset,numberofsets);
  % set everything to zero, i.e. start from perfect machine
  placet_element_set_attribute(beamlinename,list_corr,corrxattr,zeros(numcorr,1));
  placet_element_set_attribute(beamlinename,list_corr,corryattr,zeros(numcorr,1));
  
  corr_sx_initial=zeros(numcorr,1);
  corr_sy_initial=zeros(numcorr,1);

  % induce errors, i.e. misalignment and strength errors, and set bpm resolution
  induce_beamline_errors(beamlinename,...
    error_cavity_pos,error_cavity_gradient_rel,...
    error_bend_pos,error_bend_strength_rel,...
    error_quadrupole_pos,error_quadrupole_strength_rel,...
    error_multipole_pos,error_multipole_strength_rel,...
    error_bpm_pos,bpm_resolution,keepolderrors);

  [emituncorrected,beamuncorrected]=placet_test_no_correction(beamlinename,beamname,"None");
  bpm_x_uncorrected=placet_element_get_attribute(beamlinename,list_bpm,"reading_x");
  bpm_y_uncorrected=placet_element_get_attribute(beamlinename,list_bpm,"reading_y");
  
  if strcmpi(correctiontype,"svd")
    trajectory_correction_svd(beamlinename,beamname,...
                              list_bpm,list_corr,corrxattr,corryattr,SVDx,SVDy);
  elseif strcmpi(correctiontype,"1to1")
    trajectory_correction_1to1(beamlinename,beamname,...
                              list_bpm,list_corr,corrxattr,corryattr,Ax,Ay);
  end;

  % do tracking without changing survey
  [emitfinal,beamfinal]=placet_test_no_correction(beamlinename,beamname,"None");
  
  bpm_x_final=placet_element_get_attribute(beamlinename,list_bpm,"reading_x");
  bpm_y_final=placet_element_get_attribute(beamlinename,list_bpm,"reading_y");
  
  corr_sx_final=placet_element_get_attribute(beamlinename,list_corr,corrxattr);
  corr_sy_final=placet_element_get_attribute(beamlinename,list_corr,corryattr);

  % dump data to files
  fid=fopen(bpmfilename,"at");
  fprintf(fid,"# Set %d\n",nset);
  fprintf(fid,"%22.14e %22.14e %22.14e %22.14e %22.14e %22.14e %22.14e %22.14e\n",
              rms(bpm_x_uncorrected,1),rms(bpm_x_final,1),
              mean(bpm_x_uncorrected,1),mean(bpm_x_final,1),
              rms(bpm_y_uncorrected,1),rms(bpm_y_final,1),
              mean(bpm_y_uncorrected,1),mean(bpm_y_final,1));
  fclose(fid);

  fid=fopen(correctorsfilename,"at");
  fprintf(fid,"# Set %d\n",nset);
  fprintf(fid,"%22.14e %22.14e %22.14e %22.14e %22.14e %22.14e %22.14e %22.14e\n",
              rms(corr_sx_initial,1),rms(corr_sx_final,1),
              mean(corr_sx_initial,1),mean(corr_sx_final,1),
              rms(corr_sy_initial,1),rms(corr_sy_final,1),
              mean(corr_sy_initial,1),mean(corr_sy_final,1));
  fclose(fid);


  % full bunch train
  bi=beamuncorrected;
  bf=beamfinal;
  sliceemit_x_ini=mean(bi(:,3),bi(:,2))/0.0005109989*sqrt(mean(bi(:,8),bi(:,2))*mean(bi(:,10),bi(:,2)) - mean(bi(:,9),bi(:,2))^2)/1000
  sliceemit_x_fin=mean(bf(:,3),bf(:,2))/0.0005109989*sqrt(mean(bf(:,8),bf(:,2))*mean(bf(:,10),bf(:,2)) - mean(bf(:,9),bf(:,2))^2)/1000

  sliceemit_y_ini=mean(bi(:,3),bi(:,2))/0.0005109989*sqrt(mean(bi(:,11),bi(:,2))*mean(bi(:,13),bi(:,2)) - mean(bi(:,12),bi(:,2))^2)/1000
  sliceemit_y_fin=mean(bf(:,3),bf(:,2))/0.0005109989*sqrt(mean(bf(:,11),bf(:,2))*mean(bf(:,13),bf(:,2)) - mean(bf(:,12),bf(:,2))^2)/1000

  coreemit_x_ini=mean(bi(:,3),bi(:,2))/0.0005109989*sqrt(mean(bi(:,4).^2,bi(:,2))*mean(bi(:,5).^2,bi(:,2)) - mean(bi(:,4).*bi(:,5),bi(:,2))^2)/1000
  coreemit_x_fin=mean(bf(:,3),bf(:,2))/0.0005109989*sqrt(mean(bf(:,4).^2,bf(:,2))*mean(bf(:,5).^2,bf(:,2)) - mean(bf(:,4).*bf(:,5),bf(:,2))^2)/1000

  coreemit_y_ini=mean(bi(:,3),bi(:,2))/0.0005109989*sqrt(mean(bi(:,6).^2,bi(:,2))*mean(bi(:,7).^2,bi(:,2)) - mean(bi(:,6).*bi(:,7),bi(:,2))^2)/1000
  coreemit_y_fin=mean(bf(:,3),bf(:,2))/0.0005109989*sqrt(mean(bf(:,6).^2,bf(:,2))*mean(bf(:,7).^2,bf(:,2)) - mean(bf(:,6).*bf(:,7),bf(:,2))^2)/1000

  mean_x_ini=mean(bi(:,4),bi(:,2));
  mean_x_fin=mean(bf(:,4),bf(:,2));
  mean_xp_ini=mean(bi(:,5),bi(:,2));
  mean_xp_fin=mean(bf(:,5),bf(:,2));

  mean_y_ini=mean(bi(:,6),bi(:,2));
  mean_y_fin=mean(bf(:,6),bf(:,2));
  mean_yp_ini=mean(bi(:,7),bi(:,2));
  mean_yp_fin=mean(bf(:,7),bf(:,2));

  rms_x_ini=rms(bi(:,4),bi(:,2));
  rms_x_fin=rms(bf(:,4),bf(:,2));
  rms_xp_ini=rms(bi(:,5),bi(:,2));
  rms_xp_fin=rms(bf(:,5),bf(:,2));

  rms_y_ini=rms(bi(:,6),bi(:,2));
  rms_y_fin=rms(bf(:,6),bf(:,2));
  rms_yp_ini=rms(bi(:,7),bi(:,2));
  rms_yp_fin=rms(bf(:,7),bf(:,2));

  mean_sx_ini=mean(sqrt(bi(:,8)),bi(:,2));
  mean_sx_fin=mean(sqrt(bf(:,8)),bf(:,2));
  mean_sxp_ini=mean(sqrt(bi(:,10)),bi(:,2));
  mean_sxp_fin=mean(sqrt(bf(:,10)),bf(:,2));

  mean_sy_ini=mean(sqrt(bi(:,11)),bi(:,2));
  mean_sy_fin=mean(sqrt(bf(:,11)),bf(:,2));
  mean_syp_ini=mean(sqrt(bi(:,13)),bi(:,2));
  mean_syp_fin=mean(sqrt(bf(:,13)),bf(:,2));

  mean_s_ini=mean(bi(:,1),bi(:,2));
  mean_s_fin=mean(bf(:,1),bf(:,2));
  mean_e_ini=mean(bi(:,3),bi(:,2));
  mean_e_fin=mean(bf(:,3),bf(:,2));

  rms_s_ini=rms(bi(:,1),bi(:,2));
  rms_s_fin=rms(bf(:,1),bf(:,2));
  rms_e_ini=rms(bi(:,3),bi(:,2));
  rms_e_fin=rms(bf(:,3),bf(:,2));

  dee_ini=rms_e_ini/mean_e_ini;
  dee_fin=rms_e_fin/mean_e_fin;

  fid=fopen(statfilename,"at");
  if (fid!=-1)
    fprintf(fid,"%20.12e %20.12e %20.12e %20.12e %20.12e %20.12e %20.12e %20.12e %20.12e %20.12e %20.12e %20.12e %20.12e %20.12e %20.12e %20.12e %20.12e %20.12e %20.12e %20.12e %20.12e\n",
            sliceemit_x_fin,coreemit_x_fin,sliceemit_y_fin,coreemit_y_fin,
            mean_x_fin,rms_x_fin,mean_sx_fin,mean_xp_fin,rms_xp_fin,mean_sxp_fin,
            mean_y_fin,rms_y_fin,mean_sy_fin,mean_yp_fin,rms_yp_fin,mean_syp_fin,
            mean_s_fin,rms_s_fin,mean_e_fin,rms_e_fin,dee_fin);
    fclose(fid);
  end;

end;
endfunction;
%----------------------------------------


%----------------------------------------
% calculates the required corrector settings using SVD, i.e. global correction
function trajectory_correction_svd(beamlinename,beamname,list_bpm,list_corr,corrxattr,corryattr,SVDx,SVDy)

% one could iterate...
nimax=3;
for ni=1:nimax
    [emit,beaminitial]=placet_test_no_correction(beamlinename,beamname,"None");

    bpm_x_initial=(placet_element_get_attribute(beamlinename,list_bpm,"reading_x"))';
    bpm_y_initial=(placet_element_get_attribute(beamlinename,list_bpm,"reading_y"))';

    new_strength_x=-SVDx*bpm_x_initial;
    new_strength_y=-SVDy*bpm_y_initial;

    placet_element_vary_attribute(beamlinename,list_corr,corrxattr,new_strength_x);
    placet_element_vary_attribute(beamlinename,list_corr,corryattr,new_strength_y);
end;

endfunction;
%----------------------------------------


%----------------------------------------
% calculates the required corrector settings using 1-to-1 steering, i.e. local correction
function trajectory_correction_1to1(beamlinename,beamname,list_bpm,list_corr,corrxattr,corryattr,Ax,Ay)

% one could iterate...
nimax=3;
for ni=1:nimax
    [emit,beaminitial]=placet_test_no_correction(beamlinename,beamname,"None");

    bpm_x_initial=(placet_element_get_attribute(beamlinename,list_bpm,"reading_x"))';
    bpm_y_initial=(placet_element_get_attribute(beamlinename,list_bpm,"reading_y"))';

    new_strength_x=-Ax\bpm_x_initial;
    new_strength_y=-Ay\bpm_y_initial;

    placet_element_vary_attribute(beamlinename,list_corr,corrxattr,new_strength_x);
    placet_element_vary_attribute(beamlinename,list_corr,corryattr,new_strength_y);
end;

endfunction;
%----------------------------------------


%----------------------------------------
function [UT,Sinv,V]=mysvd(A,sd)

[U,S,V]=svd(A);

% octave svd returns simplified matrices missing rows and columns, which only contain zeros
sizeU=size(U);
sizeA=size(A);
if (sizeU(1)<sizeA(1))
  U=[U;zeros(sizeA(1)-sizeU(1),sizeU(2))];
end;
if (sizeU(2)<sizeA(2))
  U=[U,zeros(sizeU(1),sizeA(2)-sizeU(2))];
end;
sizeS=size(S);
if (sizeS(1)<sizeA(2))
  S=[S;zeros(sizeA(2)-sizeS(1),sizeS(2))];
end;
if (sizeS(2)<sizeA(2))
  S=[S,zeros(sizeS(1),sizeA(2)-sizeS(2))];
end;

if sd==1
  for i=1:length(S)
    if S(i,i)<0.00001
      S(i,i)=0.0;
    end;
  end;
end;


Sinv=pinv(S);
UT=U';

endfunction;
%----------------------------------------
