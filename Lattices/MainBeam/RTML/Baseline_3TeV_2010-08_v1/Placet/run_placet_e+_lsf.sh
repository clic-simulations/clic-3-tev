#!/bin/bash
#
echo "Job started: " $( date )
# I am unsure if this sets the working directory to the directory where this script is located
# or to the directory from where this script was started
# since in all cases I tried until now they where the same I did not bother to find out
# LS_SUBCWD is provided by LSF automatically
cd $LS_SUBCWD
# if you do not do this, the default working directory will be set by LSF to something like /pool/lsf/<jobid> or similar
# then you have to take care to copy your files there!

# set up some job related stuff
simdirname=./simrun_e+
mkdir $simdirname
cp -R ./Lattices_common $simdirname
cp -R ./Lattices_e+ $simdirname
cp -R ./Parameters $simdirname
cp -R ./Placet_Octave $simdirname
cp -R ./Scripts $simdirname
cp ./main_e+.tcl $simdirname
cd $simdirname
#You have to use the name of your Placet binary. It must be the version with octave.
#The grep is used to get rid of some rather useless warnings which might come up in huge numbers.
#
#placet64-octave main.tcl 2>&1 | tee placet.out
#placet64-octave main.tcl 2> placet.err | grep --line-buffered -v "too large z" - | tee placet.out
#placet-octave main_e+.tcl 2> placet.err | grep --line-buffered -v "too large z" - | tee placet.out
placet-octave main_e+.tcl &>placet.out
#placet64-octave main.tcl | grep -v "too large z" - | grep -v "Cannot track a particle" - | tee placet.out
echo "Job finished: " $( date )
