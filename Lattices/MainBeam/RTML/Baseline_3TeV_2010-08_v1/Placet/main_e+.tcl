#
# CLIC Main Beam RTML
#
#
puts ""
puts "Setting up RTML tracking"
puts ""

set latticedir ./Lattices_e+
set scriptdir ./Scripts
set paramsdir ./Parameters

# define the array placetunits for unit conversions
# placetunits=[xyz,xpyp,energy,charge,emittance]
source $scriptdir/placet_units.tcl

#
# load some external files, i.e. parameters, lattices, scripts
#

# load standard beam parameters
# beamXXX=[betax,alphax,emitnx,betay,alphay,emitny,sigmaz,charge,
#          uncespr,echirp,energy]
# units=[m,rad,m*rad,m,rad,m*rad,m,C,1,1/m,eV]
# for use in Placet some of these untis have to be converted using placetunits
source $paramsdir/initial_beam_parameters.tcl

#
# load standard rf parameters
# rfparamsXXX=[gradient,gradientww,phase,wavelength,a,g,l]
# units=[eV/m,eV/m,Degree,m,m,m,m]
# for use in Placet some of these untis have to be converted using placetunits
source $paramsdir/rf_parameters.tcl

#
# load procedures to set up lattices
source $latticedir/0010_match_dr_to_rtml.tcl
source $latticedir/0020_diagnostics_1.tcl
source $latticedir/0030_dump_and_match_diag_to_sr.tcl
source $latticedir/0040_spin_rotator.tcl
source $latticedir/0050_match_sr_to_bc1_rf.tcl
source $latticedir/0060_bc1_rf.tcl
source $latticedir/0070_match_bc1_rf_to_chicane.tcl
source $latticedir/0080_bc1_chicane.tcl
source $latticedir/0090_match_bc1_to_diag.tcl
source $latticedir/0100_diagnostics_2.tcl
source $latticedir/0110_dump_and_match_diag_to_booster.tcl
source $latticedir/0120_booster_linac.tcl
source $latticedir/0130_dump_and_match_booster_to_ca.tcl
source $latticedir/0140_central_arc.tcl
source $latticedir/0150_vertical_transfer.tcl
source $latticedir/0160_match_vt_to_ltl.tcl
source $latticedir/0170_long_transfer_line.tcl
source $latticedir/0180_dump_and_match_ltl_to_tal.tcl
source $latticedir/0190_turn_around_loop.tcl
source $latticedir/0200_match_tal_to_bc2_rf.tcl
source $latticedir/0210_bc2_rf.tcl
source $latticedir/0220_match_bc2_rf_to_chicane_1.tcl
source $latticedir/0230_bc2_chicane_1.tcl
source $latticedir/0240_match_bc2_chicanes.tcl
source $latticedir/0250_bc2_chicane_2.tcl
source $latticedir/0260_match_bc2_to_diag.tcl
source $latticedir/0270_diagnostics_3.tcl
source $latticedir/0280_dump_and_match_rtml_to_main_linac.tcl

#
# load procedures to set up cavities and beams
source $scriptdir/beamsetup.tcl
source $scriptdir/cavitywakesetup.tcl

#
# load tracking procedures
source $scriptdir/tracking.tcl

#
# set some general parameters which should not change throughout tracking
set beamparams(nslice) 1001
set beamparams(nmacro) 100
set beamparams(nsigmabunch) 4
set beamparams(nsigmawake) 6

# this is needed to concatenate data at the correct z position since each step
# restarts at z=0.0
set zoffset 0.0
set fid [open zoffset.dat w]
puts $fid "# Step / Start Position"
puts $fid "1   $zoffset"
close $fid

#
# track through first part of RTML, i.e, up to entrance of BC1 RF
puts ""
puts "Setting up tracking step step1 using beamline rtmlpart1 and beam beam1"
puts ""

# general beam and rf parameters
#
# beamparams=[betax,alphax,emitnx,betay,alphay,emitny,sigmaz,charge,
#             uncespread,echirp,energy,nslice,nmacro,nsigma]
# units=[m,rad,m*rad,m,rad,m*rad,m,C,1,1/m,eV,1,1,1]
# hence, in the array the values are stored in Placet units
set beamparams(betax) $beam0010(betax)
set beamparams(alphax) $beam0010(alphax)
set beamparams(emitnx) [expr $beam0010(emitnx)*$placetunits(emittance)]
set beamparams(betay) $beam0010(betay)
set beamparams(alphay) $beam0010(alphay)
set beamparams(emitny) [expr $beam0010(emitny)*$placetunits(emittance)]
set beamparams(sigmaz) [expr $beam0010(sigmaz)*$placetunits(xyz)]
set beamparams(meanz) [expr $beam0010(meanz)*$placetunits(xyz)]
set beamparams(charge) [expr $beam0010(charge)*$placetunits(charge)]
set beamparams(uncespread) $beam0010(uncespr)
set beamparams(echirp) [expr $beam0010(echirp)/$placetunits(xyz)]
set beamparams(energy) [expr $beam0010(energy)*$placetunits(energy)]

set beamparams(meanx) 0.0
set beamparams(meanxp) 0.0
set beamparams(meany) 0.0
set beamparams(meanyp) 0.0

set beamparams(startenergy) [expr $beam0010(energy)*$placetunits(energy)]
set beamparams(meanenergy) [expr $beam0010(energy)*$placetunits(energy)]

set beamparams(useisr) 1
set beamparams(usewakefields) 1

# Creation of particles file and wakes file has to be done early
# since the average energy loss due to the wakes is required for
# proper beam line setup.
create_particles_file particles.in beamparams
set dewake [create_wakes_file wake1.dat beamparams rfparamsbc1 particles.in]

#
#setup of lattice and beam
#
set wakelongrangedata(nmodes) 0
set wakelongrangedata(modes) {}
initialize_cavities rfparamsbc1 wakelong1 wakelongrangedata
BeamlineNew
Girder
TclCall -script {BeamDump -file bunch_step_1_in.dat}

lattice_match_dr_to_rtml beamparams
lattice_diagnostics_1 beamparams
lattice_dump_and_match_diag_to_sr beamparams
lattice_spin_rotator beamparams
lattice_match_sr_to_bc1_rf beamparams

TclCall -script {BeamDump -file bunch_step_1_out.dat}
BeamlineSet -name rtmlpart1

FirstOrder 0

make_particle_beam beam1 beamparams particles.in wake1.dat

# perform tracking
# set new values in beamparams
do_tracking rtmlpart1 beam1 beamparams 1

# end of first tracking part


#
# track through second part of RTML, i.e, up to entrance of the Booster Linac
puts ""
puts "Setting up tracking step step2 using beamline rtmlpart2 and beam beam2"
puts ""

# Creation of the wakes file has to be done early since the average energy
# loss due to the wakes is required for proper beam line setup.
set dewake [create_wakes_file wake2.dat beamparams rfparamsbc1 bunch_step_1_out.dat]

if {$beamparams(usewakefields)==1} {
  set rfparamsbc1(gradient) [expr $rfparamsbc1(gradientww)*$placetunits(energy)]
  set rfparamsbc1(dewake) $dewake
  } else {
  set rfparamsbc1(gradient) [expr $rfparamsbc1(gradient)*$placetunits(energy)]
  set rfparamsbc1(dewake) 0.0
  }

#
#setup of lattice and beam
#
set wakelongrangedata(nmodes) 0
set wakelongrangedata(modes) {}
initialize_cavities rfparamsbc1 wakelong2 wakelongrangedata
BeamlineNew
Girder
TclCall -script {BeamDump -file bunch_step_2_in.dat}

lattice_bc1_rf rfparamsbc1 beamparams
lattice_match_bc1_rf_to_chicane beamparams
lattice_bc1_chicane beamparams
lattice_match_bc1_to_diag beamparams
lattice_diagnostics_2 beamparams
lattice_dump_and_match_diag_to_booster beamparams

TclCall -script {BeamDump -file bunch_step_2_out.dat}
BeamlineSet -name rtmlpart2

FirstOrder 0

make_particle_beam beam2 beamparams bunch_step_1_out.dat wake2.dat

# perform tracking
# set new values in beamparams
do_tracking rtmlpart2 beam2 beamparams 2

# end of second tracking part


#
# track through third part of RTML, i.e, up to entrance of BC2 RF
puts ""
puts "Setting up tracking step step3 using beamline rtmlpart3 and beam beam3"
puts ""

# Creation of the wakes file has to be done early since the average energy
# loss due to the wakes is required for proper beam line setup.
set dewake [create_wakes_file wake3.dat beamparams rfparamsbooster bunch_step_2_out.dat]

if {$beamparams(usewakefields)==1} {
  set rfparamsbooster(gradient) [expr $rfparamsbooster(gradientww)*$placetunits(energy)]
  set rfparamsbooster(dewake) $dewake
  } else {
  set rfparamsbooster(gradient) [expr $rfparamsbooster(gradient)*$placetunits(energy)]
  set rfparamsbooster(dewake) 0.0
  }

#
#setup of lattice and beam
#
set wakelongrangedata(nmodes) 0
set wakelongrangedata(modes) {}
initialize_cavities rfparamsbooster wakelong3 wakelongrangedata
BeamlineNew
Girder
TclCall -script {BeamDump -file bunch_step_3_in.dat}

lattice_booster_linac rfparamsbooster beamparams
lattice_dump_and_match_booster_to_ca beamparams
lattice_central_arc beamparams
lattice_vertical_transfer beamparams
lattice_match_vt_to_ltl beamparams
lattice_long_transfer_line beamparams
lattice_dump_and_match_ltl_to_tal beamparams
lattice_turn_around_loop beamparams
lattice_match_tal_to_bc2_rf beamparams

TclCall -script {BeamDump -file bunch_step_3_out.dat}
BeamlineSet -name rtmlpart3

FirstOrder 0

make_particle_beam beam3 beamparams bunch_step_2_out.dat wake3.dat

# perform tracking
# set new values in beamparams
do_tracking rtmlpart3 beam3 beamparams 3

# end of third tracking part


#
# track through fourth part of RTML, i,e, up to entrance of main linac
puts ""
puts "Setting up tracking step step4 using beamline rtmlpart4 and beam beam4"
puts ""

# Creation of the wakes file has to be done early since the average energy
# loss due to the wakes is required for proper beam line setup.
set dewake [create_wakes_file wake4.dat beamparams rfparamsbc2 bunch_step_3_out.dat]

if {$beamparams(usewakefields)==1} {
  set rfparamsbc2(gradient) [expr $rfparamsbc2(gradientww)*$placetunits(energy)]
  set rfparamsbc2(dewake) $dewake
  } else {
  set rfparamsbc2(gradient) [expr $rfparamsbc2(gradient)*$placetunits(energy)]
  set rfparamsbc2(dewake) 0.0
  }

#
#setup of lattice and beam
#
set wakelongrangedata(nmodes) 0
set wakelongrangedata(modes) {}
initialize_cavities rfparamsbc2 wakelong4 wakelongrangedata
BeamlineNew
Girder
TclCall -script {BeamDump -file bunch_step_4_in.dat}

lattice_bc2_rf rfparamsbc2 beamparams
lattice_match_bc2_rf_to_chicane_1 beamparams
lattice_bc2_chicane_1 beamparams
lattice_match_bc2_chicanes beamparams
lattice_bc2_chicane_2 beamparams
lattice_match_bc2_to_diag beamparams
lattice_diagnostics_3 beamparams
lattice_dump_and_match_rtml_to_main_linac beamparams

TclCall -script {BeamDump -file bunch_step_4_out.dat}
BeamlineSet -name rtmlpart4

FirstOrder 0

make_particle_beam beam4 beamparams bunch_step_3_out.dat wake4.dat

# perform tracking
# set new values in beamparams
do_tracking rtmlpart4 beam4 beamparams 4

# end of fourth tracking part

