#
# matching of BC2 RF to BC2 chicane 1
#
# beamparams=[betax,alphax,emitnx,betay,alphay,emitny,sigmaz,charge,
#             uncespread,echirp,energy,nslice,nmacro,nsigmabunch,nsigmawake]
# units=[m,rad,m*rad,m,rad,m*rad,m,C,1,1/m,eV,1,1,1,1]
#
# where necessary units were converted to placet units in main.tcl

proc lattice_match_bc2_rf_to_chicane_1 {bparray} {
upvar $bparray beamparams

set usesixdim 1
set numthinlenses 100
set quad_synrad 0

set refenergy $beamparams(meanenergy)

set lquadm 0.3

set kqm1 -0.04336853627
set kqm2 -0.2563854768
set kqm3 0.1513787453
set kqm4 0.1702557789

set ldm1 0.5
set ldm2 4.0
set ldm3 2.2
set ldm4 0.5
set ldm5 0.5

SetReferenceEnergy $refenergy

Girder
Drift -length $ldm1 -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm1*$lquadm*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ldm2 -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm2*$lquadm*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ldm3 -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm3*$lquadm*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ldm4 -six_dim $usesixdim
Bpm -length 0.0
Quadrupole -synrad $quad_synrad -length $lquadm -strength [expr $kqm4*$lquadm*$refenergy] -six_dim $usesixdim -thin_lens $numthinlenses
Drift -length $ldm5 -six_dim $usesixdim
}
