

source defaults.tcl 

set simp_cor_plt {}
set dfs_cor_plt  {}
set wfs_cor_plt  {}

set ltfl "1_3" 
set qli 0.18 
set qki 2.6 
set qli2 0.25 
set qki2 0.68 
set nstr 1 
set nstr2 3

set ti_start [clock clicks -milliseconds] 

set lattice_file dbl_lat_$ltfl.0.tcl
set beamlinename drive_beam_linac_$ltfl

set beam0_indist particles0.in
set beam1_indist particles1.in

set b0_in beam0.$ltfl.in
set b0_ou beam0.$ltfl.ou

set dbl1_q_k  $qki
set dbl1_q_l  $qli
set dbl1_nstr $nstr
set dbl1_acc_sec_l   [expr $dbl1_nstr*$acc_cav(total_l)]
set l_btwn_qds_bdl1  [expr $dbl1_acc_sec_l+0.5]

# fodo starts with half quad
array set BeamDefine [match type=fodo position=center lq1=$dbl1_q_l kq1=$dbl1_q_k lq2=$dbl1_q_l kq2=-$dbl1_q_k l=$l_btwn_qds_bdl1 integrate=false plot=false]
set e0 0.05
set dE 1.00 ; # percent

array set BeamDefine "sigma_z 3.5e3 n_slice 201 emitt_x 300 emitt_y 300 charge  5.2500e+10 energy $e0"
array set BeamDefine "e_spread $dE n_macro 501 type gaussian name beam0 filename $beam0_indist n_bunch 1 "
set BeamDefine(npart) [expr $BeamDefine(n_macro)*$BeamDefine(n_slice)]
lassign "$BeamDefine(beta_x) $BeamDefine(beta_y) $BeamDefine(alpha_x) $BeamDefine(alpha_y)" btx bty alx aly 


BeamlineNew
Girder
source $script_dir/$lattice_file

BeamlineSet -name $beamlinename
BeamlineUse -name $beamlinename

# # creating default beam = beam0
make_particle_beam BeamDefine 
FirstOrder 1

# # creating correction beam = beam1
set BeamDefine(name)  beam1
set BeamDefine(charge) [expr $BeamDefine(charge)*$dcharge ]
make_particle_beam_read BeamDefine $BeamDefine(filename)
# set BeamDefine(filename) $beam1_indist
# make_particle_beam BeamDefine 
FirstOrder 1



if { $wgt1 == -1 } {
    set wgt1 [expr round(sqrt(($bpmres * $bpmres + $sigma * $sigma) / (2 * $bpmres * $bpmres)))]
    puts "Using DFS theoretical weight = $wgt1"
}

if { $wgt2 == -1 } {
    set wgt2 [expr round(sqrt(($bpmres * $bpmres + $sigma * $sigma) / (2 * $bpmres * $bpmres)))]
    puts "Using WFS theoretical weight = $wgt2"
}


Octave {
# apply wakes for all structures
	CAVITs = placet_get_number_list("$beamlinename", "cavity");
	placet_element_set_attribute("$beamlinename", CAVITs, "six_dim", true);
	placet_element_set_attribute("$beamlinename", CAVITs, "short_range_wake", "acc_cav_SR_W");
    placet_element_set_attribute("$beamlinename", CAVITs, "lambda", $acc_cav(lambda));
    
#  6d tracing in bunch compression
   SI = placet_get_number_list("$beamlinename", "sbend");
   placet_element_set_attribute("$beamlinename", SI, "six_dim", true);
  
}

Octave {

    function reduce_energy()

        LN1_CAVs = placet_get_name_number_list("$beamlinename", "DBL1_ACC_0");
        LN1_GRDs = placet_element_get_attribute("$beamlinename",LN1_CAVs,"gradient");        
        placet_element_vary_attribute("$beamlinename", LN1_CAVs, "gradient", -(LN1_GRDs.*$dgrad1/100));  
        
#         LN2_CAVs = placet_get_name_number_list("$beamlinename", "DBL2_ACC_0");
#         LN2_GRDs = placet_element_get_attribute("$beamlinename",LN2_CAVs,"gradient");
#         placet_element_vary_attribute("$beamlinename", LN2_CAVs, "gradient", -(LN2_GRDs.*$dgrad2/100));
    end
    
    function reset_energy()
        LN1_CAVs = placet_get_name_number_list("$beamlinename", "DBL1_ACC_0");
        LN1_GRDs = placet_element_get_attribute("$beamlinename",LN1_CAVs,"gradient");
        placet_element_vary_attribute("$beamlinename", LN1_CAVs, "gradient", +(LN1_GRDs.*(100*$dgrad1/(100-$dgrad1))/100));
        
#         LN2_CAVs = placet_get_name_number_list("$beamlinename", "DBL1_ACC_0");
#         LN2_GRDs = placet_element_get_attribute("$beamlinename",LN2_CAVs,"gradient");
#         placet_element_vary_attribute("$beamlinename", LN2_CAVs, "gradient", +(LN2_GRDs.*(100*$dgrad2/(100-$dgrad2))/100));
    end
}

# Octave {
#     R = placet_get_transfer_matrix("$beamlinename");
# }




Octave {
    if exist('../R_${beamlinename}_g_${dgrad1}_${dgrad2}_c_${dcharge}.dat.gz', "file")
    load '../R_${beamlinename}_g_${dgrad1}_${dgrad2}_c_${dcharge}.dat.gz';
    else
    printf("creating response matrix for %s \n" , "$beamlinename")
    # get correctors and bpms
    Response.Cx = placet_get_number_list("$beamlinename", "dipole");
    Response.Cy = placet_get_number_list("$beamlinename", "dipole");
    Response.Bpms = placet_get_number_list("$beamlinename", "bpm");
    
    # picks all correctors preceeding the last bpm, and all bpms following the first corrector
    Response.Cx = Response.Cx(Response.Cx < Response.Bpms(end));
    Response.Cy = Response.Cy(Response.Cy < Response.Bpms(end));
    Response.Bpms = Response.Bpms(Response.Bpms > max(Response.Cx(1), Response.Cy(1)));

    
     # gets the energy at each corrector
    Response.Ex = placet_element_get_attribute("$beamlinename", Response.Cx, "e0");
    Response.Ey = placet_element_get_attribute("$beamlinename", Response.Cy, "e0");
    
    # compute response matrices
    placet_test_no_correction("$beamlinename", "beam0", "Zero");
    Response.B0 = placet_get_bpm_readings("$beamlinename", Response.Bpms);
    Response.R0x = placet_get_response_matrix_attribute ("$beamlinename", "beam0", Response.Bpms, "x", Response.Cx, "strength_x", "Zero");
    Response.R0y = placet_get_response_matrix_attribute ("$beamlinename", "beam0", Response.Bpms, "y", Response.Cy, "strength_y", "Zero");
    
    reduce_energy();
    placet_test_no_correction("$beamlinename", "beam0", "Zero");
    Response.B1 = placet_get_bpm_readings("$beamlinename", Response.Bpms) - Response.B0;
    Response.R1x = placet_get_response_matrix_attribute ("$beamlinename", "beam0", Response.Bpms, "x", Response.Cx, "strength_x", "Zero") - Response.R0x;
    Response.R1y = placet_get_response_matrix_attribute ("$beamlinename", "beam0", Response.Bpms, "y", Response.Cy, "strength_y", "Zero") - Response.R0y;
    
    reset_energy();
    placet_test_no_correction("$beamlinename", "beam1", "Zero");
    Response.B2 = placet_get_bpm_readings("$beamlinename", Response.Bpms) - Response.B0;
    Response.R2x = placet_get_response_matrix_attribute ("$beamlinename", "beam1", Response.Bpms, "x", Response.Cx, "strength_x", "Zero") - Response.R0x;
    Response.R2y = placet_get_response_matrix_attribute ("$beamlinename", "beam1", Response.Bpms, "y", Response.Cy, "strength_y", "Zero") - Response.R0y;
    save -text -zip '../R_${beamlinename}_g_${dgrad1}_${dgrad2}_c_${dcharge}.dat.gz' Response;
    end
}



proc my_survey {} {
  global machine sigma sigmap bpmres beamlinename nm acc_cav

  Octave {
    randn("seed", $nm * 12345);
    
    BPMs = placet_get_number_list("$beamlinename", "bpm");
    CORs = placet_get_number_list("$beamlinename", "dipole");
    SNDs = placet_get_number_list("$beamlinename", "sbend");
    QUAs = placet_get_number_list("$beamlinename", "quadrupole");
    ACCs = placet_get_number_list("$beamlinename", "cavity");
   
    placet_element_set_attribute("$beamlinename", BPMs, "resolution", $bpmres);
    placet_element_set_attribute("$beamlinename", BPMs, "x", randn(size(BPMs)) * $sigma);
    placet_element_set_attribute("$beamlinename", BPMs, "y", randn(size(BPMs)) * $sigma);	
    
    placet_element_set_attribute("$beamlinename", CORs, "strength_x", 0.0);
    placet_element_set_attribute("$beamlinename", CORs, "strength_y", 0.0);
    
    placet_element_set_attribute("$beamlinename", QUAs, "x", randn(size(QUAs)) * $sigma);
    placet_element_set_attribute("$beamlinename", QUAs, "y", randn(size(QUAs)) * $sigma);
    
#     placet_element_set_attribute("$beamlinename", SNDs, "x", randn(size(SNDs)) * $sigma);
#     placet_element_set_attribute("$beamlinename", SNDs, "y", randn(size(SNDs)) * $sigma);    
       
    placet_element_set_attribute("$beamlinename", ACCs, "xp", randn(size(ACCs))*$sigmap);
    placet_element_set_attribute("$beamlinename", ACCs, "yp", randn(size(ACCs))*$sigmap);
    placet_element_set_attribute("$beamlinename", ACCs, "x",  randn(size(ACCs))*$sigma);
    placet_element_set_attribute("$beamlinename", ACCs, "y",  randn(size(ACCs))*$sigma);

#     set evertying to zero on BC
    BPM_Es= placet_get_name_number_list("$beamlinename", "BC1_*");
    placet_element_set_attribute("$beamlinename", BPM_Es, "x", 0.0);
    placet_element_set_attribute("$beamlinename", BPM_Es, "y", 0.0);
    
   }
}


if { $machine == "all" } {
    set machine_start 1
    set machine_end $nm
} {
    set machine_start $machine
    set machine_end $machine
}

Octave {
    E0=0;
    E1=0;
    E2=0;
    E3=0;
    dE_f=[];
    Bpm0=0;
    Bpm1=0;
    EmittX_hist=zeros($nm, 4);
    EmittY_hist=zeros($nm, 4);
}

Octave {
    [E,B] = placet_test_no_correction("$beamlinename", "$BeamDefine(name)", "Zero", "%s %E %dE %ex %ey %sz");
    save -text -ascii 'default.ou' B;
}


#  save beam on different positions
proc reset_global_vars {} {
    global dump_index
    set dump_index 0
}

proc dump_beam {} {
  global dump_index
  set dumpnames {inj_out.dump lin1_out.dump  bc1_out.dump lin2_out.dump}
  BeamDump -file [lindex  $dumpnames $dump_index]
  incr dump_index
}



Octave {
  
    DUMPS = [placet_get_name_number_list("$beamlinename", "DBL1_*")(1)
             placet_get_name_number_list("$beamlinename", "DBL1_*")(end) 
             placet_get_name_number_list("$beamlinename", "BC1_*")(end)
             placet_get_name_number_list("$beamlinename", "DBL2_*")(end)];
    placet_element_set_attribute("$beamlinename", 0, "tclcall_entrance", "reset_global_vars");
    placet_element_set_attribute("$beamlinename", DUMPS, "tclcall_exit", "dump_beam");

}

Octave {
  [s, beta_x, beta_y, alpha_x, alpha_y] = placet_evolve_beta_function("$beamlinename", $btx, $alx, $bty, $aly);
  twiss= [s  beta_x  beta_y  alpha_x alpha_y];
  save -text $beamlinename.twi twiss
#   plot(s,beta_x,s,beta_y)


}



# Binning
Octave {
    nBpms = length(Response.Bpms);
    printf("total number of bpms %g \n", nBpms);
    Blen = nBpms / ($nbins * (1 - $noverlap) + $noverlap);
    for i=1:$nbins
      Bmin = floor((i - 1) * Blen - (i - 1) * Blen * $noverlap) + 1;
      Bmax = floor((i)     * Blen - (i - 1) * Blen * $noverlap);
      Cxmin = find(Response.Cx < Response.Bpms(Bmin))(end);
      Cxmax = find(Response.Cx < Response.Bpms(Bmax))(end);
      Cymin = find(Response.Cy < Response.Bpms(Bmin))(end);
      Cymax = find(Response.Cy < Response.Bpms(Bmax))(end);
      Bins(i).Bpms = Bmin:Bmax;
      Bins(i).Cx = Cxmin:Cxmax;
      Bins(i).Cy = Cymin:Cymax;
    end
    
    printf("Each bin contains approx %g bpms.\n", round(Blen));
}



for {set machine $machine_start} {$machine <= $machine_end} {incr machine} {
    
    puts "MACHINE: $machine/$nm"
    
    my_survey
    
    Octave {
	disp("TRACKING...");
	[E,B] = placet_test_no_correction("$beamlinename", "$BeamDefine(name)", "None", "%s %E %dE %ex %ey %sz");
	E0 += E;
	EmittX_hist($machine, 1) = E(end,2);
	EmittY_hist($machine, 1) = E(end,6);
	
	disp("1-TO-1 CORRECTION");
        for bin = 1:$nbins
	  Bin = Bins(bin);
	  nCx = length(Bin.Cx);
  	  nCy = length(Bin.Cy);
   	  R0x = Response.R0x(Bin.Bpms,Bin.Cx);
   	  R0y = Response.R0y(Bin.Bpms,Bin.Cy);
	  for i=1:3
	    B0 = placet_get_bpm_readings("$beamlinename", Response.Bpms);
	    B0 -= Response.B0;
   	    B0 = B0(Bin.Bpms,:);
            Cx = -[ R0x ; $beta0 * eye(nCx) ] \ [ B0(:,1) ; zeros(nCx,1) ];
	    Cy = -[ R0y ; $beta0 * eye(nCy) ] \ [ B0(:,2) ; zeros(nCy,1) ];
	    placet_element_vary_attribute("$beamlinename", Response.Cx(Bin.Cx), "strength_x", Cx);
	    placet_element_vary_attribute("$beamlinename", Response.Cy(Bin.Cy), "strength_y", Cy);
	    [E,B] = placet_test_no_correction("$beamlinename", "$BeamDefine(name)", "None", 1, 0, Response.Bpms(Bin.Bpms(end)), "%s %E %dE %ex %ey %sz");
	  end
	end

	E1 += E;
	EmittX_hist($machine, 2) = E(end,2);
	EmittY_hist($machine, 2) = E(end,6);

	disp("DFS CORRECTION");
        for bin = 1:$nbins
	  Bin = Bins(bin);
	  nCx = length(Bin.Cx);
  	  nCy = length(Bin.Cy);
   	  R0x = Response.R0x(Bin.Bpms,Bin.Cx);
   	  R0y = Response.R0y(Bin.Bpms,Bin.Cy);
   	  R1x = Response.R1x(Bin.Bpms,Bin.Cx);
   	  R1y = Response.R1y(Bin.Bpms,Bin.Cy);
	  for i=1:3
	    B0 = placet_get_bpm_readings("$beamlinename", Response.Bpms);
	    reduce_energy();
	    placet_test_no_correction("$beamlinename", "$BeamDefine(name)", "None", 1, 0, Response.Bpms(Bin.Bpms(end)));
	    reset_energy();
	    B1 = placet_get_bpm_readings("$beamlinename", Response.Bpms) - B0;
	    B0 -= Response.B0;
	    B1 -= Response.B1;
            B0 = B0(Bin.Bpms,:);
            B1 = B1(Bin.Bpms,:);
	    Cx = -[ R0x ; $wgt1 * R1x ; $beta1 * eye(nCx) ] \ [ B0(:,1) ; $wgt1 * B1(:,1) ; zeros(nCx,1) ];
	    Cy = -[ R0y ; $wgt1 * R1y ; $beta1 * eye(nCy) ] \ [ B0(:,2) ; $wgt1 * B1(:,2) ; zeros(nCy,1) ];
	    placet_element_vary_attribute("$beamlinename", Response.Cx(Bin.Cx), "strength_x", Cx);
	    placet_element_vary_attribute("$beamlinename", Response.Cy(Bin.Cy), "strength_y", Cy);
	    [E,B] = placet_test_no_correction("$beamlinename", "$BeamDefine(name)", "None", 1, 0, Response.Bpms(Bin.Bpms(end)), "%s %E %dE %ex %ey %sz");
	  end
	end

	E2 += E;
	EmittX_hist($machine, 3) = E(end,2);
	EmittY_hist($machine, 3) = E(end,6);

	disp("WFS CORRECTION");
        for bin = 1:$nbins
	  Bin = Bins(bin);
	  nCx = length(Bin.Cx);
  	  nCy = length(Bin.Cy);
   	  R0x = Response.R0x(Bin.Bpms,Bin.Cx);
   	  R0y = Response.R0y(Bin.Bpms,Bin.Cy);
   	  R1x = Response.R1x(Bin.Bpms,Bin.Cx);
   	  R1y = Response.R1y(Bin.Bpms,Bin.Cy);
   	  R2x = Response.R2x(Bin.Bpms,Bin.Cx);
   	  R2y = Response.R2y(Bin.Bpms,Bin.Cy);
	  for i=1:3
	    B0 = placet_get_bpm_readings("$beamlinename", Response.Bpms);
	    % DFS
	    reduce_energy();
	    placet_test_no_correction("$beamlinename", "$BeamDefine(name)", "None", 1, 0, Response.Bpms(Bin.Bpms(end)));
	    reset_energy();
	    B1 = placet_get_bpm_readings("$beamlinename", Response.Bpms) - B0;
        % WFS
	    placet_test_no_correction("$beamlinename", "beam1", "None", 1, 0, Response.Bpms(Bin.Bpms(end)));
	    B2 = placet_get_bpm_readings("$beamlinename", Response.Bpms) - B0;
	    %
	    B0 -= Response.B0;
	    B1 -= Response.B1;
	    B2 -= Response.B2;
            B0 = B0(Bin.Bpms,:);
            B1 = B1(Bin.Bpms,:);
            B2 = B2(Bin.Bpms,:);
	    Cx = -[ R0x ; $wgt1 * R1x ; $wgt2 * R2x ; $beta2 * eye(nCx) ] \ [ B0(:,1) ; $wgt1 * B1(:,1) ; $wgt2 * B2(:,1) ; zeros(nCx,1) ];
	    Cy = -[ R0y ; $wgt1 * R1y ; $wgt2 * R2y ; $beta2 * eye(nCy) ] \ [ B0(:,2) ; $wgt1 * B1(:,2) ; $wgt2 * B2(:,2) ; zeros(nCy,1) ];
	    placet_element_vary_attribute("$beamlinename", Response.Cx(Bin.Cx), "strength_x", Cx);
	    placet_element_vary_attribute("$beamlinename", Response.Cy(Bin.Cy), "strength_y", Cy);
	    [E,B] = placet_test_no_correction("$beamlinename", "$BeamDefine(name)", "None", 1, 0, Response.Bpms(Bin.Bpms(end)), "%s %E %dE %ex %ey %sz");
	  end
	end

        save -text ${beamlinename}_beam_${machine}.dat B;

	E3 += E;
	EmittX_hist($machine, 4) = E(end,2);
	EmittY_hist($machine, 4) = E(end,6);
	dE_f = [ dE_f ; std(B(:,1)) ];
    }
}

set nm [expr $machine_end - $machine_start + 1]

Octave {
    E0 /= $nm;
    E1 /= $nm;
    E2 /= $nm;
    E3 /= $nm;
    save -text ${beamlinename}_emitt_no_n_${nm}.dat E0
    save -text ${beamlinename}_emitt_simple_b_${beta0}_n_${nm}.dat E1
    save -text ${beamlinename}_emitt_dfs_w_${wgt1}_b_${beta0}_${beta1}_n_${nm}.dat E2
    save -text ${beamlinename}_emitt_wfs_w_${wgt1}_${wgt2}_b_${beta0}_${beta1}_${beta2}_c_${dcharge}_n_${nm}.dat E3
    save -text ${beamlinename}_dE_${nm}.dat dE_f
}


BeamDelete -beam "beam0"
BeamDelete -beam "beam1"
BeamlineDelete -name $beamlinename
exec rm -rf particles0.in 
array set BeamDefine {}

lappend simp_cor_plt '${beamlinename}_emitt_simple_b_${beta0}_n_${nm}.dat' u 1:4 w l ti 'simple cor n1=$nstr n2=$nstr' 
lappend dfs_cor_plt  '${beamlinename}_emitt_dfs_w_${wgt1}_b_${beta0}_${beta1}_n_${nm}.dat' u 1:4 w l ti 'dfs cor n1=$nstr n2=$nstr' 
lappend wfs_cor_plt  '${beamlinename}_emitt_wfs_w_${wgt1}_${wgt2}_b_${beta0}_${beta1}_${beta2}_c_${dcharge}_n_${nm}.dat' u 1:4 w l ti 'wfs cor n1=$nstr n2=$nstr' 

print_time $ti_start

set fplt [open "../plot_emit_cmds.dat" w]

puts $fplt "$simp_cor_plt\n"
puts $fplt "$dfs_cor_plt\n"
puts $fplt "$wfs_cor_plt\n" 


