set fp [open "dbl_lat_1_3.0.tcl" r]
set file_data [read $fp]
close $fp

set tcl_precision 15

set cav_length 2.3994984
set cav_gradient [expr 4.047622286 / $cav_length / 1e3 ] ; # accelerating voltage (in full beam loading regime) / active length
set cav_ph_dbl1 27.5
set cav_ph_dbl2 18.0

set e0 0.050508409761675

set data [split $file_data "\n"]
foreach line $data {
    set words [regexp -all -inline {\S+} $line ]
    if { [lindex $words 0 ] == "SetReferenceEnergy" } {
	set e0 [lindex $words 1 ]
    } else {
	set modified 0
	set appendix ""
	if { [lindex $words 0 ] == "Quadrupole" } {
	    set modified 1
	    set k1l "[expr [lindex $words 6 ] / $e0 ]"
	    set words [ lreplace $words 6 6 "\[expr \$e0*$k1l\]" ]
	} elseif { [lindex $words 0 ] == "Cavity" } {
	    set modified 1
	    set de "[expr [lindex $words 4 ] * [lindex $words 6 ] * cos([lindex $words 8 ] * acos(-1.0) / 180.0) ]"
	    set appendix "\nset e0 \[expr \$e0 + \$cav_gradient * \$cav_length * cos([lindex $words 8 ] * acos(-1.0) / 180.0)\]\nSetReferenceEnergy \$e0"
	} 
	
	if { $modified == 0 } {
	    puts $line
	} else {
	    foreach word $words {
		puts -nonewline "$word "
	    }
	    puts $appendix
	}
    }

}

