# scripts to define short and long range wakes
# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# %% structures with 24 cell@1GHZ  and 19 cell &20 cell @ 750 MHz  RBp=49 45 40 35 30 mm  Pin=15-30 MW
# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

set RF_power      20
set cell_length   0.0999791
set bore_radi     0.050
set current 	  4.21 
set efficency 	  0.97
set frequency     9.9952e+08
set no_of_cel     24

# *****************************************************************************************
# average diameter and gap
# *****************************************************************************************
proc mean {x} {
    set sm 0.0
    foreach i $x { set sm [expr $sm + $i] }
    return [expr $sm/[llength $x]]
}
	
set diameter_all  {0.246765 0.24577 0.244795 0.243819 0.242822 0.241787 0.240692 0.239518 0.238247 0.236858 0.235331 0.233648 \
                0.231789 0.229734 0.227464 0.224959 0.222199 0.219166 0.21584 0.2122 0.208229 0.203905 }
set dia [mean $diameter_all]


set gap_all  {0.0811621 0.0797883 0.0784212 0.0770512 0.0756685 0.0742634 0.0728263 0.0713475 0.0698172 0.0682257 0.0665635 \
                0.0648206 0.0629876 0.0610545 0.0590119 0.0568499 0.0545589 0.0521292 0.049551 0.0468147 0.0439106 0.0408289 }
set gap [mean $gap_all]

set clight  299792458
set pi      [expr acos(-1.0)]    ;# pi number
set Z0      [expr 120.0*$pi]     ;# empadance of free space
set c       $clight	         ;# speed of light
set lambda  [expr $clight/$frequency]
set freqg   [expr $frequency*1e-9]


set actv_struc_l    [format " %.8f " [expr  $no_of_cel*$cell_length ]]
set tot_struc_l     [format " %.8f " [expr  double(round(100.0*$cell_length*($no_of_cel+4))/100.0)]]
set edge_length     [format " %.8f " [expr  0.5*($tot_struc_l-$actv_struc_l)]]

set acc_voltage    [format " %.8f " [expr  $RF_power*$efficency/$current]]
set acc_voltage_e  [format " %.2f " [expr  $acc_voltage*1e6]]
set acc_gradien    [format " %.8f " [expr  $acc_voltage/$actv_struc_l*1e-3]]


array set acc_cav  "grad $acc_gradien volt $acc_voltage volt_e $acc_voltage_e freq $freqg aper $bore_radi \
                     gap $gap  cell_l $cell_length cell_no $no_of_cel active_l $actv_struc_l total_l $tot_struc_l \
                     lambda $lambda diameter $dia"


set lambda $acc_cav(lambda)


Octave {
acc_cav =struct('grad',$acc_cav(grad),'freq',$acc_cav(freq),'aper',$acc_cav(aper), 'gap',$acc_cav(gap),
                'cell_l',$acc_cav(cell_l),'cell_no',$acc_cav(cell_no),'total_l',$acc_cav(total_l));

}
