# set gradient 0.00192043304134
set cav_gradient [expr 4.047622286 / 2.3994984 ] ; # accelerating voltage (in full beam loading regime) / active length
set cav_ph_dbl1 27.5
set cav_ph_dbl2 18.0

set e0 0.050508409761675

Girder
SetReferenceEnergy 0.050508409761675
Drift      -name  "DBL1_DRF_MOD_H" -length 1.4 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
#TclCall    -script    {BeamDump -file WINJ.dat} 
Quadrupole -name     "DBL1_QD_F_H" -length 0.18  -strength 0.0236379357684000
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL1_ACC_0" -length 2.3994984  -gradient $cav_gradient  -phase $cav_ph_dbl1 
SetReferenceEnergy  0.054494507859021
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name       "DBL1_QD_D" -length 0.18  -strength -0.025503429678 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL1_ACC_0" -length 2.3994984  -gradient $cav_gradient -phase $cav_ph_dbl1
SetReferenceEnergy  0.05848050375658499
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name       "DBL1_QD_F" -length 0.18  -strength 0.0273688757581 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL1_ACC_0" -length 2.3994984  -gradient $cav_gradient -phase $cav_ph_dbl1
SetReferenceEnergy  0.062466448554257994
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name       "DBL1_QD_D" -length 0.18  -strength -0.0292342979234 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL1_ACC_0" -length 2.3994984  -gradient $cav_gradient -phase $cav_ph_dbl1
SetReferenceEnergy  0.066452393351931
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name       "DBL1_QD_F" -length 0.18  -strength 0.0310997200887 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL1_ACC_0" -length 2.3994984  -gradient $cav_gradient -phase $cav_ph_dbl1
SetReferenceEnergy  0.070438338149604
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name       "DBL1_QD_D" -length 0.18  -strength -0.032965142254 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL1_ACC_0" -length 2.3994984  -gradient $cav_gradient -phase $cav_ph_dbl1
SetReferenceEnergy  0.074424231847386
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name       "DBL1_QD_F" -length 0.18  -strength 0.0348305405046 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL1_ACC_0" -length 2.3994984  -gradient $cav_gradient -phase $cav_ph_dbl1
SetReferenceEnergy  0.07841012554516799
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name       "DBL1_QD_D" -length 0.18  -strength -0.0366959387551 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL1_ACC_0" -length 2.3994984  -gradient $cav_gradient -phase $cav_ph_dbl1
SetReferenceEnergy  0.08239601924295001
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name       "DBL1_QD_F" -length 0.18  -strength 0.0385613370057 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL1_ACC_0" -length 2.3994984  -gradient $cav_gradient -phase $cav_ph_dbl1
SetReferenceEnergy  0.086381861840841
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name       "DBL1_QD_D" -length 0.18  -strength -0.0404267113415 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL1_ACC_0" -length 2.3994984  -gradient $cav_gradient -phase $cav_ph_dbl1
SetReferenceEnergy  0.090367755538623
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name       "DBL1_QD_F" -length 0.18  -strength 0.0422921095921 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL1_ACC_0" -length 2.3994984  -gradient $cav_gradient -phase $cav_ph_dbl1
SetReferenceEnergy  0.09435359813651399
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name       "DBL1_QD_D" -length 0.18  -strength -0.0441574839279 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL1_ACC_0" -length 2.3994984  -gradient $cav_gradient -phase $cav_ph_dbl1
SetReferenceEnergy  0.09833944073440501
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name       "DBL1_QD_F" -length 0.18  -strength 0.0460228582637 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL1_ACC_0" -length 2.3994984  -gradient $cav_gradient -phase $cav_ph_dbl1
SetReferenceEnergy  0.102325283332296
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name       "DBL1_QD_D" -length 0.18  -strength -0.0478882325995 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL1_ACC_0" -length 2.3994984  -gradient $cav_gradient -phase $cav_ph_dbl1
SetReferenceEnergy  0.106311125930187
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name       "DBL1_QD_F" -length 0.18  -strength 0.0497536069353 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL1_ACC_0" -length 2.3994984  -gradient $cav_gradient -phase $cav_ph_dbl1
SetReferenceEnergy  0.110296917428187
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name       "DBL1_QD_D" -length 0.18  -strength -0.0516189573564 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL1_ACC_0" -length 2.3994984  -gradient $cav_gradient -phase $cav_ph_dbl1
SetReferenceEnergy  0.114282760026078
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name       "DBL1_QD_F" -length 0.18  -strength 0.0534843316922 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL1_ACC_0" -length 2.3994984  -gradient $cav_gradient -phase $cav_ph_dbl1
SetReferenceEnergy  0.11826855152407799
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name       "DBL1_QD_D" -length 0.18  -strength -0.0553496821133 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL1_ACC_0" -length 2.3994984  -gradient $cav_gradient -phase $cav_ph_dbl1
SetReferenceEnergy  0.122254394121969
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name       "DBL1_QD_F" -length 0.18  -strength 0.0572150564491 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL1_ACC_0" -length 2.3994984  -gradient $cav_gradient -phase $cav_ph_dbl1
SetReferenceEnergy  0.126240185619969
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name       "DBL1_QD_D" -length 0.18  -strength -0.0590804068701 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL1_ACC_0" -length 2.3994984  -gradient $cav_gradient -phase $cav_ph_dbl1
SetReferenceEnergy  0.130225977117969
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name       "DBL1_QD_F" -length 0.18  -strength 0.0609457572912 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL1_ACC_0" -length 2.3994984  -gradient $cav_gradient -phase $cav_ph_dbl1
SetReferenceEnergy  0.134211768615969
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name       "DBL1_QD_D" -length 0.18  -strength -0.0628111077123 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL1_ACC_0" -length 2.3994984  -gradient $cav_gradient -phase $cav_ph_dbl1
SetReferenceEnergy  0.138197560113969
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name       "DBL1_QD_F" -length 0.18  -strength 0.0646764581333 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL1_ACC_0" -length 2.3994984  -gradient $cav_gradient -phase $cav_ph_dbl1
SetReferenceEnergy  0.142183351611969
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name       "DBL1_QD_D" -length 0.18  -strength -0.0665418085544 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL1_ACC_0" -length 2.3994984  -gradient $cav_gradient -phase $cav_ph_dbl1
SetReferenceEnergy  0.146169143109969
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name       "DBL1_QD_F" -length 0.18  -strength 0.0684071589755 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL1_ACC_0" -length 2.3994984  -gradient $cav_gradient -phase $cav_ph_dbl1
SetReferenceEnergy  0.150154934607969
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name       "DBL1_QD_D" -length 0.18  -strength -0.0702725093965 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL1_ACC_0" -length 2.3994984  -gradient $cav_gradient -phase $cav_ph_dbl1
SetReferenceEnergy  0.154140726105969
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name       "DBL1_QD_F" -length 0.18  -strength 0.0721378598176 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL1_ACC_0" -length 2.3994984  -gradient $cav_gradient -phase $cav_ph_dbl1
SetReferenceEnergy  0.15812651760396898
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name       "DBL1_QD_D" -length 0.18  -strength -0.0740032102387 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL1_ACC_0" -length 2.3994984  -gradient $cav_gradient -phase $cav_ph_dbl1
SetReferenceEnergy  0.16211225800207796
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name       "DBL1_QD_F" -length 0.18  -strength 0.075868536745 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL1_ACC_0" -length 2.3994984  -gradient $cav_gradient -phase $cav_ph_dbl1
SetReferenceEnergy  0.166098049500078
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name       "DBL1_QD_D" -length 0.18  -strength -0.077733887166 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL1_ACC_0" -length 2.3994984  -gradient $cav_gradient -phase $cav_ph_dbl1
SetReferenceEnergy  0.170083789898187
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name       "DBL1_QD_F" -length 0.18  -strength 0.0795992136724 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL1_ACC_0" -length 2.3994984  -gradient $cav_gradient -phase $cav_ph_dbl1
SetReferenceEnergy  0.17406958139618697
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name       "DBL1_QD_D" -length 0.18  -strength -0.0814645640934 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL1_ACC_0" -length 2.3994984  -gradient $cav_gradient -phase $cav_ph_dbl1
SetReferenceEnergy  0.17805537289418696
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name       "DBL1_QD_F" -length 0.18  -strength 0.0833299145145 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL1_ACC_0" -length 2.3994984  -gradient $cav_gradient -phase $cav_ph_dbl1
SetReferenceEnergy  0.182041113292296
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name       "DBL1_QD_D" -length 0.18  -strength -0.0851952410208 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL1_ACC_0" -length 2.3994984  -gradient $cav_gradient -phase $cav_ph_dbl1
SetReferenceEnergy  0.186026853690405
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name       "DBL1_QD_F" -length 0.18  -strength 0.0870605675271 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL1_ACC_0" -length 2.3994984  -gradient $cav_gradient -phase $cav_ph_dbl1
SetReferenceEnergy  0.190012645188405
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name       "DBL1_QD_D" -length 0.18  -strength -0.0889259179482 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL1_ACC_0" -length 2.3994984  -gradient $cav_gradient -phase $cav_ph_dbl1
SetReferenceEnergy  0.193998385586514
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name       "DBL1_QD_F" -length 0.18  -strength 0.0907912444545 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL1_ACC_0" -length 2.3994984  -gradient $cav_gradient -phase $cav_ph_dbl1
SetReferenceEnergy  0.197984177084514
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name       "DBL1_QD_D" -length 0.18  -strength -0.0926565948756 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL1_ACC_0" -length 2.3994984  -gradient $cav_gradient -phase $cav_ph_dbl1
SetReferenceEnergy  0.20196991748262297
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name       "DBL1_QD_F" -length 0.18  -strength 0.0945219213819 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL1_ACC_0" -length 2.3994984  -gradient $cav_gradient -phase $cav_ph_dbl1
SetReferenceEnergy  0.205955657880732
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name       "DBL1_QD_D" -length 0.18  -strength -0.0963872478882 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL1_ACC_0" -length 2.3994984  -gradient $cav_gradient -phase $cav_ph_dbl1
SetReferenceEnergy  0.20994139827884098
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name       "DBL1_QD_F" -length 0.18  -strength 0.0982525743945 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL1_ACC_0" -length 2.3994984  -gradient $cav_gradient -phase $cav_ph_dbl1
SetReferenceEnergy  0.213927189776841
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name       "DBL1_QD_D" -length 0.18  -strength -0.100117924816 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL1_ACC_0" -length 2.3994984  -gradient $cav_gradient -phase $cav_ph_dbl1
SetReferenceEnergy  0.21791293017495
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name       "DBL1_QD_F" -length 0.18  -strength 0.101983251322 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL1_ACC_0" -length 2.3994984  -gradient $cav_gradient -phase $cav_ph_dbl1
SetReferenceEnergy  0.22189867057305898
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name       "DBL1_QD_D" -length 0.18  -strength -0.103848577828 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL1_ACC_0" -length 2.3994984  -gradient $cav_gradient -phase $cav_ph_dbl1
SetReferenceEnergy  0.225884410971168
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name       "DBL1_QD_F" -length 0.18  -strength 0.105713904335 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL1_ACC_0" -length 2.3994984  -gradient $cav_gradient -phase $cav_ph_dbl1
SetReferenceEnergy  0.229870151369277
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name       "DBL1_QD_D" -length 0.18  -strength -0.107579230841 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL1_ACC_0" -length 2.3994984  -gradient $cav_gradient -phase $cav_ph_dbl1
SetReferenceEnergy  0.233855891767386
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name       "DBL1_QD_F" -length 0.18  -strength 0.109444557347 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL1_ACC_0" -length 2.3994984  -gradient $cav_gradient -phase $cav_ph_dbl1
SetReferenceEnergy  0.237841632165495
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name       "DBL1_QD_D" -length 0.18  -strength -0.111309883853 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL1_ACC_0" -length 2.3994984  -gradient $cav_gradient -phase $cav_ph_dbl1
SetReferenceEnergy  0.241827423663495
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name       "DBL1_QD_F" -length 0.18  -strength 0.113175234275 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL1_ACC_0" -length 2.3994984  -gradient $cav_gradient -phase $cav_ph_dbl1
SetReferenceEnergy  0.245813164061604
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name       "DBL1_QD_D" -length 0.18  -strength -0.115040560781 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL1_ACC_0" -length 2.3994984  -gradient $cav_gradient -phase $cav_ph_dbl1
SetReferenceEnergy  0.24979890445971298
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name       "DBL1_QD_F" -length 0.18  -strength 0.116905887287 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL1_ACC_0" -length 2.3994984  -gradient $cav_gradient -phase $cav_ph_dbl1
SetReferenceEnergy  0.253784644857822
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name       "DBL1_QD_D" -length 0.18  -strength -0.118771213793 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL1_ACC_0" -length 2.3994984  -gradient $cav_gradient -phase $cav_ph_dbl1
SetReferenceEnergy  0.257770385255931
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name       "DBL1_QD_F" -length 0.18  -strength 0.1206365403 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL1_ACC_0" -length 2.3994984  -gradient $cav_gradient -phase $cav_ph_dbl1
SetReferenceEnergy  0.26175612565404
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name       "DBL1_QD_D" -length 0.18  -strength -0.122501866806 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL1_ACC_0" -length 2.3994984  -gradient $cav_gradient -phase $cav_ph_dbl1
SetReferenceEnergy  0.265741814952258
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name       "DBL1_QD_F" -length 0.18  -strength 0.124367169398 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL1_ACC_0" -length 2.3994984  -gradient $cav_gradient -phase $cav_ph_dbl1
SetReferenceEnergy  0.26972755535036697
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name       "DBL1_QD_D" -length 0.18  -strength -0.126232495904 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL1_ACC_0" -length 2.3994984  -gradient $cav_gradient -phase $cav_ph_dbl1
SetReferenceEnergy  0.27371329574847597
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name       "DBL1_QD_F" -length 0.18  -strength 0.12809782241 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL1_ACC_0" -length 2.3994984  -gradient $cav_gradient -phase $cav_ph_dbl1
SetReferenceEnergy  0.277699036146585
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name       "DBL1_QD_D" -length 0.18  -strength -0.129963148917 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL1_ACC_0" -length 2.3994984  -gradient $cav_gradient -phase $cav_ph_dbl1
SetReferenceEnergy  0.2816847765446939
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name       "DBL1_QD_F" -length 0.18  -strength 0.131828475423 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL1_ACC_0" -length 2.3994984  -gradient $cav_gradient -phase $cav_ph_dbl1
SetReferenceEnergy  0.28567051694280304
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name       "DBL1_QD_D" -length 0.18  -strength -0.133693801929 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL1_ACC_0" -length 2.3994984  -gradient $cav_gradient -phase $cav_ph_dbl1
SetReferenceEnergy  0.28965625734091205
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name       "DBL1_QD_F" -length 0.18  -strength 0.135559128436 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL1_ACC_0" -length 2.3994984  -gradient $cav_gradient -phase $cav_ph_dbl1
SetReferenceEnergy  0.293641997739021
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name       "DBL1_QD_D" -length 0.18  -strength -0.137424454942 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL1_ACC_0" -length 2.3994984  -gradient $cav_gradient -phase $cav_ph_dbl1
SetReferenceEnergy  0.297627687037239
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name       "DBL1_QD_F" -length 0.18  -strength 0.139289757533 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL1_ACC_0" -length 2.3994984  -gradient $cav_gradient -phase $cav_ph_dbl1
SetReferenceEnergy  0.301613427435348
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name       "DBL1_QD_D" -length 0.18  -strength -0.14115508404 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL1_ACC_0" -length 2.3994984  -gradient $cav_gradient -phase $cav_ph_dbl1
SetReferenceEnergy  0.305599167833457
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name       "DBL1_QD_F" -length 0.18  -strength 0.143020410546 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DRF_MOD_H" -length 1.4 
#TclCall    -script    {BeamDump -file WDBL1.dat} 
Drift      -name       "BC1_DR_V1" -length 0.0120354686708 
Quadrupole -name       "BC1_QD_V1" -length 0.18  -strength -0.219514739043 
Drift      -name       "BC1_DR_V2" -length 3.45493793856 
Quadrupole -name       "BC1_QD_V2" -length 0.18  -strength 0.239071511568 
Drift      -name       "BC1_DR_V3" -length 0.422271451131 
Quadrupole -name       "BC1_QD_V3" -length 0.18  -strength -0.233766995415 
Drift      -name       "BC1_DR_V4" -length 0.0144012843373 
Drift      -name       "BC1_DR_25" -length 0.25 
Sbend      -name      "BC1_SBND_1" -length 0.50097713  -angle 0.10821041  -E1 0 -E2 0.10821041 -e0 0.305599167833 -six_dim 1 
Drift      -name       "BC1_DR_15" -length 0.15 
Dipole     -name       "BC1_COR_0" -length 0 
Drift      -name       "BC1_DR_10" -length 0.1 
Drift      -name      "BC1_DR_SID" -length 3.02059207 
Drift      -name       "BC1_DR_10" -length 0.1 
Bpm        -name       "BC1_BPM_0" -length 0 
Drift      -name       "BC1_DR_15" -length 0.15 
Sbend      -name      "BC1_SBND_2" -length 0.50097713  -angle -0.10821041  -E1 -0.10821041 -E2 0 -e0 0.305599167833 -six_dim 1 
Drift      -name       "BC1_DR_15" -length 0.15 
Dipole     -name       "BC1_COR_0" -length 0 
Drift      -name       "BC1_DR_10" -length 0.1 
Drift      -name      "BC1_DR_CNT" -length 0.5 
Drift      -name       "BC1_DR_10" -length 0.1 
Bpm        -name       "BC1_BPM_0" -length 0 
Drift      -name       "BC1_DR_15" -length 0.15 
Sbend      -name      "BC1_SBND_3" -length 0.50097713  -angle -0.10821041  -E1 0 -E2 -0.10821041 -e0 0.305599167833 -six_dim 1 
Drift      -name       "BC1_DR_15" -length 0.15 
Dipole     -name       "BC1_COR_0" -length 0 
Drift      -name       "BC1_DR_10" -length 0.1 
Drift      -name      "BC1_DR_SID" -length 3.02059207 
Drift      -name       "BC1_DR_10" -length 0.1 
Bpm        -name       "BC1_BPM_0" -length 0 
Drift      -name       "BC1_DR_15" -length 0.15 
Sbend      -name      "BC1_SBND_4" -length 0.50097713  -angle 0.10821041  -E1 0.10821041 -E2 0 -e0 0.305599167833 -six_dim 1 
Drift      -name       "BC1_DR_25" -length 0.25 
#TclCall    -script    {BeamDump -file WBC1.dat} 
Drift      -name      "DBL2_DR_V1" -length 0.000427638825863 
Quadrupole -name      "DBL2_QD_V1" -length 0.25  -strength -1.5348873743e-08 
Drift      -name      "DBL2_DR_V2" -length 9.0620968745e-08 
Quadrupole -name      "DBL2_QD_V2" -length 0.25  -strength 0.095792590913 
Drift      -name      "DBL2_DR_V3" -length 2.66224567021 
Quadrupole -name      "DBL2_QD_V3" -length 0.25  -strength -0.0784700293554 
Drift      -name      "DBL2_DR_V4" -length 5.58785652111 
Drift      -name  "DBL2_DRF_MOD_H" -length 4.2 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_F" -length 0.25  -strength 0.0519518585317 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.31010949971257196
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.314619780491796
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.319130112370911
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_D" -length 0.25  -strength -0.0542521191031 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.323640393150135
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.32815072502924997
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.332661005808474
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_F" -length 0.25  -strength 0.0565523709874 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.33717128658769796
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.341681618466813
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.34619189924603694
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_D" -length 0.25  -strength -0.0588526228718 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.350702180025261
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.355212460804485
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.3597227926836
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_F" -length 0.25  -strength 0.0611528747562 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.36423307346282396
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.368743354242048
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.37325363502127196
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_D" -length 0.25  -strength -0.0634531179536 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.37776391580049595
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.38227419657971995
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.386784477358944
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_F" -length 0.25  -strength 0.065753361151 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.391294758138168
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.395805038917392
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.400315319696616
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_D" -length 0.25  -strength -0.0680536043484 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.40482560047584004
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.409335830155173
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.41384611093439705
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_F" -length 0.25  -strength 0.0703538388588 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.41835639171362093
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.422866672492845
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.427376953272069
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_D" -length 0.25  -strength -0.0726540820563 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.43188718295140194
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.436397463730626
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.44090774450985
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_F" -length 0.25  -strength 0.0749543165667 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.445418025289074
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.449928254968407
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.45443853574763104
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_D" -length 0.25  -strength -0.0772545510771 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.458948816526855
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.46345904620618805
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.46796932698541194
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_F" -length 0.25  -strength 0.0795547855875 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.47247960776463593
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.476989837443969
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.48150011822319294
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_D" -length 0.25  -strength -0.0818550200979 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.486010399002417
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.49052062868174995
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.495030909460974
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_F" -length 0.25  -strength 0.0841552546084 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.499541139140307
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.504051419919531
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.508561700698755
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_D" -length 0.25  -strength -0.0864554891188 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.51307203257787
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.51758210895753
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.5220926963361
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_F" -length 0.25  -strength 0.0887557583771 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.5266027727157601
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.53111284909542
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.5356234364739899
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_D" -length 0.25  -strength -0.0910559842006 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.5401335128536501
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.54464358923331
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.5491541766118799
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_F" -length 0.25  -strength 0.093356210024 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.5536642529915399
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.5581743293712
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.56268491674977
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_D" -length 0.25  -strength -0.0956564358475 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.5671949931294299
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.57170506950909
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.57621565688766
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_F" -length 0.25  -strength 0.0979566616709 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.5807257332673199
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.58523580964698
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.58974639702555
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_D" -length 0.25  -strength -0.100256887494 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.59425647340521
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.59876654978487
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.60327713716344
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_F" -length 0.25  -strength 0.102557113318 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.6077872135431001
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.6122978009216701
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.61680787730133
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_D" -length 0.25  -strength -0.104857339141 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.62131795368099
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.62582854105956
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.6303386174392199
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_F" -length 0.25  -strength 0.107157564965 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.63484869381888
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.63935928119745
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.64386935757711
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_D" -length 0.25  -strength -0.109457790788 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.64837943395677
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.65289002133534
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.6574000977150001
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_F" -length 0.25  -strength 0.111758016612 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.66191017409466
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.6664207614732299
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.67093083785289
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_D" -length 0.25  -strength -0.114058242435 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.67544091423255
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.67995150161112
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.68446157799078
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_F" -length 0.25  -strength 0.116358468258 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.68897165437044
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.69348224174901
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.6979923181286699
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_D" -length 0.25  -strength -0.118658694082 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.7025023945083299
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.7070129818868999
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.7115230582665599
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_F" -length 0.25  -strength 0.120958919905 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.71603313464622
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.72054372202479
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.72505379840445
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_D" -length 0.25  -strength -0.123259145729 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.72956387478411
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.73407446216268
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.73858453854234
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_F" -length 0.25  -strength 0.125559371552 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.743094614922
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.74760520230057
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.75211527868023
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_D" -length 0.25  -strength -0.127859597376 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.75662535505989
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.7611359424384601
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.7656460188181201
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_F" -length 0.25  -strength 0.130159823199 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.7701560951977799
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.77466668257635
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.77917675895601
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_D" -length 0.25  -strength -0.132460049023 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.78368683533567
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.7881974227142399
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.7927074990939
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_F" -length 0.25  -strength 0.134760274846 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.79721757547356
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.8017281628521299
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.8062382392317899
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_D" -length 0.25  -strength -0.137060500669 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.8107483156114499
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.8152589029900199
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.81976897936968
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_F" -length 0.25  -strength 0.139360726493 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.82427956674825
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.82878964312791
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.83329971950757
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_D" -length 0.25  -strength -0.141660952316 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.83781030688614
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.8423203832658
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.8468304596454599
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_F" -length 0.25  -strength 0.14396117814 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.8513410470240299
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.85585112340369
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.8603611997833499
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_D" -length 0.25  -strength -0.146261403963 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.8648717871619199
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.86938186354158
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.8738919399212399
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_F" -length 0.25  -strength 0.148561629787 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.8784025272998099
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.88291260367947
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.88742268005913
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_D" -length 0.25  -strength -0.15086185561 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.8919332674377001
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.89644334381736
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.9009534201970201
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_F" -length 0.25  -strength 0.153162081433 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.9054640075755901
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.90997408395525
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.9144841603349101
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_D" -length 0.25  -strength -0.155462307257 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.91899474771348
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.9235048240931398
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.9280149004728
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_F" -length 0.25  -strength 0.15776253308 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.9325254878513699
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.93703556423103
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.9415456406106899
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_D" -length 0.25  -strength -0.160062758904 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.9460562279892599
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.95056630436892
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.9550763807485799
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_F" -length 0.25  -strength 0.162362984727 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.9595869681271499
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.96409704450681
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.9686071208864699
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_D" -length 0.25  -strength -0.164663210551 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.97311770826504
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.9776277846447
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.98213786102436
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_F" -length 0.25  -strength 0.166963436374 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.98664844840293
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.9911585247825899
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  0.99566860116225
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_D" -length 0.25  -strength -0.169263662198 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.00017918854082
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.00468926492048
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.0091993413001399
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_F" -length 0.25  -strength 0.171563888021 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.01370992867871
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.0182200050583698
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.02273008143803
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_D" -length 0.25  -strength -0.173864113844 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.0272406688166
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.03175074519626
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.03626082157592
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_F" -length 0.25  -strength 0.176164339668 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.04077140895449
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.0452814853341499
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.04979156171381
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_D" -length 0.25  -strength -0.178464565491 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.05430214909238
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.05881222547204
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.0633223018517
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_F" -length 0.25  -strength 0.180764791315 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.06783288923027
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.07234296560993
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.07685304198959
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_D" -length 0.25  -strength -0.183065017138 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.08136362936816
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.08587370574782
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.09038378212748
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_F" -length 0.25  -strength 0.185365242962 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.09489436950605
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.09940444588571
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.10391452226537
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_D" -length 0.25  -strength -0.187665468785 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.10842510964394
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.1129351860236
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.11744526240326
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_F" -length 0.25  -strength 0.189965694609 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.1219558497818298
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.12646592616149
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.13097600254115
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_D" -length 0.25  -strength -0.192265920432 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.13548658991972
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.13999666629938
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.14450725367795
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_F" -length 0.25  -strength 0.194566233125 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.1490173300576099
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.15352740643727
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.15803799381584
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_D" -length 0.25  -strength -0.196866458949 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.1625480701955
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.16705814657516
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.17156873395373
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_F" -length 0.25  -strength 0.199166684772 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.1760788103333901
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.18058888671305
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.1850994740916199
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_D" -length 0.25  -strength -0.201466910596 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.1896095504712798
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.1941196268509398
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.19863021422951
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_F" -length 0.25  -strength 0.203767136419 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.2031402906091702
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.20765036698883
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.2121609543674
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_D" -length 0.25  -strength -0.206067362242 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.2166710307470598
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.2211811071267198
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.22569169450529
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_F" -length 0.25  -strength 0.208367588066 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.2302017708849502
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.2347118472646101
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.23922243464318
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_D" -length 0.25  -strength -0.210667813889 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.2437325110228399
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.2482425874025
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.25275317478107
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_F" -length 0.25  -strength 0.212968039713 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.2572632511607298
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.2617733275403897
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.26628391491896
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_D" -length 0.25  -strength -0.215268265536 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.27079399129862
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.27530406767828
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.2798146550568499
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_F" -length 0.25  -strength 0.21756849136 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.2843247314365098
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.2888348078161698
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.29334539519474
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_D" -length 0.25  -strength -0.219868717183 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.2978554715744002
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.3023655479540601
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.30687613533263
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_F" -length 0.25  -strength 0.222168943007 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.3113862117122899
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.3158962880919498
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.32040687547052
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_D" -length 0.25  -strength -0.22446916883 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.32491695185018
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.32942702822984
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.33393761560841
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_F" -length 0.25  -strength 0.226769394653 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.33844769198807
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.34295776836773
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.3474683557463
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_D" -length 0.25  -strength -0.229069620477 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.35197843212596
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.35648901950453
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.36099909588419
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_F" -length 0.25  -strength 0.2313698463 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.36550917226385
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.3700197596424197
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.37452983602208
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_D" -length 0.25  -strength -0.233670072124 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.3790399124017398
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.38355049978031
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.38806057615997
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_F" -length 0.25  -strength 0.235970297947 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.3925706525396302
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.3970812399182
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.40159131629786
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_D" -length 0.25  -strength -0.238270523771 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.4061013926775199
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.41061198005609
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.4151220564357498
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_F" -length 0.25  -strength 0.240570749594 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.41963213281541
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.42414272019398
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.42865279657364
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_D" -length 0.25  -strength -0.242870975418 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.4331628729533
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.43767346033187
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.4421835367115299
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_F" -length 0.25  -strength 0.245171201241 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.44669361309119
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.45120420046976
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.45571427684942
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_D" -length 0.25  -strength -0.247471427064 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.46022435322908
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.46473494060765
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.4692450169873101
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_F" -length 0.25  -strength 0.249771652888 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.47375509336697
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.4782656807455399
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.4827757571251998
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_D" -length 0.25  -strength -0.252071878711 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.4872858335048598
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.4917964208834298
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.49630649726309
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_F" -length 0.25  -strength 0.254372104535 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.50081657364275
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.50532716102132
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.5098372374009799
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_D" -length 0.25  -strength -0.256672330358 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.51434731378064
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.51885790115921
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.52336797753887
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_F" -length 0.25  -strength 0.258972556182 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.52787856491744
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.5323886412971
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.53689871767676
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_D" -length 0.25  -strength -0.261272782005 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.54140930505533
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.54591938143499
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.5504294578146498
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_F" -length 0.25  -strength 0.263573007828 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.55494004519322
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.55945012157288
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.5639601979525402
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_D" -length 0.25  -strength -0.265873233652 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.56847078533111
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.57298086171077
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.5774909380904298
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_F" -length 0.25  -strength 0.268173459475 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.582001525469
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.58651160184866
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.5910216782283202
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_D" -length 0.25  -strength -0.270473685299 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.59553226560689
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.60004234198655
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.6045524183662099
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_F" -length 0.25  -strength 0.272773911122 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.60906300574478
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.6135730821244398
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.6180831585041002
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_D" -length 0.25  -strength -0.275074136946 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.62259374588267
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.62710382226233
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.63161389864199
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_F" -length 0.25  -strength 0.277374362769 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.6361244860205597
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.6406345624002199
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.6451446387798798
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_D" -length 0.25  -strength -0.279674588593 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.64965522615845
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.65416530253811
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.65867537891777
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_F" -length 0.25  -strength 0.281974814416 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.6631859662963397
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.667696042676
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.6722066300545702
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_D" -length 0.25  -strength -0.284275127109 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.67671670643423
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.68122678281389
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.6857373701924598
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_F" -length 0.25  -strength 0.286575352933 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.69024744657212
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.69475752295178
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.6992681103303502
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_D" -length 0.25  -strength -0.288875578756 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.70377818671001
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.7082882630896699
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.7127988504682399
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_F" -length 0.25  -strength 0.29117580458 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.7173089268479
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.72181900322756
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.7263295906061298
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_D" -length 0.25  -strength -0.293476030403 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.7308396669857897
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.7353497433654497
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.7398603307440201
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_F" -length 0.25  -strength 0.295776256226 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.74437040712368
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.74888048350334
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.7533910708819098
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_D" -length 0.25  -strength -0.29807648205 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.7579011472615698
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.76241122364123
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.7669218110198002
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_F" -length 0.25  -strength 0.300376707873 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.7714318873994601
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.7759419637791198
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.7804525511576899
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_D" -length 0.25  -strength -0.302676933697 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.7849626275373498
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.7894732149159198
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.79398329129558
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_F" -length 0.25  -strength 0.30497715952 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.79849336767524
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.80300395505381
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.80751403143347
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_D" -length 0.25  -strength -0.307277385344 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.81202410781313
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.8165346951917
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.82104477157136
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_F" -length 0.25  -strength 0.309577611167 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.82555484795102
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.83006543532959
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.83457551170925
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_D" -length 0.25  -strength -0.311877836991 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.83908558808891
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.84359617546748
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.8481062518471398
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_F" -length 0.25  -strength 0.314178062814 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.8526163282267998
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.8571269156053698
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.86163699198503
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_D" -length 0.25  -strength -0.316478288637 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.86614706836469
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.87065765574326
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.8751677321229199
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_F" -length 0.25  -strength 0.318778514461 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.8796778085025798
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.8841883958811498
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.88869847226081
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_D" -length 0.25  -strength -0.321078740284 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.89320905963938
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.89771913601904
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.9022292123987
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_F" -length 0.25  -strength 0.323378966108 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.90673979977727
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.91124987615693
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.91575995253659
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_D" -length 0.25  -strength -0.325679191931 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.92027053991516
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.92478061629482
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.9292906926744797
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_F" -length 0.25  -strength 0.327979417755 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.93380128005305
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.9383113564327101
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.94282143281237
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_D" -length 0.25  -strength -0.330279643578 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.9473320201909399
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.9518420965705998
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.9563521729502598
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_F" -length 0.25  -strength 0.332579869402 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.9608627603288298
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.96537283670849
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.9698829130881501
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_D" -length 0.25  -strength -0.334880095225 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.97439350046672
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.9789035768463799
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.98341365322604
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_F" -length 0.25  -strength 0.337180321048 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.98792424060461
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.99243431698427
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  1.99694490436284
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_D" -length 0.25  -strength -0.339480633742 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  2.0014549807425
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  2.0059650571221597
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  2.0104756445007297
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_F" -length 0.25  -strength 0.341780859565 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  2.01498572088039
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  2.01949579726005
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  2.02400638463862
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name       "DBL2_QD_D" -length 0.25  -strength -0.344081085389 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  2.0285164610182798
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  2.03302653739794
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity     -name      "DBL2_ACC_0" -length 2.3994984  -gradient $cav_gradient   -phase $cav_ph_dbl2
SetReferenceEnergy  2.03753712477651
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name     "DBL2_QD_F_H" -length 0.25  -strength 0.346381311212000
#TclCall    -script    {BeamDump -file WDBL2.dat} 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DRF_MOD_H" -length 3.95 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
