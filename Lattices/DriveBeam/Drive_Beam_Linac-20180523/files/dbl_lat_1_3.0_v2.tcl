# set gradient 0.00192043304134
set cav_length 2.3994984
set cav_ph_dbl1 27.5
set cav_ph_dbl2 18.0
set cav_gradient_dbl1 [expr 4.047622286 / $cav_length / cos($cav_ph_dbl1 * acos(-1.0) / 180.0) / 1e3 ] ; # accelerating voltage (in full beam loading regime) / active length
set cav_gradient_dbl2 [expr 4.047622286 / $cav_length / cos($cav_ph_dbl2 * acos(-1.0) / 180.0) / 1e3 ] ; # accelerating voltage (in full beam loading regime) / active length

set bend_angle 0.163709486843262

set e0 0.050508409761675

Girder
SetReferenceEnergy $e0
Girder
Drift      -name  "DBL1_DRF_MOD_H" -length 1.4 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
#TclCall    -script    {BeamDump -file WINJ.dat} 
Quadrupole -name "DBL1_QD_F_H" -length 0.18 -strength [expr $e0*0.467999999998735] 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL1_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl1 -phase $cav_ph_dbl1 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name "DBL1_QD_D" -length 0.18 -strength [expr $e0*-0.467999999999599] 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL1_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl1 -phase $cav_ph_dbl1 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name "DBL1_QD_F" -length 0.18 -strength [expr $e0*0.468000000000312] 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL1_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl1 -phase $cav_ph_dbl1 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name "DBL1_QD_D" -length 0.18 -strength [expr $e0*-0.468000000000116] 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL1_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl1 -phase $cav_ph_dbl1 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name "DBL1_QD_F" -length 0.18 -strength [expr $e0*0.467999999999944] 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL1_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl1 -phase $cav_ph_dbl1 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name "DBL1_QD_D" -length 0.18 -strength [expr $e0*-0.467999999999792] 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL1_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl1 -phase $cav_ph_dbl1 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name "DBL1_QD_F" -length 0.18 -strength [expr $e0*0.468000000000314] 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL1_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl1 -phase $cav_ph_dbl1 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name "DBL1_QD_D" -length 0.18 -strength [expr $e0*-0.467999999999507] 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL1_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl1 -phase $cav_ph_dbl1 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name "DBL1_QD_F" -length 0.18 -strength [expr $e0*0.467999999999993] 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL1_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl1 -phase $cav_ph_dbl1 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name "DBL1_QD_D" -length 0.18 -strength [expr $e0*-0.467999999999843] 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL1_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl1 -phase $cav_ph_dbl1 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name "DBL1_QD_F" -length 0.18 -strength [expr $e0*0.46800000000027] 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL1_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl1 -phase $cav_ph_dbl1 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name "DBL1_QD_D" -length 0.18 -strength [expr $e0*-0.468000000000121] 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL1_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl1 -phase $cav_ph_dbl1 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name "DBL1_QD_F" -length 0.18 -strength [expr $e0*0.467999999999984] 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL1_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl1 -phase $cav_ph_dbl1 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name "DBL1_QD_D" -length 0.18 -strength [expr $e0*-0.467999999999858] 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL1_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl1 -phase $cav_ph_dbl1 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name "DBL1_QD_F" -length 0.18 -strength [expr $e0*0.467999999999741] 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL1_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl1 -phase $cav_ph_dbl1 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name "DBL1_QD_D" -length 0.18 -strength [expr $e0*-0.468000000000077] 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL1_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl1 -phase $cav_ph_dbl1 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name "DBL1_QD_F" -length 0.18 -strength [expr $e0*0.467999999999961] 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL1_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl1 -phase $cav_ph_dbl1 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name "DBL1_QD_D" -length 0.18 -strength [expr $e0*-0.468000000000266] 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL1_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl1 -phase $cav_ph_dbl1 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name "DBL1_QD_F" -length 0.18 -strength [expr $e0*0.468000000000151] 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL1_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl1 -phase $cav_ph_dbl1 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name "DBL1_QD_D" -length 0.18 -strength [expr $e0*-0.46799999999964] 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL1_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl1 -phase $cav_ph_dbl1 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name "DBL1_QD_F" -length 0.18 -strength [expr $e0*0.467999999999927] 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL1_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl1 -phase $cav_ph_dbl1 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name "DBL1_QD_D" -length 0.18 -strength [expr $e0*-0.468000000000197] 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL1_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl1 -phase $cav_ph_dbl1 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name "DBL1_QD_F" -length 0.18 -strength [expr $e0*0.467999999999729] 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL1_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl1 -phase $cav_ph_dbl1 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name "DBL1_QD_D" -length 0.18 -strength [expr $e0*-0.46799999999999] 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL1_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl1 -phase $cav_ph_dbl1 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name "DBL1_QD_F" -length 0.18 -strength [expr $e0*0.468000000000236] 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL1_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl1 -phase $cav_ph_dbl1 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name "DBL1_QD_D" -length 0.18 -strength [expr $e0*-0.467999999999804] 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL1_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl1 -phase $cav_ph_dbl1 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name "DBL1_QD_F" -length 0.18 -strength [expr $e0*0.468000000000042] 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL1_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl1 -phase $cav_ph_dbl1 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name "DBL1_QD_D" -length 0.18 -strength [expr $e0*-0.468000000000269] 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL1_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl1 -phase $cav_ph_dbl1 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name "DBL1_QD_F" -length 0.18 -strength [expr $e0*0.46800000000017] 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL1_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl1 -phase $cav_ph_dbl1 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name "DBL1_QD_D" -length 0.18 -strength [expr $e0*-0.46799999999978] 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL1_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl1 -phase $cav_ph_dbl1 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name "DBL1_QD_F" -length 0.18 -strength [expr $e0*0.468000000000285] 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL1_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl1 -phase $cav_ph_dbl1 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name "DBL1_QD_D" -length 0.18 -strength [expr $e0*-0.467999999999911] 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL1_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl1 -phase $cav_ph_dbl1 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name "DBL1_QD_F" -length 0.18 -strength [expr $e0*0.468000000000115] 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL1_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl1 -phase $cav_ph_dbl1 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name "DBL1_QD_D" -length 0.18 -strength [expr $e0*-0.46800000000003] 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL1_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl1 -phase $cav_ph_dbl1 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name "DBL1_QD_F" -length 0.18 -strength [expr $e0*0.467999999999949] 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL1_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl1 -phase $cav_ph_dbl1 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name "DBL1_QD_D" -length 0.18 -strength [expr $e0*-0.468000000000139] 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL1_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl1 -phase $cav_ph_dbl1 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name "DBL1_QD_F" -length 0.18 -strength [expr $e0*0.468000000000059] 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL1_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl1 -phase $cav_ph_dbl1 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name "DBL1_QD_D" -length 0.18 -strength [expr $e0*-0.46800000000024] 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL1_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl1 -phase $cav_ph_dbl1 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name "DBL1_QD_F" -length 0.18 -strength [expr $e0*0.468000000000161] 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL1_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl1 -phase $cav_ph_dbl1 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name "DBL1_QD_D" -length 0.18 -strength [expr $e0*-0.468000000000085] 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL1_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl1 -phase $cav_ph_dbl1 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name "DBL1_QD_F" -length 0.18 -strength [expr $e0*0.468000000000012] 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL1_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl1 -phase $cav_ph_dbl1 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name "DBL1_QD_D" -length 0.18 -strength [expr $e0*-0.468000000002049] 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL1_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl1 -phase $cav_ph_dbl1 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name "DBL1_QD_F" -length 0.18 -strength [expr $e0*0.468000000000566] 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL1_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl1 -phase $cav_ph_dbl1 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name "DBL1_QD_D" -length 0.18 -strength [expr $e0*-0.467999999999137] 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL1_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl1 -phase $cav_ph_dbl1 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name "DBL1_QD_F" -length 0.18 -strength [expr $e0*0.468000000002184] 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL1_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl1 -phase $cav_ph_dbl1 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name "DBL1_QD_D" -length 0.18 -strength [expr $e0*-0.468000000000776] 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL1_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl1 -phase $cav_ph_dbl1 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name "DBL1_QD_F" -length 0.18 -strength [expr $e0*0.467999999999416] 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL1_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl1 -phase $cav_ph_dbl1 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name "DBL1_QD_D" -length 0.18 -strength [expr $e0*-0.467999999998101] 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL1_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl1 -phase $cav_ph_dbl1 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name "DBL1_QD_F" -length 0.18 -strength [expr $e0*0.468000000002003] 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL1_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl1 -phase $cav_ph_dbl1 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name "DBL1_QD_D" -length 0.18 -strength [expr $e0*-0.468000000000689] 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL1_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl1 -phase $cav_ph_dbl1 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name "DBL1_QD_F" -length 0.18 -strength [expr $e0*0.467999999999417] 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL1_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl1 -phase $cav_ph_dbl1 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name "DBL1_QD_D" -length 0.18 -strength [expr $e0*-0.467999999998185] 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL1_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl1 -phase $cav_ph_dbl1 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name "DBL1_QD_F" -length 0.18 -strength [expr $e0*0.46800000000087] 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL1_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl1 -phase $cav_ph_dbl1 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name "DBL1_QD_D" -length 0.18 -strength [expr $e0*-0.467999999999653] 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL1_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl1 -phase $cav_ph_dbl1 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name "DBL1_QD_F" -length 0.18 -strength [expr $e0*0.468000000001292] 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL1_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl1 -phase $cav_ph_dbl1 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name "DBL1_QD_D" -length 0.18 -strength [expr $e0*-0.468000000000105] 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL1_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl1 -phase $cav_ph_dbl1 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name "DBL1_QD_F" -length 0.18 -strength [expr $e0*0.467999999998952] 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL1_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl1 -phase $cav_ph_dbl1 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name "DBL1_QD_D" -length 0.18 -strength [expr $e0*-0.468000000001434] 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL1_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl1 -phase $cav_ph_dbl1 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name "DBL1_QD_F" -length 0.18 -strength [expr $e0*0.468000000000296] 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL1_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl1 -phase $cav_ph_dbl1 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name "DBL1_QD_D" -length 0.18 -strength [expr $e0*-0.467999999999188] 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL1_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl1 -phase $cav_ph_dbl1 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name "DBL1_QD_F" -length 0.18 -strength [expr $e0*0.468000000001564] 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL1_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl1 -phase $cav_ph_dbl1 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name "DBL1_QD_D" -length 0.18 -strength [expr $e0*-0.468000000000471] 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL1_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl1 -phase $cav_ph_dbl1 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL1_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL1_DR_10" -length 0.1 
Bpm        -name      "DBL1_BPM_0" -length 0 
Drift      -name      "DBL1_DR_15" -length 0.15 
Quadrupole -name "DBL1_QD_F" -length 0.18 -strength [expr $e0*0.467999999999811] 
Drift      -name      "DBL1_DR_15" -length 0.15 
Dipole     -name      "DBL1_COR_0" -length 0 
Drift      -name      "DBL1_DR_10" -length 0.1 
Drift      -name  "DBL1_DRF_MOD_H" -length 1.4 
puts "DBL1 final energy = $e0"
#TclCall    -script    {BeamDump -file WDBL1.dat} 
Drift      -name       "BC1_DR_V1" -length 0.0120354686708 
Quadrupole -name "BC1_QD_V1" -length 0.18 -strength [expr $e0*-0.718309348154473] 
Drift      -name       "BC1_DR_V2" -length 3.45493793856 
Quadrupole -name "BC1_QD_V2" -length 0.18 -strength [expr $e0*0.782304196908963] 
Drift      -name       "BC1_DR_V3" -length 0.422271451131 
Quadrupole -name "BC1_QD_V3" -length 0.18 -strength [expr $e0*-0.764946439718044] 
Drift      -name       "BC1_DR_V4" -length 0.0144012843373 
Drift      -name       "BC1_DR_25" -length 0.25 
Sbend      -name      "BC1_SBND_1" -length 0.50097713  -angle [expr $bend_angle]  -E1 0 -E2 [expr $bend_angle] -e0 $e0 -six_dim 1 
Drift      -name       "BC1_DR_15" -length 0.15 
Dipole     -name       "BC1_COR_0" -length 0 
Drift      -name       "BC1_DR_10" -length 0.1 
Drift      -name      "BC1_DR_SID" -length 3.02059207 
Drift      -name       "BC1_DR_10" -length 0.1 
Bpm        -name       "BC1_BPM_0" -length 0 
Drift      -name       "BC1_DR_15" -length 0.15 
Sbend      -name      "BC1_SBND_2" -length 0.50097713  -angle [expr -$bend_angle]  -E1 [expr -$bend_angle] -E2 0 -e0 $e0 -six_dim 1 
Drift      -name       "BC1_DR_15" -length 0.15 
Dipole     -name       "BC1_COR_0" -length 0 
Drift      -name       "BC1_DR_10" -length 0.1 
Drift      -name      "BC1_DR_CNT" -length 0.5 
Drift      -name       "BC1_DR_10" -length 0.1 
Bpm        -name       "BC1_BPM_0" -length 0 
Drift      -name       "BC1_DR_15" -length 0.15 
Sbend      -name      "BC1_SBND_3" -length 0.50097713  -angle [expr -$bend_angle]  -E1 0 -E2 [expr -$bend_angle] -e0 $e0 -six_dim 1 
Drift      -name       "BC1_DR_15" -length 0.15 
Dipole     -name       "BC1_COR_0" -length 0 
Drift      -name       "BC1_DR_10" -length 0.1 
Drift      -name      "BC1_DR_SID" -length 3.02059207 
Drift      -name       "BC1_DR_10" -length 0.1 
Bpm        -name       "BC1_BPM_0" -length 0 
Drift      -name       "BC1_DR_15" -length 0.15 
Sbend      -name      "BC1_SBND_4" -length 0.50097713  -angle [expr $bend_angle]  -E1 [expr $bend_angle] -E2 0 -e0 $e0 -six_dim 1 
Drift      -name       "BC1_DR_25" -length 0.25 
#TclCall    -script    {BeamDump -file WBC1.dat} 
Drift      -name      "DBL2_DR_V1" -length 0.000427638825863 
Quadrupole -name "DBL2_QD_V1" -length 0.25 -strength [expr $e0*-5.02255089626576e-08] 
Drift      -name      "DBL2_DR_V2" -length 9.0620968745e-08 
Quadrupole -name "DBL2_QD_V2" -length 0.25 -strength [expr $e0*0.313458284563145] 
Drift      -name      "DBL2_DR_V3" -length 2.66224567021 
Quadrupole -name "DBL2_QD_V3" -length 0.25 -strength [expr $e0*-0.256774355479148] 
Drift      -name      "DBL2_DR_V4" -length 5.58785652111 
Drift      -name  "DBL2_DRF_MOD_H" -length 4.2 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_F" -length 0.25 -strength [expr $e0*0.17000000000004] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_D" -length 0.25 -strength [expr $e0*-0.170000000000141] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_F" -length 0.25 -strength [expr $e0*0.169999999999878] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_D" -length 0.25 -strength [expr $e0*-0.169999999999924] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_F" -length 0.25 -strength [expr $e0*0.169999999999967] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_D" -length 0.25 -strength [expr $e0*-0.169999999999956] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_F" -length 0.25 -strength [expr $e0*0.169999999999947] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_D" -length 0.25 -strength [expr $e0*-0.169999999999938] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_F" -length 0.25 -strength [expr $e0*0.169999999999885] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_D" -length 0.25 -strength [expr $e0*-0.170000000000113] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_F" -length 0.25 -strength [expr $e0*0.170000000000058] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_D" -length 0.25 -strength [expr $e0*-0.170000000000006] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_F" -length 0.25 -strength [expr $e0*0.169999999999957] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_D" -length 0.25 -strength [expr $e0*-0.169999999999911] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_F" -length 0.25 -strength [expr $e0*0.17000000000007] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_D" -length 0.25 -strength [expr $e0*-0.170000000000023] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_F" -length 0.25 -strength [expr $e0*0.169999999999929] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_D" -length 0.25 -strength [expr $e0*-0.170000000000041] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_F" -length 0.25 -strength [expr $e0*0.169999999999964] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_D" -length 0.25 -strength [expr $e0*-0.17000000000007] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_F" -length 0.25 -strength [expr $e0*0.169999999999996] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_D" -length 0.25 -strength [expr $e0*-0.169999999999418] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_F" -length 0.25 -strength [expr $e0*0.170000000000357] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_D" -length 0.25 -strength [expr $e0*-0.169999999999633] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_F" -length 0.25 -strength [expr $e0*0.170000000000528] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_D" -length 0.25 -strength [expr $e0*-0.169999999999831] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_F" -length 0.25 -strength [expr $e0*0.170000000000684] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_D" -length 0.25 -strength [expr $e0*-0.170000000000013] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_F" -length 0.25 -strength [expr $e0*0.169999999999368] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_D" -length 0.25 -strength [expr $e0*-0.170000000000181] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_F" -length 0.25 -strength [expr $e0*0.169999999999557] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_D" -length 0.25 -strength [expr $e0*-0.170000000000336] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_F" -length 0.25 -strength [expr $e0*0.169999999999732] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_D" -length 0.25 -strength [expr $e0*-0.17000000000048] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_F" -length 0.25 -strength [expr $e0*0.169999999999895] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_D" -length 0.25 -strength [expr $e0*-0.170000000000614] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_F" -length 0.25 -strength [expr $e0*0.170000000000047] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_D" -length 0.25 -strength [expr $e0*-0.169999999999499] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_F" -length 0.25 -strength [expr $e0*0.170000000000188] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_D" -length 0.25 -strength [expr $e0*-0.169999999999656] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_F" -length 0.25 -strength [expr $e0*0.170000000000321] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_D" -length 0.25 -strength [expr $e0*-0.169999999999803] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_F" -length 0.25 -strength [expr $e0*0.170000000000445] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_D" -length 0.25 -strength [expr $e0*-0.169999999999941] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_F" -length 0.25 -strength [expr $e0*0.169999999999452] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_D" -length 0.25 -strength [expr $e0*-0.170000000000071] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_F" -length 0.25 -strength [expr $e0*0.169999999999595] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_D" -length 0.25 -strength [expr $e0*-0.170000000000194] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_F" -length 0.25 -strength [expr $e0*0.169999999999729] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_D" -length 0.25 -strength [expr $e0*-0.17000000000031] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_F" -length 0.25 -strength [expr $e0*0.169999999999856] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_D" -length 0.25 -strength [expr $e0*-0.170000000000419] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_F" -length 0.25 -strength [expr $e0*0.169999999999976] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_D" -length 0.25 -strength [expr $e0*-0.169999999999545] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_F" -length 0.25 -strength [expr $e0*0.17000000000009] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_D" -length 0.25 -strength [expr $e0*-0.169999999999669] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_F" -length 0.25 -strength [expr $e0*0.170000000000198] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_D" -length 0.25 -strength [expr $e0*-0.169999999999786] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_F" -length 0.25 -strength [expr $e0*0.170000000000301] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_D" -length 0.25 -strength [expr $e0*-0.169999999999898] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_F" -length 0.25 -strength [expr $e0*0.170000000000399] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_D" -length 0.25 -strength [expr $e0*-0.170000000000004] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_F" -length 0.25 -strength [expr $e0*0.16999999999978] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_D" -length 0.25 -strength [expr $e0*-0.170000000000265] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_F" -length 0.25 -strength [expr $e0*0.169999999999886] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_D" -length 0.25 -strength [expr $e0*-0.170000000000358] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_F" -length 0.25 -strength [expr $e0*0.169999999999986] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_D" -length 0.25 -strength [expr $e0*-0.169999999999622] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_F" -length 0.25 -strength [expr $e0*0.170000000000082] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_D" -length 0.25 -strength [expr $e0*-0.169999999999725] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_F" -length 0.25 -strength [expr $e0*0.170000000000174] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_D" -length 0.25 -strength [expr $e0*-0.169999999999824] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_F" -length 0.25 -strength [expr $e0*0.170000000000262] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_D" -length 0.25 -strength [expr $e0*-0.169999999999918] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_F" -length 0.25 -strength [expr $e0*0.170000000000347] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_D" -length 0.25 -strength [expr $e0*-0.170000000000009] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_F" -length 0.25 -strength [expr $e0*0.169999999999678] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_D" -length 0.25 -strength [expr $e0*-0.170000000000096] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_F" -length 0.25 -strength [expr $e0*0.169999999999771] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_D" -length 0.25 -strength [expr $e0*-0.170000000000179] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_F" -length 0.25 -strength [expr $e0*0.16999999999986] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_D" -length 0.25 -strength [expr $e0*-0.17000000000026] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_F" -length 0.25 -strength [expr $e0*0.169999999999945] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_D" -length 0.25 -strength [expr $e0*-0.170000000000337] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_F" -length 0.25 -strength [expr $e0*0.170000000000028] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_D" -length 0.25 -strength [expr $e0*-0.169999999999724] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_F" -length 0.25 -strength [expr $e0*0.170000000000107] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_D" -length 0.25 -strength [expr $e0*-0.169999999999808] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_F" -length 0.25 -strength [expr $e0*0.170000000000184] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_D" -length 0.25 -strength [expr $e0*-0.16999999999989] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_F" -length 0.25 -strength [expr $e0*0.170000000000257] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_D" -length 0.25 -strength [expr $e0*-0.169999999999968] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_F" -length 0.25 -strength [expr $e0*0.169999999999684] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_D" -length 0.25 -strength [expr $e0*-0.170000000000044] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_F" -length 0.25 -strength [expr $e0*0.169999999999764] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_D" -length 0.25 -strength [expr $e0*-0.170000000000117] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_F" -length 0.25 -strength [expr $e0*0.169999999999841] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_D" -length 0.25 -strength [expr $e0*-0.170000000000187] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_F" -length 0.25 -strength [expr $e0*0.169999999999915] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_D" -length 0.25 -strength [expr $e0*-0.170000000000256] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_F" -length 0.25 -strength [expr $e0*0.169999999999987] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_D" -length 0.25 -strength [expr $e0*-0.169999999999834] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_F" -length 0.25 -strength [expr $e0*0.170000000000167] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_D" -length 0.25 -strength [expr $e0*-0.169999999999906] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_F" -length 0.25 -strength [expr $e0*0.170000000000233] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_D" -length 0.25 -strength [expr $e0*-0.169999999999976] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_F" -length 0.25 -strength [expr $e0*0.169999999999722] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 



Quadrupole -name "DBL2_QD_D" -length 0.25 -strength [expr $e0*-0.170000000000043] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_F" -length 0.25 -strength [expr $e0*0.169999999999793] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 


Quadrupole -name "DBL2_QD_D" -length 0.25 -strength [expr $e0*-0.170000000000108] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_F" -length 0.25 -strength [expr $e0*0.169999999999861] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_D" -length 0.25 -strength [expr $e0*-0.170000000000172] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_F" -length 0.25 -strength [expr $e0*0.169999999999928] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_D" -length 0.25 -strength [expr $e0*-0.170000000000233] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_F" -length 0.25 -strength [expr $e0*0.169999999999993] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_D" -length 0.25 -strength [expr $e0*-0.169999999999756] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_F" -length 0.25 -strength [expr $e0*0.170000000000055] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_D" -length 0.25 -strength [expr $e0*-0.169999999999821] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_F" -length 0.25 -strength [expr $e0*0.170000000000116] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_D" -length 0.25 -strength [expr $e0*-0.169999999999885] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_F" -length 0.25 -strength [expr $e0*0.170000000000175] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_D" -length 0.25 -strength [expr $e0*-0.169999999999947] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_F" -length 0.25 -strength [expr $e0*0.170000000000233] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_D" -length 0.25 -strength [expr $e0*-0.170000000000007] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 

Quadrupole -name "DBL2_QD_F" -length 0.25 -strength [expr $e0*0.169999999999785] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_D" -length 0.25 -strength [expr $e0*-0.170000000000159] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_F" -length 0.25 -strength [expr $e0*0.169999999999785] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_D" -length 0.25 -strength [expr $e0*-0.170000000000159] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_F" -length 0.25 -strength [expr $e0*0.169999999999785] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_D" -length 0.25 -strength [expr $e0*-0.170000000000159] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_F" -length 0.25 -strength [expr $e0*0.169999999999785] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_D" -length 0.25 -strength [expr $e0*-0.170000000000159] 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Cavity -name "DBL2_ACC_0" -length 2.3994984 -gradient $cav_gradient_dbl2 -phase $cav_ph_dbl2 
set e0 [expr $e0 + 4.047622286 / 1e3]
SetReferenceEnergy $e0
Drift      -name  "DBL2_DR_ACC_ED" -length 0.2002508 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
Quadrupole -name "DBL2_QD_F_H" -length 0.25 -strength [expr $e0*0.169999999999997] 
#TclCall    -script    {BeamDump -file WDBL2.dat} 
Drift      -name      "DBL2_DR_15" -length 0.15 
Dipole     -name      "DBL2_COR_0" -length 0 
Drift      -name      "DBL2_DR_10" -length 0.1 
Drift      -name  "DBL2_DRF_MOD_H" -length 3.95 
Drift      -name      "DBL2_DR_10" -length 0.1 
Bpm        -name      "DBL2_BPM_0" -length 0 
Drift      -name      "DBL2_DR_15" -length 0.15 
puts "DBL2 final energy = $e0"
