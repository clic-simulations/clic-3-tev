# source tools.tcl
# routine to create beam


 
proc create_distribution_particle {outfile bparray} {

    upvar $bparray beamparams
    
    set bx $beamparams(beta_x)
    set ax $beamparams(alpha_x)
    set ex $beamparams(emitt_x)
    set by $beamparams(beta_y)
    set ay $beamparams(alpha_y)
    set ey $beamparams(emitt_y)
    set sz $beamparams(sigma_z)
    set e0 $beamparams(energy)
    set de $beamparams(e_spread)
    set nslice $beamparams(n_slice)
    set nmacro $beamparams(n_macro)
    set npart [expr $beamparams(n_slice)*$beamparams(n_macro)]
    if {[info exist beamparams(e_offset)]<1} { set eoff 0. } else { set eoff $beamparams(e_offset) }
    if {[info exist beamparams(x_offset)]<1} { set xoff 0. } else { set xoff $beamparams(x_offset) }
    if {[info exist beamparams(y_offset)]<1} { set yoff 0. } else { set yoff $beamparams(y_offset) }
    if {[info exist beamparams(z_offset)]<1} { set zoff 0. } else { set zoff $beamparams(z_offset) }
    if {[info exist beamparams(xp_offset)]<1} { set xpoff 0. } else { set xpoff $beamparams(xp_offset) }
    if {[info exist beamparams(yp_offset)]<1} { set ypoff 0. } else { set ypoff $beamparams(yp_offset) }
    
    if {[info exist beamparams(type)]<1} {
	puts "you have not given type of longitudinal charge distibution \n gaussian type of distibution for 6-d is taken account"
	set ttype gaussian
       } else {
	set ttype $beamparams(type)
       }
	
    Octave {
	zlist=generate_zlist('$ttype',$sz,$nslice);
# 	beam=beam_zlist(zlist,$npart,$e0,$de,$ex,$bx,$ax,$ey,$by,$ay,$sz);
	beam=beam_zlist_no_offset(zlist,$npart,$e0,$de,$ex,$bx,$ax,$ey,$by,$ay,$sz);
	beam=add_offset(beam,$eoff,$xoff,$yoff,$zoff,$xpoff,$ypoff);
	save -text -ascii $outfile beam
    } 
    
}



proc create_distribution_slice {outfile bparray} {

    upvar $bparray beamparams
    
    set bx $beamparams(beta_x)
    set ax $beamparams(alpha_x)
    set ex $beamparams(emitt_x)
    set by $beamparams(beta_y)
    set ay $beamparams(alpha_y)
    set ey $beamparams(emitt_y)
    set sz $beamparams(sigma_z)
    set e0 $beamparams(energy)
    set de $beamparams(e_spread)
    set nslice $beamparams(n_slice)
    set nmacro $beamparams(n_macro)
    set npart [expr $beamparams(n_slice)*$beamparams(n_macro)]
    if {[info exist beamparams(e_offset)]<1} { set eoff 0. } else { set eoff $beamparams(e_offset) }
    if {[info exist beamparams(x_offset)]<1} { set xoff 0. } else { set xoff $beamparams(x_offset) }
    if {[info exist beamparams(y_offset)]<1} { set yoff 0. } else { set yoff $beamparams(y_offset) }
    if {[info exist beamparams(z_offset)]<1} { set zoff 0. } else { set zoff $beamparams(z_offset) }
    if {[info exist beamparams(xp_offset)]<1} { set xpoff 0. } else { set xpoff $beamparams(xp_offset) }
    if {[info exist beamparams(yp_offset)]<1} { set ypoff 0. } else { set ypoff $beamparams(yp_offset) }
    
    if {[info exist beamparams(type)]<1} {
	puts "you have not given type of longitudinal charge distibution \n gaussian type of distibution for 6-d is taken account"
	set ttype gaussian
       } else {
	set ttype $beamparams(type)
       }
	
    Octave {
	zlist=generate_zlist('$ttype',$sz,$nslice);
# 	beam=beam_zlist(zlist,$npart,$e0,$de,$ex,$bx,$ax,$ey,$by,$ay,$sz);
	beam=beam_zlist_slice_no_offset(zlist,$npart,$e0,$de,$ex,$bx,$ax,$ey,$by,$ay,$sz);
	beam=add_offset(beam,$eoff,$xoff,$yoff,$zoff,$xpoff,$ypoff);
	save -text -ascii $outfile beam
    } 
    
}




proc set_to_slice {outfile bparray indist} {

    upvar $bparray beamparams
    
    set bx $beamparams(beta_x)
    set ax $beamparams(alpha_x)
    set ex $beamparams(emitt_x)
    set by $beamparams(beta_y)
    set ay $beamparams(alpha_y)
    set ey $beamparams(emitt_y)
    set sz $beamparams(sigma_z)
    set e0 $beamparams(energy)
    set de $beamparams(e_spread)
    set nslice $beamparams(n_slice)
    set nmacro $beamparams(n_macro)
    set npart [expr $beamparams(n_slice)*$beamparams(n_macro)]
    if {[info exist beamparams(e_offset)]<1} { set eoff 0. } else { set eoff $beamparams(e_offset) }
    if {[info exist beamparams(x_offset)]<1} { set xoff 0. } else { set xoff $beamparams(x_offset) }
    if {[info exist beamparams(y_offset)]<1} { set yoff 0. } else { set yoff $beamparams(y_offset) }
    if {[info exist beamparams(z_offset)]<1} { set zoff 0. } else { set zoff $beamparams(z_offset) }
    if {[info exist beamparams(xp_offset)]<1} { set xpoff 0. } else { set xpoff $beamparams(xp_offset) }
    if {[info exist beamparams(yp_offset)]<1} { set ypoff 0. } else { set ypoff $beamparams(yp_offset) }
    
#     if {[info exist beamparams(type)]<1} {
# 	puts "you have not given type of longitudinal charge distibution \n gaussian type of distibution for 6-d is taken account"
# 	set ttype gaussian
#        } else {
# 	set ttype $beamparams(type)
#        }
	
    Octave {

# 	beam=beam_zlist(zlist,$npart,$e0,$de,$ex,$bx,$ax,$ey,$by,$ay,$sz);
	beam=load('$indist');
    beam(:,1)=$e0;
    beam(:,2)=0.0;
    beam(:,3)=0.0;
    beam(:,5)=0.0;
    beam(:,6)=0.0;
	beam=add_offset(beam,$eoff,$xoff,$yoff,$zoff,$xpoff,$ypoff);
	save -text -ascii $outfile beam
    } 
    
}


# creates 6-d distribution with given longitudinal distribution type of
# gaussian plateau parabola uniform
# if type is not defined it uses gaussian type of longitudinal distibution
proc make_particle_beam {arrayname} {
    global lambda 
    
    upvar $arrayname match
    set ch {1.0}
    set filename  $match(filename)
    #particles are generated here 
    create_distribution_particle $filename match
    
    Octave {
	z_listh=beam_histogram('$filename',$match(n_slice)) ;
	calc_beamdat('beam.dat',$match(charge),z_listh,acc_cav);
    }

    InjectorBeam $match(name) -bunches 1 \
		-macroparticles $match(n_macro) \
		-slices $match(n_slice) \
		-particles [expr $match(n_slice)*$match(n_macro)] \
		-energyspread [expr 0.01*$match(e_spread)*$match(energy)] \
		-e0 $match(energy) \
		-file beam.dat \
		-chargelist $ch \
		-charge $match(charge) \
		-phase 0.0 \
		-last_wgt 1.0 \
		-alpha_y $match(alpha_y) \
		-beta_y $match(beta_y) \
		-emitt_y $match(emitt_y) \
		-alpha_x $match(alpha_x) \
		-beta_x $match(beta_x) \
		-emitt_x $match(emitt_x)


    if {[array names match -exact gradient] == {} } {
	set beam_grad  1.0 
    } else {
	set beam_grad $match(gradient)
    }
    set l {}
    lappend l "$beam_grad 0.0 0.0"
    SetRfGradientSingle $match(name) 0 $l

    BeamRead -file $filename -beam $match(name)

}




# creates 6-d distribution with given longitudinal distribution type of
# gaussian plateau parabola uniform
# if type is not defined it uses gaussian type of longitudinal distibution
proc make_slice_beam {arrayname} {
    global lambda 
    
    upvar $arrayname match
    set ch {1.0}
    set filename  $match(filename)
    #particles are generated here 
    create_distribution_slice $filename match
    
    Octave {
	z_listh=beam_histogram('$filename',$match(n_slice)) ;
	calc_beamdat('beam.dat',$match(charge),z_listh,acc_cav);
    }

    InjectorBeam $match(name) -bunches 1 \
		-macroparticles $match(n_macro) \
		-slices $match(n_slice) \
		-particles [expr $match(n_slice)*$match(n_macro)] \
		-energyspread [expr 0.01*$match(e_spread)*$match(energy)] \
		-e0 $match(energy) \
		-file beam.dat \
		-chargelist $ch \
		-charge $match(charge) \
		-phase 0.0 \
		-last_wgt 1.0 \
		-alpha_y $match(alpha_y) \
		-beta_y $match(beta_y) \
		-emitt_y $match(emitt_y) \
		-alpha_x $match(alpha_x) \
		-beta_x $match(beta_x) \
		-emitt_x $match(emitt_x)


    if {[array names match -exact gradient] == {} } {
	set beam_grad  1.0 
    } else {
	set beam_grad $match(gradient)
    }
    set l {}
    lappend l "$beam_grad 0.0 0.0"
    SetRfGradientSingle $match(name) 0 $l

    BeamRead -file $filename -beam $match(name)

}




# creates 6-d distribution with given longitudinal distribution type of
# gaussian plateau parabola uniform
# if type is not defined it uses gaussian type of longitudinal distibution
proc make_beam_read_set_slice {arrayname indist} {
    global lambda 
    
    upvar $arrayname match
    set ch {1.0}
    set filename  $match(filename)
    #particles are generated here 
#     create_distribution_slice $filename match
    set_to_slice $filename match $indist
    
    Octave {
	z_listh=beam_histogram('$filename',$match(n_slice)) ;
	calc_beamdat('beam.dat',$match(charge),z_listh,acc_cav);
    }

    InjectorBeam $match(name) -bunches 1 \
		-macroparticles $match(n_macro) \
		-slices $match(n_slice) \
		-particles [expr $match(n_slice)*$match(n_macro)] \
		-energyspread [expr 0.01*$match(e_spread)*$match(energy)] \
		-e0 $match(energy) \
		-file beam.dat \
		-chargelist $ch \
		-charge $match(charge) \
		-phase 0.0 \
		-last_wgt 1.0 \
		-alpha_y $match(alpha_y) \
		-beta_y $match(beta_y) \
		-emitt_y $match(emitt_y) \
		-alpha_x $match(alpha_x) \
		-beta_x $match(beta_x) \
		-emitt_x $match(emitt_x)


    if {[array names match -exact gradient] == {} } {
	set beam_grad  1.0 
    } else {
	set beam_grad $match(gradient)
    }
    set l {}
    lappend l "$beam_grad 0.0 0.0"
    SetRfGradientSingle $match(name) 0 $l

    BeamRead -file $filename -beam $match(name)

}






# reads given distribution and computes charge longitudinal charge distibution
proc make_particle_beam_read {arrayname indist} {
    global lambda
    set filename  $indist
    
    upvar $arrayname match
    set ch {1.0}
    set match(filename) $filename
    
    Octave {
	z_listh=beam_histogram('$filename',$match(n_slice)) ;
	calc_beamdat('beam.dat',$match(charge),z_listh,acc_cav);
    }
      
    
    InjectorBeam $match(name) -bunches 1 \
	    -macroparticles $match(n_macro) \
	    -slices $match(n_slice) \
	    -particles [expr $match(n_slice)*$match(n_macro)] \
	    -energyspread [expr 0.01*$match(e_spread)*$match(energy)] \
	    -ecut 3.0 \
	    -e0 $match(energy) \
	    -file beam.dat \
	    -chargelist $ch \
	    -charge $match(charge) \
	    -phase 0.0 \
	    -last_wgt 1.0 \
	    -alpha_y $match(alpha_y) \
	    -beta_y $match(beta_y) \
	    -emitt_y $match(emitt_y) \
	    -alpha_x $match(alpha_x) \
	    -beta_x $match(beta_x) \
	    -emitt_x $match(emitt_x)


    if {[array names match -exact gradient] == {} } {
	    set beam_grad  1.0 
    } else {
	    set beam_grad $match(gradient)
    }
    set l {}
    lappend l "$beam_grad 0.0 0.0"
    SetRfGradientSingle $match(name) 0 $l

    BeamRead -file $filename -beam $match(name)

}




