## Reference energy taken from list_energies.tcl (Computed from Eduardo Martin's Lattice) 
## This was chosen to match the less energetic "bunch type" (there are 24 different possible bunch energies). 
## Which means there will be a slight magnet missfocus for the other bunches. 
set Pi [expr acos(-1)]
if {$Dipole_synrad} { 
	set refen 2.3774105820869833 
} else { 
	set refen 2.38 
} 
Marker "CR2_P2.INJECTION" 
Drift "CR2.SEPTUM" -len 1 
Drift "CR2.DRFB6" -len 2.367622162 
Drift "CR2.DSRFB6" -len 0.4 
Drift "CR2.DRFB4A" -len 0.709135479 
Drift "CR2.D1M" -len 1 
Drift "CR2.D1M" -len 1 
Drift "CR2.D1M" -len 1 
Drift -name "CR2.D1CM" -len 0.01 
Quadrupole "QUAD.CR2.QRFB3.EXT" -len 0.5 -S1 [expr -0.5374235945859976/2.38*$refen] -refen $refen 
Bpm "BPM.CR2_2.35" -len 0 
Drift "CR2.DRFB3" -len 3.771233433 
Quadrupole "QUAD.CR2.QRFB2.EXT" -len 0.5 -S1 [expr 1.330892839528811/2.38*$refen] -refen $refen 
Bpm "BPM.CR2_2.36" -len 0 
Drift "CR2.DRFB2" -len 1.193240969 
Quadrupole "QUAD.CR2.QRFB1.EXT" -len 0.5 -S1 [expr -2.569734604068645/2.38*$refen] -refen $refen 
Bpm "BPM.CR2_2.37" -len 0 
Drift "CR2.DRFB1" -len 0.217192186 
Drift "CR2.DRRF" -len 2 
Marker "CR2.MENTEXTKCK" 
Drift "CR2.DL3IN" -len 0.4000389758 
Quadrupole "QUAD.CR2.QL3IN.EXT" -len 0.5 -S1 [expr -0.5949757844013318/2.38*$refen] -refen $refen 
Bpm "BPM.CR2_2.38" -len 0 
Drift "CR2.DL2IN" -len 1.103409345 
Quadrupole "QUAD.CR2.QL2IN.EXT" -len 0.5 -S1 [expr 2.029349968933815/2.38*$refen] -refen $refen 
Bpm "BPM.CR2_2.39" -len 0 
Drift "CR2.DL1IN" -len 0.4000469975 
Quadrupole "QUAD.CR2.QL1IN.EXT" -len 0.5 -S1 [expr -1.659043812038211/2.38*$refen] -refen $refen 
Bpm "BPM.CR2_2.40" -len 0 
Drift "CR2.DL0IN" -len 2.246504682 
#### Missing length ####
Drift "CR2.MISSLENGTH" -len 0.202692445399988 
########################
Marker "CR2MDACELLSTART" 
Quadrupole "QUAD.CR2.DBA.QI1" -len 0.4 -S1 [expr -0.590316*$refen] -refen $refen 
Drift "CR2.DBA.D11A" -len 0.1730305995 
Sextupole "SEXT.CR2.DBA.SNCH1" -len 0.1 -S2 [expr -4.466459229927851/2.38*$refen] -refen $refen 
Drift "CR2.DBA.D11B" -len 0.1628018956 
Quadrupole "QUAD.CR2.DBA.QI2" -len 0.4 -S1 [expr 0.821588*$refen] -refen $refen 
Drift "CR2.DBA.D12" -len 0.2697746031 
#######################
Sbend "BEND.CR2.DBA.DIPOLE0" -len 2.182098945 -angle -0.2108714854 -E1 -0.1054357427 -E2 -0.1054357427 -S1 [expr 0.0399258644*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*-0.2108714854*-0.2108714854/2.182098945*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "CR2.DBA.DS1C_A" -len 0.6435098443 
Sextupole "SEXT.CR2.DBA.SN1.5" -len 0.1 -S2 [expr 5.20236559863474] -refen $refen 
Drift "CR2.DBA.DS1C_B" -len 0.109313245 
Marker "CR2.DBA.HBM" 
Quadrupole "QUAD.CR2.DBA.QS1C" -len 0.4 -S1 [expr -1.114316*$refen] -refen $refen 
Drift "CR2.DBA.DS2C_A" -len 0.4926115415 
Sextupole "SEXT.CR2.DBA.SF0.5" -len 0.1 -S2 [expr 0.4180579844128172] -refen $refen 
Drift "CR2.DBA.DS2C_B" -len 0.01730305995 
Sbend "BEND.CR2.DBA.DIPN0" -len 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -S1 [expr 0.7724967744*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "CR2.DBA.DS3C_A" -len 0.01730305995 
Sextupole "SEXT.CR2.DBA.SN3.5" -len 0.1 -S2 [expr -4.881101079161915] -refen $refen 
Drift "CR2.DBA.DS3C_B" -len 0.2945377043 
Quadrupole "QUAD.CR2.DBA.QS2C" -len 0.2 -S1 [expr -0.1188136*$refen] -refen $refen 
Quadrupole "QUAD.CR2.DBA.QS2C" -len 0.2 -S1 [expr -0.1188136*$refen] -refen $refen 
Drift "CR2.DBA.DS3C_B" -len 0.2945377043 
Sextupole "SEXT.CR2.DBA.SNN3.5" -len 0.1 -S2 [expr -0.8677264653553822] -refen $refen 
Drift "CR2.DBA.DS3C_A" -len 0.01730305995 
Sbend "BEND.CR2.DBA.DIPN0" -len 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -S1 [expr 0.7724967744*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "CR2.DBA.DS2C_B" -len 0.01730305995 
Sextupole "SEXT.CR2.DBA.SF0.5" -len 0.1 -S2 [expr 0.4180579844128172] -refen $refen 
Drift "CR2.DBA.DS2C_A" -len 0.4926115415 
Quadrupole "QUAD.CR2.DBA.QS1C" -len 0.4 -S1 [expr -1.114316*$refen] -refen $refen 
Drift "CR2.DBA.DS1C_B" -len 0.109313245 
Sextupole "SEXT.CR2.DBA.SNN1.5" -len 0.1 -S2 [expr -7.854858904416774] -refen $refen 
Drift "CR2.DBA.DS1C_A" -len 0.6435098443 
Sbend "BEND.CR2.DBA.DIPOLE0" -len 2.182098945 -angle -0.2108714854 -E1 -0.1054357427 -E2 -0.1054357427 -S1 [expr 0.0399258644*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*-0.2108714854*-0.2108714854/2.182098945*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "CR2.DBA.D12" -len 0.2697746031 
Quadrupole "QUAD.CR2.DBA.QI2" -len 0.4 -S1 [expr 0.821588*$refen] -refen $refen 
Drift "CR2.DBA.D11B1" -len 0.3424874764 
Sextupole "SEXT.CR2.DBA.SD0" -len 0.1 -S2 [expr 0.1914983233102843/2.38*$refen] -refen $refen 
Drift "CR2.DBA.DM0" -len 0.109313245 
Quadrupole "QUAD.CR2.DBA.QF1" -len 0.4 -S1 [expr -2.611445582383727/2.38*$refen] -refen $refen 
Drift "CR2.DBA.DM1" -len 1.96118337075000 
Quadrupole "QUAD.CR2.DBA.QF2" -len 0.4 -S1 [expr 0.6679291738134705/2.38*$refen] -refen $refen 
Drift "CR2.DBA.DM1" -len 1.96118337075000 
Quadrupole "QUAD.CR2.DBA.QF3" -len 0.4 -S1 [expr -2.611445582383727/2.38*$refen] -refen $refen 
Drift "CR2.DBA.DM0" -len 0.109313245 
Sextupole "SEXT.CR2.DBA.SD0" -len 0.1 -S2 [expr 0.1914983233102843/2.38*$refen] -refen $refen 
Drift "CR2.DBA.D11B1" -len 0.3424874764 
Quadrupole "QUAD.CR2.DBA.QI2" -len 0.4 -S1 [expr 0.821588*$refen] -refen $refen 
Drift "CR2.DBA.D12" -len 0.2697746031 
Sbend "BEND.CR2.DBA.DIPOLE0" -len 2.182098945 -angle -0.2108714854 -E1 -0.1054357427 -E2 -0.1054357427 -S1 [expr 0.0399258644*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*-0.2108714854*-0.2108714854/2.182098945*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "CR2.DBA.DS1C_A" -len 0.6435098443 
Sextupole "SEXT.CR2.DBA.SNN1.5" -len 0.1 -S2 [expr -7.854858904416774] -refen $refen 
Drift "CR2.DBA.DS1C_B" -len 0.109313245 
Quadrupole "QUAD.CR2.DBA.QS1C" -len 0.4 -S1 [expr -1.114316*$refen] -refen $refen 
Drift "CR2.DBA.DS2C_A" -len 0.4926115415 
Sextupole "SEXT.CR2.DBA.SF0.5" -len 0.1 -S2 [expr 0.4180579844128172] -refen $refen 
Drift "CR2.DBA.DS2C_B" -len 0.01730305995 
Sbend "BEND.CR2.DBA.DIPN0" -len 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -S1 [expr 0.7724967744*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "CR2.DBA.DS3C_A" -len 0.01730305995 
Sextupole "SEXT.CR2.DBA.SNN3.5" -len 0.1 -S2 [expr -0.8677264653553822] -refen $refen 
Drift "CR2.DBA.DS3C_B" -len 0.2945377043 
Quadrupole "QUAD.CR2.DBA.QS2C" -len 0.2 -S1 [expr -0.1188136*$refen] -refen $refen 
Quadrupole "QUAD.CR2.DBA.QS2C" -len 0.2 -S1 [expr -0.1188136*$refen] -refen $refen 
Drift "CR2.DBA.DS3C_B" -len 0.2945377043 
Sextupole "SEXT.CR2.DBA.SN3.5" -len 0.1 -S2 [expr -4.881101079161915] -refen $refen 
Drift "CR2.DBA.DS3C_A" -len 0.01730305995 
Sbend "BEND.CR2.DBA.DIPN0" -len 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -S1 [expr 0.7724967744*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "CR2.DBA.DS2C_B" -len 0.01730305995 
Sextupole "SEXT.CR2.DBA.SF0.5" -len 0.1 -S2 [expr 0.4180579844128172] -refen $refen 
Drift "CR2.DBA.DS2C_A" -len 0.4926115415 
Quadrupole "QUAD.CR2.DBA.QS1C" -len 0.4 -S1 [expr -1.114316*$refen] -refen $refen 
Drift "CR2.DBA.DS1C_B" -len 0.109313245 
Sextupole "SEXT.CR2.DBA.SN1.5" -len 0.1 -S2 [expr 5.20236559863474] -refen $refen 
Drift "CR2.DBA.DS1C_A" -len 0.6435098443 
Sbend "BEND.CR2.DBA.DIPOLE0" -len 2.182098945 -angle -0.2108714854 -E1 -0.1054357427 -E2 -0.1054357427 -S1 [expr 0.0399258644*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*-0.2108714854*-0.2108714854/2.182098945*$e0*$e0*$e0*$e0*$Dipole_synrad] 
########################################
Drift "CR2.DBA.D12" -len 0.2697746031 
Quadrupole "QUAD.CR2.DBA.QI2" -len 0.4 -S1 [expr 0.821588*$refen] -refen $refen 
Drift "CR2.DBA.D11B" -len 0.1628018956 
Sextupole "SEXT.CR2.DBA.SNCH1" -len 0.1 -S2 [expr -4.466459229927851/2.38*$refen] -refen $refen 
Drift "CR2.DBA.D11A" -len 0.1730305995 
Quadrupole "QUAD.CR2.DBA.QI1" -len 0.4 -S1 [expr -0.590316*$refen] -refen $refen 
Marker "CR2MDACELLEND" 
Drift "CR2.DTR1A" -len 1.1 
Sextupole "SEXT.CR2.SXTR1" -len 0.1 -S2 [expr 11.90518312758056/2.38*$refen] -refen $refen 
Drift "CR2.DTR1B" -len 0.2 
Quadrupole "QUAD.CR2.QTR1" -len 0.5 -S1 [expr -0.8932952208812484/2.38*$refen] -refen $refen 
Bpm "BPM.CR2_1.7" -len 0 
Drift "CR2.DTR2A" -len 1.1 
Sextupole "SEXT.CR2.SXTR2" -len 0.1 -S2 [expr -21.05518599722218/2.38*$refen] -refen $refen 
Drift "CR2.DTR2B" -len 0.2 
Quadrupole "QUAD.CR2.QTR2" -len 0.5 -S1 [expr 1.396779518046066/2.38*$refen] -refen $refen 
Bpm "BPM.CR2_1.8" -len 0 
Drift "CR2.DTR3A" -len 1.5 
Sextupole "SEXT.CR2.SXTR3" -len 0.1 -S2 [expr 37.8906248766911/2.38*$refen] -refen $refen 
Drift "CR2.DTR3B" -len 0.2 
Quadrupole "QUAD.CR2.QTR3" -len 0.5 -S1 [expr -0.6630399232271197/2.38*$refen] -refen $refen 
Bpm "BPM.CR2_1.9" -len 0 
Drift "CR2.DTR4A" -len 2.1328 
Sextupole "SEXT.CR2.SXTR4" -len 0.1 -S2 [expr -13.25727703236136/2.38*$refen] -refen $refen 
Drift "CR2.DTR4B" -len 0.2 
Quadrupole "QUAD.CR2.QTR4" -len 0.25 -S1 [expr 0.4450739458716257/2.38*$refen] -refen $refen 
Bpm "BPM.CR2_1.10" -len 0 
Quadrupole "QUAD.CR2.QTR4" -len 0.25 -S1 [expr 0.4450739458716257/2.38*$refen] -refen $refen 
Bpm "BPM.CR2_1.11" -len 0 
Drift "CR2.DTR4B" -len 0.2 
Sextupole "SEXT.CR2.SXTR4" -len 0.1 -S2 [expr -13.25727703236136/2.38*$refen] -refen $refen 
Drift "CR2.DTR4A" -len 2.1328 
Quadrupole "QUAD.CR2.QTR3" -len 0.5 -S1 [expr -0.6630399232271197/2.38*$refen] -refen $refen 
Bpm "BPM.CR2_1.12" -len 0 
Drift "CR2.DTR3B" -len 0.2 
Sextupole "SEXT.CR2.SXTR3" -len 0.1 -S2 [expr 37.8906248766911/2.38*$refen] -refen $refen 
Drift "CR2.DTR3A" -len 1.5 
Quadrupole "QUAD.CR2.QTR2" -len 0.5 -S1 [expr 1.396779518046066/2.38*$refen] -refen $refen 
Bpm "BPM.CR2_1.13" -len 0 
Drift "CR2.DTR2B" -len 0.2 
Sextupole "SEXT.CR2.SXTR2" -len 0.1 -S2 [expr -21.05518599722218/2.38*$refen] -refen $refen 
Drift "CR2.DTR2A" -len 1.1 
Quadrupole "QUAD.CR2.QTR1" -len 0.5 -S1 [expr -0.8932952208812484/2.38*$refen] -refen $refen 
Bpm "BPM.CR2_1.14" -len 0 
Drift "CR2.DTR1B" -len 0.2 
Sextupole "SEXT.CR2.SXTR1" -len 0.1 -S2 [expr 11.90518312758056/2.38*$refen] -refen $refen 
Drift "CR2.DTR1A" -len 1.1 
Marker "CR2MDACELLSTART" 
Quadrupole "QUAD.CR2.DBA.QI1" -len 0.4 -S1 [expr -0.590316*$refen] -refen $refen 
Drift "CR2.DBA.D11A" -len 0.1730305995 
Sextupole "SEXT.CR2.DBA.SNCH1" -len 0.1 -S2 [expr -4.466459229927851/2.38*$refen] -refen $refen 
Drift "CR2.DBA.D11B" -len 0.1628018956 
Quadrupole "QUAD.CR2.DBA.QI2" -len 0.4 -S1 [expr 0.821588*$refen] -refen $refen 
Drift "CR2.DBA.D12" -len 0.2697746031 
#######################
Sbend "BEND.CR2.DBA.DIPOLE0" -len 2.182098945 -angle -0.2108714854 -E1 -0.1054357427 -E2 -0.1054357427 -S1 [expr 0.0399258644*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*-0.2108714854*-0.2108714854/2.182098945*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "CR2.DBA.DS1C_A" -len 0.6435098443 
Sextupole "SEXT.CR2.DBA.SN1.6" -len 0.1 -S2 [expr 6.070287629089756] -refen $refen 
Drift "CR2.DBA.DS1C_B" -len 0.109313245 
Marker "CR2.DBA.HBM" 
Quadrupole "QUAD.CR2.DBA.QS1C" -len 0.4 -S1 [expr -1.114316*$refen] -refen $refen 
Drift "CR2.DBA.DS2C_A" -len 0.4926115415 
Sextupole "SEXT.CR2.DBA.SF0.6" -len 0.1 -S2 [expr -0.1257848660334976] -refen $refen 
Drift "CR2.DBA.DS2C_B" -len 0.01730305995 
Sbend "BEND.CR2.DBA.DIPN0" -len 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -S1 [expr 0.7724967744*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "CR2.DBA.DS3C_A" -len 0.01730305995 
Sextupole "SEXT.CR2.DBA.SN3.6" -len 0.1 -S2 [expr -4.808394576721998] -refen $refen 
Drift "CR2.DBA.DS3C_B" -len 0.2945377043 
Quadrupole "QUAD.CR2.DBA.QS2C" -len 0.2 -S1 [expr -0.1188136*$refen] -refen $refen 
Quadrupole "QUAD.CR2.DBA.QS2C" -len 0.2 -S1 [expr -0.1188136*$refen] -refen $refen 
Drift "CR2.DBA.DS3C_B" -len 0.2945377043 
Sextupole "SEXT.CR2.DBA.SNN3.6" -len 0.1 -S2 [expr -0.2978498685059033] -refen $refen 
Drift "CR2.DBA.DS3C_A" -len 0.01730305995 
Sbend "BEND.CR2.DBA.DIPN0" -len 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -S1 [expr 0.7724967744*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "CR2.DBA.DS2C_B" -len 0.01730305995 
Sextupole "SEXT.CR2.DBA.SF0.6" -len 0.1 -S2 [expr -0.1257848660334976] -refen $refen 
Drift "CR2.DBA.DS2C_A" -len 0.4926115415 
Quadrupole "QUAD.CR2.DBA.QS1C" -len 0.4 -S1 [expr -1.114316*$refen] -refen $refen 
Drift "CR2.DBA.DS1C_B" -len 0.109313245 
Sextupole "SEXT.CR2.DBA.SNN1.6" -len 0.1 -S2 [expr -4.053818418657726] -refen $refen 
Drift "CR2.DBA.DS1C_A" -len 0.6435098443 
Sbend "BEND.CR2.DBA.DIPOLE0" -len 2.182098945 -angle -0.2108714854 -E1 -0.1054357427 -E2 -0.1054357427 -S1 [expr 0.0399258644*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*-0.2108714854*-0.2108714854/2.182098945*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "CR2.DBA.D12" -len 0.2697746031 
Quadrupole "QUAD.CR2.DBA.QI2" -len 0.4 -S1 [expr 0.821588*$refen] -refen $refen 
Drift "CR2.DBA.D11B1" -len 0.3424874764 
Sextupole "SEXT.CR2.DBA.SD0" -len 0.1 -S2 [expr 0.1914983233102843/2.38*$refen] -refen $refen 
Drift "CR2.DBA.DM0" -len 0.109313245 
Quadrupole "QUAD.CR2.DBA.QF1" -len 0.4 -S1 [expr -2.611445582383727/2.38*$refen] -refen $refen 
Drift "CR2.DBA.DM1" -len 1.96118337075000 
Quadrupole "QUAD.CR2.DBA.QF2" -len 0.4 -S1 [expr 0.6679291738134705/2.38*$refen] -refen $refen 
Drift "CR2.DBA.DM1" -len 1.96118337075000 
Quadrupole "QUAD.CR2.DBA.QF3" -len 0.4 -S1 [expr -2.611445582383727/2.38*$refen] -refen $refen 
Drift "CR2.DBA.DM0" -len 0.109313245 
Sextupole "SEXT.CR2.DBA.SD0" -len 0.1 -S2 [expr 0.1914983233102843/2.38*$refen] -refen $refen 
Drift "CR2.DBA.D11B1" -len 0.3424874764 
Quadrupole "QUAD.CR2.DBA.QI2" -len 0.4 -S1 [expr 0.821588*$refen] -refen $refen 
Drift "CR2.DBA.D12" -len 0.2697746031 
Sbend "BEND.CR2.DBA.DIPOLE0" -len 2.182098945 -angle -0.2108714854 -E1 -0.1054357427 -E2 -0.1054357427 -S1 [expr 0.0399258644*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*-0.2108714854*-0.2108714854/2.182098945*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "CR2.DBA.DS1C_A" -len 0.6435098443 
Sextupole "SEXT.CR2.DBA.SNN1.6" -len 0.1 -S2 [expr -4.053818418657726] -refen $refen 
Drift "CR2.DBA.DS1C_B" -len 0.109313245 
Quadrupole "QUAD.CR2.DBA.QS1C" -len 0.4 -S1 [expr -1.114316*$refen] -refen $refen 
Drift "CR2.DBA.DS2C_A" -len 0.4926115415 
Sextupole "SEXT.CR2.DBA.SF0.6" -len 0.1 -S2 [expr -0.1257848660334976] -refen $refen 
Drift "CR2.DBA.DS2C_B" -len 0.01730305995 
Sbend "BEND.CR2.DBA.DIPN0" -len 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -S1 [expr 0.7724967744*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "CR2.DBA.DS3C_A" -len 0.01730305995 
Sextupole "SEXT.CR2.DBA.SNN3.6" -len 0.1 -S2 [expr -0.2978498685059033] -refen $refen 
Drift "CR2.DBA.DS3C_B" -len 0.2945377043 
Quadrupole "QUAD.CR2.DBA.QS2C" -len 0.2 -S1 [expr -0.1188136*$refen] -refen $refen 
Quadrupole "QUAD.CR2.DBA.QS2C" -len 0.2 -S1 [expr -0.1188136*$refen] -refen $refen 
Drift "CR2.DBA.DS3C_B" -len 0.2945377043 
Sextupole "SEXT.CR2.DBA.SN3.6" -len 0.1 -S2 [expr -4.808394576721998] -refen $refen 
Drift "CR2.DBA.DS3C_A" -len 0.01730305995 
Sbend "BEND.CR2.DBA.DIPN0" -len 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -S1 [expr 0.7724967744*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "CR2.DBA.DS2C_B" -len 0.01730305995 
Sextupole "SEXT.CR2.DBA.SF0.6" -len 0.1 -S2 [expr -0.1257848660334976] -refen $refen 
Drift "CR2.DBA.DS2C_A" -len 0.4926115415 
Quadrupole "QUAD.CR2.DBA.QS1C" -len 0.4 -S1 [expr -1.114316*$refen] -refen $refen 
Drift "CR2.DBA.DS1C_B" -len 0.109313245 
Sextupole "SEXT.CR2.DBA.SN1.6" -len 0.1 -S2 [expr 6.070287629089756] -refen $refen 
Drift "CR2.DBA.DS1C_A" -len 0.6435098443 
Sbend "BEND.CR2.DBA.DIPOLE0" -len 2.182098945 -angle -0.2108714854 -E1 -0.1054357427 -E2 -0.1054357427 -S1 [expr 0.0399258644*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*-0.2108714854*-0.2108714854/2.182098945*$e0*$e0*$e0*$e0*$Dipole_synrad] 
########################################
Drift "CR2.DBA.D12" -len 0.2697746031 
Quadrupole "QUAD.CR2.DBA.QI2" -len 0.4 -S1 [expr 0.821588*$refen] -refen $refen 
Drift "CR2.DBA.D11B" -len 0.1628018956 
Sextupole "SEXT.CR2.DBA.SNCH1" -len 0.1 -S2 [expr -4.466459229927851/2.38*$refen] -refen $refen 
Drift "CR2.DBA.D11A" -len 0.1730305995 
Quadrupole "QUAD.CR2.DBA.QI1" -len 0.4 -S1 [expr -0.590316*$refen] -refen $refen 
Marker "CR2MDACELLEND" 
Drift "CR2.DS1C_A" -len 2.179474654 
Sextupole "SEXT.CR2.SXS1C" -len 0.1 -S2 [expr 5.142412649852638/2.38*$refen] -refen $refen 
Drift "CR2.DS1C_B" -len 0.3 
Quadrupole "QUAD.CR2.QS1C" -len 0.5 -S1 [expr -1.540709432006429/2.38*$refen] -refen $refen 
Bpm "BPM.CR2_1.15" -len 0 
Drift "CR2.DS2C" -len 0.2003907272 
Quadrupole "QUAD.CR2.QS2C" -len 0.5 -S1 [expr 1.881391159116313/2.38*$refen] -refen $refen 
Bpm "BPM.CR2_1.16" -len 0 
Drift "CR2.DS3C_A" -len 0.3 
Sextupole "SEXT.CR2.SXS3C" -len 0.1 -S2 [expr -1.015135827715248/2.38*$refen] -refen $refen 
Drift "CR2.DS3C_B" -len 2.777418619 
Quadrupole "QUAD.CR2.QS3C" -len 0.5 -S1 [expr -0.5875481326262182/2.38*$refen] -refen $refen 
Drift "CR2.DS3C_C" -len 0.5
Quadrupole "QUAD.CR2.QS4C" -len 0.5 -S1 [expr 0.5765703325351983/2.38*$refen] -refen $refen 
Bpm "BPM.CR2_1.17" -len 0 
Drift "CR2.DS4C" -len 0.2407160001 
Drift "CR2.BC1" -len 0.4 
Drift "CR2.BC2" -len 0.4 
Drift "CR2.BC3" -len 0.4 
Drift "CR2.BC1" -len 0.4 
Drift "CR2.DS4C" -len 0.2407160001 
Quadrupole "QUAD.CR2.QS4C" -len 0.5 -S1 [expr 0.5765703325351983/2.38*$refen] -refen $refen 
Drift "CR2.DS3C_C" -len 0.5
Quadrupole "QUAD.CR2.QS3C" -len 0.5 -S1 [expr -0.5875481326262182/2.38*$refen] -refen $refen 
Bpm "BPM.CR2_1.18" -len 0 
Drift "CR2.DS3C_B" -len 2.777418619 
Sextupole "SEXT.CR2.SXS3C" -len 0.1 -S2 [expr -1.015135827715248/2.38*$refen] -refen $refen 
Drift "CR2.DS3C_A" -len 0.3 
Quadrupole "QUAD.CR2.QS2C" -len 0.5 -S1 [expr 1.881391159116313/2.38*$refen] -refen $refen 
Bpm "BPM.CR2_1.19" -len 0 
Drift "CR2.DS2C" -len 0.2003907272 
Quadrupole "QUAD.CR2.QS1C" -len 0.5 -S1 [expr -1.540709432006429/2.38*$refen] -refen $refen 
Bpm "BPM.CR2_1.20" -len 0 
Drift "CR2.DS1C_B" -len 0.3 
Sextupole "SEXT.CR2.SXS1C" -len 0.1 -S2 [expr 5.142412649852638/2.38*$refen] -refen $refen 
Drift "CR2.DS1C_A" -len 2.179474654 
Marker "CR2MDACELLSTART" 
Quadrupole "QUAD.CR2.DBA.QI1" -len 0.4 -S1 [expr -0.590316*$refen] -refen $refen 
Drift "CR2.DBA.D11A" -len 0.1730305995 
Sextupole "SEXT.CR2.DBA.SNCH1" -len 0.1 -S2 [expr -4.466459229927851/2.38*$refen] -refen $refen 
Drift "CR2.DBA.D11B" -len 0.1628018956 
Quadrupole "QUAD.CR2.DBA.QI2" -len 0.4 -S1 [expr 0.821588*$refen] -refen $refen 
Drift "CR2.DBA.D12" -len 0.2697746031 
#######################
Sbend "BEND.CR2.DBA.DIPOLE0" -len 2.182098945 -angle -0.2108714854 -E1 -0.1054357427 -E2 -0.1054357427 -S1 [expr 0.0399258644*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*-0.2108714854*-0.2108714854/2.182098945*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "CR2.DBA.DS1C_A" -len 0.6435098443 
Sextupole "SEXT.CR2.DBA.SN1.7" -len 0.1 -S2 [expr 4.899724576729371] -refen $refen 
Drift "CR2.DBA.DS1C_B" -len 0.109313245 
Marker "CR2.DBA.HBM" 
Quadrupole "QUAD.CR2.DBA.QS1C" -len 0.4 -S1 [expr -1.114316*$refen] -refen $refen 
Drift "CR2.DBA.DS2C_A" -len 0.4926115415 
Sextupole "SEXT.CR2.DBA.SF0.7" -len 0.1 -S2 [expr 1.521106200228468] -refen $refen 
Drift "CR2.DBA.DS2C_B" -len 0.01730305995 
Sbend "BEND.CR2.DBA.DIPN0" -len 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -S1 [expr 0.7724967744*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "CR2.DBA.DS3C_A" -len 0.01730305995 
Sextupole "SEXT.CR2.DBA.SN3.7" -len 0.1 -S2 [expr -5.887343326991537] -refen $refen 
Drift "CR2.DBA.DS3C_B" -len 0.2945377043 
Quadrupole "QUAD.CR2.DBA.QS2C" -len 0.2 -S1 [expr -0.1188136*$refen] -refen $refen 
Quadrupole "QUAD.CR2.DBA.QS2C" -len 0.2 -S1 [expr -0.1188136*$refen] -refen $refen 
Drift "CR2.DBA.DS3C_B" -len 0.2945377043 
Sextupole "SEXT.CR2.DBA.SNN3.7" -len 0.1 -S2 [expr -0.2168578726832133] -refen $refen 
Drift "CR2.DBA.DS3C_A" -len 0.01730305995 
Sbend "BEND.CR2.DBA.DIPN0" -len 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -S1 [expr 0.7724967744*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "CR2.DBA.DS2C_B" -len 0.01730305995 
Sextupole "SEXT.CR2.DBA.SF0.7" -len 0.1 -S2 [expr 1.521106200228468] -refen $refen 
Drift "CR2.DBA.DS2C_A" -len 0.4926115415 
Quadrupole "QUAD.CR2.DBA.QS1C" -len 0.4 -S1 [expr -1.114316*$refen] -refen $refen 
Drift "CR2.DBA.DS1C_B" -len 0.109313245 
Sextupole "SEXT.CR2.DBA.SNN1.7" -len 0.1 -S2 [expr -9.51968689509059] -refen $refen 
Drift "CR2.DBA.DS1C_A" -len 0.6435098443 
Sbend "BEND.CR2.DBA.DIPOLE0" -len 2.182098945 -angle -0.2108714854 -E1 -0.1054357427 -E2 -0.1054357427 -S1 [expr 0.0399258644*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*-0.2108714854*-0.2108714854/2.182098945*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "CR2.DBA.D12" -len 0.2697746031 
Quadrupole "QUAD.CR2.DBA.QI2" -len 0.4 -S1 [expr 0.821588*$refen] -refen $refen 
Drift "CR2.DBA.D11B1" -len 0.3424874764 
Sextupole "SEXT.CR2.DBA.SD0" -len 0.1 -S2 [expr 0.1914983233102843/2.38*$refen] -refen $refen 
Drift "CR2.DBA.DM0" -len 0.109313245 
Quadrupole "QUAD.CR2.DBA.QF1" -len 0.4 -S1 [expr -2.611445582383727/2.38*$refen] -refen $refen 
Drift "CR2.DBA.DM1" -len 1.96118337075000 
Quadrupole "QUAD.CR2.DBA.QF2" -len 0.4 -S1 [expr 0.6679291738134705/2.38*$refen] -refen $refen 
Drift "CR2.DBA.DM1" -len 1.96118337075000 
Quadrupole "QUAD.CR2.DBA.QF3" -len 0.4 -S1 [expr -2.611445582383727/2.38*$refen] -refen $refen 
Drift "CR2.DBA.DM0" -len 0.109313245 
Sextupole "SEXT.CR2.DBA.SD0" -len 0.1 -S2 [expr 0.1914983233102843/2.38*$refen] -refen $refen 
Drift "CR2.DBA.D11B1" -len 0.3424874764 
Quadrupole "QUAD.CR2.DBA.QI2" -len 0.4 -S1 [expr 0.821588*$refen] -refen $refen 
Drift "CR2.DBA.D12" -len 0.2697746031 
Sbend "BEND.CR2.DBA.DIPOLE0" -len 2.182098945 -angle -0.2108714854 -E1 -0.1054357427 -E2 -0.1054357427 -S1 [expr 0.0399258644*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*-0.2108714854*-0.2108714854/2.182098945*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "CR2.DBA.DS1C_A" -len 0.6435098443 
Sextupole "SEXT.CR2.DBA.SNN1.7" -len 0.1 -S2 [expr -9.51968689509059] -refen $refen 
Drift "CR2.DBA.DS1C_B" -len 0.109313245 
Quadrupole "QUAD.CR2.DBA.QS1C" -len 0.4 -S1 [expr -1.114316*$refen] -refen $refen 
Drift "CR2.DBA.DS2C_A" -len 0.4926115415 
Sextupole "SEXT.CR2.DBA.SF0.7" -len 0.1 -S2 [expr 1.521106200228468] -refen $refen 
Drift "CR2.DBA.DS2C_B" -len 0.01730305995 
Sbend "BEND.CR2.DBA.DIPN0" -len 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -S1 [expr 0.7724967744*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "CR2.DBA.DS3C_A" -len 0.01730305995 
Sextupole "SEXT.CR2.DBA.SNN3.7" -len 0.1 -S2 [expr -0.2168578726832133] -refen $refen 
Drift "CR2.DBA.DS3C_B" -len 0.2945377043 
Quadrupole "QUAD.CR2.DBA.QS2C" -len 0.2 -S1 [expr -0.1188136*$refen] -refen $refen 
Quadrupole "QUAD.CR2.DBA.QS2C" -len 0.2 -S1 [expr -0.1188136*$refen] -refen $refen 
Drift "CR2.DBA.DS3C_B" -len 0.2945377043 
Sextupole "SEXT.CR2.DBA.SN3.7" -len 0.1 -S2 [expr -5.887343326991537] -refen $refen 
Drift "CR2.DBA.DS3C_A" -len 0.01730305995 
Sbend "BEND.CR2.DBA.DIPN0" -len 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -S1 [expr 0.7724967744*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "CR2.DBA.DS2C_B" -len 0.01730305995 
Sextupole "SEXT.CR2.DBA.SF0.7" -len 0.1 -S2 [expr 1.521106200228468] -refen $refen 
Drift "CR2.DBA.DS2C_A" -len 0.4926115415 
Quadrupole "QUAD.CR2.DBA.QS1C" -len 0.4 -S1 [expr -1.114316*$refen] -refen $refen 
Drift "CR2.DBA.DS1C_B" -len 0.109313245 
Sextupole "SEXT.CR2.DBA.SN1.7" -len 0.1 -S2 [expr 4.899724576729371] -refen $refen 
Drift "CR2.DBA.DS1C_A" -len 0.6435098443 
Sbend "BEND.CR2.DBA.DIPOLE0" -len 2.182098945 -angle -0.2108714854 -E1 -0.1054357427 -E2 -0.1054357427 -S1 [expr 0.0399258644*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*-0.2108714854*-0.2108714854/2.182098945*$e0*$e0*$e0*$e0*$Dipole_synrad] 
########################################
Drift "CR2.DBA.D12" -len 0.2697746031 
Quadrupole "QUAD.CR2.DBA.QI2" -len 0.4 -S1 [expr 0.821588*$refen] -refen $refen 
Drift "CR2.DBA.D11B" -len 0.1628018956 
Sextupole "SEXT.CR2.DBA.SNCH1" -len 0.1 -S2 [expr -4.466459229927851/2.38*$refen] -refen $refen 
Drift "CR2.DBA.D11A" -len 0.1730305995 
Quadrupole "QUAD.CR2.DBA.QI1" -len 0.4 -S1 [expr -0.590316*$refen] -refen $refen 
Marker "CR2MDACELLEND" 
Drift "CR2.DTR1A" -len 1.1 
Sextupole "SEXT.CR2.SXTR1" -len 0.1 -S2 [expr 11.90518312758056/2.38*$refen] -refen $refen 
Drift "CR2.DTR1B" -len 0.2 
Quadrupole "QUAD.CR2.QTR1" -len 0.5 -S1 [expr -0.8932952208812484/2.38*$refen] -refen $refen 
Bpm "BPM.CR2_1.7" -len 0 
Drift "CR2.DTR2A" -len 1.1 
Sextupole "SEXT.CR2.SXTR2" -len 0.1 -S2 [expr -21.05518599722218/2.38*$refen] -refen $refen 
Drift "CR2.DTR2B" -len 0.2 
Quadrupole "QUAD.CR2.QTR2" -len 0.5 -S1 [expr 1.396779518046066/2.38*$refen] -refen $refen 
Bpm "BPM.CR2_1.8" -len 0 
Drift "CR2.DTR3A" -len 1.5 
Sextupole "SEXT.CR2.SXTR3" -len 0.1 -S2 [expr 37.8906248766911/2.38*$refen] -refen $refen 
Drift "CR2.DTR3B" -len 0.2 
Quadrupole "QUAD.CR2.QTR3" -len 0.5 -S1 [expr -0.6630399232271197/2.38*$refen] -refen $refen 
Bpm "BPM.CR2_1.9" -len 0 
Drift "CR2.DTR4A" -len 2.1328 
Sextupole "SEXT.CR2.SXTR4" -len 0.1 -S2 [expr -13.25727703236136/2.38*$refen] -refen $refen 
Drift "CR2.DTR4B" -len 0.2 
Quadrupole "QUAD.CR2.QTR4" -len 0.25 -S1 [expr 0.4450739458716257/2.38*$refen] -refen $refen 
Bpm "BPM.CR2_1.10" -len 0 
Quadrupole "QUAD.CR2.QTR4" -len 0.25 -S1 [expr 0.4450739458716257/2.38*$refen] -refen $refen 
Bpm "BPM.CR2_1.11" -len 0 
Drift "CR2.DTR4B" -len 0.2 
Sextupole "SEXT.CR2.SXTR4" -len 0.1 -S2 [expr -13.25727703236136/2.38*$refen] -refen $refen 
Drift "CR2.DTR4A" -len 2.1328 
Quadrupole "QUAD.CR2.QTR3" -len 0.5 -S1 [expr -0.6630399232271197/2.38*$refen] -refen $refen 
Bpm "BPM.CR2_1.12" -len 0 
Drift "CR2.DTR3B" -len 0.2 
Sextupole "SEXT.CR2.SXTR3" -len 0.1 -S2 [expr 37.8906248766911/2.38*$refen] -refen $refen 
Drift "CR2.DTR3A" -len 1.5 
Quadrupole "QUAD.CR2.QTR2" -len 0.5 -S1 [expr 1.396779518046066/2.38*$refen] -refen $refen 
Bpm "BPM.CR2_1.13" -len 0 
Drift "CR2.DTR2B" -len 0.2 
Sextupole "SEXT.CR2.SXTR2" -len 0.1 -S2 [expr -21.05518599722218/2.38*$refen] -refen $refen 
Drift "CR2.DTR2A" -len 1.1 
Quadrupole "QUAD.CR2.QTR1" -len 0.5 -S1 [expr -0.8932952208812484/2.38*$refen] -refen $refen 
Bpm "BPM.CR2_1.14" -len 0 
Drift "CR2.DTR1B" -len 0.2 
Sextupole "SEXT.CR2.SXTR1" -len 0.1 -S2 [expr 11.90518312758056/2.38*$refen] -refen $refen 
Drift "CR2.DTR1A" -len 1.1 
Marker "CR2MDACELLSTART" 
Quadrupole "QUAD.CR2.DBA.QI1" -len 0.4 -S1 [expr -0.590316*$refen] -refen $refen 
Drift "CR2.DBA.D11A" -len 0.1730305995 
Sextupole "SEXT.CR2.DBA.SNCH1" -len 0.1 -S2 [expr -4.466459229927851/2.38*$refen] -refen $refen 
Drift "CR2.DBA.D11B" -len 0.1628018956 
Quadrupole "QUAD.CR2.DBA.QI2" -len 0.4 -S1 [expr 0.821588*$refen] -refen $refen 
Drift "CR2.DBA.D12" -len 0.2697746031 
#######################
Sbend "BEND.CR2.DBA.DIPOLE0" -len 2.182098945 -angle -0.2108714854 -E1 -0.1054357427 -E2 -0.1054357427 -S1 [expr 0.0399258644*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*-0.2108714854*-0.2108714854/2.182098945*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "CR2.DBA.DS1C_A" -len 0.6435098443 
Sextupole "SEXT.CR2.DBA.SN1.8" -len 0.1 -S2 [expr 11.26238722561791] -refen $refen 
Drift "CR2.DBA.DS1C_B" -len 0.109313245 
Marker "CR2.DBA.HBM" 
Quadrupole "QUAD.CR2.DBA.QS1C" -len 0.4 -S1 [expr -1.114316*$refen] -refen $refen 
Drift "CR2.DBA.DS2C_A" -len 0.4926115415 
Sextupole "SEXT.CR2.DBA.SF0.8" -len 0.1 -S2 [expr 1.305171698023675] -refen $refen 
Drift "CR2.DBA.DS2C_B" -len 0.01730305995 
Sbend "BEND.CR2.DBA.DIPN0" -len 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -S1 [expr 0.7724967744*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "CR2.DBA.DS3C_A" -len 0.01730305995 
Sextupole "SEXT.CR2.DBA.SN3.8" -len 0.1 -S2 [expr -6.385868761860418] -refen $refen 
Drift "CR2.DBA.DS3C_B" -len 0.2945377043 
Quadrupole "QUAD.CR2.DBA.QS2C" -len 0.2 -S1 [expr -0.1188136*$refen] -refen $refen 
Quadrupole "QUAD.CR2.DBA.QS2C" -len 0.2 -S1 [expr -0.1188136*$refen] -refen $refen 
Drift "CR2.DBA.DS3C_B" -len 0.2945377043 
Sextupole "SEXT.CR2.DBA.SNN3.8" -len 0.1 -S2 [expr -1.445606995157745] -refen $refen 
Drift "CR2.DBA.DS3C_A" -len 0.01730305995 
Sbend "BEND.CR2.DBA.DIPN0" -len 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -S1 [expr 0.7724967744*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "CR2.DBA.DS2C_B" -len 0.01730305995 
Sextupole "SEXT.CR2.DBA.SF0.8" -len 0.1 -S2 [expr 1.305171698023675] -refen $refen 
Drift "CR2.DBA.DS2C_A" -len 0.4926115415 
Quadrupole "QUAD.CR2.DBA.QS1C" -len 0.4 -S1 [expr -1.114316*$refen] -refen $refen 
Drift "CR2.DBA.DS1C_B" -len 0.109313245 
Sextupole "SEXT.CR2.DBA.SNN1.8" -len 0.1 -S2 [expr -8.156993945047169] -refen $refen 
Drift "CR2.DBA.DS1C_A" -len 0.6435098443 
Sbend "BEND.CR2.DBA.DIPOLE0" -len 2.182098945 -angle -0.2108714854 -E1 -0.1054357427 -E2 -0.1054357427 -S1 [expr 0.0399258644*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*-0.2108714854*-0.2108714854/2.182098945*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "CR2.DBA.D12" -len 0.2697746031 
Quadrupole "QUAD.CR2.DBA.QI2" -len 0.4 -S1 [expr 0.821588*$refen] -refen $refen 
Drift "CR2.DBA.D11B1" -len 0.3424874764 
Sextupole "SEXT.CR2.DBA.SD0" -len 0.1 -S2 [expr 0.1914983233102843/2.38*$refen] -refen $refen 
Drift "CR2.DBA.DM0" -len 0.109313245 
Quadrupole "QUAD.CR2.DBA.QF1" -len 0.4 -S1 [expr -2.611445582383727/2.38*$refen] -refen $refen 
Drift "CR2.DBA.DM1" -len 1.96118337075000 
Quadrupole "QUAD.CR2.DBA.QF2" -len 0.4 -S1 [expr 0.6679291738134705/2.38*$refen] -refen $refen 
Drift "CR2.DBA.DM1" -len 1.96118337075000 
Quadrupole "QUAD.CR2.DBA.QF3" -len 0.4 -S1 [expr -2.611445582383727/2.38*$refen] -refen $refen 
Drift "CR2.DBA.DM0" -len 0.109313245 
Sextupole "SEXT.CR2.DBA.SD0" -len 0.1 -S2 [expr 0.1914983233102843/2.38*$refen] -refen $refen 
Drift "CR2.DBA.D11B1" -len 0.3424874764 
Quadrupole "QUAD.CR2.DBA.QI2" -len 0.4 -S1 [expr 0.821588*$refen] -refen $refen 
Drift "CR2.DBA.D12" -len 0.2697746031 
Sbend "BEND.CR2.DBA.DIPOLE0" -len 2.182098945 -angle -0.2108714854 -E1 -0.1054357427 -E2 -0.1054357427 -S1 [expr 0.0399258644*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*-0.2108714854*-0.2108714854/2.182098945*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "CR2.DBA.DS1C_A" -len 0.6435098443 
Sextupole "SEXT.CR2.DBA.SNN1.8" -len 0.1 -S2 [expr -8.156993945047169] -refen $refen 
Drift "CR2.DBA.DS1C_B" -len 0.109313245 
Quadrupole "QUAD.CR2.DBA.QS1C" -len 0.4 -S1 [expr -1.114316*$refen] -refen $refen 
Drift "CR2.DBA.DS2C_A" -len 0.4926115415 
Sextupole "SEXT.CR2.DBA.SF0.8" -len 0.1 -S2 [expr 1.305171698023675] -refen $refen 
Drift "CR2.DBA.DS2C_B" -len 0.01730305995 
Sbend "BEND.CR2.DBA.DIPN0" -len 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -S1 [expr 0.7724967744*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "CR2.DBA.DS3C_A" -len 0.01730305995 
Sextupole "SEXT.CR2.DBA.SNN3.8" -len 0.1 -S2 [expr -1.445606995157745] -refen $refen 
Drift "CR2.DBA.DS3C_B" -len 0.2945377043 
Quadrupole "QUAD.CR2.DBA.QS2C" -len 0.2 -S1 [expr -0.1188136*$refen] -refen $refen 
Quadrupole "QUAD.CR2.DBA.QS2C" -len 0.2 -S1 [expr -0.1188136*$refen] -refen $refen 
Drift "CR2.DBA.DS3C_B" -len 0.2945377043 
Sextupole "SEXT.CR2.DBA.SN3.8" -len 0.1 -S2 [expr -6.385868761860418] -refen $refen 
Drift "CR2.DBA.DS3C_A" -len 0.01730305995 
Sbend "BEND.CR2.DBA.DIPN0" -len 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -S1 [expr 0.7724967744*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "CR2.DBA.DS2C_B" -len 0.01730305995 
Sextupole "SEXT.CR2.DBA.SF0.8" -len 0.1 -S2 [expr 1.305171698023675] -refen $refen 
Drift "CR2.DBA.DS2C_A" -len 0.4926115415 
Quadrupole "QUAD.CR2.DBA.QS1C" -len 0.4 -S1 [expr -1.114316*$refen] -refen $refen 
Drift "CR2.DBA.DS1C_B" -len 0.109313245 
Sextupole "SEXT.CR2.DBA.SN1.8" -len 0.1 -S2 [expr 11.26238722561791] -refen $refen 
Drift "CR2.DBA.DS1C_A" -len 0.6435098443 
Sbend "BEND.CR2.DBA.DIPOLE0" -len 2.182098945 -angle -0.2108714854 -E1 -0.1054357427 -E2 -0.1054357427 -S1 [expr 0.0399258644*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*-0.2108714854*-0.2108714854/2.182098945*$e0*$e0*$e0*$e0*$Dipole_synrad] 
########################################
Drift "CR2.DBA.D12" -len 0.2697746031 
Quadrupole "QUAD.CR2.DBA.QI2" -len 0.4 -S1 [expr 0.821588*$refen] -refen $refen 
Drift "CR2.DBA.D11B" -len 0.1628018956 
Sextupole "SEXT.CR2.DBA.SNCH1" -len 0.1 -S2 [expr -4.466459229927851/2.38*$refen] -refen $refen 
Drift "CR2.DBA.D11A" -len 0.1730305995 
Quadrupole "QUAD.CR2.DBA.QI1" -len 0.4 -S1 [expr -0.590316*$refen] -refen $refen 
Marker "CR2MDACELLEND" 
#### Missing length (0.202692445399988) ####
Drift "CR2.MISSLENGTH" -len 0.202692445399988 
########################
Drift "CR2.DL0" -len 1.246504682 
Quadrupole "QUAD.CR2.QL0IN.IN1" -len 0.5 -S1 [expr -0.8416137519093586/2.38*$refen] -refen $refen 
Drift "CR2.DL0IN" -len 0.5
Quadrupole "QUAD.CR2.QL1IN.IN1" -len 0.5 -S1 [expr 2.33456992899555/2.38*$refen] -refen $refen 
Bpm "BPM.CR2_2.63" -len 0 
Drift "CR2.DL1IN" -len 0.4000469975 
Quadrupole "QUAD.CR2.QL2IN.IN1" -len 0.5 -S1 [expr -0.07259122102091951/2.38*$refen] -refen $refen 
Bpm "BPM.CR2_2.64" -len 0 
Drift "CR2.DL2IN" -len 1.103409345 
Quadrupole "QUAD.CR2.QL3IN.IN1" -len 0.5 -S1 [expr -0.622223771004862/2.38*$refen] -refen $refen 
Bpm "BPM.CR2_2.65" -len 0 
Drift "CR2.DL3IN" -len 0.4000389758 

#### RF CORRECTOR ####
## 0.1357244419 mm delay from oscillations in the orbit
## Machine will have to take this into consideration but its easier to fix here for RF
Drift "CR2.RFK1" -len 0 
#Kalign al1 -cz 0 
Kalign al1 -fixed 1 -cz -0.0001357244419 
Drift "CR2.al1" -len 0 -node 1 -kick al1
Drift "DRRF" -len 1 
Rfmultipole "RFMulti.CR2.RFKICK1" -len 0 -strength [expr -0.004*$refen] -freq [expr 0.9995*3.] -phase [expr -$Pi/2] -refen $refen 
Drift "DRRF" -len 1 
######################

Drift "CR2.DRFB1" -len 0.217192186 
Quadrupole "QUAD.CR2.QRFB1.IN1" -len 0.5 -S1 [expr -0.5374590184440241/2.38*$refen] -refen $refen 
Drift "CR2.DRFB2" -len 1.193240969 
Quadrupole "QUAD.CR2.QRFB2.IN1" -len 0.5 -S1 [expr 0.9450649197627298/2.38*$refen] -refen $refen 
Drift "CR2.DRFB3" -len 3.771233433 
Quadrupole "QUAD.CR2.QRFB3.IN1" -len 0.5 -S1 [expr -0.1961337714186991/2.38*$refen] -refen $refen 
Drift -name "CR2.D1CM" -len 0.01 
Drift "CR2.D1M" -len 1 
Sextupole "SEXT.CR2.SRFB4.IN1" -len 0.5 -S2 [expr -11.22874735811252/2.38*$refen] -refen $refen 
Drift "CR2.DS1" -len 0.5 
Sextupole "SEXT.CR2.SRFB5.IN1" -len 0.5 -S2 [expr 7.129175399665087/2.38*$refen] -refen $refen 
Drift "CR2.DS1" -len 0.5 
Drift "CR2.DRFB4A" -len 0.709135479 
Sextupole "SEXT.CR2.SRFB6.IN1" -len 0.4 -S2 [expr 7.1495251758585/2.38*$refen] -refen $refen 
Drift "CR2.DRFB6" -len 2.367622162 
Drift "CR2.SEPTUM" -len 1 
#### Beamline under construction ####################################
#### Injection needs to be done into after the sextupole section ####
Drift "CR2.SEPTUM" -len 1 
Drift "CR2.DRFB6" -len 2.367622162 
Sextupole "SEXT.CR2.SRFB6.IN2" -len 0.4 -S2 [expr -8.181902960699396/2.38*$refen] -refen $refen 
Drift "CR2.DRFB4A" -len 0.709135479 
Drift "CR2.DS1" -len 0.5 
Sextupole "SEXT.CR2.SRFB5.IN2" -len 0.5 -S2 [expr 0.6930613244632331/2.38*$refen] -refen $refen 
Drift "CR2.DS1" -len 0.5 
Sextupole "SEXT.CR2.SRFB4.IN2" -len 0.5 -S2 [expr 3.395028044914335/2.38*$refen] -refen $refen 
#####################################################################
Marker "CR2_P2.EJECTION" 
