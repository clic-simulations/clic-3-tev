## Reference energy taken from list_energies.tcl (Computed from Eduardo Martin's Lattice) 
## This was chosen to match the less energetic "bunch type" (there are 24 different possible bunch energies). 
## Which means there will be a slight magnet missfocus for the other bunches. 
if {$Dipole_synrad} { 
	set refen 2.378298928542217 
} else { 
	set refen 2.38 
} 
Marker "TL2.INJECTION" 
### The beam exits CR1 with an offset in the combiner ring's referencial,
### This kicker puts sets the referencial so that the beam is centered (use fixed for multibunch studies)
#Kalign TL2offset -cx 0 -cxp 0 -cz 0
Kalign TL2offset -fixed 1 -cx -0.01719855576 -cxp 0.0001020892625 -cz -1.761748864e-05
#Kalign TL2offset -fixed 1 -cx -0.017198 -cxp 1.1931e-04 -cz 1.1760e-05
Marker "TL2_offset" -nodes 1 -kick TL2offset 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.005723015 
Sbend "BEND.TL2.BEND10" -len 2.01146240647403 -angle -0.2617993878 -E1 -0.1308996939 -E2 -0.1308996939 -refen $refen 
set refen [expr $refen-14.1e-6*-0.2617993878*-0.2617993878/2.01146240647403*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TL2.D122" -len 0.667347296 
Sextupole "SEXT.TL2.SX125" -len 0.3 -S2 [expr 4.819174160165822] -refen $refen 
Drift "TL2.D128" -len 0.258673648 
Quadrupole "QUAD.TL2.Q13" -len 0.75 -S1 [expr 1.198840197413727] -refen $refen 
Bpm "BPM.TL2.1" -len 0 
Drift "TL2.D142" -len 0.1747711281 
Sextupole "SEXT.TL2.SX145" -len 0.3 -S2 [expr -6.792192616542144] -refen $refen 
Drift "TL2.D148" -len 0.1747711281 
Quadrupole "QUAD.TL2.Q15" -len 0.75 -S1 [expr -1.369291380321628] -refen $refen 
Bpm "BPM.TL2.2" -len 0 
Drift "TL2.D162" -len 0.2366852486 
Drift "TL2.D168" -len 0.2366852486 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 0.500013974 
Sbend "BEND.TL2.BEND17" -len 0.500027948380432 -angle 0.02589861758 -E1 0.01294930879 -E2 0.01294930879 -refen $refen 
set refen [expr $refen-14.1e-6*0.02589861758*0.02589861758/0.500027948380432*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TL2.D182" -len 0.05000058615 
Sextupole "SEXT.TL2.SX185" -len 0.3 -S2 [expr -1.142685508617159] -refen $refen 
Drift "TL2.D188" -len 0.05000058615 
Quadrupole "QUAD.TL2.Q20A" -len 0.375 -S1 [expr 0.582477696898400] -refen $refen 
Bpm "BPM.TL2.3" -len 0 
Marker "TL2.MIDCELLONE" 
Quadrupole "QUAD.TL2.Q20B" -len 0.375 -S1 [expr 0.582477696898400] -refen $refen 
Bpm "BPM.TL2.4" -len 0 
Drift "TL2.D222" -len 0.1536564076 
Sextupole "SEXT.TL2.SX225" -len 0.3 -S2 [expr -3.753617540795152] -refen $refen 
Drift "TL2.D228" -len 0.1536564076 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 0.500013974 
Sbend "BEND.TL2.BEND23" -len 0.500027948380432 -angle 0.02589861758 -E1 0.01294930879 -E2 0.01294930879 -refen $refen 
set refen [expr $refen-14.1e-6*0.02589861758*0.02589861758/0.500027948380432*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TL2.D242" -len 0.0500000253 
Drift "TL2.D248" -len 0.0500000253 
Quadrupole "QUAD.TL2.Q25" -len 0.75 -S1 [expr -1.369291380321628] -refen $refen 
Bpm "BPM.TL2.5" -len 0 
Drift "TL2.D262" -len 0.1562568231 
Sextupole "SEXT.TL2.SX265" -len 0.3 -S2 [expr 16.559407914177331] -refen $refen 
Drift "TL2.D268" -len 0.1562568231 
Quadrupole "QUAD.TL2.Q27" -len 0.75 -S1 [expr 1.198840197413727] -refen $refen 
Bpm "BPM.TL2.6" -len 0 
Drift "TL2.D282" -len 0.3136946693 
Sextupole "SEXT.TL2.SX285" -len 0.3 -S2 [expr -16.914356376540820] -refen $refen 
Drift "TL2.D288" -len 0.7773893387 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.005723015 
Sbend "BEND.TL2.BEND30" -len 2.01146240647403 -angle -0.2617993878 -E1 -0.1308996939 -E2 -0.1308996939 -refen $refen 
set refen [expr $refen-14.1e-6*-0.2617993878*-0.2617993878/2.01146240647403*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Bpm "BPM.TL2.D302" -len 0.4980886966 
Sextupole "SEXT.TL2.SXC305" -len 0.3 -S2 [expr 23.917123431933000] -refen $refen 
Bpm "BPM.TL2.D308" -len 0.06602956553 
Quadrupole "QUAD.TL2.Q31" -len 0.75 -S1 [expr 0.514826558337493] -refen $refen 
Bpm "BPM.TL2.9" -len 0 
Bpm "BPM.TL2.D312" -len 0.9874829865 
Sextupole "SEXT.TL2.SXC315" -len 0.3 -S2 [expr -9.581066846722335] -refen $refen 
Bpm "BPM.TL2.D318" -len 0.2291609955 
Quadrupole "QUAD.TL2.Q32" -len 0.75 -S1 [expr -1.178127764059216] -refen $refen 
Bpm "BPM.TL2.12" -len 0 
Bpm "BPM.TL2.D322" -len 1.245610625 
Sextupole "SEXT.TL2.SXC325" -len 0.3 -S2 [expr -10.556881222902859] -refen $refen 
Bpm "BPM.TL2.D328" -len 0.3152035415 
Quadrupole "QUAD.TL2.Q33" -len 0.75 -S1 [expr 1.686182726464094] -refen $refen 
Bpm "BPM.TL2.15" -len 0 
Bpm "BPM.TL2.D332" -len 1.58013019 
Sextupole "SEXT.TL2.SXC335" -len 0.3 -S2 [expr -67.957687934979234] -refen $refen 
Bpm "BPM.TL2.D338" -len 0.4267100632 
Quadrupole "QUAD.TL2.Q34" -len 0.75 -S1 [expr -1.064382523635110] -refen $refen 
Bpm "BPM.TL2.18" -len 0 
Bpm "BPM.TL2.D342" -len 1.673352339 
Sextupole "SEXT.TL2.SXC345" -len 0.3 -S2 [expr -10.884747487122420] -refen $refen 
Bpm "BPM.TL2.D348" -len 0.457784113 
Quadrupole "QUAD.TL2.Q35" -len 0.75 -S1 [expr 1.357403298994673] -refen $refen 
Bpm "BPM.TL2.21" -len 0 
Bpm "BPM.TL2.D352" -len 0.457784113 
Sextupole "SEXT.TL2.SXC355" -len 0.3 -S2 [expr -10.884747487122420] -refen $refen 
Bpm "BPM.TL2.D358" -len 1.673352339 
Quadrupole "QUAD.TL2.Q36" -len 0.75 -S1 [expr -1.064382523635110] -refen $refen 
Bpm "BPM.TL2.24" -len 0 
Bpm "BPM.TL2.D362" -len 0.4267100632 
Sextupole "SEXT.TL2.SXC365" -len 0.3 -S2 [expr -67.957687934979234] -refen $refen 
Bpm "BPM.TL2.D368" -len 1.58013019 
Quadrupole "QUAD.TL2.Q37" -len 0.75 -S1 [expr 1.686182726464094] -refen $refen 
Bpm "BPM.TL2.27" -len 0 
Bpm "BPM.TL2.D372" -len 0.3152035415 
Sextupole "SEXT.TL2.SXC375" -len 0.3 -S2 [expr -10.556881222902859] -refen $refen 
Bpm "BPM.TL2.D378" -len 1.245610625 
Quadrupole "QUAD.TL2.Q38" -len 0.75 -S1 [expr -1.178127764059216] -refen $refen 
Bpm "BPM.TL2.30" -len 0 
Bpm "BPM.TL2.D382" -len 0.2291609955 
Sextupole "SEXT.TL2.SXC385" -len 0.3 -S2 [expr -9.581066846722335] -refen $refen 
Bpm "BPM.TL2.D388" -len 0.9874829865 
Quadrupole "QUAD.TL2.Q39" -len 0.75 -S1 [expr 0.514826558337493] -refen $refen 
Bpm "BPM.TL2.33" -len 0 
Bpm "BPM.TL2.D392" -len 0.06602956553 
Sextupole "SEXT.TL2.SXC395" -len 0.3 -S2 [expr 23.917123431933000] -refen $refen 
Bpm "BPM.TL2.D398" -len 0.4980886966 
Marker "TL2.INTEROMSEND" 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.005723015 
Sbend "BEND.TL2.BEND40" -len 2.01146240647403 -angle 0.2617993878 -E1 0.1308996939 -E2 0.1308996939 -refen $refen 
set refen [expr $refen-14.1e-6*0.2617993878*0.2617993878/2.01146240647403*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TL2.D422" -len 0.7773893387 
Sextupole "SEXT.TL2.SX425" -len 0.3 -S2 [expr 16.882895157071779] -refen $refen 
Drift "TL2.D428" -len 0.3136946693 
Quadrupole "QUAD.TL2.Q43" -len 0.75 -S1 [expr 1.198840197413727] -refen $refen 
Bpm "BPM.TL2.36" -len 0 
Drift "TL2.D442" -len 0.1562568231 
Sextupole "SEXT.TL2.SX445" -len 0.3 -S2 [expr -7.379602099254930] -refen $refen 
Drift "TL2.D448" -len 0.1562568231 
Quadrupole "QUAD.TL2.Q45" -len 0.75 -S1 [expr -1.369291380321628] -refen $refen 
Bpm "BPM.TL2.37" -len 0 
Drift "TL2.D462" -len 0.0500000253 
Drift "TL2.D468" -len 0.0500000253 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 0.500013974 
Sbend "BEND.TL2.BEND47" -len 0.500027948380432 -angle -0.02589861758 -E1 -0.01294930879 -E2 -0.01294930879 -refen $refen 
set refen [expr $refen-14.1e-6*-0.02589861758*-0.02589861758/0.500027948380432*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TL2.D482" -len 0.1536564076 
Sextupole "SEXT.TL2.SX485" -len 0.3 -S2 [expr -2.222683785347300] -refen $refen 
Drift "TL2.D488" -len 0.1536564076 
Quadrupole "QUAD.TL2.Q50A" -len 0.375 -S1 [expr 0.582477696898400] -refen $refen 
Bpm "BPM.TL2.38" -len 0 
Marker "TL2.MIDCELLTWO" 
Quadrupole "QUAD.TL2.Q50B" -len 0.375 -S1 [expr 0.582477696898400] -refen $refen 
Bpm "BPM.TL2.39" -len 0 
Drift "TL2.D522" -len 0.1536564076 
Sextupole "SEXT.TL2.SX525" -len 0.3 -S2 [expr 6.438748384920830] -refen $refen 
Drift "TL2.D528" -len 0.1536564076 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 0.500013974 
Sbend "BEND.TL2.BEND53" -len 0.500027948380432 -angle -0.02589861758 -E1 -0.01294930879 -E2 -0.01294930879 -refen $refen 
set refen [expr $refen-14.1e-6*-0.02589861758*-0.02589861758/0.500027948380432*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TL2.D542" -len 0.0500000253 
Drift "TL2.D548" -len 0.0500000253 
Quadrupole "QUAD.TL2.Q55" -len 0.75 -S1 [expr -1.369291380321628] -refen $refen 
Bpm "BPM.TL2.40" -len 0 
Drift "TL2.D562" -len 0.1562568231 
Sextupole "SEXT.TL2.SX565" -len 0.3 -S2 [expr 8.007650548263738] -refen $refen 
Drift "TL2.D568" -len 0.1562568231 
Quadrupole "QUAD.TL2.Q57" -len 0.75 -S1 [expr 1.198840197413727] -refen $refen 
Bpm "BPM.TL2.41" -len 0 
Drift "TL2.D582" -len 0.3136946693 
Sextupole "SEXT.TL2.SX585" -len 0.3 -S2 [expr -5.826893841039538] -refen $refen 
Drift "TL2.D588" -len 0.7773893387 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.005723015 
Sbend "BEND.TL2.BEND60" -len 2.01146240647403 -angle 0.2617993878 -E1 0.1308996939 -E2 0.1308996939 -refen $refen 
set refen [expr $refen-14.1e-6*0.2617993878*0.2617993878/2.01146240647403*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Marker "TL2.OMSEND" 
Drift "TL2.D71" -len 0.8 
Quadrupole "QUAD.TL2.Q72" -len 0.75 -S1 [expr 1.704167203287427] -refen $refen 
Bpm "BPM.TL2.42" -len 0 
Drift "TL2.D73" -len 0.8 
Quadrupole "QUAD.TL2.Q74" -len 0.75 -S1 [expr -1.664820422904495] -refen $refen 
Bpm "BPM.TL2.43" -len 0 
Drift "TL2.D75" -len 1.6 
Quadrupole "QUAD.TL2.Q76" -len 0.75 -S1 [expr 1.434252650708431] -refen $refen 
Bpm "BPM.TL2.44" -len 0 
Drift "TL2.D77" -len 0.8 
Quadrupole "QUAD.TL2.Q78" -len 0.75 -S1 [expr  -0.619805093242998] -refen $refen 
Bpm "BPM.TL2.45" -len 0 
Drift "TL2.D79" -len 0.8 
#### Start of FODO main sequence ###
Marker "TL2.MFODO" 
Quadrupole "QUAD.TL2.QFFODO" -len 0.75 -S1 [expr 0.3*$refen] -refen $refen 
Bpm "BPM.TL2.70" -len 0 
Drift "TL2.DLFODO" -len 3.25 
Quadrupole "QUAD.TL2.QDFODO" -len 0.75 -S1 [expr -0.3*$refen] -refen $refen 
Bpm "BPM.TL2.71" -len 0 
Drift "TL2.DLFODO" -len 3.25 
Marker "TL2.MFODO" 
Quadrupole "QUAD.TL2.QFFODO" -len 0.75 -S1 [expr 0.3*$refen] -refen $refen 
Bpm "BPM.TL2.72" -len 0 
Drift "TL2.DLFODO" -len 3.25 
Quadrupole "QUAD.TL2.QDFODO" -len 0.75 -S1 [expr -0.3*$refen] -refen $refen 
Bpm "BPM.TL2.73" -len 0 
Drift "TL2.DLFODO" -len 3.25 
Marker "TL2.MFODO" 
Quadrupole "QUAD.TL2.QFFODO" -len 0.75 -S1 [expr 0.3*$refen] -refen $refen 
Bpm "BPM.TL2.74" -len 0 
Drift "TL2.DLFODO" -len 3.25 
Quadrupole "QUAD.TL2.QDFODO" -len 0.75 -S1 [expr -0.3*$refen] -refen $refen 
Bpm "BPM.TL2.75" -len 0 
Drift "TL2.DLFODO" -len 3.25 
Marker "TL2.MFODO" 
Quadrupole "QUAD.TL2.QFFODO" -len 0.75 -S1 [expr 0.3*$refen] -refen $refen 
Bpm "BPM.TL2.76" -len 0 
Drift "TL2.DLFODO" -len 3.25 
Quadrupole "QUAD.TL2.QDFODO" -len 0.75 -S1 [expr -0.3*$refen] -refen $refen 
Bpm "BPM.TL2.77" -len 0 
Drift "TL2.DLFODO" -len 3.25 
Marker "TL2.MFODO" 
Quadrupole "QUAD.TL2.QFFODO" -len 0.75 -S1 [expr 0.3*$refen] -refen $refen 
Bpm "BPM.TL2.78" -len 0 
Drift "TL2.DLFODO" -len 3.25 
Quadrupole "QUAD.TL2.QDFODO" -len 0.75 -S1 [expr -0.3*$refen] -refen $refen 
Bpm "BPM.TL2.79" -len 0 
Drift "TL2.DLFODO" -len 3.25 
Marker "TL2.MFODO" 
Quadrupole "QUAD.TL2.QFFODO" -len 0.75 -S1 [expr 0.3*$refen] -refen $refen 
Bpm "BPM.TL2.80" -len 0 
Drift "TL2.DLFODO" -len 3.25 
Quadrupole "QUAD.TL2.QDFODO" -len 0.75 -S1 [expr -0.3*$refen] -refen $refen 
Bpm "BPM.TL2.81" -len 0 
Drift "TL2.DLFODO" -len 3.25 
Marker "TL2.MFODO" 
Quadrupole "QUAD.TL2.QFFODO" -len 0.75 -S1 [expr 0.3*$refen] -refen $refen 
Bpm "BPM.TL2.82" -len 0 
Drift "TL2.DLFODO" -len 3.25 
Quadrupole "QUAD.TL2.QDFODO" -len 0.75 -S1 [expr -0.3*$refen] -refen $refen 
Bpm "BPM.TL2.83" -len 0 
Drift "TL2.DLFODO" -len 3.25 
Marker "TL2.MFODO" 
Quadrupole "QUAD.TL2.QFFODO" -len 0.75 -S1 [expr 0.3*$refen] -refen $refen 
Bpm "BPM.TL2.84" -len 0 
Drift "TL2.DLFODO" -len 3.25 
Quadrupole "QUAD.TL2.QDFODO" -len 0.75 -S1 [expr -0.3*$refen] -refen $refen 
Bpm "BPM.TL2.85" -len 0 
Drift "TL2.DLFODO" -len 3.25 
Marker "TL2.MFODO" 
Quadrupole "QUAD.TL2.QFFODO" -len 0.75 -S1 [expr 0.3*$refen] -refen $refen 
Bpm "BPM.TL2.86" -len 0 
Drift "TL2.DLFODO" -len 3.25 
Quadrupole "QUAD.TL2.QDFODO" -len 0.75 -S1 [expr -0.3*$refen] -refen $refen 
Bpm "BPM.TL2.87" -len 0 
Drift "TL2.DLFODO" -len 3.25 
Marker "TL2.MFODO" 
Quadrupole "QUAD.TL2.QFFODO" -len 0.75 -S1 [expr 0.3*$refen] -refen $refen 
Bpm "BPM.TL2.88" -len 0 
Drift "TL2.DLFODO" -len 3.25 
Quadrupole "QUAD.TL2.QDFODO" -len 0.75 -S1 [expr -0.3*$refen] -refen $refen 
Bpm "BPM.TL2.89" -len 0 
Drift "TL2.DLFODO" -len 3.25 
Marker "TL2.MFODO" 
Quadrupole "QUAD.TL2.QFFODO" -len 0.75 -S1 [expr 0.3*$refen] -refen $refen 
Bpm "BPM.TL2.90" -len 0 
Drift "TL2.DLFODO" -len 3.25 
Quadrupole "QUAD.TL2.QDFODO" -len 0.75 -S1 [expr -0.3*$refen] -refen $refen 
Bpm "BPM.TL2.91" -len 0 
Drift "TL2.DLFODO" -len 3.25 
Marker "TL2.MFODO" 
Quadrupole "QUAD.TL2.QFFODO" -len 0.75 -S1 [expr 0.3*$refen] -refen $refen 
Bpm "BPM.TL2.92" -len 0 
Drift "TL2.DLFODO" -len 3.25 
Quadrupole "QUAD.TL2.QDFODO" -len 0.75 -S1 [expr -0.3*$refen] -refen $refen 
Bpm "BPM.TL2.93" -len 0 
Drift "TL2.DLFODO" -len 3.25 
Marker "TL2.MFODO" 
Quadrupole "QUAD.TL2.QFFODO" -len 0.75 -S1 [expr 0.3*$refen] -refen $refen 
Bpm "BPM.TL2.94" -len 0 
Drift "TL2.DLFODO" -len 3.25 
Quadrupole "QUAD.TL2.QDFODO" -len 0.75 -S1 [expr -0.3*$refen] -refen $refen 
Bpm "BPM.TL2.95" -len 0 
Drift "TL2.DLFODO" -len 3.25 
Marker "TL2.MFODO" 
Quadrupole "QUAD.TL2.QFFODO" -len 0.75 -S1 [expr 0.3*$refen] -refen $refen 
Bpm "BPM.TL2.96" -len 0 
Drift "TL2.DLFODO" -len 3.25 
Quadrupole "QUAD.TL2.QDFODO" -len 0.75 -S1 [expr -0.3*$refen] -refen $refen 
Bpm "BPM.TL2.97" -len 0 
Drift "TL2.DLFODO" -len 3.25 
Marker "TL2.MFODO" 
Quadrupole "QUAD.TL2.QFFODO" -len 0.75 -S1 [expr 0.3*$refen] -refen $refen 
Bpm "BPM.TL2.98" -len 0 
Drift "TL2.DLFODO" -len 3.25 
Quadrupole "QUAD.TL2.QDFODO" -len 0.75 -S1 [expr -0.3*$refen] -refen $refen 
Bpm "BPM.TL2.99" -len 0 
Drift "TL2.DLFODO" -len 3.25 
Marker "TL2.MFODO" 
Quadrupole "QUAD.TL2.QFFODO" -len 0.75 -S1 [expr 0.3*$refen] -refen $refen 
Bpm "BPM.TL2.100" -len 0 
Drift "TL2.DLFODO" -len 3.25 
Quadrupole "QUAD.TL2.QDFODO" -len 0.75 -S1 [expr -0.3*$refen] -refen $refen 
Bpm "BPM.TL2.101" -len 0 
Drift "TL2.DLFODO" -len 3.25 
Marker "TL2.MFODO" 
Quadrupole "QUAD.TL2.QFFODO" -len 0.75 -S1 [expr 0.3*$refen] -refen $refen 
Bpm "BPM.TL2.102" -len 0 
Drift "TL2.DLFODO" -len 3.25 
Quadrupole "QUAD.TL2.QDFODO" -len 0.75 -S1 [expr -0.3*$refen] -refen $refen 
Bpm "BPM.TL2.103" -len 0 
Drift "TL2.DLFODO" -len 3.25 
Marker "TL2.MFODO" 
Quadrupole "QUAD.TL2.QFFODO" -len 0.75 -S1 [expr 0.3*$refen] -refen $refen 
Bpm "BPM.TL2.104" -len 0 
Drift "TL2.DLFODO" -len 3.25 
Quadrupole "QUAD.TL2.QDFODO" -len 0.75 -S1 [expr -0.3*$refen] -refen $refen 
Bpm "BPM.TL2.105" -len 0 
Drift "TL2.DLFODO" -len 3.25 
Marker "TL2.MFODO" 
Quadrupole "QUAD.TL2.QFFODO" -len 0.75 -S1 [expr 0.3*$refen] -refen $refen 
Bpm "BPM.TL2.106" -len 0 
Drift "TL2.DLFODO" -len 3.25 
Quadrupole "QUAD.TL2.QDFODO" -len 0.75 -S1 [expr -0.3*$refen] -refen $refen 
Bpm "BPM.TL2.107" -len 0 
Drift "TL2.DLFODO" -len 3.25 
Marker "TL2.MFODO" 
Quadrupole "QUAD.TL2.QFFODO" -len 0.75 -S1 [expr 0.3*$refen] -refen $refen 
Bpm "BPM.TL2.108" -len 0 
Drift "TL2.DLFODO" -len 3.25 
Quadrupole "QUAD.TL2.QDFODO" -len 0.75 -S1 [expr -0.3*$refen] -refen $refen 
Bpm "BPM.TL2.109" -len 0 
Drift "TL2.DLFODO" -len 3.25 
Marker "TL2.MFODO" 
Quadrupole "QUAD.TL2.QFFODO" -len 0.75 -S1 [expr 0.3*$refen] -refen $refen 
Bpm "BPM.TL2.110" -len 0 
Drift "TL2.DLFODO" -len 3.25 
Quadrupole "QUAD.TL2.QDFODO" -len 0.75 -S1 [expr -0.3*$refen] -refen $refen 
Bpm "BPM.TL2.111" -len 0 
Drift "TL2.DLFODO" -len 3.25 
Marker "TL2.MFODO" 
Quadrupole "QUAD.TL2.QFFODO" -len 0.75 -S1 [expr 0.3*$refen] -refen $refen 
Bpm "BPM.TL2.112" -len 0 
Drift "TL2.DLFODO" -len 3.25 
Quadrupole "QUAD.TL2.QDFODO" -len 0.75 -S1 [expr -0.3*$refen] -refen $refen 
Bpm "BPM.TL2.113" -len 0 
Drift "TL2.DLFODO" -len 3.25 
Marker "TL2.MFODO" 
Quadrupole "QUAD.TL2.QFFODO" -len 0.75 -S1 [expr 0.3*$refen] -refen $refen 
Bpm "BPM.TL2.114" -len 0 
Drift "TL2.DLFODO" -len 3.25 
Quadrupole "QUAD.TL2.QDFODO" -len 0.75 -S1 [expr -0.3*$refen] -refen $refen 
Bpm "BPM.TL2.115" -len 0 
Drift "TL2.DLFODO" -len 3.25 
Marker "TL2.MFODO" 
Quadrupole "QUAD.TL2.QFFODO" -len 0.75 -S1 [expr 0.3*$refen] -refen $refen 
Bpm "BPM.TL2.116" -len 0 
Drift "TL2.DLFODO" -len 3.25 
Quadrupole "QUAD.TL2.QDFODO" -len 0.75 -S1 [expr -0.3*$refen] -refen $refen 
Bpm "BPM.TL2.117" -len 0 
Drift "TL2.DLFODO" -len 3.25 
Marker "TL2.MFODO" 
Quadrupole "QUAD.TL2.QFFODO" -len 0.75 -S1 [expr 0.3*$refen] -refen $refen 
Bpm "BPM.TL2.118" -len 0 
Drift "TL2.DLFODO" -len 3.25 
Quadrupole "QUAD.TL2.QDFODO" -len 0.75 -S1 [expr -0.3*$refen] -refen $refen 
Bpm "BPM.TL2.119" -len 0 
Drift "TL2.DLFODO" -len 3.25 
Marker "TL2.MFODO" 
Quadrupole "QUAD.TL2.QFFODO" -len 0.75 -S1 [expr 0.3*$refen] -refen $refen 
Bpm "BPM.TL2.120" -len 0 
Drift "TL2.DLFODO" -len 3.25 
Quadrupole "QUAD.TL2.QDFODO" -len 0.75 -S1 [expr -0.3*$refen] -refen $refen 
Bpm "BPM.TL2.121" -len 0 
Drift "TL2.DLFODO" -len 3.25 
Marker "TL2.MFODO" 
Quadrupole "QUAD.TL2.QFFODO" -len 0.75 -S1 [expr 0.3*$refen] -refen $refen 
Bpm "BPM.TL2.122" -len 0 
Drift "TL2.DLFODO" -len 3.25 
Quadrupole "QUAD.TL2.QDFODO" -len 0.75 -S1 [expr -0.3*$refen] -refen $refen 
Bpm "BPM.TL2.123" -len 0 
Drift "TL2.DLFODO" -len 3.25 
Marker "TL2.MFODO" 
Quadrupole "QUAD.TL2.QFFODO" -len 0.75 -S1 [expr 0.3*$refen] -refen $refen 
Bpm "BPM.TL2.124" -len 0 
Drift "TL2.DLFODO" -len 3.25 
Quadrupole "QUAD.TL2.QDFODO" -len 0.75 -S1 [expr -0.3*$refen] -refen $refen 
Bpm "BPM.TL2.125" -len 0 
Drift "TL2.DLFODO" -len 3.25 
Quadrupole "QUAD.TL2.QFFODO" -len 0.75 -S1 [expr 0.3*$refen] -refen $refen 
Bpm "BPM.TL2.126" -len 0 
Drift "TL2.DLFODO" -len 3.25 
Drift "TL2.D91" -len 0.8 
Quadrupole "QUAD.TL2.Q92" -len 0.75 -S1 [expr -2.17161559300819/2.38*$refen] -refen $refen 
Bpm "BPM.TL2.127" -len 0 
Drift "TL2.D93" -len 0.8 
Quadrupole "QUAD.TL2.Q94" -len 0.75 -S1 [expr 1.86452550276657/2.38*$refen] -refen $refen 
Bpm "BPM.TL2.128" -len 0 
Drift "TL2.D95" -len 1.2 
Quadrupole "QUAD.TL2.Q96" -len 0.75 -S1 [expr -1.80796092383161/2.38*$refen] -refen $refen 
Bpm "BPM.TL2.129" -len 0 
Drift "TL2.D97" -len 0.8 
Quadrupole "QUAD.TL2.Q98" -len 0.75 -S1 [expr 0.519909549354063/2.38*$refen] -refen $refen 
Bpm "BPM.TL2.130" -len 0 
Drift "TL2.D99" -len 0.8 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.005723015 
Sbend "BEND.TL2.BEND110" -len 2.01146240647403 -angle -0.2617993878 -E1 -0.1308996939 -E2 -0.1308996939 -refen $refen 
set refen [expr $refen-14.1e-6*-0.2617993878*-0.2617993878/2.01146240647403*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TL2.D1122" -len 1.60479140 
Sextupole "SEXT.TL2.SX1125" -len 0.3 -S2 [expr 4.30541159202988/2.38*$refen] -refen $refen 
Drift "TL2.D1128" -len 0.727395700 
Quadrupole "QUAD.TL2.Q113" -len 0.75 -S1 [expr 1.02128206278427/2.38*$refen] -refen $refen 
Bpm "BPM.TL2.131" -len 0 
Drift "TL2.D1142" -len 0.2203857562 
Sextupole "SEXT.TL2.SX1145" -len 0.3 -S2 [expr 0.790361292227178/2.38*$refen] -refen $refen 
Drift "TL2.D1148" -len 0.2203857562 
Quadrupole "QUAD.TL2.Q115" -len 0.75 -S1 [expr -1.21495425710102/2.38*$refen] -refen $refen 
Bpm "BPM.TL2.132" -len 0 
Drift "TL2.D1162" -len 0.05000000025 
Drift "TL2.D1168" -len 0.05000000025 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 0.5000086579 
Sbend "BEND.TL2.BEND117" -len 0.500017315941417 -angle 0.02038562618 -E1 0.01019281309 -E2 0.01019281309 -refen $refen 
set refen [expr $refen-14.1e-6*0.02038562618*0.02038562618/0.500017315941417*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TL2.D1182" -len 0.1905674726 
Sextupole "SEXT.TL2.SX1185" -len 0.3 -S2 [expr -11.1717539464023/2.38*$refen] -refen $refen 
Drift "TL2.D1188" -len 0.1905674726 
Quadrupole "QUAD.TL2.Q120A" -len 0.375 -S1 [expr 0.484365010160291/2.38*$refen] -refen $refen 
Bpm "BPM.TL2.133" -len 0 
Marker "TL2.MIDCELL11" 
Quadrupole "QUAD.TL2.Q120B" -len 0.375 -S1 [expr 0.484365010160291/2.38*$refen] -refen $refen 
Bpm "BPM.TL2.134" -len 0 
Drift "TL2.D1222" -len 0.1905674726 
Sextupole "SEXT.TL2.SX1225" -len 0.3 -S2 [expr 7.39060690141353/2.38*$refen] -refen $refen 
Drift "TL2.D1228" -len 0.1905674726 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 0.5000086579 
Sbend "BEND.TL2.BEND123" -len 0.500017315941417 -angle 0.02038562618 -E1 0.01019281309 -E2 0.01019281309 -refen $refen 
set refen [expr $refen-14.1e-6*0.02038562618*0.02038562618/0.500017315941417*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TL2.D1242" -len 0.05000000025 
Drift "TL2.D1248" -len 0.05000000025 
Quadrupole "QUAD.TL2.Q125" -len 0.75 -S1 [expr -1.21495464127517/2.38*$refen] -refen $refen 
Bpm "BPM.TL2.135" -len 0 
Drift "TL2.D1262" -len 0.2203857562 
Sextupole "SEXT.TL2.SX1265" -len 0.3 -S2 [expr -0.0637723966394775/2.38*$refen] -refen $refen 
Drift "TL2.D1268" -len 0.2203857562 
Quadrupole "QUAD.TL2.Q127" -len 0.75 -S1 [expr 1.02128238571839/2.38*$refen] -refen $refen 
Bpm "BPM.TL2.136" -len 0 
Drift "TL2.D1282" -len 0.727395700 
Sextupole "SEXT.TL2.SX1285" -len 0.3 -S2 [expr 0.0719298836817325/2.38*$refen] -refen $refen 
Drift "TL2.D1288" -len 1.60479140 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.005723015 
Sbend "BEND.TL2.BEND130" -len 2.01146240647403 -angle -0.2617993878 -E1 -0.1308996939 -E2 -0.1308996939 -refen $refen 
set refen [expr $refen-14.1e-6*-0.2617993878*-0.2617993878/2.01146240647403*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Bpm "BPM.TL2.D1302" -len 0.1024133036 
Sextupole "SEXT.TL2.SXC1305" -len 0.3 -S2 [expr 3.41356613226330/2.38*$refen] -refen $refen 
Bpm "BPM.TL2.D1308" -len 0.1024133036 
Quadrupole "QUAD.TL2.Q131" -len 0.75 -S1 [expr -1.53065520076519/2.38*$refen] -refen $refen 
Bpm "BPM.TL2.139" -len 0 
Bpm "BPM.TL2.D1312" -len 0.0911315578 
Sextupole "SEXT.TL2.SXC1315" -len 0.3 -S2 [expr -19.7246098431532/2.38*$refen] -refen $refen 
Bpm "BPM.TL2.D1318" -len 0.0911315578 
Quadrupole "QUAD.TL2.Q132" -len 0.75 -S1 [expr 1.94602723895332/2.38*$refen] -refen $refen 
Bpm "BPM.TL2.142" -len 0 
Bpm "BPM.TL2.D1322" -len 1.292423292 
Sextupole "SEXT.TL2.SXC1325" -len 0.3 -S2 [expr -2.18606324408107/2.38*$refen] -refen $refen 
Bpm "BPM.TL2.D1328" -len 0.330807764 
Quadrupole "QUAD.TL2.Q133" -len 0.75 -S1 [expr -0.0517525032945611/2.38*$refen] -refen $refen 
Bpm "BPM.TL2.145" -len 0 
Bpm "BPM.TL2.D1332" -len 0.480449320 
Sextupole "SEXT.TL2.SXC1335" -len 0.3 -S2 [expr -0.226103366441352/2.38*$refen] -refen $refen 
Bpm "BPM.TL2.D1338" -len 1.741347962 
Quadrupole "QUAD.TL2.Q134" -len 0.75 -S1 [expr 1.51895003038335/2.38*$refen] -refen $refen 
Bpm "BPM.TL2.148" -len 0 
Bpm "BPM.TL2.D1342" -len 0.1086306923 
Sextupole "SEXT.TL2.SXC1345" -len 0.3 -S2 [expr 12.9816499729857/2.38*$refen] -refen $refen 
Bpm "BPM.TL2.D1348" -len 0.6258920768 
Quadrupole "QUAD.TL2.Q135" -len 0.75 -S1 [expr -1.75455370916885/2.38*$refen] -refen $refen 
Bpm "BPM.TL2.151" -len 0 
Bpm "BPM.TL2.D1352" -len 0.6258920768 
Sextupole "SEXT.TL2.SXC1355" -len 0.3 -S2 [expr -11.8090263617674/2.38*$refen] -refen $refen 
Bpm "BPM.TL2.D1358" -len 0.1086306923 
Quadrupole "QUAD.TL2.Q136" -len 0.75 -S1 [expr 0.932405496686693/2.38*$refen] -refen $refen 
Bpm "BPM.TL2.154" -len 0 
Bpm "BPM.TL2.D1362" -len 1.741347962 
Sextupole "SEXT.TL2.SXC1365" -len 0.3 -S2 [expr -38.6383607807860/2.38*$refen] -refen $refen 
Bpm "BPM.TL2.D1368" -len 0.480449320 
Quadrupole "QUAD.TL2.Q137" -len 0.75 -S1 [expr -1.56892299569804/2.38*$refen] -refen $refen 
Bpm "BPM.TL2.157" -len 0 
Bpm "BPM.TL2.D1372" -len 0.330807764 
Sextupole "SEXT.TL2.SXC1375" -len 0.3 -S2 [expr 42.1423314315658/2.38*$refen] -refen $refen 
Bpm "BPM.TL2.D1378" -len 1.292423292 
Quadrupole "QUAD.TL2.Q138" -len 0.75 -S1 [expr -0.470191404928240/2.38*$refen] -refen $refen 
Bpm "BPM.TL2.160" -len 0 
Bpm "BPM.TL2.D1382" -len 0.0911315578 
Sextupole "SEXT.TL2.SXC1385" -len 0.3 -S2 [expr 20.06060207634605/2.38*$refen] -refen $refen 
Bpm "BPM.TL2.D1388" -len 0.0911315578 
Quadrupole "QUAD.TL2.Q139" -len 0.75 -S1 [expr 1.09704706494349/2.38*$refen] -refen $refen 
Bpm "BPM.TL2.163" -len 0 
Bpm "BPM.TL2.D1392" -len 0.1024133036 
Sextupole "SEXT.TL2.SXC1395" -len 0.3 -S2 [expr -8.108596728740821/2.38*$refen] -refen $refen 
Bpm "BPM.TL2.D1398" -len 0.1024133036 
Marker "TL2.INTEROMSEND" 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.005723015 
Sbend "BEND.TL2.BEND140" -len 2.01146240647403 -angle 0.2617993878 -E1 0.1308996939 -E2 0.1308996939 -refen $refen 
set refen [expr $refen-14.1e-6*0.2617993878*0.2617993878/2.01146240647403*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TL2.D1422" -len 1.60479140 
Sextupole "SEXT.TL2.SX1425" -len 0.3 -S2 [expr 6.62062392358905/2.38*$refen] -refen $refen 
Drift "TL2.D1428" -len 0.727395700 
Quadrupole "QUAD.TL2.Q143" -len 0.75 -S1 [expr 0.314553440389555/2.38*$refen] -refen $refen 
Bpm "BPM.TL2.166" -len 0 
Drift "TL2.D1442" -len 0.2203857562 
Sextupole "SEXT.TL2.SX1445" -len 0.3 -S2 [expr -5.18939246128068/2.38*$refen] -refen $refen 
Drift "TL2.D1448" -len 0.2203857562 
Quadrupole "QUAD.TL2.Q145" -len 0.75 -S1 [expr -1.29353480867137/2.38*$refen] -refen $refen 
Bpm "BPM.TL2.167" -len 0 
Drift "TL2.D1462" -len 0.05000000025 
Drift "TL2.D1468" -len 0.05000000025 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 0.5000086579 
Sbend "BEND.TL2.BEND147" -len 0.500017315941417 -angle -0.02038562618 -E1 -0.01019281309 -E2 -0.01019281309 -refen $refen 
set refen [expr $refen-14.1e-6*-0.02038562618*-0.02038562618/0.500017315941417*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TL2.D1482" -len 0.1905674726 
Sextupole "SEXT.TL2.SX1485" -len 0.3 -S2 [expr 1.63287098964959/2.38*$refen] -refen $refen 
Drift "TL2.D1488" -len 0.1905674726 
Quadrupole "QUAD.TL2.Q150A" -len 0.375 -S1 [expr 0.383696861367534/2.38*$refen] -refen $refen 
Bpm "BPM.TL2.168" -len 0 
Marker "TL2.MIDCELL12" 
Quadrupole "QUAD.TL2.Q150B" -len 0.375 -S1 [expr 0.383696861367534/2.38*$refen] -refen $refen 
Bpm "BPM.TL2.169" -len 0 
Drift "TL2.D1522" -len 0.0001175 
Sextupole "SEXT.TL2.SX1525" -len 0.3 -S2 [expr 0.551354533339311/2.38*$refen] -refen $refen 
Drift "TL2.D1528" -len 0.0001175 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 0.5000086579 
Sbend "BEND.TL2.BEND153" -len 0.500017315941417 -angle -0.02038562618 -E1 -0.01019281309 -E2 -0.01019281309 -refen $refen 
set refen [expr $refen-14.1e-6*-0.02038562618*-0.02038562618/0.500017315941417*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TL2.D1542" -len 0.0976775 
Drift "TL2.D1548" -len 0.0976775 
Quadrupole "QUAD.TL2.Q155" -len 0.75 -S1 [expr 1.26366654856135/2.38*$refen] -refen $refen 
Bpm "BPM.TL2.170" -len 0 
Drift "TL2.D1562" -len 0.042885 
Sextupole "SEXT.TL2.SX1565" -len 0.3 -S2 [expr -0.793893385389341/2.38*$refen] -refen $refen 
Drift "TL2.D1568" -len 0.042885 
Quadrupole "QUAD.TL2.Q157" -len 0.75 -S1 [expr -2.32369058432889/2.38*$refen] -refen $refen 
Bpm "BPM.TL2.171" -len 0 
Drift "TL2.D1582" -len 0.8837 
Sextupole "SEXT.TL2.SX1585" -len 0.3 -S2 [expr -0.153975013678105/2.38*$refen] -refen $refen 
Drift "TL2.D1588" -len 1.9174 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.005723015 
Sbend "BEND.TL2.BEND160" -len 2.01146240647403 -angle 0.2617993878 -E1 0.1308996939 -E2 0.1308996939 -refen $refen 
set refen [expr $refen-14.1e-6*0.2617993878*0.2617993878/2.01146240647403*$e0*$e0*$e0*$e0*$Dipole_synrad] 
### Offset needed for CR2 Injection   (use fixed for multibunch studies)
### b2 going for -0.02286596894359623 -0.0006579938618535864 -0.0001042623843514314
#Kalign CR2offset  -cx -0.02286596894359623 -cxp -0.0006579938618535864  -cz -0.0001042623843514314
Kalign CR2offset  -fixed 1 -cx -0.0225759468943962 -cxp -6.22745187753586e-04  -cz -2.54805275751431e-04
#Kalign CR2offset -fixed 1 -cx ?? -cxp ?? -cz ??
Drift "CR2_offset" -nodes 1 -kick CR2offset 
Marker "TL2.EJECTION" 
