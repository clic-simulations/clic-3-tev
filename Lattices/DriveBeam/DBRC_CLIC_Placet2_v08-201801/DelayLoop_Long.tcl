## Reference energy taken from list_energies.tcl (Computed from Eduardo Martin's Lattice) 
## This was chosen to match the less energetic "bunch type" (there are 24 different possible bunch energies). 
## Which means there will be a slight magnet missfocus for the other bunches if synchrotron is on. 
if {$Dipole_synrad} { 
	set refen 2.379985336493252 
} else { 
	set refen 2.38 
} 
Marker "DL_LONG.INJECTION" 
Kalign DL_inj -cx 0. -cxp 0. 
Drift "DL_LONG.CENTERINJ" -len 0. -nodes 1 -kick DL_inj 
# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 0.5000007127 
Sbend "BEND.SEPTQ" -len 0.500001425441655 -angle -0.005849061169 -E1 -0.002924530584 -E2 -0.002924530584 -S1 [expr -0.5000007127*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*-0.005849061169*-0.005849061169/0.500001425441655*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "DL1IN" -len 1.1 
# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 1.00031738 
Sbend "BEND.SEPT1" -len 1.00063486103645 -angle -0.0872664626 -E1 -0.0436332313 -E2 -0.0436332313 -S1 [expr 0.9956481989*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*-0.0872664626*-0.0872664626/1.00063486103645*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "DL2IN" -len 0.3 
Quadrupole "QUAD.QL1IN" -len 0.4 -S1 [expr -0.647036*$refen] -refen $refen 
Bpm "BPM.DL_L" -len 0 
Drift "DL3IN" -len 0.4 
# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.574109835 
Sbend "BEND.SEPT2" -len 2.58202788155376 -angle -0.2714151852 -E1 -0.1357075926 -E2 -0.1357075926 -S1 [expr -1.249456954*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*-0.2714151852*-0.2714151852/2.58202788155376*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Marker "MD3.1" 
Drift "D33" -len 0.192971 
Quadrupole "QUAD.QL2IN" -len 0.4 -S1 [expr 1.14802*$refen] -refen $refen 
Bpm "BPM.DL_L.2" -len 0 
Drift "D32A" -len 0.11111 
Sextupole "SEXT.DL.SXL2" -len 0.1 -S2 [expr -3.816703898607325/2.38*$refen] -refen $refen 
Drift "D32B" -len 0.43333 
Quadrupole "QUAD.QL3IN" -len 0.4 -S1 [expr -1.943224*$refen] -refen $refen 
Bpm "BPM.DL_L.3" -len 0 
Drift "D33A" -len 0.0464855 
Sextupole "SEXT.DL.SXL3" -len 0.1 -S2 [expr 10.13678425458882/2.38*$refen] -refen $refen 
Drift "D33B" -len 0.0464855 
Sbend "BEND.DL.DIPN0" -len 0.2851351189 -angle 0.04133674544 -E1 0.02066837272 -E2 0.02066837272 -S1 [expr 0.9511964998*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*0.04133674544*0.04133674544/0.2851351189*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Marker "MD2.1" 
Drift "DS3CA" -len 0.08511145 
Sextupole "SEXT.DL.SXL4" -len 0.1 -S2 [expr -10.82803618878591/2.38*$refen] -refen $refen 
Drift "DS3CB" -len 0.08511145 
Quadrupole "QUAD.QS2C" -len 0.2 -S1 [expr -0.1666676*$refen] -refen $refen 
Bpm "BPM.DL_L.4" -len 0 
Quadrupole "QUAD.QS2C" -len 0.2 -S1 [expr -0.1666676*$refen] -refen $refen 
Bpm "BPM.DL_L.5" -len 0 
Drift "DS3C_B" -len 0.1702229 
Sextupole "SEXT.DL.SNN3" -len 0.1 -S2 [expr 2.510029401343759/2.38*$refen] -refen $refen 
Drift "DS3C_A" -len 0.01 
Sbend "BEND.DL.DIPN0" -len 0.2851351189 -angle 0.04133674544 -E1 0.02066837272 -E2 0.02066837272 -S1 [expr 0.9511964998*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*0.04133674544*0.04133674544/0.2851351189*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "DS2C_B" -len 0.01 
Sextupole "SEXT.DL.SNN2" -len 0.1 -S2 [expr -7.207522988235507/2.38*$refen] -refen $refen 
Drift "DS2C_A" -len 0.2846962 
Quadrupole "QUAD.QS1C" -len 0.4 -S1 [expr -1.209056*$refen] -refen $refen 
Bpm "BPM.DL_L.6" -len 0 
Drift "DS1C_B" -len 0.25 
Sextupole "SEXT.DL.SNN1" -len 0.1 -S2 [expr 12.85540299483511/2.38*$refen] -refen $refen 
Drift "DS1C_A" -len 0.1850809 
Sbend "BEND.DL.DIPOLE0" -len 2.56621607 -angle -0.372030709 -E1 -0.1860153545 -E2 -0.1860153545 -S1 [expr 0.04695405543*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*-0.372030709*-0.372030709/2.56621607*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "D12" -len 0.1559115 
Quadrupole "QUAD.QI2" -len 0.4 -S1 [expr 0.824612*$refen] -refen $refen 
Bpm "BPM.DL_L.7" -len 0 
Drift "D11B" -len 0.0940885 
Sextupole "SEXT.DL.SNCH1" -len 0.1 -S2 [expr 4.124909873415538/2.38*$refen] -refen $refen 
Drift "D11A" -len 0.1 
Quadrupole "QUAD.QI1" -len 0.2 -S1 [expr -0.746512*$refen] -refen $refen 
Bpm "BPM.DL_L.8" -len 0 
Marker "MCELL_1" 
Quadrupole "QUAD.QI1" -len 0.2 -S1 [expr -0.746512*$refen] -refen $refen 
Bpm "BPM.DL_L.9" -len 0 
Drift "D11A" -len 0.1 
Sextupole "SEXT.DL.SNCH1" -len 0.1 -S2 [expr 4.124909873415538/2.38*$refen] -refen $refen 
Drift "D11B" -len 0.0940885 
Quadrupole "QUAD.QI2" -len 0.4 -S1 [expr 0.824612*$refen] -refen $refen 
Bpm "BPM.DL_L.10" -len 0 
Drift "D12" -len 0.1559115 
Sbend "BEND.DL.DIPOLE0" -len 2.56621607 -angle -0.372030709 -E1 -0.1860153545 -E2 -0.1860153545 -S1 [expr 0.04695405543*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*-0.372030709*-0.372030709/2.56621607*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "DS1C_A" -len 0.1850809 
Sextupole "SEXT.DL.SN1" -len 0.1 -S2 [expr 12.85540299483511/2.38*$refen] -refen $refen 
Drift "DS1C_B" -len 0.25 
Quadrupole "QUAD.QS1C" -len 0.4 -S1 [expr -1.209056*$refen] -refen $refen 
Bpm "BPM.DL_L.11" -len 0 
Drift "DS2C_A" -len 0.2846962 
Sextupole "SEXT.DL.SN2" -len 0.1 -S2 [expr -7.207522988235507/2.38*$refen] -refen $refen 
Drift "DS2C_B" -len 0.01 
Sbend "BEND.DL.DIPN0" -len 0.2851351189 -angle 0.04133674544 -E1 0.02066837272 -E2 0.02066837272 -S1 [expr 0.9511964998*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*0.04133674544*0.04133674544/0.2851351189*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "DS3C_A" -len 0.01 
Sextupole "SEXT.DL.SN3" -len 0.1 -S2 [expr 2.510029401343759/2.38*$refen] -refen $refen 
Drift "DS3C_B" -len 0.1702229 
Quadrupole "QUAD.QS2C" -len 0.2 -S1 [expr -0.1666676*$refen] -refen $refen 
Bpm "BPM.DL_L.12" -len 0 
Quadrupole "QUAD.QS2C" -len 0.2 -S1 [expr -0.1666676*$refen] -refen $refen 
Bpm "BPM.DL_L.13" -len 0 
Drift "DS3C_B" -len 0.1702229 
Sextupole "SEXT.DL.SNN3" -len 0.1 -S2 [expr 2.510029401343759/2.38*$refen] -refen $refen 
Drift "DS3C_A" -len 0.01 
Sbend "BEND.DL.DIPN0" -len 0.2851351189 -angle 0.04133674544 -E1 0.02066837272 -E2 0.02066837272 -S1 [expr 0.9511964998*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*0.04133674544*0.04133674544/0.2851351189*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "DS2C_B" -len 0.01 
Sextupole "SEXT.DL.SNN2" -len 0.1 -S2 [expr -7.207522988235507/2.38*$refen] -refen $refen 
Drift "DS2C_A" -len 0.2846962 
Quadrupole "QUAD.QS1C" -len 0.4 -S1 [expr -1.209056*$refen] -refen $refen 
Bpm "BPM.DL_L.14" -len 0 
Drift "DS1C_B" -len 0.25 
Sextupole "SEXT.DL.SNN1" -len 0.1 -S2 [expr 12.85540299483511/2.38*$refen] -refen $refen 
Drift "DS1C_A" -len 0.1850809 
Sbend "BEND.DL.DIPOLE0" -len 2.56621607 -angle -0.372030709 -E1 -0.1860153545 -E2 -0.1860153545 -S1 [expr 0.04695405543*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*-0.372030709*-0.372030709/2.56621607*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "D12" -len 0.1559115 
Quadrupole "QUAD.QI2" -len 0.4 -S1 [expr 0.824612*$refen] -refen $refen 
Bpm "BPM.DL_L.15" -len 0 
Drift "D11B" -len 0.0940885 
Sextupole "SEXT.DL.SNCH1" -len 0.1 -S2 [expr 4.124909873415538/2.38*$refen] -refen $refen 
Drift "D11A" -len 0.1 
Quadrupole "QUAD.QI1" -len 0.2 -S1 [expr -0.746512*$refen] -refen $refen 
Bpm "BPM.DL_L.16" -len 0 
Marker "MCELL_2" 
Quadrupole "QUAD.QI1" -len 0.2 -S1 [expr -0.746512*$refen] -refen $refen 
Bpm "BPM.DL_L.17" -len 0 
Drift "D11A" -len 0.1 
Sextupole "SEXT.DL.SNCH1" -len 0.1 -S2 [expr 4.124909873415538/2.38*$refen] -refen $refen 
Drift "D11B" -len 0.0940885 
Quadrupole "QUAD.QI2" -len 0.4 -S1 [expr 0.824612*$refen] -refen $refen 
Bpm "BPM.DL_L.18" -len 0 
Drift "D12" -len 0.1559115 
Sbend "BEND.DL.DIPOLE0" -len 2.56621607 -angle -0.372030709 -E1 -0.1860153545 -E2 -0.1860153545 -S1 [expr 0.04695405543*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*-0.372030709*-0.372030709/2.56621607*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "DS1C_A" -len 0.1850809 
Sextupole "SEXT.DL.SN1" -len 0.1 -S2 [expr 12.85540299483511/2.38*$refen] -refen $refen 
Drift "DS1C_B" -len 0.25 
Quadrupole "QUAD.QS1C" -len 0.4 -S1 [expr -1.209056*$refen] -refen $refen 
Bpm "BPM.DL_L.19" -len 0 
Drift "DS2C_A" -len 0.2846962 
Sextupole "SEXT.DL.SN2" -len 0.1 -S2 [expr -7.207522988235507/2.38*$refen] -refen $refen 
Drift "DS2C_B" -len 0.01 
Sbend "BEND.DL.DIPN0" -len 0.2851351189 -angle 0.04133674544 -E1 0.02066837272 -E2 0.02066837272 -S1 [expr 0.9511964998*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*0.04133674544*0.04133674544/0.2851351189*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "DS3C_A" -len 0.01 
Sextupole "SEXT.DL.SN3" -len 0.1 -S2 [expr 2.510029401343759/2.38*$refen] -refen $refen 
Drift "DS3C_B" -len 0.1702229 
Quadrupole "QUAD.QS2C" -len 0.2 -S1 [expr -0.1666676*$refen] -refen $refen 
Bpm "BPM.DL_L.20" -len 0 
Quadrupole "QUAD.QS2C" -len 0.2 -S1 [expr -0.1666676*$refen] -refen $refen 
Bpm "BPM.DL_L.21" -len 0 
Drift "DS3C_B" -len 0.1702229 
Sextupole "SEXT.DL.SNN3" -len 0.1 -S2 [expr 2.510029401343759/2.38*$refen] -refen $refen 
Drift "DS3C_A" -len 0.01 
Sbend "BEND.DL.DIPN0" -len 0.2851351189 -angle 0.04133674544 -E1 0.02066837272 -E2 0.02066837272 -S1 [expr 0.9511964998*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*0.04133674544*0.04133674544/0.2851351189*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "DS2C_B" -len 0.01 
Sextupole "SEXT.DL.SNN2" -len 0.1 -S2 [expr -7.207522988235507/2.38*$refen] -refen $refen 
Drift "DS2C_A" -len 0.2846962 
Quadrupole "QUAD.QS1C" -len 0.4 -S1 [expr -1.209056*$refen] -refen $refen 
Bpm "BPM.DL_L.22" -len 0 
Drift "DS1C_B" -len 0.25 
Sextupole "SEXT.DL.SNN1" -len 0.1 -S2 [expr 12.85540299483511/2.38*$refen] -refen $refen 
Drift "DS1C_A" -len 0.1850809 
Sbend "BEND.DL.DIPOLE0" -len 2.56621607 -angle -0.372030709 -E1 -0.1860153545 -E2 -0.1860153545 -S1 [expr 0.04695405543*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*-0.372030709*-0.372030709/2.56621607*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "D12" -len 0.1559115 
Quadrupole "QUAD.QI2" -len 0.4 -S1 [expr 0.824612*$refen] -refen $refen 
Bpm "BPM.DL_L.23" -len 0 
Drift "D11B" -len 0.0940885 
Sextupole "SEXT.DL.SNCH1" -len 0.1 -S2 [expr 4.124909873415538/2.38*$refen] -refen $refen 
Drift "D11A" -len 0.1 
Quadrupole "QUAD.QI1" -len 0.2 -S1 [expr -0.746512*$refen] -refen $refen 
Bpm "BPM.DL_L.24" -len 0 
Marker "MCELL_3" 
Quadrupole "QUAD.QI1" -len 0.2 -S1 [expr -0.746512*$refen] -refen $refen 
Bpm "BPM.DL_L.25" -len 0 
Drift "D11A" -len 0.1 
Sextupole "SEXT.DL.SXCH1" -len 0.1 -S2 [expr -4.124909873415538/2.38*$refen] -refen $refen 
Drift "D11B" -len 0.0940885 
Quadrupole "QUAD.QI2" -len 0.4 -S1 [expr 0.824612*$refen] -refen $refen 
Bpm "BPM.DL_L.26" -len 0 
Drift "D12" -len 0.1559115 
Sbend "BEND.DL.DIPOLE" -len 2.56621607 -angle 0.372030709 -E1 0.1860153545 -E2 0.1860153545 -S1 [expr 0.04695405543*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*0.372030709*0.372030709/2.56621607*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "DS1C_A" -len 0.1850809 
Sextupole "SEXT.DL.SX1" -len 0.1 -S2 [expr -12.85540299483511/2.38*$refen] -refen $refen 
Drift "DS1C_B" -len 0.25 
Quadrupole "QUAD.QS1C" -len 0.4 -S1 [expr -1.209056*$refen] -refen $refen 
Bpm "BPM.DL_L.27" -len 0 
Drift "DS2C_A" -len 0.2846962 
Sextupole "SEXT.DL.SX2" -len 0.1 -S2 [expr 7.207522988235507/2.38*$refen] -refen $refen 
Drift "DS2C_B" -len 0.01 
Sbend "BEND.DL.DIPN" -len 0.2851351189 -angle -0.04133674544 -E1 -0.02066837272 -E2 -0.02066837272 -S1 [expr 0.9511964998*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*-0.04133674544*-0.04133674544/0.2851351189*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "DS3C_A" -len 0.01 
Sextupole "SEXT.DL.SX3" -len 0.1 -S2 [expr -2.510029401343759/2.38*$refen] -refen $refen 
Drift "DS3C_B" -len 0.1702229 
Quadrupole "QUAD.QS2C" -len 0.2 -S1 [expr -0.1666676*$refen] -refen $refen 
Bpm "BPM.DL_L.28" -len 0 
Quadrupole "QUAD.QS2C" -len 0.2 -S1 [expr -0.1666676*$refen] -refen $refen 
Bpm "BPM.DL_L.29" -len 0 
Drift "DS3C_B" -len 0.1702229 
Sextupole "SEXT.DL.SXX3" -len 0.1 -S2 [expr -2.510029401343759/2.38*$refen] -refen $refen 
Drift "DS3C_A" -len 0.01 
Sbend "BEND.DL.DIPN" -len 0.2851351189 -angle -0.04133674544 -E1 -0.02066837272 -E2 -0.02066837272 -S1 [expr 0.9511964998*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*-0.04133674544*-0.04133674544/0.2851351189*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "DS2C_B" -len 0.01 
Sextupole "SEXT.DL.SXX2" -len 0.1 -S2 [expr 7.207522988235507/2.38*$refen] -refen $refen 
Drift "DS2C_A" -len 0.2846962 
Quadrupole "QUAD.QS1C" -len 0.4 -S1 [expr -1.209056*$refen] -refen $refen 
Bpm "BPM.DL_L.30" -len 0 
Drift "DS1C_B" -len 0.25 
Sextupole "SEXT.DL.SXX1" -len 0.1 -S2 [expr -12.85540299483511/2.38*$refen] -refen $refen 
Drift "DS1C_A" -len 0.1850809 
Sbend "BEND.DL.DIPOLE" -len 2.56621607 -angle 0.372030709 -E1 0.1860153545 -E2 0.1860153545 -S1 [expr 0.04695405543*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*0.372030709*0.372030709/2.56621607*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "D12" -len 0.1559115 
Quadrupole "QUAD.QI2" -len 0.4 -S1 [expr 0.824612*$refen] -refen $refen 
Bpm "BPM.DL_L.31" -len 0 
Drift "D11B" -len 0.0940885 
Sextupole "SEXT.DL.SXCH1" -len 0.1 -S2 [expr -4.124909873415538/2.38*$refen] -refen $refen 
Drift "D11A" -len 0.1 
Quadrupole "QUAD.QI1" -len 0.2 -S1 [expr -0.746512*$refen] -refen $refen 
Bpm "BPM.DL_L.32" -len 0 
Marker "MCELL_4" 
Quadrupole "QUAD.QI1" -len 0.2 -S1 [expr -0.746512*$refen] -refen $refen 
Bpm "BPM.DL_L.33" -len 0 
Drift "D11A" -len 0.1 
Sextupole "SEXT.DL.SXCH1" -len 0.1 -S2 [expr -4.124909873415538/2.38*$refen] -refen $refen 
Drift "D11B" -len 0.0940885 
Quadrupole "QUAD.QI2" -len 0.4 -S1 [expr 0.824612*$refen] -refen $refen 
Bpm "BPM.DL_L.34" -len 0 
Drift "D12" -len 0.1559115 
Sbend "BEND.DL.DIPOLE" -len 2.56621607 -angle 0.372030709 -E1 0.1860153545 -E2 0.1860153545 -S1 [expr 0.04695405543*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*0.372030709*0.372030709/2.56621607*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "DS1C_A" -len 0.1850809 
Sextupole "SEXT.DL.SX1" -len 0.1 -S2 [expr -12.85540299483511/2.38*$refen] -refen $refen 
Drift "DS1C_B" -len 0.25 
Quadrupole "QUAD.QS1C" -len 0.4 -S1 [expr -1.209056*$refen] -refen $refen 
Bpm "BPM.DL_L.35" -len 0 
Drift "DS2C_A" -len 0.2846962 
Sextupole "SEXT.DL.SX2" -len 0.1 -S2 [expr 7.207522988235507/2.38*$refen] -refen $refen 
Drift "DS2C_B" -len 0.01 
Sbend "BEND.DL.DIPN" -len 0.2851351189 -angle -0.04133674544 -E1 -0.02066837272 -E2 -0.02066837272 -S1 [expr 0.9511964998*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*-0.04133674544*-0.04133674544/0.2851351189*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "DS3C_A" -len 0.01 
Sextupole "SEXT.DL.SX3" -len 0.1 -S2 [expr -2.510029401343759/2.38*$refen] -refen $refen 
Drift "DS3C_B" -len 0.1702229 
Quadrupole "QUAD.QS2C" -len 0.2 -S1 [expr -0.1666676*$refen] -refen $refen 
Bpm "BPM.DL_L.36" -len 0 
Quadrupole "QUAD.QS2C" -len 0.2 -S1 [expr -0.1666676*$refen] -refen $refen 
Bpm "BPM.DL_L.37" -len 0 
Drift "DS3C_B" -len 0.1702229 
Sextupole "SEXT.DL.SXX3" -len 0.1 -S2 [expr -2.510029401343759/2.38*$refen] -refen $refen 
Drift "DS3C_A" -len 0.01 
Sbend "BEND.DL.DIPN" -len 0.2851351189 -angle -0.04133674544 -E1 -0.02066837272 -E2 -0.02066837272 -S1 [expr 0.9511964998*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*-0.04133674544*-0.04133674544/0.2851351189*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "DS2C_B" -len 0.01 
Sextupole "SEXT.DL.SXX2" -len 0.1 -S2 [expr 7.207522988235507/2.38*$refen] -refen $refen 
Drift "DS2C_A" -len 0.2846962 
Quadrupole "QUAD.QS1C" -len 0.4 -S1 [expr -1.209056*$refen] -refen $refen 
Bpm "BPM.DL_L.38" -len 0 
Drift "DS1C_B" -len 0.25 
Sextupole "SEXT.DL.SXX1" -len 0.1 -S2 [expr -12.85540299483511/2.38*$refen] -refen $refen 
Drift "DS1C_A" -len 0.1850809 
Sbend "BEND.DL.DIPOLE" -len 2.56621607 -angle 0.372030709 -E1 0.1860153545 -E2 0.1860153545 -S1 [expr 0.04695405543*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*0.372030709*0.372030709/2.56621607*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "D12" -len 0.1559115 
Quadrupole "QUAD.QI2" -len 0.4 -S1 [expr 0.824612*$refen] -refen $refen 
Bpm "BPM.DL_L.39" -len 0 
Drift "D11B" -len 0.0940885 
Sextupole "SEXT.DL.SXCH1" -len 0.1 -S2 [expr -4.124909873415538/2.38*$refen] -refen $refen 
Drift "D11A" -len 0.1 
Quadrupole "QUAD.QI1" -len 0.2 -S1 [expr -0.746512*$refen] -refen $refen 
Bpm "BPM.DL_L.40" -len 0 
Marker "MCELL_5" 
Quadrupole "QUAD.QI1" -len 0.2 -S1 [expr -0.746512*$refen] -refen $refen 
Bpm "BPM.DL_L.41" -len 0 
Drift "D11A" -len 0.1 
Sextupole "SEXT.DL.SXCH1" -len 0.1 -S2 [expr -4.124909873415538/2.38*$refen] -refen $refen 
Drift "D11B" -len 0.0940885 
Quadrupole "QUAD.QI2" -len 0.4 -S1 [expr 0.824612*$refen] -refen $refen 
Bpm "BPM.DL_L.42" -len 0 
Drift "D12" -len 0.1559115 
Sbend "BEND.DL.DIPOLE" -len 2.56621607 -angle 0.372030709 -E1 0.1860153545 -E2 0.1860153545 -S1 [expr 0.04695405543*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*0.372030709*0.372030709/2.56621607*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "DS1C_A" -len 0.1850809 
Sextupole "SEXT.DL.SX1" -len 0.1 -S2 [expr -12.85540299483511/2.38*$refen] -refen $refen 
Drift "DS1C_B" -len 0.25 
Quadrupole "QUAD.QS1C" -len 0.4 -S1 [expr -1.209056*$refen] -refen $refen 
Bpm "BPM.DL_L.43" -len 0 
Drift "DS2C_A" -len 0.2846962 
Sextupole "SEXT.DL.SX2" -len 0.1 -S2 [expr 7.207522988235507/2.38*$refen] -refen $refen 
Drift "DS2C_B" -len 0.01 
Sbend "BEND.DL.DIPN" -len 0.2851351189 -angle -0.04133674544 -E1 -0.02066837272 -E2 -0.02066837272 -S1 [expr 0.9511964998*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*-0.04133674544*-0.04133674544/0.2851351189*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "DS3C_A" -len 0.01 
Sextupole "SEXT.DL.SX3" -len 0.1 -S2 [expr -2.510029401343759/2.38*$refen] -refen $refen 
Drift "DS3C_B" -len 0.1702229 
Quadrupole "QUAD.QS2C" -len 0.2 -S1 [expr -0.1666676*$refen] -refen $refen 
Bpm "BPM.DL_L.44" -len 0 
Quadrupole "QUAD.QS2C" -len 0.2 -S1 [expr -0.1666676*$refen] -refen $refen 
Bpm "BPM.DL_L.45" -len 0 
Drift "DS3C_B" -len 0.1702229 
Sextupole "SEXT.DL.SXX3" -len 0.1 -S2 [expr -2.510029401343759/2.38*$refen] -refen $refen 
Drift "DS3C_A" -len 0.01 
Sbend "BEND.DL.DIPN" -len 0.2851351189 -angle -0.04133674544 -E1 -0.02066837272 -E2 -0.02066837272 -S1 [expr 0.9511964998*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*-0.04133674544*-0.04133674544/0.2851351189*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "DS2C_B" -len 0.01 
Sextupole "SEXT.DL.SXX2" -len 0.1 -S2 [expr 7.207522988235507/2.38*$refen] -refen $refen 
Drift "DS2C_A" -len 0.2846962 
Quadrupole "QUAD.QS1C" -len 0.4 -S1 [expr -1.209056*$refen] -refen $refen 
Bpm "BPM.DL_L.46" -len 0 
Drift "DS1C_B" -len 0.25 
Sextupole "SEXT.DL.SXX1" -len 0.1 -S2 [expr -12.85540299483511/2.38*$refen] -refen $refen 
Drift "DS1C_A" -len 0.1850809 
Sbend "BEND.DL.DIPOLE" -len 2.56621607 -angle 0.372030709 -E1 0.1860153545 -E2 0.1860153545 -S1 [expr 0.04695405543*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*0.372030709*0.372030709/2.56621607*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "D12" -len 0.1559115 
Quadrupole "QUAD.QI2" -len 0.4 -S1 [expr 0.824612*$refen] -refen $refen 
Bpm "BPM.DL_L.47" -len 0 
Drift "D11B" -len 0.0940885 
Sextupole "SEXT.DL.SXCH1" -len 0.1 -S2 [expr -4.124909873415538/2.38*$refen] -refen $refen 
Drift "D11A" -len 0.1 
Quadrupole "QUAD.QI1" -len 0.2 -S1 [expr -0.746512*$refen] -refen $refen 
Bpm "BPM.DL_L.48" -len 0 
Marker "MCELL_6" 
Marker "DL.MIDDLE" 
Quadrupole "QUAD.QI1" -len 0.2 -S1 [expr -0.746512*$refen] -refen $refen 
Bpm "BPM.DL_L.49" -len 0 
Drift "D11A" -len 0.1 
Sextupole "SEXT.DL.SXCH1" -len 0.1 -S2 [expr -4.124909873415538/2.38*$refen] -refen $refen 
Drift "D11B" -len 0.0940885 
Quadrupole "QUAD.QI2" -len 0.4 -S1 [expr 0.824612*$refen] -refen $refen 
Bpm "BPM.DL_L.50" -len 0 
Drift "D12" -len 0.1559115 
Sbend "BEND.DL.DIPOLE" -len 2.56621607 -angle 0.372030709 -E1 0.1860153545 -E2 0.1860153545 -S1 [expr 0.04695405543*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*0.372030709*0.372030709/2.56621607*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "DS1C_A" -len 0.1850809 
Sextupole "SEXT.DL.SX1" -len 0.1 -S2 [expr -12.85540299483511/2.38*$refen] -refen $refen 
Drift "DS1C_B" -len 0.25 
Quadrupole "QUAD.QS1C" -len 0.4 -S1 [expr -1.209056*$refen] -refen $refen 
Bpm "BPM.DL_L.51" -len 0 
Drift "DS2C_A" -len 0.2846962 
Sextupole "SEXT.DL.SX2" -len 0.1 -S2 [expr 7.207522988235507/2.38*$refen] -refen $refen 
Drift "DS2C_B" -len 0.01 
Sbend "BEND.DL.DIPN" -len 0.2851351189 -angle -0.04133674544 -E1 -0.02066837272 -E2 -0.02066837272 -S1 [expr 0.9511964998*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*-0.04133674544*-0.04133674544/0.2851351189*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "DS3C_A" -len 0.01 
Sextupole "SEXT.DL.SX3" -len 0.1 -S2 [expr -2.510029401343759/2.38*$refen] -refen $refen 
Drift "DS3C_B" -len 0.1702229 
Quadrupole "QUAD.QS2C" -len 0.2 -S1 [expr -0.1666676*$refen] -refen $refen 
Bpm "BPM.DL_L.52" -len 0 
Quadrupole "QUAD.QS2C" -len 0.2 -S1 [expr -0.1666676*$refen] -refen $refen 
Bpm "BPM.DL_L.53" -len 0 
Drift "DS3C_B" -len 0.1702229 
Sextupole "SEXT.DL.SXX3" -len 0.1 -S2 [expr -2.510029401343759/2.38*$refen] -refen $refen 
Drift "DS3C_A" -len 0.01 
Sbend "BEND.DL.DIPN" -len 0.2851351189 -angle -0.04133674544 -E1 -0.02066837272 -E2 -0.02066837272 -S1 [expr 0.9511964998*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*-0.04133674544*-0.04133674544/0.2851351189*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "DS2C_B" -len 0.01 
Sextupole "SEXT.DL.SXX2" -len 0.1 -S2 [expr 7.207522988235507/2.38*$refen] -refen $refen 
Drift "DS2C_A" -len 0.2846962 
Quadrupole "QUAD.QS1C" -len 0.4 -S1 [expr -1.209056*$refen] -refen $refen 
Bpm "BPM.DL_L.54" -len 0 
Drift "DS1C_B" -len 0.25 
Sextupole "SEXT.DL.SXX1" -len 0.1 -S2 [expr -12.85540299483511/2.38*$refen] -refen $refen 
Drift "DS1C_A" -len 0.1850809 
Sbend "BEND.DL.DIPOLE" -len 2.56621607 -angle 0.372030709 -E1 0.1860153545 -E2 0.1860153545 -S1 [expr 0.04695405543*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*0.372030709*0.372030709/2.56621607*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "D12" -len 0.1559115 
Quadrupole "QUAD.QI2" -len 0.4 -S1 [expr 0.824612*$refen] -refen $refen 
Bpm "BPM.DL_L.55" -len 0 
Drift "D11B" -len 0.0940885 
Sextupole "SEXT.DL.SXCH1" -len 0.1 -S2 [expr -4.124909873415538/2.38*$refen] -refen $refen 
Drift "D11A" -len 0.1 
Quadrupole "QUAD.QI1" -len 0.2 -S1 [expr -0.746512*$refen] -refen $refen 
Bpm "BPM.DL_L.56" -len 0 
Marker "MCELL_7" 
Quadrupole "QUAD.QI1" -len 0.2 -S1 [expr -0.746512*$refen] -refen $refen 
Bpm "BPM.DL_L.57" -len 0 
Drift "D11A" -len 0.1 
Sextupole "SEXT.DL.SXCH1" -len 0.1 -S2 [expr -4.124909873415538/2.38*$refen] -refen $refen 
Drift "D11B" -len 0.0940885 
Quadrupole "QUAD.QI2" -len 0.4 -S1 [expr 0.824612*$refen] -refen $refen 
Bpm "BPM.DL_L.58" -len 0 
Drift "D12" -len 0.1559115 
Sbend "BEND.DL.DIPOLE" -len 2.56621607 -angle 0.372030709 -E1 0.1860153545 -E2 0.1860153545 -S1 [expr 0.04695405543*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*0.372030709*0.372030709/2.56621607*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "DS1C_A" -len 0.1850809 
Sextupole "SEXT.DL.SX1" -len 0.1 -S2 [expr -12.85540299483511/2.38*$refen] -refen $refen 
Drift "DS1C_B" -len 0.25 
Quadrupole "QUAD.QS1C" -len 0.4 -S1 [expr -1.209056*$refen] -refen $refen 
Bpm "BPM.DL_L.59" -len 0 
Drift "DS2C_A" -len 0.2846962 
Sextupole "SEXT.DL.SX2" -len 0.1 -S2 [expr 7.207522988235507/2.38*$refen] -refen $refen 
Drift "DS2C_B" -len 0.01 
Sbend "BEND.DL.DIPN" -len 0.2851351189 -angle -0.04133674544 -E1 -0.02066837272 -E2 -0.02066837272 -S1 [expr 0.9511964998*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*-0.04133674544*-0.04133674544/0.2851351189*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "DS3C_A" -len 0.01 
Sextupole "SEXT.DL.SX3" -len 0.1 -S2 [expr -2.510029401343759/2.38*$refen] -refen $refen 
Drift "DS3C_B" -len 0.1702229 
Quadrupole "QUAD.QS2C" -len 0.2 -S1 [expr -0.1666676*$refen] -refen $refen 
Bpm "BPM.DL_L.60" -len 0 
Quadrupole "QUAD.QS2C" -len 0.2 -S1 [expr -0.1666676*$refen] -refen $refen 
Bpm "BPM.DL_L.61" -len 0 
Drift "DS3C_B" -len 0.1702229 
Sextupole "SEXT.DL.SXX3" -len 0.1 -S2 [expr -2.510029401343759/2.38*$refen] -refen $refen 
Drift "DS3C_A" -len 0.01 
Sbend "BEND.DL.DIPN" -len 0.2851351189 -angle -0.04133674544 -E1 -0.02066837272 -E2 -0.02066837272 -S1 [expr 0.9511964998*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*-0.04133674544*-0.04133674544/0.2851351189*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "DS2C_B" -len 0.01 
Sextupole "SEXT.DL.SXX2" -len 0.1 -S2 [expr 7.207522988235507/2.38*$refen] -refen $refen 
Drift "DS2C_A" -len 0.2846962 
Quadrupole "QUAD.QS1C" -len 0.4 -S1 [expr -1.209056*$refen] -refen $refen 
Bpm "BPM.DL_L.62" -len 0 
Drift "DS1C_B" -len 0.25 
Sextupole "SEXT.DL.SXX1" -len 0.1 -S2 [expr -12.85540299483511/2.38*$refen] -refen $refen 
Drift "DS1C_A" -len 0.1850809 
Sbend "BEND.DL.DIPOLE" -len 2.56621607 -angle 0.372030709 -E1 0.1860153545 -E2 0.1860153545 -S1 [expr 0.04695405543*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*0.372030709*0.372030709/2.56621607*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "D12" -len 0.1559115 
Quadrupole "QUAD.QI2" -len 0.4 -S1 [expr 0.824612*$refen] -refen $refen 
Bpm "BPM.DL_L.63" -len 0 
Drift "D11B" -len 0.0940885 
Sextupole "SEXT.DL.SXCH1" -len 0.1 -S2 [expr -4.124909873415538/2.38*$refen] -refen $refen 
Drift "D11A" -len 0.1 
Quadrupole "QUAD.QI1" -len 0.2 -S1 [expr -0.746512*$refen] -refen $refen 
Bpm "BPM.DL_L.64" -len 0 
Marker "MCELL_8" 
Quadrupole "QUAD.QI1" -len 0.2 -S1 [expr -0.746512*$refen] -refen $refen 
Bpm "BPM.DL_L.65" -len 0 
Drift "D11A" -len 0.1 
Sextupole "SEXT.DL.SXCH1" -len 0.1 -S2 [expr -4.124909873415538/2.38*$refen] -refen $refen 
Drift "D11B" -len 0.0940885 
Quadrupole "QUAD.QI2" -len 0.4 -S1 [expr 0.824612*$refen] -refen $refen 
Bpm "BPM.DL_L.66" -len 0 
Drift "D12" -len 0.1559115 
Sbend "BEND.DL.DIPOLE" -len 2.56621607 -angle 0.372030709 -E1 0.1860153545 -E2 0.1860153545 -S1 [expr 0.04695405543*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*0.372030709*0.372030709/2.56621607*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "DS1C_A" -len 0.1850809 
Sextupole "SEXT.DL.SX1" -len 0.1 -S2 [expr -12.85540299483511/2.38*$refen] -refen $refen 
Drift "DS1C_B" -len 0.25 
Quadrupole "QUAD.QS1C" -len 0.4 -S1 [expr -1.209056*$refen] -refen $refen 
Bpm "BPM.DL_L.67" -len 0 
Drift "DS2C_A" -len 0.2846962 
Sextupole "SEXT.DL.SX2" -len 0.1 -S2 [expr 7.207522988235507/2.38*$refen] -refen $refen 
Drift "DS2C_B" -len 0.01 
Sbend "BEND.DL.DIPN" -len 0.2851351189 -angle -0.04133674544 -E1 -0.02066837272 -E2 -0.02066837272 -S1 [expr 0.9511964998*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*-0.04133674544*-0.04133674544/0.2851351189*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "DS3C_A" -len 0.01 
Sextupole "SEXT.DL.SX3" -len 0.1 -S2 [expr -2.510029401343759/2.38*$refen] -refen $refen 
Drift "DS3C_B" -len 0.1702229 
Quadrupole "QUAD.QS2C" -len 0.2 -S1 [expr -0.1666676*$refen] -refen $refen 
Bpm "BPM.DL_L.68" -len 0 
Quadrupole "QUAD.QS2C" -len 0.2 -S1 [expr -0.1666676*$refen] -refen $refen 
Bpm "BPM.DL_L.69" -len 0 
Drift "DS3C_B" -len 0.1702229 
Sextupole "SEXT.DL.SXX3" -len 0.1 -S2 [expr -2.510029401343759/2.38*$refen] -refen $refen 
Drift "DS3C_A" -len 0.01 
Sbend "BEND.DL.DIPN" -len 0.2851351189 -angle -0.04133674544 -E1 -0.02066837272 -E2 -0.02066837272 -S1 [expr 0.9511964998*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*-0.04133674544*-0.04133674544/0.2851351189*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "DS2C_B" -len 0.01 
Sextupole "SEXT.DL.SXX2" -len 0.1 -S2 [expr 7.207522988235507/2.38*$refen] -refen $refen 
Drift "DS2C_A" -len 0.2846962 
Quadrupole "QUAD.QS1C" -len 0.4 -S1 [expr -1.209056*$refen] -refen $refen 
Bpm "BPM.DL_L.70" -len 0 
Drift "DS1C_B" -len 0.25 
Sextupole "SEXT.DL.SXX1" -len 0.1 -S2 [expr -12.85540299483511/2.38*$refen] -refen $refen 
Drift "DS1C_A" -len 0.1850809 
Sbend "BEND.DL.DIPOLE" -len 2.56621607 -angle 0.372030709 -E1 0.1860153545 -E2 0.1860153545 -S1 [expr 0.04695405543*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*0.372030709*0.372030709/2.56621607*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "D12" -len 0.1559115 
Quadrupole "QUAD.QI2" -len 0.4 -S1 [expr 0.824612*$refen] -refen $refen 
Bpm "BPM.DL_L.71" -len 0 
Drift "D11B" -len 0.0940885 
Sextupole "SEXT.DL.SXCH1" -len 0.1 -S2 [expr -4.124909873415538/2.38*$refen] -refen $refen 
Drift "D11A" -len 0.1 
Quadrupole "QUAD.QI1" -len 0.2 -S1 [expr -0.746512*$refen] -refen $refen 
Bpm "BPM.DL_L.72" -len 0 
Marker "MCELL_9" 
Quadrupole "QUAD.QI1" -len 0.2 -S1 [expr -0.746512*$refen] -refen $refen 
Bpm "BPM.DL_L.73" -len 0 
Drift "D11A" -len 0.1 
Sextupole "SEXT.DL.SNCH1" -len 0.1 -S2 [expr 4.124909873415538/2.38*$refen] -refen $refen 
Drift "D11B" -len 0.0940885 
Quadrupole "QUAD.QI2" -len 0.4 -S1 [expr 0.824612*$refen] -refen $refen 
Bpm "BPM.DL_L.74" -len 0 
Drift "D12" -len 0.1559115 
Sbend "BEND.DL.DIPOLE0" -len 2.56621607 -angle -0.372030709 -E1 -0.1860153545 -E2 -0.1860153545 -S1 [expr 0.04695405543*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*-0.372030709*-0.372030709/2.56621607*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "DS1C_A" -len 0.1850809 
Sextupole "SEXT.DL.SN1" -len 0.1 -S2 [expr 12.85540299483511/2.38*$refen] -refen $refen 
Drift "DS1C_B" -len 0.25 
Quadrupole "QUAD.QS1C" -len 0.4 -S1 [expr -1.209056*$refen] -refen $refen 
Bpm "BPM.DL_L.75" -len 0 
Drift "DS2C_A" -len 0.2846962 
Sextupole "SEXT.DL.SN2" -len 0.1 -S2 [expr -7.207522988235507/2.38*$refen] -refen $refen 
Drift "DS2C_B" -len 0.01 
Sbend "BEND.DL.DIPN0" -len 0.2851351189 -angle 0.04133674544 -E1 0.02066837272 -E2 0.02066837272 -S1 [expr 0.9511964998*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*0.04133674544*0.04133674544/0.2851351189*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "DS3C_A" -len 0.01 
Sextupole "SEXT.DL.SN3" -len 0.1 -S2 [expr 2.510029401343759/2.38*$refen] -refen $refen 
Drift "DS3C_B" -len 0.1702229 
Quadrupole "QUAD.QS2C" -len 0.2 -S1 [expr -0.1666676*$refen] -refen $refen 
Bpm "BPM.DL_L.76" -len 0 
Quadrupole "QUAD.QS2C" -len 0.2 -S1 [expr -0.1666676*$refen] -refen $refen 
Bpm "BPM.DL_L.77" -len 0 
Drift "DS3C_B" -len 0.1702229 
Sextupole "SEXT.DL.SNN3" -len 0.1 -S2 [expr 2.510029401343759/2.38*$refen] -refen $refen 
Drift "DS3C_A" -len 0.01 
Sbend "BEND.DL.DIPN0" -len 0.2851351189 -angle 0.04133674544 -E1 0.02066837272 -E2 0.02066837272 -S1 [expr 0.9511964998*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*0.04133674544*0.04133674544/0.2851351189*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "DS2C_B" -len 0.01 
Sextupole "SEXT.DL.SNN2" -len 0.1 -S2 [expr -7.207522988235507/2.38*$refen] -refen $refen 
Drift "DS2C_A" -len 0.2846962 
Quadrupole "QUAD.QS1C" -len 0.4 -S1 [expr -1.209056*$refen] -refen $refen 
Bpm "BPM.DL_L.78" -len 0 
Drift "DS1C_B" -len 0.25 
Sextupole "SEXT.DL.SNN1" -len 0.1 -S2 [expr 12.85540299483511/2.38*$refen] -refen $refen 
Drift "DS1C_A" -len 0.1850809 
Sbend "BEND.DL.DIPOLE0" -len 2.56621607 -angle -0.372030709 -E1 -0.1860153545 -E2 -0.1860153545 -S1 [expr 0.04695405543*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*-0.372030709*-0.372030709/2.56621607*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "D12" -len 0.1559115 
Quadrupole "QUAD.QI2" -len 0.4 -S1 [expr 0.824612*$refen] -refen $refen 
Bpm "BPM.DL_L.79" -len 0 
Drift "D11B" -len 0.0940885 
Sextupole "SEXT.DL.SNCH1" -len 0.1 -S2 [expr 4.124909873415538/2.38*$refen] -refen $refen 
Drift "D11A" -len 0.1 
Quadrupole "QUAD.QI1" -len 0.2 -S1 [expr -0.746512*$refen] -refen $refen 
Bpm "BPM.DL_L.80" -len 0 
Marker "MCELL_10" 
Quadrupole "QUAD.QI1" -len 0.2 -S1 [expr -0.746512*$refen] -refen $refen 
Bpm "BPM.DL_L.81" -len 0 
Drift "D11A" -len 0.1 
Sextupole "SEXT.DL.SNCH1" -len 0.1 -S2 [expr 4.124909873415538/2.38*$refen] -refen $refen 
Drift "D11B" -len 0.0940885 
Quadrupole "QUAD.QI2" -len 0.4 -S1 [expr 0.824612*$refen] -refen $refen 
Bpm "BPM.DL_L.82" -len 0 
Drift "D12" -len 0.1559115 
Sbend "BEND.DL.DIPOLE0" -len 2.56621607 -angle -0.372030709 -E1 -0.1860153545 -E2 -0.1860153545 -S1 [expr 0.04695405543*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*-0.372030709*-0.372030709/2.56621607*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "DS1C_A" -len 0.1850809 
Sextupole "SEXT.DL.SN1" -len 0.1 -S2 [expr 12.85540299483511/2.38*$refen] -refen $refen 
Drift "DS1C_B" -len 0.25 
Quadrupole "QUAD.QS1C" -len 0.4 -S1 [expr -1.209056*$refen] -refen $refen 
Bpm "BPM.DL_L.83" -len 0 
Drift "DS2C_A" -len 0.2846962 
Sextupole "SEXT.DL.SN2" -len 0.1 -S2 [expr -7.207522988235507/2.38*$refen] -refen $refen 
Drift "DS2C_B" -len 0.01 
Sbend "BEND.DL.DIPN0" -len 0.2851351189 -angle 0.04133674544 -E1 0.02066837272 -E2 0.02066837272 -S1 [expr 0.9511964998*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*0.04133674544*0.04133674544/0.2851351189*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "DS3C_A" -len 0.01 
Sextupole "SEXT.DL.SN3" -len 0.1 -S2 [expr 2.510029401343759/2.38*$refen] -refen $refen 
Drift "DS3C_B" -len 0.1702229 
Quadrupole "QUAD.QS2C" -len 0.2 -S1 [expr -0.1666676*$refen] -refen $refen 
Bpm "BPM.DL_L.84" -len 0 
Quadrupole "QUAD.QS2C" -len 0.2 -S1 [expr -0.1666676*$refen] -refen $refen 
Bpm "BPM.DL_L.85" -len 0 
Drift "DS3C_B" -len 0.1702229 
Sextupole "SEXT.DL.SNN3" -len 0.1 -S2 [expr 2.510029401343759/2.38*$refen] -refen $refen 
Drift "DS3C_A" -len 0.01 
Sbend "BEND.DL.DIPN0" -len 0.2851351189 -angle 0.04133674544 -E1 0.02066837272 -E2 0.02066837272 -S1 [expr 0.9511964998*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*0.04133674544*0.04133674544/0.2851351189*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "DS2C_B" -len 0.01 
Sextupole "SEXT.DL.SNN2" -len 0.1 -S2 [expr -7.207522988235507/2.38*$refen] -refen $refen 
Drift "DS2C_A" -len 0.2846962 
Quadrupole "QUAD.QS1C" -len 0.4 -S1 [expr -1.209056*$refen] -refen $refen 
Bpm "BPM.DL_L.86" -len 0 
Drift "DS1C_B" -len 0.25 
Sextupole "SEXT.DL.SNN1" -len 0.1 -S2 [expr 12.85540299483511/2.38*$refen] -refen $refen 
Drift "DS1C_A" -len 0.1850809 
Sbend "BEND.DL.DIPOLE0" -len 2.56621607 -angle -0.372030709 -E1 -0.1860153545 -E2 -0.1860153545 -S1 [expr 0.04695405543*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*-0.372030709*-0.372030709/2.56621607*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "D12" -len 0.1559115 
Quadrupole "QUAD.QI2" -len 0.4 -S1 [expr 0.824612*$refen] -refen $refen 
Bpm "BPM.DL_L.87" -len 0 
Drift "D11B" -len 0.0940885 
Sextupole "SEXT.DL.SNCH1" -len 0.1 -S2 [expr 4.124909873415538/2.38*$refen] -refen $refen 
Drift "D11A" -len 0.1 
Quadrupole "QUAD.QI1" -len 0.2 -S1 [expr -0.746512*$refen] -refen $refen 
Bpm "BPM.DL_L.88" -len 0 
Marker "MCELL_11" 
Quadrupole "QUAD.QI1" -len 0.2 -S1 [expr -0.746512*$refen] -refen $refen 
Bpm "BPM.DL_L.89" -len 0 
Drift "D11A" -len 0.1 
Sextupole "SEXT.DL.SNCH1" -len 0.1 -S2 [expr 4.124909873415538/2.38*$refen] -refen $refen 
Drift "D11B" -len 0.0940885 
Quadrupole "QUAD.QI2" -len 0.4 -S1 [expr 0.824612*$refen] -refen $refen 
Bpm "BPM.DL_L.90" -len 0 
Drift "D12" -len 0.1559115 
Sbend "BEND.DL.DIPOLE0" -len 2.56621607 -angle -0.372030709 -E1 -0.1860153545 -E2 -0.1860153545 -S1 [expr 0.04695405543*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*-0.372030709*-0.372030709/2.56621607*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "DS1C_A" -len 0.1850809 
Sextupole "SEXT.DL.SNN1" -len 0.1 -S2 [expr 12.85540299483511/2.38*$refen] -refen $refen 
Drift "DS1C_B" -len 0.25 
Quadrupole "QUAD.QS1C" -len 0.4 -S1 [expr -1.209056*$refen] -refen $refen 
Bpm "BPM.DL_L.91" -len 0 
Drift "DS2C_A" -len 0.2846962 
Sextupole "SEXT.DL.SNN2" -len 0.1 -S2 [expr -7.207522988235507/2.38*$refen] -refen $refen 
Drift "DS2C_B" -len 0.01 
Sbend "BEND.DL.DIPN0" -len 0.2851351189 -angle 0.04133674544 -E1 0.02066837272 -E2 0.02066837272 -S1 [expr 0.9511964998*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*0.04133674544*0.04133674544/0.2851351189*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "DS3C_A" -len 0.01 
Sextupole "SEXT.DL.SNN3" -len 0.1 -S2 [expr 2.510029401343759/2.38*$refen] -refen $refen 
Drift "DS3C_B" -len 0.1702229 
Quadrupole "QUAD.QS2C" -len 0.2 -S1 [expr -0.1666676*$refen] -refen $refen 
Bpm "BPM.DL_L.92" -len 0 
Quadrupole "QUAD.QS2C" -len 0.2 -S1 [expr -0.1666676*$refen] -refen $refen 
Bpm "BPM.DL_L.93" -len 0 
Drift "DS3CB" -len 0.08511145 
Sextupole "SEXT.DL.SXL4" -len 0.1 -S2 [expr -10.82803618878591/2.38*$refen] -refen $refen 
Drift "DS3CA" -len 0.08511145 
Marker "MD2.2" 
Sbend "BEND.DL.DIPN0" -len 0.2851351189 -angle 0.04133674544 -E1 0.02066837272 -E2 0.02066837272 -S1 [expr 0.9511964998*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*0.04133674544*0.04133674544/0.2851351189*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "D33B" -len 0.0464855 
Sextupole "SEXT.DL.SXL3" -len 0.1 -S2 [expr 10.13678425458882/2.38*$refen] -refen $refen 
Drift "D33A" -len 0.0464855 
Quadrupole "QUAD.QL3IN" -len 0.4 -S1 [expr -1.943224*$refen] -refen $refen 
Bpm "BPM.DL_L.94" -len 0 
Drift "D32B" -len 0.43333 
Sextupole "SEXT.DL.SXL2" -len 0.1 -S2 [expr -3.816703898607325/2.38*$refen] -refen $refen 
Drift "D32A" -len 0.11111 
Quadrupole "QUAD.QL2IN" -len 0.4 -S1 [expr 1.14802*$refen] -refen $refen 
Bpm "BPM.DL_L.95" -len 0 
Drift "D33" -len 0.192971 
Marker "MD3.2" 
# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 2.574109835 
Sbend "BEND.SEPT2" -len 2.58202788155376 -angle -0.2714151852 -E1 -0.1357075926 -E2 -0.1357075926 -S1 [expr -1.249456954*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*-0.2714151852*-0.2714151852/2.58202788155376*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "DL3IN" -len 0.4 
Quadrupole "QUAD.QL1IN" -len 0.4 -S1 [expr -0.647036*$refen] -refen $refen 
Bpm "BPM.DL_L.96" -len 0 
Drift "DL2IN" -len 0.3 
# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 1.00031738 
Sbend "BEND.SEPT1" -len 1.00063486103645 -angle -0.0872664626 -E1 -0.0436332313 -E2 -0.0436332313 -S1 [expr 0.9956481989*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*-0.0872664626*-0.0872664626/1.00063486103645*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "DL1IN" -len 1.1 
# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 
# WARNING: original length was 0.5000007127 
Sbend "BEND.SEPTQ" -len 0.500001425441655 -angle -0.005849061169 -E1 -0.002924530584 -E2 -0.002924530584 -S1 [expr -0.5000007127*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*-0.005849061169*-0.005849061169/0.500001425441655*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Kalign DL_ext -cx -0.01425 -cxp 0.0075 -cz 0.
Drift "DL_LONG.CENTEREXT" -len 0. -nodes 1 -kick DL_ext 
Marker "DL_LONG.EXTRACTION" 
