## Reference energy taken from list_energies.tcl (Computed from Eduardo Martin's Lattice) 
## This was chosen to match the less energetic "bunch type" (there are 24 different possible bunch energies). 
## Which means there will be a slight magnet missfocus for the other bunches. 
if {$Dipole_synrad} { 
	set refen 2.3793510476249953 
} else { 
	set refen 2.38 
} 
Marker "TF1.INJECTION" 
Drift "DRRF" -len 1.8 
Rfmultipole "RFMulti.DLEXTSEPTUM" -len 0.2 -freq 0.49975 -strength [expr 0.0075*$refen] -phase $pi -refen $refen 
Marker "ISODL" 
Bpm "BPM.TL1.0" -len 0 
Drift "TL1.D11" -len 0.8 
Quadrupole "QUAD.TL1.Q12" -len 0.75 -S1 [expr -0.724266702*$refen] -refen $refen 
Bpm "BPM.TL1.1" -len 0 
Drift "TL1.D13" -len 0.8 
Quadrupole "QUAD.TL1.Q14" -len 0.75 -S1 [expr 0.917454447*$refen] -refen $refen 
Bpm "BPM.TL1.2" -len 0 
Drift "TL1.D15" -len 1.6 
Quadrupole "QUAD.TL1.Q16" -len 0.75 -S1 [expr -0.8542453973*$refen] -refen $refen 
Bpm "BPM.TL1.3" -len 0 
Drift "TL1.D17" -len 0.8 
Quadrupole "QUAD.TL1.Q18" -len 0.75 -S1 [expr 0.6709421753*$refen] -refen $refen 
Bpm "BPM.TL1.4" -len 0 
Drift "TL1.D19" -len 0.8 
Marker "TL1.ENTCELLONE" 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.005723015 
Sbend "BEND.TL1.BEND20" -len 2.01146240647403 -angle 0.2617993878 -E1 0.1308996939 -E2 0.1308996939 -refen $refen 
set refen [expr $refen-14.1e-6*0.2617993878*0.2617993878/2.01146240647403*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TL1.D222" -len 0.7773893387 
Sextupole "SEXT.TL1.SX225" -len 0.3 -S2 [expr 24.3920635301111/2.38*$refen] -refen $refen 
Drift "TL1.D228" -len 0.3136946693 
Quadrupole "QUAD.TL1.Q23" -len 0.75 -S1 [expr 0.4599766094*$refen] -refen $refen 
Bpm "BPM.TL1.5" -len 0 
Drift "TL1.D242" -len 0.1562568231 
Sextupole "SEXT.TL1.SX245" -len 0.3 -S2 [expr -9.49267659039926/2.38*$refen] -refen $refen 
Drift "TL1.D248" -len 0.1562568231 
Quadrupole "QUAD.TL1.Q25" -len 0.75 -S1 [expr -0.5296565843*$refen] -refen $refen 
Bpm "BPM.TL1.6" -len 0 
Drift "TL1.D262" -len 0.0500000253 
Drift "TL1.D268" -len 0.0500000253 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 0.500013974 
Sbend "BEND.TL1.BEND27" -len 0.500027948380432 -angle -0.02589861758 -E1 -0.01294930879 -E2 -0.01294930879 -refen $refen 
set refen [expr $refen-14.1e-6*-0.02589861758*-0.02589861758/0.500027948380432*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TL1.D282" -len 0.1536564076 
Sextupole "SEXT.TL1.SX285" -len 0.3 -S2 [expr 3.338920408217688/2.38*$refen] -refen $refen 
Drift "TL1.D288" -len 0.1536564076 
Quadrupole "QUAD.TL1.Q30A" -len 0.375 -S1 [expr 0.2496519342*$refen] -refen $refen 
Bpm "BPM.TL1.7" -len 0 
Marker "TL1.MIDCELLONE" 
Quadrupole "QUAD.TL1.Q30B" -len 0.375 -S1 [expr 0.2496519342*$refen] -refen $refen 
Bpm "BPM.TL1.8" -len 0 
Drift "TL1.D322" -len 0.1536564076 
Sextupole "SEXT.TL1.SX325" -len 0.3 -S2 [expr 3.338920408217688/2.38*$refen] -refen $refen 
Drift "TL1.D328" -len 0.1536564076 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 0.500013974 
Sbend "BEND.TL1.BEND33" -len 0.500027948380432 -angle -0.02589861758 -E1 -0.01294930879 -E2 -0.01294930879 -refen $refen 
set refen [expr $refen-14.1e-6*-0.02589861758*-0.02589861758/0.500027948380432*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TL1.D342" -len 0.0500000253 
Drift "TL1.D348" -len 0.0500000253 
Quadrupole "QUAD.TL1.Q35" -len 0.75 -S1 [expr -0.5296565843*$refen] -refen $refen 
Bpm "BPM.TL1.9" -len 0 
Drift "TL1.D362" -len 0.1562568231 
Sextupole "SEXT.TL1.SX365" -len 0.3 -S2 [expr -9.49267659039926/2.38*$refen] -refen $refen 
Drift "TL1.D368" -len 0.1562568231 
Quadrupole "QUAD.TL1.Q37" -len 0.75 -S1 [expr 0.4599766094*$refen] -refen $refen 
Bpm "BPM.TL1.10" -len 0 
Drift "TL1.D382" -len 0.3136946693 
Sextupole "SEXT.TL1.SX385" -len 0.3 -S2 [expr 24.3920635301111/2.38*$refen] -refen $refen 
Drift "TL1.D388" -len 0.7773893387 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.005723015 
Sbend "BEND.TL1.BEND40" -len 2.01146240647403 -angle 0.2617993878 -E1 0.1308996939 -E2 0.1308996939 -refen $refen 
set refen [expr $refen-14.1e-6*0.2617993878*0.2617993878/2.01146240647403*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Marker "TL1.EXICELLONE" 
Drift "TL1.D402" -len 0.4980886966 
Sextupole "SEXT.TL1.SXC405" -len 0.3 -S2 [expr -47.23733777054752/2.38*$refen] -refen $refen 
Drift "TL1.D408" -len 0.06602956553 
Quadrupole "QUAD.TL1.Q41" -len 0.75 -S1 [expr 0.1786552102*$refen] -refen $refen 
Bpm "BPM.TL1.11" -len 0 
Drift "TL1.D412" -len 0.9874829865 
Sextupole "SEXT.TL1.SXC415" -len 0.3 -S2 [expr 99.08725085836913/2.38*$refen] -refen $refen 
Drift "TL1.D418" -len 0.2291609955 
Quadrupole "QUAD.TL1.Q42" -len 0.75 -S1 [expr -0.5380648102*$refen] -refen $refen 
Bpm "BPM.TL1.12" -len 0 
Drift "TL1.D422" -len 1.245610625 
Sextupole "SEXT.TL1.SXC425" -len 0.3 -S2 [expr -3.285199895923748/2.38*$refen] -refen $refen 
Drift "TL1.D428" -len 0.3152035415 
Quadrupole "QUAD.TL1.Q43" -len 0.75 -S1 [expr 0.5385499766*$refen] -refen $refen 
Bpm "BPM.TL1.13" -len 0 
Drift "TL1.D432" -len 1.58013019 
Sextupole "SEXT.TL1.SXC435" -len 0.3 -S2 [expr 4.08135320023954/2.38*$refen] -refen $refen 
Drift "TL1.D438" -len 0.4267100632 
Quadrupole "QUAD.TL1.Q44" -len 0.75 -S1 [expr -0.6426636217*$refen] -refen $refen 
Bpm "BPM.TL1.14" -len 0 
Drift "TL1.D442" -len 1.673352339 
Sextupole "SEXT.TL1.SXC445" -len 0.3 -S2 [expr -91.20282995525814/2.38*$refen] -refen $refen 
Drift "TL1.D448" -len 0.457784113 
Quadrupole "QUAD.TL1.Q45" -len 0.75 -S1 [expr 0.666494741*$refen] -refen $refen 
Bpm "BPM.TL1.15" -len 0 
Drift "TL1.D452" -len 0.457784113 
Sextupole "SEXT.TL1.SXC455" -len 0.3 -S2 [expr -76.11483403888109/2.38*$refen] -refen $refen 
Drift "TL1.D458" -len 1.673352339 
Quadrupole "QUAD.TL1.Q46" -len 0.75 -S1 [expr -0.6390286404*$refen] -refen $refen 
Bpm "BPM.TL1.16" -len 0 
Drift "TL1.D462" -len 0.4267100632 
Sextupole "SEXT.TL1.SXC465" -len 0.3 -S2 [expr -3.599377152714896/2.38*$refen] -refen $refen 
Drift "TL1.D468" -len 1.58013019 
Quadrupole "QUAD.TL1.Q47" -len 0.75 -S1 [expr 0.5420133188*$refen] -refen $refen 
Bpm "BPM.TL1.17" -len 0 
Drift "TL1.D472" -len 0.3152035415 
Sextupole "SEXT.TL1.SXC475" -len 0.3 -S2 [expr 8.303979576258959/2.38*$refen] -refen $refen 
Drift "TL1.D478" -len 1.245610625 
Quadrupole "QUAD.TL1.Q48" -len 0.75 -S1 [expr -0.5286812296*$refen] -refen $refen 
Bpm "BPM.TL1.18" -len 0 
Drift "TL1.D482" -len 0.2291609955 
Sextupole "SEXT.TL1.SXC485" -len 0.3 -S2 [expr -67.41067815699307/2.38*$refen] -refen $refen 
Drift "TL1.D488" -len 0.9874829865 
Quadrupole "QUAD.TL1.Q49" -len 0.75 -S1 [expr 0.1559722145*$refen] -refen $refen 
Bpm "BPM.TL1.19" -len 0 
Drift "TL1.D492" -len 0.06602956553 
Sextupole "SEXT.TL1.SXC495" -len 0.3 -S2 [expr -73.88344769535938/2.38*$refen] -refen $refen 
Drift "TL1.D498" -len 0.4980886966 
Marker "TL1.ENTCELLTWO" 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.005723015 
Sbend "BEND.TL1.BEND50" -len 2.01146240647403 -angle -0.2617993878 -E1 -0.1308996939 -E2 -0.1308996939 -refen $refen 
set refen [expr $refen-14.1e-6*-0.2617993878*-0.2617993878/2.01146240647403*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TL1.D522" -len 0.7773893387 
Sextupole "SEXT.TL1.SX525" -len 0.3 -S2 [expr 24.2533826388338/2.38*$refen] -refen $refen 
Drift "TL1.D528" -len 0.3136946693 
Quadrupole "QUAD.TL1.Q53" -len 0.75 -S1 [expr 1.062626852476912/2.38*$refen] -refen $refen 
Bpm "BPM.TL1.20" -len 0 
Drift "TL1.D542" -len 0.1562568231 
Sextupole "SEXT.TL1.SX545" -len 0.3 -S2 [expr -1.328228923381128/2.38*$refen] -refen $refen 
Drift "TL1.D548" -len 0.1562568231 
Quadrupole "QUAD.TL1.Q55" -len 0.75 -S1 [expr -1.209392505313036/2.38*$refen] -refen $refen 
Bpm "BPM.TL1.21" -len 0 
Drift "TL1.D562" -len 0.0500000253 
Drift "TL1.D568" -len 0.0500000253 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 0.500013974 
Sbend "BEND.TL1.BEND57" -len 0.500027948380432 -angle 0.02589861758 -E1 0.01294930879 -E2 0.01294930879 -refen $refen 
set refen [expr $refen-14.1e-6*0.02589861758*0.02589861758/0.500027948380432*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TL1.D582" -len 0.1536564076 
Sextupole "SEXT.TL1.SX585" -len 0.3 -S2 [expr -8.21948364246234/2.38*$refen] -refen $refen 
Drift "TL1.D588" -len 0.1536564076 
Quadrupole "QUAD.TL1.Q60A" -len 0.375 -S1 [expr 0.5947247278762964/2.38*$refen] -refen $refen 
Bpm "BPM.TL1.22" -len 0 
Marker "TL1.MIDCELLTWO" 
Quadrupole "QUAD.TL1.Q60B" -len 0.375 -S1 [expr 0.5947247278762964/2.38*$refen] -refen $refen 
Bpm "BPM.TL1.23" -len 0 
Drift "TL1.D622" -len 0.05000058615 
Sextupole "SEXT.TL1.SX625" -len 0.3 -S2 [expr 9.358194346159989/2.38*$refen] -refen $refen 
Drift "TL1.D628" -len 0.05000058615 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 0.500013974 
Sbend "BEND.TL1.BEND63" -len 0.500027948380432 -angle 0.02589861758 -E1 0.01294930879 -E2 0.01294930879 -refen $refen 
set refen [expr $refen-14.1e-6*0.02589861758*0.02589861758/0.500027948380432*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TL1.D642" -len 0.2366852486 
Drift "TL1.D648" -len 0.2366852486 
Quadrupole "QUAD.TL1.Q65" -len 0.75 -S1 [expr -1.348712412265545/2.38*$refen] -refen $refen 
Bpm "BPM.TL1.24" -len 0 
Drift "TL1.D662" -len 0.1747711281 
Sextupole "SEXT.TL1.SX665" -len 0.3 -S2 [expr -18.90361351065094/2.38*$refen] -refen $refen 
Drift "TL1.D668" -len 0.1747711281 
Quadrupole "QUAD.TL1.Q67" -len 0.75 -S1 [expr 1.144455737145295/2.38*$refen] -refen $refen 
Bpm "BPM.TL1.25" -len 0 
Drift "TL1.D682" -len 0.258673648 
Sextupole "SEXT.TL1.SX685" -len 0.3 -S2 [expr 38.08826908704169/2.38*$refen] -refen $refen 
Drift "TL1.D688" -len 0.667347296 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.005723015 
Sbend "BEND.TL1.BEND70" -len 2.01146240647403 -angle -0.2617993878 -E1 -0.1308996939 -E2 -0.1308996939 -refen $refen 
set refen [expr $refen-14.1e-6*-0.2617993878*-0.2617993878/2.01146240647403*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Marker "TL1.EXICELLTWO" 
Bpm "BPM.TL1.26" -len 0 
### Offset needed for CR1 Injection (used to be -cx 19590.114478093e-6 -cxp 23.8506459472122e-6)
#Kalign CR1offset -cx 0.01708861599238307 -cxp 0.0001113719435327479 -cz -6.489360086e-06
Kalign CR1offset -cx 0.01708861599238307 -cxp 0.0001113719435327479 -cz -5.761172761e-05
Drift "CR1_offset" -len 0. -nodes 1 -kick CR1offset 
Marker "TF1.EJECTION" 
