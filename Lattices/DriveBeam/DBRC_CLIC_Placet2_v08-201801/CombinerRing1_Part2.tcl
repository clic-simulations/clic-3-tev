## Reference energy taken from list_energies.tcl (Computed from Eduardo Martin's Lattice) 
## This was chosen to match the less energetic "bunch type" (there are 24 different possible bunch energies). 
## Which means there will be a slight magnet missfocus for the other bunches. 
## This was set to the 2nd turn of CR1 (notice that the 3rd turn doesn't go through the 2nd half). 
if {$Dipole_synrad} { 
	set refen 2.378693972723014 
} else { 
	set refen 2.38 
} 
Marker "CR1_P2.INJECTION" 
Drift "CR1.EXTSEPTUM" -len 1 
Drift "CR1.DL7IN" -len 0.5 
Quadrupole "QUAD.CR1.QL6EXT" -len 0.25 -S1 [expr 1.193855820504099/2.38*$refen] -refen $refen 
Bpm "BPM.CR1_2.82" -len 0 
Drift "CR1.DL6IN" -len 0.553 
Quadrupole "QUAD.CR1.QL5EXT" -len 0.5 -S1 [expr -2.15770259424327/2.38*$refen] -refen $refen 
Bpm "BPM.CR1_2.83" -len 0 
Drift "CR1.DL5IN" -len 1 
Quadrupole "QUAD.CR1.QL4IEXT" -len 0.5 -S1 [expr 1.520443016944819/2.38*$refen] -refen $refen 
Bpm "BPM.CR1_2.84" -len 0 
Drift "CR1.DL4IN" -len 0.349996062 
Drift "CR1.DRRF2" -len 0.55265 
Drift "CR1.DRRF1" -len 0.7736769688 
Drift "CR1.DRRF1" -len 0.7736769688 
Drift "CR1.DRRF" -len 0.4 
Drift "CR1.DL3IN" -len 1.099258 
Quadrupole "QUAD.CR1.QL3IEXT" -len 0.5 -S1 [expr -1.137832040354124/2.38*$refen] -refen $refen 
Bpm "BPM.CR1_2.85" -len 0 
Drift "CR1.DL2IN" -len 0.3010372 
Quadrupole "QUAD.CR1.QL2IEXT" -len 0.5 -S1 [expr 2.841774878265831/2.38*$refen] -refen $refen 
Bpm "BPM.CR1_2.86" -len 0 
Drift "CR1.DL1IN" -len 0.3098482 
Quadrupole "QUAD.CR1.QL1EXT" -len 0.5 -S1 [expr -1.74934542095025/2.38*$refen] -refen $refen 
Bpm "BPM.CR1_2.87" -len 0 
#### Missing length ####
Drift "CR1.DL0IN" -len 0.5
Sextupole "SEXT.CR1.SXL0EXT" -len 0.5 -S2 [expr 0*$refen] -refen $refen 
Drift "CR1.DMISSLEN" -len 1.66137098404886 
####
Marker "CR1MDBACELLSTART" 
Marker "CR1.DBA.MCELL" 
Quadrupole "QUAD.CR1.DBA.QI1" -len 0.4 -S1 [expr -0.3358875195*$refen] -refen $refen 
Bpm "BPM.CR1_2.88" -len 0 
Drift "CR1.DBA.D11A" -len 0.2082887778 
Drift "CR1.DBA.D11B" -len 0.1989315367 
Quadrupole "QUAD.CR1.DBA.QI2" -len 0.4 -S1 [expr 0.66922795*$refen] -refen $refen 
Bpm "BPM.CR1_2.89" -len 0 
Drift "CR1.DBA.D12" -len 0.2467904078 
### Start of arc 4 ###
Sbend "BEND.CR1.DBA.DIPOLE0" -len 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -S1 [expr 0.05125499604*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*0.2983805661*0.2983805661/2.801278682*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "CR1.DBA.DS1C_A" -len 0.2929622945 
Sextupole "SEXT.CR1.DBA.SN0.4" -len 0.1 -S2 [expr 15.65178756527762/2.38*$refen] -refen $refen 
Drift "CR1.DBA.DS1C_B" -len 0.1 
Marker "CR1.DBA.HCM0" 
Quadrupole "QUAD.CR1.DBA.QS1C" -len 0.4 -S1 [expr -0.450384*$refen] -refen $refen 
Bpm "BPM.CR1_2.90" -len 0 
Drift "CR1.DBA.DS2C" -len 0.5664710132 
Sbend "BEND.CR1.DBA.DIPN0" -len 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -S1 [expr 0.1995945832*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Marker "CR1.DBA.HBM" 
Drift "CR1.DBA.DS3C_A" -len 0.1426363128 
Sextupole "SEXT.CR1.DBA.SN3.4" -len 0.1 -S2 [expr -1.605605035635479/2.38*$refen] -refen $refen 
Drift "CR1.DBA.DS3C_B" -len 0.1426363128 
Quadrupole "QUAD.CR1.DBA.QS2C" -len 0.2 -S1 [expr 0.2323546236*$refen] -refen $refen 
Bpm "BPM.CR1_2.91" -len 0 
Quadrupole "QUAD.CR1.DBA.QS2C" -len 0.2 -S1 [expr 0.2323546236*$refen] -refen $refen 
Bpm "BPM.CR1_2.92" -len 0 
Drift "CR1.DBA.DS3C_B" -len 0.1426363128 
Sextupole "SEXT.CR1.DBA.SNN3.4" -len 0.1 -S2 [expr 4.598184354567945/2.38*$refen] -refen $refen 
Drift "CR1.DBA.DS3C_A" -len 0.1426363128 
Marker "CR1.DBA.HBM2" 
Sbend "BEND.CR1.DBA.DIPN0" -len 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -S1 [expr 0.1995945832*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "CR1.DBA.DS2C" -len 0.5664710132 
Quadrupole "QUAD.CR1.DBA.QS1C" -len 0.4 -S1 [expr -0.450384*$refen] -refen $refen 
Bpm "BPM.CR1_2.93" -len 0 
Marker "CR1.DBA.HCM" 
Drift "CR1.DBA.DS1C_B" -len 0.1 
Sextupole "SEXT.CR1.DBA.SN1.4" -len 0.1 -S2 [expr -23.10136182606666/2.38*$refen] -refen $refen 
Drift "CR1.DBA.DS1C_A" -len 0.2929622945 
Sbend "BEND.CR1.DBA.DIPOLE0" -len 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -S1 [expr 0.05125499604*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*0.2983805661*0.2983805661/2.801278682*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "CR1.DBA.D12" -len 0.2467904078 
Quadrupole "QUAD.CR1.DBA.QI2" -len 0.4 -S1 [expr 0.66922795*$refen] -refen $refen 
Bpm "BPM.CR1_2.94" -len 0 
Drift "CR1.DBA.D11B1" -len 0.3133083062 
Sextupole "SEXT.CR1.DBA.SD0" -len 0.1 -S2 [expr 0*$refen] -refen $refen 
Drift "CR1.DBA.DM0" -len 0.1 
Quadrupole "QUAD.CR1.DBA.QF1" -len 0.4 -S1 [expr -0.664141578*$refen] -refen $refen 
Bpm "BPM.CR1_2.95" -len 0 
Drift "CR1.DBA.DM1_A" -len 1.106904053 
Sextupole "SEXT.CR1.DBA.SNCH1" -len 0.1 -S2 [expr -15.73646496670576/2.38*$refen] -refen $refen 
Drift "CR1.DBA.DM1_B" -len 0.1069040525 
Quadrupole "QUAD.CR1.DBA.QF2" -len 0.4 -S1 [expr 0.5637241356*$refen] -refen $refen 
Bpm "BPM.CR1_2.96" -len 0 
Drift "CR1.DBA.DM2" -len 1.099116763 
Sextupole "SEXT.CR1.DBA.SD0" -len 0.1 -S2 [expr 0*$refen] -refen $refen 
Drift "CR1.DBA.DMSEXT1" -len 0.1 
Quadrupole "QUAD.CR1.DBA.QF3" -len 0.4 -S1 [expr -0.6662844456*$refen] -refen $refen 
Bpm "BPM.CR1_2.97" -len 0 
Drift "CR1.DBA.DM3" -len 0.2563831229 
Drift "CR1.DBA.DMSEXT1" -len 0.1 
Drift "CR1.DBA.D11B2" -len 0.1489315367 
Quadrupole "QUAD.CR1.DBA.QI2" -len 0.4 -S1 [expr 0.66922795*$refen] -refen $refen 
Bpm "BPM.CR1_2.98" -len 0 
Drift "CR1.DBA.D12" -len 0.2467904078 
Sbend "BEND.CR1.DBA.DIPOLE0" -len 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -S1 [expr 0.05125499604*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*0.2983805661*0.2983805661/2.801278682*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "CR1.DBA.DS1C_A" -len 0.2929622945 
Sextupole "SEXT.CR1.DBA.SN1.4" -len 0.1 -S2 [expr -23.10136182606666/2.38*$refen] -refen $refen 
Drift "CR1.DBA.DS1C_B" -len 0.1 
Quadrupole "QUAD.CR1.DBA.QS1C" -len 0.4 -S1 [expr -0.450384*$refen] -refen $refen 
Bpm "BPM.CR1_2.99" -len 0 
Drift "CR1.DBA.DS2C" -len 0.5664710132 
Sbend "BEND.CR1.DBA.DIPN0" -len 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -S1 [expr 0.1995945832*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "CR1.DBA.DS3C_A" -len 0.1426363128 
Sextupole "SEXT.CR1.DBA.SNN3.4" -len 0.1 -S2 [expr 4.598184354567945/2.38*$refen] -refen $refen 
Drift "CR1.DBA.DS3C_B" -len 0.1426363128 
Quadrupole "QUAD.CR1.DBA.QS2C" -len 0.2 -S1 [expr 0.2323546236*$refen] -refen $refen 
Bpm "BPM.CR1_2.100" -len 0 
Quadrupole "QUAD.CR1.DBA.QS2C" -len 0.2 -S1 [expr 0.2323546236*$refen] -refen $refen 
Bpm "BPM.CR1_2.101" -len 0 
Drift "CR1.DBA.DS3C_B" -len 0.1426363128 
Sextupole "SEXT.CR1.DBA.SN3.4" -len 0.1 -S2 [expr -1.605605035635479/2.38*$refen] -refen $refen 
Drift "CR1.DBA.DS3C_A" -len 0.1426363128 
Sbend "BEND.CR1.DBA.DIPN0" -len 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -S1 [expr 0.1995945832*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "CR1.DBA.DS2C" -len 0.5664710132 
Quadrupole "QUAD.CR1.DBA.QS1C" -len 0.4 -S1 [expr -0.450384*$refen] -refen $refen 
Bpm "BPM.CR1_2.102" -len 0 
Drift "CR1.DBA.DS1C_B" -len 0.1 
Sextupole "SEXT.CR1.DBA.SN0.4" -len 0.1 -S2 [expr 15.65178756527762/2.38*$refen] -refen $refen 
Drift "CR1.DBA.DS1C_A" -len 0.2929622945 
Sbend "BEND.CR1.DBA.DIPOLE0" -len 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -S1 [expr 0.05125499604*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*0.2983805661*0.2983805661/2.801278682*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "CR1.DBA.D12" -len 0.2467904078 
Quadrupole "QUAD.CR1.DBA.QI2" -len 0.4 -S1 [expr 0.66922795*$refen] -refen $refen 
Bpm "BPM.CR1_2.103" -len 0 
Drift "CR1.DBA.D11B" -len 0.1989315367 
Drift "CR1.DBA.D11A" -len 0.2082887778 
Quadrupole "QUAD.CR1.DBA.QI1" -len 0.4 -S1 [expr -0.3358875195*$refen] -refen $refen 
Bpm "BPM.CR1_2.104" -len 0 
Marker "CR1.DBA.MCELL" 
Marker "CR1MDBACELLEND" 
Drift "CR1.DTR1A" -len 0.35 
Sextupole "SEXT.CR1.SXTR1" -len 0.1 -S2 [expr -125.1148601198482/2.38*$refen] -refen $refen 
Drift "CR1.DTR1B" -len 0.15 
Quadrupole "QUAD.CR1.QTR1" -len 0.5 -S1 [expr -2.037573840398892/2.38*$refen] -refen $refen 
Bpm "BPM.CR1_2.105" -len 0 
Drift "CR1.DTR2" -len 1.2 
Quadrupole "QUAD.CR1.QTR2" -len 0.5 -S1 [expr 2.024955314250834/2.38*$refen] -refen $refen 
Bpm "BPM.CR1_2.106" -len 0 
Drift "CR1.DTR3A" -len 0.45 
Sextupole "SEXT.CR1.SXTR3" -len 0.1 -S2 [expr -2.738605584735498/2.38*$refen] -refen $refen 
Drift "CR1.DTR3B" -len 0.45 
Quadrupole "QUAD.CR1.QTR3" -len 0.5 -S1 [expr -1.255965828244565/2.38*$refen] -refen $refen 
Bpm "BPM.CR1_2.107" -len 0 
Drift "CR1.DTR4" -len 0.6 
Quadrupole "QUAD.CR1.QTR4" -len 0.5 -S1 [expr -0.2227315040487163/2.38*$refen] -refen $refen 
Bpm "BPM.CR1_2.108" -len 0 
Drift "CR1.DTR5A" -len 0.75 
Sextupole "SEXT.CR1.SXTR5" -len 0.1 -S2 [expr 4.963088060467437/2.38*$refen] -refen $refen 
Drift "CR1.DTR5B" -len 0.35 
Drift "CR1.DTR5B" -len 0.35 
Sextupole "SEXT.CR1.SXTR5" -len 0.1 -S2 [expr 4.963088060467437/2.38*$refen] -refen $refen 
Drift "CR1.DTR5A" -len 0.75 
Quadrupole "QUAD.CR1.QTR4" -len 0.5 -S1 [expr -0.2227315040487163/2.38*$refen] -refen $refen 
Bpm "BPM.CR1_2.109" -len 0 
Drift "CR1.DTR4" -len 0.6 
Quadrupole "QUAD.CR1.QTR3" -len 0.5 -S1 [expr -1.255965828244565/2.38*$refen] -refen $refen 
Bpm "BPM.CR1_2.110" -len 0 
Drift "CR1.DTR3B" -len 0.45 
Sextupole "SEXT.CR1.SXTR3" -len 0.1 -S2 [expr -2.738605584735498/2.38*$refen] -refen $refen 
Drift "CR1.DTR3A" -len 0.45 
Quadrupole "QUAD.CR1.QTR2" -len 0.5 -S1 [expr 2.024955314250834/2.38*$refen] -refen $refen 
Bpm "BPM.CR1_2.111" -len 0 
Drift "CR1.DTR2" -len 1.2 
Quadrupole "QUAD.CR1.QTR1" -len 0.5 -S1 [expr -2.037573840398892/2.38*$refen] -refen $refen 
Bpm "BPM.CR1_2.112" -len 0 
Drift "CR1.DTR1B" -len 0.15 
Sextupole "SEXT.CR1.SXTR1" -len 0.1 -S2 [expr -125.1148601198482/2.38*$refen] -refen $refen 
Drift "CR1.DTR1A" -len 0.35 
Marker "CR1MDBACELLSTART" 
Marker "CR1.DBA.MCELL" 
Quadrupole "QUAD.CR1.DBA.QI1" -len 0.4 -S1 [expr -0.3358875195*$refen] -refen $refen 
Bpm "BPM.CR1_2.113" -len 0 
Drift "CR1.DBA.D11A" -len 0.2082887778 
Drift "CR1.DBA.D11B" -len 0.1989315367 
Quadrupole "QUAD.CR1.DBA.QI2" -len 0.4 -S1 [expr 0.66922795*$refen] -refen $refen 
Bpm "BPM.CR1_2.114" -len 0 
Drift "CR1.DBA.D12" -len 0.2467904078 
Sbend "BEND.CR1.DBA.DIPOLE0" -len 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -S1 [expr 0.05125499604*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*0.2983805661*0.2983805661/2.801278682*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "CR1.DBA.DS1C_A" -len 0.2929622945 
Sextupole "SEXT.CR1.DBA.SN0.5" -len 0.1 -S2 [expr 9.564860811679351/2.38*$refen] -refen $refen 
Drift "CR1.DBA.DS1C_B" -len 0.1 
Marker "CR1.DBA.HCM0" 
Quadrupole "QUAD.CR1.DBA.QS1C" -len 0.4 -S1 [expr -0.450384*$refen] -refen $refen 
Bpm "BPM.CR1_2.115" -len 0 
Drift "CR1.DBA.DS2C" -len 0.5664710132 
Sbend "BEND.CR1.DBA.DIPN0" -len 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -S1 [expr 0.1995945832*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Marker "CR1.DBA.HBM" 
Drift "CR1.DBA.DS3C_A" -len 0.1426363128 
Sextupole "SEXT.CR1.DBA.SN3.5" -len 0.1 -S2 [expr -1.003837719641216/2.38*$refen] -refen $refen 
Drift "CR1.DBA.DS3C_B.9" -len 0.1426363128 
Quadrupole "QUAD.CR1.DBA.QS2C" -len 0.2 -S1 [expr 0.2323546236*$refen] -refen $refen 
Bpm "BPM.CR1_2.116" -len 0 
Quadrupole "QUAD.CR1.DBA.QS2C" -len 0.2 -S1 [expr 0.2323546236*$refen] -refen $refen 
Bpm "BPM.CR1_2.117" -len 0 
Drift "CR1.DBA.DS3C_B" -len 0.1426363128 
Sextupole "SEXT.CR1.DBA.SNN3.5" -len 0.1 -S2 [expr 4.816144769464445/2.38*$refen] -refen $refen 
Drift "CR1.DBA.DS3C_A" -len 0.1426363128 
Marker "CR1.DBA.HBM2" 
Sbend "BEND.CR1.DBA.DIPN0" -len 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -S1 [expr 0.1995945832*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "CR1.DBA.DS2C" -len 0.5664710132 
Quadrupole "QUAD.CR1.DBA.QS1C" -len 0.4 -S1 [expr -0.450384*$refen] -refen $refen 
Bpm "BPM.CR1_2.118" -len 0 
Marker "CR1.DBA.HCM" 
Drift "CR1.DBA.DS1C_B" -len 0.1 
Sextupole "SEXT.CR1.DBA.SN1.5" -len 0.1 -S2 [expr -12.54315381405628/2.38*$refen] -refen $refen 
Drift "CR1.DBA.DS1C_A" -len 0.2929622945 
Sbend "BEND.CR1.DBA.DIPOLE0" -len 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -S1 [expr 0.05125499604*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*0.2983805661*0.2983805661/2.801278682*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "CR1.DBA.D12" -len 0.2467904078 
Quadrupole "QUAD.CR1.DBA.QI2" -len 0.4 -S1 [expr 0.66922795*$refen] -refen $refen 
Bpm "BPM.CR1_2.119" -len 0 
Drift "CR1.DBA.D11B1" -len 0.3133083062 
Sextupole "SEXT.CR1.DBA.SD0" -len 0.1 -S2 [expr 0*$refen] -refen $refen 
Drift "CR1.DBA.DM0" -len 0.1 
Quadrupole "QUAD.CR1.DBA.QF1" -len 0.4 -S1 [expr -0.664141578*$refen] -refen $refen 
Bpm "BPM.CR1_2.120" -len 0 
Drift "CR1.DBA.DM1_A" -len 1.106904053 
Sextupole "SEXT.CR1.DBA.SNCH1" -len 0.1 -S2 [expr -15.73646496670576/2.38*$refen] -refen $refen 
Drift "CR1.DBA.DM1_B" -len 0.1069040525 
Quadrupole "QUAD.CR1.DBA.QF2" -len 0.4 -S1 [expr 0.5637241356*$refen] -refen $refen 
Bpm "BPM.CR1_2.121" -len 0 
Drift "CR1.DBA.DM2" -len 1.099116763 
Sextupole "SEXT.CR1.DBA.SD0" -len 0.1 -S2 [expr 0*$refen] -refen $refen 
Drift "CR1.DBA.DMSEXT1" -len 0.1 
Quadrupole "QUAD.CR1.DBA.QF3" -len 0.4 -S1 [expr -0.6662844456*$refen] -refen $refen 
Bpm "BPM.CR1_2.122" -len 0 
Drift "CR1.DBA.DM3" -len 0.2563831229 
Drift "CR1.DBA.DMSEXT1" -len 0.1 
Drift "CR1.DBA.D11B2" -len 0.1489315367 
Quadrupole "QUAD.CR1.DBA.QI2" -len 0.4 -S1 [expr 0.66922795*$refen] -refen $refen 
Bpm "BPM.CR1_2.123" -len 0 
Drift "CR1.DBA.D12" -len 0.2467904078 
Sbend "BEND.CR1.DBA.DIPOLE0" -len 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -S1 [expr 0.05125499604*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*0.2983805661*0.2983805661/2.801278682*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "CR1.DBA.DS1C_A" -len 0.2929622945 
Sextupole "SEXT.CR1.DBA.SN1.5" -len 0.1 -S2 [expr -12.54315381405628/2.38*$refen] -refen $refen 
Drift "CR1.DBA.DS1C_B" -len 0.1 
Quadrupole "QUAD.CR1.DBA.QS1C" -len 0.4 -S1 [expr -0.450384*$refen] -refen $refen 
Bpm "BPM.CR1_2.124" -len 0 
Drift "CR1.DBA.DS2C" -len 0.5664710132 
Sbend "BEND.CR1.DBA.DIPN0" -len 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -S1 [expr 0.1995945832*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "CR1.DBA.DS3C_A" -len 0.1426363128 
Sextupole "SEXT.CR1.DBA.SNN3.5" -len 0.1 -S2 [expr 4.816144769464445/2.38*$refen] -refen $refen 
Drift "CR1.DBA.DS3C_B" -len 0.1426363128 
Quadrupole "QUAD.CR1.DBA.QS2C" -len 0.2 -S1 [expr 0.2323546236*$refen] -refen $refen 
Bpm "BPM.CR1_2.125" -len 0 
Quadrupole "QUAD.CR1.DBA.QS2C" -len 0.2 -S1 [expr 0.2323546236*$refen] -refen $refen 
Bpm "BPM.CR1_2.126" -len 0 
Drift "CR1.DBA.DS3C_B" -len 0.1426363128 
Sextupole "SEXT.CR1.DBA.SN3.5" -len 0.1 -S2 [expr -1.003837719641216/2.38*$refen] -refen $refen 
Drift "CR1.DBA.DS3C_A" -len 0.1426363128 
Sbend "BEND.CR1.DBA.DIPN0" -len 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -S1 [expr 0.1995945832*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "CR1.DBA.DS2C" -len 0.5664710132 
Quadrupole "QUAD.CR1.DBA.QS1C" -len 0.4 -S1 [expr -0.450384*$refen] -refen $refen 
Bpm "BPM.CR1_2.127" -len 0 
Drift "CR1.DBA.DS1C_B" -len 0.1 
Sextupole "SEXT.CR1.DBA.SN0.5" -len 0.1 -S2 [expr 9.564860811679351/2.38*$refen] -refen $refen 
Drift "CR1.DBA.DS1C_A" -len 0.2929622945 
Sbend "BEND.CR1.DBA.DIPOLE0" -len 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -S1 [expr 0.05125499604*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*0.2983805661*0.2983805661/2.801278682*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "CR1.DBA.D12" -len 0.2467904078 
Quadrupole "QUAD.CR1.DBA.QI2" -len 0.4 -S1 [expr 0.66922795*$refen] -refen $refen 
Bpm "BPM.CR1_2.128" -len 0 
Drift "CR1.DBA.D11B" -len 0.1989315367 
Drift "CR1.DBA.D11A" -len 0.2082887778 
Quadrupole "QUAD.CR1.DBA.QI1" -len 0.4 -S1 [expr -0.3358875195*$refen] -refen $refen 
Bpm "BPM.CR1_2.129" -len 0 
Marker "CR1.DBA.MCELL" 
Marker "CR1MDBACELLEND" 
Drift "CR1.DS1C_A" -len 1.95 
Sextupole "SEXT.CR1.SX1CC" -len 0.1 -S2 [expr -6.303712536940822/2.38*$refen] -refen $refen 
Drift "CR1.DS1C_B" -len 0.95 
Quadrupole "QUAD.CR1.QS1C" -len 0.5 -S1 [expr -1.148329543780627/2.38*$refen] -refen $refen 
Bpm "BPM.CR1_2.130" -len 0 
Drift "CR1.DS2C" -len 0.5 
Quadrupole "QUAD.CR1.QS2C" -len 0.5 -S1 [expr 0*$refen] -refen $refen 
Bpm "BPM.CR1_2.131" -len 0 
Drift "CR1.DS3C_A" -len 0.4 
Sextupole "SEXT.CR1.SX3CC" -len 0.1 -S2 [expr 9.18261175582685/2.38*$refen] -refen $refen 
Drift "CR1.DS3C_B" -len 0.4 
Quadrupole "QUAD.CR1.QS3C" -len 0.5 -S1 [expr 1.602732521787652/2.38*$refen] -refen $refen 
Bpm "BPM.CR1_2.132" -len 0 
Drift "CR1.DS4C" -len 0.5 
Quadrupole "QUAD.CR1.QS4C" -len 0.5 -S1 [expr 0*$refen] -refen $refen 
Bpm "BPM.CR1_2.133" -len 0 
Drift "CR1.DS5C" -len 0.5 
Quadrupole "QUAD.CR1.QS5C" -len 0.5 -S1 [expr -1.751010177471778/2.38*$refen] -refen $refen 
Bpm "BPM.CR1_2.134" -len 0 
Drift "CR1.DS6C" -len 0.5 
Drift "CR1.WIG1" -len 0.8 
Drift "CR1.WIG2" -len 0.8 
Drift "CR1.DS6C" -len 0.5 
Quadrupole "QUAD.CR1.QS5C" -len 0.5 -S1 [expr -1.751010177471778/2.38*$refen] -refen $refen 
Bpm "BPM.CR1_2.135" -len 0 
Drift "CR1.DS5C" -len 0.5 
Quadrupole "QUAD.CR1.QS4C" -len 0.5 -S1 [expr 0*$refen] -refen $refen 
Bpm "BPM.CR1_2.136" -len 0 
Drift "CR1.DS4C" -len 0.5 
Quadrupole "QUAD.CR1.QS3C" -len 0.5 -S1 [expr 1.602732521787652/2.38*$refen] -refen $refen 
Bpm "BPM.CR1_2.137" -len 0 
Drift "CR1.DS3C_B" -len 0.4 
Sextupole "SEXT.CR1.SX3CC" -len 0.1 -S2 [expr 9.18261175582685/2.38*$refen] -refen $refen 
Drift "CR1.DS3C_A" -len 0.4 
Quadrupole "QUAD.CR1.QS2C" -len 0.5 -S1 [expr 0*$refen] -refen $refen 
Bpm "BPM.CR1_2.138" -len 0 
Drift "CR1.DS2C" -len 0.5 
Quadrupole "QUAD.CR1.QS1C" -len 0.5 -S1 [expr -1.148329543780627/2.38*$refen] -refen $refen 
Bpm "BPM.CR1_2.139" -len 0 
Drift "CR1.DS1C_B" -len 0.95 
Sextupole "SEXT.CR1.SX1CC" -len 0.1 -S2 [expr -6.303712536940822/2.38*$refen] -refen $refen 
Drift "CR1.DS1C_A" -len 1.95 
Marker "CR1MDBACELLSTART" 
Marker "CR1.DBA.MCELL" 
Quadrupole "QUAD.CR1.DBA.QI1" -len 0.4 -S1 [expr -0.3358875195*$refen] -refen $refen 
Bpm "BPM.CR1_2.140" -len 0 
Drift "CR1.DBA.D11A" -len 0.2082887778 
Drift "CR1.DBA.D11B" -len 0.1989315367 
Quadrupole "QUAD.CR1.DBA.QI2" -len 0.4 -S1 [expr 0.66922795*$refen] -refen $refen 
Bpm "BPM.CR1_2.141" -len 0 
Drift "CR1.DBA.D12" -len 0.2467904078 
Sbend "BEND.CR1.DBA.DIPOLE0" -len 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -S1 [expr 0.05125499604*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*0.2983805661*0.2983805661/2.801278682*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "CR1.DBA.DS1C_A" -len 0.2929622945 
Sextupole "SEXT.CR1.DBA.SN0.6" -len 0.1 -S2 [expr -8.861196470525726/2.38*$refen] -refen $refen 
Drift "CR1.DBA.DS1C_B" -len 0.1 
Marker "CR1.DBA.HCM0" 
Quadrupole "QUAD.CR1.DBA.QS1C" -len 0.4 -S1 [expr -0.450384*$refen] -refen $refen 
Bpm "BPM.CR1_2.142" -len 0 
Drift "CR1.DBA.DS2C" -len 0.5664710132 
Sbend "BEND.CR1.DBA.DIPN0" -len 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -S1 [expr 0.1995945832*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Marker "CR1.DBA.HBM" 
Drift "CR1.DBA.DS3C_A" -len 0.1426363128 
Sextupole "SEXT.CR1.DBA.SN3.6" -len 0.1 -S2 [expr -1.390396808193952/2.38*$refen] -refen $refen 
Drift "CR1.DBA.DS3C_B" -len 0.1426363128 
Quadrupole "QUAD.CR1.DBA.QS2C" -len 0.2 -S1 [expr 0.2323546236*$refen] -refen $refen 
Bpm "BPM.CR1_2.143" -len 0 
Quadrupole "QUAD.CR1.DBA.QS2C" -len 0.2 -S1 [expr 0.2323546236*$refen] -refen $refen 
Bpm "BPM.CR1_2.144" -len 0 
Drift "CR1.DBA.DS3C_B" -len 0.1426363128 
Sextupole "SEXT.CR1.DBA.SNN3.6" -len 0.1 -S2 [expr 7.305058639413724/2.38*$refen] -refen $refen 
Drift "CR1.DBA.DS3C_A" -len 0.1426363128 
Marker "CR1.DBA.HBM2" 
Sbend "BEND.CR1.DBA.DIPN0" -len 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -S1 [expr 0.1995945832*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "CR1.DBA.DS2C" -len 0.5664710132 
Quadrupole "QUAD.CR1.DBA.QS1C" -len 0.4 -S1 [expr -0.450384*$refen] -refen $refen 
Bpm "BPM.CR1_2.145" -len 0 
Marker "CR1.DBA.HCM" 
Drift "CR1.DBA.DS1C_B" -len 0.1 
Sextupole "SEXT.CR1.DBA.SN1.6" -len 0.1 -S2 [expr -16.55351124662135/2.38*$refen] -refen $refen 
Drift "CR1.DBA.DS1C_A" -len 0.2929622945 
Sbend "BEND.CR1.DBA.DIPOLE0" -len 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -S1 [expr 0.05125499604*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*0.2983805661*0.2983805661/2.801278682*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "CR1.DBA.D12" -len 0.2467904078 
Quadrupole "QUAD.CR1.DBA.QI2" -len 0.4 -S1 [expr 0.66922795*$refen] -refen $refen 
Bpm "BPM.CR1_2.146" -len 0 
Drift "CR1.DBA.D11B1" -len 0.3133083062 
Sextupole "SEXT.CR1.DBA.SD0" -len 0.1 -S2 [expr 0*$refen] -refen $refen 
Drift "CR1.DBA.DM0" -len 0.1 
Quadrupole "QUAD.CR1.DBA.QF1" -len 0.4 -S1 [expr -0.664141578*$refen] -refen $refen 
Bpm "BPM.CR1_2.147" -len 0 
Drift "CR1.DBA.DM1_A" -len 1.106904053 
Sextupole "SEXT.CR1.DBA.SNCH1" -len 0.1 -S2 [expr -15.73646496670576/2.38*$refen] -refen $refen 
Drift "CR1.DBA.DM1_B" -len 0.1069040525 
Quadrupole "QUAD.CR1.DBA.QF2" -len 0.4 -S1 [expr 0.5637241356*$refen] -refen $refen 
Bpm "BPM.CR1_2.148" -len 0 
Drift "CR1.DBA.DM2" -len 1.099116763 
Sextupole "SEXT.CR1.DBA.SD0" -len 0.1 -S2 [expr 0*$refen] -refen $refen 
Drift "CR1.DBA.DMSEXT1" -len 0.1 
Quadrupole "QUAD.CR1.DBA.QF3" -len 0.4 -S1 [expr -0.6662844456*$refen] -refen $refen 
Bpm "BPM.CR1_2.149" -len 0 
Drift "CR1.DBA.DM3" -len 0.2563831229 
Drift "CR1.DBA.DMSEXT1" -len 0.1 
Drift "CR1.DBA.D11B2" -len 0.1489315367 
Quadrupole "QUAD.CR1.DBA.QI2" -len 0.4 -S1 [expr 0.66922795*$refen] -refen $refen 
Bpm "BPM.CR1_2.150" -len 0 
Drift "CR1.DBA.D12" -len 0.2467904078 
Sbend "BEND.CR1.DBA.DIPOLE0" -len 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -S1 [expr 0.05125499604*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*0.2983805661*0.2983805661/2.801278682*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "CR1.DBA.DS1C_A" -len 0.2929622945 
Sextupole "SEXT.CR1.DBA.SN1.6" -len 0.1 -S2 [expr -16.55351124662135/2.38*$refen] -refen $refen 
Drift "CR1.DBA.DS1C_B" -len 0.1 
Quadrupole "QUAD.CR1.DBA.QS1C" -len 0.4 -S1 [expr -0.450384*$refen] -refen $refen 
Bpm "BPM.CR1_2.151" -len 0 
Drift "CR1.DBA.DS2C" -len 0.5664710132 
Sbend "BEND.CR1.DBA.DIPN0" -len 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -S1 [expr 0.1995945832*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "CR1.DBA.DS3C_A" -len 0.1426363128 
Sextupole "SEXT.CR1.DBA.SNN3.6" -len 0.1 -S2 [expr 7.305058639413724/2.38*$refen] -refen $refen 
Drift "CR1.DBA.DS3C_B" -len 0.1426363128 
Quadrupole "QUAD.CR1.DBA.QS2C" -len 0.2 -S1 [expr 0.2323546236*$refen] -refen $refen 
Bpm "BPM.CR1_2.152" -len 0 
Quadrupole "QUAD.CR1.DBA.QS2C" -len 0.2 -S1 [expr 0.2323546236*$refen] -refen $refen 
Bpm "BPM.CR1_2.153" -len 0 
Drift "CR1.DBA.DS3C_B" -len 0.1426363128 
Sextupole "SEXT.CR1.DBA.SN3.6" -len 0.1 -S2 [expr -1.390396808193952/2.38*$refen] -refen $refen 
Drift "CR1.DBA.DS3C_A" -len 0.1426363128 
Sbend "BEND.CR1.DBA.DIPN0" -len 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -S1 [expr 0.1995945832*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "CR1.DBA.DS2C" -len 0.5664710132 
Quadrupole "QUAD.CR1.DBA.QS1C" -len 0.4 -S1 [expr -0.450384*$refen] -refen $refen 
Bpm "BPM.CR1_2.154" -len 0 
Drift "CR1.DBA.DS1C_B" -len 0.1 
Sextupole "SEXT.CR1.DBA.SN0.6" -len 0.1 -S2 [expr -8.861196470525726/2.38*$refen] -refen $refen 
Drift "CR1.DBA.DS1C_A" -len 0.2929622945 
Sbend "BEND.CR1.DBA.DIPOLE0" -len 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -S1 [expr 0.05125499604*$refen] -refen $refen 
set refen [expr $refen-14.1e-6*0.2983805661*0.2983805661/2.801278682*$e0*$e0*$e0*$e0*$Dipole_synrad] 
### End of Arc 6 ### 
Drift "CR1.DBA.D12" -len 0.2467904078 
Quadrupole "QUAD.CR1.DBA.QI2" -len 0.4 -S1 [expr 0.66922795*$refen] -refen $refen 
Bpm "BPM.CR1_2.155" -len 0 
Drift "CR1.DBA.D11B" -len 0.1989315367 
Drift "CR1.DBA.D11A" -len 0.2082887778 
Quadrupole "QUAD.CR1.DBA.QI1" -len 0.4 -S1 [expr -0.3358875195*$refen] -refen $refen 
Bpm "BPM.CR1_2.156" -len 0 
Marker "CR1.DBA.MCELL" 
Marker "CR1MDBACELLEND"
### End of Arc 6 ### 
### Missing length correction ###
Drift "CR1.DMISSLEN" -len 1.66137098404886 
Sextupole "SEXT.CR1.SXL0IN" -len 0.5 -S2 [expr 0*$refen] -refen $refen 
Drift "CR1.DL0IN" -len 0.5
###
Quadrupole "QUAD.CR1.QL1IN" -len 0.5 -S1 [expr -1.74934542095025/2.38*$refen] -refen $refen 
Bpm "BPM.CR1_2.157" -len 0 
Drift "CR1.DL1IN" -len 0.3098482 
Quadrupole "QUAD.CR1.QL2IN" -len 0.5 -S1 [expr 2.841774878265831/2.38*$refen] -refen $refen 
Bpm "BPM.CR1_2.158" -len 0 
Drift "CR1.DL2IN" -len 0.3010372 
Quadrupole "QUAD.CR1.QL3IN" -len 0.5 -S1 [expr -1.137832040354124/2.38*$refen] -refen $refen 
Bpm "BPM.CR1_2.159" -len 0 
Drift "CR1.DL3IN" -len 1.099258 
Drift "CR1.DRRFL" -len 0.3 
##### Static Dipole #######
Rfmultipole "RFMulti.CR1.RFDEFLCOMP2" -len 0.2 -strength [expr 0.00095*$refen] -refen $refen -freq 0 
Drift "CR1.DRRFL1" -len 0.1 
##### Static Dipole #######
Rfmultipole "RFMulti.CR1.RFDEFLB" -len 1.147353938 -strength [expr -0.0019*$refen] -refen $refen -freq 0 
Drift "CR1.DRRFL1" -len 0.1 
##### Static Dipole #######
Rfmultipole "RFMulti.CR1.RFDEFLCOMP2" -len 0.2 -strength [expr 0.00095*$refen] -refen $refen -freq 0 
Drift "CR1.DRRFL2" -len 0.45265 
Drift "CR1.DL4IN" -len 0.349996062 
Quadrupole "QUAD.CR1.QL4IN" -len 0.5 -S1 [expr 1.520443016944819/2.38*$refen] -refen $refen 
Bpm "BPM.CR1_2.160" -len 0 
Drift "CR1.DL5IN" -len 1 
Quadrupole "QUAD.CR1.QL5IN" -len 0.5 -S1 [expr -2.15770259424327/2.38*$refen] -refen $refen 
Bpm "BPM.CR1_2.161" -len 0 
Drift "CR1.DL6IN" -len 0.553 
Quadrupole "QUAD.CR1.QL6IN" -len 0.25 -S1 [expr 1.193855820504099/2.38*$refen] -refen $refen 
Bpm "BPM.CR1_2.162" -len 0 
Drift "CR1.DL7IN" -len 0.5 
Drift "CR1.INJSEPTUM" -len 1  
Kalign CR1_zoff -fixed 1 -cz 4.988374294e-05
#Kalign CR1_zoff -cz 0
Drift "CR1_P2.EJECTION" -len 0 -node 1 -kick CR1_zoff
