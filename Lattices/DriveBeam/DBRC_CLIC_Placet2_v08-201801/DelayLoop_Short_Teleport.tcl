## Short path of the delay loop 
## No lattice right now... Twiss parameters should be the same so... 
## for now, just putting a Identity tensor (doesn't chance the phase space). 
# Length of the Long Path: 143.3865345297 
 
## Reference energy taken from list_energies.tcl (Computed from Eduardo Martin's Lattice) 
## This was chosen to match the less energetic "bunch type" (there are 24 different possible bunch energies). 
## Which means there will be a slight magnet missfocus for the other bunches. 
if {$Dipole_synrad} { 
	set refen 2.379985336493252 
} else { 
	set refen 2.38 
} 
Marker "DL_SHORT.INJECTION" 
Kalign DLshort_inj -cx 0. -cxp 0. 
Drift "DL_SHORT.CENTERINJ" -len 0. -nodes 1 -kick DLshort_inj 
# The Seta structure needs to be equivalent to the long path, this needs to take into 
# account the difference in path between the the arc and dritf sections of each septum
# Septa structure geometry details computed in /Lattices/DBRC_CLIC_Placet2_v06/DL_sept.m
Tensor "SEPTQ.IN" -len 0.500000712700000 
Tensor "DL1IN.IN" -len 1.09993292833525 
Tensor "SEPT1.IN" -len 0.998721902209744 
Tensor "DL2IN.IN" -len 0.298491153110126 
### This would have to be a special quadrupole in which the short-orbit passed between the poles (separation between orbits is 0.111973003318050 m)
Tensor "QL1IN.IN" -len 0.397988204146835 
#Bpm "BPM.DL_L.1" -len 0 
Tensor "DL3IN.IN" -len 0.397988204146835
Tensor "SEPT2.IN" -len 2.54563869663768 

# Total length computed at 30/03/2016 as [expr $DelayLoop_long_length-$train_length]
# for 0.49975 GHz with the phase shift every 122 bunches: 70.2005818012995 
Tensor "DL.SHORT" -len [expr 70.2005818012995 - 2.*6.23876180128646]
 
# The Seta structure needs to be equivalent to the long path, this needs to take into 
# account the difference in path between the the arc and dritf sections of each septum
# Septa structure geometry details computed in /Lattices/DBRC_CLIC_Placet2_v06/DL_sept.m
Tensor "SEPT2.IN" -len 2.54563869663768 
Tensor "DL3IN.IN" -len 0.397988204146835
#Bpm "BPM.DL_L.1" -len 0 
### This would have to be a special quadrupole in which the short-orbit passed between the poles (separation between orbits is 0.111973003318050 m)
Tensor "QL1IN.IN" -len 0.397988204146835 
Tensor "DL2IN.IN" -len 0.298491153110126 
Tensor "SEPT1.IN" -len 0.998721902209744 
Tensor "DL1IN.IN" -len 1.09993292833525 
Tensor "SEPTQ.IN" -len 0.500000712700000 
### Changing the referential to match TL1's, in which the beam eners with an angle
Kalign DLshort_ext -cx 0.01425 -cxp -0.0075  
Drift "DL_SHORT.CENTEREXT" -len 0. -nodes 1 -kick DLshort_ext 

Marker "DL_SHORT.EXTRACTION"
# WARNING: The minimum path (straigth line between septa) allowed by the current Long Path geometry is 69.1960671984 

