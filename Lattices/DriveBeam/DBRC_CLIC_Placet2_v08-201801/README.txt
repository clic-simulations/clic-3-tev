Current issues with the Lattice:

 - DelayLoop_Short lattice doesnt exist
 - DelayLoop_Long and TL1 are lightly optimized
 - Septum regions have not been verified as feasable (separation and angle)
 - CR1 slightly overcompensates T566
 - Kalign in TL2 injection needs to be a fixed value
 - Kalign in TL2 extraction needs to be a fixed value
 - CR1 needs to match the 3 bunch types better
 - CR2 optimized only for bunch type 2 out of CR1
 - DBRC_End has not been optimized
 - CR2 extraction kicker needs to be ON
 - CR2 bump does not have a large septum region


