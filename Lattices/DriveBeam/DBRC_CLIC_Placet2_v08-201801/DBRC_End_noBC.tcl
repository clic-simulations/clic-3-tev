## Reference energy taken from list_energies.tcl (Computed from Eduardo Martin's Lattice) 
## This was chosen to match the less energetic "bunch type" (there are 24 different possible bunch energies). 
## Which means there will be a slight magnet missfocus for the other bunches. 
if {$Dipole_synrad} { 
	set refen 2.3771062678575574 
} else { 
	set refen 2.38 
} 
Marker "DBRC_END.INJECTION" 
### The beam exits CR2 with an offset in the combiner ring's referencial,
### This kicker puts it in TL3's referential 
#Kalign TL3offset -cx 0. -cxp 0. 
Kalign TL3offset -fixed 1 -cx 2.73759087781738e-02 -cxp 1.53635688069138e-03 
Marker "TL3_offset" -nodes 1 -kick TL3offset 
Bpm "BPM.END.0" -len 0 
Sbend "BEND.TL3.BEND10" -len 2.01146240647403 -angle 0.2617993878 -E1 0.1308996939 -E2 0.1308996939 -refen $refen 
set refen [expr $refen-14.1e-6*0.2617993878*0.2617993878/2.01146240647403*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TL3.D122" -len 1.9174 
Sextupole "SEXT.TL3.SX125" -len 0.3 -S2 [expr 1.45031110505809] -refen $refen 
Drift "TL3.D128" -len 0.8837 
Quadrupole "QUAD.TL3.Q13" -len 0.75 -S1 [expr 8.97444309267497e-01] -refen $refen 
Bpm "BPM.END.4" -len 0 
Drift "TL3.D142" -len 0.042885 
Sextupole "SEXT.TL3.SX145" -len 0.3 -S2 [expr -2.96568595707399] -refen $refen 
Drift "TL3.D148" -len 0.042885 
Quadrupole "QUAD.TL3.Q15" -len 0.75 -S1 [expr -1.01445597215018] -refen $refen 
Bpm "BPM.END.5" -len 0 
Drift "TL3.D162" -len 0.0976775 
Drift "TL3.D168" -len 0.0976775 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 0.5000086579 
Sbend "BEND.TL3.BEND17" -len 0.500017315941417 -angle -0.02038562618 -E1 -0.01019281309 -E2 -0.01019281309 -refen $refen 
set refen [expr $refen-14.1e-6*-0.02038562618*-0.02038562618/0.500017315941417*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TL3.D182" -len 0.0001175 
Sextupole "SEXT.TL3.SX185" -len 0.3 -S2 [expr 5.45572114284215e+00] -refen $refen 
Drift "TL3.D188" -len 0.0001175 
Quadrupole "QUAD.TL3.Q20A" -len 0.375 -S1 [expr 4.71776731042729e-01] -refen $refen 
Bpm "BPM.END.6" -len 0 
Marker "TL3.MIDCELLONE" 
Quadrupole "QUAD.TL3.Q20B" -len 0.375 -S1 [expr 4.71776731042729e-01] -refen $refen 
Bpm "BPM.END.7" -len 0 
Drift "TL3.D222" -len 0.1905674726 
Sextupole "SEXT.TL3.SX225" -len 0.3 -S2 [expr 5.07737646403701e-01] -refen $refen 
Drift "TL3.D228" -len 0.1905674726 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 0.5000086579 
Sbend "BEND.TL3.BEND23" -len 0.500017315941417 -angle -0.02038562618 -E1 -0.01019281309 -E2 -0.01019281309 -refen $refen 
set refen [expr $refen-14.1e-6*-0.02038562618*-0.02038562618/0.500017315941417*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TL3.D242" -len 0.05000000025 
Drift "TL3.D248" -len 0.05000000025 
Quadrupole "QUAD.TL3.Q25" -len 0.75 -S1 [expr -1.10707927452722] -refen $refen 
Bpm "BPM.END.8" -len 0 
Drift "TL3.D262" -len 0.2203857562 
Sextupole "SEXT.TL3.SX265" -len 0.3 -S2 [expr -9.77818824191842] -refen $refen 
Drift "TL3.D268" -len 0.2203857562 
Quadrupole "QUAD.TL3.Q27" -len 0.75 -S1 [expr 9.76448958569761e-01] -refen $refen 
Bpm "BPM.END.9" -len 0 
Drift "TL3.D282" -len 0.7273957003 
Sextupole "SEXT.TL3.SX285" -len 0.3 -S2 [expr 1.43214213246393e+01] -refen $refen 
Drift "TL3.D288" -len 1.604791401 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.005723015 
Sbend "BEND.TL3.BEND30" -len 2.01146240647403 -angle 0.2617993878 -E1 0.1308996939 -E2 0.1308996939 -refen $refen 
set refen [expr $refen-14.1e-6*0.2617993878*0.2617993878/2.01146240647403*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Marker "TL3.EJECTION"  
Drift "TL3.D71" -len 0.5 
Quadrupole "QUAD.TL3.Q72" -len 0.75 -S1 [expr -9.36256859489391e-01] -refen $refen 
Bpm "BPM.END.10" -len 0 
Drift "TL3.D73" -len 0.5 
Quadrupole "QUAD.TL3.Q74" -len 0.75 -S1 [expr 1.32239980689931] -refen $refen 
Bpm "BPM.END.11" -len 0 
Drift "TL3.D75" -len 2.19951 
Quadrupole "QUAD.TL3.Q76" -len 0.75 -S1 [expr -4.54721256696498e-01] -refen $refen 
Bpm "BPM.END.12" -len 0 
Drift "TL3.D77" -len 0.5 
Quadrupole "QUAD.TL3.Q78" -len 0.75 -S1 [expr -4.23719226229484e-01] -refen $refen 
Bpm "BPM.END.13" -len 0 
Drift "TL3.D79" -len 0.150751 
Bpm "BPM.END.TTA" -len 0 
Drift "TTA.D2M" -len 2 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -S2 [expr -1.86448311441455e+01] -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -S1 [expr 0.6863003356*$refen] -refen $refen 
Bpm "BPM.END.14" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -S1 [expr -0.6004214291*$refen] -refen $refen 
Bpm "BPM.END.15" -len 0 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -S2 [expr 2.19370775767821e+01] -refen $refen 
Drift "TTA.D3B" -len 3.077193097 
Drift "TTA.D3C" -len 0.1 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -S1 [expr 0.3042074493*$refen] -refen $refen 
Bpm "BPM.END.16" -len 0 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -S2 [expr -1.16237357135490e+01] -refen $refen 
Drift "TTA.D4B" -len 0.3 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -S2 [expr -1.16237357135490e+01] -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -S1 [expr 0.3042074493*$refen] -refen $refen 
Bpm "BPM.END.17" -len 0 
Drift "TTA.D3C" -len 0.1 
Drift "TTA.D3B" -len 3.077193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -S2 [expr 2.19370775767821e+01] -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -S1 [expr -0.6004214291*$refen] -refen $refen 
Bpm "BPM.END.18" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -S1 [expr 0.6863003356*$refen] -refen $refen 
Bpm "BPM.END.19" -len 0 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -S2 [expr -1.86448311441455e+01] -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Marker "TTA.MBEND3" 
Drift "TTA.D2M" -len 2 
Marker "TTA.EXICELL" 
Drift "TTA.D2M" -len 2 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -S2 [expr -1.86448311441455e+01] -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -S1 [expr 0.6863003356*$refen] -refen $refen 
Bpm "BPM.END.20" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -S1 [expr -0.6004214291*$refen] -refen $refen 
Bpm "BPM.END.21" -len 0 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -S2 [expr 2.19370775767821e+01] -refen $refen 
Drift "TTA.D3B" -len 3.077193097 
Drift "TTA.D3C" -len 0.1 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -S1 [expr 0.3042074493*$refen] -refen $refen 
Bpm "BPM.END.22" -len 0 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -S2 [expr -1.16237357135490e+01] -refen $refen 
Drift "TTA.D4B" -len 0.3 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -S2 [expr -1.16237357135490e+01] -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -S1 [expr 0.3042074493*$refen] -refen $refen 
Bpm "BPM.END.23" -len 0 
Drift "TTA.D3C" -len 0.1 
Drift "TTA.D3B" -len 3.077193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -S2 [expr 2.19370775767821e+01] -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -S1 [expr -0.6004214291*$refen] -refen $refen 
Bpm "BPM.END.24" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -S1 [expr 0.6863003356*$refen] -refen $refen 
Bpm "BPM.END.25" -len 0 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -S2 [expr -1.86448311441455e+01] -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Marker "TTA.MBEND3" 
Drift "TTA.D2M" -len 2 
Marker "TTA.EXICELL" 
Drift "TTA.D2M" -len 2 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -S2 [expr -1.86448311441455e+01] -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -S1 [expr 0.6863003356*$refen] -refen $refen 
Bpm "BPM.END.26" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -S1 [expr -0.6004214291*$refen] -refen $refen 
Bpm "BPM.END.27" -len 0 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -S2 [expr 2.19370775767821e+01] -refen $refen 
Drift "TTA.D3B" -len 3.077193097 
Drift "TTA.D3C" -len 0.1 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -S1 [expr 0.3042074493*$refen] -refen $refen 
Bpm "BPM.END.28" -len 0 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -S2 [expr -1.16237357135490e+01] -refen $refen 
Drift "TTA.D4B" -len 0.3 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -S2 [expr -1.16237357135490e+01] -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -S1 [expr 0.3042074493*$refen] -refen $refen 
Bpm "BPM.END.29" -len 0 
Drift "TTA.D3C" -len 0.1 
Drift "TTA.D3B" -len 3.077193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -S2 [expr 2.19370775767821e+01] -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -S1 [expr -0.6004214291*$refen] -refen $refen 
Bpm "BPM.END.30" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -S1 [expr 0.6863003356*$refen] -refen $refen 
Bpm "BPM.END.31" -len 0 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -S2 [expr -1.86448311441455e+01] -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Marker "TTA.MBEND3" 
Drift "TTA.D2M" -len 2 
Marker "TTA.EXICELL" 
Drift "TTA.D2M" -len 2 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -S2 [expr -1.86448311441455e+01] -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -S1 [expr 0.6863003356*$refen] -refen $refen 
Bpm "BPM.END.32" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -S1 [expr -0.6004214291*$refen] -refen $refen 
Bpm "BPM.END.33" -len 0 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -S2 [expr 2.19370775767821e+01] -refen $refen 
Drift "TTA.D3B" -len 3.077193097 
Drift "TTA.D3C" -len 0.1 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -S1 [expr 0.3042074493*$refen] -refen $refen 
Bpm "BPM.END.34" -len 0 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -S2 [expr -1.16237357135490e+01] -refen $refen 
Drift "TTA.D4B" -len 0.3 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -S2 [expr -1.16237357135490e+01] -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -S1 [expr 0.3042074493*$refen] -refen $refen 
Bpm "BPM.END.35" -len 0 
Drift "TTA.D3C" -len 0.1 
Drift "TTA.D3B" -len 3.077193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -S2 [expr 2.19370775767821e+01] -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -S1 [expr -0.6004214291*$refen] -refen $refen 
Bpm "BPM.END.36" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -S1 [expr 0.6863003356*$refen] -refen $refen 
Bpm "BPM.END.37" -len 0 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -S2 [expr -1.86448311441455e+01] -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Marker "TTA.MBEND3" 
Drift "TTA.D2M" -len 2 
Marker "TTA.EXICELL" 
Drift "TTA.D2M" -len 2 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -S2 [expr -1.86448311441455e+01] -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -S1 [expr 0.6863003356*$refen] -refen $refen 
Bpm "BPM.END.38" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -S1 [expr -0.6004214291*$refen] -refen $refen 
Bpm "BPM.END.39" -len 0 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -S2 [expr 2.19370775767821e+01] -refen $refen 
Drift "TTA.D3B" -len 3.077193097 
Drift "TTA.D3C" -len 0.1 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -S1 [expr 0.3042074493*$refen] -refen $refen 
Bpm "BPM.END.40" -len 0 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -S2 [expr -1.16237357135490e+01] -refen $refen 
Drift "TTA.D4B" -len 0.3 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -S2 [expr -1.16237357135490e+01] -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -S1 [expr 0.3042074493*$refen] -refen $refen 
Bpm "BPM.END.41" -len 0 
Drift "TTA.D3C" -len 0.1 
Drift "TTA.D3B" -len 3.077193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -S2 [expr 2.19370775767821e+01] -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -S1 [expr -0.6004214291*$refen] -refen $refen 
Bpm "BPM.END.42" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -S1 [expr 0.6863003356*$refen] -refen $refen 
Bpm "BPM.END.43" -len 0 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -S2 [expr -1.86448311441455e+01] -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Marker "TTA.MBEND3" 
Drift "TTA.D2M" -len 2 
Marker "TTA.EXICELL" 
Drift "TTA.D2M" -len 2 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -S2 [expr -1.86448311441455e+01] -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -S1 [expr 0.6863003356*$refen] -refen $refen 
Bpm "BPM.END.44" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -S1 [expr -0.6004214291*$refen] -refen $refen 
Bpm "BPM.END.45" -len 0 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -S2 [expr 2.19370775767821e+01] -refen $refen 
Drift "TTA.D3B" -len 3.077193097 
Drift "TTA.D3C" -len 0.1 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -S1 [expr 0.3042074493*$refen] -refen $refen 
Bpm "BPM.END.46" -len 0 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -S2 [expr -1.16237357135490e+01] -refen $refen 
Drift "TTA.D4B" -len 0.3 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -S2 [expr -1.16237357135490e+01] -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -S1 [expr 0.3042074493*$refen] -refen $refen 
Bpm "BPM.END.47" -len 0 
Drift "TTA.D3C" -len 0.1 
Drift "TTA.D3B" -len 3.077193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -S2 [expr 2.19370775767821e+01] -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -S1 [expr -0.6004214291*$refen] -refen $refen 
Bpm "BPM.END.48" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -S1 [expr 0.6863003356*$refen] -refen $refen 
Bpm "BPM.END.49" -len 0 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -S2 [expr -1.86448311441455e+01] -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Marker "TTA.MBEND3" 
Drift "TTA.D2M" -len 2 
Marker "TTA.EXICELL" 
Drift "TTA.D2M" -len 2 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -S2 [expr -1.86448311441455e+01] -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -S1 [expr 0.6863003356*$refen] -refen $refen 
Bpm "BPM.END.50" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -S1 [expr -0.6004214291*$refen] -refen $refen 
Bpm "BPM.END.51" -len 0 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -S2 [expr 2.19370775767821e+01] -refen $refen 
Drift "TTA.D3B" -len 3.077193097 
Drift "TTA.D3C" -len 0.1 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -S1 [expr 0.3042074493*$refen] -refen $refen 
Bpm "BPM.END.52" -len 0 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -S2 [expr -1.16237357135490e+01] -refen $refen 
Drift "TTA.D4B" -len 0.3 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -S2 [expr -1.16237357135490e+01] -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -S1 [expr 0.3042074493*$refen] -refen $refen 
Bpm "BPM.END.53" -len 0 
Drift "TTA.D3C" -len 0.1 
Drift "TTA.D3B" -len 3.077193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -S2 [expr 2.19370775767821e+01] -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -S1 [expr -0.6004214291*$refen] -refen $refen 
Bpm "BPM.END.54" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -S1 [expr 0.6863003356*$refen] -refen $refen 
Bpm "BPM.END.55" -len 0 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -S2 [expr -1.86448311441455e+01] -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Marker "TTA.MBEND3" 
Drift "TTA.D2M" -len 2 
Marker "TTA.EXICELL" 
Drift "TTA.D2M" -len 2 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -S2 [expr -1.86448311441455e+01] -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -S1 [expr 0.6863003356*$refen] -refen $refen 
Bpm "BPM.END.56" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -S1 [expr -0.6004214291*$refen] -refen $refen 
Bpm "BPM.END.57" -len 0 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -S2 [expr 2.19370775767821e+01] -refen $refen 
Drift "TTA.D3B" -len 3.077193097 
Drift "TTA.D3C" -len 0.1 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -S1 [expr 0.3042074493*$refen] -refen $refen 
Bpm "BPM.END.58" -len 0 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -S2 [expr -1.16237357135490e+01] -refen $refen 
Drift "TTA.D4B" -len 0.3 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -S2 [expr -1.16237357135490e+01] -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -S1 [expr 0.3042074493*$refen] -refen $refen 
Bpm "BPM.END.59" -len 0 
Drift "TTA.D3C" -len 0.1 
Drift "TTA.D3B" -len 3.077193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -S2 [expr 2.19370775767821e+01] -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -S1 [expr -0.6004214291*$refen] -refen $refen 
Bpm "BPM.END.60" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -S1 [expr 0.6863003356*$refen] -refen $refen 
Bpm "BPM.END.61" -len 0 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -S2 [expr -1.86448311441455e+01] -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Marker "TTA.MBEND3" 
Drift "TTA.D2M" -len 2 
Marker "TTA.EXICELL" 
Drift "TTA.D2M" -len 2 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -S2 [expr -1.86448311441455e+01] -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -S1 [expr 0.6863003356*$refen] -refen $refen 
Bpm "BPM.END.62" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -S1 [expr -0.6004214291*$refen] -refen $refen 
Bpm "BPM.END.63" -len 0 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -S2 [expr 2.19370775767821e+01] -refen $refen 
Drift "TTA.D3B" -len 3.077193097 
Drift "TTA.D3C" -len 0.1 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -S1 [expr 0.3042074493*$refen] -refen $refen 
Bpm "BPM.END.64" -len 0 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -S2 [expr -1.16237357135490e+01] -refen $refen 
Drift "TTA.D4B" -len 0.3 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -S2 [expr -1.16237357135490e+01] -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -S1 [expr 0.3042074493*$refen] -refen $refen 
Bpm "BPM.END.65" -len 0 
Drift "TTA.D3C" -len 0.1 
Drift "TTA.D3B" -len 3.077193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -S2 [expr 2.19370775767821e+01] -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -S1 [expr -0.6004214291*$refen] -refen $refen 
Bpm "BPM.END.66" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -S1 [expr 0.6863003356*$refen] -refen $refen 
Bpm "BPM.END.67" -len 0 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -S2 [expr -1.86448311441455e+01] -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Marker "TTA.MBEND3" 
Drift "TTA.D2M" -len 2 
Marker "TTA.EXICELL" 
Drift "TTA.D2M" -len 2 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -S2 [expr -1.86448311441455e+01] -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -S1 [expr 0.6863003356*$refen] -refen $refen 
Bpm "BPM.END.68" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -S1 [expr -0.6004214291*$refen] -refen $refen 
Bpm "BPM.END.69" -len 0 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -S2 [expr 2.19370775767821e+01] -refen $refen 
Drift "TTA.D3B" -len 3.077193097 
Drift "TTA.D3C" -len 0.1 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -S1 [expr 0.3042074493*$refen] -refen $refen 
Bpm "BPM.END.70" -len 0 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -S2 [expr -1.16237357135490e+01] -refen $refen 
Drift "TTA.D4B" -len 0.3 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -S2 [expr -1.16237357135490e+01] -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -S1 [expr 0.3042074493*$refen] -refen $refen 
Bpm "BPM.END.71" -len 0 
Drift "TTA.D3C" -len 0.1 
Drift "TTA.D3B" -len 3.077193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -S2 [expr 2.19370775767821e+01] -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -S1 [expr -0.6004214291*$refen] -refen $refen 
Bpm "BPM.END.72" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -S1 [expr 0.6863003356*$refen] -refen $refen 
Bpm "BPM.END.73" -len 0 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -S2 [expr -1.86448311441455e+01] -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Marker "TTA.MBEND3" 
Drift "TTA.D2M" -len 2 
Marker "TTA.EXICELL" 
Drift "TTA.D2M" -len 2 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -S2 [expr -1.86448311441455e+01] -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -S1 [expr 0.6863003356*$refen] -refen $refen 
Bpm "BPM.END.74" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -S1 [expr -0.6004214291*$refen] -refen $refen 
Bpm "BPM.END.75" -len 0 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -S2 [expr 2.19370775767821e+01] -refen $refen 
Drift "TTA.D3B" -len 3.077193097 
Drift "TTA.D3C" -len 0.1 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -S1 [expr 0.3042074493*$refen] -refen $refen 
Bpm "BPM.END.76" -len 0 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -S2 [expr -1.16237357135490e+01] -refen $refen 
Drift "TTA.D4B" -len 0.3 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -S2 [expr -1.16237357135490e+01] -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -S1 [expr 0.3042074493*$refen] -refen $refen 
Bpm "BPM.END.77" -len 0 
Drift "TTA.D3C" -len 0.1 
Drift "TTA.D3B" -len 3.077193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -S2 [expr 2.19370775767821e+01] -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -S1 [expr -0.6004214291*$refen] -refen $refen 
Bpm "BPM.END.78" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -S1 [expr 0.6863003356*$refen] -refen $refen 
Bpm "BPM.END.79" -len 0 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -S2 [expr -1.86448311441455e+01] -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Marker "TTA.MBEND3" 
Drift "TTA.D2M" -len 2 
Marker "TTA.EXICELL" 
Drift "TTA.D2M" -len 2 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -S2 [expr -1.86448311441455e+01] -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -S1 [expr 0.6863003356*$refen] -refen $refen 
Bpm "BPM.END.80" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -S1 [expr -0.6004214291*$refen] -refen $refen 
Bpm "BPM.END.81" -len 0 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -S2 [expr 2.19370775767821e+01] -refen $refen 
Drift "TTA.D3B" -len 3.077193097 
Drift "TTA.D3C" -len 0.1 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -S1 [expr 0.3042074493*$refen] -refen $refen 
Bpm "BPM.END.82" -len 0 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -S2 [expr -1.16237357135490e+01] -refen $refen 
Drift "TTA.D4B" -len 0.3 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -S2 [expr -1.16237357135490e+01] -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -S1 [expr 0.3042074493*$refen] -refen $refen 
Bpm "BPM.END.83" -len 0 
Drift "TTA.D3C" -len 0.1 
Drift "TTA.D3B" -len 3.077193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -S2 [expr 2.19370775767821e+01] -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -S1 [expr -0.6004214291*$refen] -refen $refen 
Bpm "BPM.END.84" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -S1 [expr 0.6863003356*$refen] -refen $refen 
Bpm "BPM.END.85" -len 0 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -S2 [expr -1.86448311441455e+01] -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Marker "TTA.MBEND3" 
Drift "TTA.D2M" -len 2 
Marker "TTA.EXICELL" 
Drift "TTA.D2M" -len 2 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -S2 [expr -1.86448311441455e+01] -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -S1 [expr 0.6863003356*$refen] -refen $refen 
Bpm "BPM.END.86" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -S1 [expr -0.6004214291*$refen] -refen $refen 
Bpm "BPM.END.87" -len 0 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -S2 [expr 2.19370775767821e+01] -refen $refen 
Drift "TTA.D3B" -len 3.077193097 
Drift "TTA.D3C" -len 0.1 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -S1 [expr 0.3042074493*$refen] -refen $refen 
Bpm "BPM.END.88" -len 0 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -S2 [expr -1.16237357135490e+01] -refen $refen 
Drift "TTA.D4B" -len 0.3 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -S2 [expr -1.16237357135490e+01] -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -S1 [expr 0.3042074493*$refen] -refen $refen 
Bpm "BPM.END.89" -len 0 
Drift "TTA.D3C" -len 0.1 
Drift "TTA.D3B" -len 3.077193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -S2 [expr 2.19370775767821e+01] -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -S1 [expr -0.6004214291*$refen] -refen $refen 
Bpm "BPM.END.90" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -S1 [expr 0.6863003356*$refen] -refen $refen 
Bpm "BPM.END.91" -len 0 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -S2 [expr -1.86448311441455e+01] -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Marker "TTA.MBEND3" 
Drift "TTA.D2M" -len 2 
Marker "TTA.EXICELL" 
Drift "TTA.D2M" -len 2 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -S2 [expr -1.86448311441455e+01] -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -S1 [expr 0.6863003356*$refen] -refen $refen 
Bpm "BPM.END.92" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -S1 [expr -0.6004214291*$refen] -refen $refen 
Bpm "BPM.END.93" -len 0 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -S2 [expr 2.19370775767821e+01] -refen $refen 
Drift "TTA.D3B" -len 3.077193097 
Drift "TTA.D3C" -len 0.1 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -S1 [expr 0.3042074493*$refen] -refen $refen 
Bpm "BPM.END.94" -len 0 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -S2 [expr -1.16237357135490e+01] -refen $refen 
Drift "TTA.D4B" -len 0.3 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -S2 [expr -1.16237357135490e+01] -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -S1 [expr 0.3042074493*$refen] -refen $refen 
Bpm "BPM.END.95" -len 0 
Drift "TTA.D3C" -len 0.1 
Drift "TTA.D3B" -len 3.077193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -S2 [expr 2.19370775767821e+01] -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -S1 [expr -0.6004214291*$refen] -refen $refen 
Bpm "BPM.END.96" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -S1 [expr 0.6863003356*$refen] -refen $refen 
Bpm "BPM.END.97" -len 0 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -S2 [expr -1.86448311441455e+01] -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Marker "TTA.MBEND3" 
Drift "TTA.D2M" -len 2 
Marker "TTA.EXICELL" 
Drift "TTA.D2M" -len 2 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -S2 [expr -1.86448311441455e+01] -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -S1 [expr 0.6863003356*$refen] -refen $refen 
Bpm "BPM.END.98" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -S1 [expr -0.6004214291*$refen] -refen $refen 
Bpm "BPM.END.99" -len 0 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -S2 [expr 2.19370775767821e+01] -refen $refen 
Drift "TTA.D3B" -len 3.077193097 
Drift "TTA.D3C" -len 0.1 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -S1 [expr 0.3042074493*$refen] -refen $refen 
Bpm "BPM.END.100" -len 0 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -S2 [expr -1.16237357135490e+01] -refen $refen 
Drift "TTA.D4B" -len 0.3 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -S2 [expr -1.16237357135490e+01] -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -S1 [expr 0.3042074493*$refen] -refen $refen 
Bpm "BPM.END.101" -len 0 
Drift "TTA.D3C" -len 0.1 
Drift "TTA.D3B" -len 3.077193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -S2 [expr 2.19370775767821e+01] -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -S1 [expr -0.6004214291*$refen] -refen $refen 
Bpm "BPM.END.102" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -S1 [expr 0.6863003356*$refen] -refen $refen 
Bpm "BPM.END.103" -len 0 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -S2 [expr -1.86448311441455e+01] -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Marker "TTA.MBEND3" 
Drift "TTA.D2M" -len 2 
Marker "TTA.EXICELL" 
Drift "TTA.D2M" -len 2 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -S2 [expr -1.86448311441455e+01] -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -S1 [expr 0.6863003356*$refen] -refen $refen 
Bpm "BPM.END.104" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -S1 [expr -0.6004214291*$refen] -refen $refen 
Bpm "BPM.END.105" -len 0 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -S2 [expr 2.19370775767821e+01] -refen $refen 
Drift "TTA.D3B" -len 3.077193097 
Drift "TTA.D3C" -len 0.1 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -S1 [expr 0.3042074493*$refen] -refen $refen 
Bpm "BPM.END.106" -len 0 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -S2 [expr -1.16237357135490e+01] -refen $refen 
Drift "TTA.D4B" -len 0.3 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -S2 [expr -1.16237357135490e+01] -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -S1 [expr 0.3042074493*$refen] -refen $refen 
Bpm "BPM.END.107" -len 0 
Drift "TTA.D3C" -len 0.1 
Drift "TTA.D3B" -len 3.077193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -S2 [expr 2.19370775767821e+01] -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -S1 [expr -0.6004214291*$refen] -refen $refen 
Bpm "BPM.END.108" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -S1 [expr 0.6863003356*$refen] -refen $refen 
Bpm "BPM.END.109" -len 0 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -S2 [expr -1.86448311441455e+01] -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Marker "TTA.MBEND3" 
Drift "TTA.D2M" -len 2 
Marker "TTA.EXICELL" 
Drift "TTA.D2M" -len 2 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -S2 [expr -1.86448311441455e+01] -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -S1 [expr 0.6863003356*$refen] -refen $refen 
Bpm "BPM.END.110" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -S1 [expr -0.6004214291*$refen] -refen $refen 
Bpm "BPM.END.111" -len 0 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -S2 [expr 2.19370775767821e+01] -refen $refen 
Drift "TTA.D3B" -len 3.077193097 
Drift "TTA.D3C" -len 0.1 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -S1 [expr 0.3042074493*$refen] -refen $refen 
Bpm "BPM.END.112" -len 0 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -S2 [expr -1.16237357135490e+01] -refen $refen 
Drift "TTA.D4B" -len 0.3 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -S2 [expr -1.16237357135490e+01] -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -S1 [expr 0.3042074493*$refen] -refen $refen 
Bpm "BPM.END.113" -len 0 
Drift "TTA.D3C" -len 0.1 
Drift "TTA.D3B" -len 3.077193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -S2 [expr 2.19370775767821e+01] -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -S1 [expr -0.6004214291*$refen] -refen $refen 
Bpm "BPM.END.114" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -S1 [expr 0.6863003356*$refen] -refen $refen 
Bpm "BPM.END.115" -len 0 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -S2 [expr -1.86448311441455e+01] -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Marker "TTA.MBEND3" 
Drift "TTA.D2M" -len 2 
Marker "TTA.EXICELL" 
Drift "TTA.D2M" -len 2 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -S2 [expr -1.86448311441455e+01] -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -S1 [expr 0.6863003356*$refen] -refen $refen 
Bpm "BPM.END.116" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -S1 [expr -0.6004214291*$refen] -refen $refen 
Bpm "BPM.END.117" -len 0 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -S2 [expr 2.19370775767821e+01] -refen $refen 
Drift "TTA.D3B" -len 3.077193097 
Drift "TTA.D3C" -len 0.1 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -S1 [expr 0.3042074493*$refen] -refen $refen 
Bpm "BPM.END.118" -len 0 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -S2 [expr -1.16237357135490e+01] -refen $refen 
Drift "TTA.D4B" -len 0.3 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -S2 [expr -1.16237357135490e+01] -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -S1 [expr 0.3042074493*$refen] -refen $refen 
Bpm "BPM.END.119" -len 0 
Drift "TTA.D3C" -len 0.1 
Drift "TTA.D3B" -len 3.077193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -S2 [expr 2.19370775767821e+01] -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -S1 [expr -0.6004214291*$refen] -refen $refen 
Bpm "BPM.END.120" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -S1 [expr 0.6863003356*$refen] -refen $refen 
Bpm "BPM.END.121" -len 0 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -S2 [expr -1.86448311441455e+01] -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Marker "TTA.MBEND3" 
Drift "TTA.D2M" -len 2 
Marker "TTA.EXICELL" 
Drift "TTA.D2M" -len 2 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -S2 [expr -1.86448311441455e+01] -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -S1 [expr 0.6863003356*$refen] -refen $refen 
Bpm "BPM.END.122" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -S1 [expr -0.6004214291*$refen] -refen $refen 
Bpm "BPM.END.123" -len 0 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -S2 [expr 2.19370775767821e+01] -refen $refen 
Drift "TTA.D3B" -len 3.077193097 
Drift "TTA.D3C" -len 0.1 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -S1 [expr 0.3042074493*$refen] -refen $refen 
Bpm "BPM.END.124" -len 0 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -S2 [expr -1.16237357135490e+01] -refen $refen 
Drift "TTA.D4B" -len 0.3 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -S2 [expr -1.16237357135490e+01] -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -S1 [expr 0.3042074493*$refen] -refen $refen 
Bpm "BPM.END.125" -len 0 
Drift "TTA.D3C" -len 0.1 
Drift "TTA.D3B" -len 3.077193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -S2 [expr 2.19370775767821e+01] -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -S1 [expr -0.6004214291*$refen] -refen $refen 
Bpm "BPM.END.126" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -S1 [expr 0.6863003356*$refen] -refen $refen 
Bpm "BPM.END.127" -len 0 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -S2 [expr -1.86448311441455e+01] -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Marker "TTA.MBEND3" 
Drift "TTA.D2M" -len 2 
Marker "TTA.EXICELL" 
Drift "TTA.D2M" -len 2 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -S2 [expr -1.86448311441455e+01] -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -S1 [expr 0.6863003356*$refen] -refen $refen 
Bpm "BPM.END.128" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -S1 [expr -0.6004214291*$refen] -refen $refen 
Bpm "BPM.END.129" -len 0 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -S2 [expr 2.19370775767821e+01] -refen $refen 
Drift "TTA.D3B" -len 3.077193097 
Drift "TTA.D3C" -len 0.1 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -S1 [expr 0.3042074493*$refen] -refen $refen 
Bpm "BPM.END.130" -len 0 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -S2 [expr -1.16237357135490e+01] -refen $refen 
Drift "TTA.D4B" -len 0.3 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -S2 [expr -1.16237357135490e+01] -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -S1 [expr 0.3042074493*$refen] -refen $refen 
Bpm "BPM.END.131" -len 0 
Drift "TTA.D3C" -len 0.1 
Drift "TTA.D3B" -len 3.077193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -S2 [expr 2.19370775767821e+01] -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -S1 [expr -0.6004214291*$refen] -refen $refen 
Bpm "BPM.END.132" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -S1 [expr 0.6863003356*$refen] -refen $refen 
Bpm "BPM.END.133" -len 0 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -S2 [expr -1.86448311441455e+01] -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Marker "TTA.MBEND3" 
Drift "TTA.D2M" -len 2 
Marker "TTA.EXICELL" 
Drift "TTA.D2M" -len 2 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -S2 [expr -1.86448311441455e+01] -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -S1 [expr 0.6863003356*$refen] -refen $refen 
Bpm "BPM.END.134" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -S1 [expr -0.6004214291*$refen] -refen $refen 
Bpm "BPM.END.135" -len 0 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -S2 [expr 2.19370775767821e+01] -refen $refen 
Drift "TTA.D3B" -len 3.077193097 
Drift "TTA.D3C" -len 0.1 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -S1 [expr 0.3042074493*$refen] -refen $refen 
Bpm "BPM.END.136" -len 0 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -S2 [expr -1.16237357135490e+01] -refen $refen 
Drift "TTA.D4B" -len 0.3 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -S2 [expr -1.16237357135490e+01] -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -S1 [expr 0.3042074493*$refen] -refen $refen 
Bpm "BPM.END.137" -len 0 
Drift "TTA.D3C" -len 0.1 
Drift "TTA.D3B" -len 3.077193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -S2 [expr 2.19370775767821e+01] -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -S1 [expr -0.6004214291*$refen] -refen $refen 
Bpm "BPM.END.138" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -S1 [expr 0.6863003356*$refen] -refen $refen 
Bpm "BPM.END.139" -len 0 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -S2 [expr -1.86448311441455e+01] -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Marker "TTA.MBEND3" 
Drift "TTA.D2M" -len 2 
Marker "TTA.EXICELL" 
Drift "TTA.D2M" -len 2 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -S2 [expr -1.86448311441455e+01] -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -S1 [expr 0.6863003356*$refen] -refen $refen 
Bpm "BPM.END.140" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -S1 [expr -0.6004214291*$refen] -refen $refen 
Bpm "BPM.END.141" -len 0 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -S2 [expr 2.19370775767821e+01] -refen $refen 
Drift "TTA.D3B" -len 3.077193097 
Drift "TTA.D3C" -len 0.1 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -S1 [expr 0.3042074493*$refen] -refen $refen 
Bpm "BPM.END.142" -len 0 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -S2 [expr -1.16237357135490e+01] -refen $refen 
Drift "TTA.D4B" -len 0.3 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -S2 [expr -1.16237357135490e+01] -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -S1 [expr 0.3042074493*$refen] -refen $refen 
Bpm "BPM.END.143" -len 0 
Drift "TTA.D3C" -len 0.1 
Drift "TTA.D3B" -len 3.077193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -S2 [expr 2.19370775767821e+01] -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -S1 [expr -0.6004214291*$refen] -refen $refen 
Bpm "BPM.END.144" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -S1 [expr 0.6863003356*$refen] -refen $refen 
Bpm "BPM.END.145" -len 0 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -S2 [expr -1.86448311441455e+01] -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Marker "TTA.MBEND3" 
Drift "TTA.D2M" -len 2 
Marker "TTA.EXICELL" 
Drift "TTA.D2M" -len 2 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -S2 [expr -1.86448311441455e+01] -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -S1 [expr 0.6863003356*$refen] -refen $refen 
Bpm "BPM.END.146" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -S1 [expr -0.6004214291*$refen] -refen $refen 
Bpm "BPM.END.147" -len 0 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -S2 [expr 2.19370775767821e+01] -refen $refen 
Drift "TTA.D3B" -len 3.077193097 
Drift "TTA.D3C" -len 0.1 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -S1 [expr 0.3042074493*$refen] -refen $refen 
Bpm "BPM.END.148" -len 0 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -S2 [expr -1.16237357135490e+01] -refen $refen 
Drift "TTA.D4B" -len 0.3 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -S2 [expr -1.16237357135490e+01] -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -S1 [expr 0.3042074493*$refen] -refen $refen 
Bpm "BPM.END.149" -len 0 
Drift "TTA.D3C" -len 0.1 
Drift "TTA.D3B" -len 3.077193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -S2 [expr 2.19370775767821e+01] -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -S1 [expr -0.6004214291*$refen] -refen $refen 
Bpm "BPM.END.150" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -S1 [expr 0.6863003356*$refen] -refen $refen 
Bpm "BPM.END.151" -len 0 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -S2 [expr -1.86448311441455e+01] -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Marker "TTA.MBEND3" 
Drift "TTA.D2M" -len 2 
Marker "TTA.EXICELL" 
Drift "TTA.D2M" -len 2 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -S2 [expr -1.86448311441455e+01] -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -S1 [expr 0.6863003356*$refen] -refen $refen 
Bpm "BPM.END.152" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -S1 [expr -0.6004214291*$refen] -refen $refen 
Bpm "BPM.END.153" -len 0 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -S2 [expr 2.19370775767821e+01] -refen $refen 
Drift "TTA.D3B" -len 3.077193097 
Drift "TTA.D3C" -len 0.1 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -S1 [expr 0.3042074493*$refen] -refen $refen 
Bpm "BPM.END.154" -len 0 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -S2 [expr -1.16237357135490e+01] -refen $refen 
Drift "TTA.D4B" -len 0.3 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -S2 [expr -1.16237357135490e+01] -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -S1 [expr 0.3042074493*$refen] -refen $refen 
Bpm "BPM.END.155" -len 0 
Drift "TTA.D3C" -len 0.1 
Drift "TTA.D3B" -len 3.077193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -S2 [expr 2.19370775767821e+01] -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -S1 [expr -0.6004214291*$refen] -refen $refen 
Bpm "BPM.END.156" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -S1 [expr 0.6863003356*$refen] -refen $refen 
Bpm "BPM.END.157" -len 0 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -S2 [expr -1.86448311441455e+01] -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Marker "TTA.MBEND3" 
Drift "TTA.D2M" -len 2 
Marker "TTA.EXICELL" 
Drift "TTA.D2M" -len 2 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -S2 [expr -1.86448311441455e+01] -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -S1 [expr 0.6863003356*$refen] -refen $refen 
Bpm "BPM.END.158" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -S1 [expr -0.6004214291*$refen] -refen $refen 
Bpm "BPM.END.159" -len 0 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -S2 [expr 2.19370775767821e+01] -refen $refen 
Drift "TTA.D3B" -len 3.077193097 
Drift "TTA.D3C" -len 0.1 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -S1 [expr 0.3042074493*$refen] -refen $refen 
Bpm "BPM.END.160" -len 0 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -S2 [expr -1.16237357135490e+01] -refen $refen 
Drift "TTA.D4B" -len 0.3 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -S2 [expr -1.16237357135490e+01] -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -S1 [expr 0.3042074493*$refen] -refen $refen 
Bpm "BPM.END.161" -len 0 
Drift "TTA.D3C" -len 0.1 
Drift "TTA.D3B" -len 3.077193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -S2 [expr 2.19370775767821e+01] -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -S1 [expr -0.6004214291*$refen] -refen $refen 
Bpm "BPM.END.162" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -S1 [expr 0.6863003356*$refen] -refen $refen 
Bpm "BPM.END.163" -len 0 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -S2 [expr -1.86448311441455e+01] -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Marker "TTA.MBEND3" 
Drift "TTA.D2M" -len 2 
Marker "TTA.EXICELL" 
Drift "TTA.D2M" -len 2 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -S2 [expr -1.86448311441455e+01] -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -S1 [expr 0.6863003356*$refen] -refen $refen 
Bpm "BPM.END.164" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -S1 [expr -0.6004214291*$refen] -refen $refen 
Bpm "BPM.END.165" -len 0 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -S2 [expr 2.19370775767821e+01] -refen $refen 
Drift "TTA.D3B" -len 3.077193097 
Drift "TTA.D3C" -len 0.1 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -S1 [expr 0.3042074493*$refen] -refen $refen 
Bpm "BPM.END.166" -len 0 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -S2 [expr -1.16237357135490e+01] -refen $refen 
Drift "TTA.D4B" -len 0.3 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -S2 [expr -1.16237357135490e+01] -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -S1 [expr 0.3042074493*$refen] -refen $refen 
Bpm "BPM.END.167" -len 0 
Drift "TTA.D3C" -len 0.1 
Drift "TTA.D3B" -len 3.077193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -S2 [expr 2.19370775767821e+01] -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -S1 [expr -0.6004214291*$refen] -refen $refen 
Bpm "BPM.END.168" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -S1 [expr 0.6863003356*$refen] -refen $refen 
Bpm "BPM.END.169" -len 0 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -S2 [expr -1.86448311441455e+01] -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Marker "TTA.MBEND3" 
Drift "TTA.D2M" -len 2 
Marker "TTA.EXICELL" 
Drift "TTA.D2M" -len 2 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -S2 [expr -1.86448311441455e+01] -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -S1 [expr 0.6863003356*$refen] -refen $refen 
Bpm "BPM.END.170" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -S1 [expr -0.6004214291*$refen] -refen $refen 
Bpm "BPM.END.171" -len 0 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -S2 [expr 2.19370775767821e+01] -refen $refen 
Drift "TTA.D3B" -len 3.077193097 
Drift "TTA.D3C" -len 0.1 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -S1 [expr 0.3042074493*$refen] -refen $refen 
Bpm "BPM.END.172" -len 0 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -S2 [expr -1.16237357135490e+01] -refen $refen 
Drift "TTA.D4B" -len 0.3 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -S2 [expr -1.16237357135490e+01] -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -S1 [expr 0.3042074493*$refen] -refen $refen 
Bpm "BPM.END.173" -len 0 
Drift "TTA.D3C" -len 0.1 
Drift "TTA.D3B" -len 3.077193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -S2 [expr 2.19370775767821e+01] -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -S1 [expr -0.6004214291*$refen] -refen $refen 
Bpm "BPM.END.174" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -S1 [expr 0.6863003356*$refen] -refen $refen 
Bpm "BPM.END.175" -len 0 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -S2 [expr -1.86448311441455e+01] -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Marker "TTA.MBEND3" 
Drift "TTA.D2M" -len 2 
Marker "TTA.EXICELL" 
Drift "TTA.D2M" -len 2 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -S2 [expr -1.86448311441455e+01] -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -S1 [expr 0.6863003356*$refen] -refen $refen 
Bpm "BPM.END.176" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -S1 [expr -0.6004214291*$refen] -refen $refen 
Bpm "BPM.END.177" -len 0 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -S2 [expr 2.19370775767821e+01] -refen $refen 
Drift "TTA.D3B" -len 3.077193097 
Drift "TTA.D3C" -len 0.1 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -S1 [expr 0.3042074493*$refen] -refen $refen 
Bpm "BPM.END.178" -len 0 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -S2 [expr -1.16237357135490e+01] -refen $refen 
Drift "TTA.D4B" -len 0.3 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -S2 [expr -1.16237357135490e+01] -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -S1 [expr 0.3042074493*$refen] -refen $refen 
Bpm "BPM.END.179" -len 0 
Drift "TTA.D3C" -len 0.1 
Drift "TTA.D3B" -len 3.077193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -S2 [expr 2.19370775767821e+01] -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -S1 [expr -0.6004214291*$refen] -refen $refen 
Bpm "BPM.END.180" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -S1 [expr 0.6863003356*$refen] -refen $refen 
Bpm "BPM.END.181" -len 0 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -S2 [expr -1.86448311441455e+01] -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Marker "TTA.MBEND3" 
Drift "TTA.D2M" -len 2 
Marker "TTA.EXICELL" 
Drift "TTA.D2M" -len 2 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -S2 [expr -1.86448311441455e+01] -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -S1 [expr 0.6863003356*$refen] -refen $refen 
Bpm "BPM.END.182" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -S1 [expr -0.6004214291*$refen] -refen $refen 
Bpm "BPM.END.183" -len 0 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -S2 [expr 2.19370775767821e+01] -refen $refen 
Drift "TTA.D3B" -len 3.077193097 
Drift "TTA.D3C" -len 0.1 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -S1 [expr 0.3042074493*$refen] -refen $refen 
Bpm "BPM.END.184" -len 0 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -S2 [expr -1.16237357135490e+01] -refen $refen 
Drift "TTA.D4B" -len 0.3 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -S2 [expr -1.16237357135490e+01] -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -S1 [expr 0.3042074493*$refen] -refen $refen 
Bpm "BPM.END.185" -len 0 
Drift "TTA.D3C" -len 0.1 
Drift "TTA.D3B" -len 3.077193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -S2 [expr 2.19370775767821e+01] -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -S1 [expr -0.6004214291*$refen] -refen $refen 
Bpm "BPM.END.186" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -S1 [expr 0.6863003356*$refen] -refen $refen 
Bpm "BPM.END.187" -len 0 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -S2 [expr -1.86448311441455e+01] -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Marker "TTA.MBEND3" 
Drift "TTA.D2M" -len 2 
Marker "TTA.EXICELL" 
Drift "TTA.D2M" -len 2 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -S2 [expr -1.86448311441455e+01] -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -S1 [expr 0.6863003356*$refen] -refen $refen 
Bpm "BPM.END.188" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -S1 [expr -0.6004214291*$refen] -refen $refen 
Bpm "BPM.END.189" -len 0 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -S2 [expr 2.19370775767821e+01] -refen $refen 
Drift "TTA.D3B" -len 3.077193097 
Drift "TTA.D3C" -len 0.1 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -S1 [expr 0.3042074493*$refen] -refen $refen 
Bpm "BPM.END.190" -len 0 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -S2 [expr -1.16237357135490e+01] -refen $refen 
Drift "TTA.D4B" -len 0.3 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -S2 [expr -1.16237357135490e+01] -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -S1 [expr 0.3042074493*$refen] -refen $refen 
Bpm "BPM.END.191" -len 0 
Drift "TTA.D3C" -len 0.1 
Drift "TTA.D3B" -len 3.077193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -S2 [expr 2.19370775767821e+01] -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -S1 [expr -0.6004214291*$refen] -refen $refen 
Bpm "BPM.END.192" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -S1 [expr 0.6863003356*$refen] -refen $refen 
Bpm "BPM.END.193" -len 0 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -S2 [expr -1.86448311441455e+01] -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Marker "TTA.MBEND3" 
Drift "TTA.D2M" -len 2 
Marker "TTA.EXICELL" 
Drift "TTA.D2M" -len 2 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -S2 [expr -1.86448311441455e+01] -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -S1 [expr 0.6863003356*$refen] -refen $refen 
Bpm "BPM.END.194" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -S1 [expr -0.6004214291*$refen] -refen $refen 
Bpm "BPM.END.195" -len 0 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -S2 [expr 2.19370775767821e+01] -refen $refen 
Drift "TTA.D3B" -len 3.077193097 
Drift "TTA.D3C" -len 0.1 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -S1 [expr 0.3042074493*$refen] -refen $refen 
Bpm "BPM.END.196" -len 0 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -S2 [expr -1.16237357135490e+01] -refen $refen 
Drift "TTA.D4B" -len 0.3 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -S2 [expr -1.16237357135490e+01] -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -S1 [expr 0.3042074493*$refen] -refen $refen 
Bpm "BPM.END.197" -len 0 
Drift "TTA.D3C" -len 0.1 
Drift "TTA.D3B" -len 3.077193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -S2 [expr 2.19370775767821e+01] -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -S1 [expr -0.6004214291*$refen] -refen $refen 
Bpm "BPM.END.198" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -S1 [expr 0.6863003356*$refen] -refen $refen 
Bpm "BPM.END.199" -len 0 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -S2 [expr -1.86448311441455e+01] -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Marker "TTA.MBEND3" 
Drift "TTA.D2M" -len 2 
Marker "TTA.EXICELL" 
Drift "TTA.D2M" -len 2 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -S2 [expr -1.86448311441455e+01] -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -S1 [expr 0.6863003356*$refen] -refen $refen 
Bpm "BPM.END.200" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -S1 [expr -0.6004214291*$refen] -refen $refen 
Bpm "BPM.END.201" -len 0 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -S2 [expr 2.19370775767821e+01] -refen $refen 
Drift "TTA.D3B" -len 3.077193097 
Drift "TTA.D3C" -len 0.1 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -S1 [expr 0.3042074493*$refen] -refen $refen 
Bpm "BPM.END.202" -len 0 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -S2 [expr -1.16237357135490e+01] -refen $refen 
Drift "TTA.D4B" -len 0.3 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -S2 [expr -1.16237357135490e+01] -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -S1 [expr 0.3042074493*$refen] -refen $refen 
Bpm "BPM.END.203" -len 0 
Drift "TTA.D3C" -len 0.1 
Drift "TTA.D3B" -len 3.077193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -S2 [expr 2.19370775767821e+01] -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -S1 [expr -0.6004214291*$refen] -refen $refen 
Bpm "BPM.END.204" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -S1 [expr 0.6863003356*$refen] -refen $refen 
Bpm "BPM.END.205" -len 0 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -S2 [expr -1.86448311441455e+01] -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Marker "TTA.MBEND3" 
Drift "TTA.D2M" -len 2 
Marker "TTA.EXICELL" 
Drift "TTA.D2M" -len 2 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -S2 [expr -1.86448311441455e+01] -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -S1 [expr 0.6863003356*$refen] -refen $refen 
Bpm "BPM.END.206" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -S1 [expr -0.6004214291*$refen] -refen $refen 
Bpm "BPM.END.207" -len 0 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -S2 [expr 2.19370775767821e+01] -refen $refen 
Drift "TTA.D3B" -len 3.077193097 
Drift "TTA.D3C" -len 0.1 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -S1 [expr 0.3042074493*$refen] -refen $refen 
Bpm "BPM.END.208" -len 0 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -S2 [expr -1.16237357135490e+01] -refen $refen 
Drift "TTA.D4B" -len 0.3 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -S2 [expr -1.16237357135490e+01] -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -S1 [expr 0.3042074493*$refen] -refen $refen 
Bpm "BPM.END.209" -len 0 
Drift "TTA.D3C" -len 0.1 
Drift "TTA.D3B" -len 3.077193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -S2 [expr 2.19370775767821e+01] -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -S1 [expr -0.6004214291*$refen] -refen $refen 
Bpm "BPM.END.210" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -S1 [expr 0.6863003356*$refen] -refen $refen 
Bpm "BPM.END.211" -len 0 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -S2 [expr -1.86448311441455e+01] -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Marker "TTA.MBEND3" 
Drift "TTA.D2M" -len 2 
Marker "TTA.EXICELL" 
Drift "TTA.D2M" -len 2 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -S2 [expr -1.86448311441455e+01] -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -S1 [expr 0.6863003356*$refen] -refen $refen 
Bpm "BPM.END.212" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -S1 [expr -0.6004214291*$refen] -refen $refen 
Bpm "BPM.END.213" -len 0 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -S2 [expr 2.19370775767821e+01] -refen $refen 
Drift "TTA.D3B" -len 3.077193097 
Drift "TTA.D3C" -len 0.1 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -S1 [expr 0.3042074493*$refen] -refen $refen 
Bpm "BPM.END.214" -len 0 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -S2 [expr -1.16237357135490e+01] -refen $refen 
Drift "TTA.D4B" -len 0.3 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -S2 [expr -1.16237357135490e+01] -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -S1 [expr 0.3042074493*$refen] -refen $refen 
Bpm "BPM.END.215" -len 0 
Drift "TTA.D3C" -len 0.1 
Drift "TTA.D3B" -len 3.077193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -S2 [expr 2.19370775767821e+01] -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -S1 [expr -0.6004214291*$refen] -refen $refen 
Bpm "BPM.END.216" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -S1 [expr 0.6863003356*$refen] -refen $refen 
Bpm "BPM.END.217" -len 0 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -S2 [expr -1.86448311441455e+01] -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Marker "TTA.MBEND3" 
Drift "TTA.D2M" -len 2 
Marker "TTA.EXICELL" 
Drift "TTA.D2M" -len 2 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -S2 [expr -1.86448311441455e+01] -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -S1 [expr 0.6863003356*$refen] -refen $refen 
Bpm "BPM.END.218" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -S1 [expr -0.6004214291*$refen] -refen $refen 
Bpm "BPM.END.219" -len 0 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -S2 [expr 2.19370775767821e+01] -refen $refen 
Drift "TTA.D3B" -len 3.077193097 
Drift "TTA.D3C" -len 0.1 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -S1 [expr 0.3042074493*$refen] -refen $refen 
Bpm "BPM.END.220" -len 0 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -S2 [expr -1.16237357135490e+01] -refen $refen 
Drift "TTA.D4B" -len 0.3 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -S2 [expr -1.16237357135490e+01] -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -S1 [expr 0.3042074493*$refen] -refen $refen 
Bpm "BPM.END.221" -len 0 
Drift "TTA.D3C" -len 0.1 
Drift "TTA.D3B" -len 3.077193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -S2 [expr 2.19370775767821e+01] -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -S1 [expr -0.6004214291*$refen] -refen $refen 
Bpm "BPM.END.222" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -S1 [expr 0.6863003356*$refen] -refen $refen 
Bpm "BPM.END.223" -len 0 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -S2 [expr -1.86448311441455e+01] -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Marker "TTA.MBEND3" 
Drift "TTA.D2M" -len 2 
Marker "TTA.EXICELL" 
Drift "TTA.D2M" -len 2 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -S2 [expr -1.86448311441455e+01] -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -S1 [expr 0.6863003356*$refen] -refen $refen 
Bpm "BPM.END.224" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -S1 [expr -0.6004214291*$refen] -refen $refen 
Bpm "BPM.END.225" -len 0 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -S2 [expr 2.19370775767821e+01] -refen $refen 
Drift "TTA.D3B" -len 3.077193097 
Drift "TTA.D3C" -len 0.1 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -S1 [expr 0.3042074493*$refen] -refen $refen 
Bpm "BPM.END.226" -len 0 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -S2 [expr -1.16237357135490e+01] -refen $refen 
Drift "TTA.D4B" -len 0.3 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -S2 [expr -1.16237357135490e+01] -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -S1 [expr 0.3042074493*$refen] -refen $refen 
Bpm "BPM.END.227" -len 0 
Drift "TTA.D3C" -len 0.1 
Drift "TTA.D3B" -len 3.077193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -S2 [expr 2.19370775767821e+01] -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -S1 [expr -0.6004214291*$refen] -refen $refen 
Bpm "BPM.END.228" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -S1 [expr 0.6863003356*$refen] -refen $refen 
Bpm "BPM.END.229" -len 0 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -S2 [expr -1.86448311441455e+01] -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Marker "TTA.MBEND3" 
Drift "TTA.D2M" -len 2 
Marker "TTA.EXICELL" 
Drift "TTA.D2M" -len 2 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -S2 [expr -1.86448311441455e+01] -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -S1 [expr 0.6863003356*$refen] -refen $refen 
Bpm "BPM.END.230" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -S1 [expr -0.6004214291*$refen] -refen $refen 
Bpm "BPM.END.231" -len 0 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -S2 [expr 2.19370775767821e+01] -refen $refen 
Drift "TTA.D3B" -len 3.077193097 
Drift "TTA.D3C" -len 0.1 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -S1 [expr 0.3042074493*$refen] -refen $refen 
Bpm "BPM.END.232" -len 0 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -S2 [expr -1.16237357135490e+01] -refen $refen 
Drift "TTA.D4B" -len 0.3 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -S2 [expr -1.16237357135490e+01] -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -S1 [expr 0.3042074493*$refen] -refen $refen 
Bpm "BPM.END.233" -len 0 
Drift "TTA.D3C" -len 0.1 
Drift "TTA.D3B" -len 3.077193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -S2 [expr 2.19370775767821e+01] -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -S1 [expr -0.6004214291*$refen] -refen $refen 
Bpm "BPM.END.234" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -S1 [expr 0.6863003356*$refen] -refen $refen 
Bpm "BPM.END.235" -len 0 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -S2 [expr -1.86448311441455e+01] -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Marker "TTA.MBEND3" 
Drift "TTA.D2M" -len 2 
Marker "TTA.EXICELL" 
Drift "TTA.D2M" -len 2 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TTA.D1A" -len 2.389121542 
Sextupole "SEXT.TTA.SX1" -len 0.4 -S2 [expr -1.86448311441455e+01] -refen $refen 
Drift "TTA.D1B" -len 0.3 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -S1 [expr 0.6863003356*$refen] -refen $refen 
Bpm "BPM.END.236" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -S1 [expr -0.6004214291*$refen] -refen $refen 
Bpm "BPM.END.237" -len 0 
Drift "TTA.D3A" -len 0.3 
Sextupole "SEXT.TTA.SX2" -len 0.4 -S2 [expr 2.19370775767821e+01] -refen $refen 
Drift "TTA.D3B" -len 3.077193097 
Drift "TTA.D3C" -len 0.1 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -S1 [expr 0.3042074493*$refen] -refen $refen 
Bpm "BPM.END.238" -len 0 
Drift "TTA.D4A" -len 0.5779914647 
Sextupole "SEXT.TTA.SX3" -len 0.4 -S2 [expr -1.16237357135490e+01] -refen $refen 
Drift "TTA.D4B" -len 0.3 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "TTA.D4B" -len 0.3 
Sextupole "SEXT.TTA.SX3" -len 0.4 -S2 [expr -1.16237357135490e+01] -refen $refen 
Drift "TTA.D4A" -len 0.5779914647 
Quadrupole "QUAD.TTA.Q3" -len 0.75 -S1 [expr 0.3042074493*$refen] -refen $refen 
Bpm "BPM.END.239" -len 0 
Drift "TTA.D3C" -len 0.1 
Drift "TTA.D3B" -len 3.077193097 
Sextupole "SEXT.TTA.SX2" -len 0.4 -S2 [expr 2.19370775767821e+01] -refen $refen 
Drift "TTA.D3A" -len 0.3 
Quadrupole "QUAD.TTA.Q2" -len 0.75 -S1 [expr -0.6004214291*$refen] -refen $refen 
Bpm "BPM.END.240" -len 0 
Drift "TTA.D2" -len 0.5056938963 
Quadrupole "QUAD.TTA.Q1" -len 0.75 -S1 [expr 0.6863003356*$refen] -refen $refen 
Bpm "BPM.END.241" -len 0 
Drift "TTA.D1B" -len 0.3 
Sextupole "SEXT.TTA.SX1" -len 0.4 -S2 [expr -1.86448311441455e+01] -refen $refen 
Drift "TTA.D1A" -len 2.389121542 
# WARNING: putting a Dipole instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2 -refen $refen 
# WARNING: original length was 2.000101543 
Sbend "BEND.TTA.BEND" -len 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -refen $refen 
set refen [expr $refen-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Marker "TTA.MBEND3" 
Drift "TTA.D2M" -len 2 
Marker "TTA.EXICELL" 
#Marker "TTA.EJECTION" 
##Bpm "BPM.END.242" -len 0 
##Marker "CHICANESTART" 
##Drift "DLDG.D4" -len 1.59261465 
##Quadrupole "QUAD.DLDG.Q4" -len 0.2 -S1 [expr -0.2930570818*$refen] -refen $refen 
##Drift "DLDG.D3" -len 1.01854763 
##Quadrupole "QUAD.DLDG.Q3" -len 0.2 -S1 [expr 0.4552596096*$refen] -refen $refen 
##Drift "DLDG.D2" -len 5.26626489 
##Quadrupole "QUAD.DLDG.Q2" -len 0.2 -S1 [expr -0.3502967462*$refen] -refen $refen 
##Drift "DLDG.D1" -len 0.99977365 
##Quadrupole "QUAD.DLDG.Q1" -len 0.2 -S1 [expr 0.2935432288*$refen] -refen $refen 
##Drift "DLDG.D0" -len 0.83396146 
##Bpm "BPM.END.243" -len 0 
##Sbend "BEND.BRB1A" -len 0.45 -angle -0.0599265 -E1 -0.02836160034 -E2 0 -refen $refen 
##set refen [expr $refen-14.1e-6*-0.0599265*-0.0599265/0.45*$e0*$e0*$e0*$e0*$Dipole_synrad] 
##Sbend "BEND.BRB1B" -len 0.45 -angle -0.0599265 -E1 0 -E2 -0.02836160034 -refen $refen 
##set refen [expr $refen-14.1e-6*-0.0599265*-0.0599265/0.45*$e0*$e0*$e0*$e0*$Dipole_synrad] 
##Drift "DG.L1" -len 1.8 
##Quadrupole "QUAD.QDDOG" -len 0.1 -S1 [expr 0.1539537815*$refen] -refen $refen 
##Drift "DG.L1" -len 1.8 
##Sbend "BEND.BRB2A" -len 0.45 -angle 0.0599265 -E1 -0.02836160034 -E2 0 -refen $refen 
##set refen [expr $refen-14.1e-6*0.0599265*0.0599265/0.45*$e0*$e0*$e0*$e0*$Dipole_synrad] 
##Sbend "BEND.BRB2B" -len 0.45 -angle 0.0599265 -E1 0 -E2 -0.02836160034 -refen $refen 
##set refen [expr $refen-14.1e-6*0.0599265*0.0599265/0.45*$e0*$e0*$e0*$e0*$Dipole_synrad] 
##Drift "DG.L2" -len 0.3 
##Quadrupole "QUAD.QFDOG" -len 0.1 -S1 [expr -0.162804451*$refen] -refen $refen 
##Drift "DG.L3" -len 0.1 
##Quadrupole "QUAD.QDDOG2" -len 0.1 -S1 [expr 0.03547706207*$refen] -refen $refen 
##Drift "DG.L4" -len 0.1 
##Drift "DG.L4" -len 0.1 
##Quadrupole "QUAD.QDDOG2" -len 0.1 -S1 [expr 0.03547706207*$refen] -refen $refen 
##Drift "DG.L3" -len 0.1 
##Quadrupole "QUAD.QFDOG" -len 0.1 -S1 [expr -0.162804451*$refen] -refen $refen 
##Drift "DG.L2" -len 0.3 
##Sbend "BEND.BRB2B" -len 0.45 -angle 0.0599265 -E1 0 -E2 -0.02836160034 -refen $refen 
##set refen [expr $refen-14.1e-6*0.0599265*0.0599265/0.45*$e0*$e0*$e0*$e0*$Dipole_synrad] 
##Sbend "BEND.BRB2A" -len 0.45 -angle 0.0599265 -E1 -0.02836160034 -E2 0 -refen $refen 
##set refen [expr $refen-14.1e-6*0.0599265*0.0599265/0.45*$e0*$e0*$e0*$e0*$Dipole_synrad] 
##Drift "DG.L1" -len 1.8 
##Quadrupole "QUAD.QDDOG" -len 0.1 -S1 [expr 0.1539537815*$refen] -refen $refen 
##Drift "DG.L1" -len 1.8 
##Sbend "BEND.BRB1B" -len 0.45 -angle -0.0599265 -E1 0 -E2 -0.02836160034 -refen $refen 
##set refen [expr $refen-14.1e-6*-0.0599265*-0.0599265/0.45*$e0*$e0*$e0*$e0*$Dipole_synrad] 
##Sbend "BEND.BRB1A" -len 0.45 -angle -0.0599265 -E1 -0.02836160034 -E2 0 -refen $refen 
##set refen [expr $refen-14.1e-6*-0.0599265*-0.0599265/0.45*$e0*$e0*$e0*$e0*$Dipole_synrad] 
##Bpm "BPM.END.244" -len 0 
#Marker "DBRC_END.EJECTION" 
