## Reference energy taken from list_energies.tcl (Computed from Eduardo Martin's Lattice) 
## This was chosen to match the less energetic 'bunch type' (there are 24 different possible bunch energies). 
## Which means there will be a slight magnet missfocus for the other bunches. 
set pi [expr acos(-1)]
if {$Dipole_synrad} { 
	set refen 2.38 
} else { 
	set refen 2.38 
} 
Marker "DBRC_Start.INJECTION" 
Bpm "BPM.Start.START" -len 0 
Sbend "BEND.BRB1A" -len 2.2 -angle -0.1335176878 -E1 -0.06675884389 -E2 0 -refen $refen 
set refen [expr $refen-14.1e-6*-0.1335176878*-0.1335176878/2.2*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Sbend "BEND.BRB1B" -len 2.2 -angle -0.1335176878 -E1 0 -E2 -0.06675884389 -refen $refen 
set refen [expr $refen-14.1e-6*-0.1335176878*-0.1335176878/2.2*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Drift "DG.L1_1" -len 2 
Sextupole "SEXT.DG.SEXT1" -len 0.1 -S2 [expr -1.79143865465929/2.38*$refen] -refen $refen 
Drift "DG.L11_1" -len 0.9 
Quadrupole "QUAD.QFDOG1.1" -len 0.1 -S1 [expr 0.141447*$refen] -refen $refen 
Bpm "BPM.Start.Q1" -len 0 
Quadrupole "QUAD.QFDOG1.2" -len 0.1 -S1 [expr 0.141447*$refen] -refen $refen 
Drift "DG.L2.1" -len 5 
Quadrupole "QUAD.QDDOG1.1" -len 0.1 -S1 [expr -0.107512*$refen] -refen $refen 
Bpm "BPM.Start.Q2" -len 0 
Quadrupole "QUAD.QDDOG1.2" -len 0.1 -S1 [expr -0.107512*$refen] -refen $refen 
Drift "DG.L3.1" -len 5 
Quadrupole "QUAD.QFDOG2.1" -len 0.1 -S1 [expr 0.0795841*$refen] -refen $refen 
Bpm "BPM.Start.Q3" -len 0 
Quadrupole "QUAD.QFDOG2.2" -len 0.1 -S1 [expr 0.0795841*$refen] -refen $refen 
Drift "DG.L4.1" -len 8 
Bpm "BPM.Start.C" -len 0 
Drift "DG.L4.2" -len 8 
Quadrupole "QUAD.QFDOG3.1" -len 0.1 -S1 [expr 0.0795841*$refen] -refen $refen 
Bpm "BPM.Start.Q4" -len 0 
Quadrupole "QUAD.QFDOG3.2" -len 0.1 -S1 [expr 0.0795841*$refen] -refen $refen 
Drift "DG.L3.2" -len 5 
Quadrupole "QUAD.QDDOG2.1" -len 0.1 -S1 [expr -0.107512*$refen] -refen $refen 
Bpm "BPM.Start.Q5" -len 0 
Quadrupole "QUAD.QDDOG2.2" -len 0.1 -S1 [expr -0.107512*$refen] -refen $refen 
Drift "DG.L2.2" -len 5 
Quadrupole "QUAD.QFDOG4.1" -len 0.1 -S1 [expr 0.141447*$refen] -refen $refen 
Bpm "BPM.Start.Q6" -len 0 
Quadrupole "QUAD.QFDOG4.2" -len 0.1 -S1 [expr 0.141447*$refen] -refen $refen 
Drift "DG.L11.2" -len 0.9 
Sextupole "SEXT.DG.SEXT2" -len 0.1 -S2 [expr 1.79758422628797/2.38*$refen] -refen $refen 
Drift "DG.L1.2" -len 2 
Sbend "BEND.BRB2A" -len 2.2 -angle 0.1335176878 -E1 0.06675884389 -E2 0 -refen $refen 
set refen [expr $refen-14.1e-6*0.1335176878*0.1335176878/2.2*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Sbend "BEND.BRB2B" -len 2.2 -angle 0.1335176878 -E1 0 -E2 0.06675884389 -refen $refen 
set refen [expr $refen-14.1e-6*0.1335176878*0.1335176878/2.2*$e0*$e0*$e0*$e0*$Dipole_synrad] 
Bpm "BPM.Start.DLDLEND" -len 0 
Drift "DGDL.D1" -len 1.03396146 
Quadrupole "QUAD.DGDL.Q1" -len 0.1 -S1 [expr 0.4393661953403926/2.38*$refen] -refen $refen 
Quadrupole "QUAD.DGDL.Q1" -len 0.1 -S1 [expr 0.4393661953403926/2.38*$refen] -refen $refen 
Drift "DGDL.D2" -len 0.99977365 
Quadrupole "QUAD.DGDL.Q2" -len 0.1 -S1 [expr -0.443991127147395/2.38*$refen] -refen $refen 
Quadrupole "QUAD.DGDL.Q2" -len 0.1 -S1 [expr -0.443991127147395/2.38*$refen] -refen $refen 
Drift "DGDL.D3" -len 5.26626489 
Quadrupole "QUAD.DGDL.Q3" -len 0.1 -S1 [expr 0.568600719331402/2.38*$refen] -refen $refen 
Quadrupole "QUAD.DGDL.Q3" -len 0.1 -S1 [expr 0.568600719331402/2.38*$refen] -refen $refen 
Drift "DGDL.D4" -len 1.01854763 
Quadrupole "QUAD.DGDL.Q4" -len 0.1 -S1 [expr -0.3189678031965645/2.38*$refen] -refen $refen 
Quadrupole "QUAD.DGDL.Q4" -len 0.1 -S1 [expr -0.3189678031965645/2.38*$refen] -refen $refen 
Drift "DGDL.D5" -len 1.39261465 
Marker "ISODL" 
Rfmultipole "RFMulti.DLINJSEPTUM" -len 0.2 -freq 0.49975 -strength [expr 0.0075*$refen] -phase $pi -refen $refen 
Drift "DRRF" -len 1.8 
Marker "DBRC_Start.EJECTION" 
