
################  Kvar Dogleg #######################

    DGSEXT1 = placet_get_name_number_list("beamline", "DG.SEXT1")
    DGSEXT2 = placet_get_name_number_list("beamline", "DG.SEXT2")

	Kvar = [DGSEXT1,DGSEXT2];


################  Kvar Delay Line #######################
# SNCH1 SXCH1 SXL2 SXL3 SXL4
DLOPT=[    2.67958362394119e+01  -1.42133460893859e+00  -6.17535267247810e+00   2.35334229848969e+00 ];
# SN1 SN2 SN3 SNN1 SNN2 SNN3 
DLOPTN=[    1.71477861510673e+00   1.29592213872071e+00  -2.39349790288346e+00];
# SX1 SX2 SX3 SXX1 SXX2 SXX3

    DLSNCH = placet_get_name_number_list("beamline", "DL.SNCH1");
    DLSXCH = placet_get_name_number_list("beamline", "DL.SXCH1");
    DLSXL2 = placet_get_name_number_list("beamline", "DL.SXL2");
    DLSXL3 = placet_get_name_number_list("beamline", "DL.SXL3");
    DLSXL4 = placet_get_name_number_list("beamline", "DL.SXL4");

    DLSN1 = placet_get_name_number_list("beamline", "DL.SN1");
    DLSN2 = placet_get_name_number_list("beamline", "DL.SN2");
    DLSN3 = placet_get_name_number_list("beamline", "DL.SN3");
    DLSNN1 = placet_get_name_number_list("beamline", "DL.SNN1");
    DLSNN2 = placet_get_name_number_list("beamline", "DL.SNN2");
    DLSNN3 = placet_get_name_number_list("beamline", "DL.SNN3");

    DLSX1 = placet_get_name_number_list("beamline", "DL.SX1");
    DLSX2 = placet_get_name_number_list("beamline", "DL.SX2");
    DLSX3 = placet_get_name_number_list("beamline", "DL.SX3");
    DLSXX1 = placet_get_name_number_list("beamline", "DL.SXX1");
    DLSXX2 = placet_get_name_number_list("beamline", "DL.SXX2");
    DLSXX3 = placet_get_name_number_list("beamline", "DL.SXX3");


    placet_element_set_attribute("beamline", DLSNCH , "strength",DLOPT(1));
    placet_element_set_attribute("beamline", DLSXCH , "strength",-DLOPT(1));

    placet_element_set_attribute("beamline", DLSXL2 , "strength",DLOPT(2));
    placet_element_set_attribute("beamline", DLSXL3 , "strength",DLOPT(3));
    placet_element_set_attribute("beamline", DLSXL4 , "strength",DLOPT(4));

    placet_element_set_attribute("beamline", DLSN1 , "strength",DLOPTN(1));
    placet_element_set_attribute("beamline", DLSN2 , "strength",DLOPTN(2));
    placet_element_set_attribute("beamline", DLSN3 , "strength",DLOPTN(3));
    placet_element_set_attribute("beamline", DLSNN1 , "strength",DLOPTN(1));
    placet_element_set_attribute("beamline", DLSNN2 , "strength",DLOPTN(2));
    placet_element_set_attribute("beamline", DLSNN3 , "strength",DLOPTN(3));

    placet_element_set_attribute("beamline", DLSX1 , "strength",-DLOPTN(1));
    placet_element_set_attribute("beamline", DLSX2 , "strength",-DLOPTN(2));
    placet_element_set_attribute("beamline", DLSX3 , "strength",-DLOPTN(3));
    placet_element_set_attribute("beamline", DLSXX1 , "strength",-DLOPTN(1));
    placet_element_set_attribute("beamline", DLSXX2 , "strength",-DLOPTN(2));
    placet_element_set_attribute("beamline", DLSXX3 , "strength",-DLOPTN(3));




#	Kvar= [DLSN1(1), DLSN2(1), DLSN3(1) ];
#	Kvar = [DLSNCH(1), DLSXL2(1),  DLSXL3(1),  DLSXL4(1),DLSN1(1), DLSN2(1), DLSN3(1)];

################  Kvar TL1 #######################
TL1OPTS=[  4.54349114799672e+01  -3.33554281908425e+01   3.59382179809146e+00   1.87224494804015e+01  -1.43600973181003e+01   2.35226527081827e+01]; 

TL1OPTS2=[   3.84557050166343e+01  -4.14486841761223e+00  -2.12298831887525e+01   2.23534965492188e+01   7.69941014844163e+00   1.41762180184321e+01 ];

TL1OPTX=[  5.54249053382495e+01  -1.99705515226260e+00  -6.55327541718394e+00   1.37219406739923e+02  -9.99992611516370e+01   8.91283304189828e+00   2.32493804398689e+01  -6.67931089397281e+01  -1.37173179536429e+01];

TL1Q=[  1.14073464630108e+00  -1.33920326155093e+00   5.87191805582981e-01  -1.17101147202867e+00   1.05026357915006e+00];


    TL1SXC415 = placet_get_name_number_list("beamline", "TL1.SXC415");
    TL1SXC425 = placet_get_name_number_list("beamline", "TL1.SXC425");
    TL1SXC435 = placet_get_name_number_list("beamline", "TL1.SXC435");
    TL1SXC445 = placet_get_name_number_list("beamline", "TL1.SXC445");
    TL1SXC455 = placet_get_name_number_list("beamline", "TL1.SXC455");
    TL1SXC465 = placet_get_name_number_list("beamline", "TL1.SXC465");
    TL1SXC475 = placet_get_name_number_list("beamline", "TL1.SXC475");
    TL1SXC485 = placet_get_name_number_list("beamline", "TL1.SXC485");
    TL1SXC495 = placet_get_name_number_list("beamline", "TL1.SXC495");

    TL1SX225 = placet_get_name_number_list("beamline", "TL1.SX225");
    TL1SX245 = placet_get_name_number_list("beamline", "TL1.SX245");
    TL1SX285 = placet_get_name_number_list("beamline", "TL1.SX285");

    TL1SX325 = placet_get_name_number_list("beamline", "TL1.SX325");
    TL1SX365 = placet_get_name_number_list("beamline", "TL1.SX365");
    TL1SX385 = placet_get_name_number_list("beamline", "TL1.SX385");

    TL1SX525 = placet_get_name_number_list("beamline", "TL1.SX525");
    TL1SX545 = placet_get_name_number_list("beamline", "TL1.SX545");
    TL1SX585 = placet_get_name_number_list("beamline", "TL1.SX585");

    TL1SX625 = placet_get_name_number_list("beamline", "TL1.SX625");
    TL1SX665 = placet_get_name_number_list("beamline", "TL1.SX665");
    TL1SX685 = placet_get_name_number_list("beamline", "TL1.SX685");

    TL1Q53 = placet_get_name_number_list("beamline", "TL1.Q53");
    TL1Q55 = placet_get_name_number_list("beamline", "TL1.Q55");
    TL1Q60 = placet_get_name_number_list("beamline", "TL1.Q60*");
    TL1Q65 = placet_get_name_number_list("beamline", "TL1.Q65");
    TL1Q67 = placet_get_name_number_list("beamline", "TL1.Q67");


    placet_element_set_attribute("beamline", TL1SX225 , "strength",TL1OPTS(1));
    placet_element_set_attribute("beamline", TL1SX245 , "strength",TL1OPTS(2));
    placet_element_set_attribute("beamline", TL1SX285 , "strength",TL1OPTS(3));
    placet_element_set_attribute("beamline", TL1SX325 , "strength",TL1OPTS(4));
    placet_element_set_attribute("beamline", TL1SX365 , "strength",TL1OPTS(5));
    placet_element_set_attribute("beamline", TL1SX385 , "strength",TL1OPTS(6));

    placet_element_set_attribute("beamline", TL1SXC415 , "strength",TL1OPTX(1));
    placet_element_set_attribute("beamline", TL1SXC425 , "strength",TL1OPTX(2));
    placet_element_set_attribute("beamline", TL1SXC435 , "strength",TL1OPTX(3));
    placet_element_set_attribute("beamline", TL1SXC445 , "strength",TL1OPTX(4));
    placet_element_set_attribute("beamline", TL1SXC455 , "strength",TL1OPTX(5));
    placet_element_set_attribute("beamline", TL1SXC465 , "strength",TL1OPTX(6));
    placet_element_set_attribute("beamline", TL1SXC475 , "strength",TL1OPTX(7));
    placet_element_set_attribute("beamline", TL1SXC485 , "strength",TL1OPTX(8));
    placet_element_set_attribute("beamline", TL1SXC495 , "strength",TL1OPTX(9));

    placet_element_set_attribute("beamline", TL1SX525 , "strength",TL1OPTS2(1));
    placet_element_set_attribute("beamline", TL1SX545 , "strength",TL1OPTS2(2));
    placet_element_set_attribute("beamline", TL1SX585 , "strength",TL1OPTS2(3));
    placet_element_set_attribute("beamline", TL1SX625 , "strength",TL1OPTS2(4));
    placet_element_set_attribute("beamline", TL1SX665 , "strength",TL1OPTS2(5));
    placet_element_set_attribute("beamline", TL1SX685 , "strength",TL1OPTS2(6));

    placet_element_set_attribute("beamline", TL1Q53 , "strength",TL1Q(1));
    placet_element_set_attribute("beamline", TL1Q55 , "strength",TL1Q(2));
    placet_element_set_attribute("beamline", TL1Q60 , "strength",TL1Q(3));
    placet_element_set_attribute("beamline", TL1Q65 , "strength",TL1Q(4));
    placet_element_set_attribute("beamline", TL1Q67 , "strength",TL1Q(5));


################  Kvar TL1 #######################
#    Kvar=[TL1SXC415,TL1SXC425,TL1SXC435,TL1SXC445,TL1SXC455,TL1SXC465,TL1SXC475,TL1SXC485,TL1SXC495];
#,TL1SX225,TL1SX245,TL1SX285, TL1SX325,TL1SX365,TL1SX385,TL1SX525,TL1SX545,TL1SX585,TL1SX625,TL1SX665,TL1SX685];
#     Kvar = [TL1SX225,TL1SX245,TL1SX285,TL1SX325,TL1SX365,TL1SX385,TL1SX525,TL1SX545,TL1SX585,TL1SX625,TL1SX665,TL1SX685];
#      Kvar =[TL1Q53 ,TL1Q55 ,TL1Q60(1) ,TL1Q65 ,TL1Q67, TL1SX525,TL1SX545,TL1SX585,TL1SX625,TL1SX665,TL1SX685];

##################  CR1 #######################

#          SNCH		SN0	SN1	SN3 	SXTR1 	SXTR3 	SXTR5 	SX1CC 	SX3CC SD0
OPTQS=[ -1.07348433619901e+00   5.53832413885354e-01];
OPTQL=[ -1.79801597815565e+00   2.75032058098249e+00  -9.34509511124660e-01   1.28321828597031e+00  -1.93144657730411e+00   1.17496129483168e+00	];

OPT= [  5.29130952368813e+00   -5.35961368137582e+00  -1.68017111319711e+01   3.86772098712126e+00];
OPT=[ -26.1737178479187 -7.478813948196576 -16.29428889503573 4.443320624499688];


OPTSXTR=[  -4.05660366869620e+01   3.66901426706648e+01   2.56627517952269e+00 -1.34182832906617e+01   2.47800480039339e+00];
#OPTSXTR=[ -5.05248129876607e+01   3.04413368823193e+01  -7.19056492425764e+00  -3.76499610181546e+01   1.57003957542069e+00];

OPTQT=[   -1.81026488446796e+00   1.94367570586367e+00  -2.19433626554624e+00  6.06480047957542e-01];
OPTQSCC=[ -1.09074156840409e+00   1.56579277312701e+00  -1.66698760423252e+00];

    CR1SNCH = placet_get_name_number_list("beamline","CR1.DBA.SNCH1");
    CR1SD0 = placet_get_name_number_list("beamline", "CR1.DBA.SD0");
    CR1SN0 = placet_get_name_number_list("beamline", "CR1.DBA.SN0");
    CR1SN1 = placet_get_name_number_list("beamline", "CR1.DBA.SN1");
    CR1SN3 = placet_get_name_number_list("beamline", "CR1.DBA.SN3");
    CR1SXTR1 = placet_get_name_number_list("beamline", "CR1.SXTR1");
    CR1SXTR3 = placet_get_name_number_list("beamline", "CR1.SXTR3");
    CR1SXTR5 = placet_get_name_number_list("beamline", "CR1.SXTR5");
    CR1SX1CC = placet_get_name_number_list("beamline", "CR1.SX1CC");
    CR1SX3CC = placet_get_name_number_list("beamline", "CR1.SX3CC");

    CR1QF1DBA = placet_get_name_number_list("beamline", "CR1.DBA.QF1");
    QCR1F2DBA = placet_get_name_number_list("beamline", "CR1.DBA.QF2");
    CR1QF3DBA = placet_get_name_number_list("beamline", "CR1.DBA.QF3");

    CR1QI1DBA = placet_get_name_number_list("beamline", "CR1.DBA.QI1");
    CR1QI2DBA = placet_get_name_number_list("beamline", "CR1.DBA.QI2");
    CR1QS1DBA = placet_get_name_number_list("beamline", "CR1.DBA.QS1C");
    CR1QS2DBA = placet_get_name_number_list("beamline", "CR1.DBA.QS2C");


###################################################
    CR1QL1IN = placet_get_name_number_list("beamline", "CR1.QL1IN");
    CR1QL2IN = placet_get_name_number_list("beamline", "CR1.QL2IN");
    CR1QL3IN = placet_get_name_number_list("beamline", "CR1.QL3IN");
    CR1QL4IN = placet_get_name_number_list("beamline", "CR1.QL4IN");
    CR1QL5IN = placet_get_name_number_list("beamline", "CR1.QL5IN");
    CR1QL6IN = placet_get_name_number_list("beamline", "CR1.QL6IN");

    CR1QTR1 = placet_get_name_number_list("beamline", "CR1.QTR1");
    CR1QTR2 = placet_get_name_number_list("beamline", "CR1.QTR2");
    CR1QTR3 = placet_get_name_number_list("beamline", "CR1.QTR3");
    CR1QTR4 = placet_get_name_number_list("beamline", "CR1.QTR4");

    CR1QS1CC = placet_get_name_number_list("beamline", "CR1.QS1C");
    CR1QS3CC = placet_get_name_number_list("beamline", "CR1.QS3C");
    CR1QS5CC = placet_get_name_number_list("beamline", "CR1.QS5C");

###################################################

    placet_element_set_attribute("beamline", CR1SNCH , "strength",OPT(1));
    placet_element_set_attribute("beamline", CR1SN0 , "strength",OPT(2));
    placet_element_set_attribute("beamline", CR1SN1 , "strength",OPT(3));
    placet_element_set_attribute("beamline", CR1SN3 , "strength",OPT(4));

    placet_element_set_attribute("beamline", CR1SXTR1 , "strength",OPTSXTR(1));
    placet_element_set_attribute("beamline", CR1SXTR3 , "strength",OPTSXTR(2));
    placet_element_set_attribute("beamline", CR1SXTR5 , "strength",OPTSXTR(3));
    placet_element_set_attribute("beamline", CR1SX1CC , "strength",OPTSXTR(4));
    placet_element_set_attribute("beamline", CR1SX3CC , "strength",OPTSXTR(5));


    placet_element_set_attribute("beamline", CR1QS1CC , "strength",OPTQSCC(1));
    placet_element_set_attribute("beamline", CR1QS3CC , "strength",OPTQSCC(2));
    placet_element_set_attribute("beamline", CR1QS5CC , "strength",OPTQSCC(3));

    placet_element_set_attribute("beamline", CR1QTR1 , "strength",OPTQT(1));
    placet_element_set_attribute("beamline", CR1QTR2 , "strength",OPTQT(2));
    placet_element_set_attribute("beamline", CR1QTR3 , "strength",OPTQT(3));
    placet_element_set_attribute("beamline", CR1QTR4 , "strength",OPTQT(4));

    placet_element_set_attribute("beamline", CR1QS1CC , "strength",OPTQSCC(1));
    placet_element_set_attribute("beamline", CR1QS3CC , "strength",OPTQSCC(2));
    placet_element_set_attribute("beamline", CR1QS5CC , "strength",OPTQSCC(3));


################  Kvar CR1 #######################

#    Kvar=[CR1SXTR1(1),CR1SXTR3(1),CR1SXTR5(1),CR1SX1CC(1),CR1SX3CC(1)];
#    Kvar=[CR1QS1C(1),CR1QS2C(1)];
#    Kvar=[CR1QS1CC(1),CR1QS3CC(1),CR1QS5CC(1),CR1SX1CC(1),CR1SX3CC(1)];
#    Kvar=[CR1QTR1(1),CR1QTR2(1),CR1QTR3(1),CR1QTR4(1),CR1SXTR1(1),CR1SXTR3(1),CR1SXTR5(1)];
#    Kvar=[CR1QL1IN(1),CR1QL2IN(1),CR1QL3IN(1),CR1SX1CC(1),CR1SX3CC(1)];#QL4IN(1),QL5IN(1),QL6IN(1)];
#    Kvar=[CR1QF1DBA(1),CR1QF2DBA(1),CR1QF3DBA(1)];
#    Kvar =[    CR1QI1DBA(1), CR1QI2DBA(1), CR1QS1DBA(1), CR1QS2DBA(1)];
#    Kvar=[CR1QS1CC(1),CR1QS3CC(1),CR1QS5CC(1),CR1SNCH(1),CR1SN0(1),CR1SN1(1),CR1SN3(1)];
#     Kvar = [CR1SNCH(1), CR1SN0(1), CR1SN1(1), CR1SN3(1)];#,CR1SXTR1(1),CR1SXTR3(1),CR1SXTR5(1),CR1SX1CC(1),CR1SX3CC(1),CR1QTR1(1),CR1QTR2(1),CR1QTR3(1),CR1QTR4(1),CR1QS1CC(1),CR1QS3CC(1),CR1QS5CC(1) ];

################  Kvar TL2 #######################
OPTX=[  5.80391284385692e+00  -1.61119929827926e+00  -7.60872648701929e-01   1.13406235457690e+01  -3.73242641263048e+00  -2.62143680906083e+00];
OPTX=[ 14.80799856517664 -12.38575159683516 10.12309114611427 -1.431021709137697 12.01585367287523 -15.19041857522175];

OPTX2=[   1.67375989671776e+01  -4.73491684067275e+00  -4.38664410481842e+00];
#OPTX2=[13.27748224183206 1.005552738380775 -3.494819489761342];
OPTQ1=[  1.55435526107697e+00  -1.54488361680510e+00   1.39859170512506e+00  -6.93316160964791e-01];

OPTX3=[  -1.50413701048430e+01   6.61683347964666e+00   1.46375372137468e-01];
OPTX4=[  -3.80472840289631e+00   1.68933348368347e+01   1.27651057021638e-01   3.80499900988887e-02  -1.54739833053275e+01   2.57941254124367e+01];
OPTX4=[ -4.674918430678527 17.6312674417813 0.377945682791398 0.1198282480608121 -17.6816420001805 28.59409381001929];

OPTQ=[  1.63190031935760e+00  -1.42570250294737e+00   9.44119686454303e-01  -2.01261498824627e+00   2.04755740669061e+00  -6.38494423947751e-01   1.86291380443829e+00  -1.69669544758597e+00];

OPTC=[   2.08016126234678e+01  -1.45720671572831e+01  -8.78733914607145e+00  -4.50059698772909e-01  -8.43049463906953e-01];
OPTC=[ 9.798451735777848 -20.24769750286783 -14.58867154110562 82.69810991446334 -14.96674821282327];

OPTC2=[    5.22533869360115e+01  -3.28803749739985e-01   9.81747490497000e+00   1.76803049396469e+02  -4.08598168479112e+01   1.39166776169285e+02   2.17187676109896e+00  -1.01686551343309e+02   5.78127694131782e+01  -4.19014277378230e+01];

OPTQ3=[     8.81069902435634e-01  -1.48898574352675e+00   7.11459149010814e-01  -1.74052457474494e+00   8.99554553192201e-01];



    TL2Q72= placet_get_name_number_list("beamline", "TL2.Q72");
    TL2Q74= placet_get_name_number_list("beamline", "TL2.Q74");
    TL2Q76= placet_get_name_number_list("beamline", "TL2.Q76");
    TL2Q78= placet_get_name_number_list("beamline", "TL2.Q78");

    TL2Q132= placet_get_name_number_list("beamline", "TL2.Q132");
    TL2Q133= placet_get_name_number_list("beamline", "TL2.Q133");
    TL2Q134= placet_get_name_number_list("beamline", "TL2.Q134");
    TL2Q135= placet_get_name_number_list("beamline", "TL2.Q135");
    TL2Q136= placet_get_name_number_list("beamline", "TL2.Q136");
    TL2Q137= placet_get_name_number_list("beamline", "TL2.Q137");
    TL2Q138= placet_get_name_number_list("beamline", "TL2.Q138");
    TL2Q139= placet_get_name_number_list("beamline", "TL2.Q139");

    TL2Q143= placet_get_name_number_list("beamline", "TL2.Q143");
    TL2Q145= placet_get_name_number_list("beamline", "TL2.Q145");
    TL2Q150= [placet_get_name_number_list("beamline", "TL2.Q150A"), placet_get_name_number_list("beamline", "TL2.Q150B")];
    TL2Q155= placet_get_name_number_list("beamline", "TL2.Q155");
    TL2Q157= placet_get_name_number_list("beamline", "TL2.Q157");

    TL2SX125= placet_get_name_number_list("beamline", "TL2.SX125");
    TL2SX145= placet_get_name_number_list("beamline", "TL2.SX145");
    TL2SX185= placet_get_name_number_list("beamline", "TL2.SX185");
    TL2SX225= placet_get_name_number_list("beamline", "TL2.SX225");
    TL2SX265= placet_get_name_number_list("beamline", "TL2.SX265");
    TL2SX285= placet_get_name_number_list("beamline", "TL2.SX285");

    TL2SXC305 = [placet_get_name_number_list("beamline", "TL2.SXC305") placet_get_name_number_list("beamline", "TL2.SXC395")];
    TL2SXC315 = [placet_get_name_number_list("beamline", "TL2.SXC315") placet_get_name_number_list("beamline", "TL2.SXC385")];
    TL2SXC325 = [placet_get_name_number_list("beamline", "TL2.SXC325")  placet_get_name_number_list("beamline", "TL2.SXC375")];
    TL2SXC335 = [placet_get_name_number_list("beamline", "TL2.SXC335")  placet_get_name_number_list("beamline", "TL2.SXC365")];
    TL2SXC345 = [placet_get_name_number_list("beamline", "TL2.SXC345")  placet_get_name_number_list("beamline", "TL2.SXC355")];

     TL2SX425= [placet_get_name_number_list("beamline", "TL2.SX425") placet_get_name_number_list("beamline", "TL2.SX525")];
     TL2SX445= [placet_get_name_number_list("beamline", "TL2.SX445") placet_get_name_number_list("beamline", "TL2.SX565")];
     TL2SX485= [placet_get_name_number_list("beamline", "TL2.SX485") placet_get_name_number_list("beamline", "TL2.SX585")];

     TL2SX1125= placet_get_name_number_list("beamline", "TL2.SX1125");
     TL2SX1145= placet_get_name_number_list("beamline", "TL2.SX1145");
     TL2SX1185= placet_get_name_number_list("beamline", "TL2.SX1185");

    TL2SXC1305 = placet_get_name_number_list("beamline", "TL2.SXC1305");
    TL2SXC1315 = placet_get_name_number_list("beamline", "TL2.SXC1315");
    TL2SXC1325 = placet_get_name_number_list("beamline", "TL2.SXC1325");
    TL2SXC1335 = placet_get_name_number_list("beamline", "TL2.SXC1335");
    TL2SXC1345 = placet_get_name_number_list("beamline", "TL2.SXC1345");
    TL2SXC1355 = placet_get_name_number_list("beamline", "TL2.SXC1355");
    TL2SXC1365 = placet_get_name_number_list("beamline", "TL2.SXC1365");
    TL2SXC1375 = placet_get_name_number_list("beamline", "TL2.SXC1375");
    TL2SXC1385 = placet_get_name_number_list("beamline", "TL2.SXC1385");
    TL2SXC1395 = placet_get_name_number_list("beamline", "TL2.SXC1395");

     TL2SX1425= placet_get_name_number_list("beamline", "TL2.SX1425");
     TL2SX1445= placet_get_name_number_list("beamline", "TL2.SX1445");
     TL2SX1485= placet_get_name_number_list("beamline", "TL2.SX1485");
     TL2SX1525= placet_get_name_number_list("beamline", "TL2.SX1525");
     TL2SX1565= placet_get_name_number_list("beamline", "TL2.SX1565");
     TL2SX1585= placet_get_name_number_list("beamline", "TL2.SX1585");


    placet_element_set_attribute("beamline", TL2SXC305 , "strength",OPTC(1));
    placet_element_set_attribute("beamline", TL2SXC315 , "strength",OPTC(2));
    placet_element_set_attribute("beamline", TL2SXC325 , "strength",OPTC(3));
    placet_element_set_attribute("beamline", TL2SXC335 , "strength",OPTC(4));
    placet_element_set_attribute("beamline", TL2SXC345 , "strength",OPTC(5));
#    placet_element_set_attribute("beamline", TL2SXC355 , "strength",OPTC(5));
#    placet_element_set_attribute("beamline", TL2SXC365 , "strength",OPTC(4));
#    placet_element_set_attribute("beamline", TL2SXC375 , "strength",OPTC(3));
#    placet_element_set_attribute("beamline", TL2SXC385 , "strength",OPTC(2));
#    placet_element_set_attribute("beamline", TL2SXC395 , "strength",OPTC(1));

    placet_element_set_attribute("beamline", TL2SXC1305 , "strength",OPTC2(1));
    placet_element_set_attribute("beamline", TL2SXC1315 , "strength",OPTC2(2));
    placet_element_set_attribute("beamline", TL2SXC1325 , "strength",OPTC2(3));
    placet_element_set_attribute("beamline", TL2SXC1335 , "strength",OPTC2(4));
    placet_element_set_attribute("beamline", TL2SXC1345 , "strength",OPTC2(5));
    placet_element_set_attribute("beamline", TL2SXC1355 , "strength",OPTC2(6));
    placet_element_set_attribute("beamline", TL2SXC1365 , "strength",OPTC2(7));
    placet_element_set_attribute("beamline", TL2SXC1375 , "strength",OPTC2(8));
    placet_element_set_attribute("beamline", TL2SXC1385 , "strength",OPTC2(9));
    placet_element_set_attribute("beamline", TL2SXC1395 , "strength",OPTC2(10));



    placet_element_set_attribute("beamline", TL2SX125 , "strength",OPTX(1));
    placet_element_set_attribute("beamline", TL2SX145 , "strength",OPTX(2));
    placet_element_set_attribute("beamline", TL2SX185 , "strength",OPTX(3));
    placet_element_set_attribute("beamline", TL2SX225 , "strength",OPTX(4));
    placet_element_set_attribute("beamline", TL2SX265 , "strength",OPTX(5));
    placet_element_set_attribute("beamline", TL2SX285 , "strength",OPTX(6));

    placet_element_set_attribute("beamline",TL2SX425 , "strength",OPTX2(1));
    placet_element_set_attribute("beamline",TL2SX445 , "strength",OPTX2(2));
    placet_element_set_attribute("beamline",TL2SX485 , "strength",OPTX2(3));

    placet_element_set_attribute("beamline", TL2SX1125 , "strength",OPTX3(1));
    placet_element_set_attribute("beamline", TL2SX1145 , "strength",OPTX3(2));
    placet_element_set_attribute("beamline", TL2SX1185 , "strength",OPTX3(3));


    placet_element_set_attribute("beamline", TL2SX1425 , "strength",OPTX4(1));
    placet_element_set_attribute("beamline", TL2SX1445 , "strength",OPTX4(2));
    placet_element_set_attribute("beamline", TL2SX1485 , "strength",OPTX4(3));
    placet_element_set_attribute("beamline", TL2SX1525 , "strength",OPTX4(4));
    placet_element_set_attribute("beamline", TL2SX1565 , "strength",OPTX4(5));
    placet_element_set_attribute("beamline", TL2SX1585 , "strength",OPTX4(6));

    placet_element_set_attribute("beamline",TL2Q143 , "strength",OPTQ3(1));
    placet_element_set_attribute("beamline",TL2Q145 , "strength",OPTQ3(2));
    placet_element_set_attribute("beamline",TL2Q150 , "strength",OPTQ3(3));
    placet_element_set_attribute("beamline",TL2Q155 , "strength",OPTQ3(4));
    placet_element_set_attribute("beamline",TL2Q157 , "strength",OPTQ3(5));

#   Kvar = [TL2SX125,TL2SX145,TL2SX185, TL2SX225,TL2SX265,TL2SX285];#,TL2SXC305(1),TL2SXC315(1), TL2SXC325(1), TL2SXC335(1), TL2SXC345(1) ,TL2SX425(1),TL2SX445(1),TL2SX485(1),TL2SX1125(1),TL2SX1145(1),TL2SX1185(1),TL2SX1425,TL2SX1445,TL2SX1485,TL2SX1525,TL2SX1565,TL2SX1585];
#   Kvar = [TL2Q72,TL2Q74,TL2Q76,TL2Q78,TL2SX425(1),TL2SX445(1),TL2SX485(1)];
#   Kvar = [TL2SXC305(1),TL2SXC315(1), TL2SXC325(1), TL2SXC335(1), TL2SXC345(1)]
#   Kvar = [TL2SX425(1),TL2SX445(1),TL2SX485(1)];
#   Kvar = [TL2SX1125(1),TL2SX1145(1),TL2SX1185(1),TL2SX1425,TL2SX1445,TL2SX1485,TL2SX1525,TL2SX1565,TL2SX1585, TL2Q132,TL2Q133,TL2Q134,TL2Q135,TL2Q136 ,TL2Q137 ,TL2Q138 ,TL2Q139];
#   Kvar = [TL2SXC1305, TL2SXC1315, TL2SXC1325, TL2SXC1335, TL2SXC1345, TL2SXC1355, TL2SXC1365, TL2SXC1375, TL2SXC1385, TL2SXC1395 ,TL2SX1425,TL2SX1445,TL2SX1485,TL2SX1525,TL2SX1565,TL2SX1585]
#   Kvar = [TL2SX1425,TL2SX1445,TL2SX1485,TL2SX1525,TL2SX1565,TL2SX1585];
#TL2Q132,TL2Q133,TL2Q134,TL2Q135,
#   Kvar = [ TL2SXC1325, TL2SXC1335, TL2SXC1345, TL2SXC1355,TL2SXC1365, TL2SXC1375, TL2SXC1385,TL2SXC1395, TL2Q132,TL2Q133,TL2Q134,TL2Q135,TL2Q136 ,TL2Q137 ,TL2Q138 ,TL2Q139];
#   Kvar=[TL2Q143(1),TL2Q145(1),TL2Q150(1),TL2Q155(1),TL2Q157(1), TL2SX1125(1),TL2SX1145(1),TL2SX1185(1),TL2SX1425,TL2SX1445,TL2SX1485,TL2SX1525,TL2SX1565,TL2SX1585, TL2SXC1305, TL2SXC1315, TL2SXC1325, TL2SXC1335, TL2SXC1345, TL2SXC1355, TL2SXC1365, TL2SXC1375, TL2SXC1385, TL2SXC1395];


########################  CR2 ############################################
########################  CR2 ############################################
    CR2SNCH = placet_get_name_number_list("beamline", "CR2.DBA.SNCH1");
    CR2SF0 = placet_get_name_number_list("beamline", "CR2.DBA.SF0");
    CR2SD0 = placet_get_name_number_list("beamline", "CR2.DBA.SD0");
    CR2SN1 = placet_get_name_number_list("beamline", "CR2.DBA.SN1");
    CR2SN3 = placet_get_name_number_list("beamline", "CR2.DBA.SN3");
    CR2SNN3 = placet_get_name_number_list("beamline", "CR2.DBA.SNN3");
    CR2SXTR1 = placet_get_name_number_list("beamline", "CR2.SXTR1");
    CR2SXTR2 = placet_get_name_number_list("beamline", "CR2.SXTR2");
    CR2SXTR3 = placet_get_name_number_list("beamline", "CR2.SXTR3");
    CR2SXTR4 = placet_get_name_number_list("beamline", "CR2.SXTR4");
    CR2SXS1C = placet_get_name_number_list("beamline", "CR2.SXS1C");
    CR2SXS3C = placet_get_name_number_list("beamline", "CR2.SXS3C");
    CR2SRFB6 = placet_get_name_number_list("beamline", "CR2.SRFB6");

    CR2QTR1 = placet_get_name_number_list("beamline", "CR2.QTR1");
    CR2QTR2 = placet_get_name_number_list("beamline", "CR2.QTR2");
    CR2QTR3 = placet_get_name_number_list("beamline", "CR2.QTR3");
    CR2QTR4 = placet_get_name_number_list("beamline", "CR2.QTR4");

    CR2QS1C = placet_get_name_number_list("beamline", "CR2.QS1C");
    CR2QS2C = placet_get_name_number_list("beamline", "CR2.QS2C");
    CR2QS3C = placet_get_name_number_list("beamline", "CR2.QS3C");


############################################
OPTSXTRCR2=[  -2.644150278142905 -14.43745863115238 10.8128652257232 -27.91167354879081 5.256927573348031 -0.5665626457176449 -37.96226493529128  39.58442028010441 20.21176433255361 ];
OPTSXTRCR2=[ -2.657372173114483 -17.24355412558036 9.690713903089996 -26.99836997114769 7.98525855094706 -0.7700545708027567 -31.20107311397332 22.56740709198722 20.21176433255361 ];

OPTQTRCR2=[ -0.879549230770158 1.232463201743673 -0.3737579762019467  -1.620883233373443 1.766639355239857 -0.1566061396002063 ];

#    placet_element_set_attribute("beamline", SNCH , "strength",OPTCR2(1));
#    placet_element_set_attribute("beamline", SF0 , "strength",OPTCR2(2));
#    placet_element_set_attribute("beamline", SD0 , "strength",OPTCR2(3));
#    placet_element_set_attribute("beamline", SN1 , "strength",OPTCR2(4));
#    placet_element_set_attribute("beamline", SN3 , "strength",OPTCR2(5));

    placet_element_set_attribute("beamline", CR2SF0 , "strength",OPTSXTRCR2(1));
    placet_element_set_attribute("beamline", CR2SD0 , "strength",OPTSXTRCR2(2));
    placet_element_set_attribute("beamline", CR2SXTR1 , "strength",OPTSXTRCR2(3));
    placet_element_set_attribute("beamline", CR2SXTR2 , "strength",OPTSXTRCR2(4));
    placet_element_set_attribute("beamline", CR2SXTR3 , "strength",OPTSXTRCR2(5));
    placet_element_set_attribute("beamline", CR2SXTR4 , "strength",OPTSXTRCR2(6));
    placet_element_set_attribute("beamline", CR2SXS1C , "strength",OPTSXTRCR2(7));
    placet_element_set_attribute("beamline", CR2SXS3C , "strength",OPTSXTRCR2(8));
    placet_element_set_attribute("beamline", CR2SRFB6(1) , "strength",OPTSXTRCR2(9));

    placet_element_set_attribute("beamline", CR2QTR1 , "strength",OPTQTRCR2(1));
    placet_element_set_attribute("beamline", CR2QTR2 , "strength",OPTQTRCR2(2));
    placet_element_set_attribute("beamline", CR2QTR3 , "strength",OPTQTRCR2(3));
    placet_element_set_attribute("beamline", CR2QS1C , "strength",OPTQTRCR2(4));
    placet_element_set_attribute("beamline", CR2QS2C , "strength",OPTQTRCR2(5));
    placet_element_set_attribute("beamline", CR2QS3C , "strength",OPTQTRCR2(6));
#    Kvar=[CR2SF0(1),CR2SD0(1),CR2SXS1C(1),CR2SXS3C(1)];
    Kvar=[CR2SF0(1),CR2SD0(1),CR2SXTR1(1),CR2SXTR2(1),CR2SXTR3(1),CR2SXTR4(1),CR2SXS1C(1),CR2SXS3C(1)];#,CR2SRFB6(1)];#,SRFB6(1)];#,SXTR1(1),SXTR3(1),SXTR5(1)]SNCH(1),SF0(1),SD0(1),SN1(1),
#   Kvar =[CR2SXTR1(1),CR2SXTR2(1),CR2SXTR3(1),CR2QTR1(1),CR2QTR2(1),CR2QTR3(1),CR2QTR4(1)];
#    Kvar=[CR2SXTR1(1),CR2SXTR2(1),CR2SXTR3(1),CR2SXTR4(1),CR2SXS1C(1),CR2SXS3C(1),CR2QTR1(1),CR2QTR2(1),CR2QTR3(1),CR2QTR4(1),CR2QS1C(1),CR2QS2C(1),CR2QS3C(1)];

########################################################################################
####################  TL3 TTA  #########################

OPTL3=[   1.61881761942962e+01  -5.32893141721975e+00   9.68021652761348e-01   2.00410886625408e-02  -4.34004538648342e+00   1.24722151639192e+01];
OPTL3=[	 9.324672608925527 -5.827714679189655 2.696102178903538 0.03740970646258505 -4.95743956910607 9.324672608925527 ];
OPTTA = [ -7.36751897966608e+00   1.79398251408774e+01  -5.04129476097968e+00];
#OPTTA = [ 2.221026834143853 15.25340407983551 -5.978057639251042   ];

TL3SX125= placet_get_name_number_list("beamline", "TL3.SX125");
TL3SX145= placet_get_name_number_list("beamline", "TL3.SX145");
TL3SX185= placet_get_name_number_list("beamline", "TL3.SX185");

TL3SX225= placet_get_name_number_list("beamline", "TL3.SX225");
TL3SX265= placet_get_name_number_list("beamline", "TL3.SX265");
TL3SX285= placet_get_name_number_list("beamline", "TL3.SX285");

TTASX1= placet_get_name_number_list("beamline", "TTA.SX1");
TTASX2= placet_get_name_number_list("beamline", "TTA.SX2");
TTASX3= placet_get_name_number_list("beamline", "TTA.SX3");

placet_element_set_attribute("beamline", TL3SX125 , "strength",OPTL3(1));
placet_element_set_attribute("beamline", TL3SX145 , "strength",OPTL3(2));
placet_element_set_attribute("beamline", TL3SX185 , "strength",OPTL3(3));
placet_element_set_attribute("beamline", TL3SX225 , "strength",OPTL3(4));
placet_element_set_attribute("beamline", TL3SX265 , "strength",OPTL3(5));
placet_element_set_attribute("beamline", TL3SX285 , "strength",OPTL3(6));

placet_element_set_attribute("beamline", TTASX1 , "strength",OPTTA(1));
placet_element_set_attribute("beamline", TTASX2 , "strength",OPTTA(2));
placet_element_set_attribute("beamline", TTASX3 , "strength",OPTTA(3));

#Kvar = [TL3SX125(1),TL3SX145(1),TL3SX185(1),TL3SX225(1),TL3SX265(1),TL3SX285(1)];
#Kvar = [TTASX1(1),TTASX2(1),TTASX3(1)];

KvarallSexts=[DGSEXT1,DGSEXT2,DLSNCH(1), DLSXL2(1),  DLSXL3(1),  DLSXL4(1),DLSN1(1), DLSN2(1), DLSN3(1),TL1SX225,TL1SX245,TL1SX285,TL1SX325,TL1SX365,TL1SX385 ,TL1SXC415 , TL1SXC425,TL1SXC435,TL1SXC445,TL1SXC455,TL1SXC465,TL1SXC475,TL1SXC485,TL1SXC495 , TL1SX525,TL1SX545,TL1SX585,TL1SX625,TL1SX665,TL1SX685 ,  CR1SNCH(1), CR1SN0(1), CR1SN1(1), CR1SN3(1),CR1SXTR1(1),CR1SXTR3(1),CR1SXTR5(1),CR1SX1CC(1),CR1SX3CC(1),TL2SX125,TL2SX145,TL2SX185, TL2SX225,TL2SX265,TL2SX285,TL2SXC305(1),TL2SXC315(1), TL2SXC325(1), TL2SXC335(1), TL2SXC345(1) ,TL2SX425(1),TL2SX445(1),TL2SX485(1),TL2SX1125(1),TL2SX1145(1),TL2SX1185(1),TL2SX1425,TL2SX1445,TL2SX1485,TL2SX1525,TL2SX1565,TL2SX1585,CR2SF0(1),CR2SD0(1),CR2SXTR1(1),CR2SXTR2(1),CR2SXTR3(1),CR2SXTR4(1),CR2SXS1C(1),CR2SXS3C(1),CR2SRFB6(1),TL3SX125(1),TL3SX145(1),TL3SX185(1),TL3SX225(1),TL3SX265(1),TL3SX285(1),TTASX1(1),TTASX2(1),TTASX3(1)];

KvarallQuads=[TL1Q53 ,TL1Q55 ,TL1Q60(1) ,TL1Q65 ,TL1Q67, CR1QTR1(1),CR1QTR2(1),CR1QTR3(1),CR1QTR4(1),CR1QS1CC(1),CR1QS3CC(1),CR1QS5CC(1),TL2Q143(1),TL2Q145(1),TL2Q150(1),TL2Q155(1),TL2Q157(1),CR2QTR1(1),CR2QTR2(1),CR2QTR3(1),CR2QTR4(1),CR2QS1C(1),CR2QS2C(1),CR2QS3C(1)];



    KName=placet_element_get_attribute("beamline", KvarallQuads, "name")
   KS2=placet_element_get_attribute("beamline", KvarallQuads, "strength")
      	for i=1:length(KS2)
		fprintf("%i %s %f\n",i ,KName(i,:),KS2(i));
	endfor

########################################################################################

    KS=placet_element_get_attribute("beamline", Kvar, "strength");
#    KName=placet_element_get_attribute("beamline", Kvar, "name");

