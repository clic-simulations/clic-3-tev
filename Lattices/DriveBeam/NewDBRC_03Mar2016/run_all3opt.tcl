set scripts /home/eduneo/Work/CLIC_DB/DOGLEG/PLACET

set script_dir /home/eduneo/Work/CLIC_DB/DOGLEG/PLACET

set basename ALL

array set args {
    opt1 0
    opt2 0
    charge 8.5e-9
}   
    
array set args $argv

set opt1 $args(opt1)hare
set opt2 $args(opt2)
set charge $args(charge)

source $scripts/init.tcl

proc CORRECTOR { a b c d e f} {
  Dipole $a $b $c $d $e $f ;
}
proc HCORRECTOR { a b c d e f} {
  Dipole $a $b $c $d $e $f ;
}

proc VCORRECTOR { a b c d e f} {
  Dipole $a $b $c $d $e $f ;
}

set e0 2.38
set e_initial $e0

set match(beta_x)   2.441040835 
set match(beta_y)  11.93446154   
set match(alpha_x) 0
set match(alpha_y)  0
set match(sigma_z) 2000.0

#source radconfig
set sbend_synrad 1
set quad_synrad 0
set mult_synrad 0
set csrON 1
set csrbins 200

# Lattice definition
Girder
source ./ALL3BPM.tcl

BeamlineSet -name "beamline"

puts "final energy = $e0"

#source beamconfig
set n_total 10000
set n_slice 20
set n 500

source $scripts/make_beam.tcl
source $scripts/clic_beam.tcl


set match(emitt_x) 1000 ; # 900
set match(emitt_y) 1000 ; # 40.9
set match(phase) 0.0
set charge 8.5e-9
set match(sigma_z) 2000.0
set match(e_spread) 0.85
set deltae 1e-4
set charge $args(charge)

make_beam_many beam0 $n_slice $n
make_beam_many beam00 $n_slice $n
make_beam_many beam000 $n_slice $n
make_beam_many beam1 $n_slice $n
make_beam_many beam2 $n_slice $n
make_beam_many beam3 $n_slice $n
make_beam_many beam4 $n_slice $n
make_beam_many beam5 $n_slice $n
make_beam_many beam6 $n_slice $n
make_beam_many beam7 $n_slice $n
make_beam_many beam8 $n_slice $n
make_beam_many beam9 $n_slice $n
make_beam_many beam10 $n_slice $n
make_beam_many beam11 $n_slice $n
make_beam_many beam12 $n_slice $n

BeamRead -file test_1.dat -beam beam0


DispersionFreeEmittance -on
Octave {
  global LOCs B0 B1 B2 dlinjection dlejection tl1injection tl1ejection cr1injection cr1ejection tl2injection tl2ejection cr2injection cr2ejection tl3ejection  ttaejection chicanend 
    SI = placet_get_number_list("beamline", "sbend");
    placet_element_set_attribute("beamline", SI, "six_dim", true);
    SEPT = placet_get_name_number_list("beamline", "SEPT*");
    B0 = placet_get_beam("beam0");
    B1 = placet_get_beam("beam1");
    B2 = placet_get_beam("beam2");
    B0(:,4) =  -1*B0(:,4) ;
    placet_set_beam("beam0", B0);

    CELLSCR1 = placet_get_name_number_list("beamline", "CR1MDBACELLSTART*");
    dlinjection = placet_get_name_number_list("beamline", "DL.INJECTION")    
    dlejection = placet_get_name_number_list("beamline", "DL.EJECTION")
    tl1injection = placet_get_name_number_list("beamline", "TL1.INJECTION")
    tl1ejection = placet_get_name_number_list("beamline", "TL1.EJECTION")
    cr1injection = placet_get_name_number_list("beamline", "CR1.INJECTION")
    cr1ejection = placet_get_name_number_list("beamline", "CR1.EJECTION")
    tl2injection = placet_get_name_number_list("beamline", "TL2.INJECTION")
    tl2ejection  = placet_get_name_number_list("beamline", "TL2.EJECTION")
    cr2injection = placet_get_name_number_list("beamline", "CR2.INJECTION")
    cr2ejection  = placet_get_name_number_list("beamline", "CR2.EJECTION")
    tl3ejection  = placet_get_name_number_list("beamline", "TL3.EJECTION")
    ttaejection  = placet_get_name_number_list("beamline", "TTA.EJECTION")
    chicanend  = placet_get_name_number_list("beamline", "CHICANEND")
    LOC=[dlejection tl1ejection cr1ejection tl2ejection cr2ejection  tl3ejection ttaejection chicanend];
    LOCs=placet_element_get_attribute("beamline",LOC  , "s")  
}

#exit
ParallelThreads -num 1

FirstOrder 1

#Octave {
#    R = placet_get_transfer_matrix("beamline")
#}

##################################################################

Octave {
	global LOCs Kvar B0  DLSNCH DLSXCH DLSXL2 DLSXL3 DLSXL4  DLSN1 DLSN2 DLSN3 DLSNN1 DLSNN2 DLSNN3 DLSX1 DLSX2 DLSX3 DLSXX1 DLSXX2 DLSXX3 TL1Q53 TL1Q55 TL1Q60 TL1Q65 TL1Q67  TL1SX525 TL1SX545 TL1SX585 TL1SX625 TL1SX665 TL1SX685 CR1SNCH CR1SN0 CR1SN1 CR1SN3 CR1SXTR1 CR1SXTR3 CR1SXTR5 CR1SX1CC CR1SX3CC CR1QTR1 CR1QTR2  CR1QTR3  CR1QTR4 CR1QS1CC CR1QS3CC CR1QS5CC TL2SX125 TL2SX145 TL2SX185 TL2SX225 TL2SX265 TL2SX285  TL2SXC305 TL2SXC315 TL2SXC325 TL2SXC335 TL2SXC345  TL2SX425 TL2SX445 TL2SX485 TL2SX1125 TL2SX1145 TL2SX1185 TL2SXC1305 TL2SXC1315 TL2SXC1325 TL2SXC1335 TL2SXC1345 TL2SXC1355 TL2SXC1365 TL2SXC1375 TL2SXC1385 TL2SXC1395    TL2SX1425  TL2SX1445  TL2SX1485  TL2SX1525 TL2SX1565 TL2SX1585 TL2Q132  TL2Q133  TL2Q134  TL2Q135 TL2Q136 TL2Q137 TL2Q138 TL2Q139  TL2Q72 TL2Q74 TL2Q76 TL2Q78  TL2Q143 TL2Q145 TL2Q150 TL2Q155 TL2Q157  CR2SNCH CR2SF0 CR2SD0 CR2SN1 CR2SXTR1 CR2SXTR2 CR2SXTR3 CR2SXTR4 CR2SXS1C CR2SXS3C CR2SRFB6 CR2QTR1 CR2QTR2 CR2QTR3 CR2QL1IN CR2QL2IN CR2QL3IN CR2QS1C CR2QS2C CR2QS3C TL3SX125 TL3SX145 TL3SX185 TL3SX225 TL3SX265 TL3SX285 TTASX1 TTASX2 TTASX3
    format	long e
    pkg load optim
    source optALL2optDL_CSRON_v2.tcl

}


#exit
### Emitt optimization##################################################################

##################################################################

Octave {
	global LOCs Kvar B0 dlinjection dlejection tl1injection tl1ejection cr1ejection tl2injection tl2ejection cr2injection cr2ejection tl3ejection  ttaejection chicanend
    ALLEMIT=zeros(13,12);
    function E=final_emittance(beamline)
	global LOCs CELLS INJS B0 B3 Kvar nele noff S dlejection tl1injection tl1ejection cr1ejection tl2injection tl2ejection cr2injection cr2ejection tl3ejection ttaejection chicanend DLSNCH DLSXCH DLSXL2 DLSXL3 DLSXL4 DLSN1 DLSN2 DLSN3 DLSNN1 DLSNN2 DLSNN3 DLSX1 DLSX2 DLSX3 DLSXX1 DLSXX2 DLSXX3 TL1Q53 TL1Q5 TL1Q60 TL1Q65 TL1Q67  TL1SX525 TL1SX545 TL1SX585 TL1SX625 TL1SX665 TL1SX685 CR1SNCH CR1SN0 CR1SN1 CR1SN3 CR1SXTR1 CR1SXTR3 CR1SXTR5 CR1SX1CC CR1SX3CC CR1QTR1 CR1QTR2  CR1QTR3  CR1QTR4  CR1QS1CC CR1QS3CC CR1QS5CC TL2SX125 TL2SX145 TL2SX185 TL2SX225 TL2SX265 TL2SX285  TL2SXC305 TL2SXC315 TL2SXC325 TL2SXC335 TL2SXC345  TL2SX425 TL2SX445 TL2SX485 TL2SX1125 TL2SX1145 TL2SX1185 TL2SXC1305 TL2SXC1315 TL2SXC1325 TL2SXC1335 TL2SXC1345 TL2SXC1355 TL2SXC1365 TL2SXC1375 TL2SXC1385 TL2SXC1395    TL2SX1425  TL2SX1445  TL2SX1485  TL2SX1525 TL2SX1565 TL2SX1585 TL2Q132  TL2Q133  TL2Q134  TL2Q135 TL2Q136 TL2Q137 TL2Q138 TL2Q139  TL2Q72 TL2Q74 TL2Q76 TL2Q78  TL2Q143 TL2Q145 TL2Q150 TL2Q155 TL2Q157  CR2SNCH CR2SF0 CR2SD0 CR2SN1 CR2SXTR1 CR2SXTR2 CR2SXTR3 CR2SXTR4 CR2SXS1C CR2SXS3C CR2SRFB6 CR2QTR1 CR2QTR2 CR2QTR3 CR2QL1IN CR2QL2IN CR2QL3IN CR2QS1C CR2QS2C CR2QS3C TL3SX125 TL3SX145 TL3SX185 TL3SX225 TL3SX265 TL3SX285 TTASX1 TTASX2 TTASX3


	    placet_element_set_attribute("beamline", DLSN1 , "strength",placet_element_get_attribute("beamline",DLSN1(1), "strength"));
	    placet_element_set_attribute("beamline", DLSN2 , "strength",placet_element_get_attribute("beamline",DLSN2(1), "strength"));
	    placet_element_set_attribute("beamline", DLSN3 , "strength",placet_element_get_attribute("beamline",DLSN3(1), "strength"));
	    placet_element_set_attribute("beamline", DLSNN1 , "strength",placet_element_get_attribute("beamline",DLSN1(1), "strength"));
	    placet_element_set_attribute("beamline", DLSNN2 , "strength",placet_element_get_attribute("beamline",DLSN2(1), "strength"));
	    placet_element_set_attribute("beamline", DLSNN3 , "strength",placet_element_get_attribute("beamline",DLSN3(1), "strength"));

	    placet_element_set_attribute("beamline", DLSX1 , "strength",-placet_element_get_attribute("beamline",DLSN1(1), "strength"));
	    placet_element_set_attribute("beamline", DLSX2 , "strength",-placet_element_get_attribute("beamline",DLSN2(1), "strength"));
	    placet_element_set_attribute("beamline", DLSX3 , "strength",-placet_element_get_attribute("beamline",DLSN3(1), "strength"));
	    placet_element_set_attribute("beamline", DLSXX1 , "strength",-placet_element_get_attribute("beamline",DLSN1(1), "strength"));
	    placet_element_set_attribute("beamline", DLSXX2 , "strength",-placet_element_get_attribute("beamline",DLSN2(1), "strength"));
	    placet_element_set_attribute("beamline", DLSXX3 , "strength",-placet_element_get_attribute("beamline",DLSN3(1), "strength"));

	    placet_element_set_attribute("beamline", DLSNCH , "strength",placet_element_get_attribute("beamline",DLSNCH(1), "strength"));
	    placet_element_set_attribute("beamline", DLSXCH , "strength",-1*placet_element_get_attribute("beamline",DLSNCH(1), "strength"));

	    placet_element_set_attribute("beamline", DLSXL2 , "strength",placet_element_get_attribute("beamline",DLSXL2(1), "strength"));
	    placet_element_set_attribute("beamline", DLSXL3 , "strength",placet_element_get_attribute("beamline",DLSXL3(1), "strength"));
	    placet_element_set_attribute("beamline", DLSXL4 , "strength",placet_element_get_attribute("beamline",DLSXL4(1), "strength"));

	    placet_element_set_attribute("beamline", TL1Q60 , "strength",placet_element_get_attribute("beamline", TL1Q60(1), "strength"));

	    placet_element_set_attribute("beamline", CR1SNCH , "strength",placet_element_get_attribute("beamline", CR1SNCH(1), "strength"));
	    placet_element_set_attribute("beamline", CR1SN0 , "strength",placet_element_get_attribute("beamline", CR1SN0(1), "strength"));
	    placet_element_set_attribute("beamline", CR1SN1 , "strength",placet_element_get_attribute("beamline", CR1SN1(1), "strength"));
	    placet_element_set_attribute("beamline", CR1SN3 , "strength",placet_element_get_attribute("beamline", CR1SN3(1), "strength"));
	
	    placet_element_set_attribute("beamline", CR1SXTR1 , "strength",placet_element_get_attribute("beamline", CR1SXTR1(1), "strength"));
	    placet_element_set_attribute("beamline", CR1SXTR3 , "strength",placet_element_get_attribute("beamline", CR1SXTR3(1), "strength"));
	    placet_element_set_attribute("beamline", CR1SXTR5 , "strength",placet_element_get_attribute("beamline", CR1SXTR5(1), "strength"));
	    placet_element_set_attribute("beamline", CR1SX1CC , "strength",placet_element_get_attribute("beamline", CR1SX1CC(1), "strength"));
	    placet_element_set_attribute("beamline", CR1SX3CC , "strength",placet_element_get_attribute("beamline", CR1SX3CC(1), "strength"));
	    placet_element_set_attribute("beamline", CR1QTR1 , "strength",placet_element_get_attribute("beamline", CR1QTR1(1), "strength"));
	    placet_element_set_attribute("beamline", CR1QTR2 , "strength",placet_element_get_attribute("beamline", CR1QTR2(1), "strength"));
	    placet_element_set_attribute("beamline", CR1QTR3 , "strength",placet_element_get_attribute("beamline", CR1QTR3(1), "strength"));
	    placet_element_set_attribute("beamline", CR1QTR4 , "strength",placet_element_get_attribute("beamline", CR1QTR4(1), "strength"));

#	    placet_element_set_attribute("beamline", CR1QS1CC , "strength",placet_element_get_attribute("beamline", CR1QS1CC(1), "strength"));
#	    placet_element_set_attribute("beamline", CR1QS3CC , "strength",placet_element_get_attribute("beamline", CR1QS3CC(1), "strength"));
#	    placet_element_set_attribute("beamline", CR1QS5CC , "strength",placet_element_get_attribute("beamline", CR1QS5CC(1), "strength"));

	    placet_element_set_attribute("beamline", TL2SX125 , "strength",placet_element_get_attribute("beamline",TL2SX125(1), "strength"));
	    placet_element_set_attribute("beamline", TL2SX145 , "strength",placet_element_get_attribute("beamline",TL2SX145(1), "strength"));
	    placet_element_set_attribute("beamline", TL2SX185 , "strength",placet_element_get_attribute("beamline",TL2SX185(1), "strength"));

	    placet_element_set_attribute("beamline", TL2SX225 , "strength",placet_element_get_attribute("beamline",TL2SX225(1), "strength"));
	    placet_element_set_attribute("beamline", TL2SX265 , "strength",placet_element_get_attribute("beamline",TL2SX265(1), "strength"));
	    placet_element_set_attribute("beamline", TL2SX285 , "strength",placet_element_get_attribute("beamline",TL2SX285(1), "strength"));

	    placet_element_set_attribute("beamline", TL2SXC305 , "strength",placet_element_get_attribute("beamline",TL2SXC305(1), "strength"));
	    placet_element_set_attribute("beamline", TL2SXC315 , "strength",placet_element_get_attribute("beamline",TL2SXC315(1), "strength"));
	    placet_element_set_attribute("beamline", TL2SXC325 , "strength",placet_element_get_attribute("beamline",TL2SXC325(1), "strength"));
	    placet_element_set_attribute("beamline", TL2SXC335 , "strength",placet_element_get_attribute("beamline",TL2SXC335(1), "strength"));
	    placet_element_set_attribute("beamline", TL2SXC345 , "strength",placet_element_get_attribute("beamline",TL2SXC345(1), "strength"));

	    placet_element_set_attribute("beamline", TL2SX425 , "strength",placet_element_get_attribute("beamline",TL2SX425(1), "strength"));
	    placet_element_set_attribute("beamline", TL2SX445 , "strength",placet_element_get_attribute("beamline",TL2SX445(1), "strength"));
	    placet_element_set_attribute("beamline", TL2SX485 , "strength",placet_element_get_attribute("beamline",TL2SX485(1), "strength"));

	    placet_element_set_attribute("beamline", TL2SX1125 , "strength",placet_element_get_attribute("beamline",TL2SX1125(1), "strength"));
	    placet_element_set_attribute("beamline", TL2SX1145 , "strength",placet_element_get_attribute("beamline",TL2SX1145(1), "strength"));
	    placet_element_set_attribute("beamline", TL2SX1185 , "strength",placet_element_get_attribute("beamline",TL2SX1185(1), "strength"));

	 placet_element_set_attribute("beamline", TL2SXC1305 , "strength",placet_element_get_attribute("beamline",TL2SXC1305(1), "strength"));
	 placet_element_set_attribute("beamline", TL2SXC1315 , "strength",placet_element_get_attribute("beamline",TL2SXC1315(1), "strength"));
	 placet_element_set_attribute("beamline", TL2SXC1325 , "strength",placet_element_get_attribute("beamline",TL2SXC1325(1), "strength"));
	 placet_element_set_attribute("beamline", TL2SXC1335 , "strength",placet_element_get_attribute("beamline",TL2SXC1335(1), "strength"));
	 placet_element_set_attribute("beamline", TL2SXC1345 , "strength",placet_element_get_attribute("beamline",TL2SXC1345(1), "strength"));
	 placet_element_set_attribute("beamline", TL2SXC1355 , "strength",placet_element_get_attribute("beamline",TL2SXC1355(1), "strength"));
	 placet_element_set_attribute("beamline", TL2SXC1365 , "strength",placet_element_get_attribute("beamline",TL2SXC1365(1), "strength"));
	 placet_element_set_attribute("beamline", TL2SXC1375 , "strength",placet_element_get_attribute("beamline",TL2SXC1375(1), "strength"));
	 placet_element_set_attribute("beamline", TL2SXC1385 , "strength",placet_element_get_attribute("beamline",TL2SXC1385(1), "strength"));
	 placet_element_set_attribute("beamline", TL2SXC1395 , "strength",placet_element_get_attribute("beamline",TL2SXC1395(1), "strength"));

	 placet_element_set_attribute("beamline", TL2SX1425 , "strength",placet_element_get_attribute("beamline",TL2SX1425(1), "strength"));
	 placet_element_set_attribute("beamline", TL2SX1445 , "strength",placet_element_get_attribute("beamline",TL2SX1445(1), "strength"));
	 placet_element_set_attribute("beamline", TL2SX1485 , "strength",placet_element_get_attribute("beamline",TL2SX1485(1), "strength"));
	 placet_element_set_attribute("beamline", TL2SX1525 , "strength",placet_element_get_attribute("beamline",TL2SX1525(1), "strength"));
	 placet_element_set_attribute("beamline", TL2SX1565 , "strength",placet_element_get_attribute("beamline",TL2SX1565(1), "strength"));
	 placet_element_set_attribute("beamline", TL2SX1585 , "strength",placet_element_get_attribute("beamline",TL2SX1585(1), "strength"));

	 placet_element_set_attribute("beamline", TL2Q150 , "strength",placet_element_get_attribute("beamline",TL2Q150(1), "strength"));

	    placet_element_set_attribute("beamline", CR2SNCH , "strength",placet_element_get_attribute("beamline", CR2SNCH(1), "strength"));
	    placet_element_set_attribute("beamline", CR2SF0 , "strength",placet_element_get_attribute("beamline", CR2SF0(1), "strength"));
	    placet_element_set_attribute("beamline", CR2SD0 , "strength",placet_element_get_attribute("beamline", CR2SD0(1), "strength"));
	    placet_element_set_attribute("beamline", CR2SN1 , "strength",placet_element_get_attribute("beamline", CR2SN1(1), "strength"));

	    placet_element_set_attribute("beamline", CR2SXTR1 , "strength",placet_element_get_attribute("beamline", CR2SXTR1(1), "strength"));
	    placet_element_set_attribute("beamline", CR2SXTR2 , "strength",placet_element_get_attribute("beamline", CR2SXTR2(1), "strength"));
	    placet_element_set_attribute("beamline", CR2SXTR3 , "strength",placet_element_get_attribute("beamline", CR2SXTR3(1), "strength"));
	    placet_element_set_attribute("beamline", CR2SXTR4 , "strength",placet_element_get_attribute("beamline", CR2SXTR4(1), "strength"));
	    placet_element_set_attribute("beamline", CR2SXS1C , "strength",placet_element_get_attribute("beamline", CR2SXS1C(1), "strength"));
	    placet_element_set_attribute("beamline", CR2SXS3C , "strength",placet_element_get_attribute("beamline", CR2SXS3C(1), "strength"));

	    placet_element_set_attribute("beamline", CR2SRFB6 , "strength",placet_element_get_attribute("beamline", CR2SRFB6(1), "strength"));
	    placet_element_set_attribute("beamline", CR2QTR1 , "strength",placet_element_get_attribute("beamline", CR2QTR1(1), "strength"));
	    placet_element_set_attribute("beamline", CR2QTR2 , "strength",placet_element_get_attribute("beamline", CR2QTR2(1), "strength"));
	    placet_element_set_attribute("beamline", CR2QTR3 , "strength",placet_element_get_attribute("beamline", CR2QTR3(1), "strength"));

	    placet_element_set_attribute("beamline", CR2QS1C , "strength",placet_element_get_attribute("beamline", CR2QS1C(1), "strength"));
	    placet_element_set_attribute("beamline", CR2QS2C , "strength",placet_element_get_attribute("beamline", CR2QS2C(1), "strength"));
	    placet_element_set_attribute("beamline", CR2QS3C , "strength",placet_element_get_attribute("beamline", CR2QS3C(1), "strength"));

	    placet_element_set_attribute("beamline", TL3SX125, "strength",placet_element_get_attribute("beamline", TL3SX125(1), "strength"));
	    placet_element_set_attribute("beamline", TL3SX125, "strength",placet_element_get_attribute("beamline", TL3SX145(1), "strength"));
	    placet_element_set_attribute("beamline", TL3SX125, "strength",placet_element_get_attribute("beamline", TL3SX185(1), "strength"));
	    placet_element_set_attribute("beamline", TL3SX125, "strength",placet_element_get_attribute("beamline", TL3SX225(1), "strength"));
	    placet_element_set_attribute("beamline", TL3SX125, "strength",placet_element_get_attribute("beamline", TL3SX265(1), "strength"));
	    placet_element_set_attribute("beamline", TL3SX125, "strength",placet_element_get_attribute("beamline", TL3SX285(1), "strength"));

	    placet_element_set_attribute("beamline",  TTASX1, "strength",placet_element_get_attribute("beamline",  TTASX1(1), "strength"));
	    placet_element_set_attribute("beamline",  TTASX2, "strength",placet_element_get_attribute("beamline",  TTASX2(1), "strength"));
	    placet_element_set_attribute("beamline",  TTASX3, "strength",placet_element_get_attribute("beamline",  TTASX3(1), "strength"));


	    placet_element_get_attribute("beamline",Kvar  , "strength");

	    placet_set_beam("beam0", B0);
	    [E,Bout0]=placet_test_no_correction('beamline', "beam0", "None",1,0,dlejection);
	    [beta_x0, beta_y0, alpha_x0, alpha_y0, emitt_x0, emitt_y0] = placet_get_twiss_parameters(Bout0);
		      emittXDFS = E(end,2);
		      emittYDFS = E(end,6);
		if (isnan(emitt_x0))
		    ALLEMIT(1,:)=[LOCs(1), 1e10, 1e10, 1e10, 1e10, 1e10, 1e10, 1e10, 1e10,1e10, 1e10,1e10];
		else
		   [p,s] = polyfit (Bout0(:,1), Bout0(:,4), 1);
		    serror=sqrt(diag (s.R)/s.df)*s.normr;
		    ALLEMIT(1,:)=[LOCs(1),  emitt_x0,emitt_y0,emittXDFS, emittYDFS,std(Bout0(:,2)),std(Bout0(:,3)),std(Bout0(:,4)),serror(1),serror(2),0 , 0];
		end
            placet_set_beam("beam00", Bout0);
	    [E,Bout1]=placet_test_no_correction('beamline', "beam00", "None",1,tl1injection,tl1ejection);
	    [beta_x1, beta_y1, alpha_x1, alpha_y1, emitt_x1, emitt_y1] = placet_get_twiss_parameters(Bout1);
	      emittXDFS = E(end,2);
	      emittYDFS = E(end,6);
		if (isnan(emitt_x1))
		    ALLEMIT(2,:)=[LOCs(2), 1e10, 1e10, 1e10, 1e10, 1e10, 1e10, 1e10, 1e10,1e10, 1e10,1e10];
		else
		    [p,s] = polyfit (Bout1(:,1), Bout1(:,4), 1);
		    serror=sqrt(diag (s.R)/s.df)*s.normr;
		    ALLEMIT(2,:)=[LOCs(2), emitt_x1,emitt_y1,emittXDFS, emittYDFS,std(Bout1(:,2)),std(Bout1(:,3)),std(Bout1(:,4)),serror(1),serror(2),0 , 0];
		end
		x1 = 19590.114478093  - mean(Bout1(:,2)) ; 
		x2 = 23.8506459472122 - mean(Bout1(:,5));
	        Bout1(:,2) +=  x1 ;
		Bout1(:,5) +=  x2 ;
            placet_set_beam("beam1", Bout1);
	    [E,Bout2]=placet_test_no_correction('beamline', "beam1", "None",1,tl1ejection,cr1ejection(1));
 	    [beta_x2, beta_y2, alpha_x2, alpha_y2, emitt_x2, emitt_y2] = placet_get_twiss_parameters(Bout2);
	      emittXDFS = E(end,2);
	      emittYDFS = E(end,6);
		if (isnan(emitt_x2))
		    ALLEMIT(3,:)=[LOCs(3), 1e10, 1e10, 1e10, 1e10, 1e10, 1e10, 1e10, 1e10,1e10, 1e10,1e10];
		else
		    [p,s] = polyfit (Bout2(:,1), Bout2(:,4), 1);
		    serror=sqrt(diag (s.R)/s.df)*s.normr;
		    ALLEMIT(3,:)=[LOCs(3), emitt_x2,emitt_y2,emittXDFS, emittYDFS,std(Bout2(:,2)),std(Bout2(:,3)),std(Bout2(:,4)),serror(1),serror(2),0 , 0];
		end
            placet_set_beam("beam2", Bout2);
	    [E,Bout3]=placet_test_no_correction('beamline', "beam2", "None",1,cr1ejection(1),cr1ejection(2));
 	    [beta_x3, beta_y3, alpha_x3, alpha_y3, emitt_x3, emitt_y3] = placet_get_twiss_parameters(Bout3);
	      emittXDFS = E(end,2);
	      emittYDFS = E(end,6);
		if (isnan(emitt_x3))
		    ALLEMIT(4,:)=[LOCs(4), 1e10, 1e10, 1e10, 1e10, 1e10, 1e10, 1e10, 1e10,1e10, 1e10,1e10];
		else
		    [p,s] = polyfit (Bout3(:,1), Bout3(:,4), 1);
		    serror=sqrt(diag (s.R)/s.df)*s.normr;
		    ALLEMIT(4,:)=[LOCs(4), emitt_x3,emitt_y3,emittXDFS, emittYDFS,std(Bout3(:,2)),std(Bout3(:,3)),std(Bout3(:,4)),serror(1),serror(2),0 , 0];
		end
            placet_set_beam("beam3", Bout3);
	    [E,Bout4]=placet_test_no_correction('beamline', "beam3", "None",1,cr1ejection(2),cr1ejection(3));
 	    [beta_x4, beta_y4, alpha_x4, alpha_y4, emitt_x4, emitt_y4] = placet_get_twiss_parameters(Bout4);
	      emittXDFS = E(end,2);
	      emittYDFS = E(end,6);
		if (isnan(emitt_x4))
		    ALLEMIT(5,:)=[LOCs(5), 1e10, 1e10, 1e10, 1e10, 1e10, 1e10, 1e10, 1e10,1e10, 1e10,1e10];
		else
		    [p,s] = polyfit (Bout4(:,1), Bout4(:,4), 1);
		    serror=sqrt(diag (s.R)/s.df)*s.normr;
		    ALLEMIT(5,:)=[LOCs(5), emitt_x4,emitt_y4,emittXDFS, emittYDFS,std(Bout4(:,2)),std(Bout4(:,3)),std(Bout4(:,4)),serror(1),serror(2),0 , 0];
		end
		x1 = -mean(Bout4(:,2)) ;
		x2 = -mean(Bout4(:,5));
		   Bout4(:,2) +=  x1;
		   Bout4(:,5) +=  x2;

           placet_set_beam("beam4", Bout4);
	   [E,Bout5] = placet_test_no_correction("beamline", "beam4", "None",1,tl2injection,tl2ejection);
 	   [beta_x5, beta_y5, alpha_x5, alpha_y5, emitt_x5, emitt_y5] = placet_get_twiss_parameters(Bout5);
	    save -text BeamTL2out.dat Bout0
	      emittXDFS = E(end,2);
	      emittYDFS = E(end,6);
		if (isnan(emitt_x5))
		    ALLEMIT(6,:)=[LOCs(6), 1e10, 1e10, 1e10, 1e10, 1e10, 1e10, 1e10, 1e10,1e10, 1e10,1e10];
		else
		    [p,s] = polyfit (Bout5(:,1), Bout5(:,4), 1);
		    serror=sqrt(diag (s.R)/s.df)*s.normr;
		    ALLEMIT(6,:)=[LOCs(6), emitt_x5,emitt_y5,emittXDFS, emittYDFS,std(Bout5(:,2)),std(Bout5(:,3)),std(Bout5(:,4)),serror(1),serror(2),0 , 0];
		end
	   xoff=  -23660.4597994522 - mean(Bout5(:,2)) ;
	   pxoff= 2483.63522116367  - mean(Bout5(:,5)) ;
	   Bout5(:,2) +=  xoff;
	   Bout5(:,5) +=  pxoff;
	   placet_set_beam("beam5", Bout5);
	   [E,Bout6]=placet_test_no_correction('beamline', "beam5", "None",1,tl2ejection,cr2ejection(1));
 	   [beta_x6, beta_y6, alpha_x6, alpha_y6, emitt_x6, emitt_y6] = placet_get_twiss_parameters(Bout6)
	      emittXDFS = E(end,2);
	      emittYDFS = E(end,6);
		if (isnan(emitt_x6))
		    ALLEMIT(7,:)=[LOCs(7), 1e10, 1e10, 1e10, 1e10, 1e10, 1e10, 1e10, 1e10,1e10, 1e10,1e10];
		else
		    [p,s] = polyfit (Bout6(:,1), Bout6(:,4), 1);
		    ALLEMIT(7,:)=[LOCs(7), emitt_x6,emitt_y6,emittXDFS, emittYDFS,std(Bout6(:,2)),std(Bout6(:,3)),std(Bout6(:,4)),serror(1),serror(2),0 , 0];
		end
	   placet_set_beam("beam6", Bout6);
	   [E,Bout7]=placet_test_no_correction('beamline', "beam6", "None",1,cr2ejection(1),cr2ejection(2));
 	   [beta_x7, beta_y7, alpha_x7, alpha_y7, emitt_x7, emitt_y7] = placet_get_twiss_parameters(Bout7)
	      emittXDFS = E(end,2);
	      emittYDFS = E(end,6);
		if (isnan(emitt_x7))
		    ALLEMIT(8,:)=[LOCs(8), 1e10, 1e10, 1e10, 1e10, 1e10, 1e10, 1e10, 1e10,1e10, 1e10,1e10];
		else
		    [p,s] = polyfit (Bout7(:,1), Bout7(:,4), 1);
		    serror=sqrt(diag (s.R)/s.df)*s.normr;
		    ALLEMIT(8,:)=[LOCs(8), emitt_x7,emitt_y7,emittXDFS, emittYDFS,std(Bout7(:,2)),std(Bout7(:,3)),std(Bout7(:,4)),serror(1),serror(2),0 , 0];
		end
	   placet_set_beam("beam7", Bout7);
	   [E,Bout8]=placet_test_no_correction('beamline', "beam7", "None",1,cr2ejection(2),cr2ejection(3));
 	   [beta_x8, beta_y8, alpha_x8, alpha_y8, emitt_x8, emitt_y8] = placet_get_twiss_parameters(Bout8)
	      emittXDFS = E(end,2);
	      emittYDFS = E(end,6);
		if (isnan(emitt_x8))
		    ALLEMIT(9,:)=[LOCs(9), 1e10, 1e10, 1e10, 1e10, 1e10, 1e10, 1e10, 1e10,1e10, 1e10,1e10];
		else
		    [p,s] = polyfit (Bout8(:,1), Bout8(:,4), 1);
		    serror=sqrt(diag (s.R)/s.df)*s.normr;
		    ALLEMIT(9,:)=[LOCs(9), emitt_x8,emitt_y8,emittXDFS, emittYDFS,std(Bout8(:,2)),std(Bout8(:,3)),std(Bout8(:,4)),serror(1),serror(2),0 , 0];
		end
	   placet_set_beam("beam8", Bout8);
	   [E,Bout9]=placet_test_no_correction('beamline', "beam8", "None",1,cr2ejection(3),cr2ejection(4));
 	   [beta_x9, beta_y9, alpha_x9, alpha_y9, emitt_x9, emitt_y9] = placet_get_twiss_parameters(Bout9)
	      emittXDFS = E(end,2);
	      emittYDFS = E(end,6);
		if (isnan(emitt_x9))
		    ALLEMIT(10,:)=[LOCs(10), 1e10, 1e10, 1e10, 1e10, 1e10, 1e10, 1e10, 1e10,1e10, 1e10,1e10];
		else
		   [p,s] = polyfit (Bout9(:,1), Bout9(:,4), 1);
		    serror=sqrt(diag (s.R)/s.df)*s.normr;
		    ALLEMIT(10,:)=[LOCs(10), emitt_x9,emitt_y9,emittXDFS, emittYDFS,std(Bout9(:,2)),std(Bout9(:,3)),std(Bout9(:,4)),serror(1),serror(2),0 , 0];
		end
		x1 =    -mean(Bout9(:,2)) + 1.61808817418445e+00 ;
		x2 =  	-mean(Bout9(:,5)) + 3.78344826551530e+00 ;
		   Bout9(:,2) +=  x1;
		   Bout9(:,5) +=  x2;
%{
	   placet_set_beam("beam9", Bout9);
	   [E,Bout10]=placet_test_no_correction('beamline', "beam9", "None",1,cr2ejection(4),tl3ejection);
 	   [beta_x10, beta_y10, alpha_x10, alpha_y10, emitt_x10, emitt_y10] = placet_get_twiss_parameters(Bout10);
	      emittXDFS = E(end,2)
	      emittYDFS = E(end,6)
		if (isnan(emitt_x10))
		    ALLEMIT(11,:)=[LOCs(11), 1e10, 1e10, 1e10, 1e10, 1e10, 1e10, 1e10, 1e10,1e10, 1e10,1e10];
		else
		   [p,s] = polyfit (Bout10(:,1), Bout10(:,4), 1);
                   serror=sqrt(diag (s.R)/s.df)*s.normr;
		    ALLEMIT(11,:)=[LOCs(11), emitt_x10,emitt_y10,emittXDFS, emittYDFS,std(Bout10(:,2)),std(Bout10(:,3)),std(Bout10(:,4)),serror(1),serror(2),0,0];
		end
	   placet_set_beam("beam10", Bout10);
	   [E,Bout11]=placet_test_no_correction('beamline', "beam10", "None",1,tl3ejection,ttaejection);
 	   [beta_x, beta_y, alpha_x, alpha_y, emitt_x, emitt_y] = placet_get_twiss_parameters(Bout11)
	      emittXDFS = E(end,2)
	      emittYDFS = E(end,6)
		if (isnan(emitt_x))
		    ALLEMIT(12,:)=[LOCs(12), 1e10, 1e10, 1e10, 1e10, 1e10, 1e10, 1e10, 1e10,1e10, 1e10,1e10];
		else
		   [p,s] = polyfit (Bout11(:,1), Bout11(:,4), 1);
		    serror=sqrt(diag (s.R)/s.df)*s.normr;
		    ALLEMIT(12,:)=[LOCs(12), emitt_x,emitt_y,emittXDFS, emittYDFS,std(Bout11(:,2)),std(Bout11(:,3)),std(Bout11(:,4)),serror(1),serror(2),0,0];
		end
	   placet_set_beam("beam11", Bout11);
	   [E,Bout12]=placet_test_no_correction('beamline', "beam11", "None",1,ttaejection,chicanend);
 	   [beta_x, beta_y, alpha_x, alpha_y, emitt_x, emitt_y] = placet_get_twiss_parameters(Bout12)
	      emittXDFS = E(end,2)
	      emittYDFS = E(end,6)
		   [p,s] = polyfit (Bout12(:,1), Bout12(:,4), 1);
		    serror=sqrt(diag (s.R)/s.df)*s.normr;
	    ALLEMIT(13,:)=[LOCs(13), emitt_x,emitt_y,emittXDFS, emittYDFS,std(Bout12(:,2)),std(Bout12(:,3)),std(Bout12(:,4)),serror(1),serror(2),0,0];

%}
	range=[3:5];
	optex=sum(ALLEMIT(range,2))
	optey=sum(ALLEMIT(range,3))
	optcorr1=sum(ALLEMIT(range,10))
	optcorr2=sum(ALLEMIT(range,10))
		if (isnan(optex))
			optex=1e10;	
			optey=1e10;	
			optcorr1=1e10;
			optcorr2=1e10;
		else
			optex=optex;	
			optey=optey;
			optcorr1=optcorr1;	
			optcorr2=optcorr2;
		endif
    save -text ${basename}_emittOPt.dat ALLEMIT 
kvalues=[placet_element_get_attribute("beamline",Kvar  , "strength")];
knames=[placet_element_get_attribute("beamline",Kvar  , "name")];

  save -text ${basename}_OPtValues.dat	knames kvalues
	sumE=real(optex+optey+optcorr2);
	if isnan(sumE)
		E=1e10;
	else
		E=real(optex+optey+optcorr2*1e0)
	endif
    end
##################  END OPTIMAZION FUNCTION ########################

    save -text BeamDGin.dat B0
    sizeX = std(B0(:,2))    
    sizeY = std(B0(:,3))        
    sizeZ = std(B0(:,4))
	    [E,Bout0]=placet_test_no_correction('beamline', "beam0", "None",1,0,dlinjection);
            placet_set_beam("beam000", Bout0);
    save -text BeamDLIN.dat Bout0
	    [E,Bout0]=placet_test_no_correction('beamline', "beam000", "None",1,dlinjection,dlejection);
    sizeX = std(Bout0(:,2))    
    sizeY = std(Bout0(:,3))        
    sizeZ = std(Bout0(:,4))

	    [beta_x0, beta_y0, alpha_x0, alpha_y0, emitt_x0, emitt_y0] = placet_get_twiss_parameters(Bout0)
	      emittXDFS = E(end,2)
	      emittYDFS = E(end,6)
	   [p,s] = polyfit (Bout0(:,1), Bout0(:,4), 1);
	    serror=sqrt(diag (s.R)/s.df)*s.normr;
	    ALLEMIT(1,:)=[LOCs(1), emitt_x0,emitt_y0,emittXDFS, emittYDFS,std(Bout0(:,2)),std(Bout0(:,3)),std(Bout0(:,4)),serror(1),serror(2),0 , 0];
            placet_set_beam("beam00", Bout0);
	    [E,Bout1]=placet_test_no_correction('beamline', "beam00", "None",1,tl1injection,tl1ejection);
	    [beta_x1, beta_y1, alpha_x1, alpha_y1, emitt_x1, emitt_y1] = placet_get_twiss_parameters(Bout1)
	      emittXDFS = E(end,2)
	      emittYDFS = E(end,6)
		   [p,s] = polyfit (Bout1(:,1), Bout1(:,4), 1);
		    serror=sqrt(diag (s.R)/s.df)*s.normr;
	    ALLEMIT(2,:)=[LOCs(2), emitt_x1,emitt_y1,emittXDFS, emittYDFS,std(Bout1(:,2)),std(Bout1(:,3)),std(Bout1(:,4)),serror(1),serror(2),0 , 0];
# Offsets for DL Injection
		x1 = 19590.114478093  - mean(Bout1(:,2)) ; 
		x2 = 23.8506459472122 - mean(Bout1(:,5));
	        Bout1(:,2) +=  x1 ;
		Bout1(:,5) +=  x2 ;
            placet_set_beam("beam1", Bout1);
	    [E,Bout2]=placet_test_no_correction('beamline', "beam1", "None",1,tl1ejection,cr1ejection(1));
 	    [beta_x2, beta_y2, alpha_x2, alpha_y2, emitt_x2, emitt_y2] = placet_get_twiss_parameters(Bout2)
	      emittXDFS = E(end,2)
	      emittYDFS = E(end,6)
	   [p,s] = polyfit (Bout2(:,1), Bout2(:,4), 1);
	    serror=sqrt(diag (s.R)/s.df)*s.normr;
	    ALLEMIT(3,:)=[LOCs(3), emitt_x2,emitt_y2,emittXDFS, emittYDFS,std(Bout2(:,2)),std(Bout2(:,3)),std(Bout2(:,4)),serror(1),serror(2),0 , 0];
            placet_set_beam("beam2", Bout2);
	    [E,Bout3]=placet_test_no_correction('beamline', "beam2", "None",1,cr1ejection(1),cr1ejection(2));
 	    [beta_x3, beta_y3, alpha_x3, alpha_y3, emitt_x3, emitt_y3] = placet_get_twiss_parameters(Bout3)
	      emittXDFS = E(end,2)
	      emittYDFS = E(end,6)
	  [p,s] = polyfit (Bout3(:,1), Bout3(:,4), 1);
	    serror=sqrt(diag (s.R)/s.df)*s.normr;
	    ALLEMIT(4,:)=[LOCs(4), emitt_x3,emitt_y3,emittXDFS, emittYDFS,std(Bout3(:,2)),std(Bout3(:,3)),std(Bout3(:,4)),serror(1),serror(2),0 , 0];
            placet_set_beam("beam3", Bout3);
	    [E,Bout4]=placet_test_no_correction('beamline', "beam3", "None",1,cr1ejection(2),cr1ejection(3));
 	    [beta_x4, beta_y4, alpha_x4, alpha_y4, emitt_x4, emitt_y4] = placet_get_twiss_parameters(Bout4)
	      emittXDFS = E(end,2)
	      emittYDFS = E(end,6)
	   [p,s] = polyfit (Bout4(:,1), Bout4(:,4), 1);
	    serror=sqrt(diag (s.R)/s.df)*s.normr;
	    ALLEMIT(5,:)=[LOCs(5), emitt_x4,emitt_y4,emittXDFS, emittYDFS,std(Bout4(:,2)),std(Bout4(:,3)),std(Bout4(:,4)),serror(1),serror(2),0 , 0];
# Offsets for DL Injection
		x1 = -mean(Bout4(:,2)) ;
		x2 = -mean(Bout4(:,5)) ;
	   Bout4(:,2) +=  x1;
	   Bout4(:,5) +=  x2;

            placet_set_beam("beam4", Bout4);
	   [E,Bout5] = placet_test_no_correction("beamline", "beam4", "None",1,tl2injection,tl2ejection);
 	   [beta_x5, beta_y5, alpha_x5, alpha_y5, emitt_x5, emitt_y5] = placet_get_twiss_parameters(Bout5)
    save -text BeamTL2out.dat Bout0
	      emittXDFS = E(end,2)
	      emittYDFS = E(end,6)
	   [p,s] = polyfit (Bout5(:,1), Bout5(:,4), 1);
	    serror=sqrt(diag (s.R)/s.df)*s.normr;
	    ALLEMIT(6,:)=[LOCs(6), emitt_x5,emitt_y5,emittXDFS, emittYDFS,std(Bout5(:,2)),std(Bout5(:,3)),std(Bout5(:,4)),serror(1),serror(2),0 , 0];
# Offsets for DL Injection
	   x1=  -23660.4597994522 - mean(Bout5(:,2)) ;
	   x2= 2483.63522116367  - mean(Bout5(:,5)) ;
	   Bout5(:,2) +=  x1;
	   Bout5(:,5) +=  x2;
	   placet_set_beam("beam5", Bout5);
	   [E,Bout6]=placet_test_no_correction('beamline', "beam5", "None",1,tl2ejection,cr2ejection(1));
 	   [beta_x6, beta_y6, alpha_x6, alpha_y6, emitt_x6, emitt_y6] = placet_get_twiss_parameters(Bout6)
	      emittXDFS = E(end,2)
	      emittYDFS = E(end,6)
	   [p,s] = polyfit (Bout6(:,1), Bout6(:,4), 1);
	    serror=sqrt(diag (s.R)/s.df)*s.normr;
	    ALLEMIT(7,:)=[LOCs(7), emitt_x6,emitt_y6,emittXDFS, emittYDFS,std(Bout6(:,2)),std(Bout6(:,3)),std(Bout6(:,4)),serror(1),serror(2),0 , 0];
	   placet_set_beam("beam6", Bout6);
	   [E,Bout7]=placet_test_no_correction('beamline', "beam6", "None",1,cr2ejection(1),cr2ejection(2));
 	   [beta_x7, beta_y7, alpha_x7, alpha_y7, emitt_x7, emitt_y7] = placet_get_twiss_parameters(Bout7)
	      emittXDFS = E(end,2)
	      emittYDFS = E(end,6)
	   [p,s] = polyfit (Bout7(:,1), Bout7(:,4), 1);
	    serror=sqrt(diag (s.R)/s.df)*s.normr;
	    ALLEMIT(8,:)=[LOCs(8), emitt_x7,emitt_y7,emittXDFS, emittYDFS,std(Bout7(:,2)),std(Bout7(:,3)),std(Bout7(:,4)),serror(1),serror(2),0 , 0];
	   placet_set_beam("beam7", Bout7);
	   [E,Bout8]=placet_test_no_correction('beamline', "beam7", "None",1,cr2ejection(2),cr2ejection(3));
 	   [beta_x8, beta_y8, alpha_x8, alpha_y8, emitt_x8, emitt_y8] = placet_get_twiss_parameters(Bout8)
	      emittXDFS = E(end,2)
	      emittYDFS = E(end,6)
	   [p,s] = polyfit (Bout8(:,1), Bout8(:,4), 1);
	    serror=sqrt(diag (s.R)/s.df)*s.normr;
	    ALLEMIT(9,:)=[LOCs(9), emitt_x8,emitt_y8,emittXDFS, emittYDFS,std(Bout8(:,2)),std(Bout8(:,3)),std(Bout8(:,4)),serror(1),serror(2),0 , 0];
	   placet_set_beam("beam8", Bout8);
	   [E,Bout9]=placet_test_no_correction('beamline', "beam8", "None",1,cr2ejection(3),cr2ejection(4));
 	   [beta_x9, beta_y9, alpha_x9, alpha_y9, emitt_x9, emitt_y9] = placet_get_twiss_parameters(Bout9)
	      emittXDFS = E(end,2)
	      emittYDFS = E(end,6)
		   [p,s] = polyfit (Bout9(:,1), Bout9(:,4), 1);
		    serror=sqrt(diag (s.R)/s.df)*s.normr;
	    ALLEMIT(10,:)=[LOCs(10), emitt_x9,emitt_y9,emittXDFS, emittYDFS,std(Bout9(:,2)),std(Bout9(:,3)),std(Bout9(:,4)),serror(1),serror(2),0 , 0];
    save -text CR2_DLbeamOut.dat Bout9
		x1 =    0.023778356069e6 + 1.61808817418445e+00 ;
		x2 =    0.002478265968e6 + 3.78344826551530e+00 ;

	   Bout9(:,2) +=  x1;
	   Bout9(:,5) +=  x2;
	   placet_set_beam("beam9", Bout9);
	   [E,Bout10]=placet_test_no_correction('beamline', "beam9", "None",1,cr2ejection(4),tl3ejection);
 	   [beta_x10, beta_y10, alpha_x10, alpha_y10, emitt_x10, emitt_y10] = placet_get_twiss_parameters(Bout10)
	      emittXDFS = E(end,2)
	      emittYDFS = E(end,6)
	   [p,s] = polyfit (Bout10(:,1), Bout10(:,4), 1);
	    serror=sqrt(diag (s.R)/s.df)*s.normr;
	    ALLEMIT(11,:)=[LOCs(11), emitt_x10,emitt_y10,emittXDFS, emittYDFS,std(Bout10(:,2)),std(Bout10(:,3)),std(Bout10(:,4)),serror(1),serror(2),0 , 0];
	   placet_set_beam("beam10", Bout10);
	   [E,Bout11]=placet_test_no_correction('beamline', "beam10", "None",1,tl3ejection,ttaejection);
 	   [beta_x, beta_y, alpha_x, alpha_y, emitt_x, emitt_y] = placet_get_twiss_parameters(Bout11)
	      emittXDFS = E(end,2)
	      emittYDFS = E(end,6)
		   [p,s] = polyfit (Bout11(:,1), Bout11(:,4), 1);
		    serror=sqrt(diag (s.R)/s.df)*s.normr;
	    ALLEMIT(12,:)=[LOCs(12), emitt_x,emitt_y,emittXDFS, emittYDFS,std(Bout11(:,2)),std(Bout11(:,3)),std(Bout11(:,4)),serror(1),serror(2),0 , 0];
	   placet_set_beam("beam11", Bout11);
	   [E,Bout12]=placet_test_no_correction('beamline', "beam11", "None",1,ttaejection,chicanend);
 	   [beta_x, beta_y, alpha_x, alpha_y, emitt_x, emitt_y] = placet_get_twiss_parameters(Bout12)
	      emittXDFS = E(end,2)
	      emittYDFS = E(end,6)
		   [p,s] = polyfit (Bout12(:,1), Bout12(:,4), 1);
		    serror=sqrt(diag (s.R)/s.df)*s.normr;
	    ALLEMIT(13,:)=[LOCs(13), emitt_x,emitt_y,emittXDFS, emittYDFS,std(Bout12(:,2)),std(Bout12(:,3)),std(Bout12(:,4)),serror(1),serror(2),0 , 0];

    ALLEMIT=real(ALLEMIT)
    save -text ${basename}_emittOPt_DG2TTA.dat ALLEMIT
    [R,S]=placet_get_bpm_readings("beamline");
    T = [ S' R ];
    save -text ${basename}_bpm3.dat T

#optimum=placet_optimize("beamline", "final_emittance", Kvar,						["strength";"strength";"strength";"strength";"strength";"strength";"strength";"strength";"strength";"strength";"strength";"strength";"strength";"strength";"strength";"strength";"strength";"strength";"strength";"strength";"strength";"strength";"strength";"strength";"strength";"strength";"strength";"strength";"strength";"strength";"strength";"strength";"strength";"strength";"strength";"strength";"strength";"strength";"strength";"strength";"strength";"strength";"strength";"strength";"strength";"strength";"strength";"strength";"strength";"strength";"strength";"strength";"strength";"strength";"strength";"strength"],KS)

# Save beam distribution at different locations
#    save -text ${basename}_beam0_v2.dat Bout
#    save -text DL_beam_v0.dat Bout0
    save -text TL1_beam_v0.dat Bout1
#    save -text CR11_beam_v0.dat Bout2
#    save -text CR12_beam_v0.dat Bout3
    save -text CR13_beam_v0.dat Bout4
#    save -text TL2_beam_v0.dat Bout5
#    save -text CR21_beam_v0.dat Bout6
#    save -text CR22_beam_v0.dat Bout7
#    save -text CR23_beam_v0.dat Bout8
    save -text CR24_beam_v0.dat Bout9
#    save -text TL3_beam_v0.dat Bout10
#    save -text TTA_beam_v0.dat Bout11
    save -text ChicaneBeamOut.dat Bout12

}
exit
if 0 {
Octave {
	global Kvar B0 tl1eje nele
##############  Disp measurement  ##############

	B1 = B0;
	B2 = B0;
	B1(:,1) +=$deltae;
	B2(:,1) -=$deltae;
	placet_set_beam("beam0", B0);
	placet_set_beam("beam1", B1);
	placet_set_beam("beam2", B2);
    
	function Eta = MeasureDispersion(Beamline, Beam1, Beam2, delta,nele1,nele2)
		placet_test_no_correction(Beamline, Beam1, "None",1,nele1,nele2); Eta  = placet_get_bpm_readings(Beamline);
		placet_test_no_correction(Beamline, Beam2, "None",1,nele1,nele2); Eta -= placet_get_bpm_readings(Beamline);
		Eta /= 2 * delta;
	end

	EtaMeas = MeasureDispersion("beamline", "beam1", "beam2", $deltae,1,tl1eje);
	fout=fopen('MeasDispALL.txt','w');
       	for i=1:length(EtaMeas)
		fprintf(fout,"%f %f %f\n",S(i),EtaMeas(i,1)*1e-6,EtaMeas(i,2)*1e-6);
	endfor
	fclose(fout);
}
}
#exit
##################################################
##############  Orbit minimisation  ##############
Octave {
	global LOCs
	    ORBEMIT=zeros(4,5);
        function [M] = orbit_optim(XORB)
	      global B0 LOCs tl1injection tl1ejection cr1ejection cr2ejection tl2injection tl2ejection 
              XORB
	      placet_set_beam("beam0", B0);
	      [E,Bout1]=placet_test_no_correction('beamline', "beam0", "None",1,tl1injection,tl1ejection);
  	      [beta_x, beta_y, alpha_x, alpha_y, emitt_x, emitt_y] = placet_get_twiss_parameters(Bout1)
	      emittXDFS = E(end,2)
	      emittYDFS = E(end,6)
		if (isnan(emitt_x))
		    ORBEMIT(1,:)=[LOCs(2), 1e10, 1e10, 1e10, 1e10];
		else
		    ORBEMIT(1,:)=[LOCs(2), emitt_x,emitt_y,emittXDFS, emittYDFS];
		end
		x1 = 19590.114478093  - mean(Bout1(:,2)) ; 
		x2 = 23.8506459472122 - mean(Bout1(:,5));
	        Bout1(:,2) +=  x1 + 1.07112698119238e+02;
		Bout1(:,5) +=  x2 -3.82922467552688e+00;
	      placet_set_beam("beam1", Bout1);
	      [E,Bout4]=placet_test_no_correction('beamline', "beam1", "None",1,tl1ejection,cr1ejection(3));
 	    [beta_x4, beta_y4, alpha_x4, alpha_y4, emitt_x4, emitt_y4] = placet_get_twiss_parameters(Bout4)
	      emittXDFS = E(end,2)
	      emittYDFS = E(end,6)
		if (isnan(emitt_x4))
		    ORBEMIT(2,:)=[LOCs(5), 1e10, 1e10, 1e10, 1e10];
		else
		    ORBEMIT(2,:)=[LOCs(5), emitt_x4,emitt_y4,emittXDFS, emittYDFS];
		end
		x1 = -mean(Bout4(:,2)) +  2.22824323970429e+02;
		x2 = -mean(Bout4(:,5)) -2.89231228313200e+01 ;
	   Bout4(:,2) +=  x1;
	   Bout4(:,5) +=  x2;
            placet_set_beam("beam4", Bout4);
	   [E,Bout5] = placet_test_no_correction("beamline", "beam4", "None",1,tl2injection,tl2ejection);
 	   [beta_x5, beta_y5, alpha_x5, alpha_y5, emitt_x5, emitt_y5] = placet_get_twiss_parameters(Bout5)
	      emittXDFS = E(end,2)
	      emittYDFS = E(end,6)
		if (isnan(emitt_x5) || isinf(emitt_x5))
		    ORBEMIT(3,:)=[LOCs(6), 1e10, 1e10, 1e10, 1e10];
		else
		    ORBEMIT(3,:)=[LOCs(6), emitt_x5,emitt_y5,emittXDFS, emittYDFS];
		end
	   x1=  -23660.4597994522 - mean(Bout5(:,2)) + XORB(1) ;
	   x2= 2483.63522116367  - mean(Bout5(:,5)) +XORB(2) ;
	   Bout5(:,2) +=  x1;
	   Bout5(:,5) +=  x2;
	   placet_set_beam("beam5", Bout5);
	   [E,Bout6]=placet_test_no_correction('beamline', "beam5", "None",1,tl2ejection,cr2ejection(4));
 	   [beta_x6, beta_y6, alpha_x6, alpha_y6, emitt_x6, emitt_y6] = placet_get_twiss_parameters(Bout6)
	      emittXDFS = E(end,2)
	      emittYDFS = E(end,6)
		if (isnan(emitt_x6)  || isinf(emitt_x6))
		    ORBEMIT(4,:)=[LOCs(10), 1e10, 1e10, 1e10, 1e10];
		else
		    ORBEMIT(4,:)=[LOCs(10), emitt_x6,emitt_y6,emittXDFS, emittYDFS];
		end

		optex=sum(ORBEMIT([1:4],2))
		optey=sum(ORBEMIT([1:4],3))

#	      [Bpm ,S]= placet_get_bpm_readings("beamline");
#	      plot(S,Bpm(:,1));
#	      drawnow("expose");
#	      M1 = sum(Bpm(:,1).**2)
	      M = emitt_x6+emitt_y6;
#   	      M= optex+optey;
#	      M=M1*0+M2;
#	      Orbit = [ S' Bpm ];
#    	      save -text ${basename}_bpmOpt.dat Orbit


# 	       fout=fopen('readings','w');  
#		 for i=1:length(S)
#			fprintf(fout,"%f %f %f\n",S(i),Bpm(i,1),Bpm(i,2))
#		 endfor
#	       fclose(fout)

#	       EtaMeas = MeasureDispersion("beamline", "beam1", "beam2", $deltae);
#	       Tcl_Eval("TwissPlot -file twiss.dat -beam beam0");
#	       T = load('twiss.dat');
#	       fout=fopen('MeasDisp.txt','w');
#		 for i=1:length(EtaMeas)
#			fprintf(fout,"%f %f \n",InterD(1,i),-EtaMeas(i,1)*1e-6);
#		 endfor
#		fclose(fout);
#	       fout=fopen('T.txt ','w');  
#		 for i=1:length(T)
#			fprintf(fout,"%f %f \n",T(i,2),T(i,12))
#		 endfor
#      	       fclose(fout)
	
	    end

#    T0 = load('twisscr2disp_v2');
#    InterD=interp1(T0(:,1),T0(:,2),S);

#    Xbest = fminsearch("orbit_optim", [0  0 ])
      [M]=orbit_optim([0 0]);


#      EtaMeas = MeasureDispersion("beamline", "beam1", "beam2", $deltae);
#      fout=fopen('MeasDisp.txt','w');
#	 for i=1:length(EtaMeas)
#		fprintf(fout,"%f %f \n",InterD(1,i),-EtaMeas(i,1)*1e-6);
#	 endfor
 #     fclose(fout);


#    save -text ${basename}_beam1_v2.dat B
}





