Drift -name "DOGLEGEXTSTART" -length 0 
Bpm -name "BPMSTART" -length 0 
Sbend -name "BRB1A" -synrad $sbend_synrad -length 2.2 -angle -0.1335176878 -E1 -0.06675884389 -E2 0 -six_dim 1 -e0 $e0 -fint 0.5 -fintx -1 
set e0 [expr $e0-14.1e-6*-0.1335176878*-0.1335176878/2.2*$e0*$e0*$e0*$e0*$sbend_synrad]
SetReferenceEnergy $e0
Sbend -name "BRB1B" -synrad $sbend_synrad -length 2.2 -angle -0.1335176878 -E1 0 -E2 -0.06675884389 -six_dim 1 -e0 $e0 -fint 0.5 -fintx -1 
set e0 [expr $e0-14.1e-6*-0.1335176878*-0.1335176878/2.2*$e0*$e0*$e0*$e0*$sbend_synrad]
SetReferenceEnergy $e0
Drift -name "DG.L1" -length 2 
Multipole -name "DG.SEXT1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -0.771125*$e0] -e0 $e0 
Drift -name "DG.L11" -length 0.9 
Quadrupole -name "QFDOG" -synrad $quad_synrad -length 0.1 -strength [expr 0.141447*$e0] -e0 $e0 
Bpm -name "BPMQ" -length 0 
Quadrupole -name "QFDOG" -synrad $quad_synrad -length 0.1 -strength [expr 0.141447*$e0] -e0 $e0 
Drift -name "DG.L2" -length 5 
Quadrupole -name "QDDOG" -synrad $quad_synrad -length 0.1 -strength [expr -0.107512*$e0] -e0 $e0 
Bpm -name "BPMQ" -length 0 
Quadrupole -name "QDDOG" -synrad $quad_synrad -length 0.1 -strength [expr -0.107512*$e0] -e0 $e0 
Drift -name "DG.L3" -length 5 
Quadrupole -name "QFDOG2" -synrad $quad_synrad -length 0.1 -strength [expr 0.0795841*$e0] -e0 $e0 
Bpm -name "BPMQ" -length 0 
Quadrupole -name "QFDOG2" -synrad $quad_synrad -length 0.1 -strength [expr 0.0795841*$e0] -e0 $e0 
Drift -name "DG.L4" -length 8 
Bpm -name "BPMC" -length 0 
Drift -name "DG.L4" -length 8 
Quadrupole -name "QFDOG2" -synrad $quad_synrad -length 0.1 -strength [expr 0.0795841*$e0] -e0 $e0 
Bpm -name "BPMQ" -length 0 
Quadrupole -name "QFDOG2" -synrad $quad_synrad -length 0.1 -strength [expr 0.0795841*$e0] -e0 $e0 
Drift -name "DG.L3" -length 5 
Quadrupole -name "QDDOG" -synrad $quad_synrad -length 0.1 -strength [expr -0.107512*$e0] -e0 $e0 
Bpm -name "BPMQ" -length 0 
Quadrupole -name "QDDOG" -synrad $quad_synrad -length 0.1 -strength [expr -0.107512*$e0] -e0 $e0 
Drift -name "DG.L2" -length 5 
Quadrupole -name "QFDOG" -synrad $quad_synrad -length 0.1 -strength [expr 0.141447*$e0] -e0 $e0 
Bpm -name "BPMQ" -length 0 
Quadrupole -name "QFDOG" -synrad $quad_synrad -length 0.1 -strength [expr 0.141447*$e0] -e0 $e0 
Drift -name "DG.L11" -length 0.9 
Multipole -name "DG.SEXT2" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0.771125*$e0] -e0 $e0 
Drift -name "DG.L1" -length 2 
Sbend -name "BRB2A" -synrad $sbend_synrad -length 2.2 -angle 0.1335176878 -E1 0.06675884389 -E2 0 -six_dim 1 -e0 $e0 -fint 0.5 -fintx -1 
set e0 [expr $e0-14.1e-6*0.1335176878*0.1335176878/2.2*$e0*$e0*$e0*$e0*$sbend_synrad]
SetReferenceEnergy $e0
Sbend -name "BRB2B" -synrad $sbend_synrad -length 2.2 -angle 0.1335176878 -E1 0 -E2 0.06675884389 -six_dim 1 -e0 $e0 -fint 0.5 -fintx -1 
set e0 [expr $e0-14.1e-6*0.1335176878*0.1335176878/2.2*$e0*$e0*$e0*$e0*$sbend_synrad]
SetReferenceEnergy $e0
Bpm -name "BPMEND" -length 0 
Drift -name "DGDL.ENTCELLONE" -length 0 
Drift -name "DGDL.D1" -length 1.03396146 
Quadrupole -name "DGDL.Q1" -synrad $quad_synrad -length 0.1 -strength [expr 0.1526684235*$e0] -e0 $e0 
Quadrupole -name "DGDL.Q1" -synrad $quad_synrad -length 0.1 -strength [expr 0.1526684235*$e0] -e0 $e0 
Drift -name "DGDL.D2" -length 0.99977365 
Quadrupole -name "DGDL.Q2" -synrad $quad_synrad -length 0.1 -strength [expr -0.1681903624*$e0] -e0 $e0 
Quadrupole -name "DGDL.Q2" -synrad $quad_synrad -length 0.1 -strength [expr -0.1681903624*$e0] -e0 $e0 
Drift -name "DGDL.D3" -length 5.26626489 
Quadrupole -name "DGDL.Q3" -synrad $quad_synrad -length 0.1 -strength [expr 0.2246326034*$e0] -e0 $e0 
Quadrupole -name "DGDL.Q3" -synrad $quad_synrad -length 0.1 -strength [expr 0.2246326034*$e0] -e0 $e0 
Drift -name "DGDL.D4" -length 1.01854763 
Quadrupole -name "DGDL.Q4" -synrad $quad_synrad -length 0.1 -strength [expr -0.1354193344*$e0] -e0 $e0 
Quadrupole -name "DGDL.Q4" -synrad $quad_synrad -length 0.1 -strength [expr -0.1354193344*$e0] -e0 $e0 
Drift -name "DGDL.D5" -length 1.39261465 
Drift -name "DGDL.EXICELLONE" -length 0 
Drift -name "DOGLEGEXTEND" -length 0 
Drift -name "DL.INJECTION" -length 0 
Bpm -name "BPM0" -length 0
Drift -name "DL.DLSTART" -length 0 

Drift -name "M0" -length 0 

Drift -name "RFDEFL" -length 0 

Drift -name "DRRF" -length 2 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 0.5000007127

Sbend -name "SEPTQ" -synrad $sbend_synrad -length 0.500001425441655 -angle -0.005849061169 -E1 -0.002924530584 -E2 -0.002924530584 -e0 $e0 -K [expr -0.5000007127*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*-0.005849061169*-0.005849061169/0.500001425441655*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "DL1IN" -length 1.1 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 1.00031738

Sbend -name "SEPT1" -synrad $sbend_synrad -length 1.00063486103645 -angle -0.0872664626 -E1 -0.0436332313 -E2 -0.0436332313 -e0 $e0 -K [expr 0.9956481989*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.0872664626*-0.0872664626/1.00063486103645*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "DL2IN" -length 0.3 

Quadrupole -name "QL1IN" -synrad $quad_synrad -length 0.4 -strength [expr -0.647036*$e0] -e0 $e0 

Bpm -name "BPM1" -length 0
Drift -name "DL3IN" -length 0.4 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.574109835

Sbend -name "SEPT2" -synrad $sbend_synrad -length 2.58202788155376 -angle -0.2714151852 -E1 -0.1357075926 -E2 -0.1357075926 -e0 $e0 -K [expr -1.249456954*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2714151852*-0.2714151852/2.58202788155376*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "MD3" -length 0 

Drift -name "D33" -length 0.192971 

Quadrupole -name "QL2IN" -synrad $quad_synrad -length 0.4 -strength [expr 1.14802*$e0] -e0 $e0 

Bpm -name "BPM2" -length 0
Drift -name "D32A" -length 0.11111 

Multipole -name "DL.SXL2" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -0.5985490004*$e0] -e0 $e0 

Drift -name "D32B" -length 0.43333 

Quadrupole -name "QL3IN" -synrad $quad_synrad -length 0.4 -strength [expr -1.943224*$e0] -e0 $e0 

Bpm -name "BPM3" -length 0
Drift -name "D33A" -length 0.0464855 

Multipole -name "DL.SXL3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -2.704344389*$e0] -e0 $e0 

Drift -name "D33B" -length 0.0464855 

Sbend -name "DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.04133674544 -E1 0.02066837272 -E2 0.02066837272 -six_dim 1 -e0 $e0 -K [expr 0.9511964998*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.04133674544*0.04133674544/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "MD2" -length 0 

Drift -name "DS3CA" -length 0.08511145 

Multipole -name "DL.SXL4" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.067129221*$e0] -e0 $e0 

Drift -name "DS3CB" -length 0.08511145 

Quadrupole -name "QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1666676*$e0] -e0 $e0 

Bpm -name "BPM4" -length 0
Quadrupole -name "QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1666676*$e0] -e0 $e0 

Bpm -name "BPM5" -length 0
Drift -name "DS3C_B" -length 0.1702229 

Multipole -name "DL.SNN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.046818406*$e0] -e0 $e0 

Drift -name "DS3C_A" -length 0.01 

Sbend -name "DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.04133674544 -E1 0.02066837272 -E2 0.02066837272 -six_dim 1 -e0 $e0 -K [expr 0.9511964998*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.04133674544*0.04133674544/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "DS2C_B" -length 0.01 

Multipole -name "DL.SNN2" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0.5879111252*$e0] -e0 $e0 

Drift -name "DS2C_A" -length 0.2846962 

Quadrupole -name "QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.209056*$e0] -e0 $e0 

Bpm -name "BPM6" -length 0
Drift -name "DS1C_B" -length 0.25 

Multipole -name "DL.SNN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0.8208368585*$e0] -e0 $e0 

Drift -name "DS1C_A" -length 0.1850809 

Sbend -name "DIPOLE0" -synrad $sbend_synrad -length 2.56621607 -angle -0.372030709 -E1 -0.1860153545 -E2 -0.1860153545 -six_dim 1 -e0 $e0 -K [expr 0.04695405543*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.372030709*-0.372030709/2.56621607*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "D12" -length 0.1559115 

Quadrupole -name "QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.824612*$e0] -e0 $e0 

Bpm -name "BPM7" -length 0
Drift -name "D11B" -length 0.0940885 

Multipole -name "DL.SNCH1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 14.58528081*$e0] -e0 $e0 

Drift -name "D11A" -length 0.1 

Quadrupole -name "QI1" -synrad $quad_synrad -length 0.2 -strength [expr -0.746512*$e0] -e0 $e0 

Bpm -name "BPM8" -length 0
Drift -name "MCELL" -length 0 

Quadrupole -name "QI1" -synrad $quad_synrad -length 0.2 -strength [expr -0.746512*$e0] -e0 $e0 

Bpm -name "BPM9" -length 0
Drift -name "D11A" -length 0.1 

Multipole -name "DL.SNCH1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 14.58528081*$e0] -e0 $e0 

Drift -name "D11B" -length 0.0940885 

Quadrupole -name "QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.824612*$e0] -e0 $e0 

Bpm -name "BPM10" -length 0
Drift -name "D12" -length 0.1559115 

Sbend -name "DIPOLE0" -synrad $sbend_synrad -length 2.56621607 -angle -0.372030709 -E1 -0.1860153545 -E2 -0.1860153545 -six_dim 1 -e0 $e0 -K [expr 0.04695405543*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.372030709*-0.372030709/2.56621607*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "DS1C_A" -length 0.1850809 

Multipole -name "DL.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0.8208368585*$e0] -e0 $e0 

Drift -name "DS1C_B" -length 0.25 

Quadrupole -name "QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.209056*$e0] -e0 $e0 

Bpm -name "BPM11" -length 0
Drift -name "DS2C_A" -length 0.2846962 

Multipole -name "DL.SN2" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0.5879111252*$e0] -e0 $e0 

Drift -name "DS2C_B" -length 0.01 

Sbend -name "DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.04133674544 -E1 0.02066837272 -E2 0.02066837272 -six_dim 1 -e0 $e0 -K [expr 0.9511964998*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.04133674544*0.04133674544/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "DS3C_A" -length 0.01 

Multipole -name "DL.SN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.046818406*$e0] -e0 $e0 

Drift -name "DS3C_B" -length 0.1702229 

Quadrupole -name "QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1666676*$e0] -e0 $e0 

Bpm -name "BPM12" -length 0
Quadrupole -name "QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1666676*$e0] -e0 $e0 

Bpm -name "BPM13" -length 0
Drift -name "DS3C_B" -length 0.1702229 

Multipole -name "DL.SNN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.046818406*$e0] -e0 $e0 

Drift -name "DS3C_A" -length 0.01 

Sbend -name "DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.04133674544 -E1 0.02066837272 -E2 0.02066837272 -six_dim 1 -e0 $e0 -K [expr 0.9511964998*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.04133674544*0.04133674544/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "DS2C_B" -length 0.01 

Multipole -name "DL.SNN2" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0.5879111252*$e0] -e0 $e0 

Drift -name "DS2C_A" -length 0.2846962 

Quadrupole -name "QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.209056*$e0] -e0 $e0 

Bpm -name "BPM14" -length 0
Drift -name "DS1C_B" -length 0.25 

Multipole -name "DL.SNN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0.8208368585*$e0] -e0 $e0 

Drift -name "DS1C_A" -length 0.1850809 

Sbend -name "DIPOLE0" -synrad $sbend_synrad -length 2.56621607 -angle -0.372030709 -E1 -0.1860153545 -E2 -0.1860153545 -six_dim 1 -e0 $e0 -K [expr 0.04695405543*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.372030709*-0.372030709/2.56621607*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "D12" -length 0.1559115 

Quadrupole -name "QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.824612*$e0] -e0 $e0 

Bpm -name "BPM15" -length 0
Drift -name "D11B" -length 0.0940885 

Multipole -name "DL.SNCH1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 14.58528081*$e0] -e0 $e0 

Drift -name "D11A" -length 0.1 

Quadrupole -name "QI1" -synrad $quad_synrad -length 0.2 -strength [expr -0.746512*$e0] -e0 $e0 

Bpm -name "BPM16" -length 0
Drift -name "MCELL" -length 0 

Drift -name "MCELL" -length 0 

Quadrupole -name "QI1" -synrad $quad_synrad -length 0.2 -strength [expr -0.746512*$e0] -e0 $e0 

Bpm -name "BPM17" -length 0
Drift -name "D11A" -length 0.1 

Multipole -name "DL.SNCH1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 14.58528081*$e0] -e0 $e0 

Drift -name "D11B" -length 0.0940885 

Quadrupole -name "QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.824612*$e0] -e0 $e0 

Bpm -name "BPM18" -length 0
Drift -name "D12" -length 0.1559115 

Sbend -name "DIPOLE0" -synrad $sbend_synrad -length 2.56621607 -angle -0.372030709 -E1 -0.1860153545 -E2 -0.1860153545 -six_dim 1 -e0 $e0 -K [expr 0.04695405543*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.372030709*-0.372030709/2.56621607*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "DS1C_A" -length 0.1850809 

Multipole -name "DL.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0.8208368585*$e0] -e0 $e0 

Drift -name "DS1C_B" -length 0.25 

Quadrupole -name "QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.209056*$e0] -e0 $e0 

Bpm -name "BPM19" -length 0
Drift -name "DS2C_A" -length 0.2846962 

Multipole -name "DL.SN2" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0.5879111252*$e0] -e0 $e0 

Drift -name "DS2C_B" -length 0.01 

Sbend -name "DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.04133674544 -E1 0.02066837272 -E2 0.02066837272 -six_dim 1 -e0 $e0 -K [expr 0.9511964998*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.04133674544*0.04133674544/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "DS3C_A" -length 0.01 

Multipole -name "DL.SN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.046818406*$e0] -e0 $e0 

Drift -name "DS3C_B" -length 0.1702229 

Quadrupole -name "QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1666676*$e0] -e0 $e0 

Bpm -name "BPM20" -length 0
Quadrupole -name "QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1666676*$e0] -e0 $e0 

Bpm -name "BPM21" -length 0
Drift -name "DS3C_B" -length 0.1702229 

Multipole -name "DL.SNN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.046818406*$e0] -e0 $e0 

Drift -name "DS3C_A" -length 0.01 

Sbend -name "DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.04133674544 -E1 0.02066837272 -E2 0.02066837272 -six_dim 1 -e0 $e0 -K [expr 0.9511964998*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.04133674544*0.04133674544/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "DS2C_B" -length 0.01 

Multipole -name "DL.SNN2" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0.5879111252*$e0] -e0 $e0 

Drift -name "DS2C_A" -length 0.2846962 

Quadrupole -name "QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.209056*$e0] -e0 $e0 

Bpm -name "BPM22" -length 0
Drift -name "DS1C_B" -length 0.25 

Multipole -name "DL.SNN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0.8208368585*$e0] -e0 $e0 

Drift -name "DS1C_A" -length 0.1850809 

Sbend -name "DIPOLE0" -synrad $sbend_synrad -length 2.56621607 -angle -0.372030709 -E1 -0.1860153545 -E2 -0.1860153545 -six_dim 1 -e0 $e0 -K [expr 0.04695405543*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.372030709*-0.372030709/2.56621607*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "D12" -length 0.1559115 

Quadrupole -name "QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.824612*$e0] -e0 $e0 

Bpm -name "BPM23" -length 0
Drift -name "D11B" -length 0.0940885 

Multipole -name "DL.SNCH1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 14.58528081*$e0] -e0 $e0 

Drift -name "D11A" -length 0.1 

Quadrupole -name "QI1" -synrad $quad_synrad -length 0.2 -strength [expr -0.746512*$e0] -e0 $e0 

Bpm -name "BPM24" -length 0
Drift -name "MCELL" -length 0 

Drift -name "MCELL" -length 0 

Quadrupole -name "QI1" -synrad $quad_synrad -length 0.2 -strength [expr -0.746512*$e0] -e0 $e0 

Bpm -name "BPM25" -length 0
Drift -name "D11A" -length 0.1 

Multipole -name "DL.SXCH1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -14.58528081*$e0] -e0 $e0 

Drift -name "D11B" -length 0.0940885 

Quadrupole -name "QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.824612*$e0] -e0 $e0 

Bpm -name "BPM26" -length 0
Drift -name "D12" -length 0.1559115 

Sbend -name "DIPOLE" -synrad $sbend_synrad -length 2.56621607 -angle 0.372030709 -E1 0.1860153545 -E2 0.1860153545 -six_dim 1 -e0 $e0 -K [expr 0.04695405543*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*0.372030709*0.372030709/2.56621607*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "DS1C_A" -length 0.1850809 

Multipole -name "DL.SX1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -0.8208368585*$e0] -e0 $e0 

Drift -name "DS1C_B" -length 0.25 

Quadrupole -name "QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.209056*$e0] -e0 $e0 

Bpm -name "BPM27" -length 0
Drift -name "DS2C_A" -length 0.2846962 

Multipole -name "DL.SX2" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -0.5879111252*$e0] -e0 $e0 

Drift -name "DS2C_B" -length 0.01 

Sbend -name "DIPN" -synrad $sbend_synrad -length 0.2851351189 -angle -0.04133674544 -E1 -0.02066837272 -E2 -0.02066837272 -six_dim 1 -e0 $e0 -K [expr 0.9511964998*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*-0.04133674544*-0.04133674544/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "DS3C_A" -length 0.01 

Multipole -name "DL.SX3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.046818406*$e0] -e0 $e0 

Drift -name "DS3C_B" -length 0.1702229 

Quadrupole -name "QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1666676*$e0] -e0 $e0 

Bpm -name "BPM28" -length 0
Quadrupole -name "QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1666676*$e0] -e0 $e0 

Bpm -name "BPM29" -length 0
Drift -name "DS3C_B" -length 0.1702229 

Multipole -name "DL.SXX3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.046818406*$e0] -e0 $e0 

Drift -name "DS3C_A" -length 0.01 

Sbend -name "DIPN" -synrad $sbend_synrad -length 0.2851351189 -angle -0.04133674544 -E1 -0.02066837272 -E2 -0.02066837272 -six_dim 1 -e0 $e0 -K [expr 0.9511964998*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*-0.04133674544*-0.04133674544/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "DS2C_B" -length 0.01 

Multipole -name "DL.SXX2" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -0.5879111252*$e0] -e0 $e0 

Drift -name "DS2C_A" -length 0.2846962 

Quadrupole -name "QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.209056*$e0] -e0 $e0 

Bpm -name "BPM30" -length 0
Drift -name "DS1C_B" -length 0.25 

Multipole -name "DL.SXX1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -0.8208368585*$e0] -e0 $e0 

Drift -name "DS1C_A" -length 0.1850809 

Sbend -name "DIPOLE" -synrad $sbend_synrad -length 2.56621607 -angle 0.372030709 -E1 0.1860153545 -E2 0.1860153545 -six_dim 1 -e0 $e0 -K [expr 0.04695405543*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*0.372030709*0.372030709/2.56621607*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "D12" -length 0.1559115 

Quadrupole -name "QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.824612*$e0] -e0 $e0 

Bpm -name "BPM31" -length 0
Drift -name "D11B" -length 0.0940885 

Multipole -name "DL.SXCH1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -14.58528081*$e0] -e0 $e0 

Drift -name "D11A" -length 0.1 

Quadrupole -name "QI1" -synrad $quad_synrad -length 0.2 -strength [expr -0.746512*$e0] -e0 $e0 

Bpm -name "BPM32" -length 0
Drift -name "MCELL" -length 0 

Drift -name "MCELL" -length 0 

Quadrupole -name "QI1" -synrad $quad_synrad -length 0.2 -strength [expr -0.746512*$e0] -e0 $e0 

Bpm -name "BPM33" -length 0
Drift -name "D11A" -length 0.1 

Multipole -name "DL.SXCH1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -14.58528081*$e0] -e0 $e0 

Drift -name "D11B" -length 0.0940885 

Quadrupole -name "QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.824612*$e0] -e0 $e0 

Bpm -name "BPM34" -length 0
Drift -name "D12" -length 0.1559115 

Sbend -name "DIPOLE" -synrad $sbend_synrad -length 2.56621607 -angle 0.372030709 -E1 0.1860153545 -E2 0.1860153545 -six_dim 1 -e0 $e0 -K [expr 0.04695405543*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*0.372030709*0.372030709/2.56621607*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "DS1C_A" -length 0.1850809 

Multipole -name "DL.SX1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -0.8208368585*$e0] -e0 $e0 

Drift -name "DS1C_B" -length 0.25 

Quadrupole -name "QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.209056*$e0] -e0 $e0 

Bpm -name "BPM35" -length 0
Drift -name "DS2C_A" -length 0.2846962 

Multipole -name "DL.SX2" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -0.5879111252*$e0] -e0 $e0 

Drift -name "DS2C_B" -length 0.01 

Sbend -name "DIPN" -synrad $sbend_synrad -length 0.2851351189 -angle -0.04133674544 -E1 -0.02066837272 -E2 -0.02066837272 -six_dim 1 -e0 $e0 -K [expr 0.9511964998*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*-0.04133674544*-0.04133674544/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "DS3C_A" -length 0.01 

Multipole -name "DL.SX3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.046818406*$e0] -e0 $e0 

Drift -name "DS3C_B" -length 0.1702229 

Quadrupole -name "QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1666676*$e0] -e0 $e0 

Bpm -name "BPM36" -length 0
Quadrupole -name "QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1666676*$e0] -e0 $e0 

Bpm -name "BPM37" -length 0
Drift -name "DS3C_B" -length 0.1702229 

Multipole -name "DL.SXX3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.046818406*$e0] -e0 $e0 

Drift -name "DS3C_A" -length 0.01 

Sbend -name "DIPN" -synrad $sbend_synrad -length 0.2851351189 -angle -0.04133674544 -E1 -0.02066837272 -E2 -0.02066837272 -six_dim 1 -e0 $e0 -K [expr 0.9511964998*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*-0.04133674544*-0.04133674544/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "DS2C_B" -length 0.01 

Multipole -name "DL.SXX2" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -0.5879111252*$e0] -e0 $e0 

Drift -name "DS2C_A" -length 0.2846962 

Quadrupole -name "QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.209056*$e0] -e0 $e0 

Bpm -name "BPM38" -length 0
Drift -name "DS1C_B" -length 0.25 

Multipole -name "DL.SXX1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -0.8208368585*$e0] -e0 $e0 

Drift -name "DS1C_A" -length 0.1850809 

Sbend -name "DIPOLE" -synrad $sbend_synrad -length 2.56621607 -angle 0.372030709 -E1 0.1860153545 -E2 0.1860153545 -six_dim 1 -e0 $e0 -K [expr 0.04695405543*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*0.372030709*0.372030709/2.56621607*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "D12" -length 0.1559115 

Quadrupole -name "QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.824612*$e0] -e0 $e0 

Bpm -name "BPM39" -length 0
Drift -name "D11B" -length 0.0940885 

Multipole -name "DL.SXCH1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -14.58528081*$e0] -e0 $e0 

Drift -name "D11A" -length 0.1 

Quadrupole -name "QI1" -synrad $quad_synrad -length 0.2 -strength [expr -0.746512*$e0] -e0 $e0 

Bpm -name "BPM40" -length 0
Drift -name "MCELL" -length 0 

Drift -name "MCELL" -length 0 

Quadrupole -name "QI1" -synrad $quad_synrad -length 0.2 -strength [expr -0.746512*$e0] -e0 $e0 

Bpm -name "BPM41" -length 0
Drift -name "D11A" -length 0.1 

Multipole -name "DL.SXCH1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -14.58528081*$e0] -e0 $e0 

Drift -name "D11B" -length 0.0940885 

Quadrupole -name "QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.824612*$e0] -e0 $e0 

Bpm -name "BPM42" -length 0
Drift -name "D12" -length 0.1559115 

Sbend -name "DIPOLE" -synrad $sbend_synrad -length 2.56621607 -angle 0.372030709 -E1 0.1860153545 -E2 0.1860153545 -six_dim 1 -e0 $e0 -K [expr 0.04695405543*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*0.372030709*0.372030709/2.56621607*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "DS1C_A" -length 0.1850809 

Multipole -name "DL.SX1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -0.8208368585*$e0] -e0 $e0 

Drift -name "DS1C_B" -length 0.25 

Quadrupole -name "QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.209056*$e0] -e0 $e0 

Bpm -name "BPM43" -length 0
Drift -name "DS2C_A" -length 0.2846962 

Multipole -name "DL.SX2" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -0.5879111252*$e0] -e0 $e0 

Drift -name "DS2C_B" -length 0.01 

Sbend -name "DIPN" -synrad $sbend_synrad -length 0.2851351189 -angle -0.04133674544 -E1 -0.02066837272 -E2 -0.02066837272 -six_dim 1 -e0 $e0 -K [expr 0.9511964998*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*-0.04133674544*-0.04133674544/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "DS3C_A" -length 0.01 

Multipole -name "DL.SX3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.046818406*$e0] -e0 $e0 

Drift -name "DS3C_B" -length 0.1702229 

Quadrupole -name "QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1666676*$e0] -e0 $e0 

Bpm -name "BPM44" -length 0
Quadrupole -name "QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1666676*$e0] -e0 $e0 

Bpm -name "BPM45" -length 0
Drift -name "DS3C_B" -length 0.1702229 

Multipole -name "DL.SXX3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.046818406*$e0] -e0 $e0 

Drift -name "DS3C_A" -length 0.01 

Sbend -name "DIPN" -synrad $sbend_synrad -length 0.2851351189 -angle -0.04133674544 -E1 -0.02066837272 -E2 -0.02066837272 -six_dim 1 -e0 $e0 -K [expr 0.9511964998*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*-0.04133674544*-0.04133674544/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "DS2C_B" -length 0.01 

Multipole -name "DL.SXX2" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -0.5879111252*$e0] -e0 $e0 

Drift -name "DS2C_A" -length 0.2846962 

Quadrupole -name "QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.209056*$e0] -e0 $e0 

Bpm -name "BPM46" -length 0
Drift -name "DS1C_B" -length 0.25 

Multipole -name "DL.SXX1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -0.8208368585*$e0] -e0 $e0 

Drift -name "DS1C_A" -length 0.1850809 

Sbend -name "DIPOLE" -synrad $sbend_synrad -length 2.56621607 -angle 0.372030709 -E1 0.1860153545 -E2 0.1860153545 -six_dim 1 -e0 $e0 -K [expr 0.04695405543*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*0.372030709*0.372030709/2.56621607*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "D12" -length 0.1559115 

Quadrupole -name "QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.824612*$e0] -e0 $e0 

Bpm -name "BPM47" -length 0
Drift -name "D11B" -length 0.0940885 

Multipole -name "DL.SXCH1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -14.58528081*$e0] -e0 $e0 

Drift -name "D11A" -length 0.1 

Quadrupole -name "QI1" -synrad $quad_synrad -length 0.2 -strength [expr -0.746512*$e0] -e0 $e0 

Bpm -name "BPM48" -length 0
Drift -name "MCELL" -length 0 

Drift -name "DL.MIDDLE" -length 0 

Drift -name "MCELL" -length 0 

Quadrupole -name "QI1" -synrad $quad_synrad -length 0.2 -strength [expr -0.746512*$e0] -e0 $e0 

Bpm -name "BPM49" -length 0
Drift -name "D11A" -length 0.1 

Multipole -name "DL.SXCH1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -14.58528081*$e0] -e0 $e0 

Drift -name "D11B" -length 0.0940885 

Quadrupole -name "QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.824612*$e0] -e0 $e0 

Bpm -name "BPM50" -length 0
Drift -name "D12" -length 0.1559115 

Sbend -name "DIPOLE" -synrad $sbend_synrad -length 2.56621607 -angle 0.372030709 -E1 0.1860153545 -E2 0.1860153545 -six_dim 1 -e0 $e0 -K [expr 0.04695405543*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*0.372030709*0.372030709/2.56621607*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "DS1C_A" -length 0.1850809 

Multipole -name "DL.SX1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -0.8208368585*$e0] -e0 $e0 

Drift -name "DS1C_B" -length 0.25 

Quadrupole -name "QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.209056*$e0] -e0 $e0 

Bpm -name "BPM51" -length 0
Drift -name "DS2C_A" -length 0.2846962 

Multipole -name "DL.SX2" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -0.5879111252*$e0] -e0 $e0 

Drift -name "DS2C_B" -length 0.01 

Sbend -name "DIPN" -synrad $sbend_synrad -length 0.2851351189 -angle -0.04133674544 -E1 -0.02066837272 -E2 -0.02066837272 -six_dim 1 -e0 $e0 -K [expr 0.9511964998*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*-0.04133674544*-0.04133674544/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "DS3C_A" -length 0.01 

Multipole -name "DL.SX3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.046818406*$e0] -e0 $e0 

Drift -name "DS3C_B" -length 0.1702229 

Quadrupole -name "QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1666676*$e0] -e0 $e0 

Bpm -name "BPM52" -length 0
Quadrupole -name "QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1666676*$e0] -e0 $e0 

Bpm -name "BPM53" -length 0
Drift -name "DS3C_B" -length 0.1702229 

Multipole -name "DL.SXX3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.046818406*$e0] -e0 $e0 

Drift -name "DS3C_A" -length 0.01 

Sbend -name "DIPN" -synrad $sbend_synrad -length 0.2851351189 -angle -0.04133674544 -E1 -0.02066837272 -E2 -0.02066837272 -six_dim 1 -e0 $e0 -K [expr 0.9511964998*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*-0.04133674544*-0.04133674544/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "DS2C_B" -length 0.01 

Multipole -name "DL.SXX2" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -0.5879111252*$e0] -e0 $e0 

Drift -name "DS2C_A" -length 0.2846962 

Quadrupole -name "QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.209056*$e0] -e0 $e0 

Bpm -name "BPM54" -length 0
Drift -name "DS1C_B" -length 0.25 

Multipole -name "DL.SXX1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -0.8208368585*$e0] -e0 $e0 

Drift -name "DS1C_A" -length 0.1850809 

Sbend -name "DIPOLE" -synrad $sbend_synrad -length 2.56621607 -angle 0.372030709 -E1 0.1860153545 -E2 0.1860153545 -six_dim 1 -e0 $e0 -K [expr 0.04695405543*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*0.372030709*0.372030709/2.56621607*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "D12" -length 0.1559115 

Quadrupole -name "QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.824612*$e0] -e0 $e0 

Bpm -name "BPM55" -length 0
Drift -name "D11B" -length 0.0940885 

Multipole -name "DL.SXCH1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -14.58528081*$e0] -e0 $e0 

Drift -name "D11A" -length 0.1 

Quadrupole -name "QI1" -synrad $quad_synrad -length 0.2 -strength [expr -0.746512*$e0] -e0 $e0 

Bpm -name "BPM56" -length 0
Drift -name "MCELL" -length 0 

Drift -name "MCELL" -length 0 

Quadrupole -name "QI1" -synrad $quad_synrad -length 0.2 -strength [expr -0.746512*$e0] -e0 $e0 

Bpm -name "BPM57" -length 0
Drift -name "D11A" -length 0.1 

Multipole -name "DL.SXCH1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -14.58528081*$e0] -e0 $e0 

Drift -name "D11B" -length 0.0940885 

Quadrupole -name "QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.824612*$e0] -e0 $e0 

Bpm -name "BPM58" -length 0
Drift -name "D12" -length 0.1559115 

Sbend -name "DIPOLE" -synrad $sbend_synrad -length 2.56621607 -angle 0.372030709 -E1 0.1860153545 -E2 0.1860153545 -six_dim 1 -e0 $e0 -K [expr 0.04695405543*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*0.372030709*0.372030709/2.56621607*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "DS1C_A" -length 0.1850809 

Multipole -name "DL.SX1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -0.8208368585*$e0] -e0 $e0 

Drift -name "DS1C_B" -length 0.25 

Quadrupole -name "QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.209056*$e0] -e0 $e0 

Bpm -name "BPM59" -length 0
Drift -name "DS2C_A" -length 0.2846962 

Multipole -name "DL.SX2" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -0.5879111252*$e0] -e0 $e0 

Drift -name "DS2C_B" -length 0.01 

Sbend -name "DIPN" -synrad $sbend_synrad -length 0.2851351189 -angle -0.04133674544 -E1 -0.02066837272 -E2 -0.02066837272 -six_dim 1 -e0 $e0 -K [expr 0.9511964998*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*-0.04133674544*-0.04133674544/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "DS3C_A" -length 0.01 

Multipole -name "DL.SX3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.046818406*$e0] -e0 $e0 

Drift -name "DS3C_B" -length 0.1702229 

Quadrupole -name "QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1666676*$e0] -e0 $e0 

Bpm -name "BPM60" -length 0
Quadrupole -name "QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1666676*$e0] -e0 $e0 

Bpm -name "BPM61" -length 0
Drift -name "DS3C_B" -length 0.1702229 

Multipole -name "DL.SXX3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.046818406*$e0] -e0 $e0 

Drift -name "DS3C_A" -length 0.01 

Sbend -name "DIPN" -synrad $sbend_synrad -length 0.2851351189 -angle -0.04133674544 -E1 -0.02066837272 -E2 -0.02066837272 -six_dim 1 -e0 $e0 -K [expr 0.9511964998*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*-0.04133674544*-0.04133674544/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "DS2C_B" -length 0.01 

Multipole -name "DL.SXX2" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -0.5879111252*$e0] -e0 $e0 

Drift -name "DS2C_A" -length 0.2846962 

Quadrupole -name "QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.209056*$e0] -e0 $e0 

Bpm -name "BPM62" -length 0
Drift -name "DS1C_B" -length 0.25 

Multipole -name "DL.SXX1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -0.8208368585*$e0] -e0 $e0 

Drift -name "DS1C_A" -length 0.1850809 

Sbend -name "DIPOLE" -synrad $sbend_synrad -length 2.56621607 -angle 0.372030709 -E1 0.1860153545 -E2 0.1860153545 -six_dim 1 -e0 $e0 -K [expr 0.04695405543*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*0.372030709*0.372030709/2.56621607*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "D12" -length 0.1559115 

Quadrupole -name "QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.824612*$e0] -e0 $e0 

Bpm -name "BPM63" -length 0
Drift -name "D11B" -length 0.0940885 

Multipole -name "DL.SXCH1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -14.58528081*$e0] -e0 $e0 

Drift -name "D11A" -length 0.1 

Quadrupole -name "QI1" -synrad $quad_synrad -length 0.2 -strength [expr -0.746512*$e0] -e0 $e0 

Bpm -name "BPM64" -length 0
Drift -name "MCELL" -length 0 

Drift -name "MCELL" -length 0 

Quadrupole -name "QI1" -synrad $quad_synrad -length 0.2 -strength [expr -0.746512*$e0] -e0 $e0 

Bpm -name "BPM65" -length 0
Drift -name "D11A" -length 0.1 

Multipole -name "DL.SXCH1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -14.58528081*$e0] -e0 $e0 

Drift -name "D11B" -length 0.0940885 

Quadrupole -name "QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.824612*$e0] -e0 $e0 

Bpm -name "BPM66" -length 0
Drift -name "D12" -length 0.1559115 

Sbend -name "DIPOLE" -synrad $sbend_synrad -length 2.56621607 -angle 0.372030709 -E1 0.1860153545 -E2 0.1860153545 -six_dim 1 -e0 $e0 -K [expr 0.04695405543*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*0.372030709*0.372030709/2.56621607*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "DS1C_A" -length 0.1850809 

Multipole -name "DL.SX1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -0.8208368585*$e0] -e0 $e0 

Drift -name "DS1C_B" -length 0.25 

Quadrupole -name "QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.209056*$e0] -e0 $e0 

Bpm -name "BPM67" -length 0
Drift -name "DS2C_A" -length 0.2846962 

Multipole -name "DL.SX2" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -0.5879111252*$e0] -e0 $e0 

Drift -name "DS2C_B" -length 0.01 

Sbend -name "DIPN" -synrad $sbend_synrad -length 0.2851351189 -angle -0.04133674544 -E1 -0.02066837272 -E2 -0.02066837272 -six_dim 1 -e0 $e0 -K [expr 0.9511964998*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*-0.04133674544*-0.04133674544/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "DS3C_A" -length 0.01 

Multipole -name "DL.SX3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.046818406*$e0] -e0 $e0 

Drift -name "DS3C_B" -length 0.1702229 

Quadrupole -name "QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1666676*$e0] -e0 $e0 

Bpm -name "BPM68" -length 0
Quadrupole -name "QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1666676*$e0] -e0 $e0 

Bpm -name "BPM69" -length 0
Drift -name "DS3C_B" -length 0.1702229 

Multipole -name "DL.SXX3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.046818406*$e0] -e0 $e0 

Drift -name "DS3C_A" -length 0.01 

Sbend -name "DIPN" -synrad $sbend_synrad -length 0.2851351189 -angle -0.04133674544 -E1 -0.02066837272 -E2 -0.02066837272 -six_dim 1 -e0 $e0 -K [expr 0.9511964998*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*-0.04133674544*-0.04133674544/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "DS2C_B" -length 0.01 

Multipole -name "DL.SXX2" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -0.5879111252*$e0] -e0 $e0 

Drift -name "DS2C_A" -length 0.2846962 

Quadrupole -name "QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.209056*$e0] -e0 $e0 

Bpm -name "BPM70" -length 0
Drift -name "DS1C_B" -length 0.25 

Multipole -name "DL.SXX1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -0.8208368585*$e0] -e0 $e0 

Drift -name "DS1C_A" -length 0.1850809 

Sbend -name "DIPOLE" -synrad $sbend_synrad -length 2.56621607 -angle 0.372030709 -E1 0.1860153545 -E2 0.1860153545 -six_dim 1 -e0 $e0 -K [expr 0.04695405543*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*0.372030709*0.372030709/2.56621607*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "D12" -length 0.1559115 

Quadrupole -name "QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.824612*$e0] -e0 $e0 

Bpm -name "BPM71" -length 0
Drift -name "D11B" -length 0.0940885 

Multipole -name "DL.SXCH1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -14.58528081*$e0] -e0 $e0 

Drift -name "D11A" -length 0.1 

Quadrupole -name "QI1" -synrad $quad_synrad -length 0.2 -strength [expr -0.746512*$e0] -e0 $e0 

Bpm -name "BPM72" -length 0
Drift -name "MCELL" -length 0 

Drift -name "MCELL" -length 0 

Quadrupole -name "QI1" -synrad $quad_synrad -length 0.2 -strength [expr -0.746512*$e0] -e0 $e0 

Bpm -name "BPM73" -length 0
Drift -name "D11A" -length 0.1 

Multipole -name "DL.SNCH1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 14.58528081*$e0] -e0 $e0 

Drift -name "D11B" -length 0.0940885 

Quadrupole -name "QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.824612*$e0] -e0 $e0 

Bpm -name "BPM74" -length 0
Drift -name "D12" -length 0.1559115 

Sbend -name "DIPOLE0" -synrad $sbend_synrad -length 2.56621607 -angle -0.372030709 -E1 -0.1860153545 -E2 -0.1860153545 -six_dim 1 -e0 $e0 -K [expr 0.04695405543*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.372030709*-0.372030709/2.56621607*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "DS1C_A" -length 0.1850809 

Multipole -name "DL.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0.8208368585*$e0] -e0 $e0 

Drift -name "DS1C_B" -length 0.25 

Quadrupole -name "QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.209056*$e0] -e0 $e0 

Bpm -name "BPM75" -length 0
Drift -name "DS2C_A" -length 0.2846962 

Multipole -name "DL.SN2" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0.5879111252*$e0] -e0 $e0 

Drift -name "DS2C_B" -length 0.01 

Sbend -name "DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.04133674544 -E1 0.02066837272 -E2 0.02066837272 -six_dim 1 -e0 $e0 -K [expr 0.9511964998*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.04133674544*0.04133674544/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "DS3C_A" -length 0.01 

Multipole -name "DL.SN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.046818406*$e0] -e0 $e0 

Drift -name "DS3C_B" -length 0.1702229 

Quadrupole -name "QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1666676*$e0] -e0 $e0 

Bpm -name "BPM76" -length 0
Quadrupole -name "QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1666676*$e0] -e0 $e0 

Bpm -name "BPM77" -length 0
Drift -name "DS3C_B" -length 0.1702229 

Multipole -name "DL.SNN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.046818406*$e0] -e0 $e0 

Drift -name "DS3C_A" -length 0.01 

Sbend -name "DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.04133674544 -E1 0.02066837272 -E2 0.02066837272 -six_dim 1 -e0 $e0 -K [expr 0.9511964998*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.04133674544*0.04133674544/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "DS2C_B" -length 0.01 

Multipole -name "DL.SNN2" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0.5879111252*$e0] -e0 $e0 

Drift -name "DS2C_A" -length 0.2846962 

Quadrupole -name "QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.209056*$e0] -e0 $e0 

Bpm -name "BPM78" -length 0
Drift -name "DS1C_B" -length 0.25 

Multipole -name "DL.SNN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0.8208368585*$e0] -e0 $e0 

Drift -name "DS1C_A" -length 0.1850809 

Sbend -name "DIPOLE0" -synrad $sbend_synrad -length 2.56621607 -angle -0.372030709 -E1 -0.1860153545 -E2 -0.1860153545 -six_dim 1 -e0 $e0 -K [expr 0.04695405543*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.372030709*-0.372030709/2.56621607*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "D12" -length 0.1559115 

Quadrupole -name "QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.824612*$e0] -e0 $e0 

Bpm -name "BPM79" -length 0
Drift -name "D11B" -length 0.0940885 

Multipole -name "DL.SNCH1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 14.58528081*$e0] -e0 $e0 

Drift -name "D11A" -length 0.1 

Quadrupole -name "QI1" -synrad $quad_synrad -length 0.2 -strength [expr -0.746512*$e0] -e0 $e0 

Bpm -name "BPM80" -length 0
Drift -name "MCELL" -length 0 

Drift -name "MCELL" -length 0 

Quadrupole -name "QI1" -synrad $quad_synrad -length 0.2 -strength [expr -0.746512*$e0] -e0 $e0 

Bpm -name "BPM81" -length 0
Drift -name "D11A" -length 0.1 

Multipole -name "DL.SNCH1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 14.58528081*$e0] -e0 $e0 

Drift -name "D11B" -length 0.0940885 

Quadrupole -name "QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.824612*$e0] -e0 $e0 

Bpm -name "BPM82" -length 0
Drift -name "D12" -length 0.1559115 

Sbend -name "DIPOLE0" -synrad $sbend_synrad -length 2.56621607 -angle -0.372030709 -E1 -0.1860153545 -E2 -0.1860153545 -six_dim 1 -e0 $e0 -K [expr 0.04695405543*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.372030709*-0.372030709/2.56621607*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "DS1C_A" -length 0.1850809 

Multipole -name "DL.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0.8208368585*$e0] -e0 $e0 

Drift -name "DS1C_B" -length 0.25 

Quadrupole -name "QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.209056*$e0] -e0 $e0 

Bpm -name "BPM83" -length 0
Drift -name "DS2C_A" -length 0.2846962 

Multipole -name "DL.SN2" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0.5879111252*$e0] -e0 $e0 

Drift -name "DS2C_B" -length 0.01 

Sbend -name "DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.04133674544 -E1 0.02066837272 -E2 0.02066837272 -six_dim 1 -e0 $e0 -K [expr 0.9511964998*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.04133674544*0.04133674544/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "DS3C_A" -length 0.01 

Multipole -name "DL.SN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.046818406*$e0] -e0 $e0 

Drift -name "DS3C_B" -length 0.1702229 

Quadrupole -name "QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1666676*$e0] -e0 $e0 

Bpm -name "BPM84" -length 0
Quadrupole -name "QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1666676*$e0] -e0 $e0 

Bpm -name "BPM85" -length 0
Drift -name "DS3C_B" -length 0.1702229 

Multipole -name "DL.SNN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.046818406*$e0] -e0 $e0 

Drift -name "DS3C_A" -length 0.01 

Sbend -name "DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.04133674544 -E1 0.02066837272 -E2 0.02066837272 -six_dim 1 -e0 $e0 -K [expr 0.9511964998*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.04133674544*0.04133674544/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "DS2C_B" -length 0.01 

Multipole -name "DL.SNN2" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0.5879111252*$e0] -e0 $e0 

Drift -name "DS2C_A" -length 0.2846962 

Quadrupole -name "QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.209056*$e0] -e0 $e0 

Bpm -name "BPM86" -length 0
Drift -name "DS1C_B" -length 0.25 

Multipole -name "DL.SNN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0.8208368585*$e0] -e0 $e0 

Drift -name "DS1C_A" -length 0.1850809 

Sbend -name "DIPOLE0" -synrad $sbend_synrad -length 2.56621607 -angle -0.372030709 -E1 -0.1860153545 -E2 -0.1860153545 -six_dim 1 -e0 $e0 -K [expr 0.04695405543*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.372030709*-0.372030709/2.56621607*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "D12" -length 0.1559115 

Quadrupole -name "QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.824612*$e0] -e0 $e0 

Bpm -name "BPM87" -length 0
Drift -name "D11B" -length 0.0940885 

Multipole -name "DL.SNCH1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 14.58528081*$e0] -e0 $e0 

Drift -name "D11A" -length 0.1 

Quadrupole -name "QI1" -synrad $quad_synrad -length 0.2 -strength [expr -0.746512*$e0] -e0 $e0 

Bpm -name "BPM88" -length 0
Drift -name "MCELL" -length 0 

Quadrupole -name "QI1" -synrad $quad_synrad -length 0.2 -strength [expr -0.746512*$e0] -e0 $e0 

Bpm -name "BPM89" -length 0
Drift -name "D11A" -length 0.1 

Multipole -name "DL.SNCH1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 14.58528081*$e0] -e0 $e0 

Drift -name "D11B" -length 0.0940885 

Quadrupole -name "QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.824612*$e0] -e0 $e0 

Bpm -name "BPM90" -length 0
Drift -name "D12" -length 0.1559115 

Sbend -name "DIPOLE0" -synrad $sbend_synrad -length 2.56621607 -angle -0.372030709 -E1 -0.1860153545 -E2 -0.1860153545 -six_dim 1 -e0 $e0 -K [expr 0.04695405543*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.372030709*-0.372030709/2.56621607*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "DS1C_A" -length 0.1850809 

Multipole -name "DL.SNN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0.8208368585*$e0] -e0 $e0 

Drift -name "DS1C_B" -length 0.25 

Quadrupole -name "QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.209056*$e0] -e0 $e0 

Bpm -name "BPM91" -length 0
Drift -name "DS2C_A" -length 0.2846962 

Multipole -name "DL.SNN2" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0.5879111252*$e0] -e0 $e0 

Drift -name "DS2C_B" -length 0.01 

Sbend -name "DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.04133674544 -E1 0.02066837272 -E2 0.02066837272 -six_dim 1 -e0 $e0 -K [expr 0.9511964998*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.04133674544*0.04133674544/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "DS3C_A" -length 0.01 

Multipole -name "DL.SNN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.046818406*$e0] -e0 $e0 

Drift -name "DS3C_B" -length 0.1702229 

Quadrupole -name "QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1666676*$e0] -e0 $e0 

Bpm -name "BPM92" -length 0
Quadrupole -name "QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1666676*$e0] -e0 $e0 

Bpm -name "BPM93" -length 0
Drift -name "DS3CB" -length 0.08511145 

Multipole -name "DL.SXL4" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.067129221*$e0] -e0 $e0 

Drift -name "DS3CA" -length 0.08511145 

Drift -name "MD2" -length 0 

Sbend -name "DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.04133674544 -E1 0.02066837272 -E2 0.02066837272 -six_dim 1 -e0 $e0 -K [expr 0.9511964998*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.04133674544*0.04133674544/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "D33B" -length 0.0464855 

Multipole -name "DL.SXL3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -2.704344389*$e0] -e0 $e0 

Drift -name "D33A" -length 0.0464855 

Quadrupole -name "QL3IN" -synrad $quad_synrad -length 0.4 -strength [expr -1.943224*$e0] -e0 $e0 

Bpm -name "BPM94" -length 0
Drift -name "D32B" -length 0.43333 

Multipole -name "DL.SXL2" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -0.5985490004*$e0] -e0 $e0 

Drift -name "D32A" -length 0.11111 

Quadrupole -name "QL2IN" -synrad $quad_synrad -length 0.4 -strength [expr 1.14802*$e0] -e0 $e0 

Bpm -name "BPM95" -length 0
Drift -name "D33" -length 0.192971 

Drift -name "MD3" -length 0 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.574109835

Sbend -name "SEPT2" -synrad $sbend_synrad -length 2.58202788155376 -angle -0.2714151852 -E1 -0.1357075926 -E2 -0.1357075926 -e0 $e0 -K [expr -1.249456954*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2714151852*-0.2714151852/2.58202788155376*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "DL3IN" -length 0.4 

Quadrupole -name "QL1IN" -synrad $quad_synrad -length 0.4 -strength [expr -0.647036*$e0] -e0 $e0 

Bpm -name "BPM96" -length 0
Drift -name "DL2IN" -length 0.3 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 1.00031738

Sbend -name "SEPT1" -synrad $sbend_synrad -length 1.00063486103645 -angle -0.0872664626 -E1 -0.0436332313 -E2 -0.0436332313 -e0 $e0 -K [expr 0.9956481989*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.0872664626*-0.0872664626/1.00063486103645*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "DL1IN" -length 1.1 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 0.5000007127

Sbend -name "SEPTQ" -synrad $sbend_synrad -length 0.500001425441655 -angle -0.005849061169 -E1 -0.002924530584 -E2 -0.002924530584 -e0 $e0 -K [expr -0.5000007127*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*-0.005849061169*-0.005849061169/0.500001425441655*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "DRRF" -length 2 

Drift -name "RFDEFL" -length 0 

Drift -name "DL.DLEND" -length 0 
Bpm -name "BPM97" -length 0
Drift -name "DL.EJECTION" -length 0 

Quadrupole -name "TL1.QINJ" -synrad $quad_synrad -length 0.0 -strength [expr 0.0*$e0] -e0 $e0 
Bpm -name "BPM0" -length 0
Drift -name "TL1.TL1START" -length 0 

Drift -name "TL1.INJECTION" -length 0 

Drift -name "TL1.D11" -length 0.8 

Quadrupole -name "TL1.Q12" -synrad $quad_synrad -length 0.75 -strength [expr -0.724266702*$e0] -e0 $e0 

Bpm -name "BPM1" -length 0
Drift -name "TL1.D13" -length 0.8 

Quadrupole -name "TL1.Q14" -synrad $quad_synrad -length 0.75 -strength [expr 0.917454447*$e0] -e0 $e0 

Bpm -name "BPM2" -length 0
Drift -name "TL1.D15" -length 1.6 

Quadrupole -name "TL1.Q16" -synrad $quad_synrad -length 0.75 -strength [expr -0.8542453973*$e0] -e0 $e0 

Bpm -name "BPM3" -length 0
Drift -name "TL1.D17" -length 0.8 

Quadrupole -name "TL1.Q18" -synrad $quad_synrad -length 0.75 -strength [expr 0.6709421753*$e0] -e0 $e0 

Bpm -name "BPM4" -length 0
Drift -name "TL1.D19" -length 0.8 

Drift -name "TL1.OMSEND" -length 0 

Drift -name "TL1.ENTCELLONE" -length 0 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.005723015

Sbend -name "TL1.BEND20" -synrad $sbend_synrad -length 2.01146240647403 -angle 0.2617993878 -E1 0.1308996939 -E2 0.1308996939 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*0.2617993878*0.2617993878/2.01146240647403*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TL1.D222" -length 0.7773893387 

Multipole -name "TL1.SX225" -synrad $mult_synrad -type 3 -length 0.3 -strength [expr 7.299391041*$e0] -e0 $e0 

Drift -name "TL1.D228" -length 0.3136946693 

Quadrupole -name "TL1.Q23" -synrad $quad_synrad -length 0.75 -strength [expr 0.4599766094*$e0] -e0 $e0 

Bpm -name "BPM5" -length 0
Drift -name "TL1.D242" -length 0.1562568231 

Multipole -name "TL1.SX245" -synrad $mult_synrad -type 3 -length 0.3 -strength [expr -5.684636721*$e0] -e0 $e0 

Drift -name "TL1.D248" -length 0.1562568231 

Quadrupole -name "TL1.Q25" -synrad $quad_synrad -length 0.75 -strength [expr -0.5296565843*$e0] -e0 $e0 

Bpm -name "BPM6" -length 0
Drift -name "TL1.D262" -length 0.0500000253 

Drift -name "TL1.D268" -length 0.0500000253 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 0.500013974

Sbend -name "TL1.BEND27" -synrad $sbend_synrad -length 0.500027948380432 -angle -0.02589861758 -E1 -0.01294930879 -E2 -0.01294930879 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.02589861758*-0.02589861758/0.500027948380432*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TL1.D282" -length 0.1536564076 

Multipole -name "TL1.SX285" -synrad $mult_synrad -type 3 -length 0.3 -strength [expr 3.349512984*$e0] -e0 $e0 

Drift -name "TL1.D288" -length 0.1536564076 

Quadrupole -name "TL1.Q30A" -synrad $quad_synrad -length 0.375 -strength [expr 0.2496519342*$e0] -e0 $e0 

Bpm -name "BPM7" -length 0
Drift -name "TL1.MIDCELLONE" -length 0 

Quadrupole -name "TL1.Q30B" -synrad $quad_synrad -length 0.375 -strength [expr 0.2496519342*$e0] -e0 $e0 

Bpm -name "BPM8" -length 0
Drift -name "TL1.D322" -length 0.1536564076 

Multipole -name "TL1.SX325" -synrad $mult_synrad -type 3 -length 0.3 -strength [expr 3.349512984*$e0] -e0 $e0 

Drift -name "TL1.D328" -length 0.1536564076 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 0.500013974

Sbend -name "TL1.BEND33" -synrad $sbend_synrad -length 0.500027948380432 -angle -0.02589861758 -E1 -0.01294930879 -E2 -0.01294930879 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.02589861758*-0.02589861758/0.500027948380432*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TL1.D342" -length 0.0500000253 

Drift -name "TL1.D348" -length 0.0500000253 

Quadrupole -name "TL1.Q35" -synrad $quad_synrad -length 0.75 -strength [expr -0.5296565843*$e0] -e0 $e0 

Bpm -name "BPM9" -length 0
Drift -name "TL1.D362" -length 0.1562568231 

Multipole -name "TL1.SX365" -synrad $mult_synrad -type 3 -length 0.3 -strength [expr -5.684636721*$e0] -e0 $e0 

Drift -name "TL1.D368" -length 0.1562568231 

Quadrupole -name "TL1.Q37" -synrad $quad_synrad -length 0.75 -strength [expr 0.4599766094*$e0] -e0 $e0 

Bpm -name "BPM10" -length 0
Drift -name "TL1.D382" -length 0.3136946693 

Multipole -name "TL1.SX385" -synrad $mult_synrad -type 3 -length 0.3 -strength [expr 7.299391041*$e0] -e0 $e0 

Drift -name "TL1.D388" -length 0.7773893387 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.005723015

Sbend -name "TL1.BEND40" -synrad $sbend_synrad -length 2.01146240647403 -angle 0.2617993878 -E1 0.1308996939 -E2 0.1308996939 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*0.2617993878*0.2617993878/2.01146240647403*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TL1.EXICELLONE" -length 0 

Drift -name "TL1.D402" -length 0.4980886966 

Multipole -name "TL1.SXC405" -synrad $mult_synrad -type 3 -length 0.3 -strength [expr 4.716698928*$e0] -e0 $e0 

Drift -name "TL1.D408" -length 0.06602956553 

Quadrupole -name "TL1.Q41" -synrad $quad_synrad -length 0.75 -strength [expr 0.1786552102*$e0] -e0 $e0 

Bpm -name "BPM11" -length 0
Drift -name "TL1.D412" -length 0.9874829865 

Multipole -name "TL1.SXC415" -synrad $mult_synrad -type 3 -length 0.3 -strength [expr -0.8131321254*$e0] -e0 $e0 

Drift -name "TL1.D418" -length 0.2291609955 

Quadrupole -name "TL1.Q42" -synrad $quad_synrad -length 0.75 -strength [expr -0.5380648102*$e0] -e0 $e0 

Bpm -name "BPM12" -length 0
Drift -name "TL1.D422" -length 1.245610625 

Multipole -name "TL1.SXC425" -synrad $mult_synrad -type 3 -length 0.3 -strength [expr 1.123906591*$e0] -e0 $e0 

Drift -name "TL1.D428" -length 0.3152035415 

Quadrupole -name "TL1.Q43" -synrad $quad_synrad -length 0.75 -strength [expr 0.5385499766*$e0] -e0 $e0 

Bpm -name "BPM13" -length 0
Drift -name "TL1.D432" -length 1.58013019 

Multipole -name "TL1.SXC435" -synrad $mult_synrad -type 3 -length 0.3 -strength [expr -0.05494421721*$e0] -e0 $e0 

Drift -name "TL1.D438" -length 0.4267100632 

Quadrupole -name "TL1.Q44" -synrad $quad_synrad -length 0.75 -strength [expr -0.6426636217*$e0] -e0 $e0 

Bpm -name "BPM14" -length 0
Drift -name "TL1.D442" -length 1.673352339 

Multipole -name "TL1.SXC445" -synrad $mult_synrad -type 3 -length 0.3 -strength [expr -3.03407148*$e0] -e0 $e0 

Drift -name "TL1.D448" -length 0.457784113 

Quadrupole -name "TL1.Q45" -synrad $quad_synrad -length 0.75 -strength [expr 0.666494741*$e0] -e0 $e0 

Bpm -name "BPM15" -length 0
Drift -name "TL1.D452" -length 0.457784113 

Multipole -name "TL1.SXC455" -synrad $mult_synrad -type 3 -length 0.3 -strength [expr -1.958334129*$e0] -e0 $e0 

Drift -name "TL1.D458" -length 1.673352339 

Quadrupole -name "TL1.Q46" -synrad $quad_synrad -length 0.75 -strength [expr -0.6390286404*$e0] -e0 $e0 

Bpm -name "BPM16" -length 0
Drift -name "TL1.D462" -length 0.4267100632 

Multipole -name "TL1.SXC465" -synrad $mult_synrad -type 3 -length 0.3 -strength [expr 0.05421797688*$e0] -e0 $e0 

Drift -name "TL1.D468" -length 1.58013019 

Quadrupole -name "TL1.Q47" -synrad $quad_synrad -length 0.75 -strength [expr 0.5420133188*$e0] -e0 $e0 

Bpm -name "BPM17" -length 0
Drift -name "TL1.D472" -length 0.3152035415 

Multipole -name "TL1.SXC475" -synrad $mult_synrad -type 3 -length 0.3 -strength [expr -3.568722897*$e0] -e0 $e0 

Drift -name "TL1.D478" -length 1.245610625 

Quadrupole -name "TL1.Q48" -synrad $quad_synrad -length 0.75 -strength [expr -0.5286812296*$e0] -e0 $e0 

Bpm -name "BPM18" -length 0
Drift -name "TL1.D482" -length 0.2291609955 

Multipole -name "TL1.SXC485" -synrad $mult_synrad -type 3 -length 0.3 -strength [expr -0.7124077599*$e0] -e0 $e0 

Drift -name "TL1.D488" -length 0.9874829865 

Quadrupole -name "TL1.Q49" -synrad $quad_synrad -length 0.75 -strength [expr 0.1559722145*$e0] -e0 $e0 

Bpm -name "BPM19" -length 0
Drift -name "TL1.D492" -length 0.06602956553 

Multipole -name "TL1.SXC495" -synrad $mult_synrad -type 3 -length 0.3 -strength [expr 5.050571496*$e0] -e0 $e0 

Drift -name "TL1.D498" -length 0.4980886966 

Drift -name "TL1.INTEROMSEND" -length 0 

Drift -name "TL1.ENTCELLTWO" -length 0 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.005723015

Sbend -name "TL1.BEND50" -synrad $sbend_synrad -length 2.01146240647403 -angle -0.2617993878 -E1 -0.1308996939 -E2 -0.1308996939 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2617993878*-0.2617993878/2.01146240647403*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TL1.D522" -length 0.7773893387 

Multipole -name "TL1.SX525" -synrad $mult_synrad -type 3 -length 0.3 -strength [expr -2.201763504*$e0] -e0 $e0 

Drift -name "TL1.D528" -length 0.3136946693 

Quadrupole -name "TL1.Q53" -synrad $quad_synrad -length 0.75 -strength [expr 0.4599766094*$e0] -e0 $e0 

Bpm -name "BPM20" -length 0
Drift -name "TL1.D542" -length 0.1562568231 

Multipole -name "TL1.SX545" -synrad $mult_synrad -type 3 -length 0.3 -strength [expr 0.6378502557*$e0] -e0 $e0 

Drift -name "TL1.D548" -length 0.1562568231 

Quadrupole -name "TL1.Q55" -synrad $quad_synrad -length 0.75 -strength [expr -0.5296565843*$e0] -e0 $e0 

Bpm -name "BPM21" -length 0
Drift -name "TL1.D562" -length 0.0500000253 

Drift -name "TL1.D568" -length 0.0500000253 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 0.500013974

Sbend -name "TL1.BEND57" -synrad $sbend_synrad -length 0.500027948380432 -angle 0.02589861758 -E1 0.01294930879 -E2 0.01294930879 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*0.02589861758*0.02589861758/0.500027948380432*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TL1.D582" -length 0.1536564076 

Multipole -name "TL1.SX585" -synrad $mult_synrad -type 3 -length 0.3 -strength [expr 0.01149489044*$e0] -e0 $e0 

Drift -name "TL1.D588" -length 0.1536564076 

Quadrupole -name "TL1.Q60A" -synrad $quad_synrad -length 0.375 -strength [expr 0.2449040236*$e0] -e0 $e0 

Bpm -name "BPM22" -length 0
Drift -name "TL1.MIDCELLTWO" -length 0 

Quadrupole -name "TL1.Q60B" -synrad $quad_synrad -length 0.375 -strength [expr 0.2449040236*$e0] -e0 $e0 

Bpm -name "BPM23" -length 0
Drift -name "TL1.D622" -length 0.05000058615 

Multipole -name "TL1.SX625" -synrad $mult_synrad -type 3 -length 0.3 -strength [expr 2.579923689*$e0] -e0 $e0 

Drift -name "TL1.D628" -length 0.05000058615 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 0.500013974

Sbend -name "TL1.BEND63" -synrad $sbend_synrad -length 0.500027948380432 -angle 0.02589861758 -E1 0.01294930879 -E2 0.01294930879 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*0.02589861758*0.02589861758/0.500027948380432*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TL1.D642" -length 0.2366852486 

Drift -name "TL1.D648" -length 0.2366852486 

Quadrupole -name "TL1.Q65" -synrad $quad_synrad -length 0.75 -strength [expr -0.5144749811*$e0] -e0 $e0 

Bpm -name "BPM24" -length 0
Drift -name "TL1.D662" -length 0.1747711281 

Multipole -name "TL1.SX665" -synrad $mult_synrad -type 3 -length 0.3 -strength [expr 0.7736254974*$e0] -e0 $e0 

Drift -name "TL1.D668" -length 0.1747711281 

Quadrupole -name "TL1.Q67" -synrad $quad_synrad -length 0.75 -strength [expr 0.4599585843*$e0] -e0 $e0 

Bpm -name "BPM25" -length 0
Drift -name "TL1.D682" -length 0.258673648 

Multipole -name "TL1.SX685" -synrad $mult_synrad -type 3 -length 0.3 -strength [expr -2.373553751*$e0] -e0 $e0 

Drift -name "TL1.D688" -length 0.667347296 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.005723015

Sbend -name "TL1.BEND70" -synrad $sbend_synrad -length 2.01146240647403 -angle -0.2617993878 -E1 -0.1308996939 -E2 -0.1308996939 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2617993878*-0.2617993878/2.01146240647403*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TL1.EXICELLTWO" -length 0 

Drift -name "TL1.EJECTION" -length 0 

Drift -name "TL1.TL1END" -length 0 
Bpm -name "BPM26" -length 0
Quadrupole -name "TL1.QEJE" -synrad $quad_synrad -length 0.0 -strength [expr 0.0*$e0] -e0 $e0 

Bpm -name "BPM0" -length 0
Drift -name "MANELLOWSTART" -length 0 

Drift -name "CR1.INJECTION" -length 0 

Drift -name "CR1.DL7IN" -length 0.5 

Quadrupole -name "CR1.QL6IN" -synrad $quad_synrad -length 0.25 -strength [expr 0.496129*$e0] -e0 $e0 

Bpm -name "BPM1" -length 0
Drift -name "CR1.DL6IN" -length 0.553 

Quadrupole -name "CR1.QL5IN" -synrad $quad_synrad -length 0.5 -strength [expr -0.8298265*$e0] -e0 $e0 

Bpm -name "BPM2" -length 0
Drift -name "CR1.DL5IN" -length 1 

Quadrupole -name "CR1.QL4IN" -synrad $quad_synrad -length 0.5 -strength [expr 0.555285*$e0] -e0 $e0 

Bpm -name "BPM3" -length 0
Drift -name "CR1.DL4IN" -length 0.349996062 

Drift -name "CR1.DRRFL2" -length 0.45265 

# WARNING: CORRECTOR needs to be defined, no PLACET element

CORRECTOR -name "CR1.RFDEFLCOMP2" -length 0.2 -strength_x [expr 0.00095*1e6*$e0] 

Drift -name "CR1.DRRFL1" -length 0.1 

# WARNING: CORRECTOR needs to be defined, no PLACET element

CORRECTOR -name "CR1.RFDEFLBON" -length 1.147353938 -strength_x [expr 0.0038*1e6*$e0] 

Drift -name "CR1.DRRFL1" -length 0.1 

# WARNING: CORRECTOR needs to be defined, no PLACET element

CORRECTOR -name "CR1.RFDEFLCOMP1" -length 0.2 -strength_x [expr 0.00095*1e6*$e0] 

Drift -name "CR1.DRRFL" -length 0.3 

Drift -name "CR1.DL3IN" -length 1.099258 

Quadrupole -name "CR1.QL3IN" -synrad $quad_synrad -length 0.5 -strength [expr -0.4937532183*$e0] -e0 $e0 

Bpm -name "BPM4" -length 0
Drift -name "CR1.DL2IN" -length 0.3010372 

Quadrupole -name "CR1.QL2IN" -synrad $quad_synrad -length 0.5 -strength [expr 1.191142713*$e0] -e0 $e0 

Bpm -name "BPM5" -length 0
Drift -name "CR1.DL1IN" -length 0.3098482 

Quadrupole -name "CR1.QL1IN" -synrad $quad_synrad -length 0.5 -strength [expr -0.7229782885*$e0] -e0 $e0 

Bpm -name "BPM6" -length 0
Drift -name "CR1.DL0IN" -length 1.9898566 

Drift -name "CR1.MARCENT" -length 0 

Drift -name "CR1MDBACELLSTART" -length 0 

Drift -name "CR1.DBA.MCELL" -length 0 

Quadrupole -name "CR1.DBA.QI1" -synrad $quad_synrad -length 0.4 -strength [expr -0.3358875195*$e0] -e0 $e0 

Bpm -name "BPM7" -length 0
Drift -name "CR1.DBA.D11A" -length 0.2082887778 

Drift -name "CR1.DBA.D11B" -length 0.1989315367 

Quadrupole -name "CR1.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.66922795*$e0] -e0 $e0 

Bpm -name "BPM8" -length 0
Drift -name "CR1.DBA.D12" -length 0.2467904078 

Sbend -name "CR1.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -six_dim 1 -e0 $e0 -K [expr 0.05125499604*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*0.2983805661*0.2983805661/2.801278682*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.DS1C_A" -length 0.2929622945 

Multipole -name "CR1.DBA.SN0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.037095359*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS1C_B" -length 0.1 

Drift -name "CR1.DBA.HCM0" -length 0 

Quadrupole -name "CR1.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -0.450384*$e0] -e0 $e0 

Bpm -name "BPM9" -length 0
Drift -name "CR1.DBA.DS2C" -length 0.5664710132 

Sbend -name "CR1.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -six_dim 1 -e0 $e0 -K [expr 0.1995945832*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.HBM" -length 0 

Drift -name "CR1.DBA.DS3C_A" -length 0.1426363128 

Multipole -name "CR1.DBA.SN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.022314728*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS3C_B" -length 0.1426363128 

Quadrupole -name "CR1.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr 0.2323546236*$e0] -e0 $e0 

Bpm -name "BPM10" -length 0
Quadrupole -name "CR1.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr 0.2323546236*$e0] -e0 $e0 

Bpm -name "BPM11" -length 0
Drift -name "CR1.DBA.DS3C_B" -length 0.1426363128 

Multipole -name "CR1.DBA.SNN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS3C_A" -length 0.1426363128 

Drift -name "CR1.DBA.HBM2" -length 0 

Sbend -name "CR1.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -six_dim 1 -e0 $e0 -K [expr 0.1995945832*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.DS2C" -length 0.5664710132 

Quadrupole -name "CR1.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -0.450384*$e0] -e0 $e0 

Bpm -name "BPM12" -length 0
Drift -name "CR1.DBA.HCM" -length 0 

Drift -name "CR1.DBA.DS1C_B" -length 0.1 

Multipole -name "CR1.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 3.839149563*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS1C_A" -length 0.2929622945 

Sbend -name "CR1.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -six_dim 1 -e0 $e0 -K [expr 0.05125499604*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*0.2983805661*0.2983805661/2.801278682*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.D12" -length 0.2467904078 

Quadrupole -name "CR1.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.66922795*$e0] -e0 $e0 

Bpm -name "BPM13" -length 0
Drift -name "CR1.DBA.D11B1" -length 0.3133083062 

Multipole -name "CR1.DBA.SD0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR1.DBA.DM0" -length 0.1 

Quadrupole -name "CR1.DBA.QF1" -synrad $quad_synrad -length 0.4 -strength [expr -0.664141578*$e0] -e0 $e0 

Bpm -name "BPM14" -length 0
Drift -name "CR1.DBA.DM1_A" -length 1.106904053 

Multipole -name "CR1.DBA.SNCH1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 4.557288132*$e0] -e0 $e0 

Drift -name "CR1.DBA.DM1_B" -length 0.1069040525 

Quadrupole -name "CR1.DBA.QF2" -synrad $quad_synrad -length 0.4 -strength [expr 0.5637241356*$e0] -e0 $e0 

Bpm -name "BPM15" -length 0
Drift -name "CR1.DBA.DM2" -length 1.099116763 

Multipole -name "CR1.DBA.SD0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR1.DBA.DMSEXT1" -length 0.1 

Quadrupole -name "CR1.DBA.QF3" -synrad $quad_synrad -length 0.4 -strength [expr -0.6662844456*$e0] -e0 $e0 

Bpm -name "BPM16" -length 0
Drift -name "CR1.DBA.DM3" -length 0.2563831229 

Drift -name "CR1.DBA.DMSEXT1" -length 0.1 

Drift -name "CR1.DBA.D11B2" -length 0.1489315367 

Quadrupole -name "CR1.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.66922795*$e0] -e0 $e0 

Bpm -name "BPM17" -length 0
Drift -name "CR1.DBA.D12" -length 0.2467904078 

Sbend -name "CR1.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -six_dim 1 -e0 $e0 -K [expr 0.05125499604*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*0.2983805661*0.2983805661/2.801278682*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.DS1C_A" -length 0.2929622945 

Multipole -name "CR1.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 3.839149563*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS1C_B" -length 0.1 

Quadrupole -name "CR1.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -0.450384*$e0] -e0 $e0 

Bpm -name "BPM18" -length 0
Drift -name "CR1.DBA.DS2C" -length 0.5664710132 

Sbend -name "CR1.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -six_dim 1 -e0 $e0 -K [expr 0.1995945832*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.DS3C_A" -length 0.1426363128 

Multipole -name "CR1.DBA.SN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.022314728*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS3C_B" -length 0.1426363128 

Quadrupole -name "CR1.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr 0.2323546236*$e0] -e0 $e0 

Bpm -name "BPM19" -length 0
Quadrupole -name "CR1.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr 0.2323546236*$e0] -e0 $e0 

Bpm -name "BPM20" -length 0
Drift -name "CR1.DBA.DS3C_B" -length 0.1426363128 

Multipole -name "CR1.DBA.SNN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS3C_A" -length 0.1426363128 

Sbend -name "CR1.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -six_dim 1 -e0 $e0 -K [expr 0.1995945832*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.DS2C" -length 0.5664710132 

Quadrupole -name "CR1.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -0.450384*$e0] -e0 $e0 

Bpm -name "BPM21" -length 0
Drift -name "CR1.DBA.DS1C_B" -length 0.1 

Multipole -name "CR1.DBA.SN0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.037095359*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS1C_A" -length 0.2929622945 

Sbend -name "CR1.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -six_dim 1 -e0 $e0 -K [expr 0.05125499604*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*0.2983805661*0.2983805661/2.801278682*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.D12" -length 0.2467904078 

Quadrupole -name "CR1.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.66922795*$e0] -e0 $e0 

Bpm -name "BPM22" -length 0
Drift -name "CR1.DBA.D11B" -length 0.1989315367 

Drift -name "CR1.DBA.D11A" -length 0.2082887778 

Quadrupole -name "CR1.DBA.QI1" -synrad $quad_synrad -length 0.4 -strength [expr -0.3358875195*$e0] -e0 $e0 

Bpm -name "BPM23" -length 0
Drift -name "CR1.DBA.MCELL" -length 0 

Drift -name "CR1MDBACELLEND" -length 0 

Drift -name "CR1.DTR1A" -length 0.35 

Multipole -name "CR1.SXTR1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -12.2224*$e0] -e0 $e0 

Drift -name "CR1.DTR1B" -length 0.15 

Quadrupole -name "CR1.QTR1" -synrad $quad_synrad -length 0.5 -strength [expr -0.7495203095*$e0] -e0 $e0 

Bpm -name "BPM24" -length 0
Drift -name "CR1.DTR2" -length 1.2 

Quadrupole -name "CR1.QTR2" -synrad $quad_synrad -length 0.5 -strength [expr 0.7910425825*$e0] -e0 $e0 

Bpm -name "BPM25" -length 0
Drift -name "CR1.DTR3A" -length 0.45 

Multipole -name "CR1.SXTR3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -29.7172*$e0] -e0 $e0 

Drift -name "CR1.DTR3B" -length 0.45 

Quadrupole -name "CR1.QTR3" -synrad $quad_synrad -length 0.5 -strength [expr -0.949250117*$e0] -e0 $e0 

Bpm -name "BPM26" -length 0
Drift -name "CR1.DTR4" -length 0.6 

Quadrupole -name "CR1.QTR4" -synrad $quad_synrad -length 0.5 -strength [expr 0.2822164798*$e0] -e0 $e0 

Bpm -name "BPM27" -length 0
Drift -name "CR1.DTR5A" -length 0.75 

Multipole -name "CR1.SXTR5" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 8.54408*$e0] -e0 $e0 

Drift -name "CR1.DTR5B" -length 0.35 

Drift -name "CR1.DTR5B" -length 0.35 

Multipole -name "CR1.SXTR5" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 8.54408*$e0] -e0 $e0 

Drift -name "CR1.DTR5A" -length 0.75 

Quadrupole -name "CR1.QTR4" -synrad $quad_synrad -length 0.5 -strength [expr 0.2822164798*$e0] -e0 $e0 

Bpm -name "BPM28" -length 0
Drift -name "CR1.DTR4" -length 0.6 

Quadrupole -name "CR1.QTR3" -synrad $quad_synrad -length 0.5 -strength [expr -0.949250117*$e0] -e0 $e0 

Bpm -name "BPM29" -length 0
Drift -name "CR1.DTR3B" -length 0.45 

Multipole -name "CR1.SXTR3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -29.7172*$e0] -e0 $e0 

Drift -name "CR1.DTR3A" -length 0.45 

Quadrupole -name "CR1.QTR2" -synrad $quad_synrad -length 0.5 -strength [expr 0.7910425825*$e0] -e0 $e0 

Bpm -name "BPM30" -length 0
Drift -name "CR1.DTR2" -length 1.2 

Quadrupole -name "CR1.QTR1" -synrad $quad_synrad -length 0.5 -strength [expr -0.7495203095*$e0] -e0 $e0 

Bpm -name "BPM31" -length 0
Drift -name "CR1.DTR1B" -length 0.15 

Multipole -name "CR1.SXTR1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -12.2224*$e0] -e0 $e0 

Drift -name "CR1.DTR1A" -length 0.35 

Drift -name "CR1MDBACELLSTART" -length 0 

Drift -name "CR1.DBA.MCELL" -length 0 

Quadrupole -name "CR1.DBA.QI1" -synrad $quad_synrad -length 0.4 -strength [expr -0.3358875195*$e0] -e0 $e0 

Bpm -name "BPM32" -length 0
Drift -name "CR1.DBA.D11A" -length 0.2082887778 

Drift -name "CR1.DBA.D11B" -length 0.1989315367 

Quadrupole -name "CR1.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.66922795*$e0] -e0 $e0 

Bpm -name "BPM33" -length 0
Drift -name "CR1.DBA.D12" -length 0.2467904078 

Sbend -name "CR1.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -six_dim 1 -e0 $e0 -K [expr 0.05125499604*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*0.2983805661*0.2983805661/2.801278682*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.DS1C_A" -length 0.2929622945 

Multipole -name "CR1.DBA.SN0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.037095359*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS1C_B" -length 0.1 

Drift -name "CR1.DBA.HCM0" -length 0 

Quadrupole -name "CR1.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -0.450384*$e0] -e0 $e0 

Bpm -name "BPM34" -length 0
Drift -name "CR1.DBA.DS2C" -length 0.5664710132 

Sbend -name "CR1.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -six_dim 1 -e0 $e0 -K [expr 0.1995945832*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.HBM" -length 0 

Drift -name "CR1.DBA.DS3C_A" -length 0.1426363128 

Multipole -name "CR1.DBA.SN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.022314728*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS3C_B" -length 0.1426363128 

Quadrupole -name "CR1.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr 0.2323546236*$e0] -e0 $e0 

Bpm -name "BPM35" -length 0
Quadrupole -name "CR1.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr 0.2323546236*$e0] -e0 $e0 

Bpm -name "BPM36" -length 0
Drift -name "CR1.DBA.DS3C_B" -length 0.1426363128 

Multipole -name "CR1.DBA.SNN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS3C_A" -length 0.1426363128 

Drift -name "CR1.DBA.HBM2" -length 0 

Sbend -name "CR1.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -six_dim 1 -e0 $e0 -K [expr 0.1995945832*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.DS2C" -length 0.5664710132 

Quadrupole -name "CR1.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -0.450384*$e0] -e0 $e0 

Bpm -name "BPM37" -length 0
Drift -name "CR1.DBA.HCM" -length 0 

Drift -name "CR1.DBA.DS1C_B" -length 0.1 

Multipole -name "CR1.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 3.839149563*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS1C_A" -length 0.2929622945 

Sbend -name "CR1.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -six_dim 1 -e0 $e0 -K [expr 0.05125499604*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*0.2983805661*0.2983805661/2.801278682*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.D12" -length 0.2467904078 

Quadrupole -name "CR1.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.66922795*$e0] -e0 $e0 

Bpm -name "BPM38" -length 0
Drift -name "CR1.DBA.D11B1" -length 0.3133083062 

Multipole -name "CR1.DBA.SD0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR1.DBA.DM0" -length 0.1 

Quadrupole -name "CR1.DBA.QF1" -synrad $quad_synrad -length 0.4 -strength [expr -0.664141578*$e0] -e0 $e0 

Bpm -name "BPM39" -length 0
Drift -name "CR1.DBA.DM1_A" -length 1.106904053 

Multipole -name "CR1.DBA.SNCH1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 4.557288132*$e0] -e0 $e0 

Drift -name "CR1.DBA.DM1_B" -length 0.1069040525 

Quadrupole -name "CR1.DBA.QF2" -synrad $quad_synrad -length 0.4 -strength [expr 0.5637241356*$e0] -e0 $e0 

Bpm -name "BPM40" -length 0
Drift -name "CR1.DBA.DM2" -length 1.099116763 

Multipole -name "CR1.DBA.SD0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR1.DBA.DMSEXT1" -length 0.1 

Quadrupole -name "CR1.DBA.QF3" -synrad $quad_synrad -length 0.4 -strength [expr -0.6662844456*$e0] -e0 $e0 

Bpm -name "BPM41" -length 0
Drift -name "CR1.DBA.DM3" -length 0.2563831229 

Drift -name "CR1.DBA.DMSEXT1" -length 0.1 

Drift -name "CR1.DBA.D11B2" -length 0.1489315367 

Quadrupole -name "CR1.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.66922795*$e0] -e0 $e0 

Bpm -name "BPM42" -length 0
Drift -name "CR1.DBA.D12" -length 0.2467904078 

Sbend -name "CR1.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -six_dim 1 -e0 $e0 -K [expr 0.05125499604*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*0.2983805661*0.2983805661/2.801278682*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.DS1C_A" -length 0.2929622945 

Multipole -name "CR1.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 3.839149563*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS1C_B" -length 0.1 

Quadrupole -name "CR1.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -0.450384*$e0] -e0 $e0 

Bpm -name "BPM43" -length 0
Drift -name "CR1.DBA.DS2C" -length 0.5664710132 

Sbend -name "CR1.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -six_dim 1 -e0 $e0 -K [expr 0.1995945832*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.DS3C_A" -length 0.1426363128 

Multipole -name "CR1.DBA.SN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.022314728*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS3C_B" -length 0.1426363128 

Quadrupole -name "CR1.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr 0.2323546236*$e0] -e0 $e0 

Bpm -name "BPM44" -length 0
Quadrupole -name "CR1.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr 0.2323546236*$e0] -e0 $e0 

Bpm -name "BPM45" -length 0
Drift -name "CR1.DBA.DS3C_B" -length 0.1426363128 

Multipole -name "CR1.DBA.SNN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS3C_A" -length 0.1426363128 

Sbend -name "CR1.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -six_dim 1 -e0 $e0 -K [expr 0.1995945832*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.DS2C" -length 0.5664710132 

Quadrupole -name "CR1.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -0.450384*$e0] -e0 $e0 

Bpm -name "BPM46" -length 0
Drift -name "CR1.DBA.DS1C_B" -length 0.1 

Multipole -name "CR1.DBA.SN0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.037095359*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS1C_A" -length 0.2929622945 

Sbend -name "CR1.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -six_dim 1 -e0 $e0 -K [expr 0.05125499604*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*0.2983805661*0.2983805661/2.801278682*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.D12" -length 0.2467904078 

Quadrupole -name "CR1.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.66922795*$e0] -e0 $e0 

Bpm -name "BPM47" -length 0
Drift -name "CR1.DBA.D11B" -length 0.1989315367 

Drift -name "CR1.DBA.D11A" -length 0.2082887778 

Quadrupole -name "CR1.DBA.QI1" -synrad $quad_synrad -length 0.4 -strength [expr -0.3358875195*$e0] -e0 $e0 

Bpm -name "BPM48" -length 0
Drift -name "CR1.DBA.MCELL" -length 0 

Drift -name "CR1MDBACELLEND" -length 0 

Drift -name "CR1.DS1C_A" -length 1.95 

Multipole -name "CR1.SX1CC" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -3.54141*$e0] -e0 $e0 

Drift -name "CR1.DS1C_B" -length 0.95 

Quadrupole -name "CR1.QS1C" -synrad $quad_synrad -length 0.5 -strength [expr -0.4417115388*$e0] -e0 $e0 

Bpm -name "BPM49" -length 0
Drift -name "CR1.DS2C" -length 0.5 

Quadrupole -name "CR1.QS2C" -synrad $quad_synrad -length 0.5 -strength [expr 0*$e0] -e0 $e0 

Bpm -name "BPM50" -length 0
Drift -name "CR1.DS3C_A" -length 0.4 

Multipole -name "CR1.SX3CC" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.50153*$e0] -e0 $e0 

Drift -name "CR1.DS3C_B" -length 0.4 

Quadrupole -name "CR1.QS3C" -synrad $quad_synrad -length 0.5 -strength [expr 0.6263995*$e0] -e0 $e0 

Bpm -name "BPM51" -length 0
Drift -name "CR1.DS4C" -length 0.5 

Quadrupole -name "CR1.QS4C" -synrad $quad_synrad -length 0.5 -strength [expr 0*$e0] -e0 $e0 

Bpm -name "BPM52" -length 0
Drift -name "CR1.DS5C" -length 0.5 

Quadrupole -name "CR1.QS5C" -synrad $quad_synrad -length 0.5 -strength [expr -0.880414135*$e0] -e0 $e0 

Bpm -name "BPM53" -length 0
Drift -name "CR1.DS6C" -length 0.5 

Drift -name "CR1.WIG1" -length 0.8 

Drift -name "CR1.WIG2" -length 0.8 

Drift -name "CR1.DS6C" -length 0.5 

Quadrupole -name "CR1.QS5C" -synrad $quad_synrad -length 0.5 -strength [expr -0.880414135*$e0] -e0 $e0 

Bpm -name "BPM54" -length 0
Drift -name "CR1.DS5C" -length 0.5 

Quadrupole -name "CR1.QS4C" -synrad $quad_synrad -length 0.5 -strength [expr 0*$e0] -e0 $e0 

Bpm -name "BPM55" -length 0
Drift -name "CR1.DS4C" -length 0.5 

Quadrupole -name "CR1.QS3C" -synrad $quad_synrad -length 0.5 -strength [expr 0.6263995*$e0] -e0 $e0 

Bpm -name "BPM56" -length 0
Drift -name "CR1.DS3C_B" -length 0.4 

Multipole -name "CR1.SX3CC" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.50153*$e0] -e0 $e0 

Drift -name "CR1.DS3C_A" -length 0.4 

Quadrupole -name "CR1.QS2C" -synrad $quad_synrad -length 0.5 -strength [expr 0*$e0] -e0 $e0 

Bpm -name "BPM57" -length 0
Drift -name "CR1.DS2C" -length 0.5 

Quadrupole -name "CR1.QS1C" -synrad $quad_synrad -length 0.5 -strength [expr -0.4417115388*$e0] -e0 $e0 

Bpm -name "BPM58" -length 0
Drift -name "CR1.DS1C_B" -length 0.95 

Multipole -name "CR1.SX1CC" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -3.54141*$e0] -e0 $e0 

Drift -name "CR1.DS1C_A" -length 1.95 

Drift -name "CR1MDBACELLSTART" -length 0 

Drift -name "CR1.DBA.MCELL" -length 0 

Quadrupole -name "CR1.DBA.QI1" -synrad $quad_synrad -length 0.4 -strength [expr -0.3358875195*$e0] -e0 $e0 

Bpm -name "BPM59" -length 0
Drift -name "CR1.DBA.D11A" -length 0.2082887778 

Drift -name "CR1.DBA.D11B" -length 0.1989315367 

Quadrupole -name "CR1.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.66922795*$e0] -e0 $e0 

Bpm -name "BPM60" -length 0
Drift -name "CR1.DBA.D12" -length 0.2467904078 

Sbend -name "CR1.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -six_dim 1 -e0 $e0 -K [expr 0.05125499604*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*0.2983805661*0.2983805661/2.801278682*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.DS1C_A" -length 0.2929622945 

Multipole -name "CR1.DBA.SN0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.037095359*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS1C_B" -length 0.1 

Drift -name "CR1.DBA.HCM0" -length 0 

Quadrupole -name "CR1.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -0.450384*$e0] -e0 $e0 

Bpm -name "BPM61" -length 0
Drift -name "CR1.DBA.DS2C" -length 0.5664710132 

Sbend -name "CR1.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -six_dim 1 -e0 $e0 -K [expr 0.1995945832*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.HBM" -length 0 

Drift -name "CR1.DBA.DS3C_A" -length 0.1426363128 

Multipole -name "CR1.DBA.SN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.022314728*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS3C_B" -length 0.1426363128 

Quadrupole -name "CR1.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr 0.2323546236*$e0] -e0 $e0 

Bpm -name "BPM62" -length 0
Quadrupole -name "CR1.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr 0.2323546236*$e0] -e0 $e0 

Bpm -name "BPM63" -length 0
Drift -name "CR1.DBA.DS3C_B" -length 0.1426363128 

Multipole -name "CR1.DBA.SNN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS3C_A" -length 0.1426363128 

Drift -name "CR1.DBA.HBM2" -length 0 

Sbend -name "CR1.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -six_dim 1 -e0 $e0 -K [expr 0.1995945832*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.DS2C" -length 0.5664710132 

Quadrupole -name "CR1.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -0.450384*$e0] -e0 $e0 

Bpm -name "BPM64" -length 0
Drift -name "CR1.DBA.HCM" -length 0 

Drift -name "CR1.DBA.DS1C_B" -length 0.1 

Multipole -name "CR1.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 3.839149563*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS1C_A" -length 0.2929622945 

Sbend -name "CR1.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -six_dim 1 -e0 $e0 -K [expr 0.05125499604*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*0.2983805661*0.2983805661/2.801278682*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.D12" -length 0.2467904078 

Quadrupole -name "CR1.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.66922795*$e0] -e0 $e0 

Bpm -name "BPM65" -length 0
Drift -name "CR1.DBA.D11B1" -length 0.3133083062 

Multipole -name "CR1.DBA.SD0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR1.DBA.DM0" -length 0.1 

Quadrupole -name "CR1.DBA.QF1" -synrad $quad_synrad -length 0.4 -strength [expr -0.664141578*$e0] -e0 $e0 

Bpm -name "BPM66" -length 0
Drift -name "CR1.DBA.DM1_A" -length 1.106904053 

Multipole -name "CR1.DBA.SNCH1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 4.557288132*$e0] -e0 $e0 

Drift -name "CR1.DBA.DM1_B" -length 0.1069040525 

Quadrupole -name "CR1.DBA.QF2" -synrad $quad_synrad -length 0.4 -strength [expr 0.5637241356*$e0] -e0 $e0 

Bpm -name "BPM67" -length 0
Drift -name "CR1.DBA.DM2" -length 1.099116763 

Multipole -name "CR1.DBA.SD0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR1.DBA.DMSEXT1" -length 0.1 

Quadrupole -name "CR1.DBA.QF3" -synrad $quad_synrad -length 0.4 -strength [expr -0.6662844456*$e0] -e0 $e0 

Bpm -name "BPM68" -length 0
Drift -name "CR1.DBA.DM3" -length 0.2563831229 

Drift -name "CR1.DBA.DMSEXT1" -length 0.1 

Drift -name "CR1.DBA.D11B2" -length 0.1489315367 

Quadrupole -name "CR1.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.66922795*$e0] -e0 $e0 

Bpm -name "BPM69" -length 0
Drift -name "CR1.DBA.D12" -length 0.2467904078 

Sbend -name "CR1.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -six_dim 1 -e0 $e0 -K [expr 0.05125499604*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*0.2983805661*0.2983805661/2.801278682*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.DS1C_A" -length 0.2929622945 

Multipole -name "CR1.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 3.839149563*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS1C_B" -length 0.1 

Quadrupole -name "CR1.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -0.450384*$e0] -e0 $e0 

Bpm -name "BPM70" -length 0
Drift -name "CR1.DBA.DS2C" -length 0.5664710132 

Sbend -name "CR1.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -six_dim 1 -e0 $e0 -K [expr 0.1995945832*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.DS3C_A" -length 0.1426363128 

Multipole -name "CR1.DBA.SN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.022314728*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS3C_B" -length 0.1426363128 

Quadrupole -name "CR1.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr 0.2323546236*$e0] -e0 $e0 

Bpm -name "BPM71" -length 0
Quadrupole -name "CR1.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr 0.2323546236*$e0] -e0 $e0 

Bpm -name "BPM72" -length 0
Drift -name "CR1.DBA.DS3C_B" -length 0.1426363128 

Multipole -name "CR1.DBA.SNN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS3C_A" -length 0.1426363128 

Sbend -name "CR1.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -six_dim 1 -e0 $e0 -K [expr 0.1995945832*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.DS2C" -length 0.5664710132 

Quadrupole -name "CR1.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -0.450384*$e0] -e0 $e0 

Bpm -name "BPM73" -length 0
Drift -name "CR1.DBA.DS1C_B" -length 0.1 

Multipole -name "CR1.DBA.SN0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.037095359*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS1C_A" -length 0.2929622945 

Sbend -name "CR1.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -six_dim 1 -e0 $e0 -K [expr 0.05125499604*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*0.2983805661*0.2983805661/2.801278682*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.D12" -length 0.2467904078 

Quadrupole -name "CR1.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.66922795*$e0] -e0 $e0 

Bpm -name "BPM74" -length 0
Drift -name "CR1.DBA.D11B" -length 0.1989315367 

Drift -name "CR1.DBA.D11A" -length 0.2082887778 

Quadrupole -name "CR1.DBA.QI1" -synrad $quad_synrad -length 0.4 -strength [expr -0.3358875195*$e0] -e0 $e0 

Bpm -name "BPM75" -length 0
Drift -name "CR1.DBA.MCELL" -length 0 

Drift -name "CR1MDBACELLEND" -length 0 

Drift -name "CR1.DL0IN" -length 1.9898566 

Quadrupole -name "CR1.QL1IN" -synrad $quad_synrad -length 0.5 -strength [expr -0.7229782885*$e0] -e0 $e0 

Bpm -name "BPM76" -length 0
Drift -name "CR1.DL1IN" -length 0.3098482 

Quadrupole -name "CR1.QL2IN" -synrad $quad_synrad -length 0.5 -strength [expr 1.191142713*$e0] -e0 $e0 

Bpm -name "BPM77" -length 0
Drift -name "CR1.DL2IN" -length 0.3010372 

Quadrupole -name "CR1.QL3IN" -synrad $quad_synrad -length 0.5 -strength [expr -0.4937532183*$e0] -e0 $e0 

Bpm -name "BPM78" -length 0
Drift -name "CR1.DL3IN" -length 1.099258 

Drift -name "CR1.DRRF" -length 0.4 

Drift -name "CR1.DRRF1" -length 0.7736769688 

Drift -name "CR1.DRRF1" -length 0.7736769688 

Drift -name "CR1.DRRF2" -length 0.55265 

Drift -name "CR1.DL4IN" -length 0.349996062 

Quadrupole -name "CR1.QL4IN" -synrad $quad_synrad -length 0.5 -strength [expr 0.555285*$e0] -e0 $e0 

Bpm -name "BPM79" -length 0
Drift -name "CR1.DL5IN" -length 1 

Quadrupole -name "CR1.QL5IN" -synrad $quad_synrad -length 0.5 -strength [expr -0.8298265*$e0] -e0 $e0 

Bpm -name "BPM80" -length 0
Drift -name "CR1.DL6IN" -length 0.553 

Quadrupole -name "CR1.QL6IN" -synrad $quad_synrad -length 0.25 -strength [expr 0.496129*$e0] -e0 $e0 

Bpm -name "BPM81" -length 0
Drift -name "CR1.DL7IN" -length 0.5 
Drift -name "CR1.EJECTION" -length 0 

Drift -name "CR1.SEPTUM" -length 1 
Drift -name "CR1.SEPTUM" -length 1 

Drift -name "CR1.DL7IN" -length 0.5 

Quadrupole -name "CR1.QL6IN" -synrad $quad_synrad -length 0.25 -strength [expr 0.496129*$e0] -e0 $e0 

Bpm -name "BPM82" -length 0
Drift -name "CR1.DL6IN" -length 0.553 

Quadrupole -name "CR1.QL5IN" -synrad $quad_synrad -length 0.5 -strength [expr -0.8298265*$e0] -e0 $e0 

Bpm -name "BPM83" -length 0
Drift -name "CR1.DL5IN" -length 1 

Quadrupole -name "CR1.QL4IN" -synrad $quad_synrad -length 0.5 -strength [expr 0.555285*$e0] -e0 $e0 

Bpm -name "BPM84" -length 0
Drift -name "CR1.DL4IN" -length 0.349996062 

Drift -name "CR1.DRRF2" -length 0.55265 

Drift -name "CR1.DRRF1" -length 0.7736769688 

Drift -name "CR1.DRRF1" -length 0.7736769688 

Drift -name "CR1.DRRF" -length 0.4 

Drift -name "CR1.DL3IN" -length 1.099258 

Quadrupole -name "CR1.QL3IN" -synrad $quad_synrad -length 0.5 -strength [expr -0.4937532183*$e0] -e0 $e0 

Bpm -name "BPM85" -length 0
Drift -name "CR1.DL2IN" -length 0.3010372 

Quadrupole -name "CR1.QL2IN" -synrad $quad_synrad -length 0.5 -strength [expr 1.191142713*$e0] -e0 $e0 

Bpm -name "BPM86" -length 0
Drift -name "CR1.DL1IN" -length 0.3098482 

Quadrupole -name "CR1.QL1IN" -synrad $quad_synrad -length 0.5 -strength [expr -0.7229782885*$e0] -e0 $e0 

Bpm -name "BPM87" -length 0
Drift -name "CR1.DL0IN" -length 1.9898566 

Drift -name "CR1MDBACELLSTART" -length 0 

Drift -name "CR1.DBA.MCELL" -length 0 

Quadrupole -name "CR1.DBA.QI1" -synrad $quad_synrad -length 0.4 -strength [expr -0.3358875195*$e0] -e0 $e0 

Bpm -name "BPM88" -length 0
Drift -name "CR1.DBA.D11A" -length 0.2082887778 

Drift -name "CR1.DBA.D11B" -length 0.1989315367 

Quadrupole -name "CR1.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.66922795*$e0] -e0 $e0 

Bpm -name "BPM89" -length 0
Drift -name "CR1.DBA.D12" -length 0.2467904078 

Sbend -name "CR1.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -six_dim 1 -e0 $e0 -K [expr 0.05125499604*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*0.2983805661*0.2983805661/2.801278682*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.DS1C_A" -length 0.2929622945 

Multipole -name "CR1.DBA.SN0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.037095359*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS1C_B" -length 0.1 

Drift -name "CR1.DBA.HCM0" -length 0 

Quadrupole -name "CR1.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -0.450384*$e0] -e0 $e0 

Bpm -name "BPM90" -length 0
Drift -name "CR1.DBA.DS2C" -length 0.5664710132 

Sbend -name "CR1.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -six_dim 1 -e0 $e0 -K [expr 0.1995945832*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.HBM" -length 0 

Drift -name "CR1.DBA.DS3C_A" -length 0.1426363128 

Multipole -name "CR1.DBA.SN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.022314728*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS3C_B" -length 0.1426363128 

Quadrupole -name "CR1.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr 0.2323546236*$e0] -e0 $e0 

Bpm -name "BPM91" -length 0
Quadrupole -name "CR1.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr 0.2323546236*$e0] -e0 $e0 

Bpm -name "BPM92" -length 0
Drift -name "CR1.DBA.DS3C_B" -length 0.1426363128 

Multipole -name "CR1.DBA.SNN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS3C_A" -length 0.1426363128 

Drift -name "CR1.DBA.HBM2" -length 0 

Sbend -name "CR1.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -six_dim 1 -e0 $e0 -K [expr 0.1995945832*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.DS2C" -length 0.5664710132 

Quadrupole -name "CR1.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -0.450384*$e0] -e0 $e0 

Bpm -name "BPM93" -length 0
Drift -name "CR1.DBA.HCM" -length 0 

Drift -name "CR1.DBA.DS1C_B" -length 0.1 

Multipole -name "CR1.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 3.839149563*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS1C_A" -length 0.2929622945 

Sbend -name "CR1.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -six_dim 1 -e0 $e0 -K [expr 0.05125499604*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*0.2983805661*0.2983805661/2.801278682*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.D12" -length 0.2467904078 

Quadrupole -name "CR1.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.66922795*$e0] -e0 $e0 

Bpm -name "BPM94" -length 0
Drift -name "CR1.DBA.D11B1" -length 0.3133083062 

Multipole -name "CR1.DBA.SD0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR1.DBA.DM0" -length 0.1 

Quadrupole -name "CR1.DBA.QF1" -synrad $quad_synrad -length 0.4 -strength [expr -0.664141578*$e0] -e0 $e0 

Bpm -name "BPM95" -length 0
Drift -name "CR1.DBA.DM1_A" -length 1.106904053 

Multipole -name "CR1.DBA.SNCH1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 4.557288132*$e0] -e0 $e0 

Drift -name "CR1.DBA.DM1_B" -length 0.1069040525 

Quadrupole -name "CR1.DBA.QF2" -synrad $quad_synrad -length 0.4 -strength [expr 0.5637241356*$e0] -e0 $e0 

Bpm -name "BPM96" -length 0
Drift -name "CR1.DBA.DM2" -length 1.099116763 

Multipole -name "CR1.DBA.SD0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR1.DBA.DMSEXT1" -length 0.1 

Quadrupole -name "CR1.DBA.QF3" -synrad $quad_synrad -length 0.4 -strength [expr -0.6662844456*$e0] -e0 $e0 

Bpm -name "BPM97" -length 0
Drift -name "CR1.DBA.DM3" -length 0.2563831229 

Drift -name "CR1.DBA.DMSEXT1" -length 0.1 

Drift -name "CR1.DBA.D11B2" -length 0.1489315367 

Quadrupole -name "CR1.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.66922795*$e0] -e0 $e0 

Bpm -name "BPM98" -length 0
Drift -name "CR1.DBA.D12" -length 0.2467904078 

Sbend -name "CR1.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -six_dim 1 -e0 $e0 -K [expr 0.05125499604*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*0.2983805661*0.2983805661/2.801278682*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.DS1C_A" -length 0.2929622945 

Multipole -name "CR1.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 3.839149563*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS1C_B" -length 0.1 

Quadrupole -name "CR1.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -0.450384*$e0] -e0 $e0 

Bpm -name "BPM99" -length 0
Drift -name "CR1.DBA.DS2C" -length 0.5664710132 

Sbend -name "CR1.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -six_dim 1 -e0 $e0 -K [expr 0.1995945832*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.DS3C_A" -length 0.1426363128 

Multipole -name "CR1.DBA.SN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.022314728*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS3C_B" -length 0.1426363128 

Quadrupole -name "CR1.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr 0.2323546236*$e0] -e0 $e0 

Bpm -name "BPM100" -length 0
Quadrupole -name "CR1.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr 0.2323546236*$e0] -e0 $e0 

Bpm -name "BPM101" -length 0
Drift -name "CR1.DBA.DS3C_B" -length 0.1426363128 

Multipole -name "CR1.DBA.SNN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS3C_A" -length 0.1426363128 

Sbend -name "CR1.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -six_dim 1 -e0 $e0 -K [expr 0.1995945832*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.DS2C" -length 0.5664710132 

Quadrupole -name "CR1.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -0.450384*$e0] -e0 $e0 

Bpm -name "BPM102" -length 0
Drift -name "CR1.DBA.DS1C_B" -length 0.1 

Multipole -name "CR1.DBA.SN0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.037095359*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS1C_A" -length 0.2929622945 

Sbend -name "CR1.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -six_dim 1 -e0 $e0 -K [expr 0.05125499604*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*0.2983805661*0.2983805661/2.801278682*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.D12" -length 0.2467904078 

Quadrupole -name "CR1.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.66922795*$e0] -e0 $e0 

Bpm -name "BPM103" -length 0
Drift -name "CR1.DBA.D11B" -length 0.1989315367 

Drift -name "CR1.DBA.D11A" -length 0.2082887778 

Quadrupole -name "CR1.DBA.QI1" -synrad $quad_synrad -length 0.4 -strength [expr -0.3358875195*$e0] -e0 $e0 

Bpm -name "BPM104" -length 0
Drift -name "CR1.DBA.MCELL" -length 0 

Drift -name "CR1MDBACELLEND" -length 0 

Drift -name "CR1.DTR1A" -length 0.35 

Multipole -name "CR1.SXTR1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -12.2224*$e0] -e0 $e0 

Drift -name "CR1.DTR1B" -length 0.15 

Quadrupole -name "CR1.QTR1" -synrad $quad_synrad -length 0.5 -strength [expr -0.7495203095*$e0] -e0 $e0 

Bpm -name "BPM105" -length 0
Drift -name "CR1.DTR2" -length 1.2 

Quadrupole -name "CR1.QTR2" -synrad $quad_synrad -length 0.5 -strength [expr 0.7910425825*$e0] -e0 $e0 

Bpm -name "BPM106" -length 0
Drift -name "CR1.DTR3A" -length 0.45 

Multipole -name "CR1.SXTR3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -29.7172*$e0] -e0 $e0 

Drift -name "CR1.DTR3B" -length 0.45 

Quadrupole -name "CR1.QTR3" -synrad $quad_synrad -length 0.5 -strength [expr -0.949250117*$e0] -e0 $e0 

Bpm -name "BPM107" -length 0
Drift -name "CR1.DTR4" -length 0.6 

Quadrupole -name "CR1.QTR4" -synrad $quad_synrad -length 0.5 -strength [expr 0.2822164798*$e0] -e0 $e0 

Bpm -name "BPM108" -length 0
Drift -name "CR1.DTR5A" -length 0.75 

Multipole -name "CR1.SXTR5" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 8.54408*$e0] -e0 $e0 

Drift -name "CR1.DTR5B" -length 0.35 

Drift -name "CR1.DTR5B" -length 0.35 

Multipole -name "CR1.SXTR5" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 8.54408*$e0] -e0 $e0 

Drift -name "CR1.DTR5A" -length 0.75 

Quadrupole -name "CR1.QTR4" -synrad $quad_synrad -length 0.5 -strength [expr 0.2822164798*$e0] -e0 $e0 

Bpm -name "BPM109" -length 0
Drift -name "CR1.DTR4" -length 0.6 

Quadrupole -name "CR1.QTR3" -synrad $quad_synrad -length 0.5 -strength [expr -0.949250117*$e0] -e0 $e0 

Bpm -name "BPM110" -length 0
Drift -name "CR1.DTR3B" -length 0.45 

Multipole -name "CR1.SXTR3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -29.7172*$e0] -e0 $e0 

Drift -name "CR1.DTR3A" -length 0.45 

Quadrupole -name "CR1.QTR2" -synrad $quad_synrad -length 0.5 -strength [expr 0.7910425825*$e0] -e0 $e0 

Bpm -name "BPM111" -length 0
Drift -name "CR1.DTR2" -length 1.2 

Quadrupole -name "CR1.QTR1" -synrad $quad_synrad -length 0.5 -strength [expr -0.7495203095*$e0] -e0 $e0 

Bpm -name "BPM112" -length 0
Drift -name "CR1.DTR1B" -length 0.15 

Multipole -name "CR1.SXTR1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -12.2224*$e0] -e0 $e0 

Drift -name "CR1.DTR1A" -length 0.35 

Drift -name "CR1MDBACELLSTART" -length 0 

Drift -name "CR1.DBA.MCELL" -length 0 

Quadrupole -name "CR1.DBA.QI1" -synrad $quad_synrad -length 0.4 -strength [expr -0.3358875195*$e0] -e0 $e0 

Bpm -name "BPM113" -length 0
Drift -name "CR1.DBA.D11A" -length 0.2082887778 

Drift -name "CR1.DBA.D11B" -length 0.1989315367 

Quadrupole -name "CR1.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.66922795*$e0] -e0 $e0 

Bpm -name "BPM114" -length 0
Drift -name "CR1.DBA.D12" -length 0.2467904078 

Sbend -name "CR1.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -six_dim 1 -e0 $e0 -K [expr 0.05125499604*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*0.2983805661*0.2983805661/2.801278682*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.DS1C_A" -length 0.2929622945 

Multipole -name "CR1.DBA.SN0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.037095359*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS1C_B" -length 0.1 

Drift -name "CR1.DBA.HCM0" -length 0 

Quadrupole -name "CR1.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -0.450384*$e0] -e0 $e0 

Bpm -name "BPM115" -length 0
Drift -name "CR1.DBA.DS2C" -length 0.5664710132 

Sbend -name "CR1.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -six_dim 1 -e0 $e0 -K [expr 0.1995945832*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.HBM" -length 0 

Drift -name "CR1.DBA.DS3C_A" -length 0.1426363128 

Multipole -name "CR1.DBA.SN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.022314728*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS3C_B" -length 0.1426363128 

Quadrupole -name "CR1.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr 0.2323546236*$e0] -e0 $e0 

Bpm -name "BPM116" -length 0
Quadrupole -name "CR1.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr 0.2323546236*$e0] -e0 $e0 

Bpm -name "BPM117" -length 0
Drift -name "CR1.DBA.DS3C_B" -length 0.1426363128 

Multipole -name "CR1.DBA.SNN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS3C_A" -length 0.1426363128 

Drift -name "CR1.DBA.HBM2" -length 0 

Sbend -name "CR1.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -six_dim 1 -e0 $e0 -K [expr 0.1995945832*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.DS2C" -length 0.5664710132 

Quadrupole -name "CR1.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -0.450384*$e0] -e0 $e0 

Bpm -name "BPM118" -length 0
Drift -name "CR1.DBA.HCM" -length 0 

Drift -name "CR1.DBA.DS1C_B" -length 0.1 

Multipole -name "CR1.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 3.839149563*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS1C_A" -length 0.2929622945 

Sbend -name "CR1.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -six_dim 1 -e0 $e0 -K [expr 0.05125499604*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*0.2983805661*0.2983805661/2.801278682*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.D12" -length 0.2467904078 

Quadrupole -name "CR1.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.66922795*$e0] -e0 $e0 

Bpm -name "BPM119" -length 0
Drift -name "CR1.DBA.D11B1" -length 0.3133083062 

Multipole -name "CR1.DBA.SD0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR1.DBA.DM0" -length 0.1 

Quadrupole -name "CR1.DBA.QF1" -synrad $quad_synrad -length 0.4 -strength [expr -0.664141578*$e0] -e0 $e0 

Bpm -name "BPM120" -length 0
Drift -name "CR1.DBA.DM1_A" -length 1.106904053 

Multipole -name "CR1.DBA.SNCH1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 4.557288132*$e0] -e0 $e0 

Drift -name "CR1.DBA.DM1_B" -length 0.1069040525 

Quadrupole -name "CR1.DBA.QF2" -synrad $quad_synrad -length 0.4 -strength [expr 0.5637241356*$e0] -e0 $e0 

Bpm -name "BPM121" -length 0
Drift -name "CR1.DBA.DM2" -length 1.099116763 

Multipole -name "CR1.DBA.SD0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR1.DBA.DMSEXT1" -length 0.1 

Quadrupole -name "CR1.DBA.QF3" -synrad $quad_synrad -length 0.4 -strength [expr -0.6662844456*$e0] -e0 $e0 

Bpm -name "BPM122" -length 0
Drift -name "CR1.DBA.DM3" -length 0.2563831229 

Drift -name "CR1.DBA.DMSEXT1" -length 0.1 

Drift -name "CR1.DBA.D11B2" -length 0.1489315367 

Quadrupole -name "CR1.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.66922795*$e0] -e0 $e0 

Bpm -name "BPM123" -length 0
Drift -name "CR1.DBA.D12" -length 0.2467904078 

Sbend -name "CR1.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -six_dim 1 -e0 $e0 -K [expr 0.05125499604*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*0.2983805661*0.2983805661/2.801278682*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.DS1C_A" -length 0.2929622945 

Multipole -name "CR1.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 3.839149563*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS1C_B" -length 0.1 

Quadrupole -name "CR1.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -0.450384*$e0] -e0 $e0 

Bpm -name "BPM124" -length 0
Drift -name "CR1.DBA.DS2C" -length 0.5664710132 

Sbend -name "CR1.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -six_dim 1 -e0 $e0 -K [expr 0.1995945832*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.DS3C_A" -length 0.1426363128 

Multipole -name "CR1.DBA.SN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.022314728*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS3C_B" -length 0.1426363128 

Quadrupole -name "CR1.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr 0.2323546236*$e0] -e0 $e0 

Bpm -name "BPM125" -length 0
Quadrupole -name "CR1.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr 0.2323546236*$e0] -e0 $e0 

Bpm -name "BPM126" -length 0
Drift -name "CR1.DBA.DS3C_B" -length 0.1426363128 

Multipole -name "CR1.DBA.SNN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS3C_A" -length 0.1426363128 

Sbend -name "CR1.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -six_dim 1 -e0 $e0 -K [expr 0.1995945832*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.DS2C" -length 0.5664710132 

Quadrupole -name "CR1.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -0.450384*$e0] -e0 $e0 

Bpm -name "BPM127" -length 0
Drift -name "CR1.DBA.DS1C_B" -length 0.1 

Multipole -name "CR1.DBA.SN0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.037095359*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS1C_A" -length 0.2929622945 

Sbend -name "CR1.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -six_dim 1 -e0 $e0 -K [expr 0.05125499604*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*0.2983805661*0.2983805661/2.801278682*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.D12" -length 0.2467904078 

Quadrupole -name "CR1.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.66922795*$e0] -e0 $e0 

Bpm -name "BPM128" -length 0
Drift -name "CR1.DBA.D11B" -length 0.1989315367 

Drift -name "CR1.DBA.D11A" -length 0.2082887778 

Quadrupole -name "CR1.DBA.QI1" -synrad $quad_synrad -length 0.4 -strength [expr -0.3358875195*$e0] -e0 $e0 

Bpm -name "BPM129" -length 0
Drift -name "CR1.DBA.MCELL" -length 0 

Drift -name "CR1MDBACELLEND" -length 0 

Drift -name "CR1.DS1C_A" -length 1.95 

Multipole -name "CR1.SX1CC" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -3.54141*$e0] -e0 $e0 

Drift -name "CR1.DS1C_B" -length 0.95 

Quadrupole -name "CR1.QS1C" -synrad $quad_synrad -length 0.5 -strength [expr -0.4417115388*$e0] -e0 $e0 

Bpm -name "BPM130" -length 0
Drift -name "CR1.DS2C" -length 0.5 

Quadrupole -name "CR1.QS2C" -synrad $quad_synrad -length 0.5 -strength [expr 0*$e0] -e0 $e0 

Bpm -name "BPM131" -length 0
Drift -name "CR1.DS3C_A" -length 0.4 

Multipole -name "CR1.SX3CC" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.50153*$e0] -e0 $e0 

Drift -name "CR1.DS3C_B" -length 0.4 

Quadrupole -name "CR1.QS3C" -synrad $quad_synrad -length 0.5 -strength [expr 0.6263995*$e0] -e0 $e0 

Bpm -name "BPM132" -length 0
Drift -name "CR1.DS4C" -length 0.5 

Quadrupole -name "CR1.QS4C" -synrad $quad_synrad -length 0.5 -strength [expr 0*$e0] -e0 $e0 

Bpm -name "BPM133" -length 0
Drift -name "CR1.DS5C" -length 0.5 

Quadrupole -name "CR1.QS5C" -synrad $quad_synrad -length 0.5 -strength [expr -0.880414135*$e0] -e0 $e0 

Bpm -name "BPM134" -length 0
Drift -name "CR1.DS6C" -length 0.5 

Drift -name "CR1.WIG1" -length 0.8 

Drift -name "CR1.WIG2" -length 0.8 

Drift -name "CR1.DS6C" -length 0.5 

Quadrupole -name "CR1.QS5C" -synrad $quad_synrad -length 0.5 -strength [expr -0.880414135*$e0] -e0 $e0 

Bpm -name "BPM135" -length 0
Drift -name "CR1.DS5C" -length 0.5 

Quadrupole -name "CR1.QS4C" -synrad $quad_synrad -length 0.5 -strength [expr 0*$e0] -e0 $e0 

Bpm -name "BPM136" -length 0
Drift -name "CR1.DS4C" -length 0.5 

Quadrupole -name "CR1.QS3C" -synrad $quad_synrad -length 0.5 -strength [expr 0.6263995*$e0] -e0 $e0 

Bpm -name "BPM137" -length 0
Drift -name "CR1.DS3C_B" -length 0.4 

Multipole -name "CR1.SX3CC" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.50153*$e0] -e0 $e0 

Drift -name "CR1.DS3C_A" -length 0.4 

Quadrupole -name "CR1.QS2C" -synrad $quad_synrad -length 0.5 -strength [expr 0*$e0] -e0 $e0 

Bpm -name "BPM138" -length 0
Drift -name "CR1.DS2C" -length 0.5 

Quadrupole -name "CR1.QS1C" -synrad $quad_synrad -length 0.5 -strength [expr -0.4417115388*$e0] -e0 $e0 

Bpm -name "BPM139" -length 0
Drift -name "CR1.DS1C_B" -length 0.95 

Multipole -name "CR1.SX1CC" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -3.54141*$e0] -e0 $e0 

Drift -name "CR1.DS1C_A" -length 1.95 

Drift -name "CR1MDBACELLSTART" -length 0 

Drift -name "CR1.DBA.MCELL" -length 0 

Quadrupole -name "CR1.DBA.QI1" -synrad $quad_synrad -length 0.4 -strength [expr -0.3358875195*$e0] -e0 $e0 

Bpm -name "BPM140" -length 0
Drift -name "CR1.DBA.D11A" -length 0.2082887778 

Drift -name "CR1.DBA.D11B" -length 0.1989315367 

Quadrupole -name "CR1.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.66922795*$e0] -e0 $e0 

Bpm -name "BPM141" -length 0
Drift -name "CR1.DBA.D12" -length 0.2467904078 

Sbend -name "CR1.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -six_dim 1 -e0 $e0 -K [expr 0.05125499604*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*0.2983805661*0.2983805661/2.801278682*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.DS1C_A" -length 0.2929622945 

Multipole -name "CR1.DBA.SN0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.037095359*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS1C_B" -length 0.1 

Drift -name "CR1.DBA.HCM0" -length 0 

Quadrupole -name "CR1.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -0.450384*$e0] -e0 $e0 

Bpm -name "BPM142" -length 0
Drift -name "CR1.DBA.DS2C" -length 0.5664710132 

Sbend -name "CR1.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -six_dim 1 -e0 $e0 -K [expr 0.1995945832*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.HBM" -length 0 

Drift -name "CR1.DBA.DS3C_A" -length 0.1426363128 

Multipole -name "CR1.DBA.SN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.022314728*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS3C_B" -length 0.1426363128 

Quadrupole -name "CR1.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr 0.2323546236*$e0] -e0 $e0 

Bpm -name "BPM143" -length 0
Quadrupole -name "CR1.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr 0.2323546236*$e0] -e0 $e0 

Bpm -name "BPM144" -length 0
Drift -name "CR1.DBA.DS3C_B" -length 0.1426363128 

Multipole -name "CR1.DBA.SNN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS3C_A" -length 0.1426363128 

Drift -name "CR1.DBA.HBM2" -length 0 

Sbend -name "CR1.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -six_dim 1 -e0 $e0 -K [expr 0.1995945832*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.DS2C" -length 0.5664710132 

Quadrupole -name "CR1.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -0.450384*$e0] -e0 $e0 

Bpm -name "BPM145" -length 0
Drift -name "CR1.DBA.HCM" -length 0 

Drift -name "CR1.DBA.DS1C_B" -length 0.1 

Multipole -name "CR1.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 3.839149563*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS1C_A" -length 0.2929622945 

Sbend -name "CR1.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -six_dim 1 -e0 $e0 -K [expr 0.05125499604*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*0.2983805661*0.2983805661/2.801278682*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.D12" -length 0.2467904078 

Quadrupole -name "CR1.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.66922795*$e0] -e0 $e0 

Bpm -name "BPM146" -length 0
Drift -name "CR1.DBA.D11B1" -length 0.3133083062 

Multipole -name "CR1.DBA.SD0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR1.DBA.DM0" -length 0.1 

Quadrupole -name "CR1.DBA.QF1" -synrad $quad_synrad -length 0.4 -strength [expr -0.664141578*$e0] -e0 $e0 

Bpm -name "BPM147" -length 0
Drift -name "CR1.DBA.DM1_A" -length 1.106904053 

Multipole -name "CR1.DBA.SNCH1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 4.557288132*$e0] -e0 $e0 

Drift -name "CR1.DBA.DM1_B" -length 0.1069040525 

Quadrupole -name "CR1.DBA.QF2" -synrad $quad_synrad -length 0.4 -strength [expr 0.5637241356*$e0] -e0 $e0 

Bpm -name "BPM148" -length 0
Drift -name "CR1.DBA.DM2" -length 1.099116763 

Multipole -name "CR1.DBA.SD0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR1.DBA.DMSEXT1" -length 0.1 

Quadrupole -name "CR1.DBA.QF3" -synrad $quad_synrad -length 0.4 -strength [expr -0.6662844456*$e0] -e0 $e0 

Bpm -name "BPM149" -length 0
Drift -name "CR1.DBA.DM3" -length 0.2563831229 

Drift -name "CR1.DBA.DMSEXT1" -length 0.1 

Drift -name "CR1.DBA.D11B2" -length 0.1489315367 

Quadrupole -name "CR1.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.66922795*$e0] -e0 $e0 

Bpm -name "BPM150" -length 0
Drift -name "CR1.DBA.D12" -length 0.2467904078 

Sbend -name "CR1.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -six_dim 1 -e0 $e0 -K [expr 0.05125499604*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*0.2983805661*0.2983805661/2.801278682*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.DS1C_A" -length 0.2929622945 

Multipole -name "CR1.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 3.839149563*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS1C_B" -length 0.1 

Quadrupole -name "CR1.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -0.450384*$e0] -e0 $e0 

Bpm -name "BPM151" -length 0
Drift -name "CR1.DBA.DS2C" -length 0.5664710132 

Sbend -name "CR1.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -six_dim 1 -e0 $e0 -K [expr 0.1995945832*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.DS3C_A" -length 0.1426363128 

Multipole -name "CR1.DBA.SN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.022314728*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS3C_B" -length 0.1426363128 

Quadrupole -name "CR1.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr 0.2323546236*$e0] -e0 $e0 

Bpm -name "BPM152" -length 0
Quadrupole -name "CR1.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr 0.2323546236*$e0] -e0 $e0 

Bpm -name "BPM153" -length 0
Drift -name "CR1.DBA.DS3C_B" -length 0.1426363128 

Multipole -name "CR1.DBA.SNN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS3C_A" -length 0.1426363128 

Sbend -name "CR1.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -six_dim 1 -e0 $e0 -K [expr 0.1995945832*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.DS2C" -length 0.5664710132 

Quadrupole -name "CR1.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -0.450384*$e0] -e0 $e0 

Bpm -name "BPM154" -length 0
Drift -name "CR1.DBA.DS1C_B" -length 0.1 

Multipole -name "CR1.DBA.SN0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.037095359*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS1C_A" -length 0.2929622945 

Sbend -name "CR1.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -six_dim 1 -e0 $e0 -K [expr 0.05125499604*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*0.2983805661*0.2983805661/2.801278682*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.D12" -length 0.2467904078 

Quadrupole -name "CR1.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.66922795*$e0] -e0 $e0 

Bpm -name "BPM155" -length 0
Drift -name "CR1.DBA.D11B" -length 0.1989315367 

Drift -name "CR1.DBA.D11A" -length 0.2082887778 

Quadrupole -name "CR1.DBA.QI1" -synrad $quad_synrad -length 0.4 -strength [expr -0.3358875195*$e0] -e0 $e0 

Bpm -name "BPM156" -length 0
Drift -name "CR1.DBA.MCELL" -length 0 

Drift -name "CR1MDBACELLEND" -length 0 

Drift -name "CR1.DL0IN" -length 1.9898566 

Quadrupole -name "CR1.QL1IN" -synrad $quad_synrad -length 0.5 -strength [expr -0.7229782885*$e0] -e0 $e0 

Bpm -name "BPM157" -length 0
Drift -name "CR1.DL1IN" -length 0.3098482 

Quadrupole -name "CR1.QL2IN" -synrad $quad_synrad -length 0.5 -strength [expr 1.191142713*$e0] -e0 $e0 

Bpm -name "BPM158" -length 0
Drift -name "CR1.DL2IN" -length 0.3010372 

Quadrupole -name "CR1.QL3IN" -synrad $quad_synrad -length 0.5 -strength [expr -0.4937532183*$e0] -e0 $e0 

Bpm -name "BPM159" -length 0
Drift -name "CR1.DL3IN" -length 1.099258 

Drift -name "CR1.DRRFL" -length 0.3 

# WARNING: CORRECTOR needs to be defined, no PLACET element

CORRECTOR -name "CR1.RFDEFLCOMP1" -length 0.2 -strength_x [expr 0.00095*1e6*$e0] 

Drift -name "CR1.DRRFL1" -length 0.1 

# WARNING: CORRECTOR needs to be defined, no PLACET element

CORRECTOR -name "CR1.RFDEFLBOFF" -length 1.147353938 -strength_x [expr -0.0019*1e6*$e0] 

Drift -name "CR1.DRRFL1" -length 0.1 

# WARNING: CORRECTOR needs to be defined, no PLACET element

CORRECTOR -name "CR1.RFDEFLCOMP2" -length 0.2 -strength_x [expr 0.00095*1e6*$e0] 

Drift -name "CR1.DRRFL2" -length 0.45265 

Drift -name "CR1.DL4IN" -length 0.349996062 

Quadrupole -name "CR1.QL4IN" -synrad $quad_synrad -length 0.5 -strength [expr 0.555285*$e0] -e0 $e0 

Bpm -name "BPM160" -length 0
Drift -name "CR1.DL5IN" -length 1 

Quadrupole -name "CR1.QL5IN" -synrad $quad_synrad -length 0.5 -strength [expr -0.8298265*$e0] -e0 $e0 

Bpm -name "BPM161" -length 0
Drift -name "CR1.DL6IN" -length 0.553 

Quadrupole -name "CR1.QL6IN" -synrad $quad_synrad -length 0.25 -strength [expr 0.496129*$e0] -e0 $e0 

Bpm -name "BPM162" -length 0
Drift -name "CR1.DL7IN" -length 0.5 

Drift -name "CR1.SEPTUM" -length 1 

Drift -name "CR1.SEPTUM" -length 1 

Drift -name "CR1.DL7IN" -length 0.5 

Quadrupole -name "CR1.QL6IN" -synrad $quad_synrad -length 0.25 -strength [expr 0.496129*$e0] -e0 $e0 

Bpm -name "BPM163" -length 0
Drift -name "CR1.DL6IN" -length 0.553 

Quadrupole -name "CR1.QL5IN" -synrad $quad_synrad -length 0.5 -strength [expr -0.8298265*$e0] -e0 $e0 

Bpm -name "BPM164" -length 0
Drift -name "CR1.DL5IN" -length 1 

Quadrupole -name "CR1.QL4IN" -synrad $quad_synrad -length 0.5 -strength [expr 0.555285*$e0] -e0 $e0 

Bpm -name "BPM165" -length 0
Drift -name "CR1.DL4IN" -length 0.349996062 

Drift -name "CR1.DRRFL2" -length 0.45265 

# WARNING: CORRECTOR needs to be defined, no PLACET element

CORRECTOR -name "CR1.RFDEFLCOMP2" -length 0.2 -strength_x [expr 0.00095*1e6*$e0] 

Drift -name "CR1.DRRFL1" -length 0.1 

# WARNING: CORRECTOR needs to be defined, no PLACET element

CORRECTOR -name "CR1.RFDEFLBOFF" -length 1.147353938 -strength_x [expr -0.0019*1e6*$e0] 

Drift -name "CR1.DRRFL1" -length 0.1 

# WARNING: CORRECTOR needs to be defined, no PLACET element

CORRECTOR -name "CR1.RFDEFLCOMP1" -length 0.2 -strength_x [expr 0.00095*1e6*$e0] 

Drift -name "CR1.DRRFL" -length 0.3 

Drift -name "CR1.DL3IN" -length 1.099258 

Quadrupole -name "CR1.QL3IN" -synrad $quad_synrad -length 0.5 -strength [expr -0.4937532183*$e0] -e0 $e0 

Bpm -name "BPM166" -length 0
Drift -name "CR1.DL2IN" -length 0.3010372 

Quadrupole -name "CR1.QL2IN" -synrad $quad_synrad -length 0.5 -strength [expr 1.191142713*$e0] -e0 $e0 

Bpm -name "BPM167" -length 0
Drift -name "CR1.DL1IN" -length 0.3098482 

Quadrupole -name "CR1.QL1IN" -synrad $quad_synrad -length 0.5 -strength [expr -0.7229782885*$e0] -e0 $e0 

Bpm -name "BPM168" -length 0
Drift -name "CR1.DL0IN" -length 1.9898566 

Drift -name "CR1.MARCENT" -length 0 

Drift -name "CR1MDBACELLSTART" -length 0 

Drift -name "CR1.DBA.MCELL" -length 0 

Quadrupole -name "CR1.DBA.QI1" -synrad $quad_synrad -length 0.4 -strength [expr -0.3358875195*$e0] -e0 $e0 

Bpm -name "BPM169" -length 0
Drift -name "CR1.DBA.D11A" -length 0.2082887778 

Drift -name "CR1.DBA.D11B" -length 0.1989315367 

Quadrupole -name "CR1.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.66922795*$e0] -e0 $e0 

Bpm -name "BPM170" -length 0
Drift -name "CR1.DBA.D12" -length 0.2467904078 

Sbend -name "CR1.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -six_dim 1 -e0 $e0 -K [expr 0.05125499604*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*0.2983805661*0.2983805661/2.801278682*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.DS1C_A" -length 0.2929622945 

Multipole -name "CR1.DBA.SN0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.037095359*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS1C_B" -length 0.1 

Drift -name "CR1.DBA.HCM0" -length 0 

Quadrupole -name "CR1.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -0.450384*$e0] -e0 $e0 

Bpm -name "BPM171" -length 0
Drift -name "CR1.DBA.DS2C" -length 0.5664710132 

Sbend -name "CR1.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -six_dim 1 -e0 $e0 -K [expr 0.1995945832*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.HBM" -length 0 

Drift -name "CR1.DBA.DS3C_A" -length 0.1426363128 

Multipole -name "CR1.DBA.SN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.022314728*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS3C_B" -length 0.1426363128 

Quadrupole -name "CR1.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr 0.2323546236*$e0] -e0 $e0 

Bpm -name "BPM172" -length 0
Quadrupole -name "CR1.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr 0.2323546236*$e0] -e0 $e0 

Bpm -name "BPM173" -length 0
Drift -name "CR1.DBA.DS3C_B" -length 0.1426363128 

Multipole -name "CR1.DBA.SNN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS3C_A" -length 0.1426363128 

Drift -name "CR1.DBA.HBM2" -length 0 

Sbend -name "CR1.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -six_dim 1 -e0 $e0 -K [expr 0.1995945832*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.DS2C" -length 0.5664710132 

Quadrupole -name "CR1.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -0.450384*$e0] -e0 $e0 

Bpm -name "BPM174" -length 0
Drift -name "CR1.DBA.HCM" -length 0 

Drift -name "CR1.DBA.DS1C_B" -length 0.1 

Multipole -name "CR1.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 3.839149563*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS1C_A" -length 0.2929622945 

Sbend -name "CR1.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -six_dim 1 -e0 $e0 -K [expr 0.05125499604*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*0.2983805661*0.2983805661/2.801278682*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.D12" -length 0.2467904078 

Quadrupole -name "CR1.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.66922795*$e0] -e0 $e0 

Bpm -name "BPM175" -length 0
Drift -name "CR1.DBA.D11B1" -length 0.3133083062 

Multipole -name "CR1.DBA.SD0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR1.DBA.DM0" -length 0.1 

Quadrupole -name "CR1.DBA.QF1" -synrad $quad_synrad -length 0.4 -strength [expr -0.664141578*$e0] -e0 $e0 

Bpm -name "BPM176" -length 0
Drift -name "CR1.DBA.DM1_A" -length 1.106904053 

Multipole -name "CR1.DBA.SNCH1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 4.557288132*$e0] -e0 $e0 

Drift -name "CR1.DBA.DM1_B" -length 0.1069040525 

Quadrupole -name "CR1.DBA.QF2" -synrad $quad_synrad -length 0.4 -strength [expr 0.5637241356*$e0] -e0 $e0 

Bpm -name "BPM177" -length 0
Drift -name "CR1.DBA.DM2" -length 1.099116763 

Multipole -name "CR1.DBA.SD0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR1.DBA.DMSEXT1" -length 0.1 

Quadrupole -name "CR1.DBA.QF3" -synrad $quad_synrad -length 0.4 -strength [expr -0.6662844456*$e0] -e0 $e0 

Bpm -name "BPM178" -length 0
Drift -name "CR1.DBA.DM3" -length 0.2563831229 

Drift -name "CR1.DBA.DMSEXT1" -length 0.1 

Drift -name "CR1.DBA.D11B2" -length 0.1489315367 

Quadrupole -name "CR1.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.66922795*$e0] -e0 $e0 

Bpm -name "BPM179" -length 0
Drift -name "CR1.DBA.D12" -length 0.2467904078 

Sbend -name "CR1.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -six_dim 1 -e0 $e0 -K [expr 0.05125499604*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*0.2983805661*0.2983805661/2.801278682*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.DS1C_A" -length 0.2929622945 

Multipole -name "CR1.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 3.839149563*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS1C_B" -length 0.1 

Quadrupole -name "CR1.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -0.450384*$e0] -e0 $e0 

Bpm -name "BPM180" -length 0
Drift -name "CR1.DBA.DS2C" -length 0.5664710132 

Sbend -name "CR1.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -six_dim 1 -e0 $e0 -K [expr 0.1995945832*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.DS3C_A" -length 0.1426363128 

Multipole -name "CR1.DBA.SN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.022314728*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS3C_B" -length 0.1426363128 

Quadrupole -name "CR1.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr 0.2323546236*$e0] -e0 $e0 

Bpm -name "BPM181" -length 0
Quadrupole -name "CR1.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr 0.2323546236*$e0] -e0 $e0 

Bpm -name "BPM182" -length 0
Drift -name "CR1.DBA.DS3C_B" -length 0.1426363128 

Multipole -name "CR1.DBA.SNN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS3C_A" -length 0.1426363128 

Sbend -name "CR1.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -six_dim 1 -e0 $e0 -K [expr 0.1995945832*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.DS2C" -length 0.5664710132 

Quadrupole -name "CR1.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -0.450384*$e0] -e0 $e0 

Bpm -name "BPM183" -length 0
Drift -name "CR1.DBA.DS1C_B" -length 0.1 

Multipole -name "CR1.DBA.SN0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.037095359*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS1C_A" -length 0.2929622945 

Sbend -name "CR1.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -six_dim 1 -e0 $e0 -K [expr 0.05125499604*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*0.2983805661*0.2983805661/2.801278682*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.D12" -length 0.2467904078 

Quadrupole -name "CR1.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.66922795*$e0] -e0 $e0 

Bpm -name "BPM184" -length 0
Drift -name "CR1.DBA.D11B" -length 0.1989315367 

Drift -name "CR1.DBA.D11A" -length 0.2082887778 

Quadrupole -name "CR1.DBA.QI1" -synrad $quad_synrad -length 0.4 -strength [expr -0.3358875195*$e0] -e0 $e0 

Bpm -name "BPM185" -length 0
Drift -name "CR1.DBA.MCELL" -length 0 

Drift -name "CR1MDBACELLEND" -length 0 

Drift -name "CR1.DTR1A" -length 0.35 

Multipole -name "CR1.SXTR1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -12.2224*$e0] -e0 $e0 

Drift -name "CR1.DTR1B" -length 0.15 

Quadrupole -name "CR1.QTR1" -synrad $quad_synrad -length 0.5 -strength [expr -0.7495203095*$e0] -e0 $e0 

Bpm -name "BPM186" -length 0
Drift -name "CR1.DTR2" -length 1.2 

Quadrupole -name "CR1.QTR2" -synrad $quad_synrad -length 0.5 -strength [expr 0.7910425825*$e0] -e0 $e0 

Bpm -name "BPM187" -length 0
Drift -name "CR1.DTR3A" -length 0.45 

Multipole -name "CR1.SXTR3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -29.7172*$e0] -e0 $e0 

Drift -name "CR1.DTR3B" -length 0.45 

Quadrupole -name "CR1.QTR3" -synrad $quad_synrad -length 0.5 -strength [expr -0.949250117*$e0] -e0 $e0 

Bpm -name "BPM188" -length 0
Drift -name "CR1.DTR4" -length 0.6 

Quadrupole -name "CR1.QTR4" -synrad $quad_synrad -length 0.5 -strength [expr 0.2822164798*$e0] -e0 $e0 

Bpm -name "BPM189" -length 0
Drift -name "CR1.DTR5A" -length 0.75 

Multipole -name "CR1.SXTR5" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 8.54408*$e0] -e0 $e0 

Drift -name "CR1.DTR5B" -length 0.35 

Drift -name "CR1.DTR5B" -length 0.35 

Multipole -name "CR1.SXTR5" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 8.54408*$e0] -e0 $e0 

Drift -name "CR1.DTR5A" -length 0.75 

Quadrupole -name "CR1.QTR4" -synrad $quad_synrad -length 0.5 -strength [expr 0.2822164798*$e0] -e0 $e0 

Bpm -name "BPM190" -length 0
Drift -name "CR1.DTR4" -length 0.6 

Quadrupole -name "CR1.QTR3" -synrad $quad_synrad -length 0.5 -strength [expr -0.949250117*$e0] -e0 $e0 

Bpm -name "BPM191" -length 0
Drift -name "CR1.DTR3B" -length 0.45 

Multipole -name "CR1.SXTR3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -29.7172*$e0] -e0 $e0 

Drift -name "CR1.DTR3A" -length 0.45 

Quadrupole -name "CR1.QTR2" -synrad $quad_synrad -length 0.5 -strength [expr 0.7910425825*$e0] -e0 $e0 

Bpm -name "BPM192" -length 0
Drift -name "CR1.DTR2" -length 1.2 

Quadrupole -name "CR1.QTR1" -synrad $quad_synrad -length 0.5 -strength [expr -0.7495203095*$e0] -e0 $e0 

Bpm -name "BPM193" -length 0
Drift -name "CR1.DTR1B" -length 0.15 

Multipole -name "CR1.SXTR1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -12.2224*$e0] -e0 $e0 

Drift -name "CR1.DTR1A" -length 0.35 

Drift -name "CR1MDBACELLSTART" -length 0 

Drift -name "CR1.DBA.MCELL" -length 0 

Quadrupole -name "CR1.DBA.QI1" -synrad $quad_synrad -length 0.4 -strength [expr -0.3358875195*$e0] -e0 $e0 

Bpm -name "BPM194" -length 0
Drift -name "CR1.DBA.D11A" -length 0.2082887778 

Drift -name "CR1.DBA.D11B" -length 0.1989315367 

Quadrupole -name "CR1.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.66922795*$e0] -e0 $e0 

Bpm -name "BPM195" -length 0
Drift -name "CR1.DBA.D12" -length 0.2467904078 

Sbend -name "CR1.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -six_dim 1 -e0 $e0 -K [expr 0.05125499604*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*0.2983805661*0.2983805661/2.801278682*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.DS1C_A" -length 0.2929622945 

Multipole -name "CR1.DBA.SN0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.037095359*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS1C_B" -length 0.1 

Drift -name "CR1.DBA.HCM0" -length 0 

Quadrupole -name "CR1.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -0.450384*$e0] -e0 $e0 

Bpm -name "BPM196" -length 0
Drift -name "CR1.DBA.DS2C" -length 0.5664710132 

Sbend -name "CR1.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -six_dim 1 -e0 $e0 -K [expr 0.1995945832*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.HBM" -length 0 

Drift -name "CR1.DBA.DS3C_A" -length 0.1426363128 

Multipole -name "CR1.DBA.SN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.022314728*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS3C_B" -length 0.1426363128 

Quadrupole -name "CR1.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr 0.2323546236*$e0] -e0 $e0 

Bpm -name "BPM197" -length 0
Quadrupole -name "CR1.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr 0.2323546236*$e0] -e0 $e0 

Bpm -name "BPM198" -length 0
Drift -name "CR1.DBA.DS3C_B" -length 0.1426363128 

Multipole -name "CR1.DBA.SNN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS3C_A" -length 0.1426363128 

Drift -name "CR1.DBA.HBM2" -length 0 

Sbend -name "CR1.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -six_dim 1 -e0 $e0 -K [expr 0.1995945832*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.DS2C" -length 0.5664710132 

Quadrupole -name "CR1.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -0.450384*$e0] -e0 $e0 

Bpm -name "BPM199" -length 0
Drift -name "CR1.DBA.HCM" -length 0 

Drift -name "CR1.DBA.DS1C_B" -length 0.1 

Multipole -name "CR1.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 3.839149563*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS1C_A" -length 0.2929622945 

Sbend -name "CR1.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -six_dim 1 -e0 $e0 -K [expr 0.05125499604*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*0.2983805661*0.2983805661/2.801278682*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.D12" -length 0.2467904078 

Quadrupole -name "CR1.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.66922795*$e0] -e0 $e0 

Bpm -name "BPM200" -length 0
Drift -name "CR1.DBA.D11B1" -length 0.3133083062 

Multipole -name "CR1.DBA.SD0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR1.DBA.DM0" -length 0.1 

Quadrupole -name "CR1.DBA.QF1" -synrad $quad_synrad -length 0.4 -strength [expr -0.664141578*$e0] -e0 $e0 

Bpm -name "BPM201" -length 0
Drift -name "CR1.DBA.DM1_A" -length 1.106904053 

Multipole -name "CR1.DBA.SNCH1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 4.557288132*$e0] -e0 $e0 

Drift -name "CR1.DBA.DM1_B" -length 0.1069040525 

Quadrupole -name "CR1.DBA.QF2" -synrad $quad_synrad -length 0.4 -strength [expr 0.5637241356*$e0] -e0 $e0 

Bpm -name "BPM202" -length 0
Drift -name "CR1.DBA.DM2" -length 1.099116763 

Multipole -name "CR1.DBA.SD0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR1.DBA.DMSEXT1" -length 0.1 

Quadrupole -name "CR1.DBA.QF3" -synrad $quad_synrad -length 0.4 -strength [expr -0.6662844456*$e0] -e0 $e0 

Bpm -name "BPM203" -length 0
Drift -name "CR1.DBA.DM3" -length 0.2563831229 

Drift -name "CR1.DBA.DMSEXT1" -length 0.1 

Drift -name "CR1.DBA.D11B2" -length 0.1489315367 

Quadrupole -name "CR1.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.66922795*$e0] -e0 $e0 

Bpm -name "BPM204" -length 0
Drift -name "CR1.DBA.D12" -length 0.2467904078 

Sbend -name "CR1.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -six_dim 1 -e0 $e0 -K [expr 0.05125499604*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*0.2983805661*0.2983805661/2.801278682*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.DS1C_A" -length 0.2929622945 

Multipole -name "CR1.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 3.839149563*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS1C_B" -length 0.1 

Quadrupole -name "CR1.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -0.450384*$e0] -e0 $e0 

Bpm -name "BPM205" -length 0
Drift -name "CR1.DBA.DS2C" -length 0.5664710132 

Sbend -name "CR1.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -six_dim 1 -e0 $e0 -K [expr 0.1995945832*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.DS3C_A" -length 0.1426363128 

Multipole -name "CR1.DBA.SN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.022314728*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS3C_B" -length 0.1426363128 

Quadrupole -name "CR1.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr 0.2323546236*$e0] -e0 $e0 

Bpm -name "BPM206" -length 0
Quadrupole -name "CR1.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr 0.2323546236*$e0] -e0 $e0 

Bpm -name "BPM207" -length 0
Drift -name "CR1.DBA.DS3C_B" -length 0.1426363128 

Multipole -name "CR1.DBA.SNN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS3C_A" -length 0.1426363128 

Sbend -name "CR1.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -six_dim 1 -e0 $e0 -K [expr 0.1995945832*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.DS2C" -length 0.5664710132 

Quadrupole -name "CR1.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -0.450384*$e0] -e0 $e0 

Bpm -name "BPM208" -length 0
Drift -name "CR1.DBA.DS1C_B" -length 0.1 

Multipole -name "CR1.DBA.SN0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.037095359*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS1C_A" -length 0.2929622945 

Sbend -name "CR1.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -six_dim 1 -e0 $e0 -K [expr 0.05125499604*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*0.2983805661*0.2983805661/2.801278682*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.D12" -length 0.2467904078 

Quadrupole -name "CR1.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.66922795*$e0] -e0 $e0 

Bpm -name "BPM209" -length 0
Drift -name "CR1.DBA.D11B" -length 0.1989315367 

Drift -name "CR1.DBA.D11A" -length 0.2082887778 

Quadrupole -name "CR1.DBA.QI1" -synrad $quad_synrad -length 0.4 -strength [expr -0.3358875195*$e0] -e0 $e0 

Bpm -name "BPM210" -length 0
Drift -name "CR1.DBA.MCELL" -length 0 

Drift -name "CR1MDBACELLEND" -length 0 

Drift -name "CR1.DS1C_A" -length 1.95 

Multipole -name "CR1.SX1CC" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -3.54141*$e0] -e0 $e0 

Drift -name "CR1.DS1C_B" -length 0.95 

Quadrupole -name "CR1.QS1C" -synrad $quad_synrad -length 0.5 -strength [expr -0.4417115388*$e0] -e0 $e0 

Bpm -name "BPM211" -length 0
Drift -name "CR1.DS2C" -length 0.5 

Quadrupole -name "CR1.QS2C" -synrad $quad_synrad -length 0.5 -strength [expr 0*$e0] -e0 $e0 

Bpm -name "BPM212" -length 0
Drift -name "CR1.DS3C_A" -length 0.4 

Multipole -name "CR1.SX3CC" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.50153*$e0] -e0 $e0 

Drift -name "CR1.DS3C_B" -length 0.4 

Quadrupole -name "CR1.QS3C" -synrad $quad_synrad -length 0.5 -strength [expr 0.6263995*$e0] -e0 $e0 

Bpm -name "BPM213" -length 0
Drift -name "CR1.DS4C" -length 0.5 

Quadrupole -name "CR1.QS4C" -synrad $quad_synrad -length 0.5 -strength [expr 0*$e0] -e0 $e0 

Bpm -name "BPM214" -length 0
Drift -name "CR1.DS5C" -length 0.5 

Quadrupole -name "CR1.QS5C" -synrad $quad_synrad -length 0.5 -strength [expr -0.880414135*$e0] -e0 $e0 

Bpm -name "BPM215" -length 0
Drift -name "CR1.DS6C" -length 0.5 

Drift -name "CR1.WIG1" -length 0.8 

Drift -name "CR1.WIG2" -length 0.8 

Drift -name "CR1.DS6C" -length 0.5 

Quadrupole -name "CR1.QS5C" -synrad $quad_synrad -length 0.5 -strength [expr -0.880414135*$e0] -e0 $e0 

Bpm -name "BPM216" -length 0
Drift -name "CR1.DS5C" -length 0.5 

Quadrupole -name "CR1.QS4C" -synrad $quad_synrad -length 0.5 -strength [expr 0*$e0] -e0 $e0 

Bpm -name "BPM217" -length 0
Drift -name "CR1.DS4C" -length 0.5 

Quadrupole -name "CR1.QS3C" -synrad $quad_synrad -length 0.5 -strength [expr 0.6263995*$e0] -e0 $e0 

Bpm -name "BPM218" -length 0
Drift -name "CR1.DS3C_B" -length 0.4 

Multipole -name "CR1.SX3CC" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.50153*$e0] -e0 $e0 

Drift -name "CR1.DS3C_A" -length 0.4 

Quadrupole -name "CR1.QS2C" -synrad $quad_synrad -length 0.5 -strength [expr 0*$e0] -e0 $e0 

Bpm -name "BPM219" -length 0
Drift -name "CR1.DS2C" -length 0.5 

Quadrupole -name "CR1.QS1C" -synrad $quad_synrad -length 0.5 -strength [expr -0.4417115388*$e0] -e0 $e0 

Bpm -name "BPM220" -length 0
Drift -name "CR1.DS1C_B" -length 0.95 

Multipole -name "CR1.SX1CC" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -3.54141*$e0] -e0 $e0 

Drift -name "CR1.DS1C_A" -length 1.95 

Drift -name "CR1MDBACELLSTART" -length 0 

Drift -name "CR1.DBA.MCELL" -length 0 

Quadrupole -name "CR1.DBA.QI1" -synrad $quad_synrad -length 0.4 -strength [expr -0.3358875195*$e0] -e0 $e0 

Bpm -name "BPM221" -length 0
Drift -name "CR1.DBA.D11A" -length 0.2082887778 

Drift -name "CR1.DBA.D11B" -length 0.1989315367 

Quadrupole -name "CR1.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.66922795*$e0] -e0 $e0 

Bpm -name "BPM222" -length 0
Drift -name "CR1.DBA.D12" -length 0.2467904078 

Sbend -name "CR1.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -six_dim 1 -e0 $e0 -K [expr 0.05125499604*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*0.2983805661*0.2983805661/2.801278682*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.DS1C_A" -length 0.2929622945 

Multipole -name "CR1.DBA.SN0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.037095359*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS1C_B" -length 0.1 

Drift -name "CR1.DBA.HCM0" -length 0 

Quadrupole -name "CR1.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -0.450384*$e0] -e0 $e0 

Bpm -name "BPM223" -length 0
Drift -name "CR1.DBA.DS2C" -length 0.5664710132 

Sbend -name "CR1.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -six_dim 1 -e0 $e0 -K [expr 0.1995945832*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.HBM" -length 0 

Drift -name "CR1.DBA.DS3C_A" -length 0.1426363128 

Multipole -name "CR1.DBA.SN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.022314728*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS3C_B" -length 0.1426363128 

Quadrupole -name "CR1.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr 0.2323546236*$e0] -e0 $e0 

Bpm -name "BPM224" -length 0
Quadrupole -name "CR1.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr 0.2323546236*$e0] -e0 $e0 

Bpm -name "BPM225" -length 0
Drift -name "CR1.DBA.DS3C_B" -length 0.1426363128 

Multipole -name "CR1.DBA.SNN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS3C_A" -length 0.1426363128 

Drift -name "CR1.DBA.HBM2" -length 0 

Sbend -name "CR1.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -six_dim 1 -e0 $e0 -K [expr 0.1995945832*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.DS2C" -length 0.5664710132 

Quadrupole -name "CR1.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -0.450384*$e0] -e0 $e0 

Bpm -name "BPM226" -length 0
Drift -name "CR1.DBA.HCM" -length 0 

Drift -name "CR1.DBA.DS1C_B" -length 0.1 

Multipole -name "CR1.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 3.839149563*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS1C_A" -length 0.2929622945 

Sbend -name "CR1.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -six_dim 1 -e0 $e0 -K [expr 0.05125499604*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*0.2983805661*0.2983805661/2.801278682*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.D12" -length 0.2467904078 

Quadrupole -name "CR1.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.66922795*$e0] -e0 $e0 

Bpm -name "BPM227" -length 0
Drift -name "CR1.DBA.D11B1" -length 0.3133083062 

Multipole -name "CR1.DBA.SD0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR1.DBA.DM0" -length 0.1 

Quadrupole -name "CR1.DBA.QF1" -synrad $quad_synrad -length 0.4 -strength [expr -0.664141578*$e0] -e0 $e0 

Bpm -name "BPM228" -length 0
Drift -name "CR1.DBA.DM1_A" -length 1.106904053 

Multipole -name "CR1.DBA.SNCH1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 4.557288132*$e0] -e0 $e0 

Drift -name "CR1.DBA.DM1_B" -length 0.1069040525 

Quadrupole -name "CR1.DBA.QF2" -synrad $quad_synrad -length 0.4 -strength [expr 0.5637241356*$e0] -e0 $e0 

Bpm -name "BPM229" -length 0
Drift -name "CR1.DBA.DM2" -length 1.099116763 

Multipole -name "CR1.DBA.SD0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR1.DBA.DMSEXT1" -length 0.1 

Quadrupole -name "CR1.DBA.QF3" -synrad $quad_synrad -length 0.4 -strength [expr -0.6662844456*$e0] -e0 $e0 

Bpm -name "BPM230" -length 0
Drift -name "CR1.DBA.DM3" -length 0.2563831229 

Drift -name "CR1.DBA.DMSEXT1" -length 0.1 

Drift -name "CR1.DBA.D11B2" -length 0.1489315367 

Quadrupole -name "CR1.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.66922795*$e0] -e0 $e0 

Bpm -name "BPM231" -length 0
Drift -name "CR1.DBA.D12" -length 0.2467904078 

Sbend -name "CR1.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -six_dim 1 -e0 $e0 -K [expr 0.05125499604*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*0.2983805661*0.2983805661/2.801278682*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.DS1C_A" -length 0.2929622945 

Multipole -name "CR1.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 3.839149563*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS1C_B" -length 0.1 

Quadrupole -name "CR1.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -0.450384*$e0] -e0 $e0 

Bpm -name "BPM232" -length 0
Drift -name "CR1.DBA.DS2C" -length 0.5664710132 

Sbend -name "CR1.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -six_dim 1 -e0 $e0 -K [expr 0.1995945832*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.DS3C_A" -length 0.1426363128 

Multipole -name "CR1.DBA.SN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.022314728*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS3C_B" -length 0.1426363128 

Quadrupole -name "CR1.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr 0.2323546236*$e0] -e0 $e0 

Bpm -name "BPM233" -length 0
Quadrupole -name "CR1.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr 0.2323546236*$e0] -e0 $e0 

Bpm -name "BPM234" -length 0
Drift -name "CR1.DBA.DS3C_B" -length 0.1426363128 

Multipole -name "CR1.DBA.SNN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS3C_A" -length 0.1426363128 

Sbend -name "CR1.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -six_dim 1 -e0 $e0 -K [expr 0.1995945832*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.DS2C" -length 0.5664710132 

Quadrupole -name "CR1.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -0.450384*$e0] -e0 $e0 

Bpm -name "BPM235" -length 0
Drift -name "CR1.DBA.DS1C_B" -length 0.1 

Multipole -name "CR1.DBA.SN0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.037095359*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS1C_A" -length 0.2929622945 

Sbend -name "CR1.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -six_dim 1 -e0 $e0 -K [expr 0.05125499604*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*0.2983805661*0.2983805661/2.801278682*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.D12" -length 0.2467904078 

Quadrupole -name "CR1.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.66922795*$e0] -e0 $e0 

Bpm -name "BPM236" -length 0
Drift -name "CR1.DBA.D11B" -length 0.1989315367 

Drift -name "CR1.DBA.D11A" -length 0.2082887778 

Quadrupole -name "CR1.DBA.QI1" -synrad $quad_synrad -length 0.4 -strength [expr -0.3358875195*$e0] -e0 $e0 

Bpm -name "BPM237" -length 0
Drift -name "CR1.DBA.MCELL" -length 0 

Drift -name "CR1MDBACELLEND" -length 0 

Drift -name "CR1.DL0IN" -length 1.9898566 

Quadrupole -name "CR1.QL1IN" -synrad $quad_synrad -length 0.5 -strength [expr -0.7229782885*$e0] -e0 $e0 

Bpm -name "BPM238" -length 0
Drift -name "CR1.DL1IN" -length 0.3098482 

Quadrupole -name "CR1.QL2IN" -synrad $quad_synrad -length 0.5 -strength [expr 1.191142713*$e0] -e0 $e0 

Bpm -name "BPM239" -length 0
Drift -name "CR1.DL2IN" -length 0.3010372 

Quadrupole -name "CR1.QL3IN" -synrad $quad_synrad -length 0.5 -strength [expr -0.4937532183*$e0] -e0 $e0 

Bpm -name "BPM240" -length 0
Drift -name "CR1.DL3IN" -length 1.099258 

Drift -name "CR1.DRRF" -length 0.4 

Drift -name "CR1.DRRF1" -length 0.7736769688 

Drift -name "CR1.DRRF1" -length 0.7736769688 

Drift -name "CR1.DRRF2" -length 0.55265 

Drift -name "CR1.DL4IN" -length 0.349996062 

Quadrupole -name "CR1.QL4IN" -synrad $quad_synrad -length 0.5 -strength [expr 0.555285*$e0] -e0 $e0 

Bpm -name "BPM241" -length 0
Drift -name "CR1.DL5IN" -length 1 

Quadrupole -name "CR1.QL5IN" -synrad $quad_synrad -length 0.5 -strength [expr -0.8298265*$e0] -e0 $e0 

Bpm -name "BPM242" -length 0
Drift -name "CR1.DL6IN" -length 0.553 

Quadrupole -name "CR1.QL6IN" -synrad $quad_synrad -length 0.25 -strength [expr 0.496129*$e0] -e0 $e0 

Bpm -name "BPM243" -length 0
Drift -name "CR1.DL7IN" -length 0.5 
Drift -name "CR1.EJECTION" -length 0 
Drift -name "CR1.SEPTUM" -length 1 

Drift -name "CR1.SEPTUM" -length 1 

Drift -name "CR1.DL7IN" -length 0.5 

Quadrupole -name "CR1.QL6IN" -synrad $quad_synrad -length 0.25 -strength [expr 0.496129*$e0] -e0 $e0 

Bpm -name "BPM244" -length 0
Drift -name "CR1.DL6IN" -length 0.553 

Quadrupole -name "CR1.QL5IN" -synrad $quad_synrad -length 0.5 -strength [expr -0.8298265*$e0] -e0 $e0 

Bpm -name "BPM245" -length 0
Drift -name "CR1.DL5IN" -length 1 

Quadrupole -name "CR1.QL4IN" -synrad $quad_synrad -length 0.5 -strength [expr 0.555285*$e0] -e0 $e0 

Bpm -name "BPM246" -length 0
Drift -name "CR1.DL4IN" -length 0.349996062 

Drift -name "CR1.DRRF2" -length 0.55265 

Drift -name "CR1.DRRF1" -length 0.7736769688 

Drift -name "CR1.DRRF1" -length 0.7736769688 

Drift -name "CR1.DRRF" -length 0.4 

Drift -name "CR1.DL3IN" -length 1.099258 

Quadrupole -name "CR1.QL3IN" -synrad $quad_synrad -length 0.5 -strength [expr -0.4937532183*$e0] -e0 $e0 

Bpm -name "BPM247" -length 0
Drift -name "CR1.DL2IN" -length 0.3010372 

Quadrupole -name "CR1.QL2IN" -synrad $quad_synrad -length 0.5 -strength [expr 1.191142713*$e0] -e0 $e0 

Bpm -name "BPM248" -length 0
Drift -name "CR1.DL1IN" -length 0.3098482 

Quadrupole -name "CR1.QL1IN" -synrad $quad_synrad -length 0.5 -strength [expr -0.7229782885*$e0] -e0 $e0 

Bpm -name "BPM249" -length 0
Drift -name "CR1.DL0IN" -length 1.9898566 

Drift -name "CR1MDBACELLSTART" -length 0 

Drift -name "CR1.DBA.MCELL" -length 0 

Quadrupole -name "CR1.DBA.QI1" -synrad $quad_synrad -length 0.4 -strength [expr -0.3358875195*$e0] -e0 $e0 

Bpm -name "BPM250" -length 0
Drift -name "CR1.DBA.D11A" -length 0.2082887778 

Drift -name "CR1.DBA.D11B" -length 0.1989315367 

Quadrupole -name "CR1.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.66922795*$e0] -e0 $e0 

Bpm -name "BPM251" -length 0
Drift -name "CR1.DBA.D12" -length 0.2467904078 

Sbend -name "CR1.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -six_dim 1 -e0 $e0 -K [expr 0.05125499604*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*0.2983805661*0.2983805661/2.801278682*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.DS1C_A" -length 0.2929622945 

Multipole -name "CR1.DBA.SN0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.037095359*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS1C_B" -length 0.1 

Drift -name "CR1.DBA.HCM0" -length 0 

Quadrupole -name "CR1.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -0.450384*$e0] -e0 $e0 

Bpm -name "BPM252" -length 0
Drift -name "CR1.DBA.DS2C" -length 0.5664710132 

Sbend -name "CR1.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -six_dim 1 -e0 $e0 -K [expr 0.1995945832*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.HBM" -length 0 

Drift -name "CR1.DBA.DS3C_A" -length 0.1426363128 

Multipole -name "CR1.DBA.SN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.022314728*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS3C_B" -length 0.1426363128 

Quadrupole -name "CR1.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr 0.2323546236*$e0] -e0 $e0 

Bpm -name "BPM253" -length 0
Quadrupole -name "CR1.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr 0.2323546236*$e0] -e0 $e0 

Bpm -name "BPM254" -length 0
Drift -name "CR1.DBA.DS3C_B" -length 0.1426363128 

Multipole -name "CR1.DBA.SNN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS3C_A" -length 0.1426363128 

Drift -name "CR1.DBA.HBM2" -length 0 

Sbend -name "CR1.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -six_dim 1 -e0 $e0 -K [expr 0.1995945832*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.DS2C" -length 0.5664710132 

Quadrupole -name "CR1.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -0.450384*$e0] -e0 $e0 

Bpm -name "BPM255" -length 0
Drift -name "CR1.DBA.HCM" -length 0 

Drift -name "CR1.DBA.DS1C_B" -length 0.1 

Multipole -name "CR1.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 3.839149563*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS1C_A" -length 0.2929622945 

Sbend -name "CR1.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -six_dim 1 -e0 $e0 -K [expr 0.05125499604*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*0.2983805661*0.2983805661/2.801278682*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.D12" -length 0.2467904078 

Quadrupole -name "CR1.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.66922795*$e0] -e0 $e0 

Bpm -name "BPM256" -length 0
Drift -name "CR1.DBA.D11B1" -length 0.3133083062 

Multipole -name "CR1.DBA.SD0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR1.DBA.DM0" -length 0.1 

Quadrupole -name "CR1.DBA.QF1" -synrad $quad_synrad -length 0.4 -strength [expr -0.664141578*$e0] -e0 $e0 

Bpm -name "BPM257" -length 0
Drift -name "CR1.DBA.DM1_A" -length 1.106904053 

Multipole -name "CR1.DBA.SNCH1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 4.557288132*$e0] -e0 $e0 

Drift -name "CR1.DBA.DM1_B" -length 0.1069040525 

Quadrupole -name "CR1.DBA.QF2" -synrad $quad_synrad -length 0.4 -strength [expr 0.5637241356*$e0] -e0 $e0 

Bpm -name "BPM258" -length 0
Drift -name "CR1.DBA.DM2" -length 1.099116763 

Multipole -name "CR1.DBA.SD0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR1.DBA.DMSEXT1" -length 0.1 

Quadrupole -name "CR1.DBA.QF3" -synrad $quad_synrad -length 0.4 -strength [expr -0.6662844456*$e0] -e0 $e0 

Bpm -name "BPM259" -length 0
Drift -name "CR1.DBA.DM3" -length 0.2563831229 

Drift -name "CR1.DBA.DMSEXT1" -length 0.1 

Drift -name "CR1.DBA.D11B2" -length 0.1489315367 

Quadrupole -name "CR1.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.66922795*$e0] -e0 $e0 

Bpm -name "BPM260" -length 0
Drift -name "CR1.DBA.D12" -length 0.2467904078 

Sbend -name "CR1.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -six_dim 1 -e0 $e0 -K [expr 0.05125499604*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*0.2983805661*0.2983805661/2.801278682*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.DS1C_A" -length 0.2929622945 

Multipole -name "CR1.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 3.839149563*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS1C_B" -length 0.1 

Quadrupole -name "CR1.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -0.450384*$e0] -e0 $e0 

Bpm -name "BPM261" -length 0
Drift -name "CR1.DBA.DS2C" -length 0.5664710132 

Sbend -name "CR1.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -six_dim 1 -e0 $e0 -K [expr 0.1995945832*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.DS3C_A" -length 0.1426363128 

Multipole -name "CR1.DBA.SN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.022314728*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS3C_B" -length 0.1426363128 

Quadrupole -name "CR1.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr 0.2323546236*$e0] -e0 $e0 

Bpm -name "BPM262" -length 0
Quadrupole -name "CR1.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr 0.2323546236*$e0] -e0 $e0 

Bpm -name "BPM263" -length 0
Drift -name "CR1.DBA.DS3C_B" -length 0.1426363128 

Multipole -name "CR1.DBA.SNN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS3C_A" -length 0.1426363128 

Sbend -name "CR1.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -six_dim 1 -e0 $e0 -K [expr 0.1995945832*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.DS2C" -length 0.5664710132 

Quadrupole -name "CR1.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -0.450384*$e0] -e0 $e0 

Bpm -name "BPM264" -length 0
Drift -name "CR1.DBA.DS1C_B" -length 0.1 

Multipole -name "CR1.DBA.SN0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.037095359*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS1C_A" -length 0.2929622945 

Sbend -name "CR1.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -six_dim 1 -e0 $e0 -K [expr 0.05125499604*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*0.2983805661*0.2983805661/2.801278682*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.D12" -length 0.2467904078 

Quadrupole -name "CR1.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.66922795*$e0] -e0 $e0 

Bpm -name "BPM265" -length 0
Drift -name "CR1.DBA.D11B" -length 0.1989315367 

Drift -name "CR1.DBA.D11A" -length 0.2082887778 

Quadrupole -name "CR1.DBA.QI1" -synrad $quad_synrad -length 0.4 -strength [expr -0.3358875195*$e0] -e0 $e0 

Bpm -name "BPM266" -length 0
Drift -name "CR1.DBA.MCELL" -length 0 

Drift -name "CR1MDBACELLEND" -length 0 

Drift -name "CR1.DTR1A" -length 0.35 

Multipole -name "CR1.SXTR1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -12.2224*$e0] -e0 $e0 

Drift -name "CR1.DTR1B" -length 0.15 

Quadrupole -name "CR1.QTR1" -synrad $quad_synrad -length 0.5 -strength [expr -0.7495203095*$e0] -e0 $e0 

Bpm -name "BPM267" -length 0
Drift -name "CR1.DTR2" -length 1.2 

Quadrupole -name "CR1.QTR2" -synrad $quad_synrad -length 0.5 -strength [expr 0.7910425825*$e0] -e0 $e0 

Bpm -name "BPM268" -length 0
Drift -name "CR1.DTR3A" -length 0.45 

Multipole -name "CR1.SXTR3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -29.7172*$e0] -e0 $e0 

Drift -name "CR1.DTR3B" -length 0.45 

Quadrupole -name "CR1.QTR3" -synrad $quad_synrad -length 0.5 -strength [expr -0.949250117*$e0] -e0 $e0 

Bpm -name "BPM269" -length 0
Drift -name "CR1.DTR4" -length 0.6 

Quadrupole -name "CR1.QTR4" -synrad $quad_synrad -length 0.5 -strength [expr 0.2822164798*$e0] -e0 $e0 

Bpm -name "BPM270" -length 0
Drift -name "CR1.DTR5A" -length 0.75 

Multipole -name "CR1.SXTR5" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 8.54408*$e0] -e0 $e0 

Drift -name "CR1.DTR5B" -length 0.35 

Drift -name "CR1.DTR5B" -length 0.35 

Multipole -name "CR1.SXTR5" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 8.54408*$e0] -e0 $e0 

Drift -name "CR1.DTR5A" -length 0.75 

Quadrupole -name "CR1.QTR4" -synrad $quad_synrad -length 0.5 -strength [expr 0.2822164798*$e0] -e0 $e0 

Bpm -name "BPM271" -length 0
Drift -name "CR1.DTR4" -length 0.6 

Quadrupole -name "CR1.QTR3" -synrad $quad_synrad -length 0.5 -strength [expr -0.949250117*$e0] -e0 $e0 

Bpm -name "BPM272" -length 0
Drift -name "CR1.DTR3B" -length 0.45 

Multipole -name "CR1.SXTR3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -29.7172*$e0] -e0 $e0 

Drift -name "CR1.DTR3A" -length 0.45 

Quadrupole -name "CR1.QTR2" -synrad $quad_synrad -length 0.5 -strength [expr 0.7910425825*$e0] -e0 $e0 

Bpm -name "BPM273" -length 0
Drift -name "CR1.DTR2" -length 1.2 

Quadrupole -name "CR1.QTR1" -synrad $quad_synrad -length 0.5 -strength [expr -0.7495203095*$e0] -e0 $e0 

Bpm -name "BPM274" -length 0
Drift -name "CR1.DTR1B" -length 0.15 

Multipole -name "CR1.SXTR1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -12.2224*$e0] -e0 $e0 

Drift -name "CR1.DTR1A" -length 0.35 

Drift -name "CR1MDBACELLSTART" -length 0 

Drift -name "CR1.DBA.MCELL" -length 0 

Quadrupole -name "CR1.DBA.QI1" -synrad $quad_synrad -length 0.4 -strength [expr -0.3358875195*$e0] -e0 $e0 

Bpm -name "BPM275" -length 0
Drift -name "CR1.DBA.D11A" -length 0.2082887778 

Drift -name "CR1.DBA.D11B" -length 0.1989315367 

Quadrupole -name "CR1.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.66922795*$e0] -e0 $e0 

Bpm -name "BPM276" -length 0
Drift -name "CR1.DBA.D12" -length 0.2467904078 

Sbend -name "CR1.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -six_dim 1 -e0 $e0 -K [expr 0.05125499604*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*0.2983805661*0.2983805661/2.801278682*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.DS1C_A" -length 0.2929622945 

Multipole -name "CR1.DBA.SN0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.037095359*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS1C_B" -length 0.1 

Drift -name "CR1.DBA.HCM0" -length 0 

Quadrupole -name "CR1.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -0.450384*$e0] -e0 $e0 

Bpm -name "BPM277" -length 0
Drift -name "CR1.DBA.DS2C" -length 0.5664710132 

Sbend -name "CR1.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -six_dim 1 -e0 $e0 -K [expr 0.1995945832*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.HBM" -length 0 

Drift -name "CR1.DBA.DS3C_A" -length 0.1426363128 

Multipole -name "CR1.DBA.SN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.022314728*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS3C_B" -length 0.1426363128 

Quadrupole -name "CR1.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr 0.2323546236*$e0] -e0 $e0 

Bpm -name "BPM278" -length 0
Quadrupole -name "CR1.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr 0.2323546236*$e0] -e0 $e0 

Bpm -name "BPM279" -length 0
Drift -name "CR1.DBA.DS3C_B" -length 0.1426363128 

Multipole -name "CR1.DBA.SNN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS3C_A" -length 0.1426363128 

Drift -name "CR1.DBA.HBM2" -length 0 

Sbend -name "CR1.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -six_dim 1 -e0 $e0 -K [expr 0.1995945832*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.DS2C" -length 0.5664710132 

Quadrupole -name "CR1.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -0.450384*$e0] -e0 $e0 

Bpm -name "BPM280" -length 0
Drift -name "CR1.DBA.HCM" -length 0 

Drift -name "CR1.DBA.DS1C_B" -length 0.1 

Multipole -name "CR1.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 3.839149563*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS1C_A" -length 0.2929622945 

Sbend -name "CR1.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -six_dim 1 -e0 $e0 -K [expr 0.05125499604*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*0.2983805661*0.2983805661/2.801278682*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.D12" -length 0.2467904078 

Quadrupole -name "CR1.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.66922795*$e0] -e0 $e0 

Bpm -name "BPM281" -length 0
Drift -name "CR1.DBA.D11B1" -length 0.3133083062 

Multipole -name "CR1.DBA.SD0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR1.DBA.DM0" -length 0.1 

Quadrupole -name "CR1.DBA.QF1" -synrad $quad_synrad -length 0.4 -strength [expr -0.664141578*$e0] -e0 $e0 

Bpm -name "BPM282" -length 0
Drift -name "CR1.DBA.DM1_A" -length 1.106904053 

Multipole -name "CR1.DBA.SNCH1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 4.557288132*$e0] -e0 $e0 

Drift -name "CR1.DBA.DM1_B" -length 0.1069040525 

Quadrupole -name "CR1.DBA.QF2" -synrad $quad_synrad -length 0.4 -strength [expr 0.5637241356*$e0] -e0 $e0 

Bpm -name "BPM283" -length 0
Drift -name "CR1.DBA.DM2" -length 1.099116763 

Multipole -name "CR1.DBA.SD0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR1.DBA.DMSEXT1" -length 0.1 

Quadrupole -name "CR1.DBA.QF3" -synrad $quad_synrad -length 0.4 -strength [expr -0.6662844456*$e0] -e0 $e0 

Bpm -name "BPM284" -length 0
Drift -name "CR1.DBA.DM3" -length 0.2563831229 

Drift -name "CR1.DBA.DMSEXT1" -length 0.1 

Drift -name "CR1.DBA.D11B2" -length 0.1489315367 

Quadrupole -name "CR1.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.66922795*$e0] -e0 $e0 

Bpm -name "BPM285" -length 0
Drift -name "CR1.DBA.D12" -length 0.2467904078 

Sbend -name "CR1.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -six_dim 1 -e0 $e0 -K [expr 0.05125499604*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*0.2983805661*0.2983805661/2.801278682*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.DS1C_A" -length 0.2929622945 

Multipole -name "CR1.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 3.839149563*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS1C_B" -length 0.1 

Quadrupole -name "CR1.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -0.450384*$e0] -e0 $e0 

Bpm -name "BPM286" -length 0
Drift -name "CR1.DBA.DS2C" -length 0.5664710132 

Sbend -name "CR1.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -six_dim 1 -e0 $e0 -K [expr 0.1995945832*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.DS3C_A" -length 0.1426363128 

Multipole -name "CR1.DBA.SN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.022314728*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS3C_B" -length 0.1426363128 

Quadrupole -name "CR1.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr 0.2323546236*$e0] -e0 $e0 

Bpm -name "BPM287" -length 0
Quadrupole -name "CR1.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr 0.2323546236*$e0] -e0 $e0 

Bpm -name "BPM288" -length 0
Drift -name "CR1.DBA.DS3C_B" -length 0.1426363128 

Multipole -name "CR1.DBA.SNN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS3C_A" -length 0.1426363128 

Sbend -name "CR1.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -six_dim 1 -e0 $e0 -K [expr 0.1995945832*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.DS2C" -length 0.5664710132 

Quadrupole -name "CR1.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -0.450384*$e0] -e0 $e0 

Bpm -name "BPM289" -length 0
Drift -name "CR1.DBA.DS1C_B" -length 0.1 

Multipole -name "CR1.DBA.SN0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.037095359*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS1C_A" -length 0.2929622945 

Sbend -name "CR1.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -six_dim 1 -e0 $e0 -K [expr 0.05125499604*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*0.2983805661*0.2983805661/2.801278682*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.D12" -length 0.2467904078 

Quadrupole -name "CR1.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.66922795*$e0] -e0 $e0 

Bpm -name "BPM290" -length 0
Drift -name "CR1.DBA.D11B" -length 0.1989315367 

Drift -name "CR1.DBA.D11A" -length 0.2082887778 

Quadrupole -name "CR1.DBA.QI1" -synrad $quad_synrad -length 0.4 -strength [expr -0.3358875195*$e0] -e0 $e0 

Bpm -name "BPM291" -length 0
Drift -name "CR1.DBA.MCELL" -length 0 

Drift -name "CR1MDBACELLEND" -length 0 

Drift -name "CR1.DS1C_A" -length 1.95 

Multipole -name "CR1.SX1CC" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -3.54141*$e0] -e0 $e0 

Drift -name "CR1.DS1C_B" -length 0.95 

Quadrupole -name "CR1.QS1C" -synrad $quad_synrad -length 0.5 -strength [expr -0.4417115388*$e0] -e0 $e0 

Bpm -name "BPM292" -length 0
Drift -name "CR1.DS2C" -length 0.5 

Quadrupole -name "CR1.QS2C" -synrad $quad_synrad -length 0.5 -strength [expr 0*$e0] -e0 $e0 

Bpm -name "BPM293" -length 0
Drift -name "CR1.DS3C_A" -length 0.4 

Multipole -name "CR1.SX3CC" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.50153*$e0] -e0 $e0 

Drift -name "CR1.DS3C_B" -length 0.4 

Quadrupole -name "CR1.QS3C" -synrad $quad_synrad -length 0.5 -strength [expr 0.6263995*$e0] -e0 $e0 

Bpm -name "BPM294" -length 0
Drift -name "CR1.DS4C" -length 0.5 

Quadrupole -name "CR1.QS4C" -synrad $quad_synrad -length 0.5 -strength [expr 0*$e0] -e0 $e0 

Bpm -name "BPM295" -length 0
Drift -name "CR1.DS5C" -length 0.5 

Quadrupole -name "CR1.QS5C" -synrad $quad_synrad -length 0.5 -strength [expr -0.880414135*$e0] -e0 $e0 

Bpm -name "BPM296" -length 0
Drift -name "CR1.DS6C" -length 0.5 

Drift -name "CR1.WIG1" -length 0.8 

Drift -name "CR1.WIG2" -length 0.8 

Drift -name "CR1.DS6C" -length 0.5 

Quadrupole -name "CR1.QS5C" -synrad $quad_synrad -length 0.5 -strength [expr -0.880414135*$e0] -e0 $e0 

Bpm -name "BPM297" -length 0
Drift -name "CR1.DS5C" -length 0.5 

Quadrupole -name "CR1.QS4C" -synrad $quad_synrad -length 0.5 -strength [expr 0*$e0] -e0 $e0 

Bpm -name "BPM298" -length 0
Drift -name "CR1.DS4C" -length 0.5 

Quadrupole -name "CR1.QS3C" -synrad $quad_synrad -length 0.5 -strength [expr 0.6263995*$e0] -e0 $e0 

Bpm -name "BPM299" -length 0
Drift -name "CR1.DS3C_B" -length 0.4 

Multipole -name "CR1.SX3CC" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.50153*$e0] -e0 $e0 

Drift -name "CR1.DS3C_A" -length 0.4 

Quadrupole -name "CR1.QS2C" -synrad $quad_synrad -length 0.5 -strength [expr 0*$e0] -e0 $e0 

Bpm -name "BPM300" -length 0
Drift -name "CR1.DS2C" -length 0.5 

Quadrupole -name "CR1.QS1C" -synrad $quad_synrad -length 0.5 -strength [expr -0.4417115388*$e0] -e0 $e0 

Bpm -name "BPM301" -length 0
Drift -name "CR1.DS1C_B" -length 0.95 

Multipole -name "CR1.SX1CC" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -3.54141*$e0] -e0 $e0 

Drift -name "CR1.DS1C_A" -length 1.95 

Drift -name "CR1MDBACELLSTART" -length 0 

Drift -name "CR1.DBA.MCELL" -length 0 

Quadrupole -name "CR1.DBA.QI1" -synrad $quad_synrad -length 0.4 -strength [expr -0.3358875195*$e0] -e0 $e0 

Bpm -name "BPM302" -length 0
Drift -name "CR1.DBA.D11A" -length 0.2082887778 

Drift -name "CR1.DBA.D11B" -length 0.1989315367 

Quadrupole -name "CR1.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.66922795*$e0] -e0 $e0 

Bpm -name "BPM303" -length 0
Drift -name "CR1.DBA.D12" -length 0.2467904078 

Sbend -name "CR1.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -six_dim 1 -e0 $e0 -K [expr 0.05125499604*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*0.2983805661*0.2983805661/2.801278682*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.DS1C_A" -length 0.2929622945 

Multipole -name "CR1.DBA.SN0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.037095359*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS1C_B" -length 0.1 

Drift -name "CR1.DBA.HCM0" -length 0 

Quadrupole -name "CR1.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -0.450384*$e0] -e0 $e0 

Bpm -name "BPM304" -length 0
Drift -name "CR1.DBA.DS2C" -length 0.5664710132 

Sbend -name "CR1.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -six_dim 1 -e0 $e0 -K [expr 0.1995945832*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.HBM" -length 0 

Drift -name "CR1.DBA.DS3C_A" -length 0.1426363128 

Multipole -name "CR1.DBA.SN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.022314728*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS3C_B" -length 0.1426363128 

Quadrupole -name "CR1.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr 0.2323546236*$e0] -e0 $e0 

Bpm -name "BPM305" -length 0
Quadrupole -name "CR1.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr 0.2323546236*$e0] -e0 $e0 

Bpm -name "BPM306" -length 0
Drift -name "CR1.DBA.DS3C_B" -length 0.1426363128 

Multipole -name "CR1.DBA.SNN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS3C_A" -length 0.1426363128 

Drift -name "CR1.DBA.HBM2" -length 0 

Sbend -name "CR1.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -six_dim 1 -e0 $e0 -K [expr 0.1995945832*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.DS2C" -length 0.5664710132 

Quadrupole -name "CR1.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -0.450384*$e0] -e0 $e0 

Bpm -name "BPM307" -length 0
Drift -name "CR1.DBA.HCM" -length 0 

Drift -name "CR1.DBA.DS1C_B" -length 0.1 

Multipole -name "CR1.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 3.839149563*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS1C_A" -length 0.2929622945 

Sbend -name "CR1.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -six_dim 1 -e0 $e0 -K [expr 0.05125499604*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*0.2983805661*0.2983805661/2.801278682*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.D12" -length 0.2467904078 

Quadrupole -name "CR1.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.66922795*$e0] -e0 $e0 

Bpm -name "BPM308" -length 0
Drift -name "CR1.DBA.D11B1" -length 0.3133083062 

Multipole -name "CR1.DBA.SD0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR1.DBA.DM0" -length 0.1 

Quadrupole -name "CR1.DBA.QF1" -synrad $quad_synrad -length 0.4 -strength [expr -0.664141578*$e0] -e0 $e0 

Bpm -name "BPM309" -length 0
Drift -name "CR1.DBA.DM1_A" -length 1.106904053 

Multipole -name "CR1.DBA.SNCH1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 4.557288132*$e0] -e0 $e0 

Drift -name "CR1.DBA.DM1_B" -length 0.1069040525 

Quadrupole -name "CR1.DBA.QF2" -synrad $quad_synrad -length 0.4 -strength [expr 0.5637241356*$e0] -e0 $e0 

Bpm -name "BPM310" -length 0
Drift -name "CR1.DBA.DM2" -length 1.099116763 

Multipole -name "CR1.DBA.SD0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR1.DBA.DMSEXT1" -length 0.1 

Quadrupole -name "CR1.DBA.QF3" -synrad $quad_synrad -length 0.4 -strength [expr -0.6662844456*$e0] -e0 $e0 

Bpm -name "BPM311" -length 0
Drift -name "CR1.DBA.DM3" -length 0.2563831229 

Drift -name "CR1.DBA.DMSEXT1" -length 0.1 

Drift -name "CR1.DBA.D11B2" -length 0.1489315367 

Quadrupole -name "CR1.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.66922795*$e0] -e0 $e0 

Bpm -name "BPM312" -length 0
Drift -name "CR1.DBA.D12" -length 0.2467904078 

Sbend -name "CR1.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -six_dim 1 -e0 $e0 -K [expr 0.05125499604*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*0.2983805661*0.2983805661/2.801278682*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.DS1C_A" -length 0.2929622945 

Multipole -name "CR1.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 3.839149563*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS1C_B" -length 0.1 

Quadrupole -name "CR1.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -0.450384*$e0] -e0 $e0 

Bpm -name "BPM313" -length 0
Drift -name "CR1.DBA.DS2C" -length 0.5664710132 

Sbend -name "CR1.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -six_dim 1 -e0 $e0 -K [expr 0.1995945832*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.DS3C_A" -length 0.1426363128 

Multipole -name "CR1.DBA.SN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.022314728*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS3C_B" -length 0.1426363128 

Quadrupole -name "CR1.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr 0.2323546236*$e0] -e0 $e0 

Bpm -name "BPM314" -length 0
Quadrupole -name "CR1.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr 0.2323546236*$e0] -e0 $e0 

Bpm -name "BPM315" -length 0
Drift -name "CR1.DBA.DS3C_B" -length 0.1426363128 

Multipole -name "CR1.DBA.SNN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS3C_A" -length 0.1426363128 

Sbend -name "CR1.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -six_dim 1 -e0 $e0 -K [expr 0.1995945832*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.DS2C" -length 0.5664710132 

Quadrupole -name "CR1.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -0.450384*$e0] -e0 $e0 

Bpm -name "BPM316" -length 0
Drift -name "CR1.DBA.DS1C_B" -length 0.1 

Multipole -name "CR1.DBA.SN0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.037095359*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS1C_A" -length 0.2929622945 

Sbend -name "CR1.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -six_dim 1 -e0 $e0 -K [expr 0.05125499604*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*0.2983805661*0.2983805661/2.801278682*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.D12" -length 0.2467904078 

Quadrupole -name "CR1.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.66922795*$e0] -e0 $e0 

Bpm -name "BPM317" -length 0
Drift -name "CR1.DBA.D11B" -length 0.1989315367 

Drift -name "CR1.DBA.D11A" -length 0.2082887778 

Quadrupole -name "CR1.DBA.QI1" -synrad $quad_synrad -length 0.4 -strength [expr -0.3358875195*$e0] -e0 $e0 

Bpm -name "BPM318" -length 0
Drift -name "CR1.DBA.MCELL" -length 0 

Drift -name "CR1MDBACELLEND" -length 0 

Drift -name "CR1.DL0IN" -length 1.9898566 

Quadrupole -name "CR1.QL1IN" -synrad $quad_synrad -length 0.5 -strength [expr -0.7229782885*$e0] -e0 $e0 

Bpm -name "BPM319" -length 0
Drift -name "CR1.DL1IN" -length 0.3098482 

Quadrupole -name "CR1.QL2IN" -synrad $quad_synrad -length 0.5 -strength [expr 1.191142713*$e0] -e0 $e0 

Bpm -name "BPM320" -length 0
Drift -name "CR1.DL2IN" -length 0.3010372 

Quadrupole -name "CR1.QL3IN" -synrad $quad_synrad -length 0.5 -strength [expr -0.4937532183*$e0] -e0 $e0 

Bpm -name "BPM321" -length 0
Drift -name "CR1.DL3IN" -length 1.099258 

Drift -name "CR1.DRRFL" -length 0.3 

# WARNING: CORRECTOR needs to be defined, no PLACET element

CORRECTOR -name "CR1.RFDEFLCOMP1" -length 0.2 -strength_x [expr 0.00095*1e6*$e0] 

Drift -name "CR1.DRRFL1" -length 0.1 

# WARNING: CORRECTOR needs to be defined, no PLACET element

CORRECTOR -name "CR1.RFDEFLBOFF" -length 1.147353938 -strength_x [expr -0.0019*1e6*$e0] 

Drift -name "CR1.DRRFL1" -length 0.1 

# WARNING: CORRECTOR needs to be defined, no PLACET element

CORRECTOR -name "CR1.RFDEFLCOMP2" -length 0.2 -strength_x [expr 0.00095*1e6*$e0] 

Drift -name "CR1.DRRFL2" -length 0.45265 

Drift -name "CR1.DL4IN" -length 0.349996062 

Quadrupole -name "CR1.QL4IN" -synrad $quad_synrad -length 0.5 -strength [expr 0.555285*$e0] -e0 $e0 

Bpm -name "BPM322" -length 0
Drift -name "CR1.DL5IN" -length 1 

Quadrupole -name "CR1.QL5IN" -synrad $quad_synrad -length 0.5 -strength [expr -0.8298265*$e0] -e0 $e0 

Bpm -name "BPM323" -length 0
Drift -name "CR1.DL6IN" -length 0.553 

Quadrupole -name "CR1.QL6IN" -synrad $quad_synrad -length 0.25 -strength [expr 0.496129*$e0] -e0 $e0 

Bpm -name "BPM324" -length 0
Drift -name "CR1.DL7IN" -length 0.5 

Drift -name "CR1.SEPTUM" -length 1 

Drift -name "CR1.SEPTUM" -length 1 

Drift -name "CR1.DL7IN" -length 0.5 

Quadrupole -name "CR1.QL6IN" -synrad $quad_synrad -length 0.25 -strength [expr 0.496129*$e0] -e0 $e0 

Bpm -name "BPM325" -length 0
Drift -name "CR1.DL6IN" -length 0.553 

Quadrupole -name "CR1.QL5IN" -synrad $quad_synrad -length 0.5 -strength [expr -0.8298265*$e0] -e0 $e0 

Bpm -name "BPM326" -length 0
Drift -name "CR1.DL5IN" -length 1 

Quadrupole -name "CR1.QL4IN" -synrad $quad_synrad -length 0.5 -strength [expr 0.555285*$e0] -e0 $e0 

Bpm -name "BPM327" -length 0
Drift -name "CR1.DL4IN" -length 0.349996062 

Drift -name "CR1.DRRFL2" -length 0.45265 

# WARNING: CORRECTOR needs to be defined, no PLACET element

CORRECTOR -name "CR1.RFDEFLCOMP2" -length 0.2 -strength_x [expr 0.00095*1e6*$e0] 

Drift -name "CR1.DRRFL1" -length 0.1 

# WARNING: CORRECTOR needs to be defined, no PLACET element

CORRECTOR -name "CR1.RFDEFLBOFF" -length 1.147353938 -strength_x [expr -0.0019*1e6*$e0] 

Drift -name "CR1.DRRFL1" -length 0.1 

# WARNING: CORRECTOR needs to be defined, no PLACET element

CORRECTOR -name "CR1.RFDEFLCOMP1" -length 0.2 -strength_x [expr 0.00095*1e6*$e0] 

Drift -name "CR1.DRRFL" -length 0.3 

Drift -name "CR1.DL3IN" -length 1.099258 

Quadrupole -name "CR1.QL3IN" -synrad $quad_synrad -length 0.5 -strength [expr -0.4937532183*$e0] -e0 $e0 

Bpm -name "BPM328" -length 0
Drift -name "CR1.DL2IN" -length 0.3010372 

Quadrupole -name "CR1.QL2IN" -synrad $quad_synrad -length 0.5 -strength [expr 1.191142713*$e0] -e0 $e0 

Bpm -name "BPM329" -length 0
Drift -name "CR1.DL1IN" -length 0.3098482 

Quadrupole -name "CR1.QL1IN" -synrad $quad_synrad -length 0.5 -strength [expr -0.7229782885*$e0] -e0 $e0 

Bpm -name "BPM330" -length 0
Drift -name "CR1.DL0IN" -length 1.9898566 

Drift -name "CR1.MARCENT" -length 0 

Drift -name "CR1MDBACELLSTART" -length 0 

Drift -name "CR1.DBA.MCELL" -length 0 

Quadrupole -name "CR1.DBA.QI1" -synrad $quad_synrad -length 0.4 -strength [expr -0.3358875195*$e0] -e0 $e0 

Bpm -name "BPM331" -length 0
Drift -name "CR1.DBA.D11A" -length 0.2082887778 

Drift -name "CR1.DBA.D11B" -length 0.1989315367 

Quadrupole -name "CR1.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.66922795*$e0] -e0 $e0 

Bpm -name "BPM332" -length 0
Drift -name "CR1.DBA.D12" -length 0.2467904078 

Sbend -name "CR1.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -six_dim 1 -e0 $e0 -K [expr 0.05125499604*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*0.2983805661*0.2983805661/2.801278682*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.DS1C_A" -length 0.2929622945 

Multipole -name "CR1.DBA.SN0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.037095359*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS1C_B" -length 0.1 

Drift -name "CR1.DBA.HCM0" -length 0 

Quadrupole -name "CR1.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -0.450384*$e0] -e0 $e0 

Bpm -name "BPM333" -length 0
Drift -name "CR1.DBA.DS2C" -length 0.5664710132 

Sbend -name "CR1.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -six_dim 1 -e0 $e0 -K [expr 0.1995945832*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.HBM" -length 0 

Drift -name "CR1.DBA.DS3C_A" -length 0.1426363128 

Multipole -name "CR1.DBA.SN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.022314728*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS3C_B" -length 0.1426363128 

Quadrupole -name "CR1.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr 0.2323546236*$e0] -e0 $e0 

Bpm -name "BPM334" -length 0
Quadrupole -name "CR1.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr 0.2323546236*$e0] -e0 $e0 

Bpm -name "BPM335" -length 0
Drift -name "CR1.DBA.DS3C_B" -length 0.1426363128 

Multipole -name "CR1.DBA.SNN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS3C_A" -length 0.1426363128 

Drift -name "CR1.DBA.HBM2" -length 0 

Sbend -name "CR1.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -six_dim 1 -e0 $e0 -K [expr 0.1995945832*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.DS2C" -length 0.5664710132 

Quadrupole -name "CR1.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -0.450384*$e0] -e0 $e0 

Bpm -name "BPM336" -length 0
Drift -name "CR1.DBA.HCM" -length 0 

Drift -name "CR1.DBA.DS1C_B" -length 0.1 

Multipole -name "CR1.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 3.839149563*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS1C_A" -length 0.2929622945 

Sbend -name "CR1.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -six_dim 1 -e0 $e0 -K [expr 0.05125499604*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*0.2983805661*0.2983805661/2.801278682*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.D12" -length 0.2467904078 

Quadrupole -name "CR1.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.66922795*$e0] -e0 $e0 

Bpm -name "BPM337" -length 0
Drift -name "CR1.DBA.D11B1" -length 0.3133083062 

Multipole -name "CR1.DBA.SD0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR1.DBA.DM0" -length 0.1 

Quadrupole -name "CR1.DBA.QF1" -synrad $quad_synrad -length 0.4 -strength [expr -0.664141578*$e0] -e0 $e0 

Bpm -name "BPM338" -length 0
Drift -name "CR1.DBA.DM1_A" -length 1.106904053 

Multipole -name "CR1.DBA.SNCH1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 4.557288132*$e0] -e0 $e0 

Drift -name "CR1.DBA.DM1_B" -length 0.1069040525 

Quadrupole -name "CR1.DBA.QF2" -synrad $quad_synrad -length 0.4 -strength [expr 0.5637241356*$e0] -e0 $e0 

Bpm -name "BPM339" -length 0
Drift -name "CR1.DBA.DM2" -length 1.099116763 

Multipole -name "CR1.DBA.SD0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR1.DBA.DMSEXT1" -length 0.1 

Quadrupole -name "CR1.DBA.QF3" -synrad $quad_synrad -length 0.4 -strength [expr -0.6662844456*$e0] -e0 $e0 

Bpm -name "BPM340" -length 0
Drift -name "CR1.DBA.DM3" -length 0.2563831229 

Drift -name "CR1.DBA.DMSEXT1" -length 0.1 

Drift -name "CR1.DBA.D11B2" -length 0.1489315367 

Quadrupole -name "CR1.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.66922795*$e0] -e0 $e0 

Bpm -name "BPM341" -length 0
Drift -name "CR1.DBA.D12" -length 0.2467904078 

Sbend -name "CR1.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -six_dim 1 -e0 $e0 -K [expr 0.05125499604*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*0.2983805661*0.2983805661/2.801278682*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.DS1C_A" -length 0.2929622945 

Multipole -name "CR1.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 3.839149563*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS1C_B" -length 0.1 

Quadrupole -name "CR1.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -0.450384*$e0] -e0 $e0 

Bpm -name "BPM342" -length 0
Drift -name "CR1.DBA.DS2C" -length 0.5664710132 

Sbend -name "CR1.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -six_dim 1 -e0 $e0 -K [expr 0.1995945832*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.DS3C_A" -length 0.1426363128 

Multipole -name "CR1.DBA.SN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.022314728*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS3C_B" -length 0.1426363128 

Quadrupole -name "CR1.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr 0.2323546236*$e0] -e0 $e0 

Bpm -name "BPM343" -length 0
Quadrupole -name "CR1.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr 0.2323546236*$e0] -e0 $e0 

Bpm -name "BPM344" -length 0
Drift -name "CR1.DBA.DS3C_B" -length 0.1426363128 

Multipole -name "CR1.DBA.SNN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS3C_A" -length 0.1426363128 

Sbend -name "CR1.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -six_dim 1 -e0 $e0 -K [expr 0.1995945832*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.DS2C" -length 0.5664710132 

Quadrupole -name "CR1.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -0.450384*$e0] -e0 $e0 

Bpm -name "BPM345" -length 0
Drift -name "CR1.DBA.DS1C_B" -length 0.1 

Multipole -name "CR1.DBA.SN0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.037095359*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS1C_A" -length 0.2929622945 

Sbend -name "CR1.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -six_dim 1 -e0 $e0 -K [expr 0.05125499604*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*0.2983805661*0.2983805661/2.801278682*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.D12" -length 0.2467904078 

Quadrupole -name "CR1.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.66922795*$e0] -e0 $e0 

Bpm -name "BPM346" -length 0
Drift -name "CR1.DBA.D11B" -length 0.1989315367 

Drift -name "CR1.DBA.D11A" -length 0.2082887778 

Quadrupole -name "CR1.DBA.QI1" -synrad $quad_synrad -length 0.4 -strength [expr -0.3358875195*$e0] -e0 $e0 

Bpm -name "BPM347" -length 0
Drift -name "CR1.DBA.MCELL" -length 0 

Drift -name "CR1MDBACELLEND" -length 0 

Drift -name "CR1.DTR1A" -length 0.35 

Multipole -name "CR1.SXTR1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -12.2224*$e0] -e0 $e0 

Drift -name "CR1.DTR1B" -length 0.15 

Quadrupole -name "CR1.QTR1" -synrad $quad_synrad -length 0.5 -strength [expr -0.7495203095*$e0] -e0 $e0 

Bpm -name "BPM348" -length 0
Drift -name "CR1.DTR2" -length 1.2 

Quadrupole -name "CR1.QTR2" -synrad $quad_synrad -length 0.5 -strength [expr 0.7910425825*$e0] -e0 $e0 

Bpm -name "BPM349" -length 0
Drift -name "CR1.DTR3A" -length 0.45 

Multipole -name "CR1.SXTR3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -29.7172*$e0] -e0 $e0 

Drift -name "CR1.DTR3B" -length 0.45 

Quadrupole -name "CR1.QTR3" -synrad $quad_synrad -length 0.5 -strength [expr -0.949250117*$e0] -e0 $e0 

Bpm -name "BPM350" -length 0
Drift -name "CR1.DTR4" -length 0.6 

Quadrupole -name "CR1.QTR4" -synrad $quad_synrad -length 0.5 -strength [expr 0.2822164798*$e0] -e0 $e0 

Bpm -name "BPM351" -length 0
Drift -name "CR1.DTR5A" -length 0.75 

Multipole -name "CR1.SXTR5" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 8.54408*$e0] -e0 $e0 

Drift -name "CR1.DTR5B" -length 0.35 

Drift -name "CR1.DTR5B" -length 0.35 

Multipole -name "CR1.SXTR5" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 8.54408*$e0] -e0 $e0 

Drift -name "CR1.DTR5A" -length 0.75 

Quadrupole -name "CR1.QTR4" -synrad $quad_synrad -length 0.5 -strength [expr 0.2822164798*$e0] -e0 $e0 

Bpm -name "BPM352" -length 0
Drift -name "CR1.DTR4" -length 0.6 

Quadrupole -name "CR1.QTR3" -synrad $quad_synrad -length 0.5 -strength [expr -0.949250117*$e0] -e0 $e0 

Bpm -name "BPM353" -length 0
Drift -name "CR1.DTR3B" -length 0.45 

Multipole -name "CR1.SXTR3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -29.7172*$e0] -e0 $e0 

Drift -name "CR1.DTR3A" -length 0.45 

Quadrupole -name "CR1.QTR2" -synrad $quad_synrad -length 0.5 -strength [expr 0.7910425825*$e0] -e0 $e0 

Bpm -name "BPM354" -length 0
Drift -name "CR1.DTR2" -length 1.2 

Quadrupole -name "CR1.QTR1" -synrad $quad_synrad -length 0.5 -strength [expr -0.7495203095*$e0] -e0 $e0 

Bpm -name "BPM355" -length 0
Drift -name "CR1.DTR1B" -length 0.15 

Multipole -name "CR1.SXTR1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -12.2224*$e0] -e0 $e0 

Drift -name "CR1.DTR1A" -length 0.35 

Drift -name "CR1MDBACELLSTART" -length 0 

Drift -name "CR1.DBA.MCELL" -length 0 

Quadrupole -name "CR1.DBA.QI1" -synrad $quad_synrad -length 0.4 -strength [expr -0.3358875195*$e0] -e0 $e0 

Bpm -name "BPM356" -length 0
Drift -name "CR1.DBA.D11A" -length 0.2082887778 

Drift -name "CR1.DBA.D11B" -length 0.1989315367 

Quadrupole -name "CR1.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.66922795*$e0] -e0 $e0 

Bpm -name "BPM357" -length 0
Drift -name "CR1.DBA.D12" -length 0.2467904078 

Sbend -name "CR1.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -six_dim 1 -e0 $e0 -K [expr 0.05125499604*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*0.2983805661*0.2983805661/2.801278682*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.DS1C_A" -length 0.2929622945 

Multipole -name "CR1.DBA.SN0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.037095359*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS1C_B" -length 0.1 

Drift -name "CR1.DBA.HCM0" -length 0 

Quadrupole -name "CR1.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -0.450384*$e0] -e0 $e0 

Bpm -name "BPM358" -length 0
Drift -name "CR1.DBA.DS2C" -length 0.5664710132 

Sbend -name "CR1.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -six_dim 1 -e0 $e0 -K [expr 0.1995945832*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.HBM" -length 0 

Drift -name "CR1.DBA.DS3C_A" -length 0.1426363128 

Multipole -name "CR1.DBA.SN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.022314728*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS3C_B" -length 0.1426363128 

Quadrupole -name "CR1.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr 0.2323546236*$e0] -e0 $e0 

Bpm -name "BPM359" -length 0
Quadrupole -name "CR1.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr 0.2323546236*$e0] -e0 $e0 

Bpm -name "BPM360" -length 0
Drift -name "CR1.DBA.DS3C_B" -length 0.1426363128 

Multipole -name "CR1.DBA.SNN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS3C_A" -length 0.1426363128 

Drift -name "CR1.DBA.HBM2" -length 0 

Sbend -name "CR1.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -six_dim 1 -e0 $e0 -K [expr 0.1995945832*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.DS2C" -length 0.5664710132 

Quadrupole -name "CR1.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -0.450384*$e0] -e0 $e0 

Bpm -name "BPM361" -length 0
Drift -name "CR1.DBA.HCM" -length 0 

Drift -name "CR1.DBA.DS1C_B" -length 0.1 

Multipole -name "CR1.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 3.839149563*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS1C_A" -length 0.2929622945 

Sbend -name "CR1.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -six_dim 1 -e0 $e0 -K [expr 0.05125499604*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*0.2983805661*0.2983805661/2.801278682*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.D12" -length 0.2467904078 

Quadrupole -name "CR1.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.66922795*$e0] -e0 $e0 

Bpm -name "BPM362" -length 0
Drift -name "CR1.DBA.D11B1" -length 0.3133083062 

Multipole -name "CR1.DBA.SD0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR1.DBA.DM0" -length 0.1 

Quadrupole -name "CR1.DBA.QF1" -synrad $quad_synrad -length 0.4 -strength [expr -0.664141578*$e0] -e0 $e0 

Bpm -name "BPM363" -length 0
Drift -name "CR1.DBA.DM1_A" -length 1.106904053 

Multipole -name "CR1.DBA.SNCH1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 4.557288132*$e0] -e0 $e0 

Drift -name "CR1.DBA.DM1_B" -length 0.1069040525 

Quadrupole -name "CR1.DBA.QF2" -synrad $quad_synrad -length 0.4 -strength [expr 0.5637241356*$e0] -e0 $e0 

Bpm -name "BPM364" -length 0
Drift -name "CR1.DBA.DM2" -length 1.099116763 

Multipole -name "CR1.DBA.SD0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR1.DBA.DMSEXT1" -length 0.1 

Quadrupole -name "CR1.DBA.QF3" -synrad $quad_synrad -length 0.4 -strength [expr -0.6662844456*$e0] -e0 $e0 

Bpm -name "BPM365" -length 0
Drift -name "CR1.DBA.DM3" -length 0.2563831229 

Drift -name "CR1.DBA.DMSEXT1" -length 0.1 

Drift -name "CR1.DBA.D11B2" -length 0.1489315367 

Quadrupole -name "CR1.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.66922795*$e0] -e0 $e0 

Bpm -name "BPM366" -length 0
Drift -name "CR1.DBA.D12" -length 0.2467904078 

Sbend -name "CR1.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -six_dim 1 -e0 $e0 -K [expr 0.05125499604*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*0.2983805661*0.2983805661/2.801278682*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.DS1C_A" -length 0.2929622945 

Multipole -name "CR1.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 3.839149563*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS1C_B" -length 0.1 

Quadrupole -name "CR1.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -0.450384*$e0] -e0 $e0 

Bpm -name "BPM367" -length 0
Drift -name "CR1.DBA.DS2C" -length 0.5664710132 

Sbend -name "CR1.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -six_dim 1 -e0 $e0 -K [expr 0.1995945832*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.DS3C_A" -length 0.1426363128 

Multipole -name "CR1.DBA.SN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.022314728*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS3C_B" -length 0.1426363128 

Quadrupole -name "CR1.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr 0.2323546236*$e0] -e0 $e0 

Bpm -name "BPM368" -length 0
Quadrupole -name "CR1.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr 0.2323546236*$e0] -e0 $e0 

Bpm -name "BPM369" -length 0
Drift -name "CR1.DBA.DS3C_B" -length 0.1426363128 

Multipole -name "CR1.DBA.SNN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS3C_A" -length 0.1426363128 

Sbend -name "CR1.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -six_dim 1 -e0 $e0 -K [expr 0.1995945832*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.DS2C" -length 0.5664710132 

Quadrupole -name "CR1.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -0.450384*$e0] -e0 $e0 

Bpm -name "BPM370" -length 0
Drift -name "CR1.DBA.DS1C_B" -length 0.1 

Multipole -name "CR1.DBA.SN0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.037095359*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS1C_A" -length 0.2929622945 

Sbend -name "CR1.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -six_dim 1 -e0 $e0 -K [expr 0.05125499604*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*0.2983805661*0.2983805661/2.801278682*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.D12" -length 0.2467904078 

Quadrupole -name "CR1.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.66922795*$e0] -e0 $e0 

Bpm -name "BPM371" -length 0
Drift -name "CR1.DBA.D11B" -length 0.1989315367 

Drift -name "CR1.DBA.D11A" -length 0.2082887778 

Quadrupole -name "CR1.DBA.QI1" -synrad $quad_synrad -length 0.4 -strength [expr -0.3358875195*$e0] -e0 $e0 

Bpm -name "BPM372" -length 0
Drift -name "CR1.DBA.MCELL" -length 0 

Drift -name "CR1MDBACELLEND" -length 0 

Drift -name "CR1.DS1C_A" -length 1.95 

Multipole -name "CR1.SX1CC" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -3.54141*$e0] -e0 $e0 

Drift -name "CR1.DS1C_B" -length 0.95 

Quadrupole -name "CR1.QS1C" -synrad $quad_synrad -length 0.5 -strength [expr -0.4417115388*$e0] -e0 $e0 

Bpm -name "BPM373" -length 0
Drift -name "CR1.DS2C" -length 0.5 

Quadrupole -name "CR1.QS2C" -synrad $quad_synrad -length 0.5 -strength [expr 0*$e0] -e0 $e0 

Bpm -name "BPM374" -length 0
Drift -name "CR1.DS3C_A" -length 0.4 

Multipole -name "CR1.SX3CC" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.50153*$e0] -e0 $e0 

Drift -name "CR1.DS3C_B" -length 0.4 

Quadrupole -name "CR1.QS3C" -synrad $quad_synrad -length 0.5 -strength [expr 0.6263995*$e0] -e0 $e0 

Bpm -name "BPM375" -length 0
Drift -name "CR1.DS4C" -length 0.5 

Quadrupole -name "CR1.QS4C" -synrad $quad_synrad -length 0.5 -strength [expr 0*$e0] -e0 $e0 

Bpm -name "BPM376" -length 0
Drift -name "CR1.DS5C" -length 0.5 

Quadrupole -name "CR1.QS5C" -synrad $quad_synrad -length 0.5 -strength [expr -0.880414135*$e0] -e0 $e0 

Bpm -name "BPM377" -length 0
Drift -name "CR1.DS6C" -length 0.5 

Drift -name "CR1.WIG1" -length 0.8 

Drift -name "CR1.WIG2" -length 0.8 

Drift -name "CR1.DS6C" -length 0.5 

Quadrupole -name "CR1.QS5C" -synrad $quad_synrad -length 0.5 -strength [expr -0.880414135*$e0] -e0 $e0 

Bpm -name "BPM378" -length 0
Drift -name "CR1.DS5C" -length 0.5 

Quadrupole -name "CR1.QS4C" -synrad $quad_synrad -length 0.5 -strength [expr 0*$e0] -e0 $e0 

Bpm -name "BPM379" -length 0
Drift -name "CR1.DS4C" -length 0.5 

Quadrupole -name "CR1.QS3C" -synrad $quad_synrad -length 0.5 -strength [expr 0.6263995*$e0] -e0 $e0 

Bpm -name "BPM380" -length 0
Drift -name "CR1.DS3C_B" -length 0.4 

Multipole -name "CR1.SX3CC" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.50153*$e0] -e0 $e0 

Drift -name "CR1.DS3C_A" -length 0.4 

Quadrupole -name "CR1.QS2C" -synrad $quad_synrad -length 0.5 -strength [expr 0*$e0] -e0 $e0 

Bpm -name "BPM381" -length 0
Drift -name "CR1.DS2C" -length 0.5 

Quadrupole -name "CR1.QS1C" -synrad $quad_synrad -length 0.5 -strength [expr -0.4417115388*$e0] -e0 $e0 

Bpm -name "BPM382" -length 0
Drift -name "CR1.DS1C_B" -length 0.95 

Multipole -name "CR1.SX1CC" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -3.54141*$e0] -e0 $e0 

Drift -name "CR1.DS1C_A" -length 1.95 

Drift -name "CR1MDBACELLSTART" -length 0 

Drift -name "CR1.DBA.MCELL" -length 0 

Quadrupole -name "CR1.DBA.QI1" -synrad $quad_synrad -length 0.4 -strength [expr -0.3358875195*$e0] -e0 $e0 

Bpm -name "BPM383" -length 0
Drift -name "CR1.DBA.D11A" -length 0.2082887778 

Drift -name "CR1.DBA.D11B" -length 0.1989315367 

Quadrupole -name "CR1.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.66922795*$e0] -e0 $e0 

Bpm -name "BPM384" -length 0
Drift -name "CR1.DBA.D12" -length 0.2467904078 

Sbend -name "CR1.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -six_dim 1 -e0 $e0 -K [expr 0.05125499604*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*0.2983805661*0.2983805661/2.801278682*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.DS1C_A" -length 0.2929622945 

Multipole -name "CR1.DBA.SN0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.037095359*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS1C_B" -length 0.1 

Drift -name "CR1.DBA.HCM0" -length 0 

Quadrupole -name "CR1.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -0.450384*$e0] -e0 $e0 

Bpm -name "BPM385" -length 0
Drift -name "CR1.DBA.DS2C" -length 0.5664710132 

Sbend -name "CR1.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -six_dim 1 -e0 $e0 -K [expr 0.1995945832*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.HBM" -length 0 

Drift -name "CR1.DBA.DS3C_A" -length 0.1426363128 

Multipole -name "CR1.DBA.SN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.022314728*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS3C_B" -length 0.1426363128 

Quadrupole -name "CR1.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr 0.2323546236*$e0] -e0 $e0 

Bpm -name "BPM386" -length 0
Quadrupole -name "CR1.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr 0.2323546236*$e0] -e0 $e0 

Bpm -name "BPM387" -length 0
Drift -name "CR1.DBA.DS3C_B" -length 0.1426363128 

Multipole -name "CR1.DBA.SNN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS3C_A" -length 0.1426363128 

Drift -name "CR1.DBA.HBM2" -length 0 

Sbend -name "CR1.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -six_dim 1 -e0 $e0 -K [expr 0.1995945832*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.DS2C" -length 0.5664710132 

Quadrupole -name "CR1.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -0.450384*$e0] -e0 $e0 

Bpm -name "BPM388" -length 0
Drift -name "CR1.DBA.HCM" -length 0 

Drift -name "CR1.DBA.DS1C_B" -length 0.1 

Multipole -name "CR1.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 3.839149563*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS1C_A" -length 0.2929622945 

Sbend -name "CR1.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -six_dim 1 -e0 $e0 -K [expr 0.05125499604*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*0.2983805661*0.2983805661/2.801278682*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.D12" -length 0.2467904078 

Quadrupole -name "CR1.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.66922795*$e0] -e0 $e0 

Bpm -name "BPM389" -length 0
Drift -name "CR1.DBA.D11B1" -length 0.3133083062 

Multipole -name "CR1.DBA.SD0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR1.DBA.DM0" -length 0.1 

Quadrupole -name "CR1.DBA.QF1" -synrad $quad_synrad -length 0.4 -strength [expr -0.664141578*$e0] -e0 $e0 

Bpm -name "BPM390" -length 0
Drift -name "CR1.DBA.DM1_A" -length 1.106904053 

Multipole -name "CR1.DBA.SNCH1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 4.557288132*$e0] -e0 $e0 

Drift -name "CR1.DBA.DM1_B" -length 0.1069040525 

Quadrupole -name "CR1.DBA.QF2" -synrad $quad_synrad -length 0.4 -strength [expr 0.5637241356*$e0] -e0 $e0 

Bpm -name "BPM391" -length 0
Drift -name "CR1.DBA.DM2" -length 1.099116763 

Multipole -name "CR1.DBA.SD0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR1.DBA.DMSEXT1" -length 0.1 

Quadrupole -name "CR1.DBA.QF3" -synrad $quad_synrad -length 0.4 -strength [expr -0.6662844456*$e0] -e0 $e0 

Bpm -name "BPM392" -length 0
Drift -name "CR1.DBA.DM3" -length 0.2563831229 

Drift -name "CR1.DBA.DMSEXT1" -length 0.1 

Drift -name "CR1.DBA.D11B2" -length 0.1489315367 

Quadrupole -name "CR1.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.66922795*$e0] -e0 $e0 

Bpm -name "BPM393" -length 0
Drift -name "CR1.DBA.D12" -length 0.2467904078 

Sbend -name "CR1.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -six_dim 1 -e0 $e0 -K [expr 0.05125499604*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*0.2983805661*0.2983805661/2.801278682*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.DS1C_A" -length 0.2929622945 

Multipole -name "CR1.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 3.839149563*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS1C_B" -length 0.1 

Quadrupole -name "CR1.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -0.450384*$e0] -e0 $e0 

Bpm -name "BPM394" -length 0
Drift -name "CR1.DBA.DS2C" -length 0.5664710132 

Sbend -name "CR1.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -six_dim 1 -e0 $e0 -K [expr 0.1995945832*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.DS3C_A" -length 0.1426363128 

Multipole -name "CR1.DBA.SN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.022314728*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS3C_B" -length 0.1426363128 

Quadrupole -name "CR1.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr 0.2323546236*$e0] -e0 $e0 

Bpm -name "BPM395" -length 0
Quadrupole -name "CR1.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr 0.2323546236*$e0] -e0 $e0 

Bpm -name "BPM396" -length 0
Drift -name "CR1.DBA.DS3C_B" -length 0.1426363128 

Multipole -name "CR1.DBA.SNN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS3C_A" -length 0.1426363128 

Sbend -name "CR1.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle -0.03658132236 -E1 0.01829066118 -E2 0.01829066118 -six_dim 1 -e0 $e0 -K [expr 0.1995945832*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03658132236*-0.03658132236/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.DS2C" -length 0.5664710132 

Quadrupole -name "CR1.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -0.450384*$e0] -e0 $e0 

Bpm -name "BPM397" -length 0
Drift -name "CR1.DBA.DS1C_B" -length 0.1 

Multipole -name "CR1.DBA.SN0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.037095359*$e0] -e0 $e0 

Drift -name "CR1.DBA.DS1C_A" -length 0.2929622945 

Sbend -name "CR1.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.801278682 -angle 0.2983805661 -E1 0.1491902831 -E2 0.1491902831 -six_dim 1 -e0 $e0 -K [expr 0.05125499604*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*0.2983805661*0.2983805661/2.801278682*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR1.DBA.D12" -length 0.2467904078 

Quadrupole -name "CR1.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.66922795*$e0] -e0 $e0 

Bpm -name "BPM398" -length 0
Drift -name "CR1.DBA.D11B" -length 0.1989315367 

Drift -name "CR1.DBA.D11A" -length 0.2082887778 

Quadrupole -name "CR1.DBA.QI1" -synrad $quad_synrad -length 0.4 -strength [expr -0.3358875195*$e0] -e0 $e0 

Bpm -name "BPM399" -length 0
Drift -name "CR1.DBA.MCELL" -length 0 

Drift -name "CR1MDBACELLEND" -length 0 

Drift -name "CR1.DL0IN" -length 1.9898566 

Quadrupole -name "CR1.QL1IN" -synrad $quad_synrad -length 0.5 -strength [expr -0.7229782885*$e0] -e0 $e0 

Bpm -name "BPM400" -length 0
Drift -name "CR1.DL1IN" -length 0.3098482 

Quadrupole -name "CR1.QL2IN" -synrad $quad_synrad -length 0.5 -strength [expr 1.191142713*$e0] -e0 $e0 

Bpm -name "BPM401" -length 0
Drift -name "CR1.DL2IN" -length 0.3010372 

Quadrupole -name "CR1.QL3IN" -synrad $quad_synrad -length 0.5 -strength [expr -0.4937532183*$e0] -e0 $e0 

Bpm -name "BPM402" -length 0
Drift -name "CR1.DL3IN" -length 1.099258 

Drift -name "CR1.MSTARTEJECT" -length 0 

Drift -name "CR1.DRRF" -length 0.4 

Drift -name "CR1.DRRF1" -length 0.7736769688 

# WARNING: CORRECTOR needs to be defined, no PLACET element

CORRECTOR -name "CR1.KICKERON" -length 0 -strength_x [expr 0.0057*1e6*$e0] 

Drift -name "CR1.DRRF1" -length 0.7736769688 

Drift -name "CR1.DRRF2" -length 0.55265 

Drift -name "CR1.DL4IN" -length 0.349996062 

Quadrupole -name "CR1.QL4IN" -synrad $quad_synrad -length 0.5 -strength [expr 0.555285*$e0] -e0 $e0 

Bpm -name "BPM403" -length 0
Drift -name "CR1.DL5IN" -length 1 

Quadrupole -name "CR1.QL5IN" -synrad $quad_synrad -length 0.5 -strength [expr -0.8298265*$e0] -e0 $e0 

Bpm -name "BPM404" -length 0
Drift -name "CR1.DL6IN" -length 0.553 

Quadrupole -name "CR1.QL6IN" -synrad $quad_synrad -length 0.25 -strength [expr 0.496129*$e0] -e0 $e0 

Bpm -name "BPM405" -length 0
Drift -name "CR1.DL7IN" -length 0.5 

Drift -name "CR1.EJECTION" -length 0 

Drift -name "MANELLOWEND" -length 0 


Bpm -name "BPM0" -length 0

Quadrupole -name "TL2.TEST" -synrad $quad_synrad -length 0.0 -strength [expr -0.0*$e0] -e0 $e0 
Drift -name "TL2.INJECTION" -length 0 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.005723015
Quadrupole -name "TL2.TEST" -synrad $quad_synrad -length 0.0 -strength [expr -0.0*$e0] -e0 $e0 

Sbend -name "TL2.BEND10" -synrad $sbend_synrad -length 2.01146240647403 -angle -0.2617993878 -E1 -0.1308996939 -E2 -0.1308996939 -e0 $e0 -fintx -1 
Quadrupole -name "TL2.TEST" -synrad $quad_synrad -length 0.0 -strength [expr -0.0*$e0] -e0 $e0 

set e0 [expr $e0-14.1e-6*-0.2617993878*-0.2617993878/2.01146240647403*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TL2.D122" -length 0.667347296 

Multipole -name "TL2.SX125" -synrad $mult_synrad -type 3 -length 0.3 -strength [expr -2.066048837*$e0] -e0 $e0 

Drift -name "TL2.D128" -length 0.258673648 

Quadrupole -name "TL2.Q13" -synrad $quad_synrad -length 0.75 -strength [expr 0.4599585843*$e0] -e0 $e0 

Bpm -name "BPM1" -length 0
Drift -name "TL2.D142" -length 0.1747711281 

Multipole -name "TL2.SX145" -synrad $mult_synrad -type 3 -length 0.3 -strength [expr 0.811883127*$e0] -e0 $e0 

Drift -name "TL2.D148" -length 0.1747711281 

Quadrupole -name "TL2.Q15" -synrad $quad_synrad -length 0.75 -strength [expr -0.5144749811*$e0] -e0 $e0 

Bpm -name "BPM2" -length 0
Drift -name "TL2.D162" -length 0.2366852486 

Drift -name "TL2.D168" -length 0.2366852486 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 0.500013974

Sbend -name "TL2.BEND17" -synrad $sbend_synrad -length 0.500027948380432 -angle 0.02589861758 -E1 0.01294930879 -E2 0.01294930879 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*0.02589861758*0.02589861758/0.500027948380432*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TL2.D182" -length 0.05000058615 

Multipole -name "TL2.SX185" -synrad $mult_synrad -type 3 -length 0.3 -strength [expr 0.9659793561*$e0] -e0 $e0 

Drift -name "TL2.D188" -length 0.05000058615 

Quadrupole -name "TL2.Q20A" -synrad $quad_synrad -length 0.375 -strength [expr 0.2449040236*$e0] -e0 $e0 

Bpm -name "BPM3" -length 0
Drift -name "TL2.MIDCELLONE" -length 0 

Quadrupole -name "TL2.Q20B" -synrad $quad_synrad -length 0.375 -strength [expr 0.2449040236*$e0] -e0 $e0 

Bpm -name "BPM4" -length 0
Drift -name "TL2.D222" -length 0.1536564076 

Multipole -name "TL2.SX225" -synrad $mult_synrad -type 3 -length 0.3 -strength [expr 0.04345068822*$e0] -e0 $e0 

Drift -name "TL2.D228" -length 0.1536564076 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 0.500013974

Sbend -name "TL2.BEND23" -synrad $sbend_synrad -length 0.500027948380432 -angle 0.02589861758 -E1 0.01294930879 -E2 0.01294930879 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*0.02589861758*0.02589861758/0.500027948380432*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TL2.D242" -length 0.0500000253 

Drift -name "TL2.D248" -length 0.0500000253 

Quadrupole -name "TL2.Q25" -synrad $quad_synrad -length 0.75 -strength [expr -0.5296565843*$e0] -e0 $e0 

Bpm -name "BPM5" -length 0
Drift -name "TL2.D262" -length 0.1562568231 

Multipole -name "TL2.SX265" -synrad $mult_synrad -type 3 -length 0.3 -strength [expr 0.713171856*$e0] -e0 $e0 

Drift -name "TL2.D268" -length 0.1562568231 

Quadrupole -name "TL2.Q27" -synrad $quad_synrad -length 0.75 -strength [expr 0.4599766094*$e0] -e0 $e0 

Bpm -name "BPM6" -length 0
Drift -name "TL2.D282" -length 0.3136946693 

Multipole -name "TL2.SX285" -synrad $mult_synrad -type 3 -length 0.3 -strength [expr -2.069303632*$e0] -e0 $e0 

Drift -name "TL2.D288" -length 0.7773893387 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.005723015

Sbend -name "TL2.BEND30" -synrad $sbend_synrad -length 2.01146240647403 -angle -0.2617993878 -E1 -0.1308996939 -E2 -0.1308996939 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2617993878*-0.2617993878/2.01146240647403*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Bpm -name "TL2.D302" -length 0.4980886966 

Multipole -name "TL2.SXC305" -synrad $mult_synrad -type 3 -length 0.3 -strength [expr 4.980073338*$e0] -e0 $e0 

Bpm -name "TL2.D308" -length 0.06602956553 

Quadrupole -name "TL2.Q31" -synrad $quad_synrad -length 0.75 -strength [expr 0.1559722145*$e0] -e0 $e0 

Bpm -name "BPM9" -length 0
Bpm -name "TL2.D312" -length 0.9874829865 

Multipole -name "TL2.SXC315" -synrad $mult_synrad -type 3 -length 0.3 -strength [expr -0.6278387535*$e0] -e0 $e0 

Bpm -name "TL2.D318" -length 0.2291609955 

Quadrupole -name "TL2.Q32" -synrad $quad_synrad -length 0.75 -strength [expr -0.5286812296*$e0] -e0 $e0 

Bpm -name "BPM12" -length 0
Bpm -name "TL2.D322" -length 1.245610625 

Multipole -name "TL2.SXC325" -synrad $mult_synrad -type 3 -length 0.3 -strength [expr 1.070255064*$e0] -e0 $e0 

Bpm -name "TL2.D328" -length 0.3152035415 

Quadrupole -name "TL2.Q33" -synrad $quad_synrad -length 0.75 -strength [expr 0.5420133188*$e0] -e0 $e0 

Bpm -name "BPM15" -length 0
Bpm -name "TL2.D332" -length 1.58013019 

Multipole -name "TL2.SXC335" -synrad $mult_synrad -type 3 -length 0.3 -strength [expr -0.1132551309*$e0] -e0 $e0 

Bpm -name "TL2.D338" -length 0.4267100632 

Quadrupole -name "TL2.Q34" -synrad $quad_synrad -length 0.75 -strength [expr -0.6390286404*$e0] -e0 $e0 

Bpm -name "BPM18" -length 0
Bpm -name "TL2.D342" -length 1.673352339 

Multipole -name "TL2.SXC345" -synrad $mult_synrad -type 3 -length 0.3 -strength [expr -0.1426577958*$e0] -e0 $e0 

Bpm -name "TL2.D348" -length 0.457784113 

Quadrupole -name "TL2.Q35" -synrad $quad_synrad -length 0.75 -strength [expr 0.666494741*$e0] -e0 $e0 

Bpm -name "BPM21" -length 0
Bpm -name "TL2.D352" -length 0.457784113 

Multipole -name "TL2.SXC355" -synrad $mult_synrad -type 3 -length 0.3 -strength [expr -0.1426577958*$e0] -e0 $e0 

Bpm -name "TL2.D358" -length 1.673352339 

Quadrupole -name "TL2.Q36" -synrad $quad_synrad -length 0.75 -strength [expr -0.6426636217*$e0] -e0 $e0 

Bpm -name "BPM24" -length 0
Bpm -name "TL2.D362" -length 0.4267100632 

Multipole -name "TL2.SXC365" -synrad $mult_synrad -type 3 -length 0.3 -strength [expr -0.1132551309*$e0] -e0 $e0 

Bpm -name "TL2.D368" -length 1.58013019 

Quadrupole -name "TL2.Q37" -synrad $quad_synrad -length 0.75 -strength [expr 0.5385499766*$e0] -e0 $e0 

Bpm -name "BPM27" -length 0
Bpm -name "TL2.D372" -length 0.3152035415 

Multipole -name "TL2.SXC375" -synrad $mult_synrad -type 3 -length 0.3 -strength [expr 1.070255064*$e0] -e0 $e0 

Bpm -name "TL2.D378" -length 1.245610625 

Quadrupole -name "TL2.Q38" -synrad $quad_synrad -length 0.75 -strength [expr -0.5380648102*$e0] -e0 $e0 

Bpm -name "BPM30" -length 0
Bpm -name "TL2.D382" -length 0.2291609955 

Multipole -name "TL2.SXC385" -synrad $mult_synrad -type 3 -length 0.3 -strength [expr -0.6278387535*$e0] -e0 $e0 

Bpm -name "TL2.D388" -length 0.9874829865 

Quadrupole -name "TL2.Q39" -synrad $quad_synrad -length 0.75 -strength [expr 0.1786552102*$e0] -e0 $e0 

Bpm -name "BPM33" -length 0
Bpm -name "TL2.D392" -length 0.06602956553 

Multipole -name "TL2.SXC395" -synrad $mult_synrad -type 3 -length 0.3 -strength [expr 4.980073338*$e0] -e0 $e0 

Bpm -name "TL2.D398" -length 0.4980886966 

Drift -name "TL2.INTEROMSEND" -length 0 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.005723015

Sbend -name "TL2.BEND40" -synrad $sbend_synrad -length 2.01146240647403 -angle 0.2617993878 -E1 0.1308996939 -E2 0.1308996939 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*0.2617993878*0.2617993878/2.01146240647403*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TL2.D422" -length 0.7773893387 

Multipole -name "TL2.SX425" -synrad $mult_synrad -type 3 -length 0.3 -strength [expr 2.662610986*$e0] -e0 $e0 

Drift -name "TL2.D428" -length 0.3136946693 

Quadrupole -name "TL2.Q43" -synrad $quad_synrad -length 0.75 -strength [expr 0.4599766094*$e0] -e0 $e0 

Bpm -name "BPM36" -length 0
Drift -name "TL2.D442" -length 0.1562568231 

Multipole -name "TL2.SX445" -synrad $mult_synrad -type 3 -length 0.3 -strength [expr -1.424215457*$e0] -e0 $e0 

Drift -name "TL2.D448" -length 0.1562568231 

Quadrupole -name "TL2.Q45" -synrad $quad_synrad -length 0.75 -strength [expr -0.5296565843*$e0] -e0 $e0 

Bpm -name "BPM37" -length 0
Drift -name "TL2.D462" -length 0.0500000253 

Drift -name "TL2.D468" -length 0.0500000253 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 0.500013974

Sbend -name "TL2.BEND47" -synrad $sbend_synrad -length 0.500027948380432 -angle -0.02589861758 -E1 -0.01294930879 -E2 -0.01294930879 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.02589861758*-0.02589861758/0.500027948380432*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TL2.D482" -length 0.1536564076 

Multipole -name "TL2.SX485" -synrad $mult_synrad -type 3 -length 0.3 -strength [expr 1.638427196*$e0] -e0 $e0 

Drift -name "TL2.D488" -length 0.1536564076 

Quadrupole -name "TL2.Q50A" -synrad $quad_synrad -length 0.375 -strength [expr 0.2496519342*$e0] -e0 $e0 

Bpm -name "BPM38" -length 0
Drift -name "TL2.MIDCELLTWO" -length 0 

Quadrupole -name "TL2.Q50B" -synrad $quad_synrad -length 0.375 -strength [expr 0.2496519342*$e0] -e0 $e0 

Bpm -name "BPM39" -length 0
Drift -name "TL2.D522" -length 0.1536564076 

Multipole -name "TL2.SX525" -synrad $mult_synrad -type 3 -length 0.3 -strength [expr 1.638427196*$e0] -e0 $e0 

Drift -name "TL2.D528" -length 0.1536564076 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 0.500013974

Sbend -name "TL2.BEND53" -synrad $sbend_synrad -length 0.500027948380432 -angle -0.02589861758 -E1 -0.01294930879 -E2 -0.01294930879 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.02589861758*-0.02589861758/0.500027948380432*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TL2.D542" -length 0.0500000253 

Drift -name "TL2.D548" -length 0.0500000253 

Quadrupole -name "TL2.Q55" -synrad $quad_synrad -length 0.75 -strength [expr -0.5296565843*$e0] -e0 $e0 

Bpm -name "BPM40" -length 0
Drift -name "TL2.D562" -length 0.1562568231 

Multipole -name "TL2.SX565" -synrad $mult_synrad -type 3 -length 0.3 -strength [expr -1.424215457*$e0] -e0 $e0 

Drift -name "TL2.D568" -length 0.1562568231 

Quadrupole -name "TL2.Q57" -synrad $quad_synrad -length 0.75 -strength [expr 0.4599766094*$e0] -e0 $e0 

Bpm -name "BPM41" -length 0
Drift -name "TL2.D582" -length 0.3136946693 

Multipole -name "TL2.SX585" -synrad $mult_synrad -type 3 -length 0.3 -strength [expr 2.662610986*$e0] -e0 $e0 

Drift -name "TL2.D588" -length 0.7773893387 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.005723015

Sbend -name "TL2.BEND60" -synrad $sbend_synrad -length 2.01146240647403 -angle 0.2617993878 -E1 0.1308996939 -E2 0.1308996939 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*0.2617993878*0.2617993878/2.01146240647403*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TL2.OMSEND" -length 0 

Drift -name "TL2.D71" -length 0.8 

Quadrupole -name "TL2.Q72" -synrad $quad_synrad -length 0.75 -strength [expr 0.6098352583*$e0] -e0 $e0 

Bpm -name "BPM42" -length 0
Drift -name "TL2.D73" -length 0.8 

Quadrupole -name "TL2.Q74" -synrad $quad_synrad -length 0.75 -strength [expr -0.6459135934*$e0] -e0 $e0 

Bpm -name "BPM43" -length 0
Drift -name "TL2.D75" -length 1.6 

Quadrupole -name "TL2.Q76" -synrad $quad_synrad -length 0.75 -strength [expr 0.5866167204*$e0] -e0 $e0 

Bpm -name "BPM44" -length 0
Drift -name "TL2.D77" -length 0.8 

Quadrupole -name "TL2.Q78" -synrad $quad_synrad -length 0.75 -strength [expr -0.2626559695*$e0] -e0 $e0 

Bpm -name "BPM45" -length 0
Drift -name "TL2.D79" -length 0.8 

Drift -name "TL2.MFODO" -length 0 

Quadrupole -name "TL2.QFFODO" -synrad $quad_synrad -length 0.75 -strength [expr 0.3*$e0] -e0 $e0 

Bpm -name "BPM46" -length 0
Drift -name "TL2.DLFODO" -length 3.25 

Quadrupole -name "TL2.QDFODO" -synrad $quad_synrad -length 0.75 -strength [expr -0.3*$e0] -e0 $e0 

Bpm -name "BPM47" -length 0
Drift -name "TL2.DLFODO" -length 3.25 

Drift -name "TL2.MFODO" -length 0 

Quadrupole -name "TL2.QFFODO" -synrad $quad_synrad -length 0.75 -strength [expr 0.3*$e0] -e0 $e0 

Bpm -name "BPM48" -length 0
Drift -name "TL2.DLFODO" -length 3.25 

Quadrupole -name "TL2.QDFODO" -synrad $quad_synrad -length 0.75 -strength [expr -0.3*$e0] -e0 $e0 

Bpm -name "BPM49" -length 0
Drift -name "TL2.DLFODO" -length 3.25 

Drift -name "TL2.MFODO" -length 0 

Quadrupole -name "TL2.QFFODO" -synrad $quad_synrad -length 0.75 -strength [expr 0.3*$e0] -e0 $e0 

Bpm -name "BPM50" -length 0
Drift -name "TL2.DLFODO" -length 3.25 

Quadrupole -name "TL2.QDFODO" -synrad $quad_synrad -length 0.75 -strength [expr -0.3*$e0] -e0 $e0 

Bpm -name "BPM51" -length 0
Drift -name "TL2.DLFODO" -length 3.25 

Drift -name "TL2.MFODO" -length 0 

Quadrupole -name "TL2.QFFODO" -synrad $quad_synrad -length 0.75 -strength [expr 0.3*$e0] -e0 $e0 

Bpm -name "BPM52" -length 0
Drift -name "TL2.DLFODO" -length 3.25 

Quadrupole -name "TL2.QDFODO" -synrad $quad_synrad -length 0.75 -strength [expr -0.3*$e0] -e0 $e0 

Bpm -name "BPM53" -length 0
Drift -name "TL2.DLFODO" -length 3.25 

Drift -name "TL2.MFODO" -length 0 

Quadrupole -name "TL2.QFFODO" -synrad $quad_synrad -length 0.75 -strength [expr 0.3*$e0] -e0 $e0 

Bpm -name "BPM54" -length 0
Drift -name "TL2.DLFODO" -length 3.25 

Quadrupole -name "TL2.QDFODO" -synrad $quad_synrad -length 0.75 -strength [expr -0.3*$e0] -e0 $e0 

Bpm -name "BPM55" -length 0
Drift -name "TL2.DLFODO" -length 3.25 

Drift -name "TL2.MFODO" -length 0 

Quadrupole -name "TL2.QFFODO" -synrad $quad_synrad -length 0.75 -strength [expr 0.3*$e0] -e0 $e0 

Bpm -name "BPM56" -length 0
Drift -name "TL2.DLFODO" -length 3.25 

Quadrupole -name "TL2.QDFODO" -synrad $quad_synrad -length 0.75 -strength [expr -0.3*$e0] -e0 $e0 

Bpm -name "BPM57" -length 0
Drift -name "TL2.DLFODO" -length 3.25 

Drift -name "TL2.MFODO" -length 0 

Quadrupole -name "TL2.QFFODO" -synrad $quad_synrad -length 0.75 -strength [expr 0.3*$e0] -e0 $e0 

Bpm -name "BPM58" -length 0
Drift -name "TL2.DLFODO" -length 3.25 

Quadrupole -name "TL2.QDFODO" -synrad $quad_synrad -length 0.75 -strength [expr -0.3*$e0] -e0 $e0 

Bpm -name "BPM59" -length 0
Drift -name "TL2.DLFODO" -length 3.25 

Drift -name "TL2.MFODO" -length 0 

Quadrupole -name "TL2.QFFODO" -synrad $quad_synrad -length 0.75 -strength [expr 0.3*$e0] -e0 $e0 

Bpm -name "BPM60" -length 0
Drift -name "TL2.DLFODO" -length 3.25 

Quadrupole -name "TL2.QDFODO" -synrad $quad_synrad -length 0.75 -strength [expr -0.3*$e0] -e0 $e0 

Bpm -name "BPM61" -length 0
Drift -name "TL2.DLFODO" -length 3.25 

Drift -name "TL2.MFODO" -length 0 

Quadrupole -name "TL2.QFFODO" -synrad $quad_synrad -length 0.75 -strength [expr 0.3*$e0] -e0 $e0 

Bpm -name "BPM62" -length 0
Drift -name "TL2.DLFODO" -length 3.25 

Quadrupole -name "TL2.QDFODO" -synrad $quad_synrad -length 0.75 -strength [expr -0.3*$e0] -e0 $e0 

Bpm -name "BPM63" -length 0
Drift -name "TL2.DLFODO" -length 3.25 

Drift -name "TL2.MFODO" -length 0 

Quadrupole -name "TL2.QFFODO" -synrad $quad_synrad -length 0.75 -strength [expr 0.3*$e0] -e0 $e0 

Bpm -name "BPM64" -length 0
Drift -name "TL2.DLFODO" -length 3.25 

Quadrupole -name "TL2.QDFODO" -synrad $quad_synrad -length 0.75 -strength [expr -0.3*$e0] -e0 $e0 

Bpm -name "BPM65" -length 0
Drift -name "TL2.DLFODO" -length 3.25 

Drift -name "TL2.MFODO" -length 0 

Quadrupole -name "TL2.QFFODO" -synrad $quad_synrad -length 0.75 -strength [expr 0.3*$e0] -e0 $e0 

Bpm -name "BPM66" -length 0
Drift -name "TL2.DLFODO" -length 3.25 

Quadrupole -name "TL2.QDFODO" -synrad $quad_synrad -length 0.75 -strength [expr -0.3*$e0] -e0 $e0 

Bpm -name "BPM67" -length 0
Drift -name "TL2.DLFODO" -length 3.25 

Drift -name "TL2.MFODO" -length 0 

Quadrupole -name "TL2.QFFODO" -synrad $quad_synrad -length 0.75 -strength [expr 0.3*$e0] -e0 $e0 

Bpm -name "BPM68" -length 0
Drift -name "TL2.DLFODO" -length 3.25 

Quadrupole -name "TL2.QDFODO" -synrad $quad_synrad -length 0.75 -strength [expr -0.3*$e0] -e0 $e0 

Bpm -name "BPM69" -length 0
Drift -name "TL2.DLFODO" -length 3.25 

Drift -name "TL2.MFODO" -length 0 

Quadrupole -name "TL2.QFFODO" -synrad $quad_synrad -length 0.75 -strength [expr 0.3*$e0] -e0 $e0 

Bpm -name "BPM70" -length 0
Drift -name "TL2.DLFODO" -length 3.25 

Quadrupole -name "TL2.QDFODO" -synrad $quad_synrad -length 0.75 -strength [expr -0.3*$e0] -e0 $e0 

Bpm -name "BPM71" -length 0
Drift -name "TL2.DLFODO" -length 3.25 

Drift -name "TL2.MFODO" -length 0 

Quadrupole -name "TL2.QFFODO" -synrad $quad_synrad -length 0.75 -strength [expr 0.3*$e0] -e0 $e0 

Bpm -name "BPM72" -length 0
Drift -name "TL2.DLFODO" -length 3.25 

Quadrupole -name "TL2.QDFODO" -synrad $quad_synrad -length 0.75 -strength [expr -0.3*$e0] -e0 $e0 

Bpm -name "BPM73" -length 0
Drift -name "TL2.DLFODO" -length 3.25 

Drift -name "TL2.MFODO" -length 0 

Quadrupole -name "TL2.QFFODO" -synrad $quad_synrad -length 0.75 -strength [expr 0.3*$e0] -e0 $e0 

Bpm -name "BPM74" -length 0
Drift -name "TL2.DLFODO" -length 3.25 

Quadrupole -name "TL2.QDFODO" -synrad $quad_synrad -length 0.75 -strength [expr -0.3*$e0] -e0 $e0 

Bpm -name "BPM75" -length 0
Drift -name "TL2.DLFODO" -length 3.25 

Drift -name "TL2.MFODO" -length 0 

Quadrupole -name "TL2.QFFODO" -synrad $quad_synrad -length 0.75 -strength [expr 0.3*$e0] -e0 $e0 

Bpm -name "BPM76" -length 0
Drift -name "TL2.DLFODO" -length 3.25 

Quadrupole -name "TL2.QDFODO" -synrad $quad_synrad -length 0.75 -strength [expr -0.3*$e0] -e0 $e0 

Bpm -name "BPM77" -length 0
Drift -name "TL2.DLFODO" -length 3.25 

Drift -name "TL2.MFODO" -length 0 

Quadrupole -name "TL2.QFFODO" -synrad $quad_synrad -length 0.75 -strength [expr 0.3*$e0] -e0 $e0 

Bpm -name "BPM78" -length 0
Drift -name "TL2.DLFODO" -length 3.25 

Quadrupole -name "TL2.QDFODO" -synrad $quad_synrad -length 0.75 -strength [expr -0.3*$e0] -e0 $e0 

Bpm -name "BPM79" -length 0
Drift -name "TL2.DLFODO" -length 3.25 

Drift -name "TL2.MFODO" -length 0 

Quadrupole -name "TL2.QFFODO" -synrad $quad_synrad -length 0.75 -strength [expr 0.3*$e0] -e0 $e0 

Bpm -name "BPM80" -length 0
Drift -name "TL2.DLFODO" -length 3.25 

Quadrupole -name "TL2.QDFODO" -synrad $quad_synrad -length 0.75 -strength [expr -0.3*$e0] -e0 $e0 

Bpm -name "BPM81" -length 0
Drift -name "TL2.DLFODO" -length 3.25 

Drift -name "TL2.MFODO" -length 0 

Quadrupole -name "TL2.QFFODO" -synrad $quad_synrad -length 0.75 -strength [expr 0.3*$e0] -e0 $e0 

Bpm -name "BPM82" -length 0
Drift -name "TL2.DLFODO" -length 3.25 

Quadrupole -name "TL2.QDFODO" -synrad $quad_synrad -length 0.75 -strength [expr -0.3*$e0] -e0 $e0 

Bpm -name "BPM83" -length 0
Drift -name "TL2.DLFODO" -length 3.25 

Drift -name "TL2.MFODO" -length 0 

Quadrupole -name "TL2.QFFODO" -synrad $quad_synrad -length 0.75 -strength [expr 0.3*$e0] -e0 $e0 

Bpm -name "BPM84" -length 0
Drift -name "TL2.DLFODO" -length 3.25 

Quadrupole -name "TL2.QDFODO" -synrad $quad_synrad -length 0.75 -strength [expr -0.3*$e0] -e0 $e0 

Bpm -name "BPM85" -length 0
Drift -name "TL2.DLFODO" -length 3.25 

Drift -name "TL2.MFODO" -length 0 

Quadrupole -name "TL2.QFFODO" -synrad $quad_synrad -length 0.75 -strength [expr 0.3*$e0] -e0 $e0 

Bpm -name "BPM86" -length 0
Drift -name "TL2.DLFODO" -length 3.25 

Quadrupole -name "TL2.QDFODO" -synrad $quad_synrad -length 0.75 -strength [expr -0.3*$e0] -e0 $e0 

Bpm -name "BPM87" -length 0
Drift -name "TL2.DLFODO" -length 3.25 

Drift -name "TL2.MFODO" -length 0 

Quadrupole -name "TL2.QFFODO" -synrad $quad_synrad -length 0.75 -strength [expr 0.3*$e0] -e0 $e0 

Bpm -name "BPM88" -length 0
Drift -name "TL2.DLFODO" -length 3.25 

Quadrupole -name "TL2.QDFODO" -synrad $quad_synrad -length 0.75 -strength [expr -0.3*$e0] -e0 $e0 

Bpm -name "BPM89" -length 0
Drift -name "TL2.DLFODO" -length 3.25 

Drift -name "TL2.MFODO" -length 0 

Quadrupole -name "TL2.QFFODO" -synrad $quad_synrad -length 0.75 -strength [expr 0.3*$e0] -e0 $e0 

Bpm -name "BPM90" -length 0
Drift -name "TL2.DLFODO" -length 3.25 

Quadrupole -name "TL2.QDFODO" -synrad $quad_synrad -length 0.75 -strength [expr -0.3*$e0] -e0 $e0 

Bpm -name "BPM91" -length 0
Drift -name "TL2.DLFODO" -length 3.25 

Drift -name "TL2.MFODO" -length 0 

Quadrupole -name "TL2.QFFODO" -synrad $quad_synrad -length 0.75 -strength [expr 0.3*$e0] -e0 $e0 

Bpm -name "BPM92" -length 0
Drift -name "TL2.DLFODO" -length 3.25 

Quadrupole -name "TL2.QDFODO" -synrad $quad_synrad -length 0.75 -strength [expr -0.3*$e0] -e0 $e0 

Bpm -name "BPM93" -length 0
Drift -name "TL2.DLFODO" -length 3.25 

Drift -name "TL2.MFODO" -length 0 

Quadrupole -name "TL2.QFFODO" -synrad $quad_synrad -length 0.75 -strength [expr 0.3*$e0] -e0 $e0 

Bpm -name "BPM94" -length 0
Drift -name "TL2.DLFODO" -length 3.25 

Quadrupole -name "TL2.QDFODO" -synrad $quad_synrad -length 0.75 -strength [expr -0.3*$e0] -e0 $e0 

Bpm -name "BPM95" -length 0
Drift -name "TL2.DLFODO" -length 3.25 

Drift -name "TL2.MFODO" -length 0 

Quadrupole -name "TL2.QFFODO" -synrad $quad_synrad -length 0.75 -strength [expr 0.3*$e0] -e0 $e0 

Bpm -name "BPM96" -length 0
Drift -name "TL2.DLFODO" -length 3.25 

Quadrupole -name "TL2.QDFODO" -synrad $quad_synrad -length 0.75 -strength [expr -0.3*$e0] -e0 $e0 

Bpm -name "BPM97" -length 0
Drift -name "TL2.DLFODO" -length 3.25 

Drift -name "TL2.MFODO" -length 0 

Quadrupole -name "TL2.QFFODO" -synrad $quad_synrad -length 0.75 -strength [expr 0.3*$e0] -e0 $e0 

Bpm -name "BPM98" -length 0
Drift -name "TL2.DLFODO" -length 3.25 

Quadrupole -name "TL2.QDFODO" -synrad $quad_synrad -length 0.75 -strength [expr -0.3*$e0] -e0 $e0 

Bpm -name "BPM99" -length 0
Drift -name "TL2.DLFODO" -length 3.25 

Drift -name "TL2.MFODO" -length 0 

Quadrupole -name "TL2.QFFODO" -synrad $quad_synrad -length 0.75 -strength [expr 0.3*$e0] -e0 $e0 

Bpm -name "BPM100" -length 0
Drift -name "TL2.DLFODO" -length 3.25 

Quadrupole -name "TL2.QDFODO" -synrad $quad_synrad -length 0.75 -strength [expr -0.3*$e0] -e0 $e0 

Bpm -name "BPM101" -length 0
Drift -name "TL2.DLFODO" -length 3.25 

Drift -name "TL2.MFODO" -length 0 

Quadrupole -name "TL2.QFFODO" -synrad $quad_synrad -length 0.75 -strength [expr 0.3*$e0] -e0 $e0 

Bpm -name "BPM102" -length 0
Drift -name "TL2.DLFODO" -length 3.25 

Quadrupole -name "TL2.QDFODO" -synrad $quad_synrad -length 0.75 -strength [expr -0.3*$e0] -e0 $e0 

Bpm -name "BPM103" -length 0
Drift -name "TL2.DLFODO" -length 3.25 

Drift -name "TL2.MFODO" -length 0 

Quadrupole -name "TL2.QFFODO" -synrad $quad_synrad -length 0.75 -strength [expr 0.3*$e0] -e0 $e0 

Bpm -name "BPM104" -length 0
Drift -name "TL2.DLFODO" -length 3.25 

Quadrupole -name "TL2.QDFODO" -synrad $quad_synrad -length 0.75 -strength [expr -0.3*$e0] -e0 $e0 

Bpm -name "BPM105" -length 0
Drift -name "TL2.DLFODO" -length 3.25 

Drift -name "TL2.MFODO" -length 0 

Quadrupole -name "TL2.QFFODO" -synrad $quad_synrad -length 0.75 -strength [expr 0.3*$e0] -e0 $e0 

Bpm -name "BPM106" -length 0
Drift -name "TL2.DLFODO" -length 3.25 

Quadrupole -name "TL2.QDFODO" -synrad $quad_synrad -length 0.75 -strength [expr -0.3*$e0] -e0 $e0 

Bpm -name "BPM107" -length 0
Drift -name "TL2.DLFODO" -length 3.25 

Drift -name "TL2.MFODO" -length 0 

Quadrupole -name "TL2.QFFODO" -synrad $quad_synrad -length 0.75 -strength [expr 0.3*$e0] -e0 $e0 

Bpm -name "BPM108" -length 0
Drift -name "TL2.DLFODO" -length 3.25 

Quadrupole -name "TL2.QDFODO" -synrad $quad_synrad -length 0.75 -strength [expr -0.3*$e0] -e0 $e0 

Bpm -name "BPM109" -length 0
Drift -name "TL2.DLFODO" -length 3.25 

Drift -name "TL2.MFODO" -length 0 

Quadrupole -name "TL2.QFFODO" -synrad $quad_synrad -length 0.75 -strength [expr 0.3*$e0] -e0 $e0 

Bpm -name "BPM110" -length 0
Drift -name "TL2.DLFODO" -length 3.25 

Quadrupole -name "TL2.QDFODO" -synrad $quad_synrad -length 0.75 -strength [expr -0.3*$e0] -e0 $e0 

Bpm -name "BPM111" -length 0
Drift -name "TL2.DLFODO" -length 3.25 

Drift -name "TL2.MFODO" -length 0 

Quadrupole -name "TL2.QFFODO" -synrad $quad_synrad -length 0.75 -strength [expr 0.3*$e0] -e0 $e0 

Bpm -name "BPM112" -length 0
Drift -name "TL2.DLFODO" -length 3.25 

Quadrupole -name "TL2.QDFODO" -synrad $quad_synrad -length 0.75 -strength [expr -0.3*$e0] -e0 $e0 

Bpm -name "BPM113" -length 0
Drift -name "TL2.DLFODO" -length 3.25 

Drift -name "TL2.MFODO" -length 0 

Quadrupole -name "TL2.QFFODO" -synrad $quad_synrad -length 0.75 -strength [expr 0.3*$e0] -e0 $e0 

Bpm -name "BPM114" -length 0
Drift -name "TL2.DLFODO" -length 3.25 

Quadrupole -name "TL2.QDFODO" -synrad $quad_synrad -length 0.75 -strength [expr -0.3*$e0] -e0 $e0 

Bpm -name "BPM115" -length 0
Drift -name "TL2.DLFODO" -length 3.25 

Drift -name "TL2.MFODO" -length 0 

Quadrupole -name "TL2.QFFODO" -synrad $quad_synrad -length 0.75 -strength [expr 0.3*$e0] -e0 $e0 

Bpm -name "BPM116" -length 0
Drift -name "TL2.DLFODO" -length 3.25 

Quadrupole -name "TL2.QDFODO" -synrad $quad_synrad -length 0.75 -strength [expr -0.3*$e0] -e0 $e0 

Bpm -name "BPM117" -length 0
Drift -name "TL2.DLFODO" -length 3.25 

Drift -name "TL2.MFODO" -length 0 

Quadrupole -name "TL2.QFFODO" -synrad $quad_synrad -length 0.75 -strength [expr 0.3*$e0] -e0 $e0 

Bpm -name "BPM118" -length 0
Drift -name "TL2.DLFODO" -length 3.25 

Quadrupole -name "TL2.QDFODO" -synrad $quad_synrad -length 0.75 -strength [expr -0.3*$e0] -e0 $e0 

Bpm -name "BPM119" -length 0
Drift -name "TL2.DLFODO" -length 3.25 

Drift -name "TL2.MFODO" -length 0 

Quadrupole -name "TL2.QFFODO" -synrad $quad_synrad -length 0.75 -strength [expr 0.3*$e0] -e0 $e0 

Bpm -name "BPM120" -length 0
Drift -name "TL2.DLFODO" -length 3.25 

Quadrupole -name "TL2.QDFODO" -synrad $quad_synrad -length 0.75 -strength [expr -0.3*$e0] -e0 $e0 

Bpm -name "BPM121" -length 0
Drift -name "TL2.DLFODO" -length 3.25 

Drift -name "TL2.MFODO" -length 0 

Quadrupole -name "TL2.QFFODO" -synrad $quad_synrad -length 0.75 -strength [expr 0.3*$e0] -e0 $e0 

Bpm -name "BPM122" -length 0
Drift -name "TL2.DLFODO" -length 3.25 

Quadrupole -name "TL2.QDFODO" -synrad $quad_synrad -length 0.75 -strength [expr -0.3*$e0] -e0 $e0 

Bpm -name "BPM123" -length 0
Drift -name "TL2.DLFODO" -length 3.25 

Drift -name "TL2.MFODO" -length 0 

Quadrupole -name "TL2.QFFODO" -synrad $quad_synrad -length 0.75 -strength [expr 0.3*$e0] -e0 $e0 

Bpm -name "BPM124" -length 0
Drift -name "TL2.DLFODO" -length 3.25 

Quadrupole -name "TL2.QDFODO" -synrad $quad_synrad -length 0.75 -strength [expr -0.3*$e0] -e0 $e0 

Bpm -name "BPM125" -length 0
Drift -name "TL2.DLFODO" -length 3.25 

Quadrupole -name "TL2.QFFODO" -synrad $quad_synrad -length 0.75 -strength [expr 0.3*$e0] -e0 $e0 

Bpm -name "BPM126" -length 0
Drift -name "TL2.DLFODO" -length 3.25 

Drift -name "TL2.D91" -length 0.8 

Quadrupole -name "TL2.Q92" -synrad $quad_synrad -length 0.75 -strength [expr -0.9918909847*$e0] -e0 $e0 

Bpm -name "BPM127" -length 0
Drift -name "TL2.D93" -length 0.8 

Quadrupole -name "TL2.Q94" -synrad $quad_synrad -length 0.75 -strength [expr 0.7498661329*$e0] -e0 $e0 

Bpm -name "BPM128" -length 0
Drift -name "TL2.D95" -length 1.2 

Quadrupole -name "TL2.Q96" -synrad $quad_synrad -length 0.75 -strength [expr -0.8354180602*$e0] -e0 $e0 

Bpm -name "BPM129" -length 0
Drift -name "TL2.D97" -length 0.8 

Quadrupole -name "TL2.Q98" -synrad $quad_synrad -length 0.75 -strength [expr 0.3113087141*$e0] -e0 $e0 

Bpm -name "BPM130" -length 0
Drift -name "TL2.D99" -length 0.8 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.005723015

Sbend -name "TL2.BEND110" -synrad $sbend_synrad -length 2.01146240647403 -angle -0.2617993878 -E1 -0.1308996939 -E2 -0.1308996939 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2617993878*-0.2617993878/2.01146240647403*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TL2.D1122" -length 1.604791401 

Multipole -name "TL2.SX1125" -synrad $mult_synrad -type 3 -length 0.3 -strength [expr -6.25704*$e0] -e0 $e0 

Drift -name "TL2.D1128" -length 0.7273957003 

Quadrupole -name "TL2.Q113" -synrad $quad_synrad -length 0.75 -strength [expr 0.4130955375*$e0] -e0 $e0 

Bpm -name "BPM131" -length 0
Drift -name "TL2.D1142" -length 0.2203857562 

Multipole -name "TL2.SX1145" -synrad $mult_synrad -type 3 -length 0.3 -strength [expr 1.461645*$e0] -e0 $e0 

Drift -name "TL2.D1148" -length 0.2203857562 

Quadrupole -name "TL2.Q115" -synrad $quad_synrad -length 0.75 -strength [expr -0.6218761651*$e0] -e0 $e0 

Bpm -name "BPM132" -length 0
Drift -name "TL2.D1162" -length 0.05000000025 

Drift -name "TL2.D1168" -length 0.05000000025 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 0.5000086579

Sbend -name "TL2.BEND117" -synrad $sbend_synrad -length 0.500017315941417 -angle 0.02038562618 -E1 0.01019281309 -E2 0.01019281309 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*0.02038562618*0.02038562618/0.500017315941417*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TL2.D1182" -length 0.1905674726 

Multipole -name "TL2.SX1185" -synrad $mult_synrad -type 3 -length 0.3 -strength [expr 0.0391743*$e0] -e0 $e0 

Drift -name "TL2.D1188" -length 0.1905674726 

Quadrupole -name "TL2.Q120A" -synrad $quad_synrad -length 0.375 -strength [expr 0.2530285339*$e0] -e0 $e0 

Bpm -name "BPM133" -length 0
Drift -name "TL2.MIDCELL11" -length 0 

Quadrupole -name "TL2.Q120B" -synrad $quad_synrad -length 0.375 -strength [expr 0.2530285339*$e0] -e0 $e0 

Bpm -name "BPM134" -length 0
Drift -name "TL2.D1222" -length 0.1905674726 

Multipole -name "TL2.SX1225" -synrad $mult_synrad -type 3 -length 0.3 -strength [expr 0.0391743*$e0] -e0 $e0 

Drift -name "TL2.D1228" -length 0.1905674726 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 0.5000086579

Sbend -name "TL2.BEND123" -synrad $sbend_synrad -length 0.500017315941417 -angle 0.02038562618 -E1 0.01019281309 -E2 0.01019281309 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*0.02038562618*0.02038562618/0.500017315941417*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TL2.D1242" -length 0.05000000025 

Drift -name "TL2.D1248" -length 0.05000000025 

Quadrupole -name "TL2.Q125" -synrad $quad_synrad -length 0.75 -strength [expr -0.6218761651*$e0] -e0 $e0 

Bpm -name "BPM135" -length 0
Drift -name "TL2.D1262" -length 0.2203857562 

Multipole -name "TL2.SX1265" -synrad $mult_synrad -type 3 -length 0.3 -strength [expr 1.461645*$e0] -e0 $e0 

Drift -name "TL2.D1268" -length 0.2203857562 

Quadrupole -name "TL2.Q127" -synrad $quad_synrad -length 0.75 -strength [expr 0.4130955375*$e0] -e0 $e0 

Bpm -name "BPM136" -length 0
Drift -name "TL2.D1282" -length 0.7273957003 

Multipole -name "TL2.SX1285" -synrad $mult_synrad -type 3 -length 0.3 -strength [expr -6.25704*$e0] -e0 $e0 

Drift -name "TL2.D1288" -length 1.604791401 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.005723015

Sbend -name "TL2.BEND130" -synrad $sbend_synrad -length 2.01146240647403 -angle -0.2617993878 -E1 -0.1308996939 -E2 -0.1308996939 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2617993878*-0.2617993878/2.01146240647403*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Bpm -name "TL2.D1302" -length 0.1024133036 

Multipole -name "TL2.SXC1305" -synrad $mult_synrad -type 3 -length 0.3 -strength [expr 5.71026*$e0] -e0 $e0 

Bpm -name "TL2.D1308" -length 0.1024133036 

Quadrupole -name "TL2.Q131" -synrad $quad_synrad -length 0.75 -strength [expr -0.5878524656*$e0] -e0 $e0 

Bpm -name "BPM139" -length 0
Bpm -name "TL2.D1312" -length 0.0911315578 

Multipole -name "TL2.SXC1315" -synrad $mult_synrad -type 3 -length 0.3 -strength [expr 0.412932*$e0] -e0 $e0 

Bpm -name "TL2.D1318" -length 0.0911315578 

Quadrupole -name "TL2.Q132" -synrad $quad_synrad -length 0.75 -strength [expr 0.7636332757*$e0] -e0 $e0 

Bpm -name "BPM142" -length 0
Bpm -name "TL2.D1322" -length 1.292423292 

Multipole -name "TL2.SXC1325" -synrad $mult_synrad -type 3 -length 0.3 -strength [expr 4.91694*$e0] -e0 $e0 

Bpm -name "TL2.D1328" -length 0.330807764 

Quadrupole -name "TL2.Q133" -synrad $quad_synrad -length 0.75 -strength [expr -0.4436832933*$e0] -e0 $e0 

Bpm -name "BPM145" -length 0
Bpm -name "TL2.D1332" -length 0.4804493205 

Multipole -name "TL2.SXC1335" -synrad $mult_synrad -type 3 -length 0.3 -strength [expr 0.371718*$e0] -e0 $e0 

Bpm -name "TL2.D1338" -length 1.741347962 

Quadrupole -name "TL2.Q134" -synrad $quad_synrad -length 0.75 -strength [expr 0.6609471657*$e0] -e0 $e0 

Bpm -name "BPM148" -length 0
Bpm -name "TL2.D1342" -length 0.1086306923 

Multipole -name "TL2.SXC1345" -synrad $mult_synrad -type 3 -length 0.3 -strength [expr 12.46407*$e0] -e0 $e0 

Bpm -name "TL2.D1348" -length 0.6258920768 

Quadrupole -name "TL2.Q135" -synrad $quad_synrad -length 0.75 -strength [expr -0.8508366547*$e0] -e0 $e0 

Bpm -name "BPM151" -length 0
Bpm -name "TL2.D1352" -length 0.6258920768 

Multipole -name "TL2.SXC1355" -synrad $mult_synrad -type 3 -length 0.3 -strength [expr 16.57008*$e0] -e0 $e0 

Bpm -name "TL2.D1358" -length 0.1086306923 

Quadrupole -name "TL2.Q136" -synrad $quad_synrad -length 0.75 -strength [expr 0.6651975*$e0] -e0 $e0 

Bpm -name "BPM154" -length 0
Bpm -name "TL2.D1362" -length 1.741347962 

Multipole -name "TL2.SXC1365" -synrad $mult_synrad -type 3 -length 0.3 -strength [expr 6.80997*$e0] -e0 $e0 

Bpm -name "TL2.D1368" -length 0.4804493205 

Quadrupole -name "TL2.Q137" -synrad $quad_synrad -length 0.75 -strength [expr -0.44732325*$e0] -e0 $e0 

Bpm -name "BPM157" -length 0
Bpm -name "TL2.D1372" -length 0.330807764 

Multipole -name "TL2.SXC1375" -synrad $mult_synrad -type 3 -length 0.3 -strength [expr 17.65377*$e0] -e0 $e0 

Bpm -name "TL2.D1378" -length 1.292423292 

Quadrupole -name "TL2.Q138" -synrad $quad_synrad -length 0.75 -strength [expr 0.783705*$e0] -e0 $e0 

Bpm -name "BPM160" -length 0
Bpm -name "TL2.D1382" -length 0.0911315578 

Multipole -name "TL2.SXC1385" -synrad $mult_synrad -type 3 -length 0.3 -strength [expr 3.94098*$e0] -e0 $e0 

Bpm -name "TL2.D1388" -length 0.0911315578 

Quadrupole -name "TL2.Q139" -synrad $quad_synrad -length 0.75 -strength [expr -0.59496*$e0] -e0 $e0 

Bpm -name "BPM163" -length 0
Bpm -name "TL2.D1392" -length 0.1024133036 

Multipole -name "TL2.SXC1395" -synrad $mult_synrad -type 3 -length 0.3 -strength [expr 9.12942*$e0] -e0 $e0 

Bpm -name "TL2.D1398" -length 0.1024133036 

Drift -name "TL2.INTEROMSEND" -length 0 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.005723015

Sbend -name "TL2.BEND140" -synrad $sbend_synrad -length 2.01146240647403 -angle 0.2617993878 -E1 0.1308996939 -E2 0.1308996939 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*0.2617993878*0.2617993878/2.01146240647403*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TL2.D1422" -length 1.604791401 

Multipole -name "TL2.SX1425" -synrad $mult_synrad -type 3 -length 0.3 -strength [expr 1.366614*$e0] -e0 $e0 

Drift -name "TL2.D1428" -length 0.7273957003 

Quadrupole -name "TL2.Q143" -synrad $quad_synrad -length 0.75 -strength [expr 0.4130955375*$e0] -e0 $e0 

Bpm -name "BPM166" -length 0
Drift -name "TL2.D1442" -length 0.2203857562 

Multipole -name "TL2.SX1445" -synrad $mult_synrad -type 3 -length 0.3 -strength [expr -0.335442*$e0] -e0 $e0 

Drift -name "TL2.D1448" -length 0.2203857562 

Quadrupole -name "TL2.Q145" -synrad $quad_synrad -length 0.75 -strength [expr -0.6218761651*$e0] -e0 $e0 

Bpm -name "BPM167" -length 0
Drift -name "TL2.D1462" -length 0.05000000025 

Drift -name "TL2.D1468" -length 0.05000000025 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 0.5000086579

Sbend -name "TL2.BEND147" -synrad $sbend_synrad -length 0.500017315941417 -angle -0.02038562618 -E1 -0.01019281309 -E2 -0.01019281309 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.02038562618*-0.02038562618/0.500017315941417*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TL2.D1482" -length 0.1905674726 

Multipole -name "TL2.SX1485" -synrad $mult_synrad -type 3 -length 0.3 -strength [expr 0.3831*$e0] -e0 $e0 

Drift -name "TL2.D1488" -length 0.1905674726 

Quadrupole -name "TL2.Q150A" -synrad $quad_synrad -length 0.375 -strength [expr 0.279207*$e0] -e0 $e0 

Bpm -name "BPM168" -length 0
Drift -name "TL2.MIDCELL12" -length 0 

Quadrupole -name "TL2.Q150B" -synrad $quad_synrad -length 0.375 -strength [expr 0.279207*$e0] -e0 $e0 

Bpm -name "BPM169" -length 0
Drift -name "TL2.D1522" -length 0.0001175 

Multipole -name "TL2.SX1525" -synrad $mult_synrad -type 3 -length 0.3 -strength [expr 0.1967463*$e0] -e0 $e0 

Drift -name "TL2.D1528" -length 0.0001175 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 0.5000086579

Sbend -name "TL2.BEND153" -synrad $sbend_synrad -length 0.500017315941417 -angle -0.02038562618 -E1 -0.01019281309 -E2 -0.01019281309 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.02038562618*-0.02038562618/0.500017315941417*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TL2.D1542" -length 0.0976775 

Drift -name "TL2.D1548" -length 0.0976775 

Quadrupole -name "TL2.Q155" -synrad $quad_synrad -length 0.75 -strength [expr -0.713214*$e0] -e0 $e0 

Bpm -name "BPM170" -length 0
Drift -name "TL2.D1562" -length 0.042885 

Multipole -name "TL2.SX1565" -synrad $mult_synrad -type 3 -length 0.3 -strength [expr 0.1000155*$e0] -e0 $e0 

Drift -name "TL2.D1568" -length 0.042885 

Quadrupole -name "TL2.Q157" -synrad $quad_synrad -length 0.75 -strength [expr 0.4241595*$e0] -e0 $e0 

Bpm -name "BPM171" -length 0
Drift -name "TL2.D1582" -length 0.8837 

Multipole -name "TL2.SX1585" -synrad $mult_synrad -type 3 -length 0.3 -strength [expr 0.816579*$e0] -e0 $e0 

Drift -name "TL2.D1588" -length 1.9174 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.005723015

Sbend -name "TL2.BEND160" -synrad $sbend_synrad -length 2.01146240647403 -angle 0.2617993878 -E1 0.1308996939 -E2 0.1308996939 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*0.2617993878*0.2617993878/2.01146240647403*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TL2.EJECTION" -length 0 
Bpm -name "BPM172" -length 0
Quadrupole -name "TL2.TEST" -synrad $quad_synrad -length 0.0 -strength [expr -0.0*$e0] -e0 $e0 
Quadrupole -name "CR2.INJ" -synrad $quad_synrad -length 0.0 -strength [expr 0.0*$e0] -e0 $e0 
Bpm -name "BPM0" -length 0
Drift -name "CR2.D1CM" -length 0.01 
Drift -name "CR2.D1M" -length 1 
Drift -name "CR2.D1M" -length 1 
Drift -name "CR2.D1M" -length 1 
Drift -name "CR2.DRFB4A" -length 0.709135479 

Quadrupole -name "CR2.QRFB3" -synrad $quad_synrad -length 0.5 -strength [expr -0.3437279899*$e0] -e0 $e0 


Bpm -name "BPM1" -length 0
Drift -name "CR2.DRFB3" -length 3.771233433 

Quadrupole -name "CR2.QRFB2" -synrad $quad_synrad -length 0.5 -strength [expr 0.5775372505*$e0] -e0 $e0 

Bpm -name "BPM2" -length 0
Drift -name "CR2.DRFB2" -length 1.193240969 

Quadrupole -name "CR2.QRFB1" -synrad $quad_synrad -length 0.5 -strength [expr -1.08870911*$e0] -e0 $e0 

Bpm -name "BPM3" -length 0
Drift -name "CR2.DRFB1" -length 0.217192186 

Drift -name "CR2.DRRFH" -length 1 

# WARNING: CORRECTOR needs to be defined, no PLACET element

Dipole -name "CR2.RFKICKNEG" -length 0 -strength_x [expr -0.004*1e6*$e0] 

Drift -name "CR2.DRRFH" -length 1 

Drift -name "CR2.DL3IN" -length 0.4000389758 


Quadrupole -name "CR2.QL3IN" -synrad $quad_synrad -length 0.5 -strength [expr -0.2675265744*$e0] -e0 $e0 
Bpm -name "BPM4" -length 0
Drift -name "CR2.DL2IN" -length 1.103409345 

Quadrupole -name "CR2.QL2IN" -synrad $quad_synrad -length 0.5 -strength [expr 0.720003675*$e0] -e0 $e0 

Bpm -name "BPM5" -length 0
Drift -name "CR2.DL1IN" -length 0.4000469975 


Quadrupole -name "CR2.QL1IN" -synrad $quad_synrad -length 0.5 -strength [expr -0.642778543*$e0] -e0 $e0 

Bpm -name "BPM6" -length 0
Drift -name "CR2.DL0IN" -length 2.246504682 

Drift -name "CR2MDBACELLSTART" -length 0 

Drift -name "CR2.DBA.MCELL" -length 0 

Quadrupole -name "CR2.DBA.QI1" -synrad $quad_synrad -length 0.4 -strength [expr -0.5864293272*$e0] -e0 $e0 

Bpm -name "BPM7" -length 0
Drift -name "CR2.DBA.D11A" -length 0.1730305995 

Multipole -name "CR2.DBA.SNCH1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.D11B" -length 0.1628018956 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM8" -length 0
Drift -name "CR2.DBA.D12" -length 0.2697746031 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM9" -length 0
Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Drift -name "CR2.DBA.HBM" -length 0 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Multipole -name "CR2.DBA.SN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM10" -length 0
Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM11" -length 0
Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Multipole -name "CR2.DBA.SNN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM12" -length 0
Drift -name "CR2.DBA.HCM" -length 0 

Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.D12" -length 0.2697746031 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM13" -length 0
Drift -name "CR2.DBA.D11B1" -length 0.3424874764 

Multipole -name "CR2.DBA.SD0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.146568947*$e0] -e0 $e0 

Drift -name "CR2.DBA.DM0" -length 0.109313245 

Drift -name "CR2.DBA.HCM0" -length 0 

Quadrupole -name "CR2.DBA.QF1" -synrad $quad_synrad -length 0.4 -strength [expr -1.115855102*$e0] -e0 $e0 

Bpm -name "BPM14" -length 0
Drift -name "CR2.DBA.DM1" -length 1.973582219 

Quadrupole -name "CR2.DBA.QF2" -synrad $quad_synrad -length 0.4 -strength [expr 0.6764098228*$e0] -e0 $e0 

Bpm -name "BPM15" -length 0
Drift -name "CR2.DBA.DM2" -length 1.738896147 

Multipole -name "CR2.DBA.SD0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.146568947*$e0] -e0 $e0 

Drift -name "CR2.DBA.DMSEXT1" -length 0.109313245 

Quadrupole -name "CR2.DBA.QF3" -synrad $quad_synrad -length 0.4 -strength [expr -1.118935626*$e0] -e0 $e0 

Bpm -name "BPM16" -length 0
Drift -name "CR2.DBA.DM3" -length 0.2802607113 

Drift -name "CR2.DBA.DMSEXT1" -length 0.109313245 

Drift -name "CR2.DBA.D11B2" -length 0.1628018956 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM17" -length 0
Drift -name "CR2.DBA.D12" -length 0.2697746031 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM18" -length 0
Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Drift -name "CR2.DBA.HBM" -length 0 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Multipole -name "CR2.DBA.SN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM19" -length 0
Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM20" -length 0
Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Multipole -name "CR2.DBA.SNN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM21" -length 0
Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.D12" -length 0.2697746031 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM22" -length 0
Drift -name "CR2.DBA.D11B" -length 0.1628018956 

Multipole -name "CR2.DBA.SNCH1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.D11A" -length 0.1730305995 

Quadrupole -name "CR2.DBA.QI1" -synrad $quad_synrad -length 0.4 -strength [expr -0.5864293272*$e0] -e0 $e0 

Bpm -name "BPM23" -length 0
Drift -name "CR2.DBA.MCELL" -length 0 

Drift -name "CR2MDBACELLEND" -length 0 
Quadrupole -name "CR2.TEST2" -synrad $quad_synrad -length 0.0 -strength [expr 0.0*$e0] -e0 $e0 
Drift -name "CR2.DTR1A" -length 1.1 

Multipole -name "CR2.SXTR1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 12.54242064*$e0] -e0 $e0 

Drift -name "CR2.DTR1B" -length 0.2 

Quadrupole -name "CR2.QTR1" -synrad $quad_synrad -length 0.5 -strength [expr -0.371641026*$e0] -e0 $e0 

Bpm -name "BPM24" -length 0
Drift -name "CR2.DTR2A" -length 1.1 

Multipole -name "CR2.SXTR2" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -5.250757706*$e0] -e0 $e0 

Drift -name "CR2.DTR2B" -length 0.2 

Quadrupole -name "CR2.QTR2" -synrad $quad_synrad -length 0.5 -strength [expr 0.512918083*$e0] -e0 $e0 

Bpm -name "BPM25" -length 0
Drift -name "CR2.DTR3A" -length 1.5 

Multipole -name "CR2.SXTR3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -7.465446283*$e0] -e0 $e0 

Drift -name "CR2.DTR3B" -length 0.2 

Quadrupole -name "CR2.QTR3" -synrad $quad_synrad -length 0.5 -strength [expr -0.1592789427*$e0] -e0 $e0 

Bpm -name "BPM26" -length 0
Drift -name "CR2.DTR4A" -length 2.1328 

Multipole -name "CR2.SXTR4" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 20.76691673*$e0] -e0 $e0 

Drift -name "CR2.DTR4B" -length 0.2 

Quadrupole -name "CR2.QTR4" -synrad $quad_synrad -length 0.25 -strength [expr 0*$e0] -e0 $e0 

Bpm -name "BPM27" -length 0
Quadrupole -name "CR2.QTR4" -synrad $quad_synrad -length 0.25 -strength [expr 0*$e0] -e0 $e0 

Bpm -name "BPM28" -length 0
Drift -name "CR2.DTR4B" -length 0.2 

Multipole -name "CR2.SXTR4" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 20.76691673*$e0] -e0 $e0 

Drift -name "CR2.DTR4A" -length 2.1328 

Quadrupole -name "CR2.QTR3" -synrad $quad_synrad -length 0.5 -strength [expr -0.1592789427*$e0] -e0 $e0 

Bpm -name "BPM29" -length 0
Drift -name "CR2.DTR3B" -length 0.2 

Multipole -name "CR2.SXTR3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -7.465446283*$e0] -e0 $e0 

Drift -name "CR2.DTR3A" -length 1.5 

Quadrupole -name "CR2.QTR2" -synrad $quad_synrad -length 0.5 -strength [expr 0.512918083*$e0] -e0 $e0 

Bpm -name "BPM30" -length 0
Drift -name "CR2.DTR2B" -length 0.2 

Multipole -name "CR2.SXTR2" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -5.250757706*$e0] -e0 $e0 

Drift -name "CR2.DTR2A" -length 1.1 

Quadrupole -name "CR2.QTR1" -synrad $quad_synrad -length 0.5 -strength [expr -0.371641026*$e0] -e0 $e0 

Bpm -name "BPM31" -length 0
Drift -name "CR2.DTR1B" -length 0.2 

Multipole -name "CR2.SXTR1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 12.54242064*$e0] -e0 $e0 

Drift -name "CR2.DTR1A" -length 1.1 

Drift -name "CR2MDBACELLSTART" -length 0 

Drift -name "CR2.DBA.MCELL" -length 0 

Quadrupole -name "CR2.DBA.QI1" -synrad $quad_synrad -length 0.4 -strength [expr -0.5864293272*$e0] -e0 $e0 

Bpm -name "BPM32" -length 0
Drift -name "CR2.DBA.D11A" -length 0.1730305995 

Multipole -name "CR2.DBA.SNCH1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.D11B" -length 0.1628018956 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM33" -length 0
Drift -name "CR2.DBA.D12" -length 0.2697746031 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM34" -length 0
Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Drift -name "CR2.DBA.HBM" -length 0 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Multipole -name "CR2.DBA.SN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM35" -length 0
Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM36" -length 0
Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Multipole -name "CR2.DBA.SNN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM37" -length 0
Drift -name "CR2.DBA.HCM" -length 0 

Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.D12" -length 0.2697746031 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM38" -length 0
Drift -name "CR2.DBA.D11B1" -length 0.3424874764 

Multipole -name "CR2.DBA.SD0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.146568947*$e0] -e0 $e0 

Drift -name "CR2.DBA.DM0" -length 0.109313245 

Drift -name "CR2.DBA.HCM0" -length 0 

Quadrupole -name "CR2.DBA.QF1" -synrad $quad_synrad -length 0.4 -strength [expr -1.115855102*$e0] -e0 $e0 

Bpm -name "BPM39" -length 0
Drift -name "CR2.DBA.DM1" -length 1.973582219 

Quadrupole -name "CR2.DBA.QF2" -synrad $quad_synrad -length 0.4 -strength [expr 0.6764098228*$e0] -e0 $e0 

Bpm -name "BPM40" -length 0
Drift -name "CR2.DBA.DM2" -length 1.738896147 

Multipole -name "CR2.DBA.SD0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.146568947*$e0] -e0 $e0 

Drift -name "CR2.DBA.DMSEXT1" -length 0.109313245 

Quadrupole -name "CR2.DBA.QF3" -synrad $quad_synrad -length 0.4 -strength [expr -1.118935626*$e0] -e0 $e0 

Bpm -name "BPM41" -length 0
Drift -name "CR2.DBA.DM3" -length 0.2802607113 

Drift -name "CR2.DBA.DMSEXT1" -length 0.109313245 

Drift -name "CR2.DBA.D11B2" -length 0.1628018956 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM42" -length 0
Drift -name "CR2.DBA.D12" -length 0.2697746031 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM43" -length 0
Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Drift -name "CR2.DBA.HBM" -length 0 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Multipole -name "CR2.DBA.SN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM44" -length 0
Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM45" -length 0
Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Multipole -name "CR2.DBA.SNN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM46" -length 0
Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.D12" -length 0.2697746031 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM47" -length 0
Drift -name "CR2.DBA.D11B" -length 0.1628018956 

Multipole -name "CR2.DBA.SNCH1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.D11A" -length 0.1730305995 

Quadrupole -name "CR2.DBA.QI1" -synrad $quad_synrad -length 0.4 -strength [expr -0.5864293272*$e0] -e0 $e0 

Bpm -name "BPM48" -length 0
Drift -name "CR2.DBA.MCELL" -length 0 

Drift -name "CR2MDBACELLEND" -length 0 

Drift -name "CR2.DS1C_A" -length 2.179474654 

Multipole -name "CR2.SXS1C" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 11.05883366*$e0] -e0 $e0 

Drift -name "CR2.DS1C_B" -length 0.3 

Quadrupole -name "CR2.QS1C" -synrad $quad_synrad -length 0.5 -strength [expr -0.66814696*$e0] -e0 $e0 

Bpm -name "BPM49" -length 0
Drift -name "CR2.DS2C" -length 0.2003907272 

Quadrupole -name "CR2.QS2C" -synrad $quad_synrad -length 0.5 -strength [expr 0.7440336365*$e0] -e0 $e0 

Bpm -name "BPM50" -length 0
Drift -name "CR2.DS3C_A" -length 0.3 

Multipole -name "CR2.SXS3C" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -13.03004983*$e0] -e0 $e0 

Drift -name "CR2.DS3C_B" -length 3.777418619 

Quadrupole -name "CR2.QS3C" -synrad $quad_synrad -length 0.5 -strength [expr -0.03237714152*$e0] -e0 $e0 

Bpm -name "BPM51" -length 0
Drift -name "CR2.DS4C" -length 0.2407160001 

Drift -name "CR2.BC1" -length 0.4 

Drift -name "CR2.BC2" -length 0.4 

Drift -name "CR2.BC3" -length 0.4 

Drift -name "CR2.BC1" -length 0.4 

Drift -name "CR2.DS4C" -length 0.2407160001 

Quadrupole -name "CR2.QS3C" -synrad $quad_synrad -length 0.5 -strength [expr -0.03237714152*$e0] -e0 $e0 

Bpm -name "BPM52" -length 0
Drift -name "CR2.DS3C_B" -length 3.777418619 

Multipole -name "CR2.SXS3C" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -13.03004983*$e0] -e0 $e0 

Drift -name "CR2.DS3C_A" -length 0.3 

Quadrupole -name "CR2.QS2C" -synrad $quad_synrad -length 0.5 -strength [expr 0.7440336365*$e0] -e0 $e0 

Bpm -name "BPM53" -length 0
Drift -name "CR2.DS2C" -length 0.2003907272 

Quadrupole -name "CR2.QS1C" -synrad $quad_synrad -length 0.5 -strength [expr -0.66814696*$e0] -e0 $e0 

Bpm -name "BPM54" -length 0
Drift -name "CR2.DS1C_B" -length 0.3 

Multipole -name "CR2.SXS1C" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 11.05883366*$e0] -e0 $e0 

Drift -name "CR2.DS1C_A" -length 2.179474654 

Drift -name "CR2MDBACELLSTART" -length 0 

Drift -name "CR2.DBA.MCELL" -length 0 

Quadrupole -name "CR2.DBA.QI1" -synrad $quad_synrad -length 0.4 -strength [expr -0.5864293272*$e0] -e0 $e0 

Bpm -name "BPM55" -length 0
Drift -name "CR2.DBA.D11A" -length 0.1730305995 

Multipole -name "CR2.DBA.SNCH1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.D11B" -length 0.1628018956 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM56" -length 0
Drift -name "CR2.DBA.D12" -length 0.2697746031 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM57" -length 0
Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Drift -name "CR2.DBA.HBM" -length 0 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Multipole -name "CR2.DBA.SN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM58" -length 0
Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM59" -length 0
Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Multipole -name "CR2.DBA.SNN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM60" -length 0
Drift -name "CR2.DBA.HCM" -length 0 

Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.D12" -length 0.2697746031 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM61" -length 0
Drift -name "CR2.DBA.D11B1" -length 0.3424874764 

Multipole -name "CR2.DBA.SD0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.146568947*$e0] -e0 $e0 

Drift -name "CR2.DBA.DM0" -length 0.109313245 

Drift -name "CR2.DBA.HCM0" -length 0 

Quadrupole -name "CR2.DBA.QF1" -synrad $quad_synrad -length 0.4 -strength [expr -1.115855102*$e0] -e0 $e0 

Bpm -name "BPM62" -length 0
Drift -name "CR2.DBA.DM1" -length 1.973582219 

Quadrupole -name "CR2.DBA.QF2" -synrad $quad_synrad -length 0.4 -strength [expr 0.6764098228*$e0] -e0 $e0 

Bpm -name "BPM63" -length 0
Drift -name "CR2.DBA.DM2" -length 1.738896147 

Multipole -name "CR2.DBA.SD0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.146568947*$e0] -e0 $e0 

Drift -name "CR2.DBA.DMSEXT1" -length 0.109313245 

Quadrupole -name "CR2.DBA.QF3" -synrad $quad_synrad -length 0.4 -strength [expr -1.118935626*$e0] -e0 $e0 

Bpm -name "BPM64" -length 0
Drift -name "CR2.DBA.DM3" -length 0.2802607113 

Drift -name "CR2.DBA.DMSEXT1" -length 0.109313245 

Drift -name "CR2.DBA.D11B2" -length 0.1628018956 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM65" -length 0
Drift -name "CR2.DBA.D12" -length 0.2697746031 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM66" -length 0
Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Drift -name "CR2.DBA.HBM" -length 0 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Multipole -name "CR2.DBA.SN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM67" -length 0
Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM68" -length 0
Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Multipole -name "CR2.DBA.SNN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM69" -length 0
Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.D12" -length 0.2697746031 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM70" -length 0
Drift -name "CR2.DBA.D11B" -length 0.1628018956 

Multipole -name "CR2.DBA.SNCH1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.D11A" -length 0.1730305995 

Quadrupole -name "CR2.DBA.QI1" -synrad $quad_synrad -length 0.4 -strength [expr -0.5864293272*$e0] -e0 $e0 

Bpm -name "BPM71" -length 0
Drift -name "CR2.DBA.MCELL" -length 0 

Drift -name "CR2MDBACELLEND" -length 0 

Drift -name "CR2.DTR1A" -length 1.1 

Multipole -name "CR2.SXTR1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 12.54242064*$e0] -e0 $e0 

Drift -name "CR2.DTR1B" -length 0.2 

Quadrupole -name "CR2.QTR1" -synrad $quad_synrad -length 0.5 -strength [expr -0.371641026*$e0] -e0 $e0 

Bpm -name "BPM72" -length 0
Drift -name "CR2.DTR2A" -length 1.1 

Multipole -name "CR2.SXTR2" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -5.250757706*$e0] -e0 $e0 

Drift -name "CR2.DTR2B" -length 0.2 

Quadrupole -name "CR2.QTR2" -synrad $quad_synrad -length 0.5 -strength [expr 0.512918083*$e0] -e0 $e0 

Bpm -name "BPM73" -length 0
Drift -name "CR2.DTR3A" -length 1.5 

Multipole -name "CR2.SXTR3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -7.465446283*$e0] -e0 $e0 

Drift -name "CR2.DTR3B" -length 0.2 

Quadrupole -name "CR2.QTR3" -synrad $quad_synrad -length 0.5 -strength [expr -0.1592789427*$e0] -e0 $e0 

Bpm -name "BPM74" -length 0
Drift -name "CR2.DTR4A" -length 2.1328 

Multipole -name "CR2.SXTR4" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 20.76691673*$e0] -e0 $e0 

Drift -name "CR2.DTR4B" -length 0.2 

Quadrupole -name "CR2.QTR4" -synrad $quad_synrad -length 0.25 -strength [expr 0*$e0] -e0 $e0 

Bpm -name "BPM75" -length 0
Quadrupole -name "CR2.QTR4" -synrad $quad_synrad -length 0.25 -strength [expr 0*$e0] -e0 $e0 

Bpm -name "BPM76" -length 0
Drift -name "CR2.DTR4B" -length 0.2 

Multipole -name "CR2.SXTR4" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 20.76691673*$e0] -e0 $e0 

Drift -name "CR2.DTR4A" -length 2.1328 

Quadrupole -name "CR2.QTR3" -synrad $quad_synrad -length 0.5 -strength [expr -0.1592789427*$e0] -e0 $e0 

Bpm -name "BPM77" -length 0
Drift -name "CR2.DTR3B" -length 0.2 

Multipole -name "CR2.SXTR3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -7.465446283*$e0] -e0 $e0 

Drift -name "CR2.DTR3A" -length 1.5 

Quadrupole -name "CR2.QTR2" -synrad $quad_synrad -length 0.5 -strength [expr 0.512918083*$e0] -e0 $e0 

Bpm -name "BPM78" -length 0
Drift -name "CR2.DTR2B" -length 0.2 

Multipole -name "CR2.SXTR2" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -5.250757706*$e0] -e0 $e0 

Drift -name "CR2.DTR2A" -length 1.1 

Quadrupole -name "CR2.QTR1" -synrad $quad_synrad -length 0.5 -strength [expr -0.371641026*$e0] -e0 $e0 

Bpm -name "BPM79" -length 0
Drift -name "CR2.DTR1B" -length 0.2 

Multipole -name "CR2.SXTR1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 12.54242064*$e0] -e0 $e0 

Drift -name "CR2.DTR1A" -length 1.1 

Drift -name "CR2MDBACELLSTART" -length 0 

Drift -name "CR2.DBA.MCELL" -length 0 

Quadrupole -name "CR2.DBA.QI1" -synrad $quad_synrad -length 0.4 -strength [expr -0.5864293272*$e0] -e0 $e0 

Bpm -name "BPM80" -length 0
Drift -name "CR2.DBA.D11A" -length 0.1730305995 

Multipole -name "CR2.DBA.SNCH1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.D11B" -length 0.1628018956 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM81" -length 0
Drift -name "CR2.DBA.D12" -length 0.2697746031 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM82" -length 0
Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Drift -name "CR2.DBA.HBM" -length 0 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Multipole -name "CR2.DBA.SN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM83" -length 0
Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM84" -length 0
Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Multipole -name "CR2.DBA.SNN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM85" -length 0
Drift -name "CR2.DBA.HCM" -length 0 

Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.D12" -length 0.2697746031 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM86" -length 0
Drift -name "CR2.DBA.D11B1" -length 0.3424874764 

Multipole -name "CR2.DBA.SD0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.146568947*$e0] -e0 $e0 

Drift -name "CR2.DBA.DM0" -length 0.109313245 

Drift -name "CR2.DBA.HCM0" -length 0 

Quadrupole -name "CR2.DBA.QF1" -synrad $quad_synrad -length 0.4 -strength [expr -1.115855102*$e0] -e0 $e0 

Bpm -name "BPM87" -length 0
Drift -name "CR2.DBA.DM1" -length 1.973582219 

Quadrupole -name "CR2.DBA.QF2" -synrad $quad_synrad -length 0.4 -strength [expr 0.6764098228*$e0] -e0 $e0 

Bpm -name "BPM88" -length 0
Drift -name "CR2.DBA.DM2" -length 1.738896147 

Multipole -name "CR2.DBA.SD0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.146568947*$e0] -e0 $e0 

Drift -name "CR2.DBA.DMSEXT1" -length 0.109313245 

Quadrupole -name "CR2.DBA.QF3" -synrad $quad_synrad -length 0.4 -strength [expr -1.118935626*$e0] -e0 $e0 

Bpm -name "BPM89" -length 0
Drift -name "CR2.DBA.DM3" -length 0.2802607113 

Drift -name "CR2.DBA.DMSEXT1" -length 0.109313245 

Drift -name "CR2.DBA.D11B2" -length 0.1628018956 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM90" -length 0
Drift -name "CR2.DBA.D12" -length 0.2697746031 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM91" -length 0
Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Drift -name "CR2.DBA.HBM" -length 0 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Multipole -name "CR2.DBA.SN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM92" -length 0
Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM93" -length 0
Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Multipole -name "CR2.DBA.SNN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM94" -length 0
Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.D12" -length 0.2697746031 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM95" -length 0
Drift -name "CR2.DBA.D11B" -length 0.1628018956 

Multipole -name "CR2.DBA.SNCH1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.D11A" -length 0.1730305995 

Quadrupole -name "CR2.DBA.QI1" -synrad $quad_synrad -length 0.4 -strength [expr -0.5864293272*$e0] -e0 $e0 

Bpm -name "BPM96" -length 0
Drift -name "CR2.DBA.MCELL" -length 0 

Drift -name "CR2MDBACELLEND" -length 0 

Drift -name "CR2.DL0IN" -length 2.246504682 

Quadrupole -name "CR2.QL1IN" -synrad $quad_synrad -length 0.5 -strength [expr -0.642778543*$e0] -e0 $e0 

Bpm -name "BPM97" -length 0
Drift -name "CR2.DL1IN" -length 0.4000469975 

Quadrupole -name "CR2.QL2IN" -synrad $quad_synrad -length 0.5 -strength [expr 0.720003675*$e0] -e0 $e0 

Bpm -name "BPM98" -length 0
Drift -name "CR2.DL2IN" -length 1.103409345 

Quadrupole -name "CR2.QL3IN" -synrad $quad_synrad -length 0.5 -strength [expr -0.2675265744*$e0] -e0 $e0 

Bpm -name "BPM99" -length 0
Drift -name "CR2.DL3IN" -length 0.4000389758 

Drift -name "CR2.MENTEXTKCK" -length 0 

Drift -name "CR2.DRRF" -length 2 

Drift -name "CR2.DRFB1" -length 0.217192186 

Quadrupole -name "CR2.QRFB1" -synrad $quad_synrad -length 0.5 -strength [expr -1.08870911*$e0] -e0 $e0 

Bpm -name "BPM100" -length 0
Drift -name "CR2.DRFB2" -length 1.193240969 

Quadrupole -name "CR2.QRFB2" -synrad $quad_synrad -length 0.5 -strength [expr 0.5775372505*$e0] -e0 $e0 

Bpm -name "BPM101" -length 0
Drift -name "CR2.DRFB3" -length 3.771233433 

Quadrupole -name "CR2.QRFB3" -synrad $quad_synrad -length 0.5 -strength [expr -0.3437279899*$e0] -e0 $e0 

Bpm -name "BPM102" -length 0
Drift -name "CR2.D1CM" -length 0.01 

Drift -name "CR2.D1M" -length 1 

Drift -name "CR2.D1M" -length 1 

Drift -name "CR2.D1M" -length 1 

Drift -name "CR2.DRFB4A" -length 0.709135479 
Bpm -name "CR2.EJECTION" -length 0

Drift -name "CR2.DSRFB6" -length 0.4 

Drift -name "CR2.DRFB6" -length 2.367622162 

Drift -name "CR2.SEPTUM" -length 1 

Drift -name "CR2.SEPTUM" -length 1 

Drift -name "CR2.DRFB6" -length 2.367622162 

Drift -name "CR2.DSRFB6" -length 0.4 

Drift -name "CR2.DRFB4A" -length 0.709135479 

Drift -name "CR2.D1M" -length 1 

Drift -name "CR2.D1M" -length 1 

Drift -name "CR2.D1M" -length 1 

Drift -name "CR2.D1CM" -length 0.01 

Quadrupole -name "CR2.QRFB3" -synrad $quad_synrad -length 0.5 -strength [expr -0.3437279899*$e0] -e0 $e0 

Bpm -name "BPM103" -length 0
Drift -name "CR2.DRFB3" -length 3.771233433 

Quadrupole -name "CR2.QRFB2" -synrad $quad_synrad -length 0.5 -strength [expr 0.5775372505*$e0] -e0 $e0 

Bpm -name "BPM104" -length 0
Drift -name "CR2.DRFB2" -length 1.193240969 

Quadrupole -name "CR2.QRFB1" -synrad $quad_synrad -length 0.5 -strength [expr -1.08870911*$e0] -e0 $e0 

Bpm -name "BPM105" -length 0
Drift -name "CR2.DRFB1" -length 0.217192186 

Drift -name "CR2.DRRF" -length 2 

Drift -name "CR2.MENTEXTKCK" -length 0 

Drift -name "CR2.DL3IN" -length 0.4000389758 

Quadrupole -name "CR2.QL3IN" -synrad $quad_synrad -length 0.5 -strength [expr -0.2675265744*$e0] -e0 $e0 

Bpm -name "BPM106" -length 0
Drift -name "CR2.DL2IN" -length 1.103409345 

Quadrupole -name "CR2.QL2IN" -synrad $quad_synrad -length 0.5 -strength [expr 0.720003675*$e0] -e0 $e0 

Bpm -name "BPM107" -length 0
Drift -name "CR2.DL1IN" -length 0.4000469975 

Quadrupole -name "CR2.QL1IN" -synrad $quad_synrad -length 0.5 -strength [expr -0.642778543*$e0] -e0 $e0 

Bpm -name "BPM108" -length 0
Drift -name "CR2.DL0IN" -length 2.246504682 

Drift -name "CR2MDBACELLSTART" -length 0 

Drift -name "CR2.DBA.MCELL" -length 0 

Quadrupole -name "CR2.DBA.QI1" -synrad $quad_synrad -length 0.4 -strength [expr -0.5864293272*$e0] -e0 $e0 

Bpm -name "BPM109" -length 0
Drift -name "CR2.DBA.D11A" -length 0.1730305995 

Multipole -name "CR2.DBA.SNCH1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.D11B" -length 0.1628018956 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM110" -length 0
Drift -name "CR2.DBA.D12" -length 0.2697746031 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM111" -length 0
Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Drift -name "CR2.DBA.HBM" -length 0 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Multipole -name "CR2.DBA.SN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM112" -length 0
Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM113" -length 0
Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Multipole -name "CR2.DBA.SNN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM114" -length 0
Drift -name "CR2.DBA.HCM" -length 0 

Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.D12" -length 0.2697746031 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM115" -length 0
Drift -name "CR2.DBA.D11B1" -length 0.3424874764 

Multipole -name "CR2.DBA.SD0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.146568947*$e0] -e0 $e0 

Drift -name "CR2.DBA.DM0" -length 0.109313245 

Drift -name "CR2.DBA.HCM0" -length 0 

Quadrupole -name "CR2.DBA.QF1" -synrad $quad_synrad -length 0.4 -strength [expr -1.115855102*$e0] -e0 $e0 

Bpm -name "BPM116" -length 0
Drift -name "CR2.DBA.DM1" -length 1.973582219 

Quadrupole -name "CR2.DBA.QF2" -synrad $quad_synrad -length 0.4 -strength [expr 0.6764098228*$e0] -e0 $e0 

Bpm -name "BPM117" -length 0
Drift -name "CR2.DBA.DM2" -length 1.738896147 

Multipole -name "CR2.DBA.SD0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.146568947*$e0] -e0 $e0 

Drift -name "CR2.DBA.DMSEXT1" -length 0.109313245 

Quadrupole -name "CR2.DBA.QF3" -synrad $quad_synrad -length 0.4 -strength [expr -1.118935626*$e0] -e0 $e0 

Bpm -name "BPM118" -length 0
Drift -name "CR2.DBA.DM3" -length 0.2802607113 

Drift -name "CR2.DBA.DMSEXT1" -length 0.109313245 

Drift -name "CR2.DBA.D11B2" -length 0.1628018956 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM119" -length 0
Drift -name "CR2.DBA.D12" -length 0.2697746031 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM120" -length 0
Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Drift -name "CR2.DBA.HBM" -length 0 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Multipole -name "CR2.DBA.SN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM121" -length 0
Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM122" -length 0
Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Multipole -name "CR2.DBA.SNN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM123" -length 0
Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.D12" -length 0.2697746031 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM124" -length 0
Drift -name "CR2.DBA.D11B" -length 0.1628018956 

Multipole -name "CR2.DBA.SNCH1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.D11A" -length 0.1730305995 

Quadrupole -name "CR2.DBA.QI1" -synrad $quad_synrad -length 0.4 -strength [expr -0.5864293272*$e0] -e0 $e0 

Bpm -name "BPM125" -length 0
Drift -name "CR2.DBA.MCELL" -length 0 

Drift -name "CR2MDBACELLEND" -length 0 

Drift -name "CR2.DTR1A" -length 1.1 

Multipole -name "CR2.SXTR1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 12.54242064*$e0] -e0 $e0 

Drift -name "CR2.DTR1B" -length 0.2 

Quadrupole -name "CR2.QTR1" -synrad $quad_synrad -length 0.5 -strength [expr -0.371641026*$e0] -e0 $e0 

Bpm -name "BPM126" -length 0
Drift -name "CR2.DTR2A" -length 1.1 

Multipole -name "CR2.SXTR2" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -5.250757706*$e0] -e0 $e0 

Drift -name "CR2.DTR2B" -length 0.2 

Quadrupole -name "CR2.QTR2" -synrad $quad_synrad -length 0.5 -strength [expr 0.512918083*$e0] -e0 $e0 

Bpm -name "BPM127" -length 0
Drift -name "CR2.DTR3A" -length 1.5 

Multipole -name "CR2.SXTR3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -7.465446283*$e0] -e0 $e0 

Drift -name "CR2.DTR3B" -length 0.2 

Quadrupole -name "CR2.QTR3" -synrad $quad_synrad -length 0.5 -strength [expr -0.1592789427*$e0] -e0 $e0 

Bpm -name "BPM128" -length 0
Drift -name "CR2.DTR4A" -length 2.1328 

Multipole -name "CR2.SXTR4" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 20.76691673*$e0] -e0 $e0 

Drift -name "CR2.DTR4B" -length 0.2 

Quadrupole -name "CR2.QTR4" -synrad $quad_synrad -length 0.25 -strength [expr 0*$e0] -e0 $e0 

Bpm -name "BPM129" -length 0
Quadrupole -name "CR2.QTR4" -synrad $quad_synrad -length 0.25 -strength [expr 0*$e0] -e0 $e0 

Bpm -name "BPM130" -length 0
Drift -name "CR2.DTR4B" -length 0.2 

Multipole -name "CR2.SXTR4" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 20.76691673*$e0] -e0 $e0 

Drift -name "CR2.DTR4A" -length 2.1328 

Quadrupole -name "CR2.QTR3" -synrad $quad_synrad -length 0.5 -strength [expr -0.1592789427*$e0] -e0 $e0 

Bpm -name "BPM131" -length 0
Drift -name "CR2.DTR3B" -length 0.2 

Multipole -name "CR2.SXTR3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -7.465446283*$e0] -e0 $e0 

Drift -name "CR2.DTR3A" -length 1.5 

Quadrupole -name "CR2.QTR2" -synrad $quad_synrad -length 0.5 -strength [expr 0.512918083*$e0] -e0 $e0 

Bpm -name "BPM132" -length 0
Drift -name "CR2.DTR2B" -length 0.2 

Multipole -name "CR2.SXTR2" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -5.250757706*$e0] -e0 $e0 

Drift -name "CR2.DTR2A" -length 1.1 

Quadrupole -name "CR2.QTR1" -synrad $quad_synrad -length 0.5 -strength [expr -0.371641026*$e0] -e0 $e0 

Bpm -name "BPM133" -length 0
Drift -name "CR2.DTR1B" -length 0.2 

Multipole -name "CR2.SXTR1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 12.54242064*$e0] -e0 $e0 

Drift -name "CR2.DTR1A" -length 1.1 

Drift -name "CR2MDBACELLSTART" -length 0 

Drift -name "CR2.DBA.MCELL" -length 0 

Quadrupole -name "CR2.DBA.QI1" -synrad $quad_synrad -length 0.4 -strength [expr -0.5864293272*$e0] -e0 $e0 

Bpm -name "BPM134" -length 0
Drift -name "CR2.DBA.D11A" -length 0.1730305995 

Multipole -name "CR2.DBA.SNCH1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.D11B" -length 0.1628018956 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM135" -length 0
Drift -name "CR2.DBA.D12" -length 0.2697746031 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM136" -length 0
Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Drift -name "CR2.DBA.HBM" -length 0 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Multipole -name "CR2.DBA.SN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM137" -length 0
Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM138" -length 0
Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Multipole -name "CR2.DBA.SNN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM139" -length 0
Drift -name "CR2.DBA.HCM" -length 0 

Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.D12" -length 0.2697746031 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM140" -length 0
Drift -name "CR2.DBA.D11B1" -length 0.3424874764 

Multipole -name "CR2.DBA.SD0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.146568947*$e0] -e0 $e0 

Drift -name "CR2.DBA.DM0" -length 0.109313245 

Drift -name "CR2.DBA.HCM0" -length 0 

Quadrupole -name "CR2.DBA.QF1" -synrad $quad_synrad -length 0.4 -strength [expr -1.115855102*$e0] -e0 $e0 

Bpm -name "BPM141" -length 0
Drift -name "CR2.DBA.DM1" -length 1.973582219 

Quadrupole -name "CR2.DBA.QF2" -synrad $quad_synrad -length 0.4 -strength [expr 0.6764098228*$e0] -e0 $e0 

Bpm -name "BPM142" -length 0
Drift -name "CR2.DBA.DM2" -length 1.738896147 

Multipole -name "CR2.DBA.SD0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.146568947*$e0] -e0 $e0 

Drift -name "CR2.DBA.DMSEXT1" -length 0.109313245 

Quadrupole -name "CR2.DBA.QF3" -synrad $quad_synrad -length 0.4 -strength [expr -1.118935626*$e0] -e0 $e0 

Bpm -name "BPM143" -length 0
Drift -name "CR2.DBA.DM3" -length 0.2802607113 

Drift -name "CR2.DBA.DMSEXT1" -length 0.109313245 

Drift -name "CR2.DBA.D11B2" -length 0.1628018956 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM144" -length 0
Drift -name "CR2.DBA.D12" -length 0.2697746031 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM145" -length 0
Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Drift -name "CR2.DBA.HBM" -length 0 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Multipole -name "CR2.DBA.SN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM146" -length 0
Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM147" -length 0
Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Multipole -name "CR2.DBA.SNN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM148" -length 0
Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.D12" -length 0.2697746031 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM149" -length 0
Drift -name "CR2.DBA.D11B" -length 0.1628018956 

Multipole -name "CR2.DBA.SNCH1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.D11A" -length 0.1730305995 

Quadrupole -name "CR2.DBA.QI1" -synrad $quad_synrad -length 0.4 -strength [expr -0.5864293272*$e0] -e0 $e0 

Bpm -name "BPM150" -length 0
Drift -name "CR2.DBA.MCELL" -length 0 

Drift -name "CR2MDBACELLEND" -length 0 

Drift -name "CR2.DS1C_A" -length 2.179474654 

Multipole -name "CR2.SXS1C" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 11.05883366*$e0] -e0 $e0 

Drift -name "CR2.DS1C_B" -length 0.3 

Quadrupole -name "CR2.QS1C" -synrad $quad_synrad -length 0.5 -strength [expr -0.66814696*$e0] -e0 $e0 

Bpm -name "BPM151" -length 0
Drift -name "CR2.DS2C" -length 0.2003907272 

Quadrupole -name "CR2.QS2C" -synrad $quad_synrad -length 0.5 -strength [expr 0.7440336365*$e0] -e0 $e0 

Bpm -name "BPM152" -length 0
Drift -name "CR2.DS3C_A" -length 0.3 

Multipole -name "CR2.SXS3C" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -13.03004983*$e0] -e0 $e0 

Drift -name "CR2.DS3C_B" -length 3.777418619 

Quadrupole -name "CR2.QS3C" -synrad $quad_synrad -length 0.5 -strength [expr -0.03237714152*$e0] -e0 $e0 

Bpm -name "BPM153" -length 0
Drift -name "CR2.DS4C" -length 0.2407160001 

Drift -name "CR2.BC1" -length 0.4 

Drift -name "CR2.BC2" -length 0.4 

Drift -name "CR2.BC3" -length 0.4 

Drift -name "CR2.BC1" -length 0.4 

Drift -name "CR2.DS4C" -length 0.2407160001 

Quadrupole -name "CR2.QS3C" -synrad $quad_synrad -length 0.5 -strength [expr -0.03237714152*$e0] -e0 $e0 

Bpm -name "BPM154" -length 0
Drift -name "CR2.DS3C_B" -length 3.777418619 

Multipole -name "CR2.SXS3C" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -13.03004983*$e0] -e0 $e0 

Drift -name "CR2.DS3C_A" -length 0.3 

Quadrupole -name "CR2.QS2C" -synrad $quad_synrad -length 0.5 -strength [expr 0.7440336365*$e0] -e0 $e0 

Bpm -name "BPM155" -length 0
Drift -name "CR2.DS2C" -length 0.2003907272 

Quadrupole -name "CR2.QS1C" -synrad $quad_synrad -length 0.5 -strength [expr -0.66814696*$e0] -e0 $e0 

Bpm -name "BPM156" -length 0
Drift -name "CR2.DS1C_B" -length 0.3 

Multipole -name "CR2.SXS1C" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 11.05883366*$e0] -e0 $e0 

Drift -name "CR2.DS1C_A" -length 2.179474654 

Drift -name "CR2MDBACELLSTART" -length 0 

Drift -name "CR2.DBA.MCELL" -length 0 

Quadrupole -name "CR2.DBA.QI1" -synrad $quad_synrad -length 0.4 -strength [expr -0.5864293272*$e0] -e0 $e0 

Bpm -name "BPM157" -length 0
Drift -name "CR2.DBA.D11A" -length 0.1730305995 

Multipole -name "CR2.DBA.SNCH1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.D11B" -length 0.1628018956 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM158" -length 0
Drift -name "CR2.DBA.D12" -length 0.2697746031 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM159" -length 0
Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Drift -name "CR2.DBA.HBM" -length 0 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Multipole -name "CR2.DBA.SN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM160" -length 0
Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM161" -length 0
Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Multipole -name "CR2.DBA.SNN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM162" -length 0
Drift -name "CR2.DBA.HCM" -length 0 

Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.D12" -length 0.2697746031 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM163" -length 0
Drift -name "CR2.DBA.D11B1" -length 0.3424874764 

Multipole -name "CR2.DBA.SD0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.146568947*$e0] -e0 $e0 

Drift -name "CR2.DBA.DM0" -length 0.109313245 

Drift -name "CR2.DBA.HCM0" -length 0 

Quadrupole -name "CR2.DBA.QF1" -synrad $quad_synrad -length 0.4 -strength [expr -1.115855102*$e0] -e0 $e0 

Bpm -name "BPM164" -length 0
Drift -name "CR2.DBA.DM1" -length 1.973582219 

Quadrupole -name "CR2.DBA.QF2" -synrad $quad_synrad -length 0.4 -strength [expr 0.6764098228*$e0] -e0 $e0 

Bpm -name "BPM165" -length 0
Drift -name "CR2.DBA.DM2" -length 1.738896147 

Multipole -name "CR2.DBA.SD0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.146568947*$e0] -e0 $e0 

Drift -name "CR2.DBA.DMSEXT1" -length 0.109313245 

Quadrupole -name "CR2.DBA.QF3" -synrad $quad_synrad -length 0.4 -strength [expr -1.118935626*$e0] -e0 $e0 

Bpm -name "BPM166" -length 0
Drift -name "CR2.DBA.DM3" -length 0.2802607113 

Drift -name "CR2.DBA.DMSEXT1" -length 0.109313245 

Drift -name "CR2.DBA.D11B2" -length 0.1628018956 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM167" -length 0
Drift -name "CR2.DBA.D12" -length 0.2697746031 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM168" -length 0
Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Drift -name "CR2.DBA.HBM" -length 0 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Multipole -name "CR2.DBA.SN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM169" -length 0
Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM170" -length 0
Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Multipole -name "CR2.DBA.SNN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM171" -length 0
Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.D12" -length 0.2697746031 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM172" -length 0
Drift -name "CR2.DBA.D11B" -length 0.1628018956 

Multipole -name "CR2.DBA.SNCH1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.D11A" -length 0.1730305995 

Quadrupole -name "CR2.DBA.QI1" -synrad $quad_synrad -length 0.4 -strength [expr -0.5864293272*$e0] -e0 $e0 

Bpm -name "BPM173" -length 0
Drift -name "CR2.DBA.MCELL" -length 0 

Drift -name "CR2MDBACELLEND" -length 0 

Drift -name "CR2.DTR1A" -length 1.1 

Multipole -name "CR2.SXTR1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 12.54242064*$e0] -e0 $e0 

Drift -name "CR2.DTR1B" -length 0.2 

Quadrupole -name "CR2.QTR1" -synrad $quad_synrad -length 0.5 -strength [expr -0.371641026*$e0] -e0 $e0 

Bpm -name "BPM174" -length 0
Drift -name "CR2.DTR2A" -length 1.1 

Multipole -name "CR2.SXTR2" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -5.250757706*$e0] -e0 $e0 

Drift -name "CR2.DTR2B" -length 0.2 

Quadrupole -name "CR2.QTR2" -synrad $quad_synrad -length 0.5 -strength [expr 0.512918083*$e0] -e0 $e0 

Bpm -name "BPM175" -length 0
Drift -name "CR2.DTR3A" -length 1.5 

Multipole -name "CR2.SXTR3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -7.465446283*$e0] -e0 $e0 

Drift -name "CR2.DTR3B" -length 0.2 

Quadrupole -name "CR2.QTR3" -synrad $quad_synrad -length 0.5 -strength [expr -0.1592789427*$e0] -e0 $e0 

Bpm -name "BPM176" -length 0
Drift -name "CR2.DTR4A" -length 2.1328 

Multipole -name "CR2.SXTR4" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 20.76691673*$e0] -e0 $e0 

Drift -name "CR2.DTR4B" -length 0.2 

Quadrupole -name "CR2.QTR4" -synrad $quad_synrad -length 0.25 -strength [expr 0*$e0] -e0 $e0 

Bpm -name "BPM177" -length 0
Quadrupole -name "CR2.QTR4" -synrad $quad_synrad -length 0.25 -strength [expr 0*$e0] -e0 $e0 

Bpm -name "BPM178" -length 0
Drift -name "CR2.DTR4B" -length 0.2 

Multipole -name "CR2.SXTR4" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 20.76691673*$e0] -e0 $e0 

Drift -name "CR2.DTR4A" -length 2.1328 

Quadrupole -name "CR2.QTR3" -synrad $quad_synrad -length 0.5 -strength [expr -0.1592789427*$e0] -e0 $e0 

Bpm -name "BPM179" -length 0
Drift -name "CR2.DTR3B" -length 0.2 

Multipole -name "CR2.SXTR3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -7.465446283*$e0] -e0 $e0 

Drift -name "CR2.DTR3A" -length 1.5 

Quadrupole -name "CR2.QTR2" -synrad $quad_synrad -length 0.5 -strength [expr 0.512918083*$e0] -e0 $e0 

Bpm -name "BPM180" -length 0
Drift -name "CR2.DTR2B" -length 0.2 

Multipole -name "CR2.SXTR2" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -5.250757706*$e0] -e0 $e0 

Drift -name "CR2.DTR2A" -length 1.1 

Quadrupole -name "CR2.QTR1" -synrad $quad_synrad -length 0.5 -strength [expr -0.371641026*$e0] -e0 $e0 

Bpm -name "BPM181" -length 0
Drift -name "CR2.DTR1B" -length 0.2 

Multipole -name "CR2.SXTR1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 12.54242064*$e0] -e0 $e0 

Drift -name "CR2.DTR1A" -length 1.1 

Drift -name "CR2MDBACELLSTART" -length 0 

Drift -name "CR2.DBA.MCELL" -length 0 

Quadrupole -name "CR2.DBA.QI1" -synrad $quad_synrad -length 0.4 -strength [expr -0.5864293272*$e0] -e0 $e0 

Bpm -name "BPM182" -length 0
Drift -name "CR2.DBA.D11A" -length 0.1730305995 

Multipole -name "CR2.DBA.SNCH1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.D11B" -length 0.1628018956 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM183" -length 0
Drift -name "CR2.DBA.D12" -length 0.2697746031 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM184" -length 0
Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Drift -name "CR2.DBA.HBM" -length 0 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Multipole -name "CR2.DBA.SN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM185" -length 0
Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM186" -length 0
Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Multipole -name "CR2.DBA.SNN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM187" -length 0
Drift -name "CR2.DBA.HCM" -length 0 

Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.D12" -length 0.2697746031 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM188" -length 0
Drift -name "CR2.DBA.D11B1" -length 0.3424874764 

Multipole -name "CR2.DBA.SD0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.146568947*$e0] -e0 $e0 

Drift -name "CR2.DBA.DM0" -length 0.109313245 

Drift -name "CR2.DBA.HCM0" -length 0 

Quadrupole -name "CR2.DBA.QF1" -synrad $quad_synrad -length 0.4 -strength [expr -1.115855102*$e0] -e0 $e0 

Bpm -name "BPM189" -length 0
Drift -name "CR2.DBA.DM1" -length 1.973582219 

Quadrupole -name "CR2.DBA.QF2" -synrad $quad_synrad -length 0.4 -strength [expr 0.6764098228*$e0] -e0 $e0 

Bpm -name "BPM190" -length 0
Drift -name "CR2.DBA.DM2" -length 1.738896147 

Multipole -name "CR2.DBA.SD0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.146568947*$e0] -e0 $e0 

Drift -name "CR2.DBA.DMSEXT1" -length 0.109313245 

Quadrupole -name "CR2.DBA.QF3" -synrad $quad_synrad -length 0.4 -strength [expr -1.118935626*$e0] -e0 $e0 

Bpm -name "BPM191" -length 0
Drift -name "CR2.DBA.DM3" -length 0.2802607113 

Drift -name "CR2.DBA.DMSEXT1" -length 0.109313245 

Drift -name "CR2.DBA.D11B2" -length 0.1628018956 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM192" -length 0
Drift -name "CR2.DBA.D12" -length 0.2697746031 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM193" -length 0
Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Drift -name "CR2.DBA.HBM" -length 0 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Multipole -name "CR2.DBA.SN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM194" -length 0
Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM195" -length 0
Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Multipole -name "CR2.DBA.SNN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM196" -length 0
Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.D12" -length 0.2697746031 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM197" -length 0
Drift -name "CR2.DBA.D11B" -length 0.1628018956 

Multipole -name "CR2.DBA.SNCH1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.D11A" -length 0.1730305995 

Quadrupole -name "CR2.DBA.QI1" -synrad $quad_synrad -length 0.4 -strength [expr -0.5864293272*$e0] -e0 $e0 

Bpm -name "BPM198" -length 0
Drift -name "CR2.DBA.MCELL" -length 0 

Drift -name "CR2MDBACELLEND" -length 0 

Drift -name "CR2.DL0IN" -length 2.246504682 

Quadrupole -name "CR2.QL1IN" -synrad $quad_synrad -length 0.5 -strength [expr -0.642778543*$e0] -e0 $e0 

Bpm -name "BPM199" -length 0
Drift -name "CR2.DL1IN" -length 0.4000469975 

Quadrupole -name "CR2.QL2IN" -synrad $quad_synrad -length 0.5 -strength [expr 0.720003675*$e0] -e0 $e0 

Bpm -name "BPM200" -length 0
Drift -name "CR2.DL2IN" -length 1.103409345 

Quadrupole -name "CR2.QL3IN" -synrad $quad_synrad -length 0.5 -strength [expr -0.2675265744*$e0] -e0 $e0 

Bpm -name "BPM201" -length 0
Drift -name "CR2.DL3IN" -length 0.4000389758 

Drift -name "CR2.MAFT1T" -length 0 

Drift -name "CR2.DRRFH" -length 1 

Drift -name "CR2.RFKICKZERO" -length 0 

Drift -name "CR2.DRRFH" -length 1 

Drift -name "CR2.DRFB1" -length 0.217192186 

Quadrupole -name "CR2.QRFB1" -synrad $quad_synrad -length 0.5 -strength [expr -1.08870911*$e0] -e0 $e0 

Bpm -name "BPM202" -length 0
Drift -name "CR2.DRFB2" -length 1.193240969 

Quadrupole -name "CR2.QRFB2" -synrad $quad_synrad -length 0.5 -strength [expr 0.5775372505*$e0] -e0 $e0 

Bpm -name "BPM203" -length 0
Drift -name "CR2.DRFB3" -length 3.771233433 

Quadrupole -name "CR2.QRFB3" -synrad $quad_synrad -length 0.5 -strength [expr -0.3437279899*$e0] -e0 $e0 

Bpm -name "BPM204" -length 0
Drift -name "CR2.D1CM" -length 0.01 

Drift -name "CR2.D1M" -length 1 

Drift -name "CR2.D1M" -length 1 

Drift -name "CR2.D1M" -length 1 

Drift -name "CR2.DRFB4A" -length 0.709135479 

Multipole -name "CR2.SRFB6" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 8.540776596*$e0] -e0 $e0 

Drift -name "CR2.DRFB6" -length 2.367622162 

Drift -name "CR2.SEPTUM" -length 1 

Drift -name "CR2.MRFBSYM" -length 0 

Drift -name "CR2.SEPTUM" -length 1 

Drift -name "CR2.DRFB6" -length 2.367622162 

Multipole -name "CR2.SRFB6" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 8.540776596*$e0] -e0 $e0 

Drift -name "CR2.INJECTION" -length 0 


Drift -name "CR2.D1CM" -length 0.01 

Drift -name "CR2.D1M" -length 1 

Drift -name "CR2.D1M" -length 1 

Drift -name "CR2.D1M" -length 1 

Drift -name "CR2.DRFB4A" -length 0.709135479 

Quadrupole -name "CR2.QRFB3" -synrad $quad_synrad -length 0.5 -strength [expr -0.3437279899*$e0] -e0 $e0 

Bpm -name "BPM205" -length 0
Drift -name "CR2.DRFB3" -length 3.771233433 

Quadrupole -name "CR2.QRFB2" -synrad $quad_synrad -length 0.5 -strength [expr 0.5775372505*$e0] -e0 $e0 

Bpm -name "BPM206" -length 0
Drift -name "CR2.DRFB2" -length 1.193240969 

Quadrupole -name "CR2.QRFB1" -synrad $quad_synrad -length 0.5 -strength [expr -1.08870911*$e0] -e0 $e0 

Bpm -name "BPM207" -length 0
Drift -name "CR2.DRFB1" -length 0.217192186 

Drift -name "CR2.DRRFH" -length 1 

Drift -name "CR2.RFKICKZERO" -length 0 

Drift -name "CR2.DRRFH" -length 1 

Drift -name "CR2.DL3IN" -length 0.4000389758 

Quadrupole -name "CR2.QL3IN" -synrad $quad_synrad -length 0.5 -strength [expr -0.2675265744*$e0] -e0 $e0 

Bpm -name "BPM208" -length 0
Drift -name "CR2.DL2IN" -length 1.103409345 

Quadrupole -name "CR2.QL2IN" -synrad $quad_synrad -length 0.5 -strength [expr 0.720003675*$e0] -e0 $e0 

Bpm -name "BPM209" -length 0
Drift -name "CR2.DL1IN" -length 0.4000469975 

Quadrupole -name "CR2.QL1IN" -synrad $quad_synrad -length 0.5 -strength [expr -0.642778543*$e0] -e0 $e0 

Bpm -name "BPM210" -length 0
Drift -name "CR2.DL0IN" -length 2.246504682 

Drift -name "CR2MDBACELLSTART" -length 0 

Drift -name "CR2.DBA.MCELL" -length 0 

Quadrupole -name "CR2.DBA.QI1" -synrad $quad_synrad -length 0.4 -strength [expr -0.5864293272*$e0] -e0 $e0 

Bpm -name "BPM211" -length 0
Drift -name "CR2.DBA.D11A" -length 0.1730305995 

Multipole -name "CR2.DBA.SNCH1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.D11B" -length 0.1628018956 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM212" -length 0
Drift -name "CR2.DBA.D12" -length 0.2697746031 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM213" -length 0
Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Drift -name "CR2.DBA.HBM" -length 0 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Multipole -name "CR2.DBA.SN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM214" -length 0
Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM215" -length 0
Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Multipole -name "CR2.DBA.SNN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM216" -length 0
Drift -name "CR2.DBA.HCM" -length 0 

Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.D12" -length 0.2697746031 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM217" -length 0
Drift -name "CR2.DBA.D11B1" -length 0.3424874764 

Multipole -name "CR2.DBA.SD0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.146568947*$e0] -e0 $e0 

Drift -name "CR2.DBA.DM0" -length 0.109313245 

Drift -name "CR2.DBA.HCM0" -length 0 

Quadrupole -name "CR2.DBA.QF1" -synrad $quad_synrad -length 0.4 -strength [expr -1.115855102*$e0] -e0 $e0 

Bpm -name "BPM218" -length 0
Drift -name "CR2.DBA.DM1" -length 1.973582219 

Quadrupole -name "CR2.DBA.QF2" -synrad $quad_synrad -length 0.4 -strength [expr 0.6764098228*$e0] -e0 $e0 

Bpm -name "BPM219" -length 0
Drift -name "CR2.DBA.DM2" -length 1.738896147 

Multipole -name "CR2.DBA.SD0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.146568947*$e0] -e0 $e0 

Drift -name "CR2.DBA.DMSEXT1" -length 0.109313245 

Quadrupole -name "CR2.DBA.QF3" -synrad $quad_synrad -length 0.4 -strength [expr -1.118935626*$e0] -e0 $e0 

Bpm -name "BPM220" -length 0
Drift -name "CR2.DBA.DM3" -length 0.2802607113 

Drift -name "CR2.DBA.DMSEXT1" -length 0.109313245 

Drift -name "CR2.DBA.D11B2" -length 0.1628018956 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM221" -length 0
Drift -name "CR2.DBA.D12" -length 0.2697746031 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM222" -length 0
Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Drift -name "CR2.DBA.HBM" -length 0 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Multipole -name "CR2.DBA.SN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM223" -length 0
Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM224" -length 0
Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Multipole -name "CR2.DBA.SNN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM225" -length 0
Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.D12" -length 0.2697746031 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM226" -length 0
Drift -name "CR2.DBA.D11B" -length 0.1628018956 

Multipole -name "CR2.DBA.SNCH1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.D11A" -length 0.1730305995 

Quadrupole -name "CR2.DBA.QI1" -synrad $quad_synrad -length 0.4 -strength [expr -0.5864293272*$e0] -e0 $e0 

Bpm -name "BPM227" -length 0
Drift -name "CR2.DBA.MCELL" -length 0 

Drift -name "CR2MDBACELLEND" -length 0 

Drift -name "CR2.DTR1A" -length 1.1 

Multipole -name "CR2.SXTR1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 12.54242064*$e0] -e0 $e0 

Drift -name "CR2.DTR1B" -length 0.2 

Quadrupole -name "CR2.QTR1" -synrad $quad_synrad -length 0.5 -strength [expr -0.371641026*$e0] -e0 $e0 

Bpm -name "BPM228" -length 0
Drift -name "CR2.DTR2A" -length 1.1 

Multipole -name "CR2.SXTR2" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -5.250757706*$e0] -e0 $e0 

Drift -name "CR2.DTR2B" -length 0.2 

Quadrupole -name "CR2.QTR2" -synrad $quad_synrad -length 0.5 -strength [expr 0.512918083*$e0] -e0 $e0 

Bpm -name "BPM229" -length 0
Drift -name "CR2.DTR3A" -length 1.5 

Multipole -name "CR2.SXTR3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -7.465446283*$e0] -e0 $e0 

Drift -name "CR2.DTR3B" -length 0.2 

Quadrupole -name "CR2.QTR3" -synrad $quad_synrad -length 0.5 -strength [expr -0.1592789427*$e0] -e0 $e0 

Bpm -name "BPM230" -length 0
Drift -name "CR2.DTR4A" -length 2.1328 

Multipole -name "CR2.SXTR4" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 20.76691673*$e0] -e0 $e0 

Drift -name "CR2.DTR4B" -length 0.2 

Quadrupole -name "CR2.QTR4" -synrad $quad_synrad -length 0.25 -strength [expr 0*$e0] -e0 $e0 

Bpm -name "BPM231" -length 0
Quadrupole -name "CR2.QTR4" -synrad $quad_synrad -length 0.25 -strength [expr 0*$e0] -e0 $e0 

Bpm -name "BPM232" -length 0
Drift -name "CR2.DTR4B" -length 0.2 

Multipole -name "CR2.SXTR4" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 20.76691673*$e0] -e0 $e0 

Drift -name "CR2.DTR4A" -length 2.1328 

Quadrupole -name "CR2.QTR3" -synrad $quad_synrad -length 0.5 -strength [expr -0.1592789427*$e0] -e0 $e0 

Bpm -name "BPM233" -length 0
Drift -name "CR2.DTR3B" -length 0.2 

Multipole -name "CR2.SXTR3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -7.465446283*$e0] -e0 $e0 

Drift -name "CR2.DTR3A" -length 1.5 

Quadrupole -name "CR2.QTR2" -synrad $quad_synrad -length 0.5 -strength [expr 0.512918083*$e0] -e0 $e0 

Bpm -name "BPM234" -length 0
Drift -name "CR2.DTR2B" -length 0.2 

Multipole -name "CR2.SXTR2" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -5.250757706*$e0] -e0 $e0 

Drift -name "CR2.DTR2A" -length 1.1 

Quadrupole -name "CR2.QTR1" -synrad $quad_synrad -length 0.5 -strength [expr -0.371641026*$e0] -e0 $e0 

Bpm -name "BPM235" -length 0
Drift -name "CR2.DTR1B" -length 0.2 

Multipole -name "CR2.SXTR1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 12.54242064*$e0] -e0 $e0 

Drift -name "CR2.DTR1A" -length 1.1 

Drift -name "CR2MDBACELLSTART" -length 0 

Drift -name "CR2.DBA.MCELL" -length 0 

Quadrupole -name "CR2.DBA.QI1" -synrad $quad_synrad -length 0.4 -strength [expr -0.5864293272*$e0] -e0 $e0 

Bpm -name "BPM236" -length 0
Drift -name "CR2.DBA.D11A" -length 0.1730305995 

Multipole -name "CR2.DBA.SNCH1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.D11B" -length 0.1628018956 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM237" -length 0
Drift -name "CR2.DBA.D12" -length 0.2697746031 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM238" -length 0
Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Drift -name "CR2.DBA.HBM" -length 0 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Multipole -name "CR2.DBA.SN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM239" -length 0
Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM240" -length 0
Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Multipole -name "CR2.DBA.SNN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM241" -length 0
Drift -name "CR2.DBA.HCM" -length 0 

Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.D12" -length 0.2697746031 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM242" -length 0
Drift -name "CR2.DBA.D11B1" -length 0.3424874764 

Multipole -name "CR2.DBA.SD0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.146568947*$e0] -e0 $e0 

Drift -name "CR2.DBA.DM0" -length 0.109313245 

Drift -name "CR2.DBA.HCM0" -length 0 

Quadrupole -name "CR2.DBA.QF1" -synrad $quad_synrad -length 0.4 -strength [expr -1.115855102*$e0] -e0 $e0 

Bpm -name "BPM243" -length 0
Drift -name "CR2.DBA.DM1" -length 1.973582219 

Quadrupole -name "CR2.DBA.QF2" -synrad $quad_synrad -length 0.4 -strength [expr 0.6764098228*$e0] -e0 $e0 

Bpm -name "BPM244" -length 0
Drift -name "CR2.DBA.DM2" -length 1.738896147 

Multipole -name "CR2.DBA.SD0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.146568947*$e0] -e0 $e0 

Drift -name "CR2.DBA.DMSEXT1" -length 0.109313245 

Quadrupole -name "CR2.DBA.QF3" -synrad $quad_synrad -length 0.4 -strength [expr -1.118935626*$e0] -e0 $e0 

Bpm -name "BPM245" -length 0
Drift -name "CR2.DBA.DM3" -length 0.2802607113 

Drift -name "CR2.DBA.DMSEXT1" -length 0.109313245 

Drift -name "CR2.DBA.D11B2" -length 0.1628018956 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM246" -length 0
Drift -name "CR2.DBA.D12" -length 0.2697746031 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM247" -length 0
Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Drift -name "CR2.DBA.HBM" -length 0 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Multipole -name "CR2.DBA.SN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM248" -length 0
Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM249" -length 0
Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Multipole -name "CR2.DBA.SNN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM250" -length 0
Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.D12" -length 0.2697746031 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM251" -length 0
Drift -name "CR2.DBA.D11B" -length 0.1628018956 

Multipole -name "CR2.DBA.SNCH1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.D11A" -length 0.1730305995 

Quadrupole -name "CR2.DBA.QI1" -synrad $quad_synrad -length 0.4 -strength [expr -0.5864293272*$e0] -e0 $e0 

Bpm -name "BPM252" -length 0
Drift -name "CR2.DBA.MCELL" -length 0 

Drift -name "CR2MDBACELLEND" -length 0 

Drift -name "CR2.DS1C_A" -length 2.179474654 

Multipole -name "CR2.SXS1C" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 11.05883366*$e0] -e0 $e0 

Drift -name "CR2.DS1C_B" -length 0.3 

Quadrupole -name "CR2.QS1C" -synrad $quad_synrad -length 0.5 -strength [expr -0.66814696*$e0] -e0 $e0 

Bpm -name "BPM253" -length 0
Drift -name "CR2.DS2C" -length 0.2003907272 

Quadrupole -name "CR2.QS2C" -synrad $quad_synrad -length 0.5 -strength [expr 0.7440336365*$e0] -e0 $e0 

Bpm -name "BPM254" -length 0
Drift -name "CR2.DS3C_A" -length 0.3 

Multipole -name "CR2.SXS3C" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -13.03004983*$e0] -e0 $e0 

Drift -name "CR2.DS3C_B" -length 3.777418619 

Quadrupole -name "CR2.QS3C" -synrad $quad_synrad -length 0.5 -strength [expr -0.03237714152*$e0] -e0 $e0 

Bpm -name "BPM255" -length 0
Drift -name "CR2.DS4C" -length 0.2407160001 

Drift -name "CR2.BC1" -length 0.4 

Drift -name "CR2.BC2" -length 0.4 

Drift -name "CR2.BC3" -length 0.4 

Drift -name "CR2.BC1" -length 0.4 

Drift -name "CR2.DS4C" -length 0.2407160001 

Quadrupole -name "CR2.QS3C" -synrad $quad_synrad -length 0.5 -strength [expr -0.03237714152*$e0] -e0 $e0 

Bpm -name "BPM256" -length 0
Drift -name "CR2.DS3C_B" -length 3.777418619 

Multipole -name "CR2.SXS3C" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -13.03004983*$e0] -e0 $e0 

Drift -name "CR2.DS3C_A" -length 0.3 

Quadrupole -name "CR2.QS2C" -synrad $quad_synrad -length 0.5 -strength [expr 0.7440336365*$e0] -e0 $e0 

Bpm -name "BPM257" -length 0
Drift -name "CR2.DS2C" -length 0.2003907272 

Quadrupole -name "CR2.QS1C" -synrad $quad_synrad -length 0.5 -strength [expr -0.66814696*$e0] -e0 $e0 

Bpm -name "BPM258" -length 0
Drift -name "CR2.DS1C_B" -length 0.3 

Multipole -name "CR2.SXS1C" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 11.05883366*$e0] -e0 $e0 

Drift -name "CR2.DS1C_A" -length 2.179474654 

Drift -name "CR2MDBACELLSTART" -length 0 

Drift -name "CR2.DBA.MCELL" -length 0 

Quadrupole -name "CR2.DBA.QI1" -synrad $quad_synrad -length 0.4 -strength [expr -0.5864293272*$e0] -e0 $e0 

Bpm -name "BPM259" -length 0
Drift -name "CR2.DBA.D11A" -length 0.1730305995 

Multipole -name "CR2.DBA.SNCH1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.D11B" -length 0.1628018956 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM260" -length 0
Drift -name "CR2.DBA.D12" -length 0.2697746031 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM261" -length 0
Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Drift -name "CR2.DBA.HBM" -length 0 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Multipole -name "CR2.DBA.SN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM262" -length 0
Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM263" -length 0
Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Multipole -name "CR2.DBA.SNN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM264" -length 0
Drift -name "CR2.DBA.HCM" -length 0 

Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.D12" -length 0.2697746031 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM265" -length 0
Drift -name "CR2.DBA.D11B1" -length 0.3424874764 

Multipole -name "CR2.DBA.SD0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.146568947*$e0] -e0 $e0 

Drift -name "CR2.DBA.DM0" -length 0.109313245 

Drift -name "CR2.DBA.HCM0" -length 0 

Quadrupole -name "CR2.DBA.QF1" -synrad $quad_synrad -length 0.4 -strength [expr -1.115855102*$e0] -e0 $e0 

Bpm -name "BPM266" -length 0
Drift -name "CR2.DBA.DM1" -length 1.973582219 

Quadrupole -name "CR2.DBA.QF2" -synrad $quad_synrad -length 0.4 -strength [expr 0.6764098228*$e0] -e0 $e0 

Bpm -name "BPM267" -length 0
Drift -name "CR2.DBA.DM2" -length 1.738896147 

Multipole -name "CR2.DBA.SD0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.146568947*$e0] -e0 $e0 

Drift -name "CR2.DBA.DMSEXT1" -length 0.109313245 

Quadrupole -name "CR2.DBA.QF3" -synrad $quad_synrad -length 0.4 -strength [expr -1.118935626*$e0] -e0 $e0 

Bpm -name "BPM268" -length 0
Drift -name "CR2.DBA.DM3" -length 0.2802607113 

Drift -name "CR2.DBA.DMSEXT1" -length 0.109313245 

Drift -name "CR2.DBA.D11B2" -length 0.1628018956 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM269" -length 0
Drift -name "CR2.DBA.D12" -length 0.2697746031 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM270" -length 0
Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Drift -name "CR2.DBA.HBM" -length 0 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Multipole -name "CR2.DBA.SN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM271" -length 0
Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM272" -length 0
Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Multipole -name "CR2.DBA.SNN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM273" -length 0
Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.D12" -length 0.2697746031 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM274" -length 0
Drift -name "CR2.DBA.D11B" -length 0.1628018956 

Multipole -name "CR2.DBA.SNCH1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.D11A" -length 0.1730305995 

Quadrupole -name "CR2.DBA.QI1" -synrad $quad_synrad -length 0.4 -strength [expr -0.5864293272*$e0] -e0 $e0 

Bpm -name "BPM275" -length 0
Drift -name "CR2.DBA.MCELL" -length 0 

Drift -name "CR2MDBACELLEND" -length 0 

Drift -name "CR2.DTR1A" -length 1.1 

Multipole -name "CR2.SXTR1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 12.54242064*$e0] -e0 $e0 

Drift -name "CR2.DTR1B" -length 0.2 

Quadrupole -name "CR2.QTR1" -synrad $quad_synrad -length 0.5 -strength [expr -0.371641026*$e0] -e0 $e0 

Bpm -name "BPM276" -length 0
Drift -name "CR2.DTR2A" -length 1.1 

Multipole -name "CR2.SXTR2" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -5.250757706*$e0] -e0 $e0 

Drift -name "CR2.DTR2B" -length 0.2 

Quadrupole -name "CR2.QTR2" -synrad $quad_synrad -length 0.5 -strength [expr 0.512918083*$e0] -e0 $e0 

Bpm -name "BPM277" -length 0
Drift -name "CR2.DTR3A" -length 1.5 

Multipole -name "CR2.SXTR3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -7.465446283*$e0] -e0 $e0 

Drift -name "CR2.DTR3B" -length 0.2 

Quadrupole -name "CR2.QTR3" -synrad $quad_synrad -length 0.5 -strength [expr -0.1592789427*$e0] -e0 $e0 

Bpm -name "BPM278" -length 0
Drift -name "CR2.DTR4A" -length 2.1328 

Multipole -name "CR2.SXTR4" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 20.76691673*$e0] -e0 $e0 

Drift -name "CR2.DTR4B" -length 0.2 

Quadrupole -name "CR2.QTR4" -synrad $quad_synrad -length 0.25 -strength [expr 0*$e0] -e0 $e0 

Bpm -name "BPM279" -length 0
Quadrupole -name "CR2.QTR4" -synrad $quad_synrad -length 0.25 -strength [expr 0*$e0] -e0 $e0 

Bpm -name "BPM280" -length 0
Drift -name "CR2.DTR4B" -length 0.2 

Multipole -name "CR2.SXTR4" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 20.76691673*$e0] -e0 $e0 

Drift -name "CR2.DTR4A" -length 2.1328 

Quadrupole -name "CR2.QTR3" -synrad $quad_synrad -length 0.5 -strength [expr -0.1592789427*$e0] -e0 $e0 

Bpm -name "BPM281" -length 0
Drift -name "CR2.DTR3B" -length 0.2 

Multipole -name "CR2.SXTR3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -7.465446283*$e0] -e0 $e0 

Drift -name "CR2.DTR3A" -length 1.5 

Quadrupole -name "CR2.QTR2" -synrad $quad_synrad -length 0.5 -strength [expr 0.512918083*$e0] -e0 $e0 

Bpm -name "BPM282" -length 0
Drift -name "CR2.DTR2B" -length 0.2 

Multipole -name "CR2.SXTR2" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -5.250757706*$e0] -e0 $e0 

Drift -name "CR2.DTR2A" -length 1.1 

Quadrupole -name "CR2.QTR1" -synrad $quad_synrad -length 0.5 -strength [expr -0.371641026*$e0] -e0 $e0 

Bpm -name "BPM283" -length 0
Drift -name "CR2.DTR1B" -length 0.2 

Multipole -name "CR2.SXTR1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 12.54242064*$e0] -e0 $e0 

Drift -name "CR2.DTR1A" -length 1.1 

Drift -name "CR2MDBACELLSTART" -length 0 

Drift -name "CR2.DBA.MCELL" -length 0 

Quadrupole -name "CR2.DBA.QI1" -synrad $quad_synrad -length 0.4 -strength [expr -0.5864293272*$e0] -e0 $e0 

Bpm -name "BPM284" -length 0
Drift -name "CR2.DBA.D11A" -length 0.1730305995 

Multipole -name "CR2.DBA.SNCH1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.D11B" -length 0.1628018956 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM285" -length 0
Drift -name "CR2.DBA.D12" -length 0.2697746031 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM286" -length 0
Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Drift -name "CR2.DBA.HBM" -length 0 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Multipole -name "CR2.DBA.SN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM287" -length 0
Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM288" -length 0
Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Multipole -name "CR2.DBA.SNN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM289" -length 0
Drift -name "CR2.DBA.HCM" -length 0 

Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.D12" -length 0.2697746031 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM290" -length 0
Drift -name "CR2.DBA.D11B1" -length 0.3424874764 

Multipole -name "CR2.DBA.SD0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.146568947*$e0] -e0 $e0 

Drift -name "CR2.DBA.DM0" -length 0.109313245 

Drift -name "CR2.DBA.HCM0" -length 0 

Quadrupole -name "CR2.DBA.QF1" -synrad $quad_synrad -length 0.4 -strength [expr -1.115855102*$e0] -e0 $e0 

Bpm -name "BPM291" -length 0
Drift -name "CR2.DBA.DM1" -length 1.973582219 

Quadrupole -name "CR2.DBA.QF2" -synrad $quad_synrad -length 0.4 -strength [expr 0.6764098228*$e0] -e0 $e0 

Bpm -name "BPM292" -length 0
Drift -name "CR2.DBA.DM2" -length 1.738896147 

Multipole -name "CR2.DBA.SD0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.146568947*$e0] -e0 $e0 

Drift -name "CR2.DBA.DMSEXT1" -length 0.109313245 

Quadrupole -name "CR2.DBA.QF3" -synrad $quad_synrad -length 0.4 -strength [expr -1.118935626*$e0] -e0 $e0 

Bpm -name "BPM293" -length 0
Drift -name "CR2.DBA.DM3" -length 0.2802607113 

Drift -name "CR2.DBA.DMSEXT1" -length 0.109313245 

Drift -name "CR2.DBA.D11B2" -length 0.1628018956 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM294" -length 0
Drift -name "CR2.DBA.D12" -length 0.2697746031 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM295" -length 0
Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Drift -name "CR2.DBA.HBM" -length 0 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Multipole -name "CR2.DBA.SN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM296" -length 0
Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM297" -length 0
Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Multipole -name "CR2.DBA.SNN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM298" -length 0
Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.D12" -length 0.2697746031 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM299" -length 0
Drift -name "CR2.DBA.D11B" -length 0.1628018956 

Multipole -name "CR2.DBA.SNCH1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.D11A" -length 0.1730305995 

Quadrupole -name "CR2.DBA.QI1" -synrad $quad_synrad -length 0.4 -strength [expr -0.5864293272*$e0] -e0 $e0 

Bpm -name "BPM300" -length 0
Drift -name "CR2.DBA.MCELL" -length 0 

Drift -name "CR2MDBACELLEND" -length 0 

Drift -name "CR2.DL0IN" -length 2.246504682 

Quadrupole -name "CR2.QL1IN" -synrad $quad_synrad -length 0.5 -strength [expr -0.642778543*$e0] -e0 $e0 

Bpm -name "BPM301" -length 0
Drift -name "CR2.DL1IN" -length 0.4000469975 

Quadrupole -name "CR2.QL2IN" -synrad $quad_synrad -length 0.5 -strength [expr 0.720003675*$e0] -e0 $e0 

Bpm -name "BPM302" -length 0
Drift -name "CR2.DL2IN" -length 1.103409345 

Quadrupole -name "CR2.QL3IN" -synrad $quad_synrad -length 0.5 -strength [expr -0.2675265744*$e0] -e0 $e0 

Bpm -name "BPM303" -length 0
Drift -name "CR2.DL3IN" -length 0.4000389758 

Drift -name "CR2.MENTEXTKCK" -length 0 

Drift -name "CR2.DRRF" -length 2 

Drift -name "CR2.DRFB1" -length 0.217192186 

Quadrupole -name "CR2.QRFB1" -synrad $quad_synrad -length 0.5 -strength [expr -1.08870911*$e0] -e0 $e0 

Bpm -name "BPM304" -length 0
Drift -name "CR2.DRFB2" -length 1.193240969 

Quadrupole -name "CR2.QRFB2" -synrad $quad_synrad -length 0.5 -strength [expr 0.5775372505*$e0] -e0 $e0 

Bpm -name "BPM305" -length 0
Drift -name "CR2.DRFB3" -length 3.771233433 

Quadrupole -name "CR2.QRFB3" -synrad $quad_synrad -length 0.5 -strength [expr -0.3437279899*$e0] -e0 $e0 

Bpm -name "BPM306" -length 0
Drift -name "CR2.D1CM" -length 0.01 

Drift -name "CR2.D1M" -length 1 

Drift -name "CR2.D1M" -length 1 

Drift -name "CR2.D1M" -length 1 

Drift -name "CR2.DRFB4A" -length 0.709135479 
Bpm -name "CR2.EJECTION" -length 0

Drift -name "CR2.DSRFB6" -length 0.4 

Drift -name "CR2.DRFB6" -length 2.367622162 

Drift -name "CR2.SEPTUM" -length 1 
Drift -name "CR2.SEPTUM" -length 1 

Drift -name "CR2.DRFB6" -length 2.367622162 

Drift -name "CR2.DSRFB6" -length 0.4 

Drift -name "CR2.DRFB4A" -length 0.709135479 

Drift -name "CR2.D1M" -length 1 

Drift -name "CR2.D1M" -length 1 

Drift -name "CR2.D1M" -length 1 

Drift -name "CR2.D1CM" -length 0.01 

Quadrupole -name "CR2.QRFB3" -synrad $quad_synrad -length 0.5 -strength [expr -0.3437279899*$e0] -e0 $e0 

Bpm -name "BPM307" -length 0
Drift -name "CR2.DRFB3" -length 3.771233433 

Quadrupole -name "CR2.QRFB2" -synrad $quad_synrad -length 0.5 -strength [expr 0.5775372505*$e0] -e0 $e0 

Bpm -name "BPM308" -length 0
Drift -name "CR2.DRFB2" -length 1.193240969 

Quadrupole -name "CR2.QRFB1" -synrad $quad_synrad -length 0.5 -strength [expr -1.08870911*$e0] -e0 $e0 

Bpm -name "BPM309" -length 0
Drift -name "CR2.DRFB1" -length 0.217192186 

Drift -name "CR2.DRRF" -length 2 

Drift -name "CR2.MENTEXTKCK" -length 0 

Drift -name "CR2.DL3IN" -length 0.4000389758 

Quadrupole -name "CR2.QL3IN" -synrad $quad_synrad -length 0.5 -strength [expr -0.2675265744*$e0] -e0 $e0 

Bpm -name "BPM310" -length 0
Drift -name "CR2.DL2IN" -length 1.103409345 

Quadrupole -name "CR2.QL2IN" -synrad $quad_synrad -length 0.5 -strength [expr 0.720003675*$e0] -e0 $e0 

Bpm -name "BPM311" -length 0
Drift -name "CR2.DL1IN" -length 0.4000469975 

Quadrupole -name "CR2.QL1IN" -synrad $quad_synrad -length 0.5 -strength [expr -0.642778543*$e0] -e0 $e0 

Bpm -name "BPM312" -length 0
Drift -name "CR2.DL0IN" -length 2.246504682 

Drift -name "CR2MDBACELLSTART" -length 0 

Drift -name "CR2.DBA.MCELL" -length 0 

Quadrupole -name "CR2.DBA.QI1" -synrad $quad_synrad -length 0.4 -strength [expr -0.5864293272*$e0] -e0 $e0 

Bpm -name "BPM313" -length 0
Drift -name "CR2.DBA.D11A" -length 0.1730305995 

Multipole -name "CR2.DBA.SNCH1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.D11B" -length 0.1628018956 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM314" -length 0
Drift -name "CR2.DBA.D12" -length 0.2697746031 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM315" -length 0
Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Drift -name "CR2.DBA.HBM" -length 0 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Multipole -name "CR2.DBA.SN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM316" -length 0
Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM317" -length 0
Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Multipole -name "CR2.DBA.SNN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM318" -length 0
Drift -name "CR2.DBA.HCM" -length 0 

Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.D12" -length 0.2697746031 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM319" -length 0
Drift -name "CR2.DBA.D11B1" -length 0.3424874764 

Multipole -name "CR2.DBA.SD0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.146568947*$e0] -e0 $e0 

Drift -name "CR2.DBA.DM0" -length 0.109313245 

Drift -name "CR2.DBA.HCM0" -length 0 

Quadrupole -name "CR2.DBA.QF1" -synrad $quad_synrad -length 0.4 -strength [expr -1.115855102*$e0] -e0 $e0 

Bpm -name "BPM320" -length 0
Drift -name "CR2.DBA.DM1" -length 1.973582219 

Quadrupole -name "CR2.DBA.QF2" -synrad $quad_synrad -length 0.4 -strength [expr 0.6764098228*$e0] -e0 $e0 

Bpm -name "BPM321" -length 0
Drift -name "CR2.DBA.DM2" -length 1.738896147 

Multipole -name "CR2.DBA.SD0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.146568947*$e0] -e0 $e0 

Drift -name "CR2.DBA.DMSEXT1" -length 0.109313245 

Quadrupole -name "CR2.DBA.QF3" -synrad $quad_synrad -length 0.4 -strength [expr -1.118935626*$e0] -e0 $e0 

Bpm -name "BPM322" -length 0
Drift -name "CR2.DBA.DM3" -length 0.2802607113 

Drift -name "CR2.DBA.DMSEXT1" -length 0.109313245 

Drift -name "CR2.DBA.D11B2" -length 0.1628018956 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM323" -length 0
Drift -name "CR2.DBA.D12" -length 0.2697746031 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM324" -length 0
Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Drift -name "CR2.DBA.HBM" -length 0 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Multipole -name "CR2.DBA.SN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM325" -length 0
Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM326" -length 0
Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Multipole -name "CR2.DBA.SNN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM327" -length 0
Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.D12" -length 0.2697746031 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM328" -length 0
Drift -name "CR2.DBA.D11B" -length 0.1628018956 

Multipole -name "CR2.DBA.SNCH1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.D11A" -length 0.1730305995 

Quadrupole -name "CR2.DBA.QI1" -synrad $quad_synrad -length 0.4 -strength [expr -0.5864293272*$e0] -e0 $e0 

Bpm -name "BPM329" -length 0
Drift -name "CR2.DBA.MCELL" -length 0 

Drift -name "CR2MDBACELLEND" -length 0 

Drift -name "CR2.DTR1A" -length 1.1 

Multipole -name "CR2.SXTR1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 12.54242064*$e0] -e0 $e0 

Drift -name "CR2.DTR1B" -length 0.2 

Quadrupole -name "CR2.QTR1" -synrad $quad_synrad -length 0.5 -strength [expr -0.371641026*$e0] -e0 $e0 

Bpm -name "BPM330" -length 0
Drift -name "CR2.DTR2A" -length 1.1 

Multipole -name "CR2.SXTR2" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -5.250757706*$e0] -e0 $e0 

Drift -name "CR2.DTR2B" -length 0.2 

Quadrupole -name "CR2.QTR2" -synrad $quad_synrad -length 0.5 -strength [expr 0.512918083*$e0] -e0 $e0 

Bpm -name "BPM331" -length 0
Drift -name "CR2.DTR3A" -length 1.5 

Multipole -name "CR2.SXTR3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -7.465446283*$e0] -e0 $e0 

Drift -name "CR2.DTR3B" -length 0.2 

Quadrupole -name "CR2.QTR3" -synrad $quad_synrad -length 0.5 -strength [expr -0.1592789427*$e0] -e0 $e0 

Bpm -name "BPM332" -length 0
Drift -name "CR2.DTR4A" -length 2.1328 

Multipole -name "CR2.SXTR4" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 20.76691673*$e0] -e0 $e0 

Drift -name "CR2.DTR4B" -length 0.2 

Quadrupole -name "CR2.QTR4" -synrad $quad_synrad -length 0.25 -strength [expr 0*$e0] -e0 $e0 

Bpm -name "BPM333" -length 0
Quadrupole -name "CR2.QTR4" -synrad $quad_synrad -length 0.25 -strength [expr 0*$e0] -e0 $e0 

Bpm -name "BPM334" -length 0
Drift -name "CR2.DTR4B" -length 0.2 

Multipole -name "CR2.SXTR4" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 20.76691673*$e0] -e0 $e0 

Drift -name "CR2.DTR4A" -length 2.1328 

Quadrupole -name "CR2.QTR3" -synrad $quad_synrad -length 0.5 -strength [expr -0.1592789427*$e0] -e0 $e0 

Bpm -name "BPM335" -length 0
Drift -name "CR2.DTR3B" -length 0.2 

Multipole -name "CR2.SXTR3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -7.465446283*$e0] -e0 $e0 

Drift -name "CR2.DTR3A" -length 1.5 

Quadrupole -name "CR2.QTR2" -synrad $quad_synrad -length 0.5 -strength [expr 0.512918083*$e0] -e0 $e0 

Bpm -name "BPM336" -length 0
Drift -name "CR2.DTR2B" -length 0.2 

Multipole -name "CR2.SXTR2" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -5.250757706*$e0] -e0 $e0 

Drift -name "CR2.DTR2A" -length 1.1 

Quadrupole -name "CR2.QTR1" -synrad $quad_synrad -length 0.5 -strength [expr -0.371641026*$e0] -e0 $e0 

Bpm -name "BPM337" -length 0
Drift -name "CR2.DTR1B" -length 0.2 

Multipole -name "CR2.SXTR1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 12.54242064*$e0] -e0 $e0 

Drift -name "CR2.DTR1A" -length 1.1 

Drift -name "CR2MDBACELLSTART" -length 0 

Drift -name "CR2.DBA.MCELL" -length 0 

Quadrupole -name "CR2.DBA.QI1" -synrad $quad_synrad -length 0.4 -strength [expr -0.5864293272*$e0] -e0 $e0 

Bpm -name "BPM338" -length 0
Drift -name "CR2.DBA.D11A" -length 0.1730305995 

Multipole -name "CR2.DBA.SNCH1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.D11B" -length 0.1628018956 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM339" -length 0
Drift -name "CR2.DBA.D12" -length 0.2697746031 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM340" -length 0
Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Drift -name "CR2.DBA.HBM" -length 0 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Multipole -name "CR2.DBA.SN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM341" -length 0
Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM342" -length 0
Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Multipole -name "CR2.DBA.SNN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM343" -length 0
Drift -name "CR2.DBA.HCM" -length 0 

Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.D12" -length 0.2697746031 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM344" -length 0
Drift -name "CR2.DBA.D11B1" -length 0.3424874764 

Multipole -name "CR2.DBA.SD0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.146568947*$e0] -e0 $e0 

Drift -name "CR2.DBA.DM0" -length 0.109313245 

Drift -name "CR2.DBA.HCM0" -length 0 

Quadrupole -name "CR2.DBA.QF1" -synrad $quad_synrad -length 0.4 -strength [expr -1.115855102*$e0] -e0 $e0 

Bpm -name "BPM345" -length 0
Drift -name "CR2.DBA.DM1" -length 1.973582219 

Quadrupole -name "CR2.DBA.QF2" -synrad $quad_synrad -length 0.4 -strength [expr 0.6764098228*$e0] -e0 $e0 

Bpm -name "BPM346" -length 0
Drift -name "CR2.DBA.DM2" -length 1.738896147 

Multipole -name "CR2.DBA.SD0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.146568947*$e0] -e0 $e0 

Drift -name "CR2.DBA.DMSEXT1" -length 0.109313245 

Quadrupole -name "CR2.DBA.QF3" -synrad $quad_synrad -length 0.4 -strength [expr -1.118935626*$e0] -e0 $e0 

Bpm -name "BPM347" -length 0
Drift -name "CR2.DBA.DM3" -length 0.2802607113 

Drift -name "CR2.DBA.DMSEXT1" -length 0.109313245 

Drift -name "CR2.DBA.D11B2" -length 0.1628018956 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM348" -length 0
Drift -name "CR2.DBA.D12" -length 0.2697746031 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM349" -length 0
Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Drift -name "CR2.DBA.HBM" -length 0 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Multipole -name "CR2.DBA.SN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM350" -length 0
Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM351" -length 0
Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Multipole -name "CR2.DBA.SNN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM352" -length 0
Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.D12" -length 0.2697746031 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM353" -length 0
Drift -name "CR2.DBA.D11B" -length 0.1628018956 

Multipole -name "CR2.DBA.SNCH1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.D11A" -length 0.1730305995 

Quadrupole -name "CR2.DBA.QI1" -synrad $quad_synrad -length 0.4 -strength [expr -0.5864293272*$e0] -e0 $e0 

Bpm -name "BPM354" -length 0
Drift -name "CR2.DBA.MCELL" -length 0 

Drift -name "CR2MDBACELLEND" -length 0 

Drift -name "CR2.DS1C_A" -length 2.179474654 

Multipole -name "CR2.SXS1C" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 11.05883366*$e0] -e0 $e0 

Drift -name "CR2.DS1C_B" -length 0.3 

Quadrupole -name "CR2.QS1C" -synrad $quad_synrad -length 0.5 -strength [expr -0.66814696*$e0] -e0 $e0 

Bpm -name "BPM355" -length 0
Drift -name "CR2.DS2C" -length 0.2003907272 

Quadrupole -name "CR2.QS2C" -synrad $quad_synrad -length 0.5 -strength [expr 0.7440336365*$e0] -e0 $e0 

Bpm -name "BPM356" -length 0
Drift -name "CR2.DS3C_A" -length 0.3 

Multipole -name "CR2.SXS3C" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -13.03004983*$e0] -e0 $e0 

Drift -name "CR2.DS3C_B" -length 3.777418619 

Quadrupole -name "CR2.QS3C" -synrad $quad_synrad -length 0.5 -strength [expr -0.03237714152*$e0] -e0 $e0 

Bpm -name "BPM357" -length 0
Drift -name "CR2.DS4C" -length 0.2407160001 

Drift -name "CR2.BC1" -length 0.4 

Drift -name "CR2.BC2" -length 0.4 

Drift -name "CR2.BC3" -length 0.4 

Drift -name "CR2.BC1" -length 0.4 

Drift -name "CR2.DS4C" -length 0.2407160001 

Quadrupole -name "CR2.QS3C" -synrad $quad_synrad -length 0.5 -strength [expr -0.03237714152*$e0] -e0 $e0 

Bpm -name "BPM358" -length 0
Drift -name "CR2.DS3C_B" -length 3.777418619 

Multipole -name "CR2.SXS3C" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -13.03004983*$e0] -e0 $e0 

Drift -name "CR2.DS3C_A" -length 0.3 

Quadrupole -name "CR2.QS2C" -synrad $quad_synrad -length 0.5 -strength [expr 0.7440336365*$e0] -e0 $e0 

Bpm -name "BPM359" -length 0
Drift -name "CR2.DS2C" -length 0.2003907272 

Quadrupole -name "CR2.QS1C" -synrad $quad_synrad -length 0.5 -strength [expr -0.66814696*$e0] -e0 $e0 

Bpm -name "BPM360" -length 0
Drift -name "CR2.DS1C_B" -length 0.3 

Multipole -name "CR2.SXS1C" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 11.05883366*$e0] -e0 $e0 

Drift -name "CR2.DS1C_A" -length 2.179474654 

Drift -name "CR2MDBACELLSTART" -length 0 

Drift -name "CR2.DBA.MCELL" -length 0 

Quadrupole -name "CR2.DBA.QI1" -synrad $quad_synrad -length 0.4 -strength [expr -0.5864293272*$e0] -e0 $e0 

Bpm -name "BPM361" -length 0
Drift -name "CR2.DBA.D11A" -length 0.1730305995 

Multipole -name "CR2.DBA.SNCH1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.D11B" -length 0.1628018956 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM362" -length 0
Drift -name "CR2.DBA.D12" -length 0.2697746031 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM363" -length 0
Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Drift -name "CR2.DBA.HBM" -length 0 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Multipole -name "CR2.DBA.SN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM364" -length 0
Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM365" -length 0
Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Multipole -name "CR2.DBA.SNN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM366" -length 0
Drift -name "CR2.DBA.HCM" -length 0 

Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.D12" -length 0.2697746031 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM367" -length 0
Drift -name "CR2.DBA.D11B1" -length 0.3424874764 

Multipole -name "CR2.DBA.SD0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.146568947*$e0] -e0 $e0 

Drift -name "CR2.DBA.DM0" -length 0.109313245 

Drift -name "CR2.DBA.HCM0" -length 0 

Quadrupole -name "CR2.DBA.QF1" -synrad $quad_synrad -length 0.4 -strength [expr -1.115855102*$e0] -e0 $e0 

Bpm -name "BPM368" -length 0
Drift -name "CR2.DBA.DM1" -length 1.973582219 

Quadrupole -name "CR2.DBA.QF2" -synrad $quad_synrad -length 0.4 -strength [expr 0.6764098228*$e0] -e0 $e0 

Bpm -name "BPM369" -length 0
Drift -name "CR2.DBA.DM2" -length 1.738896147 

Multipole -name "CR2.DBA.SD0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.146568947*$e0] -e0 $e0 

Drift -name "CR2.DBA.DMSEXT1" -length 0.109313245 

Quadrupole -name "CR2.DBA.QF3" -synrad $quad_synrad -length 0.4 -strength [expr -1.118935626*$e0] -e0 $e0 

Bpm -name "BPM370" -length 0
Drift -name "CR2.DBA.DM3" -length 0.2802607113 

Drift -name "CR2.DBA.DMSEXT1" -length 0.109313245 

Drift -name "CR2.DBA.D11B2" -length 0.1628018956 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM371" -length 0
Drift -name "CR2.DBA.D12" -length 0.2697746031 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM372" -length 0
Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Drift -name "CR2.DBA.HBM" -length 0 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Multipole -name "CR2.DBA.SN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM373" -length 0
Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM374" -length 0
Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Multipole -name "CR2.DBA.SNN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM375" -length 0
Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.D12" -length 0.2697746031 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM376" -length 0
Drift -name "CR2.DBA.D11B" -length 0.1628018956 

Multipole -name "CR2.DBA.SNCH1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.D11A" -length 0.1730305995 

Quadrupole -name "CR2.DBA.QI1" -synrad $quad_synrad -length 0.4 -strength [expr -0.5864293272*$e0] -e0 $e0 

Bpm -name "BPM377" -length 0
Drift -name "CR2.DBA.MCELL" -length 0 

Drift -name "CR2MDBACELLEND" -length 0 

Drift -name "CR2.DTR1A" -length 1.1 

Multipole -name "CR2.SXTR1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 12.54242064*$e0] -e0 $e0 

Drift -name "CR2.DTR1B" -length 0.2 

Quadrupole -name "CR2.QTR1" -synrad $quad_synrad -length 0.5 -strength [expr -0.371641026*$e0] -e0 $e0 

Bpm -name "BPM378" -length 0
Drift -name "CR2.DTR2A" -length 1.1 

Multipole -name "CR2.SXTR2" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -5.250757706*$e0] -e0 $e0 

Drift -name "CR2.DTR2B" -length 0.2 

Quadrupole -name "CR2.QTR2" -synrad $quad_synrad -length 0.5 -strength [expr 0.512918083*$e0] -e0 $e0 

Bpm -name "BPM379" -length 0
Drift -name "CR2.DTR3A" -length 1.5 

Multipole -name "CR2.SXTR3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -7.465446283*$e0] -e0 $e0 

Drift -name "CR2.DTR3B" -length 0.2 

Quadrupole -name "CR2.QTR3" -synrad $quad_synrad -length 0.5 -strength [expr -0.1592789427*$e0] -e0 $e0 

Bpm -name "BPM380" -length 0
Drift -name "CR2.DTR4A" -length 2.1328 

Multipole -name "CR2.SXTR4" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 20.76691673*$e0] -e0 $e0 

Drift -name "CR2.DTR4B" -length 0.2 

Quadrupole -name "CR2.QTR4" -synrad $quad_synrad -length 0.25 -strength [expr 0*$e0] -e0 $e0 

Bpm -name "BPM381" -length 0
Quadrupole -name "CR2.QTR4" -synrad $quad_synrad -length 0.25 -strength [expr 0*$e0] -e0 $e0 

Bpm -name "BPM382" -length 0
Drift -name "CR2.DTR4B" -length 0.2 

Multipole -name "CR2.SXTR4" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 20.76691673*$e0] -e0 $e0 

Drift -name "CR2.DTR4A" -length 2.1328 

Quadrupole -name "CR2.QTR3" -synrad $quad_synrad -length 0.5 -strength [expr -0.1592789427*$e0] -e0 $e0 

Bpm -name "BPM383" -length 0
Drift -name "CR2.DTR3B" -length 0.2 

Multipole -name "CR2.SXTR3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -7.465446283*$e0] -e0 $e0 

Drift -name "CR2.DTR3A" -length 1.5 

Quadrupole -name "CR2.QTR2" -synrad $quad_synrad -length 0.5 -strength [expr 0.512918083*$e0] -e0 $e0 

Bpm -name "BPM384" -length 0
Drift -name "CR2.DTR2B" -length 0.2 

Multipole -name "CR2.SXTR2" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -5.250757706*$e0] -e0 $e0 

Drift -name "CR2.DTR2A" -length 1.1 

Quadrupole -name "CR2.QTR1" -synrad $quad_synrad -length 0.5 -strength [expr -0.371641026*$e0] -e0 $e0 

Bpm -name "BPM385" -length 0
Drift -name "CR2.DTR1B" -length 0.2 

Multipole -name "CR2.SXTR1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 12.54242064*$e0] -e0 $e0 

Drift -name "CR2.DTR1A" -length 1.1 

Drift -name "CR2MDBACELLSTART" -length 0 

Drift -name "CR2.DBA.MCELL" -length 0 

Quadrupole -name "CR2.DBA.QI1" -synrad $quad_synrad -length 0.4 -strength [expr -0.5864293272*$e0] -e0 $e0 

Bpm -name "BPM386" -length 0
Drift -name "CR2.DBA.D11A" -length 0.1730305995 

Multipole -name "CR2.DBA.SNCH1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.D11B" -length 0.1628018956 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM387" -length 0
Drift -name "CR2.DBA.D12" -length 0.2697746031 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM388" -length 0
Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Drift -name "CR2.DBA.HBM" -length 0 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Multipole -name "CR2.DBA.SN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM389" -length 0
Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM390" -length 0
Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Multipole -name "CR2.DBA.SNN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM391" -length 0
Drift -name "CR2.DBA.HCM" -length 0 

Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.D12" -length 0.2697746031 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM392" -length 0
Drift -name "CR2.DBA.D11B1" -length 0.3424874764 

Multipole -name "CR2.DBA.SD0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.146568947*$e0] -e0 $e0 

Drift -name "CR2.DBA.DM0" -length 0.109313245 

Drift -name "CR2.DBA.HCM0" -length 0 

Quadrupole -name "CR2.DBA.QF1" -synrad $quad_synrad -length 0.4 -strength [expr -1.115855102*$e0] -e0 $e0 

Bpm -name "BPM393" -length 0
Drift -name "CR2.DBA.DM1" -length 1.973582219 

Quadrupole -name "CR2.DBA.QF2" -synrad $quad_synrad -length 0.4 -strength [expr 0.6764098228*$e0] -e0 $e0 

Bpm -name "BPM394" -length 0
Drift -name "CR2.DBA.DM2" -length 1.738896147 

Multipole -name "CR2.DBA.SD0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.146568947*$e0] -e0 $e0 

Drift -name "CR2.DBA.DMSEXT1" -length 0.109313245 

Quadrupole -name "CR2.DBA.QF3" -synrad $quad_synrad -length 0.4 -strength [expr -1.118935626*$e0] -e0 $e0 

Bpm -name "BPM395" -length 0
Drift -name "CR2.DBA.DM3" -length 0.2802607113 

Drift -name "CR2.DBA.DMSEXT1" -length 0.109313245 

Drift -name "CR2.DBA.D11B2" -length 0.1628018956 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM396" -length 0
Drift -name "CR2.DBA.D12" -length 0.2697746031 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM397" -length 0
Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Drift -name "CR2.DBA.HBM" -length 0 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Multipole -name "CR2.DBA.SN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM398" -length 0
Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM399" -length 0
Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Multipole -name "CR2.DBA.SNN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM400" -length 0
Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.D12" -length 0.2697746031 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM401" -length 0
Drift -name "CR2.DBA.D11B" -length 0.1628018956 

Multipole -name "CR2.DBA.SNCH1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.D11A" -length 0.1730305995 

Quadrupole -name "CR2.DBA.QI1" -synrad $quad_synrad -length 0.4 -strength [expr -0.5864293272*$e0] -e0 $e0 

Bpm -name "BPM402" -length 0
Drift -name "CR2.DBA.MCELL" -length 0 

Drift -name "CR2MDBACELLEND" -length 0 

Drift -name "CR2.DL0IN" -length 2.246504682 

Quadrupole -name "CR2.QL1IN" -synrad $quad_synrad -length 0.5 -strength [expr -0.642778543*$e0] -e0 $e0 

Bpm -name "BPM403" -length 0
Drift -name "CR2.DL1IN" -length 0.4000469975 

Quadrupole -name "CR2.QL2IN" -synrad $quad_synrad -length 0.5 -strength [expr 0.720003675*$e0] -e0 $e0 

Bpm -name "BPM404" -length 0
Drift -name "CR2.DL2IN" -length 1.103409345 

Quadrupole -name "CR2.QL3IN" -synrad $quad_synrad -length 0.5 -strength [expr -0.2675265744*$e0] -e0 $e0 

Bpm -name "BPM405" -length 0
Drift -name "CR2.DL3IN" -length 0.4000389758 

Drift -name "CR2.MAFT2T" -length 0 

Drift -name "CR2.DRRFH" -length 1 

# WARNING: CORRECTOR needs to be defined, no PLACET element

CORRECTOR -name "CR2.RFKICKPOS" -length 0 -strength_x [expr 0.004*1e6*$e0] 

Drift -name "CR2.DRRFH" -length 1 

Drift -name "CR2.DRFB1" -length 0.217192186 

Quadrupole -name "CR2.QRFB1" -synrad $quad_synrad -length 0.5 -strength [expr -1.08870911*$e0] -e0 $e0 

Bpm -name "BPM406" -length 0
Drift -name "CR2.DRFB2" -length 1.193240969 

Quadrupole -name "CR2.QRFB2" -synrad $quad_synrad -length 0.5 -strength [expr 0.5775372505*$e0] -e0 $e0 

Bpm -name "BPM407" -length 0
Drift -name "CR2.DRFB3" -length 3.771233433 

Quadrupole -name "CR2.QRFB3" -synrad $quad_synrad -length 0.5 -strength [expr -0.3437279899*$e0] -e0 $e0 

Bpm -name "BPM408" -length 0
Drift -name "CR2.D1CM" -length 0.01 

Drift -name "CR2.D1M" -length 1 

Drift -name "CR2.D1M" -length 1 

Drift -name "CR2.D1M" -length 1 

Drift -name "CR2.DRFB4A" -length 0.709135479 

Multipole -name "CR2.SRFB6" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 8.540776596*$e0] -e0 $e0 

Drift -name "CR2.DRFB6" -length 2.367622162 

Drift -name "CR2.SEPTUM" -length 1 

Drift -name "CR2.MRFBSYM" -length 0 

Drift -name "CR2.SEPTUM" -length 1 

Drift -name "CR2.DRFB6" -length 2.367622162 

Multipole -name "CR2.SRFB6" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 8.540776596*$e0] -e0 $e0 

Drift -name "CR2.INJECTION" -length 0 

Drift -name "CR2.D1CM" -length 0.01 

Drift -name "CR2.D1M" -length 1 

Drift -name "CR2.D1M" -length 1 

Drift -name "CR2.D1M" -length 1 

Drift -name "CR2.DRFB4A" -length 0.709135479 

Quadrupole -name "CR2.QRFB3" -synrad $quad_synrad -length 0.5 -strength [expr -0.3437279899*$e0] -e0 $e0 

Bpm -name "BPM409" -length 0
Drift -name "CR2.DRFB3" -length 3.771233433 

Quadrupole -name "CR2.QRFB2" -synrad $quad_synrad -length 0.5 -strength [expr 0.5775372505*$e0] -e0 $e0 

Bpm -name "BPM410" -length 0
Drift -name "CR2.DRFB2" -length 1.193240969 

Quadrupole -name "CR2.QRFB1" -synrad $quad_synrad -length 0.5 -strength [expr -1.08870911*$e0] -e0 $e0 

Bpm -name "BPM411" -length 0
Drift -name "CR2.DRFB1" -length 0.217192186 

Drift -name "CR2.DRRFH" -length 1 

# WARNING: CORRECTOR needs to be defined, no PLACET element

CORRECTOR -name "CR2.RFKICKPOS" -length 0 -strength_x [expr 0.004*1e6*$e0] 

Drift -name "CR2.DRRFH" -length 1 

Drift -name "CR2.DL3IN" -length 0.4000389758 

Quadrupole -name "CR2.QL3IN" -synrad $quad_synrad -length 0.5 -strength [expr -0.2675265744*$e0] -e0 $e0 

Bpm -name "BPM412" -length 0
Drift -name "CR2.DL2IN" -length 1.103409345 

Quadrupole -name "CR2.QL2IN" -synrad $quad_synrad -length 0.5 -strength [expr 0.720003675*$e0] -e0 $e0 

Bpm -name "BPM413" -length 0
Drift -name "CR2.DL1IN" -length 0.4000469975 

Quadrupole -name "CR2.QL1IN" -synrad $quad_synrad -length 0.5 -strength [expr -0.642778543*$e0] -e0 $e0 

Bpm -name "BPM414" -length 0
Drift -name "CR2.DL0IN" -length 2.246504682 

Drift -name "CR2MDBACELLSTART" -length 0 

Drift -name "CR2.DBA.MCELL" -length 0 

Quadrupole -name "CR2.DBA.QI1" -synrad $quad_synrad -length 0.4 -strength [expr -0.5864293272*$e0] -e0 $e0 

Bpm -name "BPM415" -length 0
Drift -name "CR2.DBA.D11A" -length 0.1730305995 

Multipole -name "CR2.DBA.SNCH1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.D11B" -length 0.1628018956 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM416" -length 0
Drift -name "CR2.DBA.D12" -length 0.2697746031 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM417" -length 0
Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Drift -name "CR2.DBA.HBM" -length 0 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Multipole -name "CR2.DBA.SN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM418" -length 0
Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM419" -length 0
Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Multipole -name "CR2.DBA.SNN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM420" -length 0
Drift -name "CR2.DBA.HCM" -length 0 

Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.D12" -length 0.2697746031 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM421" -length 0
Drift -name "CR2.DBA.D11B1" -length 0.3424874764 

Multipole -name "CR2.DBA.SD0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.146568947*$e0] -e0 $e0 

Drift -name "CR2.DBA.DM0" -length 0.109313245 

Drift -name "CR2.DBA.HCM0" -length 0 

Quadrupole -name "CR2.DBA.QF1" -synrad $quad_synrad -length 0.4 -strength [expr -1.115855102*$e0] -e0 $e0 

Bpm -name "BPM422" -length 0
Drift -name "CR2.DBA.DM1" -length 1.973582219 

Quadrupole -name "CR2.DBA.QF2" -synrad $quad_synrad -length 0.4 -strength [expr 0.6764098228*$e0] -e0 $e0 

Bpm -name "BPM423" -length 0
Drift -name "CR2.DBA.DM2" -length 1.738896147 

Multipole -name "CR2.DBA.SD0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.146568947*$e0] -e0 $e0 

Drift -name "CR2.DBA.DMSEXT1" -length 0.109313245 

Quadrupole -name "CR2.DBA.QF3" -synrad $quad_synrad -length 0.4 -strength [expr -1.118935626*$e0] -e0 $e0 

Bpm -name "BPM424" -length 0
Drift -name "CR2.DBA.DM3" -length 0.2802607113 

Drift -name "CR2.DBA.DMSEXT1" -length 0.109313245 

Drift -name "CR2.DBA.D11B2" -length 0.1628018956 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM425" -length 0
Drift -name "CR2.DBA.D12" -length 0.2697746031 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM426" -length 0
Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Drift -name "CR2.DBA.HBM" -length 0 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Multipole -name "CR2.DBA.SN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM427" -length 0
Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM428" -length 0
Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Multipole -name "CR2.DBA.SNN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM429" -length 0
Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.D12" -length 0.2697746031 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM430" -length 0
Drift -name "CR2.DBA.D11B" -length 0.1628018956 

Multipole -name "CR2.DBA.SNCH1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.D11A" -length 0.1730305995 

Quadrupole -name "CR2.DBA.QI1" -synrad $quad_synrad -length 0.4 -strength [expr -0.5864293272*$e0] -e0 $e0 

Bpm -name "BPM431" -length 0
Drift -name "CR2.DBA.MCELL" -length 0 

Drift -name "CR2MDBACELLEND" -length 0 

Drift -name "CR2.DTR1A" -length 1.1 

Multipole -name "CR2.SXTR1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 12.54242064*$e0] -e0 $e0 

Drift -name "CR2.DTR1B" -length 0.2 

Quadrupole -name "CR2.QTR1" -synrad $quad_synrad -length 0.5 -strength [expr -0.371641026*$e0] -e0 $e0 

Bpm -name "BPM432" -length 0
Drift -name "CR2.DTR2A" -length 1.1 

Multipole -name "CR2.SXTR2" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -5.250757706*$e0] -e0 $e0 

Drift -name "CR2.DTR2B" -length 0.2 

Quadrupole -name "CR2.QTR2" -synrad $quad_synrad -length 0.5 -strength [expr 0.512918083*$e0] -e0 $e0 

Bpm -name "BPM433" -length 0
Drift -name "CR2.DTR3A" -length 1.5 

Multipole -name "CR2.SXTR3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -7.465446283*$e0] -e0 $e0 

Drift -name "CR2.DTR3B" -length 0.2 

Quadrupole -name "CR2.QTR3" -synrad $quad_synrad -length 0.5 -strength [expr -0.1592789427*$e0] -e0 $e0 

Bpm -name "BPM434" -length 0
Drift -name "CR2.DTR4A" -length 2.1328 

Multipole -name "CR2.SXTR4" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 20.76691673*$e0] -e0 $e0 

Drift -name "CR2.DTR4B" -length 0.2 

Quadrupole -name "CR2.QTR4" -synrad $quad_synrad -length 0.25 -strength [expr 0*$e0] -e0 $e0 

Bpm -name "BPM435" -length 0
Quadrupole -name "CR2.QTR4" -synrad $quad_synrad -length 0.25 -strength [expr 0*$e0] -e0 $e0 

Bpm -name "BPM436" -length 0
Drift -name "CR2.DTR4B" -length 0.2 

Multipole -name "CR2.SXTR4" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 20.76691673*$e0] -e0 $e0 

Drift -name "CR2.DTR4A" -length 2.1328 

Quadrupole -name "CR2.QTR3" -synrad $quad_synrad -length 0.5 -strength [expr -0.1592789427*$e0] -e0 $e0 

Bpm -name "BPM437" -length 0
Drift -name "CR2.DTR3B" -length 0.2 

Multipole -name "CR2.SXTR3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -7.465446283*$e0] -e0 $e0 

Drift -name "CR2.DTR3A" -length 1.5 

Quadrupole -name "CR2.QTR2" -synrad $quad_synrad -length 0.5 -strength [expr 0.512918083*$e0] -e0 $e0 

Bpm -name "BPM438" -length 0
Drift -name "CR2.DTR2B" -length 0.2 

Multipole -name "CR2.SXTR2" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -5.250757706*$e0] -e0 $e0 

Drift -name "CR2.DTR2A" -length 1.1 

Quadrupole -name "CR2.QTR1" -synrad $quad_synrad -length 0.5 -strength [expr -0.371641026*$e0] -e0 $e0 

Bpm -name "BPM439" -length 0
Drift -name "CR2.DTR1B" -length 0.2 

Multipole -name "CR2.SXTR1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 12.54242064*$e0] -e0 $e0 

Drift -name "CR2.DTR1A" -length 1.1 

Drift -name "CR2MDBACELLSTART" -length 0 

Drift -name "CR2.DBA.MCELL" -length 0 

Quadrupole -name "CR2.DBA.QI1" -synrad $quad_synrad -length 0.4 -strength [expr -0.5864293272*$e0] -e0 $e0 

Bpm -name "BPM440" -length 0
Drift -name "CR2.DBA.D11A" -length 0.1730305995 

Multipole -name "CR2.DBA.SNCH1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.D11B" -length 0.1628018956 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM441" -length 0
Drift -name "CR2.DBA.D12" -length 0.2697746031 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM442" -length 0
Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Drift -name "CR2.DBA.HBM" -length 0 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Multipole -name "CR2.DBA.SN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM443" -length 0
Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM444" -length 0
Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Multipole -name "CR2.DBA.SNN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM445" -length 0
Drift -name "CR2.DBA.HCM" -length 0 

Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.D12" -length 0.2697746031 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM446" -length 0
Drift -name "CR2.DBA.D11B1" -length 0.3424874764 

Multipole -name "CR2.DBA.SD0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.146568947*$e0] -e0 $e0 

Drift -name "CR2.DBA.DM0" -length 0.109313245 

Drift -name "CR2.DBA.HCM0" -length 0 

Quadrupole -name "CR2.DBA.QF1" -synrad $quad_synrad -length 0.4 -strength [expr -1.115855102*$e0] -e0 $e0 

Bpm -name "BPM447" -length 0
Drift -name "CR2.DBA.DM1" -length 1.973582219 

Quadrupole -name "CR2.DBA.QF2" -synrad $quad_synrad -length 0.4 -strength [expr 0.6764098228*$e0] -e0 $e0 

Bpm -name "BPM448" -length 0
Drift -name "CR2.DBA.DM2" -length 1.738896147 

Multipole -name "CR2.DBA.SD0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.146568947*$e0] -e0 $e0 

Drift -name "CR2.DBA.DMSEXT1" -length 0.109313245 

Quadrupole -name "CR2.DBA.QF3" -synrad $quad_synrad -length 0.4 -strength [expr -1.118935626*$e0] -e0 $e0 

Bpm -name "BPM449" -length 0
Drift -name "CR2.DBA.DM3" -length 0.2802607113 

Drift -name "CR2.DBA.DMSEXT1" -length 0.109313245 

Drift -name "CR2.DBA.D11B2" -length 0.1628018956 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM450" -length 0
Drift -name "CR2.DBA.D12" -length 0.2697746031 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM451" -length 0
Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Drift -name "CR2.DBA.HBM" -length 0 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Multipole -name "CR2.DBA.SN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM452" -length 0
Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM453" -length 0
Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Multipole -name "CR2.DBA.SNN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM454" -length 0
Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.D12" -length 0.2697746031 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM455" -length 0
Drift -name "CR2.DBA.D11B" -length 0.1628018956 

Multipole -name "CR2.DBA.SNCH1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.D11A" -length 0.1730305995 

Quadrupole -name "CR2.DBA.QI1" -synrad $quad_synrad -length 0.4 -strength [expr -0.5864293272*$e0] -e0 $e0 

Bpm -name "BPM456" -length 0
Drift -name "CR2.DBA.MCELL" -length 0 

Drift -name "CR2MDBACELLEND" -length 0 

Drift -name "CR2.DS1C_A" -length 2.179474654 

Multipole -name "CR2.SXS1C" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 11.05883366*$e0] -e0 $e0 

Drift -name "CR2.DS1C_B" -length 0.3 

Quadrupole -name "CR2.QS1C" -synrad $quad_synrad -length 0.5 -strength [expr -0.66814696*$e0] -e0 $e0 

Bpm -name "BPM457" -length 0
Drift -name "CR2.DS2C" -length 0.2003907272 

Quadrupole -name "CR2.QS2C" -synrad $quad_synrad -length 0.5 -strength [expr 0.7440336365*$e0] -e0 $e0 

Bpm -name "BPM458" -length 0
Drift -name "CR2.DS3C_A" -length 0.3 

Multipole -name "CR2.SXS3C" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -13.03004983*$e0] -e0 $e0 

Drift -name "CR2.DS3C_B" -length 3.777418619 

Quadrupole -name "CR2.QS3C" -synrad $quad_synrad -length 0.5 -strength [expr -0.03237714152*$e0] -e0 $e0 

Bpm -name "BPM459" -length 0
Drift -name "CR2.DS4C" -length 0.2407160001 

Drift -name "CR2.BC1" -length 0.4 

Drift -name "CR2.BC2" -length 0.4 

Drift -name "CR2.BC3" -length 0.4 

Drift -name "CR2.BC1" -length 0.4 

Drift -name "CR2.DS4C" -length 0.2407160001 

Quadrupole -name "CR2.QS3C" -synrad $quad_synrad -length 0.5 -strength [expr -0.03237714152*$e0] -e0 $e0 

Bpm -name "BPM460" -length 0
Drift -name "CR2.DS3C_B" -length 3.777418619 

Multipole -name "CR2.SXS3C" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -13.03004983*$e0] -e0 $e0 

Drift -name "CR2.DS3C_A" -length 0.3 

Quadrupole -name "CR2.QS2C" -synrad $quad_synrad -length 0.5 -strength [expr 0.7440336365*$e0] -e0 $e0 

Bpm -name "BPM461" -length 0
Drift -name "CR2.DS2C" -length 0.2003907272 

Quadrupole -name "CR2.QS1C" -synrad $quad_synrad -length 0.5 -strength [expr -0.66814696*$e0] -e0 $e0 

Bpm -name "BPM462" -length 0
Drift -name "CR2.DS1C_B" -length 0.3 

Multipole -name "CR2.SXS1C" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 11.05883366*$e0] -e0 $e0 

Drift -name "CR2.DS1C_A" -length 2.179474654 

Drift -name "CR2MDBACELLSTART" -length 0 

Drift -name "CR2.DBA.MCELL" -length 0 

Quadrupole -name "CR2.DBA.QI1" -synrad $quad_synrad -length 0.4 -strength [expr -0.5864293272*$e0] -e0 $e0 

Bpm -name "BPM463" -length 0
Drift -name "CR2.DBA.D11A" -length 0.1730305995 

Multipole -name "CR2.DBA.SNCH1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.D11B" -length 0.1628018956 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM464" -length 0
Drift -name "CR2.DBA.D12" -length 0.2697746031 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM465" -length 0
Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Drift -name "CR2.DBA.HBM" -length 0 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Multipole -name "CR2.DBA.SN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM466" -length 0
Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM467" -length 0
Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Multipole -name "CR2.DBA.SNN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM468" -length 0
Drift -name "CR2.DBA.HCM" -length 0 

Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.D12" -length 0.2697746031 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM469" -length 0
Drift -name "CR2.DBA.D11B1" -length 0.3424874764 

Multipole -name "CR2.DBA.SD0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.146568947*$e0] -e0 $e0 

Drift -name "CR2.DBA.DM0" -length 0.109313245 

Drift -name "CR2.DBA.HCM0" -length 0 

Quadrupole -name "CR2.DBA.QF1" -synrad $quad_synrad -length 0.4 -strength [expr -1.115855102*$e0] -e0 $e0 

Bpm -name "BPM470" -length 0
Drift -name "CR2.DBA.DM1" -length 1.973582219 

Quadrupole -name "CR2.DBA.QF2" -synrad $quad_synrad -length 0.4 -strength [expr 0.6764098228*$e0] -e0 $e0 

Bpm -name "BPM471" -length 0
Drift -name "CR2.DBA.DM2" -length 1.738896147 

Multipole -name "CR2.DBA.SD0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.146568947*$e0] -e0 $e0 

Drift -name "CR2.DBA.DMSEXT1" -length 0.109313245 

Quadrupole -name "CR2.DBA.QF3" -synrad $quad_synrad -length 0.4 -strength [expr -1.118935626*$e0] -e0 $e0 

Bpm -name "BPM472" -length 0
Drift -name "CR2.DBA.DM3" -length 0.2802607113 

Drift -name "CR2.DBA.DMSEXT1" -length 0.109313245 

Drift -name "CR2.DBA.D11B2" -length 0.1628018956 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM473" -length 0
Drift -name "CR2.DBA.D12" -length 0.2697746031 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM474" -length 0
Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Drift -name "CR2.DBA.HBM" -length 0 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Multipole -name "CR2.DBA.SN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM475" -length 0
Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM476" -length 0
Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Multipole -name "CR2.DBA.SNN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM477" -length 0
Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.D12" -length 0.2697746031 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM478" -length 0
Drift -name "CR2.DBA.D11B" -length 0.1628018956 

Multipole -name "CR2.DBA.SNCH1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.D11A" -length 0.1730305995 

Quadrupole -name "CR2.DBA.QI1" -synrad $quad_synrad -length 0.4 -strength [expr -0.5864293272*$e0] -e0 $e0 

Bpm -name "BPM479" -length 0
Drift -name "CR2.DBA.MCELL" -length 0 

Drift -name "CR2MDBACELLEND" -length 0 

Drift -name "CR2.DTR1A" -length 1.1 

Multipole -name "CR2.SXTR1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 12.54242064*$e0] -e0 $e0 

Drift -name "CR2.DTR1B" -length 0.2 

Quadrupole -name "CR2.QTR1" -synrad $quad_synrad -length 0.5 -strength [expr -0.371641026*$e0] -e0 $e0 

Bpm -name "BPM480" -length 0
Drift -name "CR2.DTR2A" -length 1.1 

Multipole -name "CR2.SXTR2" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -5.250757706*$e0] -e0 $e0 

Drift -name "CR2.DTR2B" -length 0.2 

Quadrupole -name "CR2.QTR2" -synrad $quad_synrad -length 0.5 -strength [expr 0.512918083*$e0] -e0 $e0 

Bpm -name "BPM481" -length 0
Drift -name "CR2.DTR3A" -length 1.5 

Multipole -name "CR2.SXTR3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -7.465446283*$e0] -e0 $e0 

Drift -name "CR2.DTR3B" -length 0.2 

Quadrupole -name "CR2.QTR3" -synrad $quad_synrad -length 0.5 -strength [expr -0.1592789427*$e0] -e0 $e0 

Bpm -name "BPM482" -length 0
Drift -name "CR2.DTR4A" -length 2.1328 

Multipole -name "CR2.SXTR4" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 20.76691673*$e0] -e0 $e0 

Drift -name "CR2.DTR4B" -length 0.2 

Quadrupole -name "CR2.QTR4" -synrad $quad_synrad -length 0.25 -strength [expr 0*$e0] -e0 $e0 

Bpm -name "BPM483" -length 0
Quadrupole -name "CR2.QTR4" -synrad $quad_synrad -length 0.25 -strength [expr 0*$e0] -e0 $e0 

Bpm -name "BPM484" -length 0
Drift -name "CR2.DTR4B" -length 0.2 

Multipole -name "CR2.SXTR4" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 20.76691673*$e0] -e0 $e0 

Drift -name "CR2.DTR4A" -length 2.1328 

Quadrupole -name "CR2.QTR3" -synrad $quad_synrad -length 0.5 -strength [expr -0.1592789427*$e0] -e0 $e0 

Bpm -name "BPM485" -length 0
Drift -name "CR2.DTR3B" -length 0.2 

Multipole -name "CR2.SXTR3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -7.465446283*$e0] -e0 $e0 

Drift -name "CR2.DTR3A" -length 1.5 

Quadrupole -name "CR2.QTR2" -synrad $quad_synrad -length 0.5 -strength [expr 0.512918083*$e0] -e0 $e0 

Bpm -name "BPM486" -length 0
Drift -name "CR2.DTR2B" -length 0.2 

Multipole -name "CR2.SXTR2" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -5.250757706*$e0] -e0 $e0 

Drift -name "CR2.DTR2A" -length 1.1 

Quadrupole -name "CR2.QTR1" -synrad $quad_synrad -length 0.5 -strength [expr -0.371641026*$e0] -e0 $e0 

Bpm -name "BPM487" -length 0
Drift -name "CR2.DTR1B" -length 0.2 

Multipole -name "CR2.SXTR1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 12.54242064*$e0] -e0 $e0 

Drift -name "CR2.DTR1A" -length 1.1 

Drift -name "CR2MDBACELLSTART" -length 0 

Drift -name "CR2.DBA.MCELL" -length 0 

Quadrupole -name "CR2.DBA.QI1" -synrad $quad_synrad -length 0.4 -strength [expr -0.5864293272*$e0] -e0 $e0 

Bpm -name "BPM488" -length 0
Drift -name "CR2.DBA.D11A" -length 0.1730305995 

Multipole -name "CR2.DBA.SNCH1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.D11B" -length 0.1628018956 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM489" -length 0
Drift -name "CR2.DBA.D12" -length 0.2697746031 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM490" -length 0
Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Drift -name "CR2.DBA.HBM" -length 0 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Multipole -name "CR2.DBA.SN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM491" -length 0
Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM492" -length 0
Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Multipole -name "CR2.DBA.SNN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM493" -length 0
Drift -name "CR2.DBA.HCM" -length 0 

Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.D12" -length 0.2697746031 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM494" -length 0
Drift -name "CR2.DBA.D11B1" -length 0.3424874764 

Multipole -name "CR2.DBA.SD0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.146568947*$e0] -e0 $e0 

Drift -name "CR2.DBA.DM0" -length 0.109313245 

Drift -name "CR2.DBA.HCM0" -length 0 

Quadrupole -name "CR2.DBA.QF1" -synrad $quad_synrad -length 0.4 -strength [expr -1.115855102*$e0] -e0 $e0 

Bpm -name "BPM495" -length 0
Drift -name "CR2.DBA.DM1" -length 1.973582219 

Quadrupole -name "CR2.DBA.QF2" -synrad $quad_synrad -length 0.4 -strength [expr 0.6764098228*$e0] -e0 $e0 

Bpm -name "BPM496" -length 0
Drift -name "CR2.DBA.DM2" -length 1.738896147 

Multipole -name "CR2.DBA.SD0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.146568947*$e0] -e0 $e0 

Drift -name "CR2.DBA.DMSEXT1" -length 0.109313245 

Quadrupole -name "CR2.DBA.QF3" -synrad $quad_synrad -length 0.4 -strength [expr -1.118935626*$e0] -e0 $e0 

Bpm -name "BPM497" -length 0
Drift -name "CR2.DBA.DM3" -length 0.2802607113 

Drift -name "CR2.DBA.DMSEXT1" -length 0.109313245 

Drift -name "CR2.DBA.D11B2" -length 0.1628018956 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM498" -length 0
Drift -name "CR2.DBA.D12" -length 0.2697746031 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM499" -length 0
Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Drift -name "CR2.DBA.HBM" -length 0 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Multipole -name "CR2.DBA.SN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM500" -length 0
Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM501" -length 0
Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Multipole -name "CR2.DBA.SNN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM502" -length 0
Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.D12" -length 0.2697746031 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM503" -length 0
Drift -name "CR2.DBA.D11B" -length 0.1628018956 

Multipole -name "CR2.DBA.SNCH1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.D11A" -length 0.1730305995 

Quadrupole -name "CR2.DBA.QI1" -synrad $quad_synrad -length 0.4 -strength [expr -0.5864293272*$e0] -e0 $e0 

Bpm -name "BPM504" -length 0
Drift -name "CR2.DBA.MCELL" -length 0 

Drift -name "CR2MDBACELLEND" -length 0 

Drift -name "CR2.DL0IN" -length 2.246504682 

Quadrupole -name "CR2.QL1IN" -synrad $quad_synrad -length 0.5 -strength [expr -0.642778543*$e0] -e0 $e0 

Bpm -name "BPM505" -length 0
Drift -name "CR2.DL1IN" -length 0.4000469975 

Quadrupole -name "CR2.QL2IN" -synrad $quad_synrad -length 0.5 -strength [expr 0.720003675*$e0] -e0 $e0 

Bpm -name "BPM506" -length 0
Drift -name "CR2.DL2IN" -length 1.103409345 

Quadrupole -name "CR2.QL3IN" -synrad $quad_synrad -length 0.5 -strength [expr -0.2675265744*$e0] -e0 $e0 

Bpm -name "BPM507" -length 0
Drift -name "CR2.DL3IN" -length 0.4000389758 

Drift -name "CR2.MENTEXTKCK" -length 0 

Drift -name "CR2.DRRF" -length 2 

Drift -name "CR2.DRFB1" -length 0.217192186 

Quadrupole -name "CR2.QRFB1" -synrad $quad_synrad -length 0.5 -strength [expr -1.08870911*$e0] -e0 $e0 

Bpm -name "BPM508" -length 0
Drift -name "CR2.DRFB2" -length 1.193240969 

Quadrupole -name "CR2.QRFB2" -synrad $quad_synrad -length 0.5 -strength [expr 0.5775372505*$e0] -e0 $e0 

Bpm -name "BPM509" -length 0
Drift -name "CR2.DRFB3" -length 3.771233433 

Quadrupole -name "CR2.QRFB3" -synrad $quad_synrad -length 0.5 -strength [expr -0.3437279899*$e0] -e0 $e0 

Bpm -name "BPM510" -length 0
Drift -name "CR2.D1CM" -length 0.01 

Drift -name "CR2.D1M" -length 1 

Drift -name "CR2.D1M" -length 1 

Drift -name "CR2.D1M" -length 1 

Drift -name "CR2.DRFB4A" -length 0.709135479 
Bpm -name "CR2.EJECTION" -length 0

Drift -name "CR2.DSRFB6" -length 0.4 

Drift -name "CR2.DRFB6" -length 2.367622162 

Drift -name "CR2.SEPTUM" -length 1 
Drift -name "CR2.SEPTUM" -length 1 

Drift -name "CR2.DRFB6" -length 2.367622162 

Drift -name "CR2.DSRFB6" -length 0.4 

Drift -name "CR2.DRFB4A" -length 0.709135479 

Drift -name "CR2.D1M" -length 1 

Drift -name "CR2.D1M" -length 1 

Drift -name "CR2.D1M" -length 1 

Drift -name "CR2.D1CM" -length 0.01 

Quadrupole -name "CR2.QRFB3" -synrad $quad_synrad -length 0.5 -strength [expr -0.3437279899*$e0] -e0 $e0 

Bpm -name "BPM511" -length 0
Drift -name "CR2.DRFB3" -length 3.771233433 

Quadrupole -name "CR2.QRFB2" -synrad $quad_synrad -length 0.5 -strength [expr 0.5775372505*$e0] -e0 $e0 

Bpm -name "BPM512" -length 0
Drift -name "CR2.DRFB2" -length 1.193240969 

Quadrupole -name "CR2.QRFB1" -synrad $quad_synrad -length 0.5 -strength [expr -1.08870911*$e0] -e0 $e0 

Bpm -name "BPM513" -length 0
Drift -name "CR2.DRFB1" -length 0.217192186 

Drift -name "CR2.DRRF" -length 2 

Drift -name "CR2.MENTEXTKCK" -length 0 

Drift -name "CR2.DL3IN" -length 0.4000389758 

Quadrupole -name "CR2.QL3IN" -synrad $quad_synrad -length 0.5 -strength [expr -0.2675265744*$e0] -e0 $e0 

Bpm -name "BPM514" -length 0
Drift -name "CR2.DL2IN" -length 1.103409345 

Quadrupole -name "CR2.QL2IN" -synrad $quad_synrad -length 0.5 -strength [expr 0.720003675*$e0] -e0 $e0 

Bpm -name "BPM515" -length 0
Drift -name "CR2.DL1IN" -length 0.4000469975 

Quadrupole -name "CR2.QL1IN" -synrad $quad_synrad -length 0.5 -strength [expr -0.642778543*$e0] -e0 $e0 

Bpm -name "BPM516" -length 0
Drift -name "CR2.DL0IN" -length 2.246504682 

Drift -name "CR2MDBACELLSTART" -length 0 

Drift -name "CR2.DBA.MCELL" -length 0 

Quadrupole -name "CR2.DBA.QI1" -synrad $quad_synrad -length 0.4 -strength [expr -0.5864293272*$e0] -e0 $e0 

Bpm -name "BPM517" -length 0
Drift -name "CR2.DBA.D11A" -length 0.1730305995 

Multipole -name "CR2.DBA.SNCH1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.D11B" -length 0.1628018956 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM518" -length 0
Drift -name "CR2.DBA.D12" -length 0.2697746031 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM519" -length 0
Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Drift -name "CR2.DBA.HBM" -length 0 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Multipole -name "CR2.DBA.SN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM520" -length 0
Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM521" -length 0
Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Multipole -name "CR2.DBA.SNN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM522" -length 0
Drift -name "CR2.DBA.HCM" -length 0 

Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.D12" -length 0.2697746031 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM523" -length 0
Drift -name "CR2.DBA.D11B1" -length 0.3424874764 

Multipole -name "CR2.DBA.SD0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.146568947*$e0] -e0 $e0 

Drift -name "CR2.DBA.DM0" -length 0.109313245 

Drift -name "CR2.DBA.HCM0" -length 0 

Quadrupole -name "CR2.DBA.QF1" -synrad $quad_synrad -length 0.4 -strength [expr -1.115855102*$e0] -e0 $e0 

Bpm -name "BPM524" -length 0
Drift -name "CR2.DBA.DM1" -length 1.973582219 

Quadrupole -name "CR2.DBA.QF2" -synrad $quad_synrad -length 0.4 -strength [expr 0.6764098228*$e0] -e0 $e0 

Bpm -name "BPM525" -length 0
Drift -name "CR2.DBA.DM2" -length 1.738896147 

Multipole -name "CR2.DBA.SD0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.146568947*$e0] -e0 $e0 

Drift -name "CR2.DBA.DMSEXT1" -length 0.109313245 

Quadrupole -name "CR2.DBA.QF3" -synrad $quad_synrad -length 0.4 -strength [expr -1.118935626*$e0] -e0 $e0 

Bpm -name "BPM526" -length 0
Drift -name "CR2.DBA.DM3" -length 0.2802607113 

Drift -name "CR2.DBA.DMSEXT1" -length 0.109313245 

Drift -name "CR2.DBA.D11B2" -length 0.1628018956 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM527" -length 0
Drift -name "CR2.DBA.D12" -length 0.2697746031 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM528" -length 0
Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Drift -name "CR2.DBA.HBM" -length 0 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Multipole -name "CR2.DBA.SN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM529" -length 0
Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM530" -length 0
Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Multipole -name "CR2.DBA.SNN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM531" -length 0
Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.D12" -length 0.2697746031 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM532" -length 0
Drift -name "CR2.DBA.D11B" -length 0.1628018956 

Multipole -name "CR2.DBA.SNCH1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.D11A" -length 0.1730305995 

Quadrupole -name "CR2.DBA.QI1" -synrad $quad_synrad -length 0.4 -strength [expr -0.5864293272*$e0] -e0 $e0 

Bpm -name "BPM533" -length 0
Drift -name "CR2.DBA.MCELL" -length 0 

Drift -name "CR2MDBACELLEND" -length 0 

Drift -name "CR2.DTR1A" -length 1.1 

Multipole -name "CR2.SXTR1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 12.54242064*$e0] -e0 $e0 

Drift -name "CR2.DTR1B" -length 0.2 

Quadrupole -name "CR2.QTR1" -synrad $quad_synrad -length 0.5 -strength [expr -0.371641026*$e0] -e0 $e0 

Bpm -name "BPM534" -length 0
Drift -name "CR2.DTR2A" -length 1.1 

Multipole -name "CR2.SXTR2" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -5.250757706*$e0] -e0 $e0 

Drift -name "CR2.DTR2B" -length 0.2 

Quadrupole -name "CR2.QTR2" -synrad $quad_synrad -length 0.5 -strength [expr 0.512918083*$e0] -e0 $e0 

Bpm -name "BPM535" -length 0
Drift -name "CR2.DTR3A" -length 1.5 

Multipole -name "CR2.SXTR3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -7.465446283*$e0] -e0 $e0 

Drift -name "CR2.DTR3B" -length 0.2 

Quadrupole -name "CR2.QTR3" -synrad $quad_synrad -length 0.5 -strength [expr -0.1592789427*$e0] -e0 $e0 

Bpm -name "BPM536" -length 0
Drift -name "CR2.DTR4A" -length 2.1328 

Multipole -name "CR2.SXTR4" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 20.76691673*$e0] -e0 $e0 

Drift -name "CR2.DTR4B" -length 0.2 

Quadrupole -name "CR2.QTR4" -synrad $quad_synrad -length 0.25 -strength [expr 0*$e0] -e0 $e0 

Bpm -name "BPM537" -length 0
Quadrupole -name "CR2.QTR4" -synrad $quad_synrad -length 0.25 -strength [expr 0*$e0] -e0 $e0 

Bpm -name "BPM538" -length 0
Drift -name "CR2.DTR4B" -length 0.2 

Multipole -name "CR2.SXTR4" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 20.76691673*$e0] -e0 $e0 

Drift -name "CR2.DTR4A" -length 2.1328 

Quadrupole -name "CR2.QTR3" -synrad $quad_synrad -length 0.5 -strength [expr -0.1592789427*$e0] -e0 $e0 

Bpm -name "BPM539" -length 0
Drift -name "CR2.DTR3B" -length 0.2 

Multipole -name "CR2.SXTR3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -7.465446283*$e0] -e0 $e0 

Drift -name "CR2.DTR3A" -length 1.5 

Quadrupole -name "CR2.QTR2" -synrad $quad_synrad -length 0.5 -strength [expr 0.512918083*$e0] -e0 $e0 

Bpm -name "BPM540" -length 0
Drift -name "CR2.DTR2B" -length 0.2 

Multipole -name "CR2.SXTR2" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -5.250757706*$e0] -e0 $e0 

Drift -name "CR2.DTR2A" -length 1.1 

Quadrupole -name "CR2.QTR1" -synrad $quad_synrad -length 0.5 -strength [expr -0.371641026*$e0] -e0 $e0 

Bpm -name "BPM541" -length 0
Drift -name "CR2.DTR1B" -length 0.2 

Multipole -name "CR2.SXTR1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 12.54242064*$e0] -e0 $e0 

Drift -name "CR2.DTR1A" -length 1.1 

Drift -name "CR2MDBACELLSTART" -length 0 

Drift -name "CR2.DBA.MCELL" -length 0 

Quadrupole -name "CR2.DBA.QI1" -synrad $quad_synrad -length 0.4 -strength [expr -0.5864293272*$e0] -e0 $e0 

Bpm -name "BPM542" -length 0
Drift -name "CR2.DBA.D11A" -length 0.1730305995 

Multipole -name "CR2.DBA.SNCH1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.D11B" -length 0.1628018956 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM543" -length 0
Drift -name "CR2.DBA.D12" -length 0.2697746031 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM544" -length 0
Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Drift -name "CR2.DBA.HBM" -length 0 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Multipole -name "CR2.DBA.SN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM545" -length 0
Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM546" -length 0
Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Multipole -name "CR2.DBA.SNN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM547" -length 0
Drift -name "CR2.DBA.HCM" -length 0 

Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.D12" -length 0.2697746031 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM548" -length 0
Drift -name "CR2.DBA.D11B1" -length 0.3424874764 

Multipole -name "CR2.DBA.SD0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.146568947*$e0] -e0 $e0 

Drift -name "CR2.DBA.DM0" -length 0.109313245 

Drift -name "CR2.DBA.HCM0" -length 0 

Quadrupole -name "CR2.DBA.QF1" -synrad $quad_synrad -length 0.4 -strength [expr -1.115855102*$e0] -e0 $e0 

Bpm -name "BPM549" -length 0
Drift -name "CR2.DBA.DM1" -length 1.973582219 

Quadrupole -name "CR2.DBA.QF2" -synrad $quad_synrad -length 0.4 -strength [expr 0.6764098228*$e0] -e0 $e0 

Bpm -name "BPM550" -length 0
Drift -name "CR2.DBA.DM2" -length 1.738896147 

Multipole -name "CR2.DBA.SD0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.146568947*$e0] -e0 $e0 

Drift -name "CR2.DBA.DMSEXT1" -length 0.109313245 

Quadrupole -name "CR2.DBA.QF3" -synrad $quad_synrad -length 0.4 -strength [expr -1.118935626*$e0] -e0 $e0 

Bpm -name "BPM551" -length 0
Drift -name "CR2.DBA.DM3" -length 0.2802607113 

Drift -name "CR2.DBA.DMSEXT1" -length 0.109313245 

Drift -name "CR2.DBA.D11B2" -length 0.1628018956 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM552" -length 0
Drift -name "CR2.DBA.D12" -length 0.2697746031 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM553" -length 0
Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Drift -name "CR2.DBA.HBM" -length 0 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Multipole -name "CR2.DBA.SN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM554" -length 0
Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM555" -length 0
Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Multipole -name "CR2.DBA.SNN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM556" -length 0
Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.D12" -length 0.2697746031 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM557" -length 0
Drift -name "CR2.DBA.D11B" -length 0.1628018956 

Multipole -name "CR2.DBA.SNCH1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.D11A" -length 0.1730305995 

Quadrupole -name "CR2.DBA.QI1" -synrad $quad_synrad -length 0.4 -strength [expr -0.5864293272*$e0] -e0 $e0 

Bpm -name "BPM558" -length 0
Drift -name "CR2.DBA.MCELL" -length 0 

Drift -name "CR2MDBACELLEND" -length 0 

Drift -name "CR2.DS1C_A" -length 2.179474654 

Multipole -name "CR2.SXS1C" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 11.05883366*$e0] -e0 $e0 

Drift -name "CR2.DS1C_B" -length 0.3 

Quadrupole -name "CR2.QS1C" -synrad $quad_synrad -length 0.5 -strength [expr -0.66814696*$e0] -e0 $e0 

Bpm -name "BPM559" -length 0
Drift -name "CR2.DS2C" -length 0.2003907272 

Quadrupole -name "CR2.QS2C" -synrad $quad_synrad -length 0.5 -strength [expr 0.7440336365*$e0] -e0 $e0 

Bpm -name "BPM560" -length 0
Drift -name "CR2.DS3C_A" -length 0.3 

Multipole -name "CR2.SXS3C" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -13.03004983*$e0] -e0 $e0 

Drift -name "CR2.DS3C_B" -length 3.777418619 

Quadrupole -name "CR2.QS3C" -synrad $quad_synrad -length 0.5 -strength [expr -0.03237714152*$e0] -e0 $e0 

Bpm -name "BPM561" -length 0
Drift -name "CR2.DS4C" -length 0.2407160001 

Drift -name "CR2.BC1" -length 0.4 

Drift -name "CR2.BC2" -length 0.4 

Drift -name "CR2.BC3" -length 0.4 

Drift -name "CR2.BC1" -length 0.4 

Drift -name "CR2.DS4C" -length 0.2407160001 

Quadrupole -name "CR2.QS3C" -synrad $quad_synrad -length 0.5 -strength [expr -0.03237714152*$e0] -e0 $e0 

Bpm -name "BPM562" -length 0
Drift -name "CR2.DS3C_B" -length 3.777418619 

Multipole -name "CR2.SXS3C" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -13.03004983*$e0] -e0 $e0 

Drift -name "CR2.DS3C_A" -length 0.3 

Quadrupole -name "CR2.QS2C" -synrad $quad_synrad -length 0.5 -strength [expr 0.7440336365*$e0] -e0 $e0 

Bpm -name "BPM563" -length 0
Drift -name "CR2.DS2C" -length 0.2003907272 

Quadrupole -name "CR2.QS1C" -synrad $quad_synrad -length 0.5 -strength [expr -0.66814696*$e0] -e0 $e0 

Bpm -name "BPM564" -length 0
Drift -name "CR2.DS1C_B" -length 0.3 

Multipole -name "CR2.SXS1C" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 11.05883366*$e0] -e0 $e0 

Drift -name "CR2.DS1C_A" -length 2.179474654 

Drift -name "CR2MDBACELLSTART" -length 0 

Drift -name "CR2.DBA.MCELL" -length 0 

Quadrupole -name "CR2.DBA.QI1" -synrad $quad_synrad -length 0.4 -strength [expr -0.5864293272*$e0] -e0 $e0 

Bpm -name "BPM565" -length 0
Drift -name "CR2.DBA.D11A" -length 0.1730305995 

Multipole -name "CR2.DBA.SNCH1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.D11B" -length 0.1628018956 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM566" -length 0
Drift -name "CR2.DBA.D12" -length 0.2697746031 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM567" -length 0
Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Drift -name "CR2.DBA.HBM" -length 0 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Multipole -name "CR2.DBA.SN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM568" -length 0
Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM569" -length 0
Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Multipole -name "CR2.DBA.SNN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM570" -length 0
Drift -name "CR2.DBA.HCM" -length 0 

Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.D12" -length 0.2697746031 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM571" -length 0
Drift -name "CR2.DBA.D11B1" -length 0.3424874764 

Multipole -name "CR2.DBA.SD0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.146568947*$e0] -e0 $e0 

Drift -name "CR2.DBA.DM0" -length 0.109313245 

Drift -name "CR2.DBA.HCM0" -length 0 

Quadrupole -name "CR2.DBA.QF1" -synrad $quad_synrad -length 0.4 -strength [expr -1.115855102*$e0] -e0 $e0 

Bpm -name "BPM572" -length 0
Drift -name "CR2.DBA.DM1" -length 1.973582219 

Quadrupole -name "CR2.DBA.QF2" -synrad $quad_synrad -length 0.4 -strength [expr 0.6764098228*$e0] -e0 $e0 

Bpm -name "BPM573" -length 0
Drift -name "CR2.DBA.DM2" -length 1.738896147 

Multipole -name "CR2.DBA.SD0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.146568947*$e0] -e0 $e0 

Drift -name "CR2.DBA.DMSEXT1" -length 0.109313245 

Quadrupole -name "CR2.DBA.QF3" -synrad $quad_synrad -length 0.4 -strength [expr -1.118935626*$e0] -e0 $e0 

Bpm -name "BPM574" -length 0
Drift -name "CR2.DBA.DM3" -length 0.2802607113 

Drift -name "CR2.DBA.DMSEXT1" -length 0.109313245 

Drift -name "CR2.DBA.D11B2" -length 0.1628018956 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM575" -length 0
Drift -name "CR2.DBA.D12" -length 0.2697746031 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM576" -length 0
Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Drift -name "CR2.DBA.HBM" -length 0 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Multipole -name "CR2.DBA.SN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM577" -length 0
Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM578" -length 0
Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Multipole -name "CR2.DBA.SNN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM579" -length 0
Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.D12" -length 0.2697746031 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM580" -length 0
Drift -name "CR2.DBA.D11B" -length 0.1628018956 

Multipole -name "CR2.DBA.SNCH1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.D11A" -length 0.1730305995 

Quadrupole -name "CR2.DBA.QI1" -synrad $quad_synrad -length 0.4 -strength [expr -0.5864293272*$e0] -e0 $e0 

Bpm -name "BPM581" -length 0
Drift -name "CR2.DBA.MCELL" -length 0 

Drift -name "CR2MDBACELLEND" -length 0 

Drift -name "CR2.DTR1A" -length 1.1 

Multipole -name "CR2.SXTR1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 12.54242064*$e0] -e0 $e0 

Drift -name "CR2.DTR1B" -length 0.2 

Quadrupole -name "CR2.QTR1" -synrad $quad_synrad -length 0.5 -strength [expr -0.371641026*$e0] -e0 $e0 

Bpm -name "BPM582" -length 0
Drift -name "CR2.DTR2A" -length 1.1 

Multipole -name "CR2.SXTR2" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -5.250757706*$e0] -e0 $e0 

Drift -name "CR2.DTR2B" -length 0.2 

Quadrupole -name "CR2.QTR2" -synrad $quad_synrad -length 0.5 -strength [expr 0.512918083*$e0] -e0 $e0 

Bpm -name "BPM583" -length 0
Drift -name "CR2.DTR3A" -length 1.5 

Multipole -name "CR2.SXTR3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -7.465446283*$e0] -e0 $e0 

Drift -name "CR2.DTR3B" -length 0.2 

Quadrupole -name "CR2.QTR3" -synrad $quad_synrad -length 0.5 -strength [expr -0.1592789427*$e0] -e0 $e0 

Bpm -name "BPM584" -length 0
Drift -name "CR2.DTR4A" -length 2.1328 

Multipole -name "CR2.SXTR4" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 20.76691673*$e0] -e0 $e0 

Drift -name "CR2.DTR4B" -length 0.2 

Quadrupole -name "CR2.QTR4" -synrad $quad_synrad -length 0.25 -strength [expr 0*$e0] -e0 $e0 

Bpm -name "BPM585" -length 0
Quadrupole -name "CR2.QTR4" -synrad $quad_synrad -length 0.25 -strength [expr 0*$e0] -e0 $e0 

Bpm -name "BPM586" -length 0
Drift -name "CR2.DTR4B" -length 0.2 

Multipole -name "CR2.SXTR4" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 20.76691673*$e0] -e0 $e0 

Drift -name "CR2.DTR4A" -length 2.1328 

Quadrupole -name "CR2.QTR3" -synrad $quad_synrad -length 0.5 -strength [expr -0.1592789427*$e0] -e0 $e0 

Bpm -name "BPM587" -length 0
Drift -name "CR2.DTR3B" -length 0.2 

Multipole -name "CR2.SXTR3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -7.465446283*$e0] -e0 $e0 

Drift -name "CR2.DTR3A" -length 1.5 

Quadrupole -name "CR2.QTR2" -synrad $quad_synrad -length 0.5 -strength [expr 0.512918083*$e0] -e0 $e0 

Bpm -name "BPM588" -length 0
Drift -name "CR2.DTR2B" -length 0.2 

Multipole -name "CR2.SXTR2" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -5.250757706*$e0] -e0 $e0 

Drift -name "CR2.DTR2A" -length 1.1 

Quadrupole -name "CR2.QTR1" -synrad $quad_synrad -length 0.5 -strength [expr -0.371641026*$e0] -e0 $e0 

Bpm -name "BPM589" -length 0
Drift -name "CR2.DTR1B" -length 0.2 

Multipole -name "CR2.SXTR1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 12.54242064*$e0] -e0 $e0 

Drift -name "CR2.DTR1A" -length 1.1 

Drift -name "CR2MDBACELLSTART" -length 0 

Drift -name "CR2.DBA.MCELL" -length 0 

Quadrupole -name "CR2.DBA.QI1" -synrad $quad_synrad -length 0.4 -strength [expr -0.5864293272*$e0] -e0 $e0 

Bpm -name "BPM590" -length 0
Drift -name "CR2.DBA.D11A" -length 0.1730305995 

Multipole -name "CR2.DBA.SNCH1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.D11B" -length 0.1628018956 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM591" -length 0
Drift -name "CR2.DBA.D12" -length 0.2697746031 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM592" -length 0
Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Drift -name "CR2.DBA.HBM" -length 0 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Multipole -name "CR2.DBA.SN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM593" -length 0
Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM594" -length 0
Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Multipole -name "CR2.DBA.SNN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM595" -length 0
Drift -name "CR2.DBA.HCM" -length 0 

Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.D12" -length 0.2697746031 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM596" -length 0
Drift -name "CR2.DBA.D11B1" -length 0.3424874764 

Multipole -name "CR2.DBA.SD0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.146568947*$e0] -e0 $e0 

Drift -name "CR2.DBA.DM0" -length 0.109313245 

Drift -name "CR2.DBA.HCM0" -length 0 

Quadrupole -name "CR2.DBA.QF1" -synrad $quad_synrad -length 0.4 -strength [expr -1.115855102*$e0] -e0 $e0 

Bpm -name "BPM597" -length 0
Drift -name "CR2.DBA.DM1" -length 1.973582219 

Quadrupole -name "CR2.DBA.QF2" -synrad $quad_synrad -length 0.4 -strength [expr 0.6764098228*$e0] -e0 $e0 

Bpm -name "BPM598" -length 0
Drift -name "CR2.DBA.DM2" -length 1.738896147 

Multipole -name "CR2.DBA.SD0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.146568947*$e0] -e0 $e0 

Drift -name "CR2.DBA.DMSEXT1" -length 0.109313245 

Quadrupole -name "CR2.DBA.QF3" -synrad $quad_synrad -length 0.4 -strength [expr -1.118935626*$e0] -e0 $e0 

Bpm -name "BPM599" -length 0
Drift -name "CR2.DBA.DM3" -length 0.2802607113 

Drift -name "CR2.DBA.DMSEXT1" -length 0.109313245 

Drift -name "CR2.DBA.D11B2" -length 0.1628018956 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM600" -length 0
Drift -name "CR2.DBA.D12" -length 0.2697746031 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM601" -length 0
Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Drift -name "CR2.DBA.HBM" -length 0 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Multipole -name "CR2.DBA.SN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM602" -length 0
Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM603" -length 0
Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Multipole -name "CR2.DBA.SNN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM604" -length 0
Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.D12" -length 0.2697746031 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM605" -length 0
Drift -name "CR2.DBA.D11B" -length 0.1628018956 

Multipole -name "CR2.DBA.SNCH1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.D11A" -length 0.1730305995 

Quadrupole -name "CR2.DBA.QI1" -synrad $quad_synrad -length 0.4 -strength [expr -0.5864293272*$e0] -e0 $e0 

Bpm -name "BPM606" -length 0
Drift -name "CR2.DBA.MCELL" -length 0 

Drift -name "CR2MDBACELLEND" -length 0 

Drift -name "CR2.DL0IN" -length 2.246504682 

Quadrupole -name "CR2.QL1IN" -synrad $quad_synrad -length 0.5 -strength [expr -0.642778543*$e0] -e0 $e0 

Bpm -name "BPM607" -length 0
Drift -name "CR2.DL1IN" -length 0.4000469975 

Quadrupole -name "CR2.QL2IN" -synrad $quad_synrad -length 0.5 -strength [expr 0.720003675*$e0] -e0 $e0 

Bpm -name "BPM608" -length 0
Drift -name "CR2.DL2IN" -length 1.103409345 

Quadrupole -name "CR2.QL3IN" -synrad $quad_synrad -length 0.5 -strength [expr -0.2675265744*$e0] -e0 $e0 

Bpm -name "BPM609" -length 0
Drift -name "CR2.DL3IN" -length 0.4000389758 

Drift -name "CR2.MAFT3T" -length 0 

Drift -name "CR2.DRRFH" -length 1 

Drift -name "CR2.RFKICKZERO" -length 0 

Drift -name "CR2.DRRFH" -length 1 

Drift -name "CR2.DRFB1" -length 0.217192186 

Quadrupole -name "CR2.QRFB1" -synrad $quad_synrad -length 0.5 -strength [expr -1.08870911*$e0] -e0 $e0 

Bpm -name "BPM610" -length 0
Drift -name "CR2.DRFB2" -length 1.193240969 

Quadrupole -name "CR2.QRFB2" -synrad $quad_synrad -length 0.5 -strength [expr 0.5775372505*$e0] -e0 $e0 

Bpm -name "BPM611" -length 0
Drift -name "CR2.DRFB3" -length 3.771233433 

Quadrupole -name "CR2.QRFB3" -synrad $quad_synrad -length 0.5 -strength [expr -0.3437279899*$e0] -e0 $e0 

Bpm -name "BPM612" -length 0
Drift -name "CR2.D1CM" -length 0.01 

Drift -name "CR2.D1M" -length 1 

Drift -name "CR2.D1M" -length 1 

Drift -name "CR2.D1M" -length 1 

Drift -name "CR2.DRFB4A" -length 0.709135479 

Multipole -name "CR2.SRFB6" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 8.540776596*$e0] -e0 $e0 

Drift -name "CR2.DRFB6" -length 2.367622162 

Drift -name "CR2.SEPTUM" -length 1 

Drift -name "CR2.MRFBSYM" -length 0 

Drift -name "CR2.SEPTUM" -length 1 

Drift -name "CR2.DRFB6" -length 2.367622162 

Multipole -name "CR2.SRFB6" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 8.540776596*$e0] -e0 $e0 

Drift -name "CR2.INJECTION" -length 0 

Drift -name "CR2.D1CM" -length 0.01 

Drift -name "CR2.D1M" -length 1 

Drift -name "CR2.D1M" -length 1 

Drift -name "CR2.D1M" -length 1 

Drift -name "CR2.DRFB4A" -length 0.709135479 

Quadrupole -name "CR2.QRFB3" -synrad $quad_synrad -length 0.5 -strength [expr -0.3437279899*$e0] -e0 $e0 

Bpm -name "BPM613" -length 0
Drift -name "CR2.DRFB3" -length 3.771233433 

Quadrupole -name "CR2.QRFB2" -synrad $quad_synrad -length 0.5 -strength [expr 0.5775372505*$e0] -e0 $e0 

Bpm -name "BPM614" -length 0
Drift -name "CR2.DRFB2" -length 1.193240969 

Quadrupole -name "CR2.QRFB1" -synrad $quad_synrad -length 0.5 -strength [expr -1.08870911*$e0] -e0 $e0 

Bpm -name "BPM615" -length 0
Drift -name "CR2.DRFB1" -length 0.217192186 

Drift -name "CR2.DRRFH" -length 1 

Drift -name "CR2.RFKICKZERO" -length 0 

Drift -name "CR2.DRRFH" -length 1 

Drift -name "CR2.DL3IN" -length 0.4000389758 

Quadrupole -name "CR2.QL3IN" -synrad $quad_synrad -length 0.5 -strength [expr -0.2675265744*$e0] -e0 $e0 

Bpm -name "BPM616" -length 0
Drift -name "CR2.DL2IN" -length 1.103409345 

Quadrupole -name "CR2.QL2IN" -synrad $quad_synrad -length 0.5 -strength [expr 0.720003675*$e0] -e0 $e0 

Bpm -name "BPM617" -length 0
Drift -name "CR2.DL1IN" -length 0.4000469975 

Quadrupole -name "CR2.QL1IN" -synrad $quad_synrad -length 0.5 -strength [expr -0.642778543*$e0] -e0 $e0 

Bpm -name "BPM618" -length 0
Drift -name "CR2.DL0IN" -length 2.246504682 

Drift -name "CR2MDBACELLSTART" -length 0 

Drift -name "CR2.DBA.MCELL" -length 0 

Quadrupole -name "CR2.DBA.QI1" -synrad $quad_synrad -length 0.4 -strength [expr -0.5864293272*$e0] -e0 $e0 

Bpm -name "BPM619" -length 0
Drift -name "CR2.DBA.D11A" -length 0.1730305995 

Multipole -name "CR2.DBA.SNCH1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.D11B" -length 0.1628018956 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM620" -length 0
Drift -name "CR2.DBA.D12" -length 0.2697746031 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM621" -length 0
Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Drift -name "CR2.DBA.HBM" -length 0 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Multipole -name "CR2.DBA.SN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM622" -length 0
Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM623" -length 0
Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Multipole -name "CR2.DBA.SNN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM624" -length 0
Drift -name "CR2.DBA.HCM" -length 0 

Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.D12" -length 0.2697746031 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM625" -length 0
Drift -name "CR2.DBA.D11B1" -length 0.3424874764 

Multipole -name "CR2.DBA.SD0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.146568947*$e0] -e0 $e0 

Drift -name "CR2.DBA.DM0" -length 0.109313245 

Drift -name "CR2.DBA.HCM0" -length 0 

Quadrupole -name "CR2.DBA.QF1" -synrad $quad_synrad -length 0.4 -strength [expr -1.115855102*$e0] -e0 $e0 

Bpm -name "BPM626" -length 0
Drift -name "CR2.DBA.DM1" -length 1.973582219 

Quadrupole -name "CR2.DBA.QF2" -synrad $quad_synrad -length 0.4 -strength [expr 0.6764098228*$e0] -e0 $e0 

Bpm -name "BPM627" -length 0
Drift -name "CR2.DBA.DM2" -length 1.738896147 

Multipole -name "CR2.DBA.SD0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.146568947*$e0] -e0 $e0 

Drift -name "CR2.DBA.DMSEXT1" -length 0.109313245 

Quadrupole -name "CR2.DBA.QF3" -synrad $quad_synrad -length 0.4 -strength [expr -1.118935626*$e0] -e0 $e0 

Bpm -name "BPM628" -length 0
Drift -name "CR2.DBA.DM3" -length 0.2802607113 

Drift -name "CR2.DBA.DMSEXT1" -length 0.109313245 

Drift -name "CR2.DBA.D11B2" -length 0.1628018956 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM629" -length 0
Drift -name "CR2.DBA.D12" -length 0.2697746031 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM630" -length 0
Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Drift -name "CR2.DBA.HBM" -length 0 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Multipole -name "CR2.DBA.SN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM631" -length 0
Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM632" -length 0
Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Multipole -name "CR2.DBA.SNN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM633" -length 0
Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.D12" -length 0.2697746031 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM634" -length 0
Drift -name "CR2.DBA.D11B" -length 0.1628018956 

Multipole -name "CR2.DBA.SNCH1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.D11A" -length 0.1730305995 

Quadrupole -name "CR2.DBA.QI1" -synrad $quad_synrad -length 0.4 -strength [expr -0.5864293272*$e0] -e0 $e0 

Bpm -name "BPM635" -length 0
Drift -name "CR2.DBA.MCELL" -length 0 

Drift -name "CR2MDBACELLEND" -length 0 

Drift -name "CR2.DTR1A" -length 1.1 

Multipole -name "CR2.SXTR1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 12.54242064*$e0] -e0 $e0 

Drift -name "CR2.DTR1B" -length 0.2 

Quadrupole -name "CR2.QTR1" -synrad $quad_synrad -length 0.5 -strength [expr -0.371641026*$e0] -e0 $e0 

Bpm -name "BPM636" -length 0
Drift -name "CR2.DTR2A" -length 1.1 

Multipole -name "CR2.SXTR2" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -5.250757706*$e0] -e0 $e0 

Drift -name "CR2.DTR2B" -length 0.2 

Quadrupole -name "CR2.QTR2" -synrad $quad_synrad -length 0.5 -strength [expr 0.512918083*$e0] -e0 $e0 

Bpm -name "BPM637" -length 0
Drift -name "CR2.DTR3A" -length 1.5 

Multipole -name "CR2.SXTR3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -7.465446283*$e0] -e0 $e0 

Drift -name "CR2.DTR3B" -length 0.2 

Quadrupole -name "CR2.QTR3" -synrad $quad_synrad -length 0.5 -strength [expr -0.1592789427*$e0] -e0 $e0 

Bpm -name "BPM638" -length 0
Drift -name "CR2.DTR4A" -length 2.1328 

Multipole -name "CR2.SXTR4" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 20.76691673*$e0] -e0 $e0 

Drift -name "CR2.DTR4B" -length 0.2 

Quadrupole -name "CR2.QTR4" -synrad $quad_synrad -length 0.25 -strength [expr 0*$e0] -e0 $e0 

Bpm -name "BPM639" -length 0
Quadrupole -name "CR2.QTR4" -synrad $quad_synrad -length 0.25 -strength [expr 0*$e0] -e0 $e0 

Bpm -name "BPM640" -length 0
Drift -name "CR2.DTR4B" -length 0.2 

Multipole -name "CR2.SXTR4" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 20.76691673*$e0] -e0 $e0 

Drift -name "CR2.DTR4A" -length 2.1328 

Quadrupole -name "CR2.QTR3" -synrad $quad_synrad -length 0.5 -strength [expr -0.1592789427*$e0] -e0 $e0 

Bpm -name "BPM641" -length 0
Drift -name "CR2.DTR3B" -length 0.2 

Multipole -name "CR2.SXTR3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -7.465446283*$e0] -e0 $e0 

Drift -name "CR2.DTR3A" -length 1.5 

Quadrupole -name "CR2.QTR2" -synrad $quad_synrad -length 0.5 -strength [expr 0.512918083*$e0] -e0 $e0 

Bpm -name "BPM642" -length 0
Drift -name "CR2.DTR2B" -length 0.2 

Multipole -name "CR2.SXTR2" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -5.250757706*$e0] -e0 $e0 

Drift -name "CR2.DTR2A" -length 1.1 

Quadrupole -name "CR2.QTR1" -synrad $quad_synrad -length 0.5 -strength [expr -0.371641026*$e0] -e0 $e0 

Bpm -name "BPM643" -length 0
Drift -name "CR2.DTR1B" -length 0.2 

Multipole -name "CR2.SXTR1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 12.54242064*$e0] -e0 $e0 

Drift -name "CR2.DTR1A" -length 1.1 

Drift -name "CR2MDBACELLSTART" -length 0 

Drift -name "CR2.DBA.MCELL" -length 0 

Quadrupole -name "CR2.DBA.QI1" -synrad $quad_synrad -length 0.4 -strength [expr -0.5864293272*$e0] -e0 $e0 

Bpm -name "BPM644" -length 0
Drift -name "CR2.DBA.D11A" -length 0.1730305995 

Multipole -name "CR2.DBA.SNCH1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.D11B" -length 0.1628018956 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM645" -length 0
Drift -name "CR2.DBA.D12" -length 0.2697746031 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM646" -length 0
Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Drift -name "CR2.DBA.HBM" -length 0 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Multipole -name "CR2.DBA.SN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM647" -length 0
Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM648" -length 0
Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Multipole -name "CR2.DBA.SNN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM649" -length 0
Drift -name "CR2.DBA.HCM" -length 0 

Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.D12" -length 0.2697746031 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM650" -length 0
Drift -name "CR2.DBA.D11B1" -length 0.3424874764 

Multipole -name "CR2.DBA.SD0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.146568947*$e0] -e0 $e0 

Drift -name "CR2.DBA.DM0" -length 0.109313245 

Drift -name "CR2.DBA.HCM0" -length 0 

Quadrupole -name "CR2.DBA.QF1" -synrad $quad_synrad -length 0.4 -strength [expr -1.115855102*$e0] -e0 $e0 

Bpm -name "BPM651" -length 0
Drift -name "CR2.DBA.DM1" -length 1.973582219 

Quadrupole -name "CR2.DBA.QF2" -synrad $quad_synrad -length 0.4 -strength [expr 0.6764098228*$e0] -e0 $e0 

Bpm -name "BPM652" -length 0
Drift -name "CR2.DBA.DM2" -length 1.738896147 

Multipole -name "CR2.DBA.SD0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.146568947*$e0] -e0 $e0 

Drift -name "CR2.DBA.DMSEXT1" -length 0.109313245 

Quadrupole -name "CR2.DBA.QF3" -synrad $quad_synrad -length 0.4 -strength [expr -1.118935626*$e0] -e0 $e0 

Bpm -name "BPM653" -length 0
Drift -name "CR2.DBA.DM3" -length 0.2802607113 

Drift -name "CR2.DBA.DMSEXT1" -length 0.109313245 

Drift -name "CR2.DBA.D11B2" -length 0.1628018956 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM654" -length 0
Drift -name "CR2.DBA.D12" -length 0.2697746031 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM655" -length 0
Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Drift -name "CR2.DBA.HBM" -length 0 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Multipole -name "CR2.DBA.SN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM656" -length 0
Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM657" -length 0
Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Multipole -name "CR2.DBA.SNN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM658" -length 0
Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.D12" -length 0.2697746031 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM659" -length 0
Drift -name "CR2.DBA.D11B" -length 0.1628018956 

Multipole -name "CR2.DBA.SNCH1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.D11A" -length 0.1730305995 

Quadrupole -name "CR2.DBA.QI1" -synrad $quad_synrad -length 0.4 -strength [expr -0.5864293272*$e0] -e0 $e0 

Bpm -name "BPM660" -length 0
Drift -name "CR2.DBA.MCELL" -length 0 

Drift -name "CR2MDBACELLEND" -length 0 

Drift -name "CR2.DS1C_A" -length 2.179474654 

Multipole -name "CR2.SXS1C" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 11.05883366*$e0] -e0 $e0 

Drift -name "CR2.DS1C_B" -length 0.3 

Quadrupole -name "CR2.QS1C" -synrad $quad_synrad -length 0.5 -strength [expr -0.66814696*$e0] -e0 $e0 

Bpm -name "BPM661" -length 0
Drift -name "CR2.DS2C" -length 0.2003907272 

Quadrupole -name "CR2.QS2C" -synrad $quad_synrad -length 0.5 -strength [expr 0.7440336365*$e0] -e0 $e0 

Bpm -name "BPM662" -length 0
Drift -name "CR2.DS3C_A" -length 0.3 

Multipole -name "CR2.SXS3C" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -13.03004983*$e0] -e0 $e0 

Drift -name "CR2.DS3C_B" -length 3.777418619 

Quadrupole -name "CR2.QS3C" -synrad $quad_synrad -length 0.5 -strength [expr -0.03237714152*$e0] -e0 $e0 

Bpm -name "BPM663" -length 0
Drift -name "CR2.DS4C" -length 0.2407160001 

Drift -name "CR2.BC1" -length 0.4 

Drift -name "CR2.BC2" -length 0.4 

Drift -name "CR2.BC3" -length 0.4 

Drift -name "CR2.BC1" -length 0.4 

Drift -name "CR2.DS4C" -length 0.2407160001 

Quadrupole -name "CR2.QS3C" -synrad $quad_synrad -length 0.5 -strength [expr -0.03237714152*$e0] -e0 $e0 

Bpm -name "BPM664" -length 0
Drift -name "CR2.DS3C_B" -length 3.777418619 

Multipole -name "CR2.SXS3C" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -13.03004983*$e0] -e0 $e0 

Drift -name "CR2.DS3C_A" -length 0.3 

Quadrupole -name "CR2.QS2C" -synrad $quad_synrad -length 0.5 -strength [expr 0.7440336365*$e0] -e0 $e0 

Bpm -name "BPM665" -length 0
Drift -name "CR2.DS2C" -length 0.2003907272 

Quadrupole -name "CR2.QS1C" -synrad $quad_synrad -length 0.5 -strength [expr -0.66814696*$e0] -e0 $e0 

Bpm -name "BPM666" -length 0
Drift -name "CR2.DS1C_B" -length 0.3 

Multipole -name "CR2.SXS1C" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 11.05883366*$e0] -e0 $e0 

Drift -name "CR2.DS1C_A" -length 2.179474654 

Drift -name "CR2MDBACELLSTART" -length 0 

Drift -name "CR2.DBA.MCELL" -length 0 

Quadrupole -name "CR2.DBA.QI1" -synrad $quad_synrad -length 0.4 -strength [expr -0.5864293272*$e0] -e0 $e0 

Bpm -name "BPM667" -length 0
Drift -name "CR2.DBA.D11A" -length 0.1730305995 

Multipole -name "CR2.DBA.SNCH1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.D11B" -length 0.1628018956 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM668" -length 0
Drift -name "CR2.DBA.D12" -length 0.2697746031 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM669" -length 0
Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Drift -name "CR2.DBA.HBM" -length 0 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Multipole -name "CR2.DBA.SN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM670" -length 0
Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM671" -length 0
Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Multipole -name "CR2.DBA.SNN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM672" -length 0
Drift -name "CR2.DBA.HCM" -length 0 

Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.D12" -length 0.2697746031 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM673" -length 0
Drift -name "CR2.DBA.D11B1" -length 0.3424874764 

Multipole -name "CR2.DBA.SD0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.146568947*$e0] -e0 $e0 

Drift -name "CR2.DBA.DM0" -length 0.109313245 

Drift -name "CR2.DBA.HCM0" -length 0 

Quadrupole -name "CR2.DBA.QF1" -synrad $quad_synrad -length 0.4 -strength [expr -1.115855102*$e0] -e0 $e0 

Bpm -name "BPM674" -length 0
Drift -name "CR2.DBA.DM1" -length 1.973582219 

Quadrupole -name "CR2.DBA.QF2" -synrad $quad_synrad -length 0.4 -strength [expr 0.6764098228*$e0] -e0 $e0 

Bpm -name "BPM675" -length 0
Drift -name "CR2.DBA.DM2" -length 1.738896147 

Multipole -name "CR2.DBA.SD0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.146568947*$e0] -e0 $e0 

Drift -name "CR2.DBA.DMSEXT1" -length 0.109313245 

Quadrupole -name "CR2.DBA.QF3" -synrad $quad_synrad -length 0.4 -strength [expr -1.118935626*$e0] -e0 $e0 

Bpm -name "BPM676" -length 0
Drift -name "CR2.DBA.DM3" -length 0.2802607113 

Drift -name "CR2.DBA.DMSEXT1" -length 0.109313245 

Drift -name "CR2.DBA.D11B2" -length 0.1628018956 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM677" -length 0
Drift -name "CR2.DBA.D12" -length 0.2697746031 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM678" -length 0
Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Drift -name "CR2.DBA.HBM" -length 0 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Multipole -name "CR2.DBA.SN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM679" -length 0
Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM680" -length 0
Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Multipole -name "CR2.DBA.SNN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM681" -length 0
Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.D12" -length 0.2697746031 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM682" -length 0
Drift -name "CR2.DBA.D11B" -length 0.1628018956 

Multipole -name "CR2.DBA.SNCH1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.D11A" -length 0.1730305995 

Quadrupole -name "CR2.DBA.QI1" -synrad $quad_synrad -length 0.4 -strength [expr -0.5864293272*$e0] -e0 $e0 

Bpm -name "BPM683" -length 0
Drift -name "CR2.DBA.MCELL" -length 0 

Drift -name "CR2MDBACELLEND" -length 0 

Drift -name "CR2.DTR1A" -length 1.1 

Multipole -name "CR2.SXTR1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 12.54242064*$e0] -e0 $e0 

Drift -name "CR2.DTR1B" -length 0.2 

Quadrupole -name "CR2.QTR1" -synrad $quad_synrad -length 0.5 -strength [expr -0.371641026*$e0] -e0 $e0 

Bpm -name "BPM684" -length 0
Drift -name "CR2.DTR2A" -length 1.1 

Multipole -name "CR2.SXTR2" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -5.250757706*$e0] -e0 $e0 

Drift -name "CR2.DTR2B" -length 0.2 

Quadrupole -name "CR2.QTR2" -synrad $quad_synrad -length 0.5 -strength [expr 0.512918083*$e0] -e0 $e0 

Bpm -name "BPM685" -length 0
Drift -name "CR2.DTR3A" -length 1.5 

Multipole -name "CR2.SXTR3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -7.465446283*$e0] -e0 $e0 

Drift -name "CR2.DTR3B" -length 0.2 

Quadrupole -name "CR2.QTR3" -synrad $quad_synrad -length 0.5 -strength [expr -0.1592789427*$e0] -e0 $e0 

Bpm -name "BPM686" -length 0
Drift -name "CR2.DTR4A" -length 2.1328 

Multipole -name "CR2.SXTR4" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 20.76691673*$e0] -e0 $e0 

Drift -name "CR2.DTR4B" -length 0.2 

Quadrupole -name "CR2.QTR4" -synrad $quad_synrad -length 0.25 -strength [expr 0*$e0] -e0 $e0 

Bpm -name "BPM687" -length 0
Quadrupole -name "CR2.QTR4" -synrad $quad_synrad -length 0.25 -strength [expr 0*$e0] -e0 $e0 

Bpm -name "BPM688" -length 0
Drift -name "CR2.DTR4B" -length 0.2 

Multipole -name "CR2.SXTR4" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 20.76691673*$e0] -e0 $e0 

Drift -name "CR2.DTR4A" -length 2.1328 

Quadrupole -name "CR2.QTR3" -synrad $quad_synrad -length 0.5 -strength [expr -0.1592789427*$e0] -e0 $e0 

Bpm -name "BPM689" -length 0
Drift -name "CR2.DTR3B" -length 0.2 

Multipole -name "CR2.SXTR3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -7.465446283*$e0] -e0 $e0 

Drift -name "CR2.DTR3A" -length 1.5 

Quadrupole -name "CR2.QTR2" -synrad $quad_synrad -length 0.5 -strength [expr 0.512918083*$e0] -e0 $e0 

Bpm -name "BPM690" -length 0
Drift -name "CR2.DTR2B" -length 0.2 

Multipole -name "CR2.SXTR2" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -5.250757706*$e0] -e0 $e0 

Drift -name "CR2.DTR2A" -length 1.1 

Quadrupole -name "CR2.QTR1" -synrad $quad_synrad -length 0.5 -strength [expr -0.371641026*$e0] -e0 $e0 

Bpm -name "BPM691" -length 0
Drift -name "CR2.DTR1B" -length 0.2 

Multipole -name "CR2.SXTR1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 12.54242064*$e0] -e0 $e0 

Drift -name "CR2.DTR1A" -length 1.1 

Drift -name "CR2MDBACELLSTART" -length 0 

Drift -name "CR2.DBA.MCELL" -length 0 

Quadrupole -name "CR2.DBA.QI1" -synrad $quad_synrad -length 0.4 -strength [expr -0.5864293272*$e0] -e0 $e0 

Bpm -name "BPM692" -length 0
Drift -name "CR2.DBA.D11A" -length 0.1730305995 

Multipole -name "CR2.DBA.SNCH1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.D11B" -length 0.1628018956 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM693" -length 0
Drift -name "CR2.DBA.D12" -length 0.2697746031 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM694" -length 0
Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Drift -name "CR2.DBA.HBM" -length 0 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Multipole -name "CR2.DBA.SN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM695" -length 0
Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM696" -length 0
Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Multipole -name "CR2.DBA.SNN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM697" -length 0
Drift -name "CR2.DBA.HCM" -length 0 

Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.D12" -length 0.2697746031 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM698" -length 0
Drift -name "CR2.DBA.D11B1" -length 0.3424874764 

Multipole -name "CR2.DBA.SD0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.146568947*$e0] -e0 $e0 

Drift -name "CR2.DBA.DM0" -length 0.109313245 

Drift -name "CR2.DBA.HCM0" -length 0 

Quadrupole -name "CR2.DBA.QF1" -synrad $quad_synrad -length 0.4 -strength [expr -1.115855102*$e0] -e0 $e0 

Bpm -name "BPM699" -length 0
Drift -name "CR2.DBA.DM1" -length 1.973582219 

Quadrupole -name "CR2.DBA.QF2" -synrad $quad_synrad -length 0.4 -strength [expr 0.6764098228*$e0] -e0 $e0 

Bpm -name "BPM700" -length 0
Drift -name "CR2.DBA.DM2" -length 1.738896147 

Multipole -name "CR2.DBA.SD0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 1.146568947*$e0] -e0 $e0 

Drift -name "CR2.DBA.DMSEXT1" -length 0.109313245 

Quadrupole -name "CR2.DBA.QF3" -synrad $quad_synrad -length 0.4 -strength [expr -1.118935626*$e0] -e0 $e0 

Bpm -name "BPM701" -length 0
Drift -name "CR2.DBA.DM3" -length 0.2802607113 

Drift -name "CR2.DBA.DMSEXT1" -length 0.109313245 

Drift -name "CR2.DBA.D11B2" -length 0.1628018956 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM702" -length 0
Drift -name "CR2.DBA.D12" -length 0.2697746031 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM703" -length 0
Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Drift -name "CR2.DBA.HBM" -length 0 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Multipole -name "CR2.DBA.SN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM704" -length 0
Quadrupole -name "CR2.DBA.QS2C" -synrad $quad_synrad -length 0.2 -strength [expr -0.1179278091*$e0] -e0 $e0 

Bpm -name "BPM705" -length 0
Drift -name "CR2.DBA.DS3C_B" -length 0.2945377043 

Multipole -name "CR2.DBA.SNN3" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS3C_A" -length 0.01730305995 

Sbend -name "CR2.DBA.DIPN0" -synrad $sbend_synrad -length 0.2851351189 -angle 0.01452194454 -E1 0.00726097227 -E2 0.00726097227 -six_dim 1 -e0 $e0 -K [expr 0.7724967744*$e0] -fintx -1 

set e0 [expr $e0-14.1e-6*0.01452194454*0.01452194454/0.2851351189*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.DS2C_B" -length 0.01730305995 

Multipole -name "CR2.DBA.SF0" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr -1.074795249*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS2C_A" -length 0.4926115415 

Quadrupole -name "CR2.DBA.QS1C" -synrad $quad_synrad -length 0.4 -strength [expr -1.116186623*$e0] -e0 $e0 

Bpm -name "BPM706" -length 0
Drift -name "CR2.DBA.DS1C_B" -length 0.109313245 

Multipole -name "CR2.DBA.SN1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.DS1C_A" -length 0.6435098443 

Sbend -name "CR2.DBA.DIPOLE0" -synrad $sbend_synrad -length 2.182098945 -angle -0.2182098945 -E1 -0.1091049473 -E2 -0.1091049473 -six_dim 1 -e0 $e0 -K [expr 0.0399258644*$e0] -fint 0.5 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.2182098945*-0.2182098945/2.182098945*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "CR2.DBA.D12" -length 0.2697746031 

Quadrupole -name "CR2.DBA.QI2" -synrad $quad_synrad -length 0.4 -strength [expr 0.8145759856*$e0] -e0 $e0 

Bpm -name "BPM707" -length 0
Drift -name "CR2.DBA.D11B" -length 0.1628018956 

Multipole -name "CR2.DBA.SNCH1" -synrad $mult_synrad -type 3 -length 0.1 -strength [expr 0*$e0] -e0 $e0 

Drift -name "CR2.DBA.D11A" -length 0.1730305995 

Quadrupole -name "CR2.DBA.QI1" -synrad $quad_synrad -length 0.4 -strength [expr -0.5864293272*$e0] -e0 $e0 

Bpm -name "BPM708" -length 0
Drift -name "CR2.DBA.MCELL" -length 0 

Drift -name "CR2MDBACELLEND" -length 0 

Drift -name "CR2.DL0IN" -length 2.246504682 

Quadrupole -name "CR2.QL1IN" -synrad $quad_synrad -length 0.5 -strength [expr -0.642778543*$e0] -e0 $e0 

Bpm -name "BPM709" -length 0
Drift -name "CR2.DL1IN" -length 0.4000469975 

Quadrupole -name "CR2.QL2IN" -synrad $quad_synrad -length 0.5 -strength [expr 0.720003675*$e0] -e0 $e0 

Bpm -name "BPM710" -length 0
Drift -name "CR2.DL2IN" -length 1.103409345 

Quadrupole -name "CR2.QL3IN" -synrad $quad_synrad -length 0.5 -strength [expr -0.2675265744*$e0] -e0 $e0 

Bpm -name "BPM711" -length 0
Drift -name "CR2.DL3IN" -length 0.4000389758 

Drift -name "CR2.MENTEXTKCK" -length 0 

Drift -name "CR2.DRRFH" -length 1 

# WARNING: CORRECTOR needs to be defined, no PLACET element

CORRECTOR -name "CR2.KICKER" -length 0 -strength_x [expr -0.004*1e6*$e0] 

Drift -name "CR2.DRRFH" -length 1 

Drift -name "CR2.DRFB1" -length 0.217192186 

Quadrupole -name "CR2.QRFB1" -synrad $quad_synrad -length 0.5 -strength [expr -1.08870911*$e0] -e0 $e0 

Bpm -name "BPM712" -length 0
Drift -name "CR2.DRFB2" -length 1.193240969 

Quadrupole -name "CR2.QRFB2" -synrad $quad_synrad -length 0.5 -strength [expr 0.5775372505*$e0] -e0 $e0 

Bpm -name "BPM713" -length 0
Drift -name "CR2.DRFB3" -length 3.771233433 

Quadrupole -name "CR2.QRFB3" -synrad $quad_synrad -length 0.5 -strength [expr -0.3437279899*$e0] -e0 $e0 

Bpm -name "BPM714" -length 0
Drift -name "CR2.D1CM" -length 0.01 

Drift -name "CR2.D1M" -length 1 

Drift -name "CR2.D1M" -length 1 

Drift -name "CR2.D1M" -length 1 

Drift -name "CR2.DRFB4A" -length 0.709135479 

Bpm -name "BPM715" -length 0

Drift -name "CR2.EJECTION" -length 0 


Bpm -name "BPM0" -length 0

Sbend -name "TL3.BEND10" -synrad $sbend_synrad -length 2.01146240647403 -angle 0.2617993878 -E1 0.1308996939 -E2 0.1308996939 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*0.2617993878*0.2617993878/2.01146240647403*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TL3.D122" -length 1.9174 

Multipole -name "TL3.SX125" -synrad $mult_synrad -type 3 -length 0.3 -strength [expr 0.816579*$e0] -e0 $e0 

Drift -name "TL3.D128" -length 0.8837 

Quadrupole -name "TL3.Q13" -synrad $quad_synrad -length 0.75 -strength [expr 0.4241595*$e0] -e0 $e0 

Bpm -name "BPM4" -length 0
Drift -name "TL3.D142" -length 0.042885 

Multipole -name "TL3.SX145" -synrad $mult_synrad -type 3 -length 0.3 -strength [expr 0.1000155*$e0] -e0 $e0 

Drift -name "TL3.D148" -length 0.042885 

Quadrupole -name "TL3.Q15" -synrad $quad_synrad -length 0.75 -strength [expr -0.713214*$e0] -e0 $e0 

Bpm -name "BPM5" -length 0
Drift -name "TL3.D162" -length 0.0976775 

Drift -name "TL3.D168" -length 0.0976775 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 0.5000086579

Sbend -name "TL3.BEND17" -synrad $sbend_synrad -length 0.500017315941417 -angle -0.02038562618 -E1 -0.01019281309 -E2 -0.01019281309 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.02038562618*-0.02038562618/0.500017315941417*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TL3.D182" -length 0.0001175 

Multipole -name "TL3.SX185" -synrad $mult_synrad -type 3 -length 0.3 -strength [expr 0.1967463*$e0] -e0 $e0 

Drift -name "TL3.D188" -length 0.0001175 

Quadrupole -name "TL3.Q20A" -synrad $quad_synrad -length 0.375 -strength [expr 0.279207*$e0] -e0 $e0 

Bpm -name "BPM6" -length 0
Drift -name "TL3.MIDCELLONE" -length 0 

Quadrupole -name "TL3.Q20B" -synrad $quad_synrad -length 0.375 -strength [expr 0.279207*$e0] -e0 $e0 

Bpm -name "BPM7" -length 0
Drift -name "TL3.D222" -length 0.1905674726 

Multipole -name "TL3.SX225" -synrad $mult_synrad -type 3 -length 0.3 -strength [expr 0.3831*$e0] -e0 $e0 

Drift -name "TL3.D228" -length 0.1905674726 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 0.5000086579

Sbend -name "TL3.BEND23" -synrad $sbend_synrad -length 0.500017315941417 -angle -0.02038562618 -E1 -0.01019281309 -E2 -0.01019281309 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.02038562618*-0.02038562618/0.500017315941417*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TL3.D242" -length 0.05000000025 

Drift -name "TL3.D248" -length 0.05000000025 

Quadrupole -name "TL3.Q25" -synrad $quad_synrad -length 0.75 -strength [expr -0.6218761651*$e0] -e0 $e0 

Bpm -name "BPM8" -length 0
Drift -name "TL3.D262" -length 0.2203857562 

Multipole -name "TL3.SX265" -synrad $mult_synrad -type 3 -length 0.3 -strength [expr 0.335442*$e0] -e0 $e0 

Drift -name "TL3.D268" -length 0.2203857562 

Quadrupole -name "TL3.Q27" -synrad $quad_synrad -length 0.75 -strength [expr 0.4130955375*$e0] -e0 $e0 

Bpm -name "BPM9" -length 0
Drift -name "TL3.D282" -length 0.7273957003 

Multipole -name "TL3.SX285" -synrad $mult_synrad -type 3 -length 0.3 -strength [expr 1.366614*$e0] -e0 $e0 

Drift -name "TL3.D288" -length 1.604791401 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.005723015

Sbend -name "TL3.BEND30" -synrad $sbend_synrad -length 2.01146240647403 -angle 0.2617993878 -E1 0.1308996939 -E2 0.1308996939 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*0.2617993878*0.2617993878/2.01146240647403*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TL3.EJECTION" -length 0 

Drift -name "TL3.OMSEND" -length 0 

Drift -name "TL3.D71" -length 0.5 

Quadrupole -name "TL3.Q72" -synrad $quad_synrad -length 0.75 -strength [expr -0.45951075*$e0] -e0 $e0 

Bpm -name "BPM10" -length 0
Drift -name "TL3.D73" -length 0.5 

Quadrupole -name "TL3.Q74" -synrad $quad_synrad -length 0.75 -strength [expr 0.70000125*$e0] -e0 $e0 

Bpm -name "BPM11" -length 0
Drift -name "TL3.D75" -length 2.19951 

Quadrupole -name "TL3.Q76" -synrad $quad_synrad -length 0.75 -strength [expr -0.7355895*$e0] -e0 $e0 

Bpm -name "BPM12" -length 0
Drift -name "TL3.D77" -length 0.5 

Quadrupole -name "TL3.Q78" -synrad $quad_synrad -length 0.75 -strength [expr 1.0130475*$e0] -e0 $e0 

Bpm -name "BPM13" -length 0
Drift -name "TL3.D79" -length 0.150751 
Bpm -name "BPMTTA" -length 0
Drift -name "TTA.D2M" -length 2 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.D1A" -length 2.389121542 

Multipole -name "TTA.SX1" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D1B" -length 0.3 

Quadrupole -name "TTA.Q1" -synrad $quad_synrad -length 0.75 -strength [expr 0.6863003356*$e0] -e0 $e0 

Bpm -name "BPM14" -length 0
Drift -name "TTA.D2" -length 0.5056938963 

Quadrupole -name "TTA.Q2" -synrad $quad_synrad -length 0.75 -strength [expr -0.6004214291*$e0] -e0 $e0 

Bpm -name "BPM15" -length 0
Drift -name "TTA.D3A" -length 0.3 

Multipole -name "TTA.SX2" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D3B" -length 3.077193097 

Drift -name "TTA.D3C" -length 0.1 

Quadrupole -name "TTA.Q3" -synrad $quad_synrad -length 0.75 -strength [expr 0.3042074493*$e0] -e0 $e0 

Bpm -name "BPM16" -length 0
Drift -name "TTA.D4A" -length 0.5779914647 

Multipole -name "TTA.SX3" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D4B" -length 0.3 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.D4B" -length 0.3 

Multipole -name "TTA.SX3" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D4A" -length 0.5779914647 

Quadrupole -name "TTA.Q3" -synrad $quad_synrad -length 0.75 -strength [expr 0.3042074493*$e0] -e0 $e0 

Bpm -name "BPM17" -length 0
Drift -name "TTA.D3C" -length 0.1 

Drift -name "TTA.D3B" -length 3.077193097 

Multipole -name "TTA.SX2" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D3A" -length 0.3 

Quadrupole -name "TTA.Q2" -synrad $quad_synrad -length 0.75 -strength [expr -0.6004214291*$e0] -e0 $e0 

Bpm -name "BPM18" -length 0
Drift -name "TTA.D2" -length 0.5056938963 

Quadrupole -name "TTA.Q1" -synrad $quad_synrad -length 0.75 -strength [expr 0.6863003356*$e0] -e0 $e0 

Bpm -name "BPM19" -length 0
Drift -name "TTA.D1B" -length 0.3 

Multipole -name "TTA.SX1" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D1A" -length 2.389121542 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.MBEND3" -length 0 

Drift -name "TTA.D2M" -length 2 

Drift -name "TTA.EXICELL" -length 0 

Drift -name "TTA.D2M" -length 2 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.D1A" -length 2.389121542 

Multipole -name "TTA.SX1" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D1B" -length 0.3 

Quadrupole -name "TTA.Q1" -synrad $quad_synrad -length 0.75 -strength [expr 0.6863003356*$e0] -e0 $e0 

Bpm -name "BPM20" -length 0
Drift -name "TTA.D2" -length 0.5056938963 

Quadrupole -name "TTA.Q2" -synrad $quad_synrad -length 0.75 -strength [expr -0.6004214291*$e0] -e0 $e0 

Bpm -name "BPM21" -length 0
Drift -name "TTA.D3A" -length 0.3 

Multipole -name "TTA.SX2" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D3B" -length 3.077193097 

Drift -name "TTA.D3C" -length 0.1 

Quadrupole -name "TTA.Q3" -synrad $quad_synrad -length 0.75 -strength [expr 0.3042074493*$e0] -e0 $e0 

Bpm -name "BPM22" -length 0
Drift -name "TTA.D4A" -length 0.5779914647 

Multipole -name "TTA.SX3" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D4B" -length 0.3 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.D4B" -length 0.3 

Multipole -name "TTA.SX3" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D4A" -length 0.5779914647 

Quadrupole -name "TTA.Q3" -synrad $quad_synrad -length 0.75 -strength [expr 0.3042074493*$e0] -e0 $e0 

Bpm -name "BPM23" -length 0
Drift -name "TTA.D3C" -length 0.1 

Drift -name "TTA.D3B" -length 3.077193097 

Multipole -name "TTA.SX2" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D3A" -length 0.3 

Quadrupole -name "TTA.Q2" -synrad $quad_synrad -length 0.75 -strength [expr -0.6004214291*$e0] -e0 $e0 

Bpm -name "BPM24" -length 0
Drift -name "TTA.D2" -length 0.5056938963 

Quadrupole -name "TTA.Q1" -synrad $quad_synrad -length 0.75 -strength [expr 0.6863003356*$e0] -e0 $e0 

Bpm -name "BPM25" -length 0
Drift -name "TTA.D1B" -length 0.3 

Multipole -name "TTA.SX1" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D1A" -length 2.389121542 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.MBEND3" -length 0 

Drift -name "TTA.D2M" -length 2 

Drift -name "TTA.EXICELL" -length 0 

Drift -name "TTA.D2M" -length 2 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.D1A" -length 2.389121542 

Multipole -name "TTA.SX1" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D1B" -length 0.3 

Quadrupole -name "TTA.Q1" -synrad $quad_synrad -length 0.75 -strength [expr 0.6863003356*$e0] -e0 $e0 

Bpm -name "BPM26" -length 0
Drift -name "TTA.D2" -length 0.5056938963 

Quadrupole -name "TTA.Q2" -synrad $quad_synrad -length 0.75 -strength [expr -0.6004214291*$e0] -e0 $e0 

Bpm -name "BPM27" -length 0
Drift -name "TTA.D3A" -length 0.3 

Multipole -name "TTA.SX2" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D3B" -length 3.077193097 

Drift -name "TTA.D3C" -length 0.1 

Quadrupole -name "TTA.Q3" -synrad $quad_synrad -length 0.75 -strength [expr 0.3042074493*$e0] -e0 $e0 

Bpm -name "BPM28" -length 0
Drift -name "TTA.D4A" -length 0.5779914647 

Multipole -name "TTA.SX3" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D4B" -length 0.3 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.D4B" -length 0.3 

Multipole -name "TTA.SX3" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D4A" -length 0.5779914647 

Quadrupole -name "TTA.Q3" -synrad $quad_synrad -length 0.75 -strength [expr 0.3042074493*$e0] -e0 $e0 

Bpm -name "BPM29" -length 0
Drift -name "TTA.D3C" -length 0.1 

Drift -name "TTA.D3B" -length 3.077193097 

Multipole -name "TTA.SX2" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D3A" -length 0.3 

Quadrupole -name "TTA.Q2" -synrad $quad_synrad -length 0.75 -strength [expr -0.6004214291*$e0] -e0 $e0 

Bpm -name "BPM30" -length 0
Drift -name "TTA.D2" -length 0.5056938963 

Quadrupole -name "TTA.Q1" -synrad $quad_synrad -length 0.75 -strength [expr 0.6863003356*$e0] -e0 $e0 

Bpm -name "BPM31" -length 0
Drift -name "TTA.D1B" -length 0.3 

Multipole -name "TTA.SX1" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D1A" -length 2.389121542 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.MBEND3" -length 0 

Drift -name "TTA.D2M" -length 2 

Drift -name "TTA.EXICELL" -length 0 

Drift -name "TTA.D2M" -length 2 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.D1A" -length 2.389121542 

Multipole -name "TTA.SX1" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D1B" -length 0.3 

Quadrupole -name "TTA.Q1" -synrad $quad_synrad -length 0.75 -strength [expr 0.6863003356*$e0] -e0 $e0 

Bpm -name "BPM32" -length 0
Drift -name "TTA.D2" -length 0.5056938963 

Quadrupole -name "TTA.Q2" -synrad $quad_synrad -length 0.75 -strength [expr -0.6004214291*$e0] -e0 $e0 

Bpm -name "BPM33" -length 0
Drift -name "TTA.D3A" -length 0.3 

Multipole -name "TTA.SX2" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D3B" -length 3.077193097 

Drift -name "TTA.D3C" -length 0.1 

Quadrupole -name "TTA.Q3" -synrad $quad_synrad -length 0.75 -strength [expr 0.3042074493*$e0] -e0 $e0 

Bpm -name "BPM34" -length 0
Drift -name "TTA.D4A" -length 0.5779914647 

Multipole -name "TTA.SX3" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D4B" -length 0.3 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.D4B" -length 0.3 

Multipole -name "TTA.SX3" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D4A" -length 0.5779914647 

Quadrupole -name "TTA.Q3" -synrad $quad_synrad -length 0.75 -strength [expr 0.3042074493*$e0] -e0 $e0 

Bpm -name "BPM35" -length 0
Drift -name "TTA.D3C" -length 0.1 

Drift -name "TTA.D3B" -length 3.077193097 

Multipole -name "TTA.SX2" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D3A" -length 0.3 

Quadrupole -name "TTA.Q2" -synrad $quad_synrad -length 0.75 -strength [expr -0.6004214291*$e0] -e0 $e0 

Bpm -name "BPM36" -length 0
Drift -name "TTA.D2" -length 0.5056938963 

Quadrupole -name "TTA.Q1" -synrad $quad_synrad -length 0.75 -strength [expr 0.6863003356*$e0] -e0 $e0 

Bpm -name "BPM37" -length 0
Drift -name "TTA.D1B" -length 0.3 

Multipole -name "TTA.SX1" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D1A" -length 2.389121542 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.MBEND3" -length 0 

Drift -name "TTA.D2M" -length 2 

Drift -name "TTA.EXICELL" -length 0 

Drift -name "TTA.D2M" -length 2 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.D1A" -length 2.389121542 

Multipole -name "TTA.SX1" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D1B" -length 0.3 

Quadrupole -name "TTA.Q1" -synrad $quad_synrad -length 0.75 -strength [expr 0.6863003356*$e0] -e0 $e0 

Bpm -name "BPM38" -length 0
Drift -name "TTA.D2" -length 0.5056938963 

Quadrupole -name "TTA.Q2" -synrad $quad_synrad -length 0.75 -strength [expr -0.6004214291*$e0] -e0 $e0 

Bpm -name "BPM39" -length 0
Drift -name "TTA.D3A" -length 0.3 

Multipole -name "TTA.SX2" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D3B" -length 3.077193097 

Drift -name "TTA.D3C" -length 0.1 

Quadrupole -name "TTA.Q3" -synrad $quad_synrad -length 0.75 -strength [expr 0.3042074493*$e0] -e0 $e0 

Bpm -name "BPM40" -length 0
Drift -name "TTA.D4A" -length 0.5779914647 

Multipole -name "TTA.SX3" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D4B" -length 0.3 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.D4B" -length 0.3 

Multipole -name "TTA.SX3" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D4A" -length 0.5779914647 

Quadrupole -name "TTA.Q3" -synrad $quad_synrad -length 0.75 -strength [expr 0.3042074493*$e0] -e0 $e0 

Bpm -name "BPM41" -length 0
Drift -name "TTA.D3C" -length 0.1 

Drift -name "TTA.D3B" -length 3.077193097 

Multipole -name "TTA.SX2" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D3A" -length 0.3 

Quadrupole -name "TTA.Q2" -synrad $quad_synrad -length 0.75 -strength [expr -0.6004214291*$e0] -e0 $e0 

Bpm -name "BPM42" -length 0
Drift -name "TTA.D2" -length 0.5056938963 

Quadrupole -name "TTA.Q1" -synrad $quad_synrad -length 0.75 -strength [expr 0.6863003356*$e0] -e0 $e0 

Bpm -name "BPM43" -length 0
Drift -name "TTA.D1B" -length 0.3 

Multipole -name "TTA.SX1" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D1A" -length 2.389121542 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.MBEND3" -length 0 

Drift -name "TTA.D2M" -length 2 

Drift -name "TTA.EXICELL" -length 0 

Drift -name "TTA.D2M" -length 2 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.D1A" -length 2.389121542 

Multipole -name "TTA.SX1" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D1B" -length 0.3 

Quadrupole -name "TTA.Q1" -synrad $quad_synrad -length 0.75 -strength [expr 0.6863003356*$e0] -e0 $e0 

Bpm -name "BPM44" -length 0
Drift -name "TTA.D2" -length 0.5056938963 

Quadrupole -name "TTA.Q2" -synrad $quad_synrad -length 0.75 -strength [expr -0.6004214291*$e0] -e0 $e0 

Bpm -name "BPM45" -length 0
Drift -name "TTA.D3A" -length 0.3 

Multipole -name "TTA.SX2" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D3B" -length 3.077193097 

Drift -name "TTA.D3C" -length 0.1 

Quadrupole -name "TTA.Q3" -synrad $quad_synrad -length 0.75 -strength [expr 0.3042074493*$e0] -e0 $e0 

Bpm -name "BPM46" -length 0
Drift -name "TTA.D4A" -length 0.5779914647 

Multipole -name "TTA.SX3" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D4B" -length 0.3 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.D4B" -length 0.3 

Multipole -name "TTA.SX3" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D4A" -length 0.5779914647 

Quadrupole -name "TTA.Q3" -synrad $quad_synrad -length 0.75 -strength [expr 0.3042074493*$e0] -e0 $e0 

Bpm -name "BPM47" -length 0
Drift -name "TTA.D3C" -length 0.1 

Drift -name "TTA.D3B" -length 3.077193097 

Multipole -name "TTA.SX2" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D3A" -length 0.3 

Quadrupole -name "TTA.Q2" -synrad $quad_synrad -length 0.75 -strength [expr -0.6004214291*$e0] -e0 $e0 

Bpm -name "BPM48" -length 0
Drift -name "TTA.D2" -length 0.5056938963 

Quadrupole -name "TTA.Q1" -synrad $quad_synrad -length 0.75 -strength [expr 0.6863003356*$e0] -e0 $e0 

Bpm -name "BPM49" -length 0
Drift -name "TTA.D1B" -length 0.3 

Multipole -name "TTA.SX1" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D1A" -length 2.389121542 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.MBEND3" -length 0 

Drift -name "TTA.D2M" -length 2 

Drift -name "TTA.EXICELL" -length 0 

Drift -name "TTA.D2M" -length 2 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.D1A" -length 2.389121542 

Multipole -name "TTA.SX1" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D1B" -length 0.3 

Quadrupole -name "TTA.Q1" -synrad $quad_synrad -length 0.75 -strength [expr 0.6863003356*$e0] -e0 $e0 

Bpm -name "BPM50" -length 0
Drift -name "TTA.D2" -length 0.5056938963 

Quadrupole -name "TTA.Q2" -synrad $quad_synrad -length 0.75 -strength [expr -0.6004214291*$e0] -e0 $e0 

Bpm -name "BPM51" -length 0
Drift -name "TTA.D3A" -length 0.3 

Multipole -name "TTA.SX2" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D3B" -length 3.077193097 

Drift -name "TTA.D3C" -length 0.1 

Quadrupole -name "TTA.Q3" -synrad $quad_synrad -length 0.75 -strength [expr 0.3042074493*$e0] -e0 $e0 

Bpm -name "BPM52" -length 0
Drift -name "TTA.D4A" -length 0.5779914647 

Multipole -name "TTA.SX3" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D4B" -length 0.3 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.D4B" -length 0.3 

Multipole -name "TTA.SX3" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D4A" -length 0.5779914647 

Quadrupole -name "TTA.Q3" -synrad $quad_synrad -length 0.75 -strength [expr 0.3042074493*$e0] -e0 $e0 

Bpm -name "BPM53" -length 0
Drift -name "TTA.D3C" -length 0.1 

Drift -name "TTA.D3B" -length 3.077193097 

Multipole -name "TTA.SX2" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D3A" -length 0.3 

Quadrupole -name "TTA.Q2" -synrad $quad_synrad -length 0.75 -strength [expr -0.6004214291*$e0] -e0 $e0 

Bpm -name "BPM54" -length 0
Drift -name "TTA.D2" -length 0.5056938963 

Quadrupole -name "TTA.Q1" -synrad $quad_synrad -length 0.75 -strength [expr 0.6863003356*$e0] -e0 $e0 

Bpm -name "BPM55" -length 0
Drift -name "TTA.D1B" -length 0.3 

Multipole -name "TTA.SX1" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D1A" -length 2.389121542 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.MBEND3" -length 0 

Drift -name "TTA.D2M" -length 2 

Drift -name "TTA.EXICELL" -length 0 

Drift -name "TTA.D2M" -length 2 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.D1A" -length 2.389121542 

Multipole -name "TTA.SX1" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D1B" -length 0.3 

Quadrupole -name "TTA.Q1" -synrad $quad_synrad -length 0.75 -strength [expr 0.6863003356*$e0] -e0 $e0 

Bpm -name "BPM56" -length 0
Drift -name "TTA.D2" -length 0.5056938963 

Quadrupole -name "TTA.Q2" -synrad $quad_synrad -length 0.75 -strength [expr -0.6004214291*$e0] -e0 $e0 

Bpm -name "BPM57" -length 0
Drift -name "TTA.D3A" -length 0.3 

Multipole -name "TTA.SX2" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D3B" -length 3.077193097 

Drift -name "TTA.D3C" -length 0.1 

Quadrupole -name "TTA.Q3" -synrad $quad_synrad -length 0.75 -strength [expr 0.3042074493*$e0] -e0 $e0 

Bpm -name "BPM58" -length 0
Drift -name "TTA.D4A" -length 0.5779914647 

Multipole -name "TTA.SX3" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D4B" -length 0.3 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.D4B" -length 0.3 

Multipole -name "TTA.SX3" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D4A" -length 0.5779914647 

Quadrupole -name "TTA.Q3" -synrad $quad_synrad -length 0.75 -strength [expr 0.3042074493*$e0] -e0 $e0 

Bpm -name "BPM59" -length 0
Drift -name "TTA.D3C" -length 0.1 

Drift -name "TTA.D3B" -length 3.077193097 

Multipole -name "TTA.SX2" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D3A" -length 0.3 

Quadrupole -name "TTA.Q2" -synrad $quad_synrad -length 0.75 -strength [expr -0.6004214291*$e0] -e0 $e0 

Bpm -name "BPM60" -length 0
Drift -name "TTA.D2" -length 0.5056938963 

Quadrupole -name "TTA.Q1" -synrad $quad_synrad -length 0.75 -strength [expr 0.6863003356*$e0] -e0 $e0 

Bpm -name "BPM61" -length 0
Drift -name "TTA.D1B" -length 0.3 

Multipole -name "TTA.SX1" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D1A" -length 2.389121542 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.MBEND3" -length 0 

Drift -name "TTA.D2M" -length 2 

Drift -name "TTA.EXICELL" -length 0 

Drift -name "TTA.D2M" -length 2 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.D1A" -length 2.389121542 

Multipole -name "TTA.SX1" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D1B" -length 0.3 

Quadrupole -name "TTA.Q1" -synrad $quad_synrad -length 0.75 -strength [expr 0.6863003356*$e0] -e0 $e0 

Bpm -name "BPM62" -length 0
Drift -name "TTA.D2" -length 0.5056938963 

Quadrupole -name "TTA.Q2" -synrad $quad_synrad -length 0.75 -strength [expr -0.6004214291*$e0] -e0 $e0 

Bpm -name "BPM63" -length 0
Drift -name "TTA.D3A" -length 0.3 

Multipole -name "TTA.SX2" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D3B" -length 3.077193097 

Drift -name "TTA.D3C" -length 0.1 

Quadrupole -name "TTA.Q3" -synrad $quad_synrad -length 0.75 -strength [expr 0.3042074493*$e0] -e0 $e0 

Bpm -name "BPM64" -length 0
Drift -name "TTA.D4A" -length 0.5779914647 

Multipole -name "TTA.SX3" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D4B" -length 0.3 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.D4B" -length 0.3 

Multipole -name "TTA.SX3" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D4A" -length 0.5779914647 

Quadrupole -name "TTA.Q3" -synrad $quad_synrad -length 0.75 -strength [expr 0.3042074493*$e0] -e0 $e0 

Bpm -name "BPM65" -length 0
Drift -name "TTA.D3C" -length 0.1 

Drift -name "TTA.D3B" -length 3.077193097 

Multipole -name "TTA.SX2" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D3A" -length 0.3 

Quadrupole -name "TTA.Q2" -synrad $quad_synrad -length 0.75 -strength [expr -0.6004214291*$e0] -e0 $e0 

Bpm -name "BPM66" -length 0
Drift -name "TTA.D2" -length 0.5056938963 

Quadrupole -name "TTA.Q1" -synrad $quad_synrad -length 0.75 -strength [expr 0.6863003356*$e0] -e0 $e0 

Bpm -name "BPM67" -length 0
Drift -name "TTA.D1B" -length 0.3 

Multipole -name "TTA.SX1" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D1A" -length 2.389121542 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.MBEND3" -length 0 

Drift -name "TTA.D2M" -length 2 

Drift -name "TTA.EXICELL" -length 0 

Drift -name "TTA.D2M" -length 2 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.D1A" -length 2.389121542 

Multipole -name "TTA.SX1" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D1B" -length 0.3 

Quadrupole -name "TTA.Q1" -synrad $quad_synrad -length 0.75 -strength [expr 0.6863003356*$e0] -e0 $e0 

Bpm -name "BPM68" -length 0
Drift -name "TTA.D2" -length 0.5056938963 

Quadrupole -name "TTA.Q2" -synrad $quad_synrad -length 0.75 -strength [expr -0.6004214291*$e0] -e0 $e0 

Bpm -name "BPM69" -length 0
Drift -name "TTA.D3A" -length 0.3 

Multipole -name "TTA.SX2" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D3B" -length 3.077193097 

Drift -name "TTA.D3C" -length 0.1 

Quadrupole -name "TTA.Q3" -synrad $quad_synrad -length 0.75 -strength [expr 0.3042074493*$e0] -e0 $e0 

Bpm -name "BPM70" -length 0
Drift -name "TTA.D4A" -length 0.5779914647 

Multipole -name "TTA.SX3" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D4B" -length 0.3 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.D4B" -length 0.3 

Multipole -name "TTA.SX3" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D4A" -length 0.5779914647 

Quadrupole -name "TTA.Q3" -synrad $quad_synrad -length 0.75 -strength [expr 0.3042074493*$e0] -e0 $e0 

Bpm -name "BPM71" -length 0
Drift -name "TTA.D3C" -length 0.1 

Drift -name "TTA.D3B" -length 3.077193097 

Multipole -name "TTA.SX2" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D3A" -length 0.3 

Quadrupole -name "TTA.Q2" -synrad $quad_synrad -length 0.75 -strength [expr -0.6004214291*$e0] -e0 $e0 

Bpm -name "BPM72" -length 0
Drift -name "TTA.D2" -length 0.5056938963 

Quadrupole -name "TTA.Q1" -synrad $quad_synrad -length 0.75 -strength [expr 0.6863003356*$e0] -e0 $e0 

Bpm -name "BPM73" -length 0
Drift -name "TTA.D1B" -length 0.3 

Multipole -name "TTA.SX1" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D1A" -length 2.389121542 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.MBEND3" -length 0 

Drift -name "TTA.D2M" -length 2 

Drift -name "TTA.EXICELL" -length 0 

Drift -name "TTA.D2M" -length 2 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.D1A" -length 2.389121542 

Multipole -name "TTA.SX1" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D1B" -length 0.3 

Quadrupole -name "TTA.Q1" -synrad $quad_synrad -length 0.75 -strength [expr 0.6863003356*$e0] -e0 $e0 

Bpm -name "BPM74" -length 0
Drift -name "TTA.D2" -length 0.5056938963 

Quadrupole -name "TTA.Q2" -synrad $quad_synrad -length 0.75 -strength [expr -0.6004214291*$e0] -e0 $e0 

Bpm -name "BPM75" -length 0
Drift -name "TTA.D3A" -length 0.3 

Multipole -name "TTA.SX2" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D3B" -length 3.077193097 

Drift -name "TTA.D3C" -length 0.1 

Quadrupole -name "TTA.Q3" -synrad $quad_synrad -length 0.75 -strength [expr 0.3042074493*$e0] -e0 $e0 

Bpm -name "BPM76" -length 0
Drift -name "TTA.D4A" -length 0.5779914647 

Multipole -name "TTA.SX3" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D4B" -length 0.3 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.D4B" -length 0.3 

Multipole -name "TTA.SX3" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D4A" -length 0.5779914647 

Quadrupole -name "TTA.Q3" -synrad $quad_synrad -length 0.75 -strength [expr 0.3042074493*$e0] -e0 $e0 

Bpm -name "BPM77" -length 0
Drift -name "TTA.D3C" -length 0.1 

Drift -name "TTA.D3B" -length 3.077193097 

Multipole -name "TTA.SX2" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D3A" -length 0.3 

Quadrupole -name "TTA.Q2" -synrad $quad_synrad -length 0.75 -strength [expr -0.6004214291*$e0] -e0 $e0 

Bpm -name "BPM78" -length 0
Drift -name "TTA.D2" -length 0.5056938963 

Quadrupole -name "TTA.Q1" -synrad $quad_synrad -length 0.75 -strength [expr 0.6863003356*$e0] -e0 $e0 

Bpm -name "BPM79" -length 0
Drift -name "TTA.D1B" -length 0.3 

Multipole -name "TTA.SX1" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D1A" -length 2.389121542 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.MBEND3" -length 0 

Drift -name "TTA.D2M" -length 2 

Drift -name "TTA.EXICELL" -length 0 

Drift -name "TTA.D2M" -length 2 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.D1A" -length 2.389121542 

Multipole -name "TTA.SX1" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D1B" -length 0.3 

Quadrupole -name "TTA.Q1" -synrad $quad_synrad -length 0.75 -strength [expr 0.6863003356*$e0] -e0 $e0 

Bpm -name "BPM80" -length 0
Drift -name "TTA.D2" -length 0.5056938963 

Quadrupole -name "TTA.Q2" -synrad $quad_synrad -length 0.75 -strength [expr -0.6004214291*$e0] -e0 $e0 

Bpm -name "BPM81" -length 0
Drift -name "TTA.D3A" -length 0.3 

Multipole -name "TTA.SX2" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D3B" -length 3.077193097 

Drift -name "TTA.D3C" -length 0.1 

Quadrupole -name "TTA.Q3" -synrad $quad_synrad -length 0.75 -strength [expr 0.3042074493*$e0] -e0 $e0 

Bpm -name "BPM82" -length 0
Drift -name "TTA.D4A" -length 0.5779914647 

Multipole -name "TTA.SX3" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D4B" -length 0.3 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.D4B" -length 0.3 

Multipole -name "TTA.SX3" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D4A" -length 0.5779914647 

Quadrupole -name "TTA.Q3" -synrad $quad_synrad -length 0.75 -strength [expr 0.3042074493*$e0] -e0 $e0 

Bpm -name "BPM83" -length 0
Drift -name "TTA.D3C" -length 0.1 

Drift -name "TTA.D3B" -length 3.077193097 

Multipole -name "TTA.SX2" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D3A" -length 0.3 

Quadrupole -name "TTA.Q2" -synrad $quad_synrad -length 0.75 -strength [expr -0.6004214291*$e0] -e0 $e0 

Bpm -name "BPM84" -length 0
Drift -name "TTA.D2" -length 0.5056938963 

Quadrupole -name "TTA.Q1" -synrad $quad_synrad -length 0.75 -strength [expr 0.6863003356*$e0] -e0 $e0 

Bpm -name "BPM85" -length 0
Drift -name "TTA.D1B" -length 0.3 

Multipole -name "TTA.SX1" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D1A" -length 2.389121542 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.MBEND3" -length 0 

Drift -name "TTA.D2M" -length 2 

Drift -name "TTA.EXICELL" -length 0 

Drift -name "TTA.D2M" -length 2 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.D1A" -length 2.389121542 

Multipole -name "TTA.SX1" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D1B" -length 0.3 

Quadrupole -name "TTA.Q1" -synrad $quad_synrad -length 0.75 -strength [expr 0.6863003356*$e0] -e0 $e0 

Bpm -name "BPM86" -length 0
Drift -name "TTA.D2" -length 0.5056938963 

Quadrupole -name "TTA.Q2" -synrad $quad_synrad -length 0.75 -strength [expr -0.6004214291*$e0] -e0 $e0 

Bpm -name "BPM87" -length 0
Drift -name "TTA.D3A" -length 0.3 

Multipole -name "TTA.SX2" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D3B" -length 3.077193097 

Drift -name "TTA.D3C" -length 0.1 

Quadrupole -name "TTA.Q3" -synrad $quad_synrad -length 0.75 -strength [expr 0.3042074493*$e0] -e0 $e0 

Bpm -name "BPM88" -length 0
Drift -name "TTA.D4A" -length 0.5779914647 

Multipole -name "TTA.SX3" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D4B" -length 0.3 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.D4B" -length 0.3 

Multipole -name "TTA.SX3" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D4A" -length 0.5779914647 

Quadrupole -name "TTA.Q3" -synrad $quad_synrad -length 0.75 -strength [expr 0.3042074493*$e0] -e0 $e0 

Bpm -name "BPM89" -length 0
Drift -name "TTA.D3C" -length 0.1 

Drift -name "TTA.D3B" -length 3.077193097 

Multipole -name "TTA.SX2" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D3A" -length 0.3 

Quadrupole -name "TTA.Q2" -synrad $quad_synrad -length 0.75 -strength [expr -0.6004214291*$e0] -e0 $e0 

Bpm -name "BPM90" -length 0
Drift -name "TTA.D2" -length 0.5056938963 

Quadrupole -name "TTA.Q1" -synrad $quad_synrad -length 0.75 -strength [expr 0.6863003356*$e0] -e0 $e0 

Bpm -name "BPM91" -length 0
Drift -name "TTA.D1B" -length 0.3 

Multipole -name "TTA.SX1" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D1A" -length 2.389121542 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.MBEND3" -length 0 

Drift -name "TTA.D2M" -length 2 

Drift -name "TTA.EXICELL" -length 0 

Drift -name "TTA.D2M" -length 2 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.D1A" -length 2.389121542 

Multipole -name "TTA.SX1" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D1B" -length 0.3 

Quadrupole -name "TTA.Q1" -synrad $quad_synrad -length 0.75 -strength [expr 0.6863003356*$e0] -e0 $e0 

Bpm -name "BPM92" -length 0
Drift -name "TTA.D2" -length 0.5056938963 

Quadrupole -name "TTA.Q2" -synrad $quad_synrad -length 0.75 -strength [expr -0.6004214291*$e0] -e0 $e0 

Bpm -name "BPM93" -length 0
Drift -name "TTA.D3A" -length 0.3 

Multipole -name "TTA.SX2" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D3B" -length 3.077193097 

Drift -name "TTA.D3C" -length 0.1 

Quadrupole -name "TTA.Q3" -synrad $quad_synrad -length 0.75 -strength [expr 0.3042074493*$e0] -e0 $e0 

Bpm -name "BPM94" -length 0
Drift -name "TTA.D4A" -length 0.5779914647 

Multipole -name "TTA.SX3" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D4B" -length 0.3 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.D4B" -length 0.3 

Multipole -name "TTA.SX3" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D4A" -length 0.5779914647 

Quadrupole -name "TTA.Q3" -synrad $quad_synrad -length 0.75 -strength [expr 0.3042074493*$e0] -e0 $e0 

Bpm -name "BPM95" -length 0
Drift -name "TTA.D3C" -length 0.1 

Drift -name "TTA.D3B" -length 3.077193097 

Multipole -name "TTA.SX2" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D3A" -length 0.3 

Quadrupole -name "TTA.Q2" -synrad $quad_synrad -length 0.75 -strength [expr -0.6004214291*$e0] -e0 $e0 

Bpm -name "BPM96" -length 0
Drift -name "TTA.D2" -length 0.5056938963 

Quadrupole -name "TTA.Q1" -synrad $quad_synrad -length 0.75 -strength [expr 0.6863003356*$e0] -e0 $e0 

Bpm -name "BPM97" -length 0
Drift -name "TTA.D1B" -length 0.3 

Multipole -name "TTA.SX1" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D1A" -length 2.389121542 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.MBEND3" -length 0 

Drift -name "TTA.D2M" -length 2 

Drift -name "TTA.EXICELL" -length 0 

Drift -name "TTA.D2M" -length 2 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.D1A" -length 2.389121542 

Multipole -name "TTA.SX1" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D1B" -length 0.3 

Quadrupole -name "TTA.Q1" -synrad $quad_synrad -length 0.75 -strength [expr 0.6863003356*$e0] -e0 $e0 

Bpm -name "BPM98" -length 0
Drift -name "TTA.D2" -length 0.5056938963 

Quadrupole -name "TTA.Q2" -synrad $quad_synrad -length 0.75 -strength [expr -0.6004214291*$e0] -e0 $e0 

Bpm -name "BPM99" -length 0
Drift -name "TTA.D3A" -length 0.3 

Multipole -name "TTA.SX2" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D3B" -length 3.077193097 

Drift -name "TTA.D3C" -length 0.1 

Quadrupole -name "TTA.Q3" -synrad $quad_synrad -length 0.75 -strength [expr 0.3042074493*$e0] -e0 $e0 

Bpm -name "BPM100" -length 0
Drift -name "TTA.D4A" -length 0.5779914647 

Multipole -name "TTA.SX3" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D4B" -length 0.3 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.D4B" -length 0.3 

Multipole -name "TTA.SX3" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D4A" -length 0.5779914647 

Quadrupole -name "TTA.Q3" -synrad $quad_synrad -length 0.75 -strength [expr 0.3042074493*$e0] -e0 $e0 

Bpm -name "BPM101" -length 0
Drift -name "TTA.D3C" -length 0.1 

Drift -name "TTA.D3B" -length 3.077193097 

Multipole -name "TTA.SX2" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D3A" -length 0.3 

Quadrupole -name "TTA.Q2" -synrad $quad_synrad -length 0.75 -strength [expr -0.6004214291*$e0] -e0 $e0 

Bpm -name "BPM102" -length 0
Drift -name "TTA.D2" -length 0.5056938963 

Quadrupole -name "TTA.Q1" -synrad $quad_synrad -length 0.75 -strength [expr 0.6863003356*$e0] -e0 $e0 

Bpm -name "BPM103" -length 0
Drift -name "TTA.D1B" -length 0.3 

Multipole -name "TTA.SX1" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D1A" -length 2.389121542 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.MBEND3" -length 0 

Drift -name "TTA.D2M" -length 2 

Drift -name "TTA.EXICELL" -length 0 

Drift -name "TTA.D2M" -length 2 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.D1A" -length 2.389121542 

Multipole -name "TTA.SX1" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D1B" -length 0.3 

Quadrupole -name "TTA.Q1" -synrad $quad_synrad -length 0.75 -strength [expr 0.6863003356*$e0] -e0 $e0 

Bpm -name "BPM104" -length 0
Drift -name "TTA.D2" -length 0.5056938963 

Quadrupole -name "TTA.Q2" -synrad $quad_synrad -length 0.75 -strength [expr -0.6004214291*$e0] -e0 $e0 

Bpm -name "BPM105" -length 0
Drift -name "TTA.D3A" -length 0.3 

Multipole -name "TTA.SX2" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D3B" -length 3.077193097 

Drift -name "TTA.D3C" -length 0.1 

Quadrupole -name "TTA.Q3" -synrad $quad_synrad -length 0.75 -strength [expr 0.3042074493*$e0] -e0 $e0 

Bpm -name "BPM106" -length 0
Drift -name "TTA.D4A" -length 0.5779914647 

Multipole -name "TTA.SX3" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D4B" -length 0.3 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.D4B" -length 0.3 

Multipole -name "TTA.SX3" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D4A" -length 0.5779914647 

Quadrupole -name "TTA.Q3" -synrad $quad_synrad -length 0.75 -strength [expr 0.3042074493*$e0] -e0 $e0 

Bpm -name "BPM107" -length 0
Drift -name "TTA.D3C" -length 0.1 

Drift -name "TTA.D3B" -length 3.077193097 

Multipole -name "TTA.SX2" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D3A" -length 0.3 

Quadrupole -name "TTA.Q2" -synrad $quad_synrad -length 0.75 -strength [expr -0.6004214291*$e0] -e0 $e0 

Bpm -name "BPM108" -length 0
Drift -name "TTA.D2" -length 0.5056938963 

Quadrupole -name "TTA.Q1" -synrad $quad_synrad -length 0.75 -strength [expr 0.6863003356*$e0] -e0 $e0 

Bpm -name "BPM109" -length 0
Drift -name "TTA.D1B" -length 0.3 

Multipole -name "TTA.SX1" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D1A" -length 2.389121542 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.MBEND3" -length 0 

Drift -name "TTA.D2M" -length 2 

Drift -name "TTA.EXICELL" -length 0 

Drift -name "TTA.D2M" -length 2 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.D1A" -length 2.389121542 

Multipole -name "TTA.SX1" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D1B" -length 0.3 

Quadrupole -name "TTA.Q1" -synrad $quad_synrad -length 0.75 -strength [expr 0.6863003356*$e0] -e0 $e0 

Bpm -name "BPM110" -length 0
Drift -name "TTA.D2" -length 0.5056938963 

Quadrupole -name "TTA.Q2" -synrad $quad_synrad -length 0.75 -strength [expr -0.6004214291*$e0] -e0 $e0 

Bpm -name "BPM111" -length 0
Drift -name "TTA.D3A" -length 0.3 

Multipole -name "TTA.SX2" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D3B" -length 3.077193097 

Drift -name "TTA.D3C" -length 0.1 

Quadrupole -name "TTA.Q3" -synrad $quad_synrad -length 0.75 -strength [expr 0.3042074493*$e0] -e0 $e0 

Bpm -name "BPM112" -length 0
Drift -name "TTA.D4A" -length 0.5779914647 

Multipole -name "TTA.SX3" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D4B" -length 0.3 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.D4B" -length 0.3 

Multipole -name "TTA.SX3" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D4A" -length 0.5779914647 

Quadrupole -name "TTA.Q3" -synrad $quad_synrad -length 0.75 -strength [expr 0.3042074493*$e0] -e0 $e0 

Bpm -name "BPM113" -length 0
Drift -name "TTA.D3C" -length 0.1 

Drift -name "TTA.D3B" -length 3.077193097 

Multipole -name "TTA.SX2" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D3A" -length 0.3 

Quadrupole -name "TTA.Q2" -synrad $quad_synrad -length 0.75 -strength [expr -0.6004214291*$e0] -e0 $e0 

Bpm -name "BPM114" -length 0
Drift -name "TTA.D2" -length 0.5056938963 

Quadrupole -name "TTA.Q1" -synrad $quad_synrad -length 0.75 -strength [expr 0.6863003356*$e0] -e0 $e0 

Bpm -name "BPM115" -length 0
Drift -name "TTA.D1B" -length 0.3 

Multipole -name "TTA.SX1" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D1A" -length 2.389121542 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.MBEND3" -length 0 

Drift -name "TTA.D2M" -length 2 

Drift -name "TTA.EXICELL" -length 0 

Drift -name "TTA.D2M" -length 2 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.D1A" -length 2.389121542 

Multipole -name "TTA.SX1" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D1B" -length 0.3 

Quadrupole -name "TTA.Q1" -synrad $quad_synrad -length 0.75 -strength [expr 0.6863003356*$e0] -e0 $e0 

Bpm -name "BPM116" -length 0
Drift -name "TTA.D2" -length 0.5056938963 

Quadrupole -name "TTA.Q2" -synrad $quad_synrad -length 0.75 -strength [expr -0.6004214291*$e0] -e0 $e0 

Bpm -name "BPM117" -length 0
Drift -name "TTA.D3A" -length 0.3 

Multipole -name "TTA.SX2" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D3B" -length 3.077193097 

Drift -name "TTA.D3C" -length 0.1 

Quadrupole -name "TTA.Q3" -synrad $quad_synrad -length 0.75 -strength [expr 0.3042074493*$e0] -e0 $e0 

Bpm -name "BPM118" -length 0
Drift -name "TTA.D4A" -length 0.5779914647 

Multipole -name "TTA.SX3" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D4B" -length 0.3 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.D4B" -length 0.3 

Multipole -name "TTA.SX3" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D4A" -length 0.5779914647 

Quadrupole -name "TTA.Q3" -synrad $quad_synrad -length 0.75 -strength [expr 0.3042074493*$e0] -e0 $e0 

Bpm -name "BPM119" -length 0
Drift -name "TTA.D3C" -length 0.1 

Drift -name "TTA.D3B" -length 3.077193097 

Multipole -name "TTA.SX2" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D3A" -length 0.3 

Quadrupole -name "TTA.Q2" -synrad $quad_synrad -length 0.75 -strength [expr -0.6004214291*$e0] -e0 $e0 

Bpm -name "BPM120" -length 0
Drift -name "TTA.D2" -length 0.5056938963 

Quadrupole -name "TTA.Q1" -synrad $quad_synrad -length 0.75 -strength [expr 0.6863003356*$e0] -e0 $e0 

Bpm -name "BPM121" -length 0
Drift -name "TTA.D1B" -length 0.3 

Multipole -name "TTA.SX1" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D1A" -length 2.389121542 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.MBEND3" -length 0 

Drift -name "TTA.D2M" -length 2 

Drift -name "TTA.EXICELL" -length 0 

Drift -name "TTA.D2M" -length 2 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.D1A" -length 2.389121542 

Multipole -name "TTA.SX1" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D1B" -length 0.3 

Quadrupole -name "TTA.Q1" -synrad $quad_synrad -length 0.75 -strength [expr 0.6863003356*$e0] -e0 $e0 

Bpm -name "BPM122" -length 0
Drift -name "TTA.D2" -length 0.5056938963 

Quadrupole -name "TTA.Q2" -synrad $quad_synrad -length 0.75 -strength [expr -0.6004214291*$e0] -e0 $e0 

Bpm -name "BPM123" -length 0
Drift -name "TTA.D3A" -length 0.3 

Multipole -name "TTA.SX2" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D3B" -length 3.077193097 

Drift -name "TTA.D3C" -length 0.1 

Quadrupole -name "TTA.Q3" -synrad $quad_synrad -length 0.75 -strength [expr 0.3042074493*$e0] -e0 $e0 

Bpm -name "BPM124" -length 0
Drift -name "TTA.D4A" -length 0.5779914647 

Multipole -name "TTA.SX3" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D4B" -length 0.3 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.D4B" -length 0.3 

Multipole -name "TTA.SX3" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D4A" -length 0.5779914647 

Quadrupole -name "TTA.Q3" -synrad $quad_synrad -length 0.75 -strength [expr 0.3042074493*$e0] -e0 $e0 

Bpm -name "BPM125" -length 0
Drift -name "TTA.D3C" -length 0.1 

Drift -name "TTA.D3B" -length 3.077193097 

Multipole -name "TTA.SX2" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D3A" -length 0.3 

Quadrupole -name "TTA.Q2" -synrad $quad_synrad -length 0.75 -strength [expr -0.6004214291*$e0] -e0 $e0 

Bpm -name "BPM126" -length 0
Drift -name "TTA.D2" -length 0.5056938963 

Quadrupole -name "TTA.Q1" -synrad $quad_synrad -length 0.75 -strength [expr 0.6863003356*$e0] -e0 $e0 

Bpm -name "BPM127" -length 0
Drift -name "TTA.D1B" -length 0.3 

Multipole -name "TTA.SX1" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D1A" -length 2.389121542 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.MBEND3" -length 0 

Drift -name "TTA.D2M" -length 2 

Drift -name "TTA.EXICELL" -length 0 

Drift -name "TTA.D2M" -length 2 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.D1A" -length 2.389121542 

Multipole -name "TTA.SX1" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D1B" -length 0.3 

Quadrupole -name "TTA.Q1" -synrad $quad_synrad -length 0.75 -strength [expr 0.6863003356*$e0] -e0 $e0 

Bpm -name "BPM128" -length 0
Drift -name "TTA.D2" -length 0.5056938963 

Quadrupole -name "TTA.Q2" -synrad $quad_synrad -length 0.75 -strength [expr -0.6004214291*$e0] -e0 $e0 

Bpm -name "BPM129" -length 0
Drift -name "TTA.D3A" -length 0.3 

Multipole -name "TTA.SX2" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D3B" -length 3.077193097 

Drift -name "TTA.D3C" -length 0.1 

Quadrupole -name "TTA.Q3" -synrad $quad_synrad -length 0.75 -strength [expr 0.3042074493*$e0] -e0 $e0 

Bpm -name "BPM130" -length 0
Drift -name "TTA.D4A" -length 0.5779914647 

Multipole -name "TTA.SX3" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D4B" -length 0.3 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.D4B" -length 0.3 

Multipole -name "TTA.SX3" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D4A" -length 0.5779914647 

Quadrupole -name "TTA.Q3" -synrad $quad_synrad -length 0.75 -strength [expr 0.3042074493*$e0] -e0 $e0 

Bpm -name "BPM131" -length 0
Drift -name "TTA.D3C" -length 0.1 

Drift -name "TTA.D3B" -length 3.077193097 

Multipole -name "TTA.SX2" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D3A" -length 0.3 

Quadrupole -name "TTA.Q2" -synrad $quad_synrad -length 0.75 -strength [expr -0.6004214291*$e0] -e0 $e0 

Bpm -name "BPM132" -length 0
Drift -name "TTA.D2" -length 0.5056938963 

Quadrupole -name "TTA.Q1" -synrad $quad_synrad -length 0.75 -strength [expr 0.6863003356*$e0] -e0 $e0 

Bpm -name "BPM133" -length 0
Drift -name "TTA.D1B" -length 0.3 

Multipole -name "TTA.SX1" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D1A" -length 2.389121542 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.MBEND3" -length 0 

Drift -name "TTA.D2M" -length 2 

Drift -name "TTA.EXICELL" -length 0 

Drift -name "TTA.D2M" -length 2 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.D1A" -length 2.389121542 

Multipole -name "TTA.SX1" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D1B" -length 0.3 

Quadrupole -name "TTA.Q1" -synrad $quad_synrad -length 0.75 -strength [expr 0.6863003356*$e0] -e0 $e0 

Bpm -name "BPM134" -length 0
Drift -name "TTA.D2" -length 0.5056938963 

Quadrupole -name "TTA.Q2" -synrad $quad_synrad -length 0.75 -strength [expr -0.6004214291*$e0] -e0 $e0 

Bpm -name "BPM135" -length 0
Drift -name "TTA.D3A" -length 0.3 

Multipole -name "TTA.SX2" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D3B" -length 3.077193097 

Drift -name "TTA.D3C" -length 0.1 

Quadrupole -name "TTA.Q3" -synrad $quad_synrad -length 0.75 -strength [expr 0.3042074493*$e0] -e0 $e0 

Bpm -name "BPM136" -length 0
Drift -name "TTA.D4A" -length 0.5779914647 

Multipole -name "TTA.SX3" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D4B" -length 0.3 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.D4B" -length 0.3 

Multipole -name "TTA.SX3" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D4A" -length 0.5779914647 

Quadrupole -name "TTA.Q3" -synrad $quad_synrad -length 0.75 -strength [expr 0.3042074493*$e0] -e0 $e0 

Bpm -name "BPM137" -length 0
Drift -name "TTA.D3C" -length 0.1 

Drift -name "TTA.D3B" -length 3.077193097 

Multipole -name "TTA.SX2" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D3A" -length 0.3 

Quadrupole -name "TTA.Q2" -synrad $quad_synrad -length 0.75 -strength [expr -0.6004214291*$e0] -e0 $e0 

Bpm -name "BPM138" -length 0
Drift -name "TTA.D2" -length 0.5056938963 

Quadrupole -name "TTA.Q1" -synrad $quad_synrad -length 0.75 -strength [expr 0.6863003356*$e0] -e0 $e0 

Bpm -name "BPM139" -length 0
Drift -name "TTA.D1B" -length 0.3 

Multipole -name "TTA.SX1" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D1A" -length 2.389121542 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.MBEND3" -length 0 

Drift -name "TTA.D2M" -length 2 

Drift -name "TTA.EXICELL" -length 0 

Drift -name "TTA.D2M" -length 2 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.D1A" -length 2.389121542 

Multipole -name "TTA.SX1" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D1B" -length 0.3 

Quadrupole -name "TTA.Q1" -synrad $quad_synrad -length 0.75 -strength [expr 0.6863003356*$e0] -e0 $e0 

Bpm -name "BPM140" -length 0
Drift -name "TTA.D2" -length 0.5056938963 

Quadrupole -name "TTA.Q2" -synrad $quad_synrad -length 0.75 -strength [expr -0.6004214291*$e0] -e0 $e0 

Bpm -name "BPM141" -length 0
Drift -name "TTA.D3A" -length 0.3 

Multipole -name "TTA.SX2" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D3B" -length 3.077193097 

Drift -name "TTA.D3C" -length 0.1 

Quadrupole -name "TTA.Q3" -synrad $quad_synrad -length 0.75 -strength [expr 0.3042074493*$e0] -e0 $e0 

Bpm -name "BPM142" -length 0
Drift -name "TTA.D4A" -length 0.5779914647 

Multipole -name "TTA.SX3" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D4B" -length 0.3 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.D4B" -length 0.3 

Multipole -name "TTA.SX3" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D4A" -length 0.5779914647 

Quadrupole -name "TTA.Q3" -synrad $quad_synrad -length 0.75 -strength [expr 0.3042074493*$e0] -e0 $e0 

Bpm -name "BPM143" -length 0
Drift -name "TTA.D3C" -length 0.1 

Drift -name "TTA.D3B" -length 3.077193097 

Multipole -name "TTA.SX2" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D3A" -length 0.3 

Quadrupole -name "TTA.Q2" -synrad $quad_synrad -length 0.75 -strength [expr -0.6004214291*$e0] -e0 $e0 

Bpm -name "BPM144" -length 0
Drift -name "TTA.D2" -length 0.5056938963 

Quadrupole -name "TTA.Q1" -synrad $quad_synrad -length 0.75 -strength [expr 0.6863003356*$e0] -e0 $e0 

Bpm -name "BPM145" -length 0
Drift -name "TTA.D1B" -length 0.3 

Multipole -name "TTA.SX1" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D1A" -length 2.389121542 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.MBEND3" -length 0 

Drift -name "TTA.D2M" -length 2 

Drift -name "TTA.EXICELL" -length 0 

Drift -name "TTA.D2M" -length 2 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.D1A" -length 2.389121542 

Multipole -name "TTA.SX1" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D1B" -length 0.3 

Quadrupole -name "TTA.Q1" -synrad $quad_synrad -length 0.75 -strength [expr 0.6863003356*$e0] -e0 $e0 

Bpm -name "BPM146" -length 0
Drift -name "TTA.D2" -length 0.5056938963 

Quadrupole -name "TTA.Q2" -synrad $quad_synrad -length 0.75 -strength [expr -0.6004214291*$e0] -e0 $e0 

Bpm -name "BPM147" -length 0
Drift -name "TTA.D3A" -length 0.3 

Multipole -name "TTA.SX2" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D3B" -length 3.077193097 

Drift -name "TTA.D3C" -length 0.1 

Quadrupole -name "TTA.Q3" -synrad $quad_synrad -length 0.75 -strength [expr 0.3042074493*$e0] -e0 $e0 

Bpm -name "BPM148" -length 0
Drift -name "TTA.D4A" -length 0.5779914647 

Multipole -name "TTA.SX3" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D4B" -length 0.3 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.D4B" -length 0.3 

Multipole -name "TTA.SX3" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D4A" -length 0.5779914647 

Quadrupole -name "TTA.Q3" -synrad $quad_synrad -length 0.75 -strength [expr 0.3042074493*$e0] -e0 $e0 

Bpm -name "BPM149" -length 0
Drift -name "TTA.D3C" -length 0.1 

Drift -name "TTA.D3B" -length 3.077193097 

Multipole -name "TTA.SX2" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D3A" -length 0.3 

Quadrupole -name "TTA.Q2" -synrad $quad_synrad -length 0.75 -strength [expr -0.6004214291*$e0] -e0 $e0 

Bpm -name "BPM150" -length 0
Drift -name "TTA.D2" -length 0.5056938963 

Quadrupole -name "TTA.Q1" -synrad $quad_synrad -length 0.75 -strength [expr 0.6863003356*$e0] -e0 $e0 

Bpm -name "BPM151" -length 0
Drift -name "TTA.D1B" -length 0.3 

Multipole -name "TTA.SX1" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D1A" -length 2.389121542 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.MBEND3" -length 0 

Drift -name "TTA.D2M" -length 2 

Drift -name "TTA.EXICELL" -length 0 

Drift -name "TTA.D2M" -length 2 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.D1A" -length 2.389121542 

Multipole -name "TTA.SX1" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D1B" -length 0.3 

Quadrupole -name "TTA.Q1" -synrad $quad_synrad -length 0.75 -strength [expr 0.6863003356*$e0] -e0 $e0 

Bpm -name "BPM152" -length 0
Drift -name "TTA.D2" -length 0.5056938963 

Quadrupole -name "TTA.Q2" -synrad $quad_synrad -length 0.75 -strength [expr -0.6004214291*$e0] -e0 $e0 

Bpm -name "BPM153" -length 0
Drift -name "TTA.D3A" -length 0.3 

Multipole -name "TTA.SX2" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D3B" -length 3.077193097 

Drift -name "TTA.D3C" -length 0.1 

Quadrupole -name "TTA.Q3" -synrad $quad_synrad -length 0.75 -strength [expr 0.3042074493*$e0] -e0 $e0 

Bpm -name "BPM154" -length 0
Drift -name "TTA.D4A" -length 0.5779914647 

Multipole -name "TTA.SX3" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D4B" -length 0.3 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.D4B" -length 0.3 

Multipole -name "TTA.SX3" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D4A" -length 0.5779914647 

Quadrupole -name "TTA.Q3" -synrad $quad_synrad -length 0.75 -strength [expr 0.3042074493*$e0] -e0 $e0 

Bpm -name "BPM155" -length 0
Drift -name "TTA.D3C" -length 0.1 

Drift -name "TTA.D3B" -length 3.077193097 

Multipole -name "TTA.SX2" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D3A" -length 0.3 

Quadrupole -name "TTA.Q2" -synrad $quad_synrad -length 0.75 -strength [expr -0.6004214291*$e0] -e0 $e0 

Bpm -name "BPM156" -length 0
Drift -name "TTA.D2" -length 0.5056938963 

Quadrupole -name "TTA.Q1" -synrad $quad_synrad -length 0.75 -strength [expr 0.6863003356*$e0] -e0 $e0 

Bpm -name "BPM157" -length 0
Drift -name "TTA.D1B" -length 0.3 

Multipole -name "TTA.SX1" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D1A" -length 2.389121542 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.MBEND3" -length 0 

Drift -name "TTA.D2M" -length 2 

Drift -name "TTA.EXICELL" -length 0 

Drift -name "TTA.D2M" -length 2 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.D1A" -length 2.389121542 

Multipole -name "TTA.SX1" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D1B" -length 0.3 

Quadrupole -name "TTA.Q1" -synrad $quad_synrad -length 0.75 -strength [expr 0.6863003356*$e0] -e0 $e0 

Bpm -name "BPM158" -length 0
Drift -name "TTA.D2" -length 0.5056938963 

Quadrupole -name "TTA.Q2" -synrad $quad_synrad -length 0.75 -strength [expr -0.6004214291*$e0] -e0 $e0 

Bpm -name "BPM159" -length 0
Drift -name "TTA.D3A" -length 0.3 

Multipole -name "TTA.SX2" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D3B" -length 3.077193097 

Drift -name "TTA.D3C" -length 0.1 

Quadrupole -name "TTA.Q3" -synrad $quad_synrad -length 0.75 -strength [expr 0.3042074493*$e0] -e0 $e0 

Bpm -name "BPM160" -length 0
Drift -name "TTA.D4A" -length 0.5779914647 

Multipole -name "TTA.SX3" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D4B" -length 0.3 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.D4B" -length 0.3 

Multipole -name "TTA.SX3" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D4A" -length 0.5779914647 

Quadrupole -name "TTA.Q3" -synrad $quad_synrad -length 0.75 -strength [expr 0.3042074493*$e0] -e0 $e0 

Bpm -name "BPM161" -length 0
Drift -name "TTA.D3C" -length 0.1 

Drift -name "TTA.D3B" -length 3.077193097 

Multipole -name "TTA.SX2" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D3A" -length 0.3 

Quadrupole -name "TTA.Q2" -synrad $quad_synrad -length 0.75 -strength [expr -0.6004214291*$e0] -e0 $e0 

Bpm -name "BPM162" -length 0
Drift -name "TTA.D2" -length 0.5056938963 

Quadrupole -name "TTA.Q1" -synrad $quad_synrad -length 0.75 -strength [expr 0.6863003356*$e0] -e0 $e0 

Bpm -name "BPM163" -length 0
Drift -name "TTA.D1B" -length 0.3 

Multipole -name "TTA.SX1" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D1A" -length 2.389121542 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.MBEND3" -length 0 

Drift -name "TTA.D2M" -length 2 

Drift -name "TTA.EXICELL" -length 0 

Drift -name "TTA.D2M" -length 2 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.D1A" -length 2.389121542 

Multipole -name "TTA.SX1" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D1B" -length 0.3 

Quadrupole -name "TTA.Q1" -synrad $quad_synrad -length 0.75 -strength [expr 0.6863003356*$e0] -e0 $e0 

Bpm -name "BPM164" -length 0
Drift -name "TTA.D2" -length 0.5056938963 

Quadrupole -name "TTA.Q2" -synrad $quad_synrad -length 0.75 -strength [expr -0.6004214291*$e0] -e0 $e0 

Bpm -name "BPM165" -length 0
Drift -name "TTA.D3A" -length 0.3 

Multipole -name "TTA.SX2" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D3B" -length 3.077193097 

Drift -name "TTA.D3C" -length 0.1 

Quadrupole -name "TTA.Q3" -synrad $quad_synrad -length 0.75 -strength [expr 0.3042074493*$e0] -e0 $e0 

Bpm -name "BPM166" -length 0
Drift -name "TTA.D4A" -length 0.5779914647 

Multipole -name "TTA.SX3" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D4B" -length 0.3 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.D4B" -length 0.3 

Multipole -name "TTA.SX3" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D4A" -length 0.5779914647 

Quadrupole -name "TTA.Q3" -synrad $quad_synrad -length 0.75 -strength [expr 0.3042074493*$e0] -e0 $e0 

Bpm -name "BPM167" -length 0
Drift -name "TTA.D3C" -length 0.1 

Drift -name "TTA.D3B" -length 3.077193097 

Multipole -name "TTA.SX2" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D3A" -length 0.3 

Quadrupole -name "TTA.Q2" -synrad $quad_synrad -length 0.75 -strength [expr -0.6004214291*$e0] -e0 $e0 

Bpm -name "BPM168" -length 0
Drift -name "TTA.D2" -length 0.5056938963 

Quadrupole -name "TTA.Q1" -synrad $quad_synrad -length 0.75 -strength [expr 0.6863003356*$e0] -e0 $e0 

Bpm -name "BPM169" -length 0
Drift -name "TTA.D1B" -length 0.3 

Multipole -name "TTA.SX1" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D1A" -length 2.389121542 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.MBEND3" -length 0 

Drift -name "TTA.D2M" -length 2 

Drift -name "TTA.EXICELL" -length 0 

Drift -name "TTA.D2M" -length 2 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.D1A" -length 2.389121542 

Multipole -name "TTA.SX1" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D1B" -length 0.3 

Quadrupole -name "TTA.Q1" -synrad $quad_synrad -length 0.75 -strength [expr 0.6863003356*$e0] -e0 $e0 

Bpm -name "BPM170" -length 0
Drift -name "TTA.D2" -length 0.5056938963 

Quadrupole -name "TTA.Q2" -synrad $quad_synrad -length 0.75 -strength [expr -0.6004214291*$e0] -e0 $e0 

Bpm -name "BPM171" -length 0
Drift -name "TTA.D3A" -length 0.3 

Multipole -name "TTA.SX2" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D3B" -length 3.077193097 

Drift -name "TTA.D3C" -length 0.1 

Quadrupole -name "TTA.Q3" -synrad $quad_synrad -length 0.75 -strength [expr 0.3042074493*$e0] -e0 $e0 

Bpm -name "BPM172" -length 0
Drift -name "TTA.D4A" -length 0.5779914647 

Multipole -name "TTA.SX3" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D4B" -length 0.3 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.D4B" -length 0.3 

Multipole -name "TTA.SX3" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D4A" -length 0.5779914647 

Quadrupole -name "TTA.Q3" -synrad $quad_synrad -length 0.75 -strength [expr 0.3042074493*$e0] -e0 $e0 

Bpm -name "BPM173" -length 0
Drift -name "TTA.D3C" -length 0.1 

Drift -name "TTA.D3B" -length 3.077193097 

Multipole -name "TTA.SX2" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D3A" -length 0.3 

Quadrupole -name "TTA.Q2" -synrad $quad_synrad -length 0.75 -strength [expr -0.6004214291*$e0] -e0 $e0 

Bpm -name "BPM174" -length 0
Drift -name "TTA.D2" -length 0.5056938963 

Quadrupole -name "TTA.Q1" -synrad $quad_synrad -length 0.75 -strength [expr 0.6863003356*$e0] -e0 $e0 

Bpm -name "BPM175" -length 0
Drift -name "TTA.D1B" -length 0.3 

Multipole -name "TTA.SX1" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D1A" -length 2.389121542 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.MBEND3" -length 0 

Drift -name "TTA.D2M" -length 2 

Drift -name "TTA.EXICELL" -length 0 

Drift -name "TTA.D2M" -length 2 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.D1A" -length 2.389121542 

Multipole -name "TTA.SX1" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D1B" -length 0.3 

Quadrupole -name "TTA.Q1" -synrad $quad_synrad -length 0.75 -strength [expr 0.6863003356*$e0] -e0 $e0 

Bpm -name "BPM176" -length 0
Drift -name "TTA.D2" -length 0.5056938963 

Quadrupole -name "TTA.Q2" -synrad $quad_synrad -length 0.75 -strength [expr -0.6004214291*$e0] -e0 $e0 

Bpm -name "BPM177" -length 0
Drift -name "TTA.D3A" -length 0.3 

Multipole -name "TTA.SX2" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D3B" -length 3.077193097 

Drift -name "TTA.D3C" -length 0.1 

Quadrupole -name "TTA.Q3" -synrad $quad_synrad -length 0.75 -strength [expr 0.3042074493*$e0] -e0 $e0 

Bpm -name "BPM178" -length 0
Drift -name "TTA.D4A" -length 0.5779914647 

Multipole -name "TTA.SX3" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D4B" -length 0.3 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.D4B" -length 0.3 

Multipole -name "TTA.SX3" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D4A" -length 0.5779914647 

Quadrupole -name "TTA.Q3" -synrad $quad_synrad -length 0.75 -strength [expr 0.3042074493*$e0] -e0 $e0 

Bpm -name "BPM179" -length 0
Drift -name "TTA.D3C" -length 0.1 

Drift -name "TTA.D3B" -length 3.077193097 

Multipole -name "TTA.SX2" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D3A" -length 0.3 

Quadrupole -name "TTA.Q2" -synrad $quad_synrad -length 0.75 -strength [expr -0.6004214291*$e0] -e0 $e0 

Bpm -name "BPM180" -length 0
Drift -name "TTA.D2" -length 0.5056938963 

Quadrupole -name "TTA.Q1" -synrad $quad_synrad -length 0.75 -strength [expr 0.6863003356*$e0] -e0 $e0 

Bpm -name "BPM181" -length 0
Drift -name "TTA.D1B" -length 0.3 

Multipole -name "TTA.SX1" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D1A" -length 2.389121542 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.MBEND3" -length 0 

Drift -name "TTA.D2M" -length 2 

Drift -name "TTA.EXICELL" -length 0 

Drift -name "TTA.D2M" -length 2 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.D1A" -length 2.389121542 

Multipole -name "TTA.SX1" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D1B" -length 0.3 

Quadrupole -name "TTA.Q1" -synrad $quad_synrad -length 0.75 -strength [expr 0.6863003356*$e0] -e0 $e0 

Bpm -name "BPM182" -length 0
Drift -name "TTA.D2" -length 0.5056938963 

Quadrupole -name "TTA.Q2" -synrad $quad_synrad -length 0.75 -strength [expr -0.6004214291*$e0] -e0 $e0 

Bpm -name "BPM183" -length 0
Drift -name "TTA.D3A" -length 0.3 

Multipole -name "TTA.SX2" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D3B" -length 3.077193097 

Drift -name "TTA.D3C" -length 0.1 

Quadrupole -name "TTA.Q3" -synrad $quad_synrad -length 0.75 -strength [expr 0.3042074493*$e0] -e0 $e0 

Bpm -name "BPM184" -length 0
Drift -name "TTA.D4A" -length 0.5779914647 

Multipole -name "TTA.SX3" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D4B" -length 0.3 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.D4B" -length 0.3 

Multipole -name "TTA.SX3" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D4A" -length 0.5779914647 

Quadrupole -name "TTA.Q3" -synrad $quad_synrad -length 0.75 -strength [expr 0.3042074493*$e0] -e0 $e0 

Bpm -name "BPM185" -length 0
Drift -name "TTA.D3C" -length 0.1 

Drift -name "TTA.D3B" -length 3.077193097 

Multipole -name "TTA.SX2" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D3A" -length 0.3 

Quadrupole -name "TTA.Q2" -synrad $quad_synrad -length 0.75 -strength [expr -0.6004214291*$e0] -e0 $e0 

Bpm -name "BPM186" -length 0
Drift -name "TTA.D2" -length 0.5056938963 

Quadrupole -name "TTA.Q1" -synrad $quad_synrad -length 0.75 -strength [expr 0.6863003356*$e0] -e0 $e0 

Bpm -name "BPM187" -length 0
Drift -name "TTA.D1B" -length 0.3 

Multipole -name "TTA.SX1" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D1A" -length 2.389121542 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.MBEND3" -length 0 

Drift -name "TTA.D2M" -length 2 

Drift -name "TTA.EXICELL" -length 0 

Drift -name "TTA.D2M" -length 2 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.D1A" -length 2.389121542 

Multipole -name "TTA.SX1" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D1B" -length 0.3 

Quadrupole -name "TTA.Q1" -synrad $quad_synrad -length 0.75 -strength [expr 0.6863003356*$e0] -e0 $e0 

Bpm -name "BPM188" -length 0
Drift -name "TTA.D2" -length 0.5056938963 

Quadrupole -name "TTA.Q2" -synrad $quad_synrad -length 0.75 -strength [expr -0.6004214291*$e0] -e0 $e0 

Bpm -name "BPM189" -length 0
Drift -name "TTA.D3A" -length 0.3 

Multipole -name "TTA.SX2" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D3B" -length 3.077193097 

Drift -name "TTA.D3C" -length 0.1 

Quadrupole -name "TTA.Q3" -synrad $quad_synrad -length 0.75 -strength [expr 0.3042074493*$e0] -e0 $e0 

Bpm -name "BPM190" -length 0
Drift -name "TTA.D4A" -length 0.5779914647 

Multipole -name "TTA.SX3" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D4B" -length 0.3 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.D4B" -length 0.3 

Multipole -name "TTA.SX3" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D4A" -length 0.5779914647 

Quadrupole -name "TTA.Q3" -synrad $quad_synrad -length 0.75 -strength [expr 0.3042074493*$e0] -e0 $e0 

Bpm -name "BPM191" -length 0
Drift -name "TTA.D3C" -length 0.1 

Drift -name "TTA.D3B" -length 3.077193097 

Multipole -name "TTA.SX2" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D3A" -length 0.3 

Quadrupole -name "TTA.Q2" -synrad $quad_synrad -length 0.75 -strength [expr -0.6004214291*$e0] -e0 $e0 

Bpm -name "BPM192" -length 0
Drift -name "TTA.D2" -length 0.5056938963 

Quadrupole -name "TTA.Q1" -synrad $quad_synrad -length 0.75 -strength [expr 0.6863003356*$e0] -e0 $e0 

Bpm -name "BPM193" -length 0
Drift -name "TTA.D1B" -length 0.3 

Multipole -name "TTA.SX1" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D1A" -length 2.389121542 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.MBEND3" -length 0 

Drift -name "TTA.D2M" -length 2 

Drift -name "TTA.EXICELL" -length 0 

Drift -name "TTA.D2M" -length 2 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.D1A" -length 2.389121542 

Multipole -name "TTA.SX1" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D1B" -length 0.3 

Quadrupole -name "TTA.Q1" -synrad $quad_synrad -length 0.75 -strength [expr 0.6863003356*$e0] -e0 $e0 

Bpm -name "BPM194" -length 0
Drift -name "TTA.D2" -length 0.5056938963 

Quadrupole -name "TTA.Q2" -synrad $quad_synrad -length 0.75 -strength [expr -0.6004214291*$e0] -e0 $e0 

Bpm -name "BPM195" -length 0
Drift -name "TTA.D3A" -length 0.3 

Multipole -name "TTA.SX2" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D3B" -length 3.077193097 

Drift -name "TTA.D3C" -length 0.1 

Quadrupole -name "TTA.Q3" -synrad $quad_synrad -length 0.75 -strength [expr 0.3042074493*$e0] -e0 $e0 

Bpm -name "BPM196" -length 0
Drift -name "TTA.D4A" -length 0.5779914647 

Multipole -name "TTA.SX3" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D4B" -length 0.3 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.D4B" -length 0.3 

Multipole -name "TTA.SX3" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D4A" -length 0.5779914647 

Quadrupole -name "TTA.Q3" -synrad $quad_synrad -length 0.75 -strength [expr 0.3042074493*$e0] -e0 $e0 

Bpm -name "BPM197" -length 0
Drift -name "TTA.D3C" -length 0.1 

Drift -name "TTA.D3B" -length 3.077193097 

Multipole -name "TTA.SX2" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D3A" -length 0.3 

Quadrupole -name "TTA.Q2" -synrad $quad_synrad -length 0.75 -strength [expr -0.6004214291*$e0] -e0 $e0 

Bpm -name "BPM198" -length 0
Drift -name "TTA.D2" -length 0.5056938963 

Quadrupole -name "TTA.Q1" -synrad $quad_synrad -length 0.75 -strength [expr 0.6863003356*$e0] -e0 $e0 

Bpm -name "BPM199" -length 0
Drift -name "TTA.D1B" -length 0.3 

Multipole -name "TTA.SX1" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D1A" -length 2.389121542 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.MBEND3" -length 0 

Drift -name "TTA.D2M" -length 2 

Drift -name "TTA.EXICELL" -length 0 

Drift -name "TTA.D2M" -length 2 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.D1A" -length 2.389121542 

Multipole -name "TTA.SX1" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D1B" -length 0.3 

Quadrupole -name "TTA.Q1" -synrad $quad_synrad -length 0.75 -strength [expr 0.6863003356*$e0] -e0 $e0 

Bpm -name "BPM200" -length 0
Drift -name "TTA.D2" -length 0.5056938963 

Quadrupole -name "TTA.Q2" -synrad $quad_synrad -length 0.75 -strength [expr -0.6004214291*$e0] -e0 $e0 

Bpm -name "BPM201" -length 0
Drift -name "TTA.D3A" -length 0.3 

Multipole -name "TTA.SX2" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D3B" -length 3.077193097 

Drift -name "TTA.D3C" -length 0.1 

Quadrupole -name "TTA.Q3" -synrad $quad_synrad -length 0.75 -strength [expr 0.3042074493*$e0] -e0 $e0 

Bpm -name "BPM202" -length 0
Drift -name "TTA.D4A" -length 0.5779914647 

Multipole -name "TTA.SX3" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D4B" -length 0.3 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.D4B" -length 0.3 

Multipole -name "TTA.SX3" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D4A" -length 0.5779914647 

Quadrupole -name "TTA.Q3" -synrad $quad_synrad -length 0.75 -strength [expr 0.3042074493*$e0] -e0 $e0 

Bpm -name "BPM203" -length 0
Drift -name "TTA.D3C" -length 0.1 

Drift -name "TTA.D3B" -length 3.077193097 

Multipole -name "TTA.SX2" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D3A" -length 0.3 

Quadrupole -name "TTA.Q2" -synrad $quad_synrad -length 0.75 -strength [expr -0.6004214291*$e0] -e0 $e0 

Bpm -name "BPM204" -length 0
Drift -name "TTA.D2" -length 0.5056938963 

Quadrupole -name "TTA.Q1" -synrad $quad_synrad -length 0.75 -strength [expr 0.6863003356*$e0] -e0 $e0 

Bpm -name "BPM205" -length 0
Drift -name "TTA.D1B" -length 0.3 

Multipole -name "TTA.SX1" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D1A" -length 2.389121542 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.MBEND3" -length 0 

Drift -name "TTA.D2M" -length 2 

Drift -name "TTA.EXICELL" -length 0 

Drift -name "TTA.D2M" -length 2 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.D1A" -length 2.389121542 

Multipole -name "TTA.SX1" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D1B" -length 0.3 

Quadrupole -name "TTA.Q1" -synrad $quad_synrad -length 0.75 -strength [expr 0.6863003356*$e0] -e0 $e0 

Bpm -name "BPM206" -length 0
Drift -name "TTA.D2" -length 0.5056938963 

Quadrupole -name "TTA.Q2" -synrad $quad_synrad -length 0.75 -strength [expr -0.6004214291*$e0] -e0 $e0 

Bpm -name "BPM207" -length 0
Drift -name "TTA.D3A" -length 0.3 

Multipole -name "TTA.SX2" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D3B" -length 3.077193097 

Drift -name "TTA.D3C" -length 0.1 

Quadrupole -name "TTA.Q3" -synrad $quad_synrad -length 0.75 -strength [expr 0.3042074493*$e0] -e0 $e0 

Bpm -name "BPM208" -length 0
Drift -name "TTA.D4A" -length 0.5779914647 

Multipole -name "TTA.SX3" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D4B" -length 0.3 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.D4B" -length 0.3 

Multipole -name "TTA.SX3" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D4A" -length 0.5779914647 

Quadrupole -name "TTA.Q3" -synrad $quad_synrad -length 0.75 -strength [expr 0.3042074493*$e0] -e0 $e0 

Bpm -name "BPM209" -length 0
Drift -name "TTA.D3C" -length 0.1 

Drift -name "TTA.D3B" -length 3.077193097 

Multipole -name "TTA.SX2" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D3A" -length 0.3 

Quadrupole -name "TTA.Q2" -synrad $quad_synrad -length 0.75 -strength [expr -0.6004214291*$e0] -e0 $e0 

Bpm -name "BPM210" -length 0
Drift -name "TTA.D2" -length 0.5056938963 

Quadrupole -name "TTA.Q1" -synrad $quad_synrad -length 0.75 -strength [expr 0.6863003356*$e0] -e0 $e0 

Bpm -name "BPM211" -length 0
Drift -name "TTA.D1B" -length 0.3 

Multipole -name "TTA.SX1" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D1A" -length 2.389121542 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.MBEND3" -length 0 

Drift -name "TTA.D2M" -length 2 

Drift -name "TTA.EXICELL" -length 0 

Drift -name "TTA.D2M" -length 2 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.D1A" -length 2.389121542 

Multipole -name "TTA.SX1" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D1B" -length 0.3 

Quadrupole -name "TTA.Q1" -synrad $quad_synrad -length 0.75 -strength [expr 0.6863003356*$e0] -e0 $e0 

Bpm -name "BPM212" -length 0
Drift -name "TTA.D2" -length 0.5056938963 

Quadrupole -name "TTA.Q2" -synrad $quad_synrad -length 0.75 -strength [expr -0.6004214291*$e0] -e0 $e0 

Bpm -name "BPM213" -length 0
Drift -name "TTA.D3A" -length 0.3 

Multipole -name "TTA.SX2" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D3B" -length 3.077193097 

Drift -name "TTA.D3C" -length 0.1 

Quadrupole -name "TTA.Q3" -synrad $quad_synrad -length 0.75 -strength [expr 0.3042074493*$e0] -e0 $e0 

Bpm -name "BPM214" -length 0
Drift -name "TTA.D4A" -length 0.5779914647 

Multipole -name "TTA.SX3" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D4B" -length 0.3 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.D4B" -length 0.3 

Multipole -name "TTA.SX3" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D4A" -length 0.5779914647 

Quadrupole -name "TTA.Q3" -synrad $quad_synrad -length 0.75 -strength [expr 0.3042074493*$e0] -e0 $e0 

Bpm -name "BPM215" -length 0
Drift -name "TTA.D3C" -length 0.1 

Drift -name "TTA.D3B" -length 3.077193097 

Multipole -name "TTA.SX2" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D3A" -length 0.3 

Quadrupole -name "TTA.Q2" -synrad $quad_synrad -length 0.75 -strength [expr -0.6004214291*$e0] -e0 $e0 

Bpm -name "BPM216" -length 0
Drift -name "TTA.D2" -length 0.5056938963 

Quadrupole -name "TTA.Q1" -synrad $quad_synrad -length 0.75 -strength [expr 0.6863003356*$e0] -e0 $e0 

Bpm -name "BPM217" -length 0
Drift -name "TTA.D1B" -length 0.3 

Multipole -name "TTA.SX1" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D1A" -length 2.389121542 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.MBEND3" -length 0 

Drift -name "TTA.D2M" -length 2 

Drift -name "TTA.EXICELL" -length 0 

Drift -name "TTA.D2M" -length 2 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.D1A" -length 2.389121542 

Multipole -name "TTA.SX1" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D1B" -length 0.3 

Quadrupole -name "TTA.Q1" -synrad $quad_synrad -length 0.75 -strength [expr 0.6863003356*$e0] -e0 $e0 

Bpm -name "BPM218" -length 0
Drift -name "TTA.D2" -length 0.5056938963 

Quadrupole -name "TTA.Q2" -synrad $quad_synrad -length 0.75 -strength [expr -0.6004214291*$e0] -e0 $e0 

Bpm -name "BPM219" -length 0
Drift -name "TTA.D3A" -length 0.3 

Multipole -name "TTA.SX2" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D3B" -length 3.077193097 

Drift -name "TTA.D3C" -length 0.1 

Quadrupole -name "TTA.Q3" -synrad $quad_synrad -length 0.75 -strength [expr 0.3042074493*$e0] -e0 $e0 

Bpm -name "BPM220" -length 0
Drift -name "TTA.D4A" -length 0.5779914647 

Multipole -name "TTA.SX3" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D4B" -length 0.3 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.D4B" -length 0.3 

Multipole -name "TTA.SX3" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D4A" -length 0.5779914647 

Quadrupole -name "TTA.Q3" -synrad $quad_synrad -length 0.75 -strength [expr 0.3042074493*$e0] -e0 $e0 

Bpm -name "BPM221" -length 0
Drift -name "TTA.D3C" -length 0.1 

Drift -name "TTA.D3B" -length 3.077193097 

Multipole -name "TTA.SX2" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D3A" -length 0.3 

Quadrupole -name "TTA.Q2" -synrad $quad_synrad -length 0.75 -strength [expr -0.6004214291*$e0] -e0 $e0 

Bpm -name "BPM222" -length 0
Drift -name "TTA.D2" -length 0.5056938963 

Quadrupole -name "TTA.Q1" -synrad $quad_synrad -length 0.75 -strength [expr 0.6863003356*$e0] -e0 $e0 

Bpm -name "BPM223" -length 0
Drift -name "TTA.D1B" -length 0.3 

Multipole -name "TTA.SX1" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D1A" -length 2.389121542 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.MBEND3" -length 0 

Drift -name "TTA.D2M" -length 2 

Drift -name "TTA.EXICELL" -length 0 

Drift -name "TTA.D2M" -length 2 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.D1A" -length 2.389121542 

Multipole -name "TTA.SX1" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D1B" -length 0.3 

Quadrupole -name "TTA.Q1" -synrad $quad_synrad -length 0.75 -strength [expr 0.6863003356*$e0] -e0 $e0 

Bpm -name "BPM224" -length 0
Drift -name "TTA.D2" -length 0.5056938963 

Quadrupole -name "TTA.Q2" -synrad $quad_synrad -length 0.75 -strength [expr -0.6004214291*$e0] -e0 $e0 

Bpm -name "BPM225" -length 0
Drift -name "TTA.D3A" -length 0.3 

Multipole -name "TTA.SX2" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D3B" -length 3.077193097 

Drift -name "TTA.D3C" -length 0.1 

Quadrupole -name "TTA.Q3" -synrad $quad_synrad -length 0.75 -strength [expr 0.3042074493*$e0] -e0 $e0 

Bpm -name "BPM226" -length 0
Drift -name "TTA.D4A" -length 0.5779914647 

Multipole -name "TTA.SX3" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D4B" -length 0.3 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.D4B" -length 0.3 

Multipole -name "TTA.SX3" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D4A" -length 0.5779914647 

Quadrupole -name "TTA.Q3" -synrad $quad_synrad -length 0.75 -strength [expr 0.3042074493*$e0] -e0 $e0 

Bpm -name "BPM227" -length 0
Drift -name "TTA.D3C" -length 0.1 

Drift -name "TTA.D3B" -length 3.077193097 

Multipole -name "TTA.SX2" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D3A" -length 0.3 

Quadrupole -name "TTA.Q2" -synrad $quad_synrad -length 0.75 -strength [expr -0.6004214291*$e0] -e0 $e0 

Bpm -name "BPM228" -length 0
Drift -name "TTA.D2" -length 0.5056938963 

Quadrupole -name "TTA.Q1" -synrad $quad_synrad -length 0.75 -strength [expr 0.6863003356*$e0] -e0 $e0 

Bpm -name "BPM229" -length 0
Drift -name "TTA.D1B" -length 0.3 

Multipole -name "TTA.SX1" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D1A" -length 2.389121542 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.MBEND3" -length 0 

Drift -name "TTA.D2M" -length 2 

Drift -name "TTA.EXICELL" -length 0 

Drift -name "TTA.D2M" -length 2 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.D1A" -length 2.389121542 

Multipole -name "TTA.SX1" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D1B" -length 0.3 

Quadrupole -name "TTA.Q1" -synrad $quad_synrad -length 0.75 -strength [expr 0.6863003356*$e0] -e0 $e0 

Bpm -name "BPM230" -length 0
Drift -name "TTA.D2" -length 0.5056938963 

Quadrupole -name "TTA.Q2" -synrad $quad_synrad -length 0.75 -strength [expr -0.6004214291*$e0] -e0 $e0 

Bpm -name "BPM231" -length 0
Drift -name "TTA.D3A" -length 0.3 

Multipole -name "TTA.SX2" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D3B" -length 3.077193097 

Drift -name "TTA.D3C" -length 0.1 

Quadrupole -name "TTA.Q3" -synrad $quad_synrad -length 0.75 -strength [expr 0.3042074493*$e0] -e0 $e0 

Bpm -name "BPM232" -length 0
Drift -name "TTA.D4A" -length 0.5779914647 

Multipole -name "TTA.SX3" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D4B" -length 0.3 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.D4B" -length 0.3 

Multipole -name "TTA.SX3" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D4A" -length 0.5779914647 

Quadrupole -name "TTA.Q3" -synrad $quad_synrad -length 0.75 -strength [expr 0.3042074493*$e0] -e0 $e0 

Bpm -name "BPM233" -length 0
Drift -name "TTA.D3C" -length 0.1 

Drift -name "TTA.D3B" -length 3.077193097 

Multipole -name "TTA.SX2" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D3A" -length 0.3 

Quadrupole -name "TTA.Q2" -synrad $quad_synrad -length 0.75 -strength [expr -0.6004214291*$e0] -e0 $e0 

Bpm -name "BPM234" -length 0
Drift -name "TTA.D2" -length 0.5056938963 

Quadrupole -name "TTA.Q1" -synrad $quad_synrad -length 0.75 -strength [expr 0.6863003356*$e0] -e0 $e0 

Bpm -name "BPM235" -length 0
Drift -name "TTA.D1B" -length 0.3 

Multipole -name "TTA.SX1" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D1A" -length 2.389121542 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.MBEND3" -length 0 

Drift -name "TTA.D2M" -length 2 

Drift -name "TTA.EXICELL" -length 0 

Drift -name "TTA.D2M" -length 2 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.D1A" -length 2.389121542 

Multipole -name "TTA.SX1" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D1B" -length 0.3 

Quadrupole -name "TTA.Q1" -synrad $quad_synrad -length 0.75 -strength [expr 0.6863003356*$e0] -e0 $e0 

Bpm -name "BPM236" -length 0
Drift -name "TTA.D2" -length 0.5056938963 

Quadrupole -name "TTA.Q2" -synrad $quad_synrad -length 0.75 -strength [expr -0.6004214291*$e0] -e0 $e0 

Bpm -name "BPM237" -length 0
Drift -name "TTA.D3A" -length 0.3 

Multipole -name "TTA.SX2" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D3B" -length 3.077193097 

Drift -name "TTA.D3C" -length 0.1 

Quadrupole -name "TTA.Q3" -synrad $quad_synrad -length 0.75 -strength [expr 0.3042074493*$e0] -e0 $e0 

Bpm -name "BPM238" -length 0
Drift -name "TTA.D4A" -length 0.5779914647 

Multipole -name "TTA.SX3" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D4B" -length 0.3 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.D4B" -length 0.3 

Multipole -name "TTA.SX3" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D4A" -length 0.5779914647 

Quadrupole -name "TTA.Q3" -synrad $quad_synrad -length 0.75 -strength [expr 0.3042074493*$e0] -e0 $e0 

Bpm -name "BPM239" -length 0
Drift -name "TTA.D3C" -length 0.1 

Drift -name "TTA.D3B" -length 3.077193097 

Multipole -name "TTA.SX2" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D3A" -length 0.3 

Quadrupole -name "TTA.Q2" -synrad $quad_synrad -length 0.75 -strength [expr -0.6004214291*$e0] -e0 $e0 

Bpm -name "BPM240" -length 0
Drift -name "TTA.D2" -length 0.5056938963 

Quadrupole -name "TTA.Q1" -synrad $quad_synrad -length 0.75 -strength [expr 0.6863003356*$e0] -e0 $e0 

Bpm -name "BPM241" -length 0
Drift -name "TTA.D1B" -length 0.3 

Multipole -name "TTA.SX1" -synrad $mult_synrad -type 3 -length 0.4 -strength [expr 0*$e0] -e0 $e0 

Drift -name "TTA.D1A" -length 2.389121542 

# WARNING: putting a Sbend instead of a Rbend. Arc's length is : angle * L / sin(angle/2) / 2

# WARNING: original length was 2.000101543

Sbend -name "TTA.BEND" -synrad $sbend_synrad -length 2.00020309090409 -angle -0.03490658504 -E1 -0.01745329252 -E2 -0.01745329252 -e0 $e0 -fintx -1 

set e0 [expr $e0-14.1e-6*-0.03490658504*-0.03490658504/2.00020309090409*$e0*$e0*$e0*$e0*$sbend_synrad]

SetReferenceEnergy $e0

Drift -name "TTA.MBEND3" -length 0 

Drift -name "TTA.D2M" -length 2 

Drift -name "TTA.EXICELL" -length 0 

Drift -name "TTA.EJECTION" -length 0 
Bpm -name "BPM242" -length 0
Drift -name "CHICANESTART" -length 0 
Drift -name "DLDG.D4" -length 1.59261465 
Quadrupole -name "DLDG.Q4" -synrad $quad_synrad -length 0.2 -strength [expr -0.2930570818*$e0] -e0 $e0 
Drift -name "DLDG.D3" -length 1.01854763 
Quadrupole -name "DLDG.Q3" -synrad $quad_synrad -length 0.2 -strength [expr 0.4552596096*$e0] -e0 $e0 
Drift -name "DLDG.D2" -length 5.26626489 
Quadrupole -name "DLDG.Q2" -synrad $quad_synrad -length 0.2 -strength [expr -0.3502967462*$e0] -e0 $e0 
Drift -name "DLDG.D1" -length 0.99977365 
Quadrupole -name "DLDG.Q1" -synrad $quad_synrad -length 0.2 -strength [expr 0.2935432288*$e0] -e0 $e0 
Drift -name "DLDG.D0" -length 0.83396146 
Bpm -name "BPMSTART" -length 0 
Sbend -name "BRB1A" -synrad $sbend_synrad -length 0.45 -angle -0.0599265 -E1 -0.02836160034 -E2 0 -six_dim 1 -e0 $e0 -fint 0.5 -fintx -1 
set e0 [expr $e0-14.1e-6*-0.0599265*-0.0599265/0.45*$e0*$e0*$e0*$e0*$sbend_synrad]
SetReferenceEnergy $e0
Sbend -name "BRB1B" -synrad $sbend_synrad -length 0.45 -angle -0.0599265 -E1 0 -E2 -0.02836160034 -six_dim 1 -e0 $e0 -fint 0.5 -fintx -1 
set e0 [expr $e0-14.1e-6*-0.0599265*-0.0599265/0.45*$e0*$e0*$e0*$e0*$sbend_synrad]
SetReferenceEnergy $e0
Drift -name "DG.L1" -length 1.8 
Quadrupole -name "QDDOG" -synrad $quad_synrad -length 0.1 -strength [expr 0.1539537815*$e0] -e0 $e0 
Drift -name "DG.L1" -length 1.8 
Sbend -name "BRB2A" -synrad $sbend_synrad -length 0.45 -angle 0.0599265 -E1 -0.02836160034 -E2 0 -six_dim 1 -e0 $e0 -fint 0.5 -fintx -1 
set e0 [expr $e0-14.1e-6*0.0599265*0.0599265/0.45*$e0*$e0*$e0*$e0*$sbend_synrad]
SetReferenceEnergy $e0
Sbend -name "BRB2B" -synrad $sbend_synrad -length 0.45 -angle 0.0599265 -E1 0 -E2 -0.02836160034 -six_dim 1 -e0 $e0 -fint 0.5 -fintx -1 
set e0 [expr $e0-14.1e-6*0.0599265*0.0599265/0.45*$e0*$e0*$e0*$e0*$sbend_synrad]
SetReferenceEnergy $e0
Drift -name "DG.L2" -length 0.3 
Quadrupole -name "QFDOG" -synrad $quad_synrad -length 0.1 -strength [expr -0.162804451*$e0] -e0 $e0 
Drift -name "DG.L3" -length 0.1 
Quadrupole -name "QDDOG2" -synrad $quad_synrad -length 0.1 -strength [expr 0.03547706207*$e0] -e0 $e0 
Drift -name "DG.L4" -length 0.1 
Drift -name "DG.L4" -length 0.1 
Quadrupole -name "QDDOG2" -synrad $quad_synrad -length 0.1 -strength [expr 0.03547706207*$e0] -e0 $e0 
Drift -name "DG.L3" -length 0.1 
Quadrupole -name "QFDOG" -synrad $quad_synrad -length 0.1 -strength [expr -0.162804451*$e0] -e0 $e0 
Drift -name "DG.L2" -length 0.3 
Sbend -name "BRB2B" -synrad $sbend_synrad -length 0.45 -angle 0.0599265 -E1 0 -E2 -0.02836160034 -six_dim 1 -e0 $e0 -fint 0.5 -fintx -1 
set e0 [expr $e0-14.1e-6*0.0599265*0.0599265/0.45*$e0*$e0*$e0*$e0*$sbend_synrad]
SetReferenceEnergy $e0
Sbend -name "BRB2A" -synrad $sbend_synrad -length 0.45 -angle 0.0599265 -E1 -0.02836160034 -E2 0 -six_dim 1 -e0 $e0 -fint 0.5 -fintx -1 
set e0 [expr $e0-14.1e-6*0.0599265*0.0599265/0.45*$e0*$e0*$e0*$e0*$sbend_synrad]
SetReferenceEnergy $e0
Drift -name "DG.L1" -length 1.8 
Quadrupole -name "QDDOG" -synrad $quad_synrad -length 0.1 -strength [expr 0.1539537815*$e0] -e0 $e0 
Drift -name "DG.L1" -length 1.8 
Sbend -name "BRB1B" -synrad $sbend_synrad -length 0.45 -angle -0.0599265 -E1 0 -E2 -0.02836160034 -six_dim 1 -e0 $e0 -fint 0.5 -fintx -1 
set e0 [expr $e0-14.1e-6*-0.0599265*-0.0599265/0.45*$e0*$e0*$e0*$e0*$sbend_synrad]
SetReferenceEnergy $e0
Sbend -name "BRB1A" -synrad $sbend_synrad -length 0.45 -angle -0.0599265 -E1 -0.02836160034 -E2 0 -six_dim 1 -e0 $e0 -fint 0.5 -fintx -1 
set e0 [expr $e0-14.1e-6*-0.0599265*-0.0599265/0.45*$e0*$e0*$e0*$e0*$sbend_synrad]
SetReferenceEnergy $e0
Bpm -name "BPMSTART" -length 0 
Bpm -name "BPMEND" -length 0 
Drift -name "CHICANEND" -length 0 
