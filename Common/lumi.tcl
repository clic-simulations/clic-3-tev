proc read_lumi {name} {
    exec ./gpv $name lumi_ee
    set f [open lumi_ee.dat r]
    for {set i 0} {$i<950} {incr i} {
        gets $f line
        gets $f line
    }
    set sum 0.0
    for {} {$i<1010} {incr i} {
        gets $f line
        set sum [expr $sum+[lindex $line 1]]
        gets $f line
    }
    close $f
    return [expr $sum*50*312.0*1e-4]
}
