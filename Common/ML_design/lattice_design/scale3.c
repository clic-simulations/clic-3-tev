#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#define PI 3.141592653589793

#define max(a,b) (((a)<(b))?(b):(a))

#include "brent.c"

mkquad(double rk,double rl,double r[])
{
    double sign1, sign2;
    int i;
    double rkabs, c1, c2, s1, s2, rksqrt;

    for (i=0;i<16;i++) {
      r[i]=0.0;
    }
    rkabs=fabs(rk);
    rksqrt=sqrt(rkabs);
    if (rk>0.0) {
	c1=cos(rksqrt*rl);
	s1=sin(rksqrt*rl);
	c2=cosh(rksqrt*rl);
	s2=sinh(rksqrt*rl);
	sign1=-1.0;
	sign2=1.0;
    } else {
	c1=cosh(rksqrt*rl);
	s1=sinh(rksqrt*rl);
	c2=cos(rksqrt*rl);
	s2=sin(rksqrt*rl);
	sign1=1.0;
	sign2=-1.0;
    }
    r[0]=c1;
    r[1]=s1/rksqrt;
    r[4]=s1*rksqrt*sign1;
    r[5]=c1;
    r[10]=c2;
    r[11]=s2/rksqrt;
    r[14]=s2*rksqrt*sign2;
    r[15]=c2;
}

mkdrift(double rl,double r[])
{
  int i;
  
  for (i=0;i<16;i++) {
    r[i]=0.0;
  }
  r[0]=1.0;
  r[1]=rl;
  r[5]=1.0;
  r[10]=1.0;
  r[11]=rl;
  r[15]=1.0;
}

mattransp(double r[])
{
  int i,j;
  double tmp;
  for (i=1;i<4;i++){
    for (j=0;j<i;j++){
      tmp=r[i*4+j];
      r[i*4+j]=r[i+4*j];
      r[i+4*j]=tmp;
    }
  }
}

matcopy(double r1[],double r2[])
{
  int i;
  for (i=0;i<16;i++){
    r2[i]=r1[i];
  }
}

matmul(double r1[],double r2[],double r3[])
{
  int i,j,k;
  double rhelp[16],sum;
  
  for (k=0;k<4;k++) {
    for (j=0;j<4;j++) {
      sum=0.0;
      for (i=0;i<4;i++) {
	sum += r1[i+k*4]*r2[j+4*i];
      }
      rhelp[j+4*k]=sum;
    }
  }
  for (j=0;j<16;j++) {
    r3[j]=rhelp[j];
  }
}

print_r(double r[])
{
  int i,j;
  for (j=0;j<4;j++){
    for (i=0;i<4;i++){
      printf("%g ",r[i+j*4]);
    }  
    printf("\n");
  }
  printf("\n");
}

int infodo(double rk1,double rl1,double rk2,double rl2,double d,
	   double *alpha1,double *alpha2,double *beta1,double *beta2,
	   double *rmu1,double *rmu2)
{
  double rtot[16],drift,rd[16],rq[16];
  double cos1, cos2;
  
  drift=d-0.5*(rl1+rl2);
  mkquad(rk2,rl2,rq);
  mkdrift(drift,rd);
  matmul(rq,rd,rtot);
  matmul(rd,rtot,rtot);
  mkquad(rk1,0.5*rl1,rq);
  matmul(rtot,rq,rtot);
  matmul(rq,rtot,rtot);
  cos1=0.5*(rtot[0]+rtot[5]);
  cos2=0.5*(rtot[10]+rtot[15]);
  if (cos1>1.0) {
    printf("error in infodo: abs(cos1)>1.0\n");
    return -1;
  }
  if (cos2>1.0) {
    printf("error in infodo: abs(cos2)>1.0\n");
    return -1;
  }
  *rmu1 = acos(cos1);
  *rmu2 = acos(cos2);
  if (rtot[1]<0.0) {
    *rmu1 = 2.0*PI-(*rmu1);
  }
  if (rtot[11]<0.0) {
    *rmu2 = 2.0*PI-(*rmu2);
  }
  if ((rtot[1]<0.0)||(rtot[11]<0.0)) return 1;
  *alpha1 = (rtot[0]-rtot[5])*0.5/sin(*rmu1);
  *alpha2 = (rtot[10]-rtot[15])*0.5/sin(*rmu2);
  *beta1 = rtot[1]/sin(*rmu1);
  *beta2 = rtot[11]/sin(*rmu2);
  return 0;
}

struct{
  double l,d;
  double betamin,betamax,wgt,max_val;
} struct_beta;

double f_select_beta(double k)
{
  double a1,a2,b1,b2,mu1,mu2;
  double ret,tmp,bmax,bmin;
  if (!infodo(k,struct_beta.l,-k,struct_beta.l,struct_beta.d,&a1,&a2,
	      &b1,&b2,&mu1,&mu2)){
    if (b1>b2){
      bmax=b1;
      bmin=b2;
    }
    else{ 
      bmax=b2;
      bmin=b1;
    }
    tmp=bmax-struct_beta.betamax;
    ret=tmp*tmp;
    tmp=bmin-struct_beta.betamin;
    ret+=struct_beta.wgt*tmp*tmp;
    return ret;
  }
  else{
    return struct_beta.max_val;
  }
}

double select_beta(double betamin,double betamax,double k0,double l,double d)
{
  double a,b,c,k,muf;
  double a1,a2,b1,b2,mu1,mu2;
  struct_beta.l=l;
  struct_beta.d=d;
  struct_beta.wgt=10.0;
  struct_beta.betamin=betamin;
  struct_beta.betamax=betamax;
  a=0.0;
  b=k0;
  c=2.0/(d*l);
  while(infodo(c,l,-c,l,d,&a1,&a2,&b1,&b2,&mu1,&mu2)) c*=0.99;
  b=0.5*(c+a);
  struct_beta.max_val=b1*b1+b2*b2;
  muf=brent(a,b,c,&f_select_beta,1e-20,&k);
  infodo(k,l,-k,l,d,&a1,&a2,&b1,&b2,&mu1,&mu2);
//  printf("%g %g %g %g\n",b2,betamin,b1,betamax);
  return mu1;
}

struct{
  double l,d;
  double mu0;
} struct_phase;

double f_select_phase(double k)
{
  double a1,a2,b1,b2,mu1,mu2;
  if (!infodo(k,struct_phase.l,-k,struct_phase.l,struct_phase.d,&a1,&a2,
	      &b1,&b2,&mu1,&mu2)){
    return (mu1-struct_phase.mu0)*(mu1-struct_phase.mu0);
  }
  else{
    if (k<=0.0){
      return struct_phase.mu0*struct_phase.mu0;
    }
    else{
      return (acos(-1.0)-struct_phase.mu0)*(acos(-1.0)-struct_phase.mu0);
    }
  }
}

double select_phase(double mu,double k0,double l,double d)
{
  double a,b,c,k,muf;
  double a1,a2,b1,b2,mu1,mu2;
  struct_phase.l=l;
  struct_phase.d=d;
  struct_phase.mu0=mu/180.0*acos(-1.0);
  a=1e-10;
  b=k0;
  c=2.0/(d*l);
  while(infodo(c,l,-c,l,d,&a1,&a2,&b1,&b2,&mu1,&mu2)) c*=0.99;
  b=0.5*(c+a);
  muf=brent(a,b,c,&f_select_phase,1e-20,&k);
  infodo(k,l,-k,l,d,&a1,&a2,&b1,&b2,&mu1,&mu2);
  /*  printf("%g %g %g\n",k,muf,mu1);*/
  return k;
}

double bns(double n,double l,double mu,double e)
{
  double tmp;
  tmp=tan(mu*acos(-1.0)/360.0);
  return n*1.6e-9*l*l*(1.0+1.5/(tmp*tmp))/48.0/e*1e6/3.332*878.0;
}

struct{
  double alpha,beta,e0,f0,l0;
} scale_data;

scale_set(double e0,double l0,double f0,double alpha,double beta)
{
  scale_data.e0=e0;
  scale_data.f0=f0;
  scale_data.l0=l0;
  scale_data.beta=beta;
  scale_data.alpha=alpha;
}

double l(double e)
{
  return scale_data.l0*pow(e/scale_data.e0,scale_data.alpha);
}

double f(double e)
{
  return scale_data.f0*pow(e/scale_data.e0,scale_data.beta);
}

double mu(double e)
{
  return 2.0*asin(0.5*l(e)/f(e));
}

main(int argc,char *argv[])
{
  double e=9.0,ef=1650.0,de=1.0,e_old,mu_g;
  int i;
  double lstep=2.01,dec=0.46,cavlen=0.46;
  double qmax=50.0,qlen[4]={0.35,0.85,1.35,1.85};

  int nc0=4,nc[4]={3,2,1,0};
  int kq=0,ng=1,kq_old,ng_old,k;
  double str,betamin,betamax;
  int scaling=1;
  int nsect=0;
  int nq[100];
  double kf[100];
  int ql[100],dl[100];
  FILE *file,*lattice;
  int nq_tot=0,ncount=0,ncav=0,ngirder=0;
  double l0,f0,alpha,beta,gradient;
  char *point;

  l0=strtod(argv[1],&point);
  f0=strtod(argv[2],&point);
  alpha=strtod(argv[3],&point);
  beta=strtod(argv[4],&point);
  scaling=strtol(argv[5],&point,10);
  gradient=strtod(argv[6],&point);
  dec=cavlen*gradient;

  scale_set(e,l0,f0,alpha,beta);
  kq_old=kq;
  ng=(int)(l(e)/lstep+0.5);
  ng_old=ng;
  k=0;
  e_old=e;
  while(e<ef){
      ng=(int)(l(e)/lstep+0.5);
      switch (scaling){
	  case 1:
	      mu_g=mu(0.5*(e+e_old));
	      break;
	  case 2:
	      mu_g=2.0*asin(ng_old*lstep/(2.0*f(0.5*(e+e_old))));
	      break;
	  case 3:
	      mu_g=2.0*asin(l(0.5*(e+e_old))/(2.0*f(0.5*(e+e_old))));
	      betamax=2.0*l(0.5*(e+e_old))/sin(mu_g)*(1.0+sin(0.5*mu_g));
	      betamin=2.0*l(0.5*(e+e_old))/sin(mu_g)*(1.0-sin(0.5*mu_g));
	      mu_g=select_beta(betamin,betamax,1e-6,qlen[kq_old],ng_old*lstep);
	      break;
      }
      mu_g*=180.0/PI;
      str=select_phase(mu_g,1e-8,qlen[kq_old],ng_old*lstep);
      if (qmax<str*e) kq++;
      if (kq>3) kq=3;
      if ((ng>ng_old)||(kq>kq_old)){
	  if (k>10){
	      printf("%g %g %g %d %d %d\n",e_old,e,str,ng_old,kq_old,2*k);
	      printf("%g %g max: %g %g\n",f(0.5*(e+e_old)),1.0/(str*qlen[kq_old]),str*e,mu_g);
	      nq[nsect]=2*k;
	      ql[nsect]=kq_old;
	      dl[nsect]=ng_old;
	      kf[nsect]=str;
	      nsect++;
	      e_old=e;
	      k=0;
	  }
	  kq_old=kq;
	  ng_old=ng;
      }
      /* use only FODO cells */
      de=2*((ng-1)*nc0+nc[kq])*dec;
      nq_tot+=2*(nc0-nc[kq]);
      ncount+=2;
      ncav+=2*((ng-1)*nc0+nc[kq]);
      ngirder+=2*ng;
      e+=de;
      k++;
  }
  switch (scaling){
      case 1:
	  mu_g=mu(0.5*(e+e_old));
	  break;
      case 2:
	  mu_g=2.0*asin(ng_old*lstep/(2.0*f(0.5*(e+e_old))));
	  break;
      case 3:
	  mu_g=2.0*asin(ng_old*lstep/(2.0*f(0.5*(e+e_old))));
	  betamax=2.0*l(e)/sin(mu_g)*(1.0+sin(0.5*mu_g));
	  betamin=2.0*l(e)/sin(mu_g)*(1.0-sin(0.5*mu_g));
	  mu_g=select_beta(betamin,betamax,1e-6,qlen[kq_old],ng_old*lstep);
	  break;
  }
  mu_g*=180.0/PI;
  str=select_phase(mu_g,1e-8,qlen[kq_old],ng_old*lstep);
  printf(">>%g %g %g %d %d %d %g\n",e_old,e,str,ng_old,kq_old,2*k,mu_g);
  nq[nsect]=2*k;
  ql[nsect]=kq_old;
  dl[nsect]=ng_old;
  kf[nsect]=str;
  nsect++;
  file=fopen("sectors.ini","w");
  lattice=fopen("lattice.ini","w");
  fprintf(file,"%d\n",nsect);
  fprintf(lattice,"%d\n",nsect);
  for (i=0;i<nsect;i++){
      fprintf(file,"%d %g %g %g %g %g\n",nq[i],qlen[ql[i]],kf[i],qlen[ql[i]],
	      -kf[i],dl[i]*lstep,nq[i]);
      fprintf(lattice,"%d %d %d\n",nq[i],ql[i]+1,dl[i]-1);
  }
  fclose(file);
  fclose(lattice);
  printf("number of quadrupoles %d\n",ncount);
  printf("sections for quadrupoles %d\n",nq_tot);
  printf("sections for cavities %d\n",ncav);
  printf("girders %d\n",ngirder);
  printf("fill factor %g\n",ncav*cavlen/(ngirder*lstep));
  exit(0);
}
