## Original lattice data set 
#set l0 1.5
#set f0 1.3
#set alpha 0.5
#set beta 0.5
#set scale 3
#set gradient 0.1
#exec ./scale $l0 $f0 $alpha $beta $scale $gradient 2000

## Continuously changing phase advance
#set l0 1.5
##set mu 72.0
##set f0 [expr ($l0+0.5)/2/sin($mu/2/180*3.1415)]
#set f0 1.3
#set alpha 0.46653
#set beta 0.5
#set scale 3
#set gradient 0.1
#exec ./scale $l0 $f0 $alpha $beta $scale $gradient 2000

# Change of phase advance in the end of the linac
set l0 1.5
set f0 1.3
set alpha 0.5
set beta 0.5
set scale 3
set gradient 0.1
set k_end -0.1
set max_scale_energy 1200
# Correspond to a removal of two sector
#set max_scale_energy 1000 
# Correspond to a removal of one sector
#set max_scale_energy 1100
puts "Scale" 
exec ./scale $l0 $f0 $alpha $beta $scale $gradient $max_scale_energy $k_end
puts "Match"
exec ./match
set f [open match.ini]
gets $f l
set bx [lindex $l 1]
set ax [lindex $l 2]
gets $f l
set by [lindex $l 1]
set ay [lindex $l 2]
close $f
set f [open lattice.ini r]
set f2 [open sectors.out r]
gets $f line
set n $line
set sectors {}
for {set i 0} {$i<$n} {incr i} {
    gets $f line
    gets $f2 line2
    lappend sectors $line
    lappend sectors $line2
}
close $f
close $f2
set f [open lattice.def w]
puts $f "\# Lattice scaling was $l0 $f0 $alpha $beta $scale"
puts $f ""
puts $f "variable sectors \{"
foreach x $sectors {
    puts $f "\"$x\""
}
puts $f "\}"
puts $f ""
puts $f "global set_match"
puts $f "array set set_match \{"
puts $f "    alpha_x $ax beta_x $bx alpha_y $ay beta_y $by"
puts $f "\}"
close $f
