set ch {1.0}

#
# old transverse wakefield
# s is given in micro metres
# return value is in V/pCm^2
#

array set structure {a 2e-3 g 1.0e-3 l 1.5e-3}
array set structure {a 2.25e-3 g 1.0e-3 l e-3}
array set structure {a 2.75e-3 g 7e-3 l 8.33333e-3 delta 0.145 delta_g 0.333e-3}

proc w_transv_old {s} {
    global structure
    set a $structure(a)
    return [expr 1e-12*4.0*377.0*3e8/acos(-1.0)/pow($a,4.0-1.46)*(1.0-(1.0+sqrt($s*1e-6/pow($a,1.46)))*exp(-sqrt($s*1e-6/pow($a,1.46))))]
}

#set wnorm [expr [w_transv_old 44.0]*3.72e9]

proc w_transv {s} {
    global structure
    set a $structure(a)
    set g $structure(g)
    set l $structure(l)

    set s0 [expr 0.169*pow($a,1.79)*pow($g,0.38)*pow($l,-1.17)]
    set tmp [expr 4.0*377.0*3e8*$s0*1e-12/(acos(-1.0)*pow($a,4))*(1.0-(1.0+sqrt($s*1e-6/$s0))*exp(-sqrt($s*1e-6/$s0)))]

    set s0 [expr 0.169*pow($a*(1.0+0.5*$structure(delta)),1.79)*pow($g,0.38)*pow($l,-1.17)]
    set tmp [expr $tmp+4.0*377.0*3e8*$s0*1e-12/(acos(-1.0)*pow($a,4))*(1.0-(1.0+sqrt($s*1e-6/$s0))*exp(-sqrt($s*1e-6/$s0)))]

    set s0 [expr 0.169*pow($a*(1.0-0.5*$structure(delta)),1.79)*pow($g,0.38)*pow($l,-1.17)]
    set tmp [expr $tmp+4.0*377.0*3e8*$s0*1e-12/(acos(-1.0)*pow($a,4))*(1.0-(1.0+sqrt($s*1e-6/$s0))*exp(-sqrt($s*1e-6/$s0)))]

    set s0 [expr 0.169*pow($a*(1.0-$structure(delta)),1.79)*pow($g,0.38)*pow($l,-1.17)]
    set tmp [expr $tmp+0.5*4.0*377.0*3e8*$s0*1e-12/(acos(-1.0)*pow($a,4))*(1.0-(1.0+sqrt($s*1e-6/$s0))*exp(-sqrt($s*1e-6/$s0)))]

    set s0 [expr 0.169*pow($a*(1.0+$structure(delta)),1.79)*pow($g,0.38)*pow($l,-1.17)]
    set tmp [expr $tmp+0.5*4.0*377.0*3e8*$s0*1e-12/(acos(-1.0)*pow($a,4))*(1.0-(1.0+sqrt($s*1e-6/$s0))*exp(-sqrt($s*1e-6/$s0)))]

    return [expr $tmp/4.0]
}

set wnorm [expr [w_transv 2*44.0]*3.72e9]

proc w_transv_x {s} {
    global structure
    set a $structure(a)
    set g $structure(g)
    set l $structure(l)
    set tmp [expr $g/$l]
    set alpha [expr 1.0-0.4648*sqrt($tmp)-(1.0-2.0*0.4648)*$tmp]
    set s0 [expr $g/8.0*pow($a/($l*$alpha),2)]
    return [expr 4.0*377.0*3e8*$s0*1e-12/(acos(-1.0)*pow($a,4))*(1.0-(1.0+sqrt($s*1e-6/$s0))*exp(-sqrt($s*1e-6/$s0)))]
}

proc w_transv_x {s} {
    global structure
    set g $structure(g)
    set l $structure(l)
    set tmp [expr $g/$l]
    set alpha [expr 1.0-0.4648*sqrt($tmp)-(1.0-2.0*0.4648)*$tmp]

    set a $structure(a)
    set s0 [expr $g/8.0*pow($a/($l*$alpha),2)]
    set tmp [expr 4.0*377.0*3e8*$s0*1e-12/(acos(-1.0)*pow($a,4))*(1.0-(1.0+sqrt($s*1e-6/$s0))*exp(-sqrt($s*1e-6/$s0)))]

    set a [expr $structure(a)*(1.0+0.5*$structure(delta))]
    set s0 [expr $g/8.0*pow($a/($l*$alpha),2)]
    set tmp [expr $tmp+4.0*377.0*3e8*$s0*1e-12/(acos(-1.0)*pow($a,4))*(1.0-(1.0+sqrt($s*1e-6/$s0))*exp(-sqrt($s*1e-6/$s0)))]

    set a [expr $structure(a)*(1.0-0.5*$structure(delta))]
    set s0 [expr $g/8.0*pow($a/($l*$alpha),2)]
    set tmp [expr $tmp+4.0*377.0*3e8*$s0*1e-12/(acos(-1.0)*pow($a,4))*(1.0-(1.0+sqrt($s*1e-6/$s0))*exp(-sqrt($s*1e-6/$s0)))]

    set a [expr $structure(a)*(1.0+$structure(delta))]
    set s0 [expr $g/8.0*pow($a/($l*$alpha),2)]
    set tmp [expr $tmp+0.5*4.0*377.0*3e8*$s0*1e-12/(acos(-1.0)*pow($a,4))*(1.0-(1.0+sqrt($s*1e-6/$s0))*exp(-sqrt($s*1e-6/$s0)))]

    set a [expr $structure(a)*(1.0-$structure(delta))]
    set s0 [expr $g/8.0*pow($a/($l*$alpha),2)]
    set tmp [expr $tmp+0.5*4.0*377.0*3e8*$s0*1e-12/(acos(-1.0)*pow($a,4))*(1.0-(1.0+sqrt($s*1e-6/$s0))*exp(-sqrt($s*1e-6/$s0)))]

    return [expr $tmp/4.0]
}

proc w_transv_x {s} {
    global structure
    set g $structure(g)
    set l $structure(l)
    set tmp [expr $g/$l]
    set alpha [expr 1.0-0.4648*sqrt($tmp)-(1.0-2.0*0.4648)*$tmp]

    set a $structure(a)
    set s0 [expr $g/8.0*pow($a/($g*$alpha),2)]
    set tmp [expr 4.0*377.0*3e8*$s0*1e-12/(acos(-1.0)*pow($a,4))*(1.0-(1.0+sqrt($s*1e-6/$s0))*exp(-sqrt($s*1e-6/$s0)))]

    set a [expr $structure(a)*(1.0+0.5*$structure(delta))]
    set s0 [expr $g/8.0*pow($a/($g*$alpha),2)]
    set tmp [expr $tmp+4.0*377.0*3e8*$s0*1e-12/(acos(-1.0)*pow($a,4))*(1.0-(1.0+sqrt($s*1e-6/$s0))*exp(-sqrt($s*1e-6/$s0)))]

    set a [expr $structure(a)*(1.0-0.5*$structure(delta))]
    set s0 [expr $g/8.0*pow($a/($g*$alpha),2)]
    set tmp [expr $tmp+4.0*377.0*3e8*$s0*1e-12/(acos(-1.0)*pow($a,4))*(1.0-(1.0+sqrt($s*1e-6/$s0))*exp(-sqrt($s*1e-6/$s0)))]

    set a [expr $structure(a)*(1.0+$structure(delta))]
    set s0 [expr $g/8.0*pow($a/($g*$alpha),2)]
    set tmp [expr $tmp+0.5*4.0*377.0*3e8*$s0*1e-12/(acos(-1.0)*pow($a,4))*(1.0-(1.0+sqrt($s*1e-6/$s0))*exp(-sqrt($s*1e-6/$s0)))]

    set a [expr $structure(a)*(1.0-$structure(delta))]
    set s0 [expr $g/8.0*pow($a/($g*$alpha),2)]
    set tmp [expr $tmp+0.5*4.0*377.0*3e8*$s0*1e-12/(acos(-1.0)*pow($a,4))*(1.0-(1.0+sqrt($s*1e-6/$s0))*exp(-sqrt($s*1e-6/$s0)))]

    return [expr $tmp/4.0]
}

#
# longitudinal wakefield
# s is given in micro metres
# return value is in V/pCm
#

proc w_long {s} {
    global structure
    set g $structure(g)
    set l $structure(l)
    set tmp [expr $g/$l]
    set alpha [expr 1.0-0.4648*sqrt($tmp)-(1.0-2.0*0.4648)*$tmp]

    set a $structure(a)
    set s0 [expr $g/8.0*pow($a/($l*$alpha),2)]
    set tmp [expr 377.0*3e8*1e-12/(acos(-1.0)*$a*$a)*exp(-sqrt($s*1e-6/$s0))]

    set a [expr $structure(a)*(1.0-0.5*$structure(delta))]
    set s0 [expr $g/8.0*pow($a/($l*$alpha),2)]
    set tmp [expr $tmp+377.0*3e8*1e-12/(acos(-1.0)*$a*$a)*exp(-sqrt($s*1e-6/$s0))]

    set a [expr $structure(a)*(1.0+0.5*$structure(delta))]
    set s0 [expr $g/8.0*pow($a/($l*$alpha),2)]
    set tmp [expr $tmp+377.0*3e8*1e-12/(acos(-1.0)*$a*$a)*exp(-sqrt($s*1e-6/$s0))]

    set a [expr $structure(a)*(1.0-$structure(delta))]
    set s0 [expr $g/8.0*pow($a/($l*$alpha),2)]
    set tmp [expr $tmp+0.5*377.0*3e8*1e-12/(acos(-1.0)*$a*$a)*exp(-sqrt($s*1e-6/$s0))]

    set a [expr $structure(a)*(1.0+$structure(delta))]
    set s0 [expr $g/8.0*pow($a/($l*$alpha),2)]
    set tmp [expr $tmp+0.5*377.0*3e8*1e-12/(acos(-1.0)*$a*$a)*exp(-sqrt($s*1e-6/$s0))]

    return [expr $tmp/4.0]
}

proc w_long_x {s} {
    global structure
    set g $structure(g)
    set l $structure(l)
    set tmp [expr $g/$l]
    set alpha [expr 1.0-0.4648*sqrt($tmp)-(1.0-2.0*0.4648)*$tmp]

    set a $structure(a)
    set s0 [expr $g/8.0*pow($a/($g*$alpha),2)]
    set tmp [expr 377.0*3e8*1e-12/(acos(-1.0)*$a*$a)*exp(-sqrt($s*1e-6/$s0))]

    set a [expr $structure(a)*(1.0-0.5*$structure(delta))]
    set s0 [expr $g/8.0*pow($a/($g*$alpha),2)]
    set tmp [expr $tmp+377.0*3e8*1e-12/(acos(-1.0)*$a*$a)*exp(-sqrt($s*1e-6/$s0))]

    set a [expr $structure(a)*(1.0+0.5*$structure(delta))]
    set s0 [expr $g/8.0*pow($a/($g*$alpha),2)]
    set tmp [expr $tmp+377.0*3e8*1e-12/(acos(-1.0)*$a*$a)*exp(-sqrt($s*1e-6/$s0))]

    set a [expr $structure(a)*(1.0-$structure(delta))]
    set s0 [expr $g/8.0*pow($a/($g*$alpha),2)]
    set tmp [expr $tmp+0.5*377.0*3e8*1e-12/(acos(-1.0)*$a*$a)*exp(-sqrt($s*1e-6/$s0))]

    set a [expr $structure(a)*(1.0+$structure(delta))]
    set s0 [expr $g/8.0*pow($a/($g*$alpha),2)]
    set tmp [expr $tmp+0.5*377.0*3e8*1e-12/(acos(-1.0)*$a*$a)*exp(-sqrt($s*1e-6/$s0))]

    return [expr $tmp/4.0]
}

proc w_long {s} {
    global structure
    set g $structure(g)
    set l $structure(l)


    set a $structure(a)
    set s0 [expr 0.169*pow($a,1.79)*pow($g,0.38)*pow($l,-1.17)]
    set tmp [expr 377.0*3e8*1e-12/(acos(-1.0)*$a*$a)*exp(-sqrt($s*1e-6/$s0))]

    set a [expr $structure(a)*(1.0+0.5*$structure(delta))]
    set s0 [expr 0.169*pow($a,1.79)*pow($g,0.38)*pow($l,-1.17)]
    set tmp [expr $tmp+377.0*3e8*1e-12/(acos(-1.0)*$a*$a)*exp(-sqrt($s*1e-6/$s0))]

    set a [expr $structure(a)*(1.0-0.5*$structure(delta))]
    set s0 [expr 0.169*pow($a,1.79)*pow($g,0.38)*pow($l,-1.17)]
    set tmp [expr $tmp+377.0*3e8*1e-12/(acos(-1.0)*$a*$a)*exp(-sqrt($s*1e-6/$s0))]

    set a [expr $structure(a)*(1.0+$structure(delta))]
    set s0 [expr 0.169*pow($a,1.79)*pow($g,0.38)*pow($l,-1.17)]
    set tmp [expr $tmp+0.5*377.0*3e8*1e-12/(acos(-1.0)*$a*$a)*exp(-sqrt($s*1e-6/$s0))]

    set a [expr $structure(a)*(1.0-$structure(delta))]
    set s0 [expr 0.169*pow($a,1.79)*pow($g,0.38)*pow($l,-1.17)]
    set tmp [expr $tmp+0.5*377.0*3e8*1e-12/(acos(-1.0)*$a*$a)*exp(-sqrt($s*1e-6/$s0))]

    return [expr $tmp/4.0]
}

proc w_long_x {s} {
    global structure
    set a $structure(a)
    set g $structure(g)
    set l $structure(l)
    set tmp [expr $g/$l]
    set alpha [expr 1.0-0.4648*sqrt($tmp)-(1.0-2.0*0.4648)*$tmp]
    set s0 [expr $g/8.0*pow($a/($l*$alpha),2)]
    return [expr 377.0*3e8*1e-12/(acos(-1.0)*$a*$a)*exp(-sqrt($s*1e-6/$s0))]
}

proc calc_new_long {charge z_list} {
    set z_l {}
    set n [llength $z_list]
    set tmp {}
    foreach l $z_list {
	set z0 [lindex $l 0]
	set wgt0 [expr $charge*[lindex $l 1]]
	set sum 0.0
	foreach j $z_l {
	    set z [lindex $j 0]
	    set wgt [expr [lindex $j 1]*$charge]
	    set sum [expr $sum+$wgt*[w_long [expr $z0-$z]]]
	}
	set sum [expr $sum+0.5*$wgt0*[w_long 0.0]]
	lappend z_l $l
#
# multiply with factor for pC and MV
#
	set sum [expr -($sum*1.6e-7*1e-6)]
	lappend tmp "$z0 $sum"
    }
    return $tmp
}

proc calc {charge a b sigma n} {
    set long [calc_new_long $charge [GaussList -min $a -max $b -sigma $sigma -charge 1.0 -n_slices [expr $n]]]
    set z_list [GaussList -min $a -max $b -sigma $sigma -charge 1.0 -n_slices [expr $n]]
    set res ""
    for {set i 0} {$i<$n} {incr i} {
	set x [lindex $long [expr $i]]
	lappend res "[lindex $z_list $i] [lindex $x 1]"
    }
    return $res
}

set pi [expr acos(-1.0)]
