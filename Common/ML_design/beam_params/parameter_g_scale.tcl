#
# Load wakefield functions
#

source wake.tcl

#
# Define the scale of the transverse wake
#

#set wakescale 1.0
set wakescale [lindex $argv 0]

#
# Define the gradient in MV/m
#

#set gradient [lindex $argv 0]
set gradient 50
#set gradient 100

#set wakescale [expr $wakescale*sqrt($gradient/100.0)]
set wakescale [expr $wakescale*$gradient/100.0]

#
# Choice of Structure
#

set dummy {
}

#set fin [open alexei.in r]
set fin [open clic_ml_structure.in r]

gets $fin line

while {![eof $fin]} {

    set f [lindex $line 0]
    set a [lindex $line 1]
    set delta [lindex $line 2]
    gets $fin line

    set g 2.8333e-3
    set l 3.3333e-3
#    set g 1.17e-3
#    set l 1.67e-3
    set lambda 0.01
    
    set g [expr $g*30.0/$f]
    set l [expr $l*30.0/$f]
    set a [expr $a*30.0/$f*$lambda]
    
    set lambda [expr $lambda*30.0/$f]
    puts "lambda $lambda"
    
    set k [expr 2.0*$pi/$lambda]

    array set structure "a $a g $g l $l delta $delta"
    
    #
    # Loop over all charges n to find for each the bunch length
    #
    
    set bspread ""
    set bspread2 ""
    
    #
    # Define the spline with the wakefields for this structure
    #
    
    for {set sigma 1.0} {$sigma<3000} {set sigma [expr $sigma*1.025]} {
	lappend ls "[expr [w_transv [expr 2.0*$sigma]]/$wnorm] $sigma"
    }
    SplineCreate sp$a$f$delta $ls
    
    set fc 1.2
    for {set n 20e9} {$n>5e8} {set n [expr $n/$fc]} {
	
	set ls ""
	
	#
	# Find bunch length yielding the same transverse wake field effect
	#
	
	set sigma [sp$a$f$delta [expr $wakescale/$n]]
	
	#
	# Find longitudinal effect corresponding to this bunch length
	#
	
	set tmp [calc $n -3.0 3.0 $sigma 51]
	set sum 0.0
	set sum2 0.0
	set wgt 0.0
	foreach x $tmp {
	    set de [expr [lindex $x 2]+\
			0.9*$gradient*cos(-12.0/180.0*$pi+$k*[lindex $x 0]*1e-6)]
	    set sum [expr $sum+[lindex $x 1]*$de]
	    set wgt [expr $wgt+[lindex $x 1]]
	}
	set sum [expr $sum/$wgt]
	foreach x $tmp {
	    set de [expr [lindex $x 2]+\
			0.9*$gradient*cos(-12.0/180.0*$pi+$k*[lindex $x 0]*1e-6)-$sum]
	    set sum2 [expr $sum2+[lindex $x 1]*$de*$de]
	}
	set sum2 [expr sqrt($sum2/$wgt)/$sum]
	
	lappend bspread "$sum2 $n"
	lappend bspread2 "$sum2 $sigma"
	
	if {$sum2<0.03} {
	    set fc 1.05
	}
	if {$sum2<0.01} {
	    set fc 1.01
	}
	if {$sum2<0.003} {
	    break
	}
    }
    
    SplineCreate bspread$a$f$delta $bspread
    SplineCreate bspread2$a$f$delta $bspread2
    
    puts "$f [expr $a/$lambda] $delta [bspread2$a$f$delta [lindex $argv 1]] [expr 1e-10*[bspread$a$f$delta [lindex $argv 1]]] $g $l"
    flush stdout
}

close $fin

return
#
#
#

exec mkdir -p $gradient
cd $gradient
exec cp ../alexei.$gradient .

proc run_case {e0 n sz scalex scaley name} {

    set epsx [expr 1.2*$n/0.68]
    if {$epsx<0.66} {set epsx 0.66}

    set f [open acc.dat w]
    puts $f "
\$ACCELERATOR:: clic
\{energy=$e0;particles=$n;beta_x=8.0*$scalex*$scalex;beta_y=0.1*$scaley*$scaley;emitt_x=$epsx;
emitt_y=0.025;sigma_z=$sz;\}
"
    puts $f "
\$PARAMETERS:: clic
\{n_x=33;n_y=63;n_z=31;n_t=1;n_m=50000;cut_x=3.0*sigma_x.1;do_eloss=1;
cut_y=6.0*sigma_y.1;cut_z=3.0*sigma_z.1;pair_ecut=0e-3;offset_y=0.0;
do_photons=0;do_coherent=0;charge_sign=-1;num_lumi=0;
force_symmetric=0;electron_ratio=0.2;photon_ratio=0.2;do_lumi=0;lumi_p=1e-29;
hist_ee_min=0.0;hist_ee_max=2.00001*energy.1;hist_ee_bins=100;\}
"
    close $f
    puts "go"
    exec ~/code/gp/guinea clic clic $name
}

proc read_lumi {name} {
    exec ~/code/gp/gpv $name lumi_ee
    set f [open lumi_ee.dat r]
    set suma 0.0
    set sum 0.0
    for {set i 0} {$i<100} {incr i} {
	gets $f line
	gets $f line
	set suma [expr $suma+[lindex $line 1]]
    }
    set sum [lindex $line 1]
    close $f
    return "$suma $sum"
}

set e [expr 0.5*350]
set fin [open alexei.$gradient r]
set fout [open alexei.res.$gradient w]
gets $fin line
while {![eof $fin]} {
    set fr [lindex $line 0]
    set a [lindex $line 1]
    set delta [lindex $line 2]
    set sz [lindex $line 3]
    set ch [lindex $line 4]
    set g [lindex $line 5]
    set ll [lindex $line 6]
    set n [expr $ch]
    set name dummy
    set f [open newnew.$fr.$a.$ll.$g a]
    set rat 0.0
    for {set scalex 1.0} {$rat<0.6} {set scalex [expr $scalex*1.05]} {
	run_case $e $n $sz $scalex 1.0 $name
	set tmp [read_lumi $name]
	set rat [expr [lindex $tmp 1]/[lindex $tmp 0]] 
	puts $f "$scalex $tmp"
	flush $f
	puts "done"
    }

    set lfirst [lindex $tmp 1]
    set lold 0.0
    set lnew [lindex $tmp 1]

    for {set scaley 1.05} {$lnew>$lold} {set scaley [expr $scaley*1.05]} {
	set lold $lnew
	set tmpold $tmp
	run_case $e $n $sz $scalex $scaley $name
	set tmp [read_lumi $name]
	set rat [expr [lindex $tmp 1]/[lindex $tmp 0]] 
	puts $f "$scalex $tmp"
	flush $f
	set lnew [lindex $tmp 1]
	puts "OK"
    }
    close $f
    puts $fout "$gradient $fr $a [expr $ch*10.0] $sz $ll $g $tmpold $rat"
    flush $fout
    gets $fin line
}
close $fin
close $fout
