#global set_match

set temp [file dirname [info script]]
source $temp/../clic_beam_pars.tcl

#set match(alpha_x) -3.03147e-17
#set match(alpha_y) 6.06295e-17
#set match(beta_x) 2.02111
#set match(beta_y) 6.68204
# Set the beam properties (match) according to the matching conditions of the lattice (set_match)
#set match(alpha_x) $set_match(alpha_y)
#set match(alpha_y) $set_match(alpha_x)
#set match(beta_x) $set_match(beta_y)
#set match(beta_y) $set_match(beta_x)
