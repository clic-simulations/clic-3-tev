# this file contains a set of tcl procedures, mainly manipulating the tcl environment.

# calculate the power of base**p
proc power {base p} { 
    set result 1
    while {$p > 0} { 
	set result [expr $result*$base] 
	set p [expr $p-1]
    }
    return $result
}

# procedure to define const variables, 'unset' still works
proc const {name value} {
    uplevel 1 [list set $name $value]
    uplevel 1 [list trace var $name w {error constant ;#} ]
    uplevel 1 [list trace var $name w "set $name [list $value];#" ]
}

# make array const
proc const_array {arrayname} {
    uplevel 1 "set arrayname $arrayname"
    uplevel 1 {
	foreach {key} [array names $arrayname] {
	    array set myArray [array get $arrayname]
	    const ${arrayname}($key) $myArray($key)
	}
    }
}

# count lines in a file
proc linecount {file} {
    set i 0
    set fh [open $file r]
    while {[gets $fh line] > -1} {incr i}
    close $fh
    return $i
}

# save array to file
proc save_array {array0 arrayname {octave 1} {octave_extension _tcl}} {
    upvar 1 $array0 array1
    set fh [open $arrayname.dat w]
    puts $fh [list array set $arrayname [array get array1]]
    close $fh

    if {$octave} {
	set fo [open $arrayname.m w]
	#puts $array1
	#array set myArray $array1
	foreach element [array names array1] {
	    puts $fo "$element${octave_extension} =  $array1($element);"
	}
	close $fo
    }
}

# read array from file
proc read_array {arrayname} {
    uplevel 1 "set arrayname $arrayname"
    uplevel 1 {
	source $arrayname.dat
    }
}

# print elements of an array
proc show_array {arrayName arrayList} {
    upvar $arrayName myArrayName
    array set myArray $arrayList

    foreach element [array names myArray] {
	puts "${arrayName}($element) =  $myArray($element)"
    }
}

# export tcl array into octave variables
proc export_array_2_octave { arrayname {extension _tcl} } {
    upvar $arrayname array1
    foreach {key} [array names array1] {
	Octave {
	    ${key}${extension}=str2double(Tcl_GetVar('array1','$key'));
	    if (isnan(${key}${extension}))
	      ${key}${extension}=Tcl_GetVar('array1','$key');
	    end
	}
    }
}

# export tcl array into octave variables 
# for exporting of environment to octave, which somehow does not work with above function
proc export_array_2_octave_env { arrayname arraylist {extension _tcl} } {
    upvar 1 $arrayname myarrayname
    array set array1 $arraylist
    foreach {key} [array names array1] {
	Octave {
	    temp_var = str2double(Tcl_GetVar('array1','$key'));
	    if (isnan(temp_var))
	        global ${myarrayname}_${key}${extension}=Tcl_GetVar('array1','$key');
	    else
	        global ${myarrayname}_${key}${extension}=temp_var;
	    end
	}
    }
}

# export tcl environment into octave variables
proc export_env_2_octave { {extension "_tcl"} } {
    if {$extension==""} {
	uplevel 1 "set extension \"\" "
    } else {
	uplevel 1 "set extension $extension"
    }
    uplevel 1 {
	set not_exported_vars {auto_index env not_exported_vars v}
	# loop over all variables
	foreach v [info vars] {
	    # check if variable is not in list that should not be exported
	    if {[lsearch $not_exported_vars $v] < 0} {
		# if variable is an array
		if [array exists $v] {
		    export_array_2_octave_env v [array get $v] $extension
		} else {
		    # if variable is a list (assume all numbers for now)
		    # list with strings are not supported nor wanted
		    set not_exported_lists {tracking_output_string not_exported_lists auto_path timestamp argv}
		    set varvalue [set $v]
		    if {[llength $varvalue]>1 && [lsearch $not_exported_lists $v]<0} {
			set listsize [llength $varvalue]
			# initialise array
			Octave {
			    global ${v}${extension} = zeros(1,$listsize);
			}
			for {set i 0} {$i<$listsize} {incr i} {
			    set varitem [lindex $varvalue $i]
			    set j [expr $i+1]
			    # check if element in string is a number/double (again strings are not supported)
			    if [string is double $varitem] {
				Octave {
				    ${v}${extension}($j) = $varitem;
				}
			    }
			}
			unset i j listsize varvalue varitem not_exported_lists
		    } else {
			Octave {
			    # try to convert to double
			    temp_var = str2double(Tcl_GetVar('$v'));
			    # if not possible, then it is a string (Tcl has no built-in string test)
			    if (any(isnan(temp_var)))
			    global ${v}${extension} = Tcl_GetVar('$v');
			    else
			    global ${v}${extension} = temp_var;
			    end
			}
		    }
		}
	    }
	}
	unset extension v not_exported_vars
    }
}

# fix tcl environment (can only be done once)
proc const_env {} {
    uplevel 1 {
	foreach var [info vars] {
	    # errorCode and errorInfo should not be const
	    # other environment variables can be const, but problem with auto_index
	    if {$var != "errorCode" && $var != "errorInfo" && $var != "auto_index"} { 
		if [array exists $var] {
		    const_array $var
		} else {
		    const $var [set $var]
		}
	    }
	}
	unset key myArray var
    }
}

# save all vars and arrays
proc save_state {filename} {
    uplevel 1 "set filename $filename"
    uplevel 1 {
	set fh [open ${filename}_tmp w]
	set not_saved_vars {auto_index auto_oldpath auto_path common_dir common_dir_machine data_dir dir_name env fh errorCode errorInfo lattice_dir load_machine_electron load_machine_positron not_saved_vars script_dir tcl_interactive tcl_libPath tcl_library tcl_pkgPath tcl_patchLevel tcl_platform tcl_version v}
        foreach v [info vars] {
	    if {[lsearch $not_saved_vars $v] < 0} {
		if [array exists $v] {
		    puts $fh [list array set $v [array get $v]]
		} else {
		    puts $fh [list set $v [set $v]]
		}
            }
        }
        close $fh
	# sort alphabetically with Unix sort
	exec sort ${filename}_tmp > $filename
	# delete tmp file
	file delete ${filename}_tmp
	unset fh v
    }
}

# restore variables
proc restore_state {filename} {
    uplevel 1 "set filename $filename"
    uplevel 1 {
        source $filename
    }
}

# source file and issue warning if variable does not exist
# known issue: not able to deal with lists
proc check_file {filename} {
    uplevel 1 "set filename $filename"
    uplevel 1 {
	puts "$filename"
	set fh [open $filename r]
	while {[gets $fh line] > -1} {
	    # skip empty lines or comments
	    if {[string is space $line] || [string index $line 0]== "#"} {
		continue
	    }
	    # split line into a tcl list:
	    set tcl_list [split $line " "]
	    # compare first argument to "set"
	    # don't do anything for now as more elaborate tcl code might be in the file
	    if {[llength $tcl_list]<3 || [string compare "set" [lindex $tcl_list 0]] != 0} {
		#puts "WARNING input $tcl_list has not the correct format in $filename"
		#exit;
		continue
	    }
	    set temp [lindex $tcl_list 1]
	    if {[info exists $temp]==0} {
		puts "WARNING new variable introduced in private file: $temp (typo?)"
	    }
	    #set $temp [lindex $tcl_list 2]
	    unset temp tcl_list
	}
	unset line
    }
}

# source file and issue warning if variable does not exist
# known issue: not able to deal with lists
proc read_variable_from_file {filename var_name} {
    set var_value ""
    set fh [open $filename r]
    while {[gets $fh line] > -1} {
	# skip empty lines or comments
	if {[string is space $line] || [string index $line 0]== "#"} {
	    continue
	}
	# split line into a tcl list:
	set tcl_list [split $line " "]
	# compare first argument to "set"
	# don't do anything for now as more elaborate tcl code might be in the file
	if {[llength $tcl_list]<3 || [string compare "set" [lindex $tcl_list 0]] != 0} {
	    continue
	}
	set temp [lindex $tcl_list 1]
	if {$temp == $var_name} {
	    set var_value [lindex $tcl_list 2]
	    set var_value [lindex $var_value 0]
	}
    }
    #set var_value [lindex $var_value 0]
    return $var_value
}
