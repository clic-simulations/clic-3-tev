% *************************************************************************
% Frequency Maps plots (Woks with Matlab)
%
% Function FreqMaps(sx,sy,Qxint,Qyint)
% 
% Arguments:    sx, sy -> Horizontal and vertical Beam sizes
%               Qxint, Qyint -> Integer parts of the tunes
%
% November 2009
% *************************************************************************

%function D = FreqMaps(sx,sy,Qxint,Qyint)
ls
clear all
A=textread(input('Which File do you want to analyze? ','s'));

%sx=0.004;
%sy=0.002;

%sx=input('\sigma_x [mm]: ')/1000;
%sy=input('\sigma_y [mm]: ')/1000;

%Qxint=input('Integer part of Qx: ');
%Qyint=input('Integer part of Qy: ');

Params=importdata('Params.dat');
sx=Params.data(1);
sy=Params.data(2);
Qxint=double(int16(Params.data(3)));
Qyint=double(int16(Params.data(4)));

xx=A(:,3);
yy=A(:,5);
wx=Qxint+abs(A(:,11));
wy=Qyint+abs(A(:,12));
wx1=abs(A(:,13));
wy1=abs(A(:,14));
wx2=abs(A(:,15));
wy2=abs(A(:,16));

D=log(sqrt((wx1-wx2).^2+(wy1-wy2).^2));

D((wx==Qxint) & (wy==Qyint))=0;
ns=input('Geometrical acceptance in \sigmas: ');
xp=-ns^2:0.1:ns^2;
yp=0:0.1:ns^2;
x=repmat(xp',1,length(yp));
y=repmat(yp,length(xp),1);

mm=abs(x+y)==ns^2;

f1=figure;
hold all
box on
scatter(xx(D<0)/sx,yy(D<0)/sy,25,D(D<0),'filled','s')
plot(sqrt(x(mm)),sqrt(y(mm)),'.k')
xlabel('x/\sigma_x','FontSize',16)
ylabel('y/\sigma_y','FontSize',16)
caxis([-15 0])
c=colorbar;
set(gca,'FontName','Times','FontSize',16);
ylabel(c,'D')
axis tight
%saveas(f1,input('Give a name to the DA plot: ','s'),'epsc')

%open resonances2.fig
f2=figure;
hold all
box on
%resonl_systematic(Qxint,Qyint,input('Resonance Order: '),2);
resonl(Qxint,Qyint,input('Resonance Order: '),2);
scatter(wx(D<0),wy(D<0),15,D(D<0),'filled','s')
xlabel('Q_x','FontSize',16)
ylabel('Q_y','FontSize',16)
caxis([-15 0])
c=colorbar;
set(gca,'FontName','Times','FontSize',16);
ylabel(c,'D')

mm=wx>Qxint+0.1&wx<Qxint+1&wy>Qyint+0.1&wy<Qyint+1;
xmax=max(wx(mm));
xmin=min(wx(mm));
ymax=max(wy(mm));
ymin=min(wy(mm));

%resonl_systematic(Qxint,Qyint,input('Resonance Order: '),2);
axis([xmin-0.1 xmax+0.1 ymin-0.1 ymax+0.1])
%saveas(gcf,input('Give a name to the Frequency map: ','s'),'epsc')
