#!/bin/bash

BATCHSUB_DIR=""
GLOBAL_RESULTS_DIR=""

sequence_file="DR_matching.seq"

npoint=`wc -l tracking_pos.dat | awk '{print $1}' `
let "npoint -=4"
#change here he number of tracking by job
npoint_calc=30
pointini=1

if [ -z "$BATCHSUB_DIR" ]; then
    echo "BATCHSUB_DIR variable not defined. Edit $0 file"
    exit;
fi
if [ -z "$GLOBAL_RESULTS_DIR" ]; then
    echo "GLOBAL_RESULTS_DIR variable not defined. Edit $0 file"
    exit;
fi

#**************<ERRORS PART>********************
#Wigglers
dxpfieldint=3e-5;
#3e-5[Tm]
dxfieldint=4e-4;
#4e-4[Tm²]
dypfieldint=3e-6;
#3e-6[Tm]
dyfieldint=1e-5;
#1e-5[Tm²]

#Quad positions
deltax=5e-6;
deltay=5e-6;

#Relative strength errors
dkn=1e-4
# relative error on quadrupole field
dks=1e-5
#skew relative error on quadrupole field
dsn=1e-4
#sextupole relative error term
dss=1e-5
#sextupole skew relative error term

#Off momentum
dp=-0.005
#**************</ERRORS PART>********************


#RESULT_DIR determination
RESULTS_DIR=$GLOBAL_RESULTS_DIR
if [[ $dp != 0 ]]
then
    RESULTS_DIR=$RESULTS_DIR"dp${dp}_"
fi
if [[ $dxpfieldint != 0 ]]
then
    RESULTS_DIR=$RESULTS_DIR"dxpwig${dxpfieldint}Tm_"
fi
if [[ $dxfieldint != 0 ]]
then
    RESULTS_DIR=$RESULTS_DIR"dxwig${dxfieldint}Tm2_"
fi
if [[ $dypfieldint != 0 ]]
then
    RESULTS_DIR=$RESULTS_DIR"dypwig${dypfieldint}Tm_"
fi
if [[ $dyfieldint != 0 ]]
then
    RESULTS_DIR=$RESULTS_DIR"dywig${dyfieldint}Tm2_"
fi
if [[ $deltax != 0 ]]
then
    RESULTS_DIR=$RESULTS_DIR"dx$( echo 1|awk '{print '$deltax' * 1e6}' )um_"
fi
if [[ $deltay != 0 ]]
then
    RESULTS_DIR=$RESULTS_DIR"dy$( echo 1|awk '{print '$deltay' * 1e6}' )um_"
fi
if [[ $dkn != 0 ]]
then
    RESULTS_DIR=$RESULTS_DIR"dKn${dkn}_"
fi
if [[ $dks != 0 ]]
then
    RESULTS_DIR=$RESULTS_DIR"dKs${dks}_"
fi
if [[ $dsn != 0 ]]
then
    RESULTS_DIR=$RESULTS_DIR"dSn${dsn}_"
fi
if [[ $dss != 0 ]]
then
    RESULTS_DIR=$RESULTS_DIR"dSs${dss}_"
fi
if [[ $RESULTS_DIR == $GLOBAL_RESULTS_DIR ]];
then
    RESULTS_DIR=$RESULTS_DIR"nominal"
else
    RESULTS_DIR=${RESULTS_DIR%_}
fi

echo "*******************************************************"
echo "Parameters chosen :"
echo "Latice : $sequence_file"
echo "Results directory : $RESULTS_DIR"
echo "* Momentum deviation"
echo "    DP = $dp"
echo "* Wiglers :"
echo "    dxp_fieldint = $dxpfieldint Tm"
echo "    dx_fieldint = $dxfieldint Tm2"
echo "    dyp_fieldint = $dypfieldint Tm"
echo "    dy_fieldint = $dyfieldint Tm2"
echo "* Quads :"
echo "    deltax = $( echo 1|awk '{print '$deltax' * 1e6}' ) um" 
echo "    deltay = $( echo 1|awk '{print '$deltay' * 1e6}' ) um"
echo "    dKn = $dkn	dKs = $dks"
echo "    dSn = $dsn	dSs = $dss"
echo "*******************************************************"
if [[ $[ $pointini / $npoint_calc * $npoint_calc ] == $pointini ]];
then
	job=$[ $pointini / $npoint_calc ]
else
	job=$[ $pointini / $npoint_calc +1 ]
fi
if [[ $[ $npoint/$npoint_calc*$npoint_calc ] == $npoint ]];
then
	njobs=$[ $npoint/$npoint_calc]
else
	njobs=$[ $npoint/$npoint_calc + 1]
fi
echo "$[ $npoint - $pointini +1 ] tracking to do, $npoint_calc per job : $njobs jobs"
echo "Continue ? [yY/n(N)]"
read input
if [[ $input != 'Y' && $input != 'y' ]]; 
then 
    exit;
fi

echo "*******************************************************"	>simu.dat
echo "Parameters chosen :"										>>simu.dat
echo "Latice : $sequence_file"									>>simu.dat
echo "Results directory : $RESULTS_DIR"							>>simu.dat
echo "* Momentum deviation"										>>simu.dat
echo "    DP = $dp"												>>simu.dat
echo "* Wiglers :"												>>simu.dat
echo "    dxp_fieldint=$dxpfieldint Tm"							>>simu.dat
echo "    dx_fieldint=$dxfieldint Tm2"							>>simu.dat
echo "    dyp_fieldint=$dypfieldint Tm"							>>simu.dat
echo "    dy_fieldint=$dyfieldint Tm2"							>>simu.dat
echo "* Quads :"												>>simu.dat
echo "    deltax=$( echo 1|awk '{print '$deltax' * 1e6}' ) um"	>>simu.dat
echo "    deltay=$( echo 1|awk '{print '$deltay' * 1e6}' ) um"	>>simu.dat
echo "    dKn=$dkn  dKs=$dks"									>>simu.dat
echo "    dSn=$dsn  dSs=$dss"									>>simu.dat
echo "*******************************************************"	>>simu.dat


while [ "$pointini" -le $npoint ]
do
    let "pointend = $pointini + $npoint_calc - 1"
    echo "send job $job/$njobs for points $pointini to $pointend ..."
    
    sed -e 's/'%%pointini'/'$pointini'/g' \
        -e 's/'%%pointend'/'$pointend'/g' \
        -e 's/'%%sequence_file'/'$sequence_file'/g' \
        -e 's/'%%dp'/'$dp'/g' \
        -e 's/'%%dxpfieldint'/'$dxpfieldint'/g' \
        -e 's/'%%dxfieldint'/'$dxfieldint'/g' \
        -e 's/'%%dypfieldint'/'$dypfieldint'/g' \
        -e 's/'%%dyfieldint'/'$dyfieldint'/g' \
        -e 's/'%%deltax'/'$deltax'/g' \
        -e 's/'%%deltay'/'$deltay'/g' \
        -e 's/'%%dkn'/'$dkn'/g' \
        -e 's/'%%dks'/'$dks'/g' \
        -e 's/'%%dsn'/'$dsn'/g' \
        -e 's/'%%dss'/'$dss'/g'	$BATCHSUB_DIR/tracking_rad_1ang.madx > track_$pointini-$dp.madx
    sed -e 's/'input.madx'/'track_$pointini-$dp.madx'/g' \
        -e 's/'%%sequence_file'/'$sequence_file'/g' \
        -e 's|'%%BATCHSUB_DIR'|'$BATCHSUB_DIR'|g' \
        -e 's|'%%GLOBAL_RESULTS_DIR'|'$GLOBAL_RESULTS_DIR'|g' \
        -e 's|'%%RESULTS_DIR'|'$RESULTS_DIR'|g' \
        -e 's/'%%dp'/'$dp'/g' \
        -e 's/'%%dxpfieldint'/'$dxpfieldint'/g' \
        -e 's/'%%dxfieldint'/'$dxfieldint'/g' \
        -e 's/'%%dypfieldint'/'$dypfieldint'/g' \
        -e 's/'%%dyfieldint'/'$dyfieldint'/g' \
        -e 's/'%%deltax'/'$deltax'/g' \
        -e 's/'%%deltay'/'$deltay'/g' \
        -e 's/'%%dkn'/'$dkn'/g' \
        -e 's/'%%dks'/'$dks'/g' \
        -e 's/'%%dsn'/'$dsn'/g' \
        -e 's/'%%dss'/'$dss'/g' \
	    -e 's/'%%data'/'FM-data-$pointini'/g' batch_submit > batch_submit_$pointini-$dp

 	chmod +x batch_submit_$pointini-$dp
    bsub -q 8nh \
    	 -eo $RESULTS_DIR/FM-data-$pointini_bsub.out \
    	 -J "FM_$pointini-dp$dp-dxpwig$dxpfieldint-dxwig$dxfieldint-dypwig$dypfieldint-dywig$dyfieldint-deltax$deltax-deltay$deltay-dkn$dkn-dks$dks-dsn$dsn-dss$dss" \
    	 batch_submit_$pointini-$dp 
    let "pointini=$pointend+1"
    let "job+=1"
done
