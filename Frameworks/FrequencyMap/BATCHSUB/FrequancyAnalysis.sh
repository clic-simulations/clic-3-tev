#!/bin/zsh

	cp ~/scratch0/FRMAD/NAFAUT/nafaut_x.par .
	cp ~/scratch0/FRMAD/NAFAUT/nafaut_y.par .
	cp ~/scratch0/FRMAD/NAFAUT/nafaut.lxplus .

ls track.obs* > files
wc -l files >tmp

nlines=$(awk {'print $1'} tmp)

echo $nlines

for (( j=1 ; j < nlines+1 ; j++ ))
do
	m=$(printf "%04d" $j)	
	echo "m = $m"	

		head -n 9 track.obs0001.p$m |tail -1 > position.$j.out
		
		sed -e 's/'%trackfile'/'track.obs0001.p$m'/g' nafaut_x.par | sed -e 's/'%freqfile'/'trackx.$j.out'/g' | sed -e 's/'%turns'/'1056'/g' | sed -e 's/'%skiplines'/'8'/g' > nafaut.par
		chmod +x nafaut.lxplus
		./nafaut.lxplus
		
		sed -e 's/'%trackfile'/'track.obs0001.p$m'/g' nafaut_y.par | sed -e 's/'%freqfile'/'tracky.$j.out'/g' | sed -e 's/'%turns'/'1056'/g' | sed -e 's/'%skiplines'/'8'/g' > nafaut.par
		
		./nafaut.lxplus
		
		sed -e 's/'%trackfile'/'track.obs0001.p$m'/g' nafaut_x.par | sed -e 's/'%freqfile'/'trackxhf.$j.out'/g' | sed -e 's/'%turns'/'528'/g' | sed -e 's/'%skiplines'/'8'/g' > nafaut.par
		
		./nafaut.lxplus
		
		sed -e 's/'%trackfile'/'track.obs0001.p$m'/g' nafaut_y.par | sed -e 's/'%freqfile'/'trackyhf.$j.out'/g' | sed -e 's/'%turns'/'528'/g' | sed -e 's/'%skiplines'/'8'/g' > nafaut.par
		
		./nafaut.lxplus
		
		sed -e 's/'%trackfile'/'track.obs0001.p$m'/g' nafaut_x.par | sed -e 's/'%freqfile'/'trackxhs.$j.out'/g' | sed -e 's/'%turns'/'528'/g' | sed -e 's/'%skiplines'/'536'/g' > nafaut.par
		
		./nafaut.lxplus
		
		sed -e 's/'%trackfile'/'track.obs0001.p$m'/g' nafaut_y.par | sed -e 's/'%freqfile'/'trackyhs.$j.out'/g' | sed -e 's/'%turns'/'528'/g' | sed -e 's/'%skiplines'/'536'/g' > nafaut.par
		
		./nafaut.lxplus					
 
#		rm nafaut_x.par
	

	awk {'print $2'} trackx.$j.out | tail -1 > freqx.$j.out
	awk {'print $2'} tracky.$j.out | tail -1 > freqy.$j.out
	awk {'print $2'} trackxhf.$j.out | tail -1 > freqxhf.$j.out
	awk {'print $2'} trackyhf.$j.out | tail -1 > freqyhf.$j.out
	awk {'print $2'} trackxhs.$j.out | tail -1 > freqxhs.$j.out
	awk {'print $2'} trackyhs.$j.out | tail -1 > freqyhs.$j.out
	
	paste position.$j.out freqx.$j.out freqy.$j.out freqxhf.$j.out freqyhf.$j.out freqxhs.$j.out freqyhs.$j.out > all.$j.dat
		
	echo "j = $j"	
	rm position.$j.out freqx.$j.out freqy.$j.out freqxhf.$j.out freqyhf.$j.out freqxhs.$j.out freqyhs.$j.out
done

cat all.*.dat > FM-data.dat
rm all.*.dat
	
echo "i = $i"	
#	let k=$k+1
#	cd ../
#done

	
