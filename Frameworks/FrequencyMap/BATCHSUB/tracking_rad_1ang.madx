option,verify,-warn,-info;

/*****************************************************************************************************
 *****************************************************************************************************
 * Setting up the accelerator
 *****************************************************************************************************
 *****************************************************************************************************/

/***********************************************************
 * Optics and sequence files
 ***********************************************************/

beam,particle=positron,bunched=true,radiate=true,mass:= 0.00051099891,charge:= 1,energy:= 2.86,pc:= 2.859999954,gamma:= 5596.880823,ex:= 1.328e-08,exn:= 0.0002973063046,ey:= 3.162e-10,eyn:= 7.078934752e-06,et:= 4.56605e-05,sigt:= 0.0091321
,sige:= 0.005,kbunch:= 312,npart:= 4070000000,bcurrent:= 0,freq0:= 0,circ:= 0,dtbyds:= 0,deltap:= 0,beta:= 0.999999984,alfa:= 3.19233075e-08,u0:= 0,qs:= 0,arad:= 2.817940287e-15,bv:= 1,pdamp:={1 ,1 ,2 },n1min:= -1;


option,-echo;
call,file="%%sequence_file";
option,echo;

use, sequence=RING;

/**********************************
 *   MACRO DEFINITIONS
 **********************************/

trackin(point): macro=
{ 
	xx = table(pos,X,point);
	yy = table(pos,Y,point);
	show, xx, yy;
	ptc_start, x=xx, px=0, y=yy, py=0;
}

errorwighalfpolepos(index,dkn0,dks0): macro=
{
	SELECT,flag=error, clear;
    show, dkn0,dks0;
    SELECT, flag=error, range=wighalfpolepos[index];
    EFCOMP, DKN:={dkn0},DKS:={dks0};
}

SELECT, flag = twiss, clear;
SELECT, flag = twiss, range = #S/#E, column = NAME, KEYWORD, S, L, ANGLE, BETX, BETY, DX, DY, DPX, DPY, ALFX, ALFY, MUX, MUY;
TWISS, sequence = ring, table = ring_init;

!------------------------------------------------------------------------
! get initial betatron tunesaticity
q1_orig = table(summ,Q1);
q2_orig = table(summ,Q2);
dq1_orig = table(summ,DQ1);
dq2_orig = table(summ,DQ2);

!------------------------------------------------------------------------
! get initial beta functions
!SET, sequence = ring_de_arc;
SELECT, flag = twiss, clear;
SELECT, flag = twiss, range = MH1[1]/MH1[2], column = NAME, KEYWORD, S, L, ANGLE, K1L, K2L, BETX, ALFX, MUX, DX, DPX, BETY, ALFY, MUY, DY, DPY;
SAVEBETA, place = mh1[1], label = beta_mh1_1;
TWISS, table = ring_de_arc_init;

!SET, sequence = ring_de_wigg;
SELECT, flag = twiss, clear;
SELECT, flag = twiss, range = MH3[1]/MH3[2], column = NAME, KEYWORD, S, L, ANGLE, K1L, K2L, BETX, ALFX, MUX, DX, DPX, BETY, ALFY, MUY, DY, DPY;
SAVEBETA, place = MH3[1], label = beta_mh3_1;
TWISS, table = ring_de_wigg_init;

q1_s_orig = table(summ,Q1);
q2_s_orig = table(summ,Q2);

SELECT, flag = twiss, clear;
SELECT, flag = twiss, range = #s/#e, column = NAME, KEYWORD, S, L, ANGLE, K1L, K2L, BETX, ALFX, MUX, DX, DPX, BETY, ALFY, MUY, DY, DPY;
SAVEBETA, place = mh1[2], label = beta_mh1_2;
TWISS;
SAVEBETA, place = mh1[3], label = beta_mh1_3;
TWISS;
SAVEBETA, place = mh1[4], label = beta_mh1_4;
TWISS;
SAVEBETA, place = mh3[2], label = beta_mh3_2;
TWISS;
SAVEBETA, place = mh3[3], label = beta_mh3_3;
TWISS;
SAVEBETA, place = mh3[4], label = beta_mh3_4;
TWISS;

SELECT, flag = twiss, clear;
SELECT, flag = twiss, class = quadrupole, range = #S/#E, column = NAME, KEYWORD, S, L, ANGLE, BETX, ALFX, MUX, DX, DPX, BETY, ALFY, MUY, DY, DPY;
TWISS, sequence = ring;
plot,haxis=s,vaxis1=BETX,BETY,vaxis2=DX,DY,colour=100,range=#s/#e,title="twiss nominal",file=twiss,noversion=true;
SELECT, flag = twiss, clear;
SELECT, flag = twiss, class = quadrupole, range = mh1[4]/#e, column = NAME, KEYWORD, S, L, ANGLE, BETX, ALFX, MUX, DX, DPX, BETY, ALFY, MUY, DY, DPY;
TWISS, sequence = ring;
plot,haxis=s,vaxis1=BETX,BETY,vaxis2=DX,DY,colour=100,range=mh1[4]/#e,title="twiss nominal",file=twiss,noversion=true;
plot,haxis=s,vaxis=MUX,MUY,colour=100,range=mh1[4]/#e,title="phases nominal",file=twiss,noversion=true;

! ###########################
! ## Target beta functions ##
! ###########################

! set target functions

q1_target  = q1_orig + 0;
q2_target  = q2_orig + 0;
dq1_target = 0;
dq2_target = 0;

! set target tunes in the straight sections

q1_increase = q1_target - q1_orig;
q2_increase = q2_target - q2_orig;

q1_s_target = q1_s_orig + q1_increase;
q2_s_target = q2_s_orig + q2_increase;


!Get beam emittance and size
SELECT, flag = twiss, clear;
twiss;

create, table=sumup,column=description,bx,ex,exn,xcorms,Dxrms,Qx,dQx,by,ey,eyn,ycorms,Dyrms,Qy,dQy;
bx:=table(twiss,betx,1);
by:=table(twiss,bety,1);
ex:=beam->EX;
ey:=beam->Ey;
exn:=beam->EXn;
eyn:=beam->Eyn;
xcorms:=table(summ,Dxrms);
ycorms:=table(summ,Dyrms);
Dxrms:=table(summ,Dxrms);
Dyrms:=table(summ,Dyrms);
Qx:=table(summ,Q1);
Qy:=table(summ,Q2);
dQx:=table(summ,dQ1);
dQy:=table(summ,dQ2);

description="nominal with PDR beam";
fill,table=sumup;

bx_pdr_nom=table(twiss,betx,1);
by_pdr_nom=table(twiss,bety,1);
ex_pdr_nom=beam->EX;
ey_pdr_nom=beam->Ey;
sx_pdr_nom=sqrt(bx_pdr_nom*ex_pdr_nom);
sy_pdr_nom=sqrt(by_pdr_nom*ey_pdr_nom);
Qx_pdr_nom=table(summ,Q1);
Qy_pdr_nom=table(summ,Q2);

EMIT,DELTAP=0.0;

description="nominal";
fill,table=sumup;

/***********************************************************
 * ERRORS
 ***********************************************************/

EOPTION,SEED=1;

//INTRODUCE ERRORS ON QUAD FIELDS
radmult=0.01; //1cm
dkn=%%dkn;// relative error on quadrupole field
dks=%%dks;//skew relative error on quadrupole field
dsn=%%dsn;// sextupole relative error term
dss=%%dss;// sextupole skew relative error term
SELECT,flag=error, clear;
SELECT, flag=error, class=quadrupole;
EFCOMP, radius:=radmult, order=1,
    DKNR:={0,dkn*tgauss(3),dsn*tgauss(3)},
    DKSR:={0,dks*tgauss(3),dss*tgauss(3)};

//INTRODUCE ERRORS ON QUAD POSITIONS
deltax=%%deltax;
deltay=%%deltay;
SELECT,flag=error, clear;
SELECT, flag=error, class=quadrupole;
EALIGN, DX:=deltax*tgauss(3),DY:=deltay*tgauss(3);

//INTRODUCE ERRORS ON WIGLER FIELDS
wiglL=2;
halfwiglpolL= 0.02026423673;
nwigl=52;
wigl=0;
dxpfieldint:=%%dxpfieldint*tgauss(3)/3;//[Tm]
dxfieldint:=%%dxfieldint*tgauss(3)/3;//[Tm²]
dypfieldint:=%%dypfieldint*tgauss(3)/3;//[Tm]
dyfieldint:=%%dyfieldint*tgauss(3)/3;//[Tm²]
//xp=int(B*ds)*300/E[MeV]
dxp:=dxpfieldint*300/2860; 
dx:=dxfieldint*300/2860; 
dyp:=dypfieldint*300/2860;
dy:=dyfieldint*300/2860;
anglexin:=dx/wiglL;
anglexout:=dxp-anglexin;
angleyin:=dy/wiglL;
angleyout:=dyp-angleyin;  
dkn0in:=anglexin;
dks0in:=angleyin;
dkn0out:=anglexout;
dks0out:=angleyout;
 
show, dkn0in,dks0in,dkn0out,dks0out;

option,-echo,-warn,-info,-verify;
while (wigl<nwigl)
{
    i = 2*wigl+1;
    j = 2*wigl+2;
    exec, errorwighalfpolepos($i,dkn0in,dks0in);
    exec, errorwighalfpolepos($j,dkn0out,dks0out);
    wigl=wigl+1;
}
option,echo,verify;

ESAVE,FILE=errfile_before_correction;

SELECT, flag = twiss, clear;
SELECT, flag = twiss, class = quadrupole, range = #S/#E, column = NAME, KEYWORD, S, L, ANGLE, BETX, ALFX, MUX, DX, DPX, BETY, ALFY, MUY, DY, DPY;
TWISS, sequence = ring;
plot,haxis=s,vaxis1=BETX,BETY,vaxis2=DX,DY,colour=100,range=#s/#e,title="twiss before tune matching",file=twiss,noversion=true;
EMIT,DELTAP=0.0;

description="with errors";
fill,table=sumup;

! #########################################
! ## Match the tune in straight sections ##
! #########################################

MATCH, sequence = ring;
GLOBAL,  sequence = ring, Q1 = q1_s_target, Q2 = q2_s_target;
VARY, name = kqfw, step = 0.000001;
VARY, name = kqdw, step = 0.000001;
LMDIF, calls = 50, tolerance = 1.0e-12;
ENDMATCH;

SELECT, flag = twiss, clear;
SELECT, flag = twiss, class = quadrupole, range = #S/#E, column = NAME, KEYWORD, S, L, ANGLE, BETX, ALFX, MUX, DX, DPX, BETY, ALFY, MUY, DY, DPY;
TWISS, sequence = ring;
!plot,haxis=s,vaxis1=BETX,BETY,vaxis2=DX,DY,colour=100,range=#s/#e,title="twiss after tune matching",file=twiss,noversion=true;
EMIT,DELTAP=0.0;

description="tune matched";
fill,table=sumup;

! #######################
! ## Correct the orbit ##
! #######################

SELECT, flag = twiss, clear;
SELECT, flag = twiss, range = #S/#E, column = NAME, KEYWORD, S, L, ANGLE, BETX, DX, BETY, DY;
TWISS,  sequence = ring, table = ring_uncorr;

! -----------------------------------------------------------------------
USEKICK, class = hcorr, status = on;
USEKICK, class = vcorr, status = on;
USEKICK, class = hvcorr, status = on;
USEMONITOR, class = bpm, status = on;

CORRECT, orbit = ring_uncorr, model = ring_init,
	 flag = ring, mode = svd, monon = 1,
	 plane = y, ncorr = 0,
	 clist = "cor_list_y.tab", mlist = "mon_list_y.tab", corzero = 1;

SELECT, flag = twiss, clear;
SELECT, flag = twiss, range = #S/#E, column = NAME, KEYWORD, S, L, ANGLE, BETX, ALFX, MUX, DX, DPX, BETY, ALFY, MUY, DY, DPY;
TWISS, sequence = ring, table = ring_corr_y;
EMIT, deltap = 0.0;

description="vertical orbit correction";
fill,table=sumup;

! -----------------------------------------------------------------------
CORRECT, orbit = ring_corr_y, model = ring_init,
	 flag = ring, mode = svd, monon = 1,
	 plane = x, ncorr = 0,
	 clist = "cor_list_x.tab", mlist = "mon_list_x.tab", corzero = 1;


SELECT, flag = twiss, clear;
SELECT, flag = twiss, range = #S/#E, column = NAME, KEYWORD, S, L, ANGLE, BETX, ALFX, MUX, DX, DPX, BETY, ALFY, MUY, DY, DPY;
TWISS, sequence = ring, table = ring_corr_xy;

SELECT, flag = twiss, clear;
SELECT, flag = twiss, class = quadrupole, range = #S/#E, column = NAME, KEYWORD, S, L, ANGLE, BETX, ALFX, MUX, DX, DPX, BETY, ALFY, MUY, DY, DPY;
TWISS, sequence = ring;
plot,haxis=s,vaxis1=BETX,BETY,vaxis2=DX,DY,colour=100,range=#s/#e,title="twiss after orbit correction",file=twiss,noversion=true;
EMIT, deltap = 0.0;

description="horizontal orbit correction";
fill,table=sumup;

! #########################################
! ## Match the tune in straight sections ##
! #########################################

MATCH, sequence = ring;
GLOBAL,  sequence = ring, Q1 = q1_s_target, Q2 = q2_s_target;
VARY, name = kqfw, step = 0.000001;
VARY, name = kqdw, step = 0.000001;
LMDIF, calls = 50, tolerance = 1.0e-12;
ENDMATCH;

SELECT, flag = twiss, clear;
SELECT, flag = twiss, class = quadrupole, range = #S/#E, column = NAME, KEYWORD, S, L, ANGLE, BETX, ALFX, MUX, DX, DPX, BETY, ALFY, MUY, DY, DPY;
TWISS, sequence = ring;
//plot,haxis=s,vaxis1=BETX,BETY,vaxis2=DX,DY,colour=100,range=#s/#e,title="twiss after tune matching",file=twiss,noversion=true;
EMIT, deltap = 0.0;

description="tune matched";
fill,table=sumup;

! ##################
! ## Match the RF ##
! ##################

MATCH, sequence=ring;
VARY, name = lgrf, step = 1.0e-6;
CONSTRAINT, range = #e, t = 0;
WEIGHT, T = 10000;
JACOBIAN, calls= 20, tolerance = 1.E-22, bisec=3;
LMDIF, calls = 1000, tolerance = 1.E-22;
ENDMATCH;

SELECT, flag = twiss, clear;
SELECT, flag = twiss, class = quadrupole, range = #S/#E, column = NAME, KEYWORD, S, L, ANGLE, BETX, ALFX, MUX, DX, DPX, BETY, ALFY, MUY, DY, DPY;
TWISS, sequence = ring;
//plot,haxis=s,vaxis1=BETX,BETY,vaxis2=DX,DY,colour=100,range=#s/#e,title="twiss after RF matching",file=twiss,noversion=true;
EMIT, deltap = 0.0;

description="RF matched";
fill,table=sumup;

! ######################################
! ## Match the dispersion suppressors ##
! ######################################

MATCH, sequence = ring;
CONSTRAINT, sequence = ring, range = mh3[1],
		DX = beta_mh3_1->DX, DPX=beta_mh3_1->DPX;
VARY, name = kqf2ds.1.1 , step = 1e-6;
VARY, name = agtme.1 ,  step = 1e-6;
CONSTRAINT, sequence = ring, range = mh3[3],
		DX = beta_mh3_3->DX, DPX=beta_mh3_3->DPX;
VARY, name = kqf2ds.3.1 , step = 1e-6;
VARY, name = agtme.3 ,  step = 1e-6;
CONSTRAINT, sequence = ring, range = mh1[2],
		DX = beta_mh1_2->DX, DPX=beta_mh1_2->DPX;
VARY, name = kqf2ds.2.1 , step = 1e-6;
VARY, name = agtme.2 ,  step = 1e-6;
CONSTRAINT, sequence = ring, range = mh1[4],
		DX = beta_mh1_4->DX, DPX=beta_mh1_4->DPX;
VARY, name = kqf2ds.4.1 , step = 1e-6;
VARY, name = agtme.4.1 ,  step = 1e-6;
LMDIF, calls = 200, tolerance = 1.0e-15;
ENDMATCH;

SELECT, flag = twiss, clear;
SELECT, flag = twiss, class = quadrupole, range = #S/#E, column = NAME, KEYWORD, S, L, ANGLE, BETX, ALFX, MUX, DX, DPX, BETY, ALFY, MUY, DY, DPY;
TWISS, sequence = ring;
plot,haxis=s,vaxis1=BETX,BETY,vaxis2=DX,DY,colour=100,range=#s/#e,title="twiss after Dx matching",file=twiss,noversion=true;
EMIT, deltap = 0.0;

description="Dx matched";
fill,table=sumup;

MATCH, sequence = ring;
CONSTRAINT, sequence = ring, range = mh3[1],
		betx = beta_mh3_1->betx, alfx = beta_mh3_1->alfx,
		bety = beta_mh3_1->bety, alfy = beta_mh3_1->alfy;
VARY, name = kqm31.1.1 , step = 1e-6;
VARY, name = kqm41.1.1 ,  step = 1e-6;
VARY, name = kqfw0.1.1 , step = 1e-6;
VARY, name = kqdw0.1.1 ,  step = 1e-6;
CONSTRAINT, sequence = ring, range = mh3[3],
		betx = beta_mh3_3->betx, alfx = beta_mh3_3->alfx,
		bety = beta_mh3_3->bety, alfy = beta_mh3_3->alfy;
VARY, name = kqm31.3.1 , step = 1e-6;
VARY, name = kqm41.3.1 ,  step = 1e-6;
VARY, name = kqfw0.3.1 , step = 1e-6;
VARY, name = kqdw0.3.1 ,  step = 1e-6;
CONSTRAINT, sequence = ring, range = mh1[2],
		betx = beta_mh1_2->betx, alfx = beta_mh1_2->alfx,
		bety = beta_mh1_2->bety, alfy = beta_mh1_2->alfy;
VARY, name = kqm31.2.1 , step = 1e-6;
VARY, name = kqm41.2.1 ,  step = 1e-6;
VARY, name = kqfw0.2.1 , step = 1e-6;
VARY, name = kqdw0.2.1 ,  step = 1e-6;
CONSTRAINT, sequence = ring, range = mh1[4],
		betx = beta_mh1_4->betx, alfx = beta_mh1_4->alfx,
		bety = beta_mh1_4->bety, alfy = beta_mh1_4->alfy;
VARY, name = kqm31.4.1 , step = 1e-6;
VARY, name = kqm41.4.1 ,  step = 1e-6;
VARY, name = kqfw0.4.1 , step = 1e-6;
VARY, name = kqdw0.4.1 ,  step = 1e-6;
LMDIF, calls = 200, tolerance = 1.0e-15;
ENDMATCH;

SELECT, flag = twiss, clear;
SELECT, flag = twiss, class = quadrupole, range = #S/#E, column = NAME, KEYWORD, S, L, ANGLE, BETX, ALFX, MUX, DX, DPX, BETY, ALFY, MUY, DY, DPY;
TWISS, sequence = ring;
plot,haxis=s,vaxis1=BETX,BETY,vaxis2=DX,DY,colour=100,range=#s/#e,title="twiss after twiss matching",file=twiss,noversion=true;
EMIT, deltap = 0.0;

description="Twiss matched";
fill,table=sumup;

//MATCH, sequence = ring, RMATRIX, BETA0=beta_mh1_1;
//CONSTRAINT, sequence = ring, range = mh3[1],
//		DY = beta_mh3_1->DY, DPY = beta_mh3_1->DPY,
//		RE41=0, RE31=0, RE42=0, RE32=0;
//VARY, name = kqs1.1 ,  step = 1e-6;
//VARY, name = kqs2.1 ,  step = 1e-6;
//VARY, name = kqs3.1 ,  step = 1e-6;
//CONSTRAINT, sequence = ring, range = mh3[3],
//		DY = beta_mh3_3->DY, DPY = beta_mh3_3->DPY,
//		RE41=0, RE31=0, RE42=0, RE32=0;
//VARY, name = kqs1.3 ,  step = 1e-6;
//VARY, name = kqs2.3 ,  step = 1e-6;
//VARY, name = kqs3.3 ,  step = 1e-6;
//CONSTRAINT, sequence = ring, range = mh1[2],
//		DY = beta_mh1_2->DY, DPY = beta_mh1_2->DPY,
//		RE41=0, RE31=0, RE42=0, RE32=0;
//VARY, name = kqs12.2 ,  step = 1e-6;
//VARY, name = kqs22.2 ,  step = 1e-6;
//VARY, name = kqs32.2 ,  step = 1e-6;
//CONSTRAINT, sequence = ring, range = mh1[4],
//		DY = beta_mh1_4->DY, DPY = beta_mh1_4->DPY,
//		RE41=0, RE31=0, RE42=0, RE32=0;
//VARY, name = kqs12.4 ,  step = 1e-6;
//VARY, name = kqs22.4 ,  step = 1e-6;
//VARY, name = kqs32.4 ,  step = 1e-6;
//LMDIF, calls = 200, tolerance = 1.0e-18;
//ENDMATCH;

//SELECT, flag = twiss, clear;
//SELECT, flag = twiss, class = quadrupole, range = #S/#E, column = NAME, KEYWORD, S, L, ANGLE, BETX, ALFX, MUX, DX, DPX, BETY, ALFY, MUY, DY, DPY;
//TWISS, sequence = ring;
//plot,haxis=s,vaxis1=BETX,BETY,vaxis2=DX,DY,colour=100,range=#s/#e,title="twiss after Dy + coupl. matching",file=twiss,noversion=true;

//SELECT, flag = twiss, clear;
//twiss;
//EMIT,DELTAP=0.0;

! ######################################
! ## Match the chromaticities to zero ##
! ######################################

MATCH, sequence = ring;
GLOBAL, sequence = ring, DQ1 = dq1_target, DQ2 = dq2_target;
VARY, name = ks2x2, step = 0.000001;
VARY, name = ks2y1, step = 0.000001;
LMDIF, calls = 10, tolerance = 1.0e-10;
ENDMATCH;

//SELECT, flag = twiss, clear;
//SELECT, flag = twiss, class = quadrupole, range = #S/#E, column = NAME, KEYWORD, S, L, ANGLE, BETX, ALFX, MUX, DX, DPX, BETY, ALFY, MUY, DY, DPY;
//TWISS, sequence = ring;
//plot,haxis=s,vaxis1=BETX,BETY,vaxis2=DX,DY,colour=100,range=#s/#e,title="twiss after chrom. matching",file=twiss;

SELECT, flag = twiss, clear;
twiss;
EMIT,DELTAP=0.0;

description="chromaticities matched";
fill,table=sumup;

! ###############################################
! ## Match the tunes, in the wiggler straights ##
! ###############################################

//MATCH, sequence = ring;
//GLOBAL,  sequence = ring, Q1 = q1_s_target, Q2 = q2_s_target;
//VARY, name = kqfw, step = 0.000001;
//VARY, name = kqdw, step = 0.000001;
//LMDIF, calls = 50, tolerance = 1.0e-12;
//ENDMATCH;


//SELECT, flag = twiss, clear;
//SELECT, flag = twiss, class = quadrupole, range = #S/#E, column = NAME, KEYWORD, S, L, ANGLE, BETX, ALFX, MUX, DX, DPX, BETY, ALFY, MUY, DY, DPY;
//TWISS, sequence = ring;
//plot,haxis=s,vaxis1=BETX,BETY,vaxis2=DX,DY,colour=100,range=#s/#e,title="twiss after tune matching",file=twiss;
//EMIT,DELTAP=0.0;

//description="Tune matched";
//fill,table=sumup;

/***********************************************************
 * CREATE PARAM.DAT
 ***********************************************************/
 
write,table=sumup,file=sumup.dat;

assign, echo="Params.dat";
show, sx_pdr_nom,sy_pdr_nom,Qx_pdr_nom,Qy_pdr_nom;
assign,echo=terminal;

/****************************************
 *    GET COORDIANTES FOR TRACKING FROM tracking_pos.dat
 ****************************************/
readmytable,file="tracking_pos.dat",table="pos";

/****************************************
 *    TRACKING !
 ****************************************/

ptc_create_universe;
ptc_create_layout,model=2,method=6,nst=10,exact;
PTC_ALIGN;
PTC_SETSWITCH, fringe=true,debuglevel = 1; 

point=%%pointini;
pointend=%%pointend;

while (point<=pointend)
{
	exec, trackin($point);	//PTC_START,X=...
    point=point+1;
}

ptc_track,icase=5,deltap=%%dp,closed_orbit,dump,turns=1056,maxaper={1,1,1,1,1,1};
     
ptc_track_end;
ptc_end;

stop;
