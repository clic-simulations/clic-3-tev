%*************************************************************%
% ************* Tune diagram ************** %
%     Crossing points of the equation       %
% **** n1*x + n2*y - C = 0, Scanning C **** %
% ***************************************** %
%
% Basismeno sto script tou yanni resonline.m
%
% Function resonl(xmin,ymin,orders,period)
%
% Inputs: xmin->Qx_min, ymin->Qy_min
%         orders->orders of resonances I need to plot
%         period->The periodicity of the lattice
%
% Outputs:acC-> Matrix with the values of n1, n2, C and if C 
%               is divided by the periodicity or not
%         plot->optional plot of the tune diagram
%
% Gives also the option to put the working point
%
% 16/01/2009
%*************************************************************%

function  abC = resonl(xmin,ymin,orders,period)

    xmax=xmin+1; %Megisto Qx
    ymax=ymin+1; %Megisto Qy

for order=min(orders):max(orders)

    N1=[order:-1:0 order-1:-1:0]; % Ola ta n1
    N2=[0:1:order -1:-1:-order];  % Ola ta n2   
    
    n1=N1'; 
    n2=N2';
  
    % Ypologismos olwn twn dinatwn akrotatwn C gia kathe
    % zeugos n1,n2 kai euresi
    Cextr=[-n1.*xmin-n2.*ymin -n1.*xmax-n2.*ymax -n1.*xmin-n2.*ymax -n1.*xmax-n2.*ymin];

    % Megisto kai elaxisto C gia kathe periptwsi
    Cmax=max(Cextr');
    Cmin=min(Cextr');

    % Ypologismos olwn twn C apo Cmin ews Cmax
    for i=1:length(Cmax)    
        CC=Cmin(i):Cmax(i);                  
        abc{i}=[repmat([N1(:,i) N2(:,i)],length(CC),1) CC'];
    end

    res_order{order}=cell2mat(abc');
end
    resonances=cell2mat(res_order');
    nn1=resonances(:,1);
    nn2=resonances(:,2);
    Cvect=resonances(:,3);
   
    flag=Cvect/period-double(int16(Cvect/period))==0;
     
    abC=[Cvect nn1 nn2 flag]
    
    stepx=(xmax-xmin)/10;
    stepy=(ymax-ymin)/10;

    % Dunatotita epilogis gia plot
    reply = input('Do you want a resonance plot? Y/N [Y]: ','s');
    if isempty(reply)
        reply = 'Y';
    end
    if reply=='N'
        return;
    else
    
    for i=1:length(nn1)
        if nn2(i)~=0
            h(i)=line([xmin-stepx,xmax+stepx],[(-Cvect(i)-nn1(i)*(xmin-stepx))/nn2(i),(-Cvect(i)-nn1(i)*(xmax+stepx))/nn2(i)]);
            
        else
            h(i)=line([-Cvect(i)/nn1(i),-Cvect(i)/nn1(i)],[ymin-stepy,ymax+stepy]);
        end        
        if abs(nn1(i))+abs(nn2(i)) > 2
            if nn2(i)/2-round(nn2(i)/2)~=0
                set(h(i),'LineStyle','--')
            end
        else
            set(h(i),'LineStyle','-')
        end
        if flag(i)~=0
            set(h(i),'Color',[1 0 0],'LineWidth',2)
        else
            set(h(i),'Color',[0 0 1],'LineWidth',0.5)
        end                            
            
    end
    end
    xlim([xmin-stepx xmax+stepx])
    ylim([ymin-stepy ymax+stepy])
    xlabel('Qx')
    ylabel('Qy')   
    
    reply2 = input('Do you want to add working point? Y/N [Y]: ','s');
    if isempty(reply2)     
        return;
    end
    if reply2=='N'
        return;
    else
    Qx = input('Qx: ');
    Qy = input('Qy: ');
    hold('all')
    plot(Qx,Qy,'*m')
    end
    
    reply3 = input('Do you want to add tune shift with amplitude????? Y/N [Y]: ','s');
    if isempty(reply3)     
%        return;
        reply4 = input('Do you want to add tune shift with deltap? Y/N [Y]: ','s');
    if isempty(reply4)     
        return;
    end
    if reply4=='N'
        return;
    else     
        load Q11.dat
        load Q22.dat
        plot(Qx+Q11,Qy+Q22,'.')
    end
    end
    if reply3=='N'
%        return;
        reply4 = input('Do you want to add tune shift with deltap? Y/N [Y]: ','s');
    if isempty(reply4)     
        return;
    end
    if reply4=='N'
        return;
    else     
        load Q11.dat
        load Q22.dat
        plot(double(int16(Qx))+Q11,double(int16(Qy))+Q22,'.')
%        plot(Qx,Qy,'*r')
    end
    else
        axx=input('axx: ');
        axy=input('axy: ');
        ayy=input('ayy: ');
        %bx=input('betax [m]: '); 
        %by=input('betay [m]: '); 
        En=input('Energy [MeV]: ');
        emitinx=input('Normalized horizontal emittance in m: ')*0.511/En;
        emitiny=input('Normalized vertical emittance in m: ')*0.511/En;
        Nem=input('Maximum beam size distance: ');
        %emitinx=7*10^-3*0.511/En;
        %emitiny=7*10^-3*0.511/En;
%        sigmax=sqrt(bx.*(emitinx:48*emitinx/100:49*emitinx));
%        sigmay=sqrt(by.*(49*emitiny:-48*emitiny/100:emitiny));
%        emitx=sigmax.^2/bx;
%        emity=sigmay.^2/by;
        
        emitx=emitinx:emitinx:Nem^2*emitinx;
        emity=emitiny:emitiny:Nem^2*emitiny;
        emx=repmat(emitx,length(emitx),1);
        emy=repmat(emity',1,length(emity));
               
        dQx = axx.*emx(emx+emy<max(emitx))+axy.*emy(emx+emy<max(emitx));
        dQy = axy.*emx(emx+emy<max(emitx))+ayy.*emy(emx+emy<max(emitx));
        
        hold('all')
        plot(Qx+dQx,Qy+dQy,'.g')
        plot(Qx,Qy,'*r')
    end
        
%    save mpla.mat