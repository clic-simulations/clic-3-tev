**************************************************************************
*     nafaut.f
*     programme pour l'analyse en frequence NAFF sur les points d'un 
*     fichier  une seule tranche mais possibilite de plusieurs variables
*     version 1.0
*     26/4/92
*     utilise la librairie naff
*
*   (c) Jacques Laskar 
*       Astronomie et Systemes Dynamiques
*       Bureau des Longitudes
*       75006 Paris
*       email : laskar@bdl.fr
***************************************************************************
*-----------------  initialisations NAFF (parameter)
*   NFR : nombre max de frequences demandees
*   KTABS : nombre de points utilises KTABS doit etre divisible par 6
*           les tableau seront dimensiones 0:KTABS
*  KFICHM : dimension de buffer
*---------------------------------------------------------------------
      IMPLICIT DOUBLE PRECISION (A-H,O-Y)
      IMPLICIT COMPLEX*16(Z)
      character*40 nomfpar,nomfout,nomfdat,nomfsol,nomftab 
      PARAMETER (NFR=100)
      PARAMETER (KTABSM=500000)
      DIMENSION ZRTABS(0:KTABSM)
      DIMENSION ZAMPR(NFR),TFSR(NFR)
      DIMENSION TFM1(10),TFM2(10),TNT(10)
      COMMON/CGRAM/ZAMP(NFR),ZALP(NFR,NFR),TFS(NFR),NFS
      COMMON/CDATA/XH,T0,UNIANG,FREFON,ICPLX,IW
      COMMON/CPRINT/NFPRT,IPRT
      COMMON/CPOW/ZT(0:KTABSM)
      COMMON/CTABS/ZTABS(0:KTABSM)
      COMMON/CWIN/TWIN(0:KTABSM)
      COMMON/CWORK1/ZWORK1(0:KTABSM)
      COMMON/CWORK2/WORK2(0:KTABSM)
      COMMON/CMODT/FM1,FM2,IMODT
*-----------------parametres variables obtenus par init
      COMMON/CSTD/DTOUR,KTABS,NEPS,NTERM,
     $ NULIN,ICOL,ICX,ICY,
     $ IPRNAF,IFTES,IFSAV,IFCOMP,IFRES,IFSTAB,
     $ nomfpar,nomfout,nomfdat,nomfsol,nomftab,
     $ IFMODT,TFM1,TFM2,TNT
*----------------- premieres initialisations
      PI = atan2(1.d0,0.d0)*2
      write (*,*) '  PI = ' , PI
*----------------- lecture des donnees complementaires
      NFPRT=6
      call inipar
      call prtpar(nfprt)
*----------------- fichiers
      NFSOL=21
      OPEN(NFSOL,FILE =nomfsol, FORM='FORMATTED',STATUS='UNKNOWN')
      NFPAR=22
      OPEN(NFPAR,FILE =nomfpar, FORM='FORMATTED',STATUS='UNKNOWN')
      NFOUT=23
      OPEN(NFOUT,FILE =nomfout, FORM='FORMATTED',STATUS='UNKNOWN')
      call prtpar(NFPAR)
      close(NFPAR)
*----------------- initialisations complementaires
      UNIANG = DTOUR/(2*PI)
      FREFON = DTOUR/(KTABS*XH)
      EPS=ABS(FREFON)/NEPS
      WRITE(*,*) 'FREFON= ',FREFON,',EPS= ',EPS
      IKT = KTABS/10
*---------------- verifications de dimensions et autres
      if (KTABS.GT.KTABSM) then
         write (*,*) 'KTABS.GT.KTABSM'
         stop
      endif
      if (idm.GT.idmax) then
         write (*,*) 'idm.GT.idmax'
         stop
      endif
***************    autres initialisations
      CALL INIWIN(KTABS)
***************    chargement des donnees
      call loadsol(ktabs,ztabs,nomfdat,icx,icy,icol,nulin)
      IPRT = 1
      CALL PRTABS(KTABS,ZTABS,IKT)
      IPRT = iprnaf
      IMODT =0
      CALL MFTNAF (KTABS,NTERM,EPS)
      IPRT = 1
      CALL PRTABS(KTABS,ZTABS,IKT)
      CALL SAVSOL(NFSOL)
      if (ifcomp.eq.1) then
*---------- sauvegarde du tableau existant
         DO I = 0,KTABS
             ZRTABS(I) = ZTABS(I)
         end do
         IPRT = 0
         IMODT =0
         CALL TESSOL (KTABS,EPS,TFSR,ZAMPR)
         IPRT=1
         CALL COMPSO (NFOUT,NFS,TFS,ZAMP,TFSR,ZAMPR)
*---------- restitution du tableau existant
         DO I = 0,KTABS
            ZTABS(I) = ZRTABS(I)
         end do
      endif
      if (ifres.eq.1) then
*---------- analyse des residus
          IPRT=1
          CALL PRTABS(KTABS,ZTABS,IKT)
          IPRT=0
          CALL MFTNAF (KTABS,NTERM,EPS)
          IPRT=1
          CALL PRTABS(KTABS,ZTABS,IKT)
          write(Nfsol,*) ' ***********'
          CALL SAVSOL(NFSOL)
      endif
      if (ifmodt.ge.1) then
         do imo = 1,ifmodt
            FM1 = TFM1(imo)
            FM2 = TFM2(imo)
            NTERM = TNT(imo)
            IMODT = 1
            IPRT  = 0
            CALL MFTNAF (KTABS,NTERM,EPS)
            write(Nfsol,*) ' *********** F1 < F < F2', FM1,FM2
            CALL SAVSOL(NFSOL)
         end do
      endif
      if (ifstab.eq.1) then
         CALL SAVTAB(KTABS,ZTABS,nomftab)
      endif
      END

      subroutine inipar
********************************************************************
*     KTABS  : nombre de points pour MFTNAF
*     KOFF   : offset pour la fenetre glissante
*     NEPS   : rapport pour la precision de recherche des freq.
*     NTERM  : nombre de termes recherches
*     ISTUD  : type d'etude demandee
*               1 : long terme cond. initiales fixees
*               2 : section y varie  
*               3 : section t varie  
*     NTRA   : nombre de tranches dans une etape
*             cas typique NTRA = 100 pour KOFF = 100 
*     nomfpar  : nom du fichier de parametres
*     nomfout  : nom du fichier de sortie
*     nomfsol  : nom du fichier de la solution
*     nomfdat  : nom du fichier de donnees
*     DTOUR    : longeur d un tour  dans les unites considerees
*     T0       : date initiale
*     XH       : pas (positif ou negatif)
*     ICPLX    : 1 donnees complexes  (pas d autre cas)
*     IW       : 1 fenetre sinon 0
*     ICOL     : nombre de colonnnes du fichier d entree
*     ICX      : indice de la colonne des x
*     ICY      : indice de la colonne des y
*     NULIN    : nombre de lignes a sauter au depart
*******************************************************************
      implicit double precision (a-h,o-y)
      character*40 nomfpar,nomfout,nomfdat,nomfsol,nomftab  
*-----------------tabeaux  
      DIMENSION TFM1(10),TFM2(10),TNT(10)
*-----------------parametres variables obtenus par init
      COMMON/CDATA/XH,T0,UNIANG,FREFON,ICPLX,IW
      COMMON/CSTD/DTOUR,KTABS,NEPS,NTERM,
     $ NULIN,ICOL,ICX,ICY,
     $ IPRNAF,IFTES,IFSAV,IFCOMP,IFRES,IFSTAB,
     $ nomfpar,nomfout,nomfdat,nomfsol,nomftab,
     $ IFMODT,TFM1,TFM2,TNT
      NAMELIST/NAMSTD/DTOUR,T0,XH,KTABS,NEPS,NTERM,
     $ ICPLX,IW,NULIN,ICOL,ICX,ICY,
     $ IPRNAF,IFTES,IFSAV,IFCOMP,IFRES,IFSTAB,
     $ nomfpar,nomfout,nomfdat,nomfsol,nomftab,
     $ IFMODT,TFM1,TFM2,TNT
      open (10, file = 'nafaut.par',form='formatted',status='UNKNOWN')
      read(10,NAMSTD)  
      write(*,*) 'lashfasahf'
      end

      subroutine prtpar(NFPRT)
********************************************************************
*   imprime les parametres sur le fichier nomfpar
*******************************************************************
      implicit double precision (a-h,o-y)
      character*40 nomfpar,nomfout,nomfdat,nomfsol,nomftab  
      DIMENSION TFM1(10),TFM2(10),TNT(10)
*-----------------parametres variables obtenus par init
      COMMON/CDATA/XH,T0,UNIANG,FREFON,ICPLX,IW
      COMMON/CSTD/DTOUR,KTABS,NEPS,NTERM,
     $ NULIN,ICOL,ICX,ICY,
     $ IPRNAF,IFTES,IFSAV,IFCOMP,IFRES,IFSTAB,
     $ nomfpar,nomfout,nomfdat,nomfsol,nomftab,
     $ IFMODT,TFM1,TFM2,TNT
      write (nfprt,*) 'nomfpar = ', nomfpar 
      write (nfprt,*) 'nomfout = ', nomfout
      write (nfprt,*) 'nomfdat = ', nomfdat 
      write (nfprt,*) 'nomfsol = ', nomfsol 
      write (nfprt,*) 'nomftab = ', nomftab 
      write (nfprt,*) 'DTOUR   = ', DTOUR 
      write (nfprt,*) 'T0      = ', T0 
      write (nfprt,*) 'XH      = ', XH   
      write (nfprt,*) 'KTABS   = ', KTABS 
      write (nfprt,*) 'NEPS    = ', NEPS 
      write (nfprt,*) 'NTERM   = ', NTERM  
      write (nfprt,*) 'ICPLX   = ', ICPLX 
      write (nfprt,*) 'IW      = ',IW 
      write (nfprt,*) 'NULIN   = ', NULIN  
      write (nfprt,*) 'ICOL    = ', ICOL 
      write (nfprt,*) 'ICX     = ', ICX   
      write (nfprt,*) 'ICY     = ',ICY     
      write (nfprt,*) 'IPRNAF  = ', IPRNAF 
      write (nfprt,*) 'IFTES   = ',IFTES    
      write (nfprt,*) 'IFSAV   = ', IFSAV   
      write (nfprt,*) 'IFCOMP  = ',IFCOMP   
      write (nfprt,*) 'IFRES   = ', IFRES  
      write (nfprt,*) 'IFSTAB  = ', IFSTAB  
      write (nfprt,*) 'IFMODT  = ', IFMODT  
      write (nfprt,*) 'TFM1  = ', (TFM1(i),i=1,IFMODT)  
      write (nfprt,*) 'TFM2  = ', (TFM2(i),i=1,IFMODT)  
      write (nfprt,*) 'TNT  = ', (TNT(i),i=1,IFMODT)  
      end





      SUBROUTINE SAVSOL(NFSOL)
************************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Y)
      IMPLICIT COMPLEX*16(Z)
      PARAMETER (NFR=100)
      COMMON/CDATA/XH,T0,UNIANG,FREFON,ICPLX,IW
      COMMON/CGRAM/ZAMP(NFR),ZALP(NFR,NFR),TFS(NFR),NFS
      WRITE (NFSOL,*) UNIANG
      WRITE (NFSOL,*) NFS
      DO I=1,NFS
         AMP = ABS(ZAMP(I))
         WRITE (NFSOL,1000) I, TFS(I),AMP,DREAL(ZAMP(I)),DIMAG(ZAMP(I))
      ENDDO
1000  format (I5,F21.14,D14.6,2D20.12)
      END

      SUBROUTINE SAVTAB(KTABS,ZTABS,nomftab)
************************************************************************
      implicit double precision (a-h, o-y)
      implicit complex*16 (z)
      character*40 nomftab 
      dimension ztabs(0:ktabs)
      open (31,file=nomftab, status = 'unknown')
      DO I=0,KTABS
         WRITE (31,*) DREAL(ZTABS(I)), DIMAG(ZTABS(I))
      ENDDO
      close (31)
      END

      subroutine loadsol(ktabs,ztabs,nomfdat,icx,icy,icol,nulin)
*******************************************************
*       remplit un tableau ztabs(0:ktabs) avec la
*       solution 
*       icx et icy sont les indice des colonnes ou se trouvent 
*       partie reelle et partie imaginaire de la solution
*       dans le fichier  nomfich
*       icol est le nombre de colonnes du fichier nomfich (icol <= 100)
*      26/4/92
********************************************************
      implicit double precision (a-h, o-y)
      implicit complex*16 (z)
      character*40 nomfdat 
      dimension ztabs(0:ktabs), x(100)
      open (30,file=nomfdat, status = 'old')
*------- saut des lignes non utiles
      do i = 1, nulin
         read(30,*)  
      enddo
*------- lecture
      do i = 0, ktabs
         read(30,*) (x(j),j=1,icol)
         ztabs(i)=cmplx(x(icx),x(icy))
      enddo
      close(30)
      end



     
      SUBROUTINE COMPSO (NFOUT,NFS,TFSA,ZAMPA,TFSB,ZAMPB)
*-----------------------------------------------------------------------
*     COMPSO
*                compare les deux solutions A et B
*
*     J. Laskar 25/5/89
*               revu le 17/12/90
*-----------------------------------------------------------------------
      IMPLICIT DOUBLE PRECISION (A-H,O-Y)
      IMPLICIT COMPLEX*16 (Z)
*-----------------------------------------------------------------------
      DIMENSION TFSA(NFS),ZAMPA(NFS)
      DIMENSION TFSB(NFS),ZAMPB(NFS)
      COMMON/CPRINT/NFPRT,IPRT
*-----------! comparaison des solutions
      WRITE(NFOUT,*)'TESSOL: EDITION COMPARAISON,NFS= ',NFS
      DO IFR = 1, NFS
         RO=SQRT(DREAL(ZAMPA(IFR))**2+DIMAG(ZAMPA(IFR))**2)
         WRITE(NFOUT,1000) IFR, TFSA(IFR),  TFSB(IFR),
     $         TFSA(IFR) - TFSB(IFR),RO
      ENDDO
1000  FORMAT ( 1X, I3,3F19.12, D17.9)
      END





