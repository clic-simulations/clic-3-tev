* ************************************************************************
*   NAFF.0.82 Numerical analysis of fundamental frequencies
* 
*            (27 janvier 1996)
*  
*   (c) Jacques Laskar 
*       Astronomie et Systemes Dynamiques
*       Bureau des Longitudes
*       75006 Paris
*       email : laskar@bdl.fr
*
*    main references : 
*    Laskar, J.: The chaotic motion of the solar system. A numerical
*    estimate of the size of the chaotic zones,Icarus,88,(1990),266--291
*
*
***************************************************************************
*  This programm  cannot be copied, 
*  distributed nor modified without the agreement of the author.
***************************************************************************
*
*                        PROGRAMME D'ANALYSE DE FOURIER AUTOMATIQUE
*   -- NAFF --           CALCULE UN TERME A LA FOIS ET LE RETRANCHE
*                        A TABS
*                        NOUVELLE VERSION 26/9/87
*                        MODIFIEE POUR NFS LE 18/10/87
*                        MODIFIEE LE 10/4/88 POUR TRAITER
*                        A LA FOIS UN TABLEAU REEL OU COMPLEXE
*                        CALCUL BATCH SUR CIRCE
* 8/10/90 modif pour remettre sur VP (fortran standard)
* 13/12/90   modif pour rendre plus modulaire
* 18/12/90   modif il n'y a plus de dimensions a regler dans
*            les sous programmes. TABS(2,*) au lieu de TABS(2,0:KTABS)
*            NFR est encore en PARAMETER, mais est rarement a changer.
*
*  il faut remplacer le * des tableaux
*       dimensiones T(*) ou T(n,*) par T(1) ou T(n,1)
*   19/12/90
*  16/4/91   compilation separee de NAFF en une bibliotheque
*  30/4/91   traitement du cas ou on retrouve la meme frequence
*  27/1/96   plusieurs fenetres
************************************************************************
*-----------------------------------------------------------------------
*-----------------------------------------------------------------------
*           ROUTINES DE MFTNAF
*-----------------------------------------------------------------------
*-----------------------------------------------------------------------
      SUBROUTINE MFTNAF (KTABS,NTERM,EPS)
*-----------------------------------------------------------------------
*     MFTNAF
*                calcule une approximation quasi periodique
*                de la fonction tabulee dans TABS(2,0:KTABSX)
*
*     KTABS : TABS(2,0:KTABSX)  TABLEAU DES DONNEES (REEL,IMAG)
*       KTABS TRANSMIS EN ARGUMENT  KTABSX = KTABS MAXI
*
*     NTERM               : nombre de termes recherches
*
*
*     EPS                 : precision avec laquelle on recherche 
*                           les frequences
*
*     COMMON/CDATA/  
*           XH            :pas entre deux donnees 
*                          dans une unite de temps quelconque
*                          la duree de l'intervalle est alors XH*KTABS
*           T0            :date de depart des donnees
*           UNIANG        : unite d'angle = 1RD=UNIANG unite d'angle
*                          (si l'unite est en secondes d'arc
*                              UNIANG=206264.80624709D0 )
*           FREFON        : frequence fondamentale
*                           FREFON=2*PI/(KTABS*XH) ou en secondes
*                           FREFON=360.D0*3600.D0/(KTABS*XH)
*           ICPLX         :  0 si la fonction est reelle, 1 sinon
*
*     COMMON/CGRAM/   
*           NFS           : nombre de frequences entierement determinees
*                           ainsi que leur amplitude
*           TFS(NFR)      : tableau des frequences
*           ZAMP(NFR)     : amplitude complexe des termes
*           ZALP(NFR,NFR) : tableau de changement de base
*
*-----------------------------------------------------------------------
*-----------------------------------------------------------------------
      IMPLICIT DOUBLE PRECISION (A-H,O-Y)
      IMPLICIT COMPLEX*16 (Z)
*----------! nombre maximal de frequences
      PARAMETER (NFR=100)
      COMMON/CTABS/TABS(2,1)
      COMMON/CGRAM/ZAMP(NFR),ZALP(NFR,NFR),TFS(NFR),NFS
      COMMON/CPRINT/NFPRT,IPRT
      COMMON/CDATA/XH,T0,UNIANG,FREFON,ICPLX,IW
*-----------------------------------------------------------------------
      TOL = 1.D-4
      STAREP=ABS(FREFON)/3
      CALL INIFRE
      DO 10 I=1,NTERM
         CALL FFTMAX(KTABS,FR)
         CALL FREFIN(KTABS,FR,A,B,RM,STAREP,EPS)
         CALL FRETES(FR,IFLAG,TOL,NUMFR)
         IF (IFLAG.EQ.0) GOTO 999
         IF (IFLAG.EQ.1) THEN
            CALL GRAMSC(KTABS,FR,A,B)
            NFS=NFS+1
         ENDIF
         IF (IFLAG.EQ.-1) THEN
            CALL MODFRE(KTABS,NUMFR,A,B)
         ENDIF
10    CONTINUE
999   CONTINUE
      END


      SUBROUTINE PRTABS(KTABS,ZTABS,IPAS)
*-----------------------------------------------------------------------
*     IMPRESSION DE TABS
*-----------------------------------------------------------------------
      IMPLICIT DOUBLE PRECISION (A-H,O-Y)
      IMPLICIT COMPLEX*16 (Z)
      DIMENSION ZTABS(0:KTABS)
      COMMON/CPRINT/NFPRT,IPRT
      IF (IPRT.EQ.1) THEN
         DO 10 I=0,KTABS,IPAS
            WRITE(NFPRT,1000) I,DREAL(ZTABS(I)),DIMAG(ZTABS(I))
10       CONTINUE
      ENDIF
1000  FORMAT (1X,I6,2X,2D20.6)
      END


      SUBROUTINE SMOY(KTABS,ZM)
*-----------------------------------------------------------------------
*    CALCUL DES MOYENNNES DE TABS ET SOUSTRAIT DE CHACUNE
*    la valeur moyenne
*-----------------------------------------------------------------------
      IMPLICIT DOUBLE PRECISION (A-H,O-Y)
      IMPLICIT COMPLEX*16 (Z)
      COMMON/CTABS/ZTABS(1)
*-----------------------------------------------------------------------
      ZM=DCMPLX(0.D0,0.D0)
      DO 30 I=1,KTABS+1
         ZM=ZM+ZTABS(I)
30    CONTINUE
      ZM=ZM/(KTABS+1)
      DO 40 I=1,KTABS+1
         ZTABS(I)=ZTABS(I)-ZM
40    CONTINUE
      END


      SUBROUTINE FRETES (FR,IFLAG,TOL,NUMFR)
************************************************************************
*     test de la nouvelle frequence trouvee par rapport aux anciennes.
*     la distance entre deux freq  doit etre de FREFON
*
*     renvoie IFLAG =  1 si le test reussit (on peut continuer)
*             IFLAG =  0 si le test echoue (il vaut mieux s'arreter)
*             IFLAG = -1 si TEST < ECART, mais TEST/ECART < TOL
*                        (on retrouve pratiquement la meme frequence 
*                         d'indice NFR)
*     TOL (entree) tolerance ( 1.D-7) est un bon exemple
*     NFR (sortie) indice de la frequence retrouvee
*
************************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Y)
      IMPLICIT COMPLEX*16(Z)
      PARAMETER (NFR=100)
      COMMON/CGRAM/ZAMP(NFR),ZALP(NFR,NFR),TFS(NFR),NFS
      COMMON/CDATA/XH,T0,UNIANG,FREFON,ICPLX,IW
      IFLAG = 1
      ECART = ABS(FREFON)  
      DO 100 I = 1, NFS
         TEST = ABS(TFS(I) - FR)
         IF (TEST.LT.ECART) THEN
            IF (TEST/ECART.LT.TOL) THEN
               IFLAG = -1
               NUMFR   = I
               WRITE(*,*) 'TEST/ECART = ', TEST/ECART ,'  ON CONTINUE'
               GOTO 999
            ELSE
               IFLAG = 0
               WRITE(*,*)
               WRITE(*,*) 'TEST = ', TEST, 'ECART = ',ECART
               WRITE(*,*) 'FREQUENCE   FR = ', FR, ' TROP PROCHE  DE  ',
     $                  TFS(I)
            ENDIF
         ENDIF
100   CONTINUE
999   CONTINUE
      END

      SUBROUTINE ZTPOW (N,N1,ZT,ZA,ZAST)
*-----------------------------------------------------------------------
*     ZTPOW   calcule  ZT(i) = ZAST*ZA**i en vectoriel
*             ZT(N)
*-----------------------------------------------------------------------
      IMPLICIT DOUBLE PRECISION (A-H,O-Y)
      IMPLICIT COMPLEX*16 (Z)
      DIMENSION ZT(N)
      IF (N.LT.1) THEN
         WRITE (*,*) 'DANS ZTPOW, N = ', N
         RETURN
      END IF
*----------! 
      ZT(1) = ZAST*ZA
      DO 10 I = 2,N1
         ZT(I) = ZT(I-1)*ZA
10    CONTINUE
      ZT1 = ZT(N1)/ZAST
      ZINC= 1
      INC =0
      NT = N/N1  
      DO 20 IT = 2, NT
         ZINC = ZINC*ZT1
         INC  = INC + N1
         DO 30 I = 1, N1
            ZT(INC +I) = ZT(I)*ZINC
30       CONTINUE
20    CONTINUE
      ZINC = ZINC*ZT1
      INC  = INC + N1
      NX = N-NT*N1
      DO 40 I = 1, NX
         ZT(INC +I) = ZT(I)*ZINC
40    CONTINUE
      END


            



      SUBROUTINE TESSOL (KTABS,EPS,TFSR,ZAMPR)
*-----------------------------------------------------------------------
*     TESSOL
*                sous programme pour verifier la precision des
*                solutions obtenues par analyse de FOURIER
*                ON analyse a nouveau la solution et on compare 
*                les termes
*
*     J. Laskar 25/5/89
*-----------------------------------------------------------------------
      IMPLICIT DOUBLE PRECISION (A-H,O-Y)
      IMPLICIT COMPLEX*16 (Z)
*************************************************************
*----------! nombre maximal de frequences
      PARAMETER (NFR=100)
      DIMENSION TFST(NFR),ZAMPT(NFR),ZALPT(NFR,NFR)
      DIMENSION TFSR(NFR),ZAMPR(NFR)
*----------! tableaux de travail
      COMMON/CPOW/ZT(1)
      COMMON/CTABS/ZTABS(1)
      COMMON/CGRAM/ZAMP(NFR),ZALP(NFR,NFR),TFS(NFR),NFS
      COMMON/CPRINT/NFPRT,IPRT
      COMMON/CDATA/XH,T0,UNIANG,FREFON,ICPLX
*----------! TFS    est le tableau final des frequences
*----------! ZAMP   tableau des amplitudes complexes
*----------! ZALP   matrice de passage de la base orthonormalisee
*----------! NFS    nombre de freq.deja determinees
*************************************************************
      OFFSET=KTABS*XH
*----------! initialisation de TABS
      DO 30 IT=1,KTABS+1
         ZTABS(IT)=DCMPLX(0.D0,0.D0)
30    CONTINUE
*----------! calcul de la nouvelle solution
      ZI=DCMPLX(0.D0,1.D0)
      DO 10 IFR = 1, NFS
         ZOM=TFS(IFR)/UNIANG*ZI
         ZA=ZAMP(IFR)
*-----------! attention ici (cas reel) on aura aussi le terme conjugue
*-----------! qu'on ne calcule pas. le terme total est
*-----------!       2*Re(ZAMP(i)*exp(ZI*TFS(i)*T) )
*-----------! on rajoute la contribution totale du terme TFS(i) dans TAB2
         IF (ICPLX.EQ.1) THEN
            ZEX = ZA*EXP ((T0+OFFSET-XH)*ZOM)
            ZINC= EXP (XH*ZOM)
            CALL  ZTPOW (KTABS+1,64,ZT,ZINC,ZEX)
            DO 20 IT=0,KTABS
               ZTABS(IT+1)=ZTABS(IT+1)+ZT(IT+1)
20          CONTINUE
         ELSE
            ZEX = ZA*EXP ((T0+OFFSET-XH)*ZOM)
            ZINC= EXP (XH*ZOM)
            CALL  ZTPOW (KTABS+1,64,ZT,ZINC,ZEX)
            DO 40 IT=0,KTABS
               ZTABS(IT+1)=ZTABS(IT+1)+DREAL(ZT(IT+1))
40          CONTINUE
         END IF
10    CONTINUE
*-----------! sauvegarde dans des tableaux annexes de la solution
      DO 110 IFR = 1, NFS
         TFST(IFR) = TFS(IFR)
         ZAMPT(IFR) = ZAMP(IFR)
		 DO 111 JFR = 1,NFS
		    ZALPT(IFR,JFR) = ZALP(IFR,JFR)
111      CONTINUE
110   CONTINUE
*-----------! nouveau calcul de la solution
      NTERM=NFS
****DEBUG      CALL PRTABS(KTABS,ZTABS,KTABS/10)
      CALL MFTNAF (KTABS,NTERM,EPS)
*-----------! resultats
      DO 114 IFR = 1, NFS
         TFSR(IFR) = TFS(IFR)
         ZAMPR(IFR) = ZAMP(IFR)
114   CONTINUE
*-----------! restitution  de la solution
      DO 112 IFR = 1, NFS
         TFS(IFR) = TFST(IFR)
         ZAMP(IFR) = ZAMPT(IFR)
		 DO 113 JFR = 1,NFS
		    ZALP(IFR,JFR) = ZALPT(IFR,JFR)
113      CONTINUE
112   CONTINUE
      END


      SUBROUTINE INIFRE
*************************************************************************
*     REMET A ZERO TOUT CE QUI EST DANS LE COMMON CGRAM
*     UTILE QUAND ON BOUCLE SUR PLUSIEURS cas
*
************************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Y)
      IMPLICIT COMPLEX*16(Z)
      PARAMETER (NFR=100)
      COMMON/CGRAM/ZAMP(NFR),ZALP(NFR,NFR),TFS(NFR),NFS
      COMMON/CDATA/XH,T0,UNIANG,FREFON,ICPLX,IW
      DO 100 I = 1, NFR
         TFS(I) = 0.D0
         ZAMP(I) = DCMPLX(0.D0,0.D0)
         DO 110 J = 1, NFR
            ZALP(I,J) = DCMPLX(0.D0,0.D0)
110      CONTINUE
100   CONTINUE
      NFS = 0
      END


      SUBROUTINE FFTMAX (KTABS,FR)
*-----------------------------------------------------------------------
*     FFTMAX          MODIF NICE 90 (J. Laskar)
*                     modif 13/12/90
*                     VERSION STANDARD. (four1)
*                     ANALYSE DE FOURIER RAPIDE
*                     RECHERCHE D'UN MAXIMUM DE SPECTRE POUR UNE FREQ. F
************************************************************************
*     on utilisera cela ulterieurement (13/12/90)
*     NFR11, NFR22 NUMERO MIN ET MAX DES FREQUENCES RECHERCHEES
*                   NFR1 ET NFR2 INCLUSES
*                -NRTAB<NFR1<NFR2<NRTAB
*
*     RTAB(NRTAB,2) EN 1 AMPLITUDE DE SFREQUENCES POSITIVES
*                      2 AMPLITUDE DES FREQUENCES NEGATIVES
*     DISTANCE ENTRE DEUX LIGNES  FREFON
*-----------------------------------------------------------------------
      IMPLICIT DOUBLE PRECISION (A-H,O-Y)
	IMPLICIT COMPLEX*16 (Z)
      COMMON/CTABS/ZTABS(1)
      COMMON/CWIN/TWIN(1)
      COMMON/CWORK1/TAB(2,1)
      COMMON/CWORK2/RTAB(1)
      COMMON/CPRINT/NFPRT,IPRT
      COMMON/CDATA/XH,T0,UNIANG,FREFON,ICPLX
******************* 
      CALL PUISS2(KTABS,KTABS2)
      FREFO2=(FREFON*KTABS)/KTABS2
      IF (IPRT.EQ.1) WRITE(NFPRT,*) 
     $      'KTABS2= ',KTABS2,'   FREFO2= ', FREFO2
*****************! CALCUL DES FREQUENCES 
      ISG=-1
      IDV=KTABS2
      IPAS=1
      DO 10 I=1,KTABS2
         TAB(1,I)=DREAL(ZTABS(I))*TWIN(I)
         TAB(2,I)=DIMAG(ZTABS(I))*TWIN(I)
10    CONTINUE
      CALL FOUR1(TAB,KTABS2,ISG)
      DO 20 I=1,KTABS2
         RTAB(I)=SQRT(TAB(1,I)**2+TAB(2,I)**2)/IDV
20    CONTINUE
***********************        CALL MODTAB(KTABS2,RTAB)
      CALL MAXX(KTABS2,RTAB,INDX)
      IF (INDX.LE.KTABS2/2) THEN
          IFR = INDX-1
      ELSE
          IFR = INDX-1-KTABS2
      ENDIF
      FR = IFR*FREFO2*IPAS
      IF (IPRT.EQ.1)  WRITE (NFPRT,*) 
     $    'IFR = ',IFR,' FR = ', FR, 'RTAB = ',RTAB(INDX)
      END


      SUBROUTINE FOUR1(DATA,NN,ISIGN)
*************** fourier rapide de NUMERICAL RECIPES
	IMPLICIT DOUBLE PRECISION (A-H,O-Y)
      DIMENSION DATA(*)
      N=2*NN
      J=1
      DO 11 I=1,N,2
        IF(J.GT.I)THEN
          TEMPR=DATA(J)
          TEMPI=DATA(J+1)
          DATA(J)=DATA(I)
          DATA(J+1)=DATA(I+1)
          DATA(I)=TEMPR
          DATA(I+1)=TEMPI
        ENDIF
        M=N/2
1       IF ((M.GE.2).AND.(J.GT.M)) THEN
          J=J-M
          M=M/2
        GO TO 1
        ENDIF
        J=J+M
11    CONTINUE
      MMAX=2
2     IF (N.GT.MMAX) THEN
        ISTEP=2*MMAX
        THETA=6.28318530717959D0/(ISIGN*MMAX)
        WPR=-2.D0*DSIN(0.5D0*THETA)**2
        WPI=DSIN(THETA)
        WR=1.D0
        WI=0.D0
        DO 13 M=1,MMAX,2
          DO 12 I=M,N,ISTEP
            J=I+MMAX
            TEMPR=SNGL(WR)*DATA(J)-SNGL(WI)*DATA(J+1)
            TEMPI=SNGL(WR)*DATA(J+1)+SNGL(WI)*DATA(J)
            DATA(J)=DATA(I)-TEMPR
            DATA(J+1)=DATA(I+1)-TEMPI
            DATA(I)=DATA(I)+TEMPR
            DATA(I+1)=DATA(I+1)+TEMPI
12        CONTINUE
          WTEMP=WR
          WR=WR*WPR-WI*WPI+WR
          WI=WI*WPR+WTEMP*WPI+WI
13      CONTINUE
        MMAX=ISTEP
      GO TO 2
      ENDIF
      RETURN
      END

      SUBROUTINE FOUR2(ZDATA,NN,ISIGN)
*-----------------------------------------------------------------------
*  appel de fourier rapide sur le modele de NUMERICAL RECIPES
*  DATA(2,NN) tableau complexe des donnees en entree
*  en sortie coef de Fourier
*-----------------------------------------------------------------------
	IMPLICIT DOUBLE PRECISION (A-H,O-Y)
	IMPLICIT COMPLEX*16 (Z)
	PARAMETER (NAUX1 = 20000, NAUX2 = 20 000)
      DIMENSION ZDATA(*)
      DIMENSION AUX1(NAUX1), AUX2(NAUX2)
      inc1x =1
      inc2x =1
      inc1y =1
      inc2y =1
      m =1
      n = NN
      IF (ISIGN.EQ.1) THEN
         ISIGN = -1
         SCALE = 1.D0
      ELSE
         ISIGN =  1
         SCALE = 1.D0
      ENDIF
*      CALL DCFT(1,ZDATA,INC1X,INC2X,ZDATA,INC1Y,INC2Y,N,M,ISIGN,SCALE,
*     $          AUX1,NAUX1,AUX2,NAUX2)
*      CALL DCFT(0,ZDATA,INC1X,INC2X,ZDATA,INC1Y,INC2Y,N,M,ISIGN,SCALE,
*     $          AUX1,NAUX1,AUX2,NAUX2)
      END

      SUBROUTINE PUISS2(NT,N2)
********************************************
*    calcule la plus grande puissance de 2
*    contenue dans NT
********************************************
      INTEGER N,N2,NT
      N=NT
      IF (N.EQ.0) THEN
         N2=0
         RETURN
      ENDIF
      N2=1
100   CONTINUE
      IF (N.GE.2) THEN
         N2=N2*2
         N=N/2
         GOTO 100
      ENDIF
      END
	  
      SUBROUTINE MAXX(N,T,INDX)
********************************************
*    calcule le max et l'indice du max
********************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Y)
      DIMENSION T(N)
      VMAX=-1.D30
      DO 11 J=1,N
         IF (T(J).GT.VMAX) THEN
            VMAX=T(J)
            INDX=J
         ENDIF
11    CONTINUE
      END

      SUBROUTINE MODTAB(N,T)
*****************************************
*    suppression de certains termes
*    a modifier ulterieurement (13/12/90)
*****************************************
      DIMENSION T(0:N)
      NOK = 40
      DO 10 I = 0,NOK
         T(I) = 0
         T(N-I) = 0
10    CONTINUE
      END


      SUBROUTINE MODFRE(KTABS,NUMFR,A,B)
*-----------------------------------------------------------------------
*     permet de modifier une amplitude deja calculee quand
*     on retrouve le meme terme, a TOL pres dans le developpement
* 
*     NUMFR     indice de la frequence existant deja
*     A,B       PARTIES REELLES ET IMAGINAIRES DE L'AMPLITUDE de la modif 
*               a apporter a l'amplitude de la frequence NUMFR
*
*     AUTRES PARAMETRES TRANSMIS PAR LE COMMON/CGRAM/ZAMP,ZALP,TFS,NFS
*
*     30/4/91
*-----------------------------------------------------------------------
      IMPLICIT DOUBLE PRECISION (A-H,O-Y)
      IMPLICIT COMPLEX*16 (Z)
      PARAMETER (NFR=100)
*----------! NOMBRE MAXIMAL DE FREQUENCES
      COMMON/CPOW/ZT(1)
      COMMON/CTABS/ZTABS(1)
      COMMON/CGRAM/ZAMP(NFR),ZALP(NFR,NFR),TFS(NFR),NFS
      COMMON/CPRINT/NFPRT,IPRT
      COMMON/CDATA/XH,T0,UNIANG,FREFON,ICPLX
*----------! TFS EST LE TABLEAU FINAL DES FREQUENCES
*----------! ZAMP   TABLEAU DES AMPLITUDES COMPLEXES
*----------! ZALP   MATRICE DE PASSAGE DE LA BASE ORTHONORMALISEE
*----------! NFS    NOMBRE DE FREQ.DEJA DETERMINEES
         ZI = DCMPLX(0.D0,1.D0)
         ZOM=TFS(NUMFR)/UNIANG*ZI
         ZA=DCMPLX(A,B)
         write(*,*) 'correction de  IFR = ',NUMFR,
     $               'amplitude  = ', ABS(ZA)
*-----------! L' AMPLITUDES Du TERMES est CORRIGEES
*-----------! ATTENTION ICI (CAS REEL) ON AURA AUSSI LE TERME CONJUGUE
*-----------! QU'ON NE CALCULE PAS. LE TERME TOTAL EST
*-----------!       2*RE(ZAMP(I)*EXP(ZI*TFS(I)*T) )
         ZAMP(NUMFR)=ZAMP(NUMFR)+ZA
         IF (IPRT.EQ.1)  WRITE (NFPRT,1000) TFS(NUMFR),
     $       ABS(ZAMP(NUMFR)),DREAL(ZAMP(NUMFR)),
     $   DIMAG(ZAMP(NUMFR)),ATAN2(DIMAG(ZAMP(NUMFR)),DREAL(ZAMP(NUMFR)))
1000     FORMAT (1X,F14.9,4D16.8)
*-----------! ON RETIRE LA CONTRIBUTION DU TERME TFS(NUMFR) DANS TABS
       IF (ICPLX.EQ.1) THEN
         ZEX = ZA*EXP(ZOM*(T0-XH))
         ZINC=EXP(ZOM*XH)
         CALL  ZTPOW (KTABS+1,64,ZT,ZINC,ZEX)
         DO 80 IT=0,KTABS
            ZTABS(IT+1)=ZTABS(IT+1)- ZT(IT+1) 
80       CONTINUE
       ELSE
         ZEX = ZA*EXP(ZOM*(T0-XH))
         ZINC=EXP(ZOM*XH)
         CALL  ZTPOW (KTABS+1,64,ZT,ZINC,ZEX)
         DO 81 IT=0,KTABS
            ZTABS(IT+1)=ZTABS(IT+1)- DREAL(ZT(IT+1)) 
81       CONTINUE
       ENDIF
      END


      SUBROUTINE GRAMSC(KTABS,FS,A,B)
*-----------------------------------------------------------------------
*     GRAMSC   ORTHONORMALISATION  PAR GRAM-SCHIMDT DE LA BASE DE FONCTI
*               CONSTITUEE DES TERMES PERIODIQUES TROUVES PAR ANALYSE
*               SPECTRALE .
*     FS        FREQUENCE EN "/AN
*     A,B       PARTIES REELLES ET IMAGINAIRES DE L'AMPLITUDE
*
*     AUTRES PARAMETRES TRANSMIS PAR LE COMMON/CGRAM/ZAMP,ZALP,TFS,NFS
*
*     MODIFIE LE 26 9 87 POUR LES FONCTIONS REELLES   J. LASKAR
*-----------------------------------------------------------------------
      IMPLICIT DOUBLE PRECISION (A-H,O-Y)
      IMPLICIT COMPLEX*16 (Z)
      PARAMETER (NFR=100)
*----------! NOMBRE MAXIMAL DE FREQUENCES
      DIMENSION ZTEE(NFR)
      COMMON/CPOW/ZT(1)
      COMMON/CTABS/ZTABS(1)
      COMMON/CGRAM/ZAMP(NFR),ZALP(NFR,NFR),TFS(NFR),NFS
      COMMON/CPRINT/NFPRT,IPRT
      COMMON/CDATA/XH,T0,UNIANG,FREFON,ICPLX
*----------! TFS EST LE TABLEAU FINAL DES FREQUENCES
*----------! ZAMP   TABLEAU DES AMPLITUDES COMPLEXES
*----------! ZALP   MATRICE DE PASSAGE DE LA BASE ORTHONORMALISEE
*----------! ZTEE   TABLEAU DE TRAVAIL
*----------! NFS    NOMBRE DE FREQ.DEJA DETERMINEES
*----------! CALCUL DE ZTEE(I)=<EN,EI>
      DO 10 I =1,NFS
        CALL PROSCAA(KTABS,FS,TFS(I),ZTEE(I))
10    CONTINUE
*----------! NF EST LE NUMERO DE LA NOUVELLE FREQUENCE
      NF=NFS+1
      ZTEE(NF)=DCMPLX(1.D0,0.D0)
*----------! CALCUL DE FN = EN - SOM(<EN,FI>FI) QUI EST ORTHOGONAL AUX F
      TFS(NF)=FS
      DO 20 K=1,NFS
         DO 30 I=K,NFS
            DO 40 J=1,I
               ZALP(NF,K)=ZALP(NF,K)-DCONJG(ZALP(I,J))*ZALP(I,K)*ZTEE(J)
40          CONTINUE
30       CONTINUE
20    CONTINUE
      ZALP(NF,NF)=DCMPLX(1.D0,0.D0)
*----------! ON REND LA NORME DE FN = 1
      DIV=1.D0
      ZDIV=DCMPLX(0.D0,0.D0)
      DO 50 I=1,NF
         ZDIV=ZDIV+DCONJG(ZALP(NF,I))*ZTEE(I)
50    CONTINUE
      DIV=SQRT(ABS(ZDIV))
      IF (IPRT.EQ.1) WRITE(NFPRT,*) 'ZDIV= ',ZDIV,' DIV= ',DIV
      DO 60 I=1,NF
         ZALP(NF,I)=ZALP(NF,I)/DIV
60    CONTINUE
*-----------! F1,F2....., FN EST UNE BASE ORTHONORMEE
*-----------! ON RETIRE MAINTENANT A F  <F,FN>FN  (<F,FN>=<F,EN>)
      ZMUL=DCMPLX(A,B)/DIV
      ZI=DCMPLX(0.D0,1.D0)
      DO 70 I=1,NF
         ZOM=TFS(I)/UNIANG*ZI
         ZA=ZALP(NF,I)*ZMUL
*-----------! LES AMPLITUDES DES TERMES SONT CORRIGEES
*-----------! ATTENTION ICI (CAS REEL) ON AURA AUSSI LE TERME CONJUGUE
*-----------! QU'ON NE CALCULE PAS. LE TERME TOTAL EST
*-----------!       2*RE(ZAMP(I)*EXP(ZI*TFS(I)*T) )
         ZAMP(I)=ZAMP(I)+ZA
         IF (IPRT.EQ.1)  WRITE (NFPRT,1000) TFS(I),
     $       ABS(ZAMP(I)),DREAL(ZAMP(I)),
     $    DIMAG(ZAMP(I)),ATAN2(DIMAG(ZAMP(I)),DREAL(ZAMP(I)))
1000     FORMAT (1X,F14.9,4D16.8)
*-----------! ON RETIRE LA CONTRIBUTION TOTALE DU TERME TFS(I) DANS TABS
       IF (ICPLX.EQ.1) THEN
         ZEX = ZA*EXP(ZOM*(T0-XH))
         ZINC=EXP(ZOM*XH)
         CALL  ZTPOW (KTABS+1,64,ZT,ZINC,ZEX)
         DO 80 IT=0,KTABS
            ZTABS(IT+1)=ZTABS(IT+1)- ZT(IT+1) 
80       CONTINUE
       ELSE
         ZEX = ZA*EXP(ZOM*(T0-XH))
         ZINC=EXP(ZOM*XH)
         CALL  ZTPOW (KTABS+1,64,ZT,ZINC,ZEX)
         DO 81 IT=0,KTABS
            ZTABS(IT+1)=ZTABS(IT+1)- DREAL(ZT(IT+1)) 
81       CONTINUE
       ENDIF
70    CONTINUE
      END

      SUBROUTINE PROSCA(KTABS,F1,F2,ZP)
*-----------------------------------------------------------------------
*     PROSCA   CALCULE LE PRODUIT SCALAIRE  DE EXP(I*F1*T) PAR EXP (-I*F
*               SUR L'INTERVALLE [0:KTABS]  T=T0+XH*IT
*              CALCUL ANALYTIQUE
*     ZP        PRODUIT SCALAIRE COMPLEXE
*     F1,F2     FREQUENCES EN "/AN
*               REVU LE 26/9/87 J. LASKAR
*-----------------------------------------------------------------------
      IMPLICIT DOUBLE PRECISION (A-H,O-Y)
      IMPLICIT COMPLEX*16 (Z)
      COMMON/CDATA/XH,T0,UNIANG,FREFON,ICPLX
      ZI=DCMPLX(0.D0,1.D0)
*----------! FREQUENCES EN UNITE D'ANGLE PAR UNITE DE TEMPS
      FR1=F1/UNIANG
      FR2=F2/UNIANG
      T1 =T0
      T2 =T0+KTABS*XH
      ZF=ZI*(FR1-FR2)*(T2-T1)
      ZF1=ZI*(FR1-FR2)*T1
      ZF2=ZI*(FR1-FR2)*T2
      ZP=(EXP(ZF2)-EXP(ZF1))/ZF
*
      PI=2.D0*ATAN2(1.D0,0.D0)
      PI2=PI**2
      T=(T2-T1)/2.D0
      XT=(FR1-FR2)*T
      DIV=XT**2-PI2
      IF(ABS(DIV).LT.1.D-10) THEN
         WRITE (*,*) 'T0= ',T0,',XH= ',XH,',KTABS= ',KTABS
         WRITE (*,*) 'F1= ',F1,',F2= ',F2
         WRITE (*,*) 'T1= ',T1,',T2= ',T2,',T= ',T
         WRITE (*,*) 'FR1= ',FR1,',FR2= ',FR2,',XT= ',XT
         WRITE (*,*) ' DIV= ',DIV
      ELSE
         FAC=-PI2/DIV
         ZP=ZP*FAC
      ENDIF
      END


      SUBROUTINE FREFIN (KTABS,FR,A,B,RM, RPAS0,RPREC)
*-----------------------------------------------------------------------
*     FREFIN            RECHERCHE FINE DE LA FREQUENCE
*                       FR     FREQUENCE DE BASE EN "/AN  ENTREE ET SORT
*                       A      PARTIE REELLE DE L'AMPLITUDE     (SORTIE)
*                       B      PARTIE IMAGINAIRE DE L'AMPLITUDE (SORTIE)
*                       RM     MODULE DE L'AMPLITUDE            (SORTIE)
*                       RPAS0  PAS INITIAL EN "/AN
*                       RPREC  PRECISION DEMANDEE EN "/AN
*               REVU LE 26/9/87 J. LASKAR
*               MODIF LE 15/11/90 MAX PAR INTERP. QUAD.
*-----------------------------------------------------------------------
      IMPLICIT DOUBLE PRECISION (A-H,O-Y)
      EXTERNAL FUNC
      COMMON/CPRINT/NFPRT,IPRT
      COMMON/CDATA/XH,T0,UNIANG,FREFON,ICPLX
      COMMON/CFUNC/AF,BF,KTABSF
      X    = FR
      PASS = RPAS0
      EPS  = RPREC
	  KTABSF = KTABS
      CALL MAXIQUA(X,PASS,EPS,XM,YM,FUNC,IPRT,NFPRT)
      FR=XM
      A=AF
      B=BF
      RM=YM
      IF (IPRT.EQ.1) THEN
      WRITE (*,*)
      WRITE (NFPRT,1000) FR,RM,AF,BF
      ENDIF
1000  FORMAT (1X,F10.6,3D19.9)
      END


	  SUBROUTINE MAXIQUA(X,PASS,EPS,XM,YM,FUNC,IPRT,NFPRT)
************************************************************
*      CALCUL PAR INTERPOLATION QUADRATIQUE DU MAX
*      D'UNE FONCTION FUNC FOURNIE PAR L'UTILISATEUR
*      
*     X, PASS : LE MAX EST SUPPOSE ETRE SI POSSIBLE 
*               DANS X-PASS,X+PASS
*     EPS     : PRECISION AVEC LAQUELLE ON VA CALCULER
*               LE MAXIMUM
*     XM      : ABSCISSE DU MAX
*     YM      : YM=FUNC(XM)
*    IPRT     : 0: PAS D'IMPRESSION
*               1: IMPRESSION SUR LE FICHIER NFPRT
* 
*    FUNC(X)  :FONCTION DONNEE PAR L'UTILISATEUR
*
************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Y)
      PARAMETER (EPSI = 1.D-15)
      PAS = PASS
      X2 = X
	  Y2 = FUNC (X2)
	  X1 = X2 - PAS
	  X3 = X2 + PAS
	  Y1 = FUNC (X1)
	  Y3 = FUNC (X3)
110   CONTINUE	  
	  IF (PAS.LT.EPS) THEN
	     GOTO 100
	  ELSE
		 IF (IPRT.EQ.1) WRITE(NFPRT,1000) PAS,X2,Y1,Y2,Y3
********* test sur la precision interne de la machine
         IF (ABS (Y3-Y1).LT.EPSI) THEN
            WRITE (*,*) ' ATTENTION, Y3 - Y1 < EPSI '
            GOTO 100
         ENDIF
	     IF (Y1.LT.Y2.AND.Y3.LT.Y2) THEN
		    R2 = (Y1-Y2)/(X1-X2)
		    R3 = (Y1-Y3)/(X1-X3)
            A = (R2 - R3)/(X2-X3)
            B = R2 - A*(X1+X2)
            XX2 = -B/(2.D0*A)
            PAS = ABS (XX2-X2)
            IF (XX2.GT.X2) THEN
               X1 = X2
               Y1 = Y2
               X2 = XX2
               Y2 = FUNC(X2)
               X3 = X2 + PAS
               Y3 = FUNC(X3)
            ELSE
               X3 = X2
               Y3 = Y2
               X2 = XX2
               Y2 = FUNC(X2)
               X1 = X2 - PAS
               Y1 = FUNC(X1)
            ENDIF
	     ELSE
		    IF (Y1.GT.Y3) THEN
			   X2 = X1
			   Y2 = Y1
		    ELSE
			   X2 = X3
			   Y2 = Y3
			ENDIF
	        X1 = X2 - PAS
	        X3 = X2 + PAS
	        Y1 = FUNC (X1)
	        Y3 = FUNC (X3)
	     ENDIF
		 GOTO 110
      ENDIF
100   CONTINUE
      XM = X2
      YM = Y2
      IF (IPRT.EQ.1) THEN
         WRITE (NFPRT,*)
         WRITE (NFPRT,1000) PAS,XM,YM
      ENDIF
1000  FORMAT (1X, 2F12.8, 3D19.9)
      END
      DOUBLE PRECISION FUNCTION FUNC(X)
      IMPLICIT DOUBLE PRECISION (A-H,O-Y)
      COMMON/CFUNC/AF,BF,KTABSF
      CALL PROFRE(KTABSF,X,AF,BF,RMD)
      FUNC=RMD
      END


      SUBROUTINE PROFRE(KTABS,FS,A,B,RMD)
************************************************************************
*  CE SOUS PROG. CALCULE LE PRODUIT SCALAIRE DE EXP(-I*FS)*T PAR TAB(0:7
*  UTILISE LA METHODE D'INTEGRATION DE HARDY
*  LES RESULTATS SONT DANS A,B,RMD PARTIE REELLE, IMAGINAIRE, MODULE
*  FS EST DONNEE EN SECONDES PAR AN
*               REVU LE 26/9/87 J. LASKAR
************************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Y)
      IMPLICIT COMPLEX*16 (Z)
      COMMON/CPOW/ZT(1)
      COMMON/CTABS/ZTABS(1)
      COMMON/CWIN/TWIN(1)
      COMMON/CWORK1/ZTF(1)
      COMMON/CWORK2/TF2(1)
      COMMON/CDATA/XH,T0,UNIANG,FREFON,ICPLX
*
*------------------ CONVERSION DE FS EN RD/AN
      OM=FS/UNIANG
      ZI=DCMPLX(0.D0,1.D0)
*------------------ LONGUEUR DU TABLEAU A INTEGRER
*------------------ dans le cas reel meme chose 
      LTF=KTABS
      ANG0=OM*T0
      ANGI=OM*XH
      ZAC = EXP (-ZI*ANG0)
      ZINC= EXP (-ZI*ANGI)
      ZEX = ZAC/ZINC
      CALL  ZTPOW2(KTABS+1,64,ZT,ZTF,ZTABS,TWIN,ZINC,ZEX)
*------------------ TAILLE DU PAS
      H=1.D0/LTF
      CALL ZARDYD(ZTF,LTF+1,H,ZA)
      RMD=ABS(ZA)
      A = DREAL(ZA)
      B = DIMAG(ZA)
      END

      SUBROUTINE ZTPOW2 (N,N1,ZT,ZTF,ZTA,TW,ZA,ZAST)
*-----------------------------------------------------------------------
*     ZTPOW   calcule  ZTF(i) = ZTA(i)* TW(i)*ZAST*ZA**i en vectoriel
*             ZT(N1) zone de travail
*-----------------------------------------------------------------------
      IMPLICIT DOUBLE PRECISION (A-H,O-Y)
      IMPLICIT COMPLEX*16 (Z)
      DIMENSION ZT(N1)
      DIMENSION ZTF(N),ZTA(N),TW(N)
      IF (N.LT.1) THEN
         WRITE (*,*) 'DANS ZTPOW, N = ', N
         RETURN
      END IF
*----------! 
      ZT(1) = ZAST*ZA
      DO 10 I = 2,N1
         ZT(I) = ZT(I-1)*ZA
10    CONTINUE
      DO 11 I = 1,N1
         ZTF(I) = ZTA(I)*TW(I)*ZT(I)
11    CONTINUE
      ZT1 = ZT(N1)/ZAST
      ZINC= 1
      INC =0
      NT = N/N1  
      DO 20 IT = 2, NT
         ZINC = ZINC*ZT1
         INC  = INC + N1
         DO 30 I = 1, N1
            ZTF(INC +I) = ZTA(INC+I)*TW(INC+I)*ZT(I)*ZINC
30       CONTINUE
20    CONTINUE
      ZINC = ZINC*ZT1
      INC  = INC + N1
      NX = N-NT*N1
      DO 40 I = 1, NX
            ZTF(INC +I) = ZTA(INC+I)*TW(INC+I)*ZT(I)*ZINC
40    CONTINUE
      END


            



      SUBROUTINE INIWIN(KTABS)
*******************************************************************
*     INITIALISE LE TABLEAU TWIN POUR UTILISATION D'UNE FENETRE
*     DANS L'ANALYSE DE FOURIER.
*     FENETRE DE HANNING:
*      PHI(T) = (1+COS(PI*T))
*     modif le 13/12/90 (J. Laskar)
*     modif le 27/1/96 for various windows  (J. Laskar)
*
*     working area TWIN(*) should be given in 
*
*     IW is the window flag
*     IW = 0 : no window
*     IW = n > 0   PHI(T) = CN*(1+COS(PI*T))**n
*                  with CN = 2^n*(n!)^2/(2n)!
*     IW = -1  exponential window PHI(T) = 1/CE*exp(-1/(1-t^2))
*******************************************************************
*
      IMPLICIT DOUBLE PRECISION (A-H,O-Y)
      COMMON/CDATA/XH,T0,UNIANG,FREFON,ICPLX,IW
      COMMON/CPRINT/NFPRT,IPRT
      COMMON/CWIN/TWIN(1)
      CE= 0.22199690808403971891D0
*
      PI=ATAN2(1.D0,0.D0)*2.D0
      T1=T0
      T2=T0+KTABS*XH
      TM=(T2-T1)/2
      PIST=PI/TM
      IF (IW.EQ.0) THEN
         DO   IT=1,KTABS+1
            TWIN(IT)=1.D0 
         ENDDO
      ELSE IF(IW.GE.0) THEN
         CN = 1.D0
         DO I = 1,IW
            CN = CN*2.D0*I/(IW+I)
         ENDDO       
         DO IT=1,KTABS+1
            T=(IT-1)*XH-TM
            TWIN(IT)=CN*(1.D0+COS(T*PIST))**IW
         ENDDO
      ELSE IF(IW.EQ.-1) THEN
         TWIN(1) =0.D0
         TWIN(KTABS+1) =0.D0
         DO IT=2,KTABS 
            T=((IT-1)*XH-TM)/TM
            TWIN(IT)= EXP(-1.D0/(1.D0-T**2))/CE
         ENDDO
      ENDIF
      IF (IPRT.EQ.1) THEN
*----------------   IMPRESSION TEMOIN
         DO 20 IT=1,KTABS+1,KTABS/20
            WRITE (*,1000) TWIN(IT)*1.D6
20       CONTINUE
      ENDIF
1000  FORMAT (1X,F20.3)
      END
*
      SUBROUTINE ZARDYD(ZT,N,H,ZOM)
************************************************************************
*     CALCULE L'INTEGRALE D'UNE FONCTION TATULEE PAR LA METHODE DE HARDY
*      T(N) TABLEAU DOUBLE PRECISION DES VALEURS DE LA FONCTION
*      N = 6*K+1  ENTIER
*      H PAS ENTRE DEUX VALEURS
*      SOM VALEUR DE L'INTEGRALE SUR L'INTERVALLE [X1,XN]
*      LE PROGRAMME EST EN DOUBLE PRECISION
*               REVU LE 26/9/87 J. LASKAR
************************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Y)
      IMPLICIT COMPLEX*16 (Z)
      DIMENSION ZT(N)
      ITEST=MOD(N,6)
      IF (ITEST.NE.1) THEN
         WRITE(*,*) 'N-1 N''EST PAS UN MULTIPLE DE 6'
         STOP
      ENDIF
      K=(N-1)/6
      ZOM=41*ZT(1)+216*ZT(2)+27*ZT(3)+272*ZT(4)
     $                      +27*ZT(5)+216*ZT(6)+41*ZT(N)
      DO 10 I=1,K-1
         ZOM=ZOM+82*ZT(6*I+1)+216*ZT(6*I+2)+27*ZT(6*I+3)+272*ZT(6*I+4)
     $           +27*ZT(6*I+5)+216*ZT(6*I+6)
10    CONTINUE
      ZOM=ZOM*H*6.D0/840.D0
      END


      SUBROUTINE PROSCAA(KTABS,F1,F2,ZP)
*-----------------------------------------------------------------------
*     PROSCAA   CALCULE LE PRODUIT SCALAIRE 
*           < EXP(I*F1*T),  EXP(I*F2*T)>
*               SUR L'INTERVALLE [0:KTABS]  T=T0+XH*IT
*              CALCUL numerique
*     ZP        PRODUIT SCALAIRE COMPLEXE
*     F1,F2     FREQUENCES EN "/AN
*               REVU LE 26/9/87 J. LASKAR
*-----------------------------------------------------------------------
      IMPLICIT DOUBLE PRECISION (A-H,O-Y)
      IMPLICIT COMPLEX*16 (Z)
      COMMON/CPOW/ZT(1)
      COMMON/CWORK1/ZTF(1)
      COMMON/CWORK2/TF2(1)
      COMMON/CWIN/TWIN(1)
      COMMON/CDATA/XH,T0,UNIANG,FREFON,ICPLX
      ZI=DCMPLX(0.D0,1.D0)
*----------! FREQUENCES EN UNITE D'ANGLE PAR UNITE DE TEMPS
      OM = (F1-F2)/UNIANG
      LTF=KTABS
      ANG0=OM*T0
      ANGI=OM*XH
      ZAC = EXP (-ZI*ANG0)
      ZINC= EXP (-ZI*ANGI)
      ZEX = ZAC/ZINC
      CALL  ZTPOW2A(KTABS+1,64,ZT,ZTF,TWIN,ZINC,ZEX)
*------------------ TAILLE DU PAS
      H=1.D0/LTF
      CALL ZARDYD(ZTF,LTF+1,H,ZP)
      END


      SUBROUTINE ZTPOW2A (N,N1,ZT,ZTF,TW,ZA,ZAST)
*-----------------------------------------------------------------------
*     ZTPOW   calcule  ZTF(i) = TW(i)*ZAST*ZA**i en vectoriel
*             ZT(N1) zone de travail
*-----------------------------------------------------------------------
      IMPLICIT DOUBLE PRECISION (A-H,O-Y)
      IMPLICIT COMPLEX*16 (Z)
      DIMENSION ZT(N1)
      DIMENSION ZTF(N),TW(N)
      IF (N.LT.1) THEN
         WRITE (*,*) 'DANS ZTPOW, N = ', N
         RETURN
      END IF
*----------! 
      ZT(1) = ZAST*ZA
      DO 10 I = 2,N1
         ZT(I) = ZT(I-1)*ZA
10    CONTINUE
      DO 11 I = 1,N1
         ZTF(I) = TW(I)*ZT(I)
11    CONTINUE
      ZT1 = ZT(N1)/ZAST
      ZINC= 1
      INC =0
      NT = N/N1  
      DO 20 IT = 2, NT
         ZINC = ZINC*ZT1
         INC  = INC + N1
         DO 30 I = 1, N1
            ZTF(INC +I) = TW(INC+I)*ZT(I)*ZINC
30       CONTINUE
20    CONTINUE
      ZINC = ZINC*ZT1
      INC  = INC + N1
      NX = N-NT*N1
      DO 40 I = 1, NX
            ZTF(INC +I) = TW(INC+I)*ZT(I)*ZINC
40    CONTINUE
      END


