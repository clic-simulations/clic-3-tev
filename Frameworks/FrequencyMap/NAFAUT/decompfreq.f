********************************************************************
*                                                                  *
*    Decomposition des frequences d'un fichier                     *
*    du type 'nafaut' dans une base donnee                         *
*                                                                  *
******  Lecture du fichier 'type' nafaut de forme :                *
******  nbr termes                                                 *
******  nn  nuk  aux  a  b                                         *
****** avec  nn   : numero de la ligne                             *
******       nuk  : frequence                                      *
******       aux  : amplitude du terme                             * 
******       a  b : coefficients  z = (a+i*b)exp(i*nuk*t)          *
******                                                             *
******  Pour nDL degre de liberte, chaque frequence est decomposee *
******  sous la forme : nuk = p_1*F(1) + ... + p_nDL*F(nDL)        *
******  Pour chaque terme, on calcule aussi la phase phasek(i)     *
******  et l'amplitude alphak(i)  tels que :                       *
******  z_i = alphak(i)*exp(nuk(i)*t + phasek(i))                  * 
******                                                             *   
******  phasek(i) : phase en degre  (si angl = 0)                  *
*                            radian (si angl not= 0)               *
***  sortie = 0  :  format type tableau Tex                        *
***  sortie = 1  :  Classique                                      *
******                                                             *
*                                                                  *
*                    ( 13 Octobre 1995 )                           *
*                                                                  *
********************************************************************
   

      subroutine inipar(FF,DLmax)
      implicit none
      character*40 nomsol,nomout
      integer ORMAX,nDL,sortie,DLmax,angl,i
      double precision FF(DLmax),F(100)
      common/cstd/ORMAX,nDL,sortie,angl,nomsol,nomout

******* utilisation de F(100) car namelist n'admat pas de 
******* tableaux de taille variable !!
      namelist/namstd/ORMAX,nDL,sortie,angl,nomsol,nomout,F

      open(10,file='decompfreq.par',form= 'formatted',status='old')
      read(10,namstd)
      do i=1,DLmax
        FF(i) = F(i)
      end do
      close(10)
      end


      subroutine prtpar(NFPRT,F,DLmax)
      implicit none
      character*40 nomsol,nomout
      integer ORMAX,nDL,sortie,DLmax,angl,NFPRT,i
      double precision F(DLmax)
      common/cstd/ORMAX,nDL,sortie,angl,nomsol,nomout
      
      write (nfprt,*) 'nomsol   = ',nomsol
      write (nfprt,*) 'nomout   = ',nomout
      write (nfprt,*) 'nDL      = ',nDL
      write (nfprt,*) 'sortie   = ',sortie
      write (nfprt,*) 'angl     = ',angl
      write (nfprt,*) 'ORMAX    = ',ORMAX
      do i=1,nDL
        write (nfprt,100) 'F(',i,')','=',F(i)
      end do
100   format(1X,A2,I2,A1,4X,A1,e16.10)
      end



      subroutine charge(nterm,Mterm,nuk,alphak,phasek,a,b)
******
******  Lecture du fichier 'type' nafaut
******  et pour chaque terme (chaque ligne)
******  calcul de 
******  nuk(i)    : frequence
******  alphak(i) : amplitude
******  phasek(i) : phase en degre  (angl = 0)
*                            radian (angl not= 0)
******
      implicit none  
      character*40 nomsol,nomout    
      integer i,nterm,Mterm,nn,ORMAX,DLmax,nDL,sortie,angl
      double precision Pi,radsec,nuk(Mterm),aux,a(Mterm),b(Mterm),
     $alphak(Mterm),phasek(Mterm),vide

      common/cstd/ORMAX,nDL,sortie,angl,nomsol,nomout

      Pi = atan2(1.d0,1.d0)*4.d0

      open(11,file=nomsol,status='unknown')      
      read(11,*)vide
      read(11,*)nterm
      if (nterm.gt.Mterm) stop'nterm.gt.Mterm'
      write(*,*)'nterm    =',nterm
      do i=1,nterm  
        read(11,*)nn,nuk(i),aux,a(i),b(i)
        alphak(i)=dsqrt(a(i)**2+b(i)**2)
        phasek(i)=datan2(b(i),a(i))
      end do
      close(11)      
      if (angl.eq.0) then
        do i=1,nterm        
          phasek(i)=phasek(i)*180d0/Pi
          if (phasek(i).le.0d0) then
            phasek(i)=phasek(i)+360d0
          end if
        end do
      end if
      end


      subroutine freqfon(nterm,Mterm,nuk,delta,p,F,DLmax)
      implicit none
      character*40 nomsol,nomout 
      integer i,j,nn,p(Mterm,10),nterm,rien,ORMAX,nDL,
     $sortie,angl,Mterm,DLmax,n1,n2,n3,n4,n5,n6,n7,n8,
     $n9,n10,max(10),min(10)
      double precision F(DLmax),nuk(Mterm),delta(Mterm)
     $,del,ddel

      common/cstd/ORMAX,nDL,sortie,angl,nomsol,nomout

      write(*,*)'**** info *******'
      write(*,*)'nDL',nDL

      do nn=1,nterm
        delta(nn) = 1d8
        do j=1,nDL
          max(j) = ORMAX
          min(j) = -ORMAX
        end do
        do j=nDL+1,DLmax
          max(j) = 0
          min(j) = 0
        end do
        do n1=min(1),max(1)
        do n2=min(2),max(2)
        do n3=min(3),max(3)
        do n4=min(4),max(4)
        do n5=min(5),max(5)
        do n6=min(6),max(6)
        do n7=min(7),max(7)
        do n8=min(8),max(8)        
        do n9=min(9),max(9)
        do n10=min(10),max(10)          
          del = n1*F(1)+n2*F(2)+n3*F(3)+n4*F(4)+n5*F(5)
     $         +n6*F(6)+n7*F(7)+n8*F(8)+n9*F(9)+n10*F(10)
          ddel =  abs(del - nuk(nn))
          if (ddel.le.delta(nn)) then
            delta(nn) = ddel
            p(nn,1) = n1
            p(nn,2) = n2
            p(nn,3) = n3
            p(nn,4) = n4         
            p(nn,5) = n5
            p(nn,6) = n6
            p(nn,7) = n7
            p(nn,8) = n8
            p(nn,9) = n9
            p(nn,10) = n10          
          end if
        end do
        end do
        end do         
        end do
        end do
        end do
        end do
        end do
        end do
        end do
      end do 
      end



      subroutine ecrite(nterm,Mterm,nuk,alphak,phasek,delta,p,a,b)
***  sortie = 0  :  format type tableau Tex
***  sortie = 1  :  Classique
     
      implicit none
      character*40 nomsol,nomout
      integer n,p(Mterm,10),nterm,Mterm,ORMAX,nDL,sortie,
     $angl,i,frm,rm
      double precision nuk(Mterm),delta(Mterm),alphak(Mterm),
     $phasek(Mterm),a(Mterm),b(Mterm)

      common/cstd/ORMAX,nDL,sortie,angl,nomsol,nomout

      open (12,file=nomout,status='unknown')


      if (sortie.eq.0) then
        do n=1,nterm        
          write(12,200)nuk(n),' &',alphak(n),' & ',phasek(n),' &'
          do i=1,nDL
            write(12,201) p(n,i),' &'
          end do
          write(12,202) delta(n),' crla'
          
          write(*,300) nuk(n),alphak(n),phasek(n)
          do i=1,nDL
            write(*,301) p(n,i)
          end do
          write(*,302) delta(n)
        end do
      end if

      if (sortie.eq.1) then
        do n=1,nterm   
          write(12,300) nuk(n),alphak(n),phasek(n)
          do i=1,nDL
            write(12,301) p(n,i)
          end do
          write(12,302) delta(n)

          write(*,300) nuk(n),alphak(n),phasek(n)
          do i=1,nDL
            write(*,301) p(n,i)
          end do
          write(*,302) delta(n)
        end do
      end if
      close (12)

300     format(F18.13,1X,F11.9,2X,F8.4,$)
301     format(1X,I3,$)
302     format(1X,F18.13)
200     format(F18.13,A2,F16.11,A3,F8.4,A2,$)
201     format(I3,A2,$)
202     format(F16.11,A5)
      end 


      program main
      implicit none
      integer DLmax,Nmax
      parameter (DLmax = 10)
      parameter (Nmax = 100)
      character*40 nomsol,nomout
      integer ORMAX,nDL,sortie,nterm,p(Nmax,DLmax)
      double precision F(DLmax),nuk(Nmax),alphak(Nmax),phasek(Nmax)
     $,a(Nmax),b(Nmax),delta(Nmax)

*      common/cstd/ORMAX,nDL,sortie,angl,nomsol,nomout

      call inipar(F,DLmax)
      call prtpar(6,F,DLmax)
      call charge(nterm,Nmax,nuk,alphak,phasek,a,b)
      call freqfon(nterm,Nmax,nuk,delta,p,F,DLmax)
      call ecrite(nterm,Nmax,nuk,alphak,phasek,delta,p,a,b)
      end
          
