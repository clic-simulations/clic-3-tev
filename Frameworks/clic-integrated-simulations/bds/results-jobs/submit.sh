iter=40
nm=40
w1=1
w2=1
w3=0.01
gain=0.25

bsub -q 2nd << EOF
 placet-octave $PWD/../run_km_dfs.tcl w1 $w1 w2 $w2 w3 $w3 iter $iter gain $gain nm $nm
 cp -r emitt* bpm* failed* $PWD
EOF

