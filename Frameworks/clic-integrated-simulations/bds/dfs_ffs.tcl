set e_initial 1500
set e0 $e_initial
set script_dir [pwd]

array set params {
    seed 1234
    ksigma 0
    rollsigma 0
    bpmres 0.025
    nm 20
    wgt1 10.0
    iter 4
    sigma 10.0
    en 0.996
    skick1 500.0
    skick2 500.0 
}

array set params $argv

set seed $params(seed)
set ksigma $params(ksigma)
set rollsigma $params(rollsigma)
set bpmres $params(bpmres)
set nm $params(nm)
set wgt1 $params(wgt1)
set iter $params(iter)
set sigma $params(sigma)
set en $params(en)
set scalekick1 $params(skick1)
set scalekick2 $params(skick2)

set extension ""
foreach {name value} [array get params] { append extension "_${name}_${value}" }


set scale 1.0
source $script_dir/scripts/clic_basic_single.tcl

set synrad 1
set quad_synrad 1
set mult_synrad 0
set sbend_synrad 1
set dp 0.00

proc save_beam {} {
    BeamDump -file particles.out
}

puts "sigma $sigma"

Girder
#source /afs/cern.ch/user/g/gzamudio/scratch0/MADX/CLIC_3Tev/Lstar6m/Tuning/eff1s3T.placetBD
source $script_dir/lattices/ffs_l3.5m_noape

TclCall -script save_beam

BeamlineSet -name test

array set match {
alpha_x  -7.702459167e-08
alpha_y 7.426526367e-07
beta_x  64.999884046
beta_y  17.99971384
}

#emittances unit is e-7


#
# Create the beam
#
source $script_dir/scripts/clic_beam.tcl

set match(emitt_x)  6.6 
set match(emitt_y)  0.2
set match(charge) 3.72e9
set charge $match(charge)
set match(sigma_z) 44.0
set match(phase) 0.0
set match(e_spread) -1.0
# set n_slice 20
# set n 2500
# set n_total [expr $n_slice*$n]

# make_beam_particles [expr $e0*(1+($dp))] $match(e_spread) $n_total

# make_beam_many beam0 $n_slice $n 
# BeamRead -file particles.in -beam beam0
set n_slice 25 
set n 6000
set n_total [expr $n_slice * $n]

make_beam_many_energy beam0 $n_slice $n 1.00
make_beam_many_energy beam1 $n_slice $n $en

FirstOrder 1

proc run_guinea {offx offy} {
    global gp_param
    set res [exec /afs/cern.ch/eng/sl/lintrack/PLACET/x86_64/bin/grid]
    set yoff [expr $offy-0.5*([lindex $res 2]+[lindex $res 3])]
    set xoff [expr $offx-0.5*([lindex $res 0]+[lindex $res 1])]
    set tx $gp_param(cut_x)
    set ty $gp_param(cut_y)
    if {[lindex $res 1]-[lindex $res 0]>2.0*$tx} {
 	set gp_param(cut_x) [expr 0.5*([lindex $res 1]-[lindex $res 0])]
    }
    if {[lindex $res 3]-[lindex $res 2]>2.0*$ty} {
 	set gp_param(cut_y) [expr 0.5*([lindex $res 3]-[lindex $res 2])]
    }
    #puts "cut_x $gp_param(cut_x)"
    #puts "cut_y $gp_param(cut_y)"
	    #puts "yoff $yoff"
    #puts "yoff $xoff"
    #puts $res
    #
    # Modify this
    #
    #write_guinea_correct_angle_xy $xoff 0.0 $yoff 0.0
    write_guinea_correct $xoff $yoff
    
    exec guinea default default default
    set gp_param(cut_x) $tx
    set gp_param(cut_y) $ty
    return [get_results default]
}

array set gp_param "
    energy 1500.0
    particles [expr $match(charge)*1e-10]
    sigmaz $match(sigma_z)
    cut_x 400.0
    cut_y 15.0
    n_x 128
    n_y 256
    do_coherent 1
    n_t 1
    charge_sign -1.0"


source $script_dir/scripts/clic_guinea.tcl

proc my_survey {} {
  global sigma
  Octave {
      Q = placet_get_number_list("electron", "quadrupole");
      #       S = placet_element_get_attribute("electron", Q, "s");
      #       QN = placet_element_get_name("electron", Q);
      #       for j=1,size(Q)
      #       printf("%g  %s",S(j),QN(j));
      #       endfor
      M=placet_get_number_list("electron", "multipole");
      DQ = randn(size(Q)) * $sigma;
      DM = randn(size(M)) * $sigma;
      placet_element_set_attribute("electron", Q, "y", DQ);
      placet_element_set_attribute("electron", M, "y", DM);
      placet_element_set_attribute("electron", response.C, response.leverage, 0.0);
      placet_element_set_attribute("electron", response.B, "y", randn(size(response.B)) * $sigma);
      #      placet_element_set_attribute("electron", response.B(end), "y", 0.0);
  }
}

foreach use_multipole { 0 1 } {
  Octave {
    E0_s$use_multipole = 0;
    E1_s$use_multipole = 0;
    E2_s$use_multipole = 0;
    E3_s$use_multipole = 0;
    BPM0_s$use_multipole = 0;
    BPM1_s$use_multipole = 0;
    BPM2_s$use_multipole = 0;
    BPM3_s$use_multipole = 0;
  }
}

for {set i 0} {$i<$nm} {incr i} {
  Octave {
    printf("MACHINE: $i/$nm\n");
  }
  foreach use_multipole { 0 1 } {
    Octave {
      printf("PHASE: %d/2\n", ${use_multipole}+1);
      # switch off the multipoles
        I=placet_get_name_number_list("electron", "*");
 	if $use_multipole == 0
        MI=placet_get_number_list("electron", "multipole");
        MS=placet_element_get_attribute("electron", MI, "strength");
        placet_element_set_attribute("electron", MI, "strength", 0.0);
	#SR=placet_element_get_attribute("electron", I, "synrad");
	#placet_element_set_attribute("electron", I, "synrad", 0);
	endif
	
	# load or create the response matrices
	if exist("R0_${use_multipole}_${en}.dat", "file")
        load R0_${use_multipole}_${en}.dat;
	else
        response.C = placet_get_number_list("electron", "dipole");
        response.B = placet_get_number_list("electron", "bpm");
        if $use_multipole == 0
	#	response.scale = $e_initial / 2.0;
	response.scale = $e_initial /$scalekick1;
        else
	#	response.scale = $e_initial / 16.0;
	response.scale = $e_initial /$scalekick2;
        endif
        response.gain = 1.0;
        response.leverage = "strength_y";
        response.observable = "reading_y";
	P.x=placet_element_get_attribute("electron", I, "x");
        P.y=placet_element_get_attribute("electron", I, "y");
        P.xp=placet_element_get_attribute("electron", I, "xp");
        P.yp=placet_element_get_attribute("electron", I, "yp");
        S=placet_element_get_attribute("electron", response.C, response.leverage);
#	save -text $script_dir/dipole_strength.dat S;
	placet_element_set_attribute("electron", response.C, response.leverage, 0.0);
        [response.R0, response.T0] = placet_get_response_matrix_opt("electron", "beam0", "Zero", response);
        [response.R1, response.T1] = placet_get_response_matrix_opt("electron", "beam1", "Zero", response);
        placet_element_set_attribute("electron", response.C, response.leverage, S);
        placet_element_set_attribute("electron", I, "x", P.x);
        placet_element_set_attribute("electron", I, "y", P.y);
        placet_element_set_attribute("electron", I, "xp", P.xp);
        placet_element_set_attribute("electron", I, "yp", P.yp);
        save -text R0_${use_multipole}_${en}.dat response
      endif

      if $use_multipole == 0
        Tcl_Eval("my_survey");
        placet_element_set_attribute("electron", response.B, "resolution", $bpmres);
	#        placet_element_set_attribute("electron", I, "synrad", SR);
      endif

       
      # init dfs
      # R is the big response matrix for DFS
      #
      w = [ 1.0, $wgt1 ];
      R = [ w(1) * response.R0; w(2) * (response.R1-response.R0) ]; #  ; means append vertically
      SV = svd(R)(1); # First singular value

      # start correction

      response.R=response.R0; # This new (copy) matrix is for the command  placet_test_simple_correction_opt
      response.T=response.T0;
      response.pinv_tol=svd(response.R)(1)*1e-4;

      E0_s${use_multipole} += placet_test_no_correction("electron", "beam0", "None");
      BPM0_s${use_multipole} += [placet_element_get_attribute("electron", response.B', "reading_y"), placet_element_get_attribute("electron", response.B', "y") ];

      # SIMPLE CORRECTION

      E1_s${use_multipole} += placet_test_simple_correction_opt("electron", "beam0", "None", response); # This returns beam size, emittance (6th column) and more
      BPM1_s${use_multipole} += [placet_element_get_attribute("electron", response.B', "reading_y"), placet_element_get_attribute("electron", response.B', "y") ];

      # DISPERSION FREE STEERING

      gain = 1.0;
      for j=1:$iter

        printf("DFS: iteration %d/$iter\n", j);

        placet_test_no_correction("electron", "beam0", "None"); # None leaves the previous misalignment untouched
        B0 = placet_element_get_attribute("electron", response.B, "reading_y") - response.T0;
        placet_test_no_correction("electron", "beam1", "None");
        B1 = placet_element_get_attribute("electron", response.B, "reading_y") - response.T1;

        B = [ w(1) * B0, w(2) * (B1-B0) ];
        X = -pinv(R, SV*1e-4) * B';
        placet_element_vary_attribute("electron", response.C, response.leverage, gain * X);

      endfor

      E2_s${use_multipole} += placet_test_no_correction("electron", "beam0", "None"); 
      BPM2_s${use_multipole} += [placet_element_get_attribute("electron", response.B', "reading_y"), placet_element_get_attribute("electron", response.B', "y") ];
	Tcl_Eval("SaveAllPositions -vertical_only 0 -cav_grad_phas 0 -binary 0 -file electron.pos -cav_bpm 1 -nodrift 1");

      if $use_multipole == 0
        placet_element_set_attribute("electron", MI, "strength", MS);
      endif
    }
  }
}

foreach use_multipole { 0 1 } {
  Octave {
    E0_s$use_multipole /= $nm;
    E1_s$use_multipole /= $nm;
    E2_s$use_multipole /= $nm;
    BPM0_s$use_multipole /= $nm;
    BPM1_s$use_multipole /= $nm;
    BPM2_s$use_multipole /= $nm;
    save -text emitt.no${extension}_s${use_multipole} E0_s$use_multipole
    save -text emitt.simple${extension}_s${use_multipole} E1_s$use_multipole
    save -text emitt.dfs${extension}_s${use_multipole} E2_s$use_multipole
    save -text bpm.no${extension}_s${use_multipole} BPM0_s$use_multipole
    save -text bpm.simple${extension}_s${use_multipole} BPM1_s$use_multipole
    save -text bpm.dfs${extension}_s${use_multipole} BPM2_s$use_multipole
  }
}

