set e_initial 1500
set e0 $e_initial
set script_dir [pwd]

array set args {
    seed 1234
    sigma 0.0
    ksigma 0
    rollsigma 0
    dp 1.0
    wtrack 1
}
   
array set args $argv

set seed $args(seed)
set sigma $args(sigma)
set ksigma $args(ksigma)
set rollsigma $args(rollsigma)
set dp $args(dp)
set which_track $args(wtrack)

Random -type gaussian gcav

set scale 1.0
source $script_dir/scripts/clic_basic_single.tcl
source $script_dir/scripts/clic_beam.tcl

set synrad 1
set quad_synrad 1
set mult_synrad 0
set sbend_synrad 1

puts "tracking option: $which_track"


# lattice
proc save_beam {} {
   BeamDump -file particles.out
}
source $script_dir/lattices/bds.match.linac4b

TclCall -script save_beam

BeamlineSet -name test

if { $which_track == 1 || $which_track == 3 } {
    # Twiss parameters at the BDS entrance 
    array set match {
	alpha_x 0.59971622
	alpha_y -1.93937335
	beta_x  18.382571
	beta_y  64.450775
    }
} elseif { $which_track == 2 } {
    # Twiss parameters at the IP (ideal beam) 
    # no tracking in the BDS system
    array set match {
	alpha_x 0.0
	alpha_y 0.0
	beta_x  0.0069
	beta_y  0.000068
    }
}
#emittances unit is e-7
set match(emitt_x)  6.6 
set match(emitt_y)  0.2
set match(charge) 3.72e9
set charge $match(charge)
set match(sigma_z) 44.0
set match(phase) 0.0
set match(e_spread) -1.0

set n_slice 25
set n 6000
set n_total [expr $n_slice*$n]


make_beam_particles [expr $e0*($dp)] $match(e_spread) $n_total
make_beam_many beam0 $n_slice $n 
BeamRead -file particles.in -beam beam0

if { $which_track == 1 } {
    # perfect machine tracking
    FirstOrder 1
    BeamlineUse -name test
    TestNoCorrection -beam beam0 -emitt_file emitt.dat -survey Zero
    exec cp particles.out electron.ini
    exec mv particles.out positron.ini
} elseif { $which_track == 2 } {
    exec cp particles.in electron.ini
    exec mv particles.in positron.ini
} elseif { $which_track == 3  } {
    #
    #  Octave example for perfect tracking + bpm readings
    #
    Octave {
	QI = placet_get_number_list("test", "bpm");
	#  Q1 = QI(3:2:end);
	#  Q2 = QI(4:2:end);
	Qs=placet_element_get_attribute("test", QI, "s");
	#Ql=placet_element_get_attribute('test', QI, "length");
	#Qs=placet_element_get_attribute('test', QI, "name");
	#placet_element_get_attribute("test", Q1, "strength")
	#placet_element_get_attribute("test", Q2, "strength")
	#  placet_link_elements("test", Q1, Q2);
	#  QI = Q1(end-20:end) 
	#placet_link_elements("test", Q1, Q2);
	#MU = placet_get_number_list("test", "multipole");
	#placet_element_set_attribute("test", MU, "strength", 0.0);
	placet_test_no_correction("test", "beam0", "Zero");
	system("cp particles.out electron.ini");
	system("mv particles.out positron.ini");
	name.bsignal1=placet_get_bpm_readings("test",QI);
	b1=name.bsignal1;
	fname="bpm_readings_$dp";
	f=fopen(fname,'wt');
	for i=1:length(QI)
	fprintf(f,"%g   %.8g   %.8g  %g\n",Qs(i), b1(i,1), b1(i,2), $dp);
	endfor
	fclose(f);
    }
} else {
    puts "No option corresponding to $which_track "
}

# Beam-Beam
proc run_guinea {offx offy} {
    global gp_param
    set res [exec grid]
    set yoff [expr $offy-0.5*([lindex $res 2]+[lindex $res 3])]
    set xoff [expr $offx-0.5*([lindex $res 0]+[lindex $res 1])]
    set tx $gp_param(cut_x)
    set ty $gp_param(cut_y)
    if {[lindex $res 1]-[lindex $res 0]>2.0*$tx} {
        set gp_param(cut_x) [expr 0.5*([lindex $res 1]-[lindex $res 0])]
       }
    if {[lindex $res 3]-[lindex $res 2]>2.0*$ty} {
        set gp_param(cut_y) [expr 0.5*([lindex $res 3]-[lindex $res 2])]
       }
    #puts "yoff $yoff"
    #puts "xoff $xoff"
    #puts $res
    write_guinea_correct $xoff $yoff

    exec guinea default default default
    
    set gp_param(cut_x) $tx
    set gp_param(cut_y) $ty
    return [get_results default]
}

array set gp_param "
    energy $e_initial
    particles [expr $match(charge)*1e-10]
    sigmaz $match(sigma_z)
    cut_x 200.0
    cut_y 25.0
    n_x 64
    n_y 320
    do_coherent 1
    n_t 1
    charge_sign -1.0"

source $script_dir/scripts/clic_guinea.tcl

set res [run_guinea 0.0 0.0]
set lumi [lindex $res 0]
set lumi_peak [lindex $res 1]

puts " total lumi: $lumi "
puts " peak lumi: $lumi_peak "
exit
