set script_dir [pwd]

#
# usage:
#
# placet run.tcl <arguments>
#
#

#
# parsing the arguments
#

array set params {
  bpmres 0.1
  nm 1
  iter 1
  gain 0.25
  sigma 10.0
  w1 1
  w2 1
  w3 0.01
}

array set params $argv

set bpmres $params(bpmres)
set nm $params(nm)
set iter $params(iter)
set gain $params(gain)
set sigma $params(sigma)
set w1 $params(w1)
set w2 $params(w2)
set w3 $params(w3)

set extension ""
foreach { name } [lsort [array names params]] { append extension "_${name}_$params($name)" }

#
# here we start
#

set colloct 0
set scale 1.0

#source $script_dir/lumi.tcl
source $script_dir/scripts/clic_basic_single.tcl

#
# Define the two bds as 'electron' and 'positron'
#

proc Marker {a b} {
}

foreach line {electron} {
    BeamlineNew
    Girder
    set quad_synrad 1
    set sbend_synrad 1
    set mult_synrad 0
    set synrad 1
    set e_initial 1500.0
    set e0 $e_initial
    source $script_dir/lattices/bds.match.linac4b_noape
    ReferencePoint -sense -1
#    TclCall -script "save_$line"
    BeamlineSet -name $line
}

#
# define the beam
#
# the sourcing of the clic_beam need to be before
# the the definition of the beam
source $script_dir/scripts/clic_beam.tcl

set e0 1500.0
set e_initial $e0

source $script_dir/lattices/bds.match.linac4b_noape_beam


set n_slice 200
set n 100
set n_total [expr $n_slice * $n]

#
# Create the beam
#

#source $script_dir/clic_guinea.tcl

make_beam_many beam0 $n_slice $n

FirstOrder 1

# 
# misalignments (in microm or microrad)
#

proc my_survey {} {
  global sigma
  global machine w1 w2 w3
  Octave {
    randn("seed", $machine * 1e10 + $w1 * 1e8 + $w2 * 1e4 + $w3);
    Q1 = placet_get_number_list("electron", "quadrupole");
    placet_element_set_attribute("electron", Q1, "y", randn(size(Q1)) * $sigma);
    placet_element_set_attribute("electron", Q1, "x", randn(size(Q1)) * $sigma);
    placet_element_set_attribute("electron", response.C, "strength_x", 0.0);
    placet_element_set_attribute("electron", response.C, "strength_y", 0.0);
    placet_element_set_attribute("electron", response.B, "x", randn(size(response.B)) * $sigma);
    placet_element_set_attribute("electron", response.B, "y", randn(size(response.B)) * $sigma);
    placet_element_set_attribute("electron", response.B(end), "x", 0.0);
    placet_element_set_attribute("electron", response.B(end), "y", 0.0);
  }
}

Octave {
  if $colloct == 0
    I=placet_get_name_number_list("electron", "COLLOCT");
    placet_element_set_attribute("electron", I, "strength", 0.0);
  endif
}

foreach use_multipole { 0 1 } {
  Octave {
    E0_s$use_multipole = 0;
    E1_s$use_multipole = 0;
    E2_s$use_multipole = 0;
    BPM0_s$use_multipole = 0;
    BPM1_s$use_multipole = 0;
    BPM2_s$use_multipole = 0;
  }
}

Octave {
  E3 = 0;
  BPM3 = 0;
  failed = 0;
}

Octave {
  response.B = placet_get_number_list("electron", "bpm");
}

foreach use_multipole { 0 1 } {
  Octave {
    if $use_multipole == 0
      MI=placet_get_number_list("electron", "multipole");
      MS=placet_element_get_attribute("electron", MI, "strength");
      placet_element_set_attribute("electron", MI, "strength", 0.0);
    else
      placet_element_set_attribute("electron", MI, "strength", MS);
    endif
    placet_test_no_correction("electron", "beam0", "Zero");
    BPM${use_multipole}_x = placet_element_get_attribute("electron", response.B', "reading_x");
    BPM${use_multipole}_y = placet_element_get_attribute("electron", response.B', "reading_y");
  }
}

for {set machine 0} {$machine<$nm} {incr machine} {
  Octave {
    printf("MACHINE: %d/$nm\n", $machine+1);
  }
  foreach use_multipole { 0 1 } {
    Octave {
      printf("PHASE: %d/2\n", ${use_multipole}+1);
      # switch off the multipoles
      if $use_multipole == 0
        placet_element_set_attribute("electron", MI, "strength", 0.0);
      else
        placet_element_set_attribute("electron", MI, "strength", MS);
      endif

      # load or create the response matrices
      if exist("$script_dir/models/T0_${use_multipole}.dat", "file")
        load $script_dir/models/T0_${use_multipole}.dat;
      else
        response.C = placet_get_number_list("electron", "dipole");
        response.B = placet_get_number_list("electron", "bpm");
        if $use_multipole == 0
          response.scale = $e_initial / 2.0;
        else
          response.scale = $e_initial / 20.0;
        endif
        response.gain = $gain;
        response.leverage = "strength_y";
        response.observable = "reading_y";
        I=placet_get_name_number_list("electron", "*");
        P.x=placet_element_get_attribute("electron", I, "x");
        P.y=placet_element_get_attribute("electron", I, "y");
        P.xp=placet_element_get_attribute("electron", I, "xp");
        P.yp=placet_element_get_attribute("electron", I, "yp");
        S=placet_element_get_attribute("electron", response.C, response.leverage);
        SR=placet_element_get_attribute("electron", I, "synrad");
        placet_element_set_attribute("electron", I, "synrad", 0);
        placet_element_set_attribute("electron", response.C, response.leverage, 0.0);

        response.observable = "reading_x";
        response.leverage = "strength_x"; 
        [response.Rxx, response.Txx] = placet_get_response_matrix_opt("electron", "beam0", "Zero", response);

        response.observable = "reading_x";
        response.leverage = "strength_y"; 
        [response.Rxy, response.Txy] = placet_get_response_matrix_opt("electron", "beam0", "Zero", response);

        response.observable = "reading_y";
        response.leverage = "strength_x"; 
        [response.Ryx, response.Tyx] = placet_get_response_matrix_opt("electron", "beam0", "Zero", response);

        response.observable = "reading_y";
        response.leverage = "strength_y"; 
        [response.Ryy, response.Tyy] = placet_get_response_matrix_opt("electron", "beam0", "Zero", response);

        placet_element_set_attribute("electron", response.C, response.leverage, S);
        placet_element_set_attribute("electron", I, "x", P.x);
        placet_element_set_attribute("electron", I, "y", P.y);
        placet_element_set_attribute("electron", I, "xp", P.xp);
        placet_element_set_attribute("electron", I, "yp", P.yp);
        placet_element_set_attribute("electron", I, "synrad", SR);
        save -text $script_dir/models/T0_${use_multipole}.dat response
      endif

      response.gain = $gain;

      if $use_multipole == 0
        Tcl_Eval("my_survey");
        placet_element_set_attribute("electron", response.B(1:end-1), "resolution", $bpmres);
        placet_element_set_attribute("electron", response.B(end), "resolution", 0.0);
      endif
      
      # start correction

      E0_s${use_multipole} += placet_test_no_correction("electron", "beam0", "None");
      BPM0_s${use_multipole} += [placet_element_get_attribute("electron", response.B', "reading_y"), placet_element_get_attribute("electron", response.B', "y") ];

      big_R = [ response.Rxx response.Rxy ; response.Ryx response.Ryy ];

      QKL = placet_element_get_attribute("electron", Q1, "strength") ./ placet_element_get_attribute("electron", Q1, "e0");

      big_N = big_R;

      index_b=1;
      index_c=1;
      for bpm=response.B
        b_x = index_b;
        b_y = index_b + length(response.B);
	if strcmp(placet_element_get_attribute("electron", bpm, "name"), "BPMQUAD")
          c_x = index_c;
          c_y = index_c + length(response.C);
	  big_N(b_x,c_x) -= 1 / QKL(index_c);
          big_N(b_y,c_y) += 1 / QKL(index_c);
	  index_c++;
	end
        index_b++;
      end

      # SIMPLE CORRECTION COUPLED

      TOL = svd(big_R)(1) * 1e-4;

      for i=1:$iter

        BPM_x = placet_element_get_attribute("electron", response.B', "reading_x") - BPM${use_multipole}_x;
        BPM_y = placet_element_get_attribute("electron", response.B', "reading_y") - BPM${use_multipole}_y;

        CORR = -pinv(big_R, TOL) * [ BPM_x; BPM_y ];
          
        X = CORR(1:(length(CORR)/2));
        Y = CORR((length(CORR)/2+1):end);

        placet_element_vary_attribute("electron", response.C, "strength_x", response.gain * X);
        placet_element_vary_attribute("electron", response.C, "strength_y", response.gain * Y);

        placet_test_no_correction("electron", "beam0", "None");

      end

      E1_s${use_multipole} += placet_test_no_correction("electron", "beam0", "None");
      BPM1_s${use_multipole} += [placet_element_get_attribute("electron", response.B', "reading_y"), placet_element_get_attribute("electron", response.B', "y") ];
      
      # KICK MINIMIZATION

      w1 = 100;
      w2 = 1;

      TOL = svd( [ w1 * big_R; w2 * big_N ] )(1) * 1e-4;

      for i=1:$iter

        BPM_x = placet_element_get_attribute("electron", response.B', "reading_x") - BPM${use_multipole}_x;
        BPM_y = placet_element_get_attribute("electron", response.B', "reading_y") - BPM${use_multipole}_y;

	C_x = BPM_x;
	C_y = BPM_y;

        index_b=1;
	index_c=1;
	for bpm=response.B
	  if strcmp(placet_element_get_attribute("electron", bpm, "name"), "BPMQUAD")
            energy = placet_element_get_attribute("electron", response.C(index_c), "e0");
            C_x(index_b) -= placet_element_get_attribute("electron", response.C(index_c), "strength_x") / energy / QKL(index_c);
            C_y(index_b) += placet_element_get_attribute("electron", response.C(index_c), "strength_y") / energy / QKL(index_c);
	    index_c++;
	  end
          index_b++;
	end

        CORR = -pinv( [ w1 * big_R; w2 * big_N ] , TOL) * [ w1 * BPM_x; w1 * BPM_y; w2 * C_x; w2 * C_y ];
          
        X = CORR(1:(length(CORR)/2));
        Y = CORR((length(CORR)/2+1):end);

        placet_element_vary_attribute("electron", response.C, "strength_x", response.gain * X);
        placet_element_vary_attribute("electron", response.C, "strength_y", response.gain * Y);

        placet_test_no_correction("electron", "beam0", "None");

      end

      E2_s${use_multipole} += placet_test_no_correction("electron", "beam0", "None");
      BPM2_s${use_multipole} += [placet_element_get_attribute("electron", response.B', "reading_y"), placet_element_get_attribute("electron", response.B', "y") ];
    }
  }
  
  Octave {

    # DISPERSION FREE STEERING
  
    load $script_dir/models/T0_0.dat;
    big_R0 = [ response.Rxx response.Rxy ; response.Ryx response.Ryy ];
    load $script_dir/models/T0_1.dat;
    big_R1 = [ response.Rxx response.Rxy ; response.Ryx response.Ryy ];
    
    big_Diff = big_R1 - big_R0;

    R = [ $w1 * big_R0; $w2 * big_R1; $w3 * big_Diff ];
    TOL = svd(R)(1) * 1e-4;

    for i=1:$iter

      placet_element_set_attribute("electron", MI, "strength", 0.0);
      placet_test_no_correction("electron", "beam0", "None");
      BPM_x = placet_element_get_attribute("electron", response.B', "reading_x") - BPM0_x;
      BPM_y = placet_element_get_attribute("electron", response.B', "reading_y") - BPM0_y;
      b0 = [ BPM_x; BPM_y ];

      placet_element_set_attribute("electron", MI, "strength", MS);
      placet_test_no_correction("electron", "beam0", "None");
      BPM_x = placet_element_get_attribute("electron", response.B', "reading_x") - BPM1_x;
      BPM_y = placet_element_get_attribute("electron", response.B', "reading_y") - BPM1_y;
      b1 = [ BPM_x; BPM_y ];
      
      diff = b1 - b0;

      b = [ $w1 * b0; $w2 * b1; $w3 * diff ];

      CORR = -pinv(R, TOL) * b;

      X = CORR(1:(length(CORR)/2));
      Y = CORR((length(CORR)/2+1):end);

      placet_element_vary_attribute("electron", response.C, "strength_x", response.gain * X);
      placet_element_vary_attribute("electron", response.C, "strength_y", response.gain * Y);
    end

    emitt = placet_test_no_correction("electron", "beam0", "None");
    E3 += emitt;
    BPM3 += [placet_element_get_attribute("electron", response.B', "reading_y"), placet_element_get_attribute("electron", response.B', "y") ];
    
    if isnan(emitt(end,6))
      failed++;
    end
  }
}

foreach use_multipole { 0 1 } {
  Octave {
    E0_s$use_multipole /= $nm;
    E1_s$use_multipole /= $nm;
    E2_s$use_multipole /= $nm;
    BPM0_s$use_multipole /= $nm;
    BPM1_s$use_multipole /= $nm;
    BPM2_s$use_multipole /= $nm;
    save -text emitt.no${extension}_s${use_multipole} E0_s$use_multipole
    save -text emitt.simple_coupled${extension}_s${use_multipole} E1_s$use_multipole
    save -text emitt.kick_coupled${extension}_s${use_multipole} E2_s$use_multipole
    save -text bpm.no${extension}_s${use_multipole} BPM0_s$use_multipole
    save -text bpm.simple_coupled${extension}_s${use_multipole} BPM1_s$use_multipole
    save -text bpm.kick_coupled${extension}_s${use_multipole} BPM2_s$use_multipole
  }
}

Octave {  
  E3 /= $nm;
  BPM3 /= $nm;
  save -text emitt.dfs_coupled${extension} E3;
  save -text bpm.dfs_coupled${extension} BPM3;
  save -text failed${extension} failed;
}
