#set script_dir /afs/cern.ch/user/a/alatina/scratch1/CLIC/bds_dfs_noff/scripts
set script_dir [pwd] 
#
# usage:
#
# placet run.tcl <arguments>
#
#

#
# parsing the arguments
#

array set params {
  bpmres 0.02
  nm 20
  wgt1 10000.0
  iter 1
  sigma 1.0
  en 0.996
  skick1 50.0
  skick2 500.0 
}

array set params $argv

set bpmres $params(bpmres)
set nm $params(nm)
set wgt1 $params(wgt1)
set iter $params(iter)
set sigma $params(sigma)
set en $params(en)
set scalekick1 $params(skick1)
set scalekick2 $params(skick2)

set extension ""
foreach {name value} [array get params] { append extension "_${name}_${value}" }
#
# here we start
#

set scale 1.0
#source $script_dir/lumi.tcl
source $script_dir/scripts/clic_basic_single.tcl

#
# Define the two bds as 'electron' and 'positron'
#

foreach line {electron} {
    BeamlineNew
    Girder
    set quad_synrad 1
    set sbend_synrad 1
    set mult_synrad 1
    set synrad 1
    set e_initial 1500.0
    set e0 $e_initial
    #source $script_dir/bds.last2_noff
    source $script_dir/lattices/bds.match.linac4b
    TclCall -script "save_$line"
    #    source $script_dir/coll.tcl
    ReferencePoint -sense -1
    #    TclCall -script "save_$line"
    BeamlineSet -name $line
}

#
# define the beam
#
source $script_dir/scripts/clic_beam.tcl

set e0 1500.0
set e_initial $e0

set match(emitt_x)   6.6
set match(emitt_y)   0.2
set match(phase) 0.0
set charge 4e9
set match(charge) $charge

set match(beta_x) 18.382571
set match(beta_y) 64.450775
set match(alpha_x) 0.59971622
set match(alpha_y) -1.93937335
set match(sigma_z) 44.0
set match(e_spread) -1.0

set n_slice 40
set n 1000
set n_total [expr $n_slice * $n]

#
# Create the beam
#


make_beam_many_energy beam0 $n_slice $n 1.00
make_beam_many_energy beam1 $n_slice $n $en

FirstOrder 1

proc save_electron {} { 
    global i use_multipole
    BeamDump -file beam.out.$use_multipole.$i
    #      array set params [BeamMeasure]
    #      set beam_emix $params(emitt_x)
    #      set beam_emiy $params(emitt_y)
    #      set beam_betx $params(beta_x)
    #      set beam_bety $params(beta_y)
    #      set beam_alpx $params(alpha_x)
    #      set beam_alpy $params(alpha_y)
    #      set beam_s $params(s)
    #      puts "beam longitudinal position = $beam_s"
    #      puts "beam emix = $beam_emix"
    #      puts "beam emiy = $beam_emiy"
    #      puts "beam betax = $beam_betx"
    #      puts "beam betay = $beam_bety"
    #      puts "beam alpx = $beam_alpx"
    #      puts "beam alpy = $beam_alpy"
    #      set sig_x [expr sqrt($beam_betx*$beam_emix*1e-7/(3.0*1e6))]
    #      set sig_y [expr sqrt($beam_bety*$beam_emiy*1e-7/(3.0*1e6))]
    #      puts "beam sigx = $sig_x"
    #      puts "beam sigy = $sig_y"
}
# 
# misalignments (in microm or microrad)
#

proc my_survey {} {
  global sigma
  Octave {
      Q = placet_get_number_list("electron", "quadrupole");
      M=placet_get_number_list("electron", "multipole");
      DQ = randn(size(Q)) * $sigma;
      DM = randn(size(M)) * $sigma;
      placet_element_set_attribute("electron", Q, "y", DQ);
      placet_element_set_attribute("electron", M, "y", DM);
      placet_element_set_attribute("electron", response.C, response.leverage, 0.0);
      placet_element_set_attribute("electron", response.B, "y", randn(size(response.B)) * $sigma);
      placet_element_set_attribute("electron", response.B(end), "y", 0.0);
  }
}

foreach use_multipole { 0 1 } {
  Octave {
    E0_s$use_multipole = 0;
    E1_s$use_multipole = 0;
    E2_s$use_multipole = 0;
    E3_s$use_multipole = 0;
    BPM0_s$use_multipole = 0;
    BPM1_s$use_multipole = 0;
    BPM2_s$use_multipole = 0;
    BPM3_s$use_multipole = 0;
  }
}

for {set i 0} {$i<$nm} {incr i} {
  Octave {
    printf("MACHINE: $i/$nm\n");
  }
  foreach use_multipole { 0 1 } {
    Octave {
      printf("PHASE: %d/2\n", ${use_multipole}+1);
      # switch off the multipoles
        I=placet_get_name_number_list("electron", "*");
 	if $use_multipole == 0
        MI=placet_get_number_list("electron", "multipole");
        MS=placet_element_get_attribute("electron", MI, "strength");
        placet_element_set_attribute("electron", MI, "strength", 0.0);
	#SR=placet_element_get_attribute("electron", I, "synrad");
	#placet_element_set_attribute("electron", I, "synrad", 0);
	endif
	
	# load or create the response matrices
	if exist("$script_dir/models/R0_${use_multipole}_${en}.dat", "file")
        load $script_dir/models/R0_${use_multipole}_${en}.dat;
	else
        response.C = placet_get_number_list("electron", "dipole");
        response.B = placet_get_number_list("electron", "bpm");
        if $use_multipole == 0
	#	response.scale = $e_initial / 2.0;
	response.scale = $e_initial /$scalekick1;
        else
	#	response.scale = $e_initial / 16.0;
	response.scale = $e_initial /$scalekick2;
        endif
        response.gain = 1.0;
        response.leverage = "strength_y";
        response.observable = "reading_y";
	P.x=placet_element_get_attribute("electron", I, "x");
        P.y=placet_element_get_attribute("electron", I, "y");
        P.xp=placet_element_get_attribute("electron", I, "xp");
        P.yp=placet_element_get_attribute("electron", I, "yp");
        S=placet_element_get_attribute("electron", response.C, response.leverage);
	save -text $script_dir/dipole_strength.dat S;
	placet_element_set_attribute("electron", response.C, response.leverage, 0.0);
        [response.R0, response.T0] = placet_get_response_matrix_opt("electron", "beam0", "Zero", response);
        [response.R1, response.T1] = placet_get_response_matrix_opt("electron", "beam1", "Zero", response);
        placet_element_set_attribute("electron", response.C, response.leverage, S);
        placet_element_set_attribute("electron", I, "x", P.x);
        placet_element_set_attribute("electron", I, "y", P.y);
        placet_element_set_attribute("electron", I, "xp", P.xp);
        placet_element_set_attribute("electron", I, "yp", P.yp);
        save -text $script_dir/models/R0_${use_multipole}_${en}.dat response
      endif

      if $use_multipole == 0
        Tcl_Eval("my_survey");
        placet_element_set_attribute("electron", response.B, "resolution", $bpmres);
	#        placet_element_set_attribute("electron", I, "synrad", SR);
      endif
      
      # init dfs
      # R is the big response matrix for DFS
      #
      w = [ 1.0, $wgt1 ];
      R = [ w(1) * response.R0; w(2) * (response.R1-response.R0) ]; #  ; means append vertically
      SV = svd(R)(1); # First singular value

      # start correction

      response.R=response.R0; # This new (copy) matrix is for the command  placet_test_simple_correction_opt
      response.T=response.T0;
      response.pinv_tol=svd(response.R)(1)*1e-4;

      E0_s${use_multipole} += placet_test_no_correction("electron", "beam0", "None");
      BPM0_s${use_multipole} += [placet_element_get_attribute("electron", response.B', "reading_y"), placet_element_get_attribute("electron", response.B', "y") ];

      # SIMPLE CORRECTION

      E1_s${use_multipole} += placet_test_simple_correction_opt("electron", "beam0", "None", response); # This returns beam size, emittance (6th column) and more
      BPM1_s${use_multipole} += [placet_element_get_attribute("electron", response.B', "reading_y"), placet_element_get_attribute("electron", response.B', "y") ];

      # DISPERSION FREE STEERING

      gain = 1.0;
      for j=1:$iter

        printf("DFS: iteration %d/$iter\n", j);

        placet_test_no_correction("electron", "beam0", "None"); # None leaves the previous misalignment untouched
        B0 = placet_element_get_attribute("electron", response.B, "reading_y") - response.T0;
        placet_test_no_correction("electron", "beam1", "None");
        B1 = placet_element_get_attribute("electron", response.B, "reading_y") - response.T1;

        B = [ w(1) * B0, w(2) * (B1-B0) ];
        X = -pinv(R, SV*1e-4) * B';
        placet_element_vary_attribute("electron", response.C, response.leverage, gain * X);

      endfor

      E2_s${use_multipole} += placet_test_no_correction("electron", "beam0", "None"); 
      BPM2_s${use_multipole} += [placet_element_get_attribute("electron", response.B', "reading_y"), placet_element_get_attribute("electron", response.B', "y") ];
      
      if $use_multipole == 0
        placet_element_set_attribute("electron", MI, "strength", MS);
      endif
    }
  }
}

foreach use_multipole { 0 1 } {
  Octave {
    E0_s$use_multipole /= $nm;
    E1_s$use_multipole /= $nm;
    E2_s$use_multipole /= $nm;
    BPM0_s$use_multipole /= $nm;
    BPM1_s$use_multipole /= $nm;
    BPM2_s$use_multipole /= $nm;
    save -text emitt.no${extension}_s${use_multipole} E0_s$use_multipole
    save -text emitt.simple${extension}_s${use_multipole} E1_s$use_multipole
    save -text emitt.dfs${extension}_s${use_multipole} E2_s$use_multipole
    save -text bpm.no${extension}_s${use_multipole} BPM0_s$use_multipole
    save -text bpm.simple${extension}_s${use_multipole} BPM1_s$use_multipole
    save -text bpm.dfs${extension}_s${use_multipole} BPM2_s$use_multipole
  }
}
