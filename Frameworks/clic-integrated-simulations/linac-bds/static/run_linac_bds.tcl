set script_dir [pwd]  
# global variables
array set params {
    ch 3.72e9
    emitt_x 6.6
    emitt_y 0.2
    offset 0.0
    waist_y 0.0
}

array set params $argv
exec mkdir -p bkg_3TeV_emix_$params(emitt_x)_emiy_$params(emitt_y)_ch_$params(ch)_off_$params(offset) 
cd bkg_3TeV_emix_$params(emitt_x)_emiy_$params(emitt_y)_ch_$params(ch)_off_$params(offset) 
#
# General
#
Random -type gaussian gcav

# number of machine to simulate
set nm 20

set n_slice 25
set n_part 15

set frac_lambda 0.25

set n_total 150000
set n [ expr $n_total/$n_slice ]
set scale 1.0


source $script_dir/scripts/clic_basic_single.tcl
source $script_dir/scripts/clic_beam.tcl


set match(n_total) $n_total

set match(emitt_y) $params(emitt_y)

set match(sigma_z) 44.0

set match(charge) $params(ch)

set charge $match(charge)

set match(emitt_x) $params(emitt_x)

#set match(e_spread) -1.0

puts "charge $match(charge)"
puts "emitt_y $match(emitt_y)"

set off $params(offset)
set waist $params(waist_y)


#
# Linacs
#

source $script_dir/lattices/lattice4b.tcl


foreach name { linelectron  linpositron } {
    puts "Putting Linac"
    global n_total n_slice n_part
    BeamlineNew
    set phase 8a
    set e_initial 9.0
    set e0 $e_initial
    Linac::choose_linac $script_dir/lattices/phase.def $phase
    Linac::put_linac
    TclCall -script "save_beam_$name"
    BeamlineSet -name $name
    # this is needed in order to overwrite the sliced beam info
}

set cav0 [CavityGetPhaseList]
set grad0 [CavityGetGradientList]

proc my_survey { } {

    set bpm_res 0.0
    set jitter 0.0
    set phase_err 1.0
    SurveyErrorSet -quadrupole_y 0.0 \
	-bpm_y 0.0 \
	-cavity_y 0.0 \
	-cavity_realign_y 0.0 \
	-cavity_yp 0.0

    Clic
    set cav {}
    #set grad {}
    #set fcav [open cav.dat.$i w]
    foreach c $cav0 g $grad0 {
	set cc [expr $c+[gcav]*$phase_err]
	#set gg [expr $g+[gcav]*$gradient_err]
	#puts $fcav "$gg $cc"
	lappend cav $cc
	#lappend grad $gg
    }
    # close $fcav
    CavitySetPhaseList $cav
    #CavitySetGradientList $grad
    #SaveAllPositions -vertical_only 0 -binary 0 -cav 1 -nodrift 1 -file buc.$i
    puts "misalignment done"
    
}

proc save_beam_linpositron { } {
    global j
    BeamSaveAll -file positron_linac.$j.ini -bunches 1
#    BeamDump -file positron_linac.$j
#         array set params [BeamMeasure]
#         set beam_emix $params(emitt_x)
#         set beam_emiy $params(emitt_y)
#         set beam_betx $params(beta_x)
#         set beam_bety $params(beta_y)
#         set beam_alpx $params(alpha_x)
#         set beam_alpy $params(alpha_y)
#         set beam_s $params(s)
#         puts "beam longitudinal position = $beam_s"
#         puts "beam emix = $beam_emix"
#         puts "beam emiy = $beam_emiy"
#         puts "beam betax = $beam_betx"
#         puts "beam betay = $beam_bety"
#         puts "beam alpx = $beam_alpx"
#         puts "beam alpy = $beam_alpy"
#         set sig_x [expr sqrt($beam_betx*$beam_emix*1e-7/(3.0*1e6))]
#         set sig_y [expr sqrt($beam_bety*$beam_emiy*1e-7/(3.0*1e6))]
#         puts "beam sigx = $sig_x"
#         puts "beam sigy = $sig_y"
}

proc save_beam_linelectron { } {
    global j
    BeamSaveAll -file electron_linac.$j.ini -bunches 1
    #    BeamDump -file electron_linac.$j
    #     array set params [BeamMeasure]
    #     set beam_emix $params(emitt_x)
    #     set beam_emiy $params(emitt_y)
    #     set beam_betx $params(beta_x)
    #     set beam_bety $params(beta_y)
    #     set beam_alpx $params(alpha_x)
    #     set beam_alpy $params(alpha_y)
    #     set beam_s $params(s)
    #     puts "beam longitudinal position = $beam_s"
    #     puts "beam emix = $beam_emix"
    #     puts "beam emiy = $beam_emiy"
    #     puts "beam betax = $beam_betx"
    #     puts "beam betay = $beam_bety"
    #     puts "beam alpx = $beam_alpx"
    #     puts "beam alpy = $beam_alpy"
    #     set sig_x [expr sqrt($beam_betx*$beam_emix*1e-7/(3.0*1e6))]
    #     set sig_y [expr sqrt($beam_bety*$beam_emiy*1e-7/(3.0*1e6))]
    #     puts "beam sigx = $sig_x"
    #     puts "beam sigy = $sig_y"
}

#set n_bunches 312
set n_bunches 1
if [ expr $n_bunches > 1 ] {
 foreach beamname { electron  positron } {
    #    Generate beam: multibunch 
    global n_slice n_part n_bunches
    puts "Generating the beam"
    puts "n. slices $n_slice "
    puts "n. particles $n_part "
    #    make_beam_slice_energy_gradient beam_${beamname} $n_slice $n_part 1.0 1.0
    make_beam_slice_energy_gradient_train beam_${beamname} $n_slice $n_part 1.0 1.0 $n_bunches
 }
 foreach beamname { electron1  positron1 } {
    #    Generate beam
    global n_slice n_part
    puts "Generating the beam"
    puts "n. slices $n_slice "
    puts "n. particles $n_part "
    #    make_beam_slice_energy_gradient beam_${beamname} $n_slice $n_part 1.0 1.0
    make_beam_slice_energy_gradient beam_${beamname} $n_slice $n_part 1.0 1.0 
 }
} else {
 foreach beamname { electron  positron } {
    #    Generate beam: single bunch
    global n_slice n_part n_bunches
    puts "Generating the beam"
    puts "n. slices $n_slice "
    puts "n. particles $n_part "
    make_beam_slice_energy_gradient beam_${beamname} $n_slice $n_part 1.0 1.0
 }
}

#source $script_dir/scripts/survey.tcl
set atl_time 1e6
# number of iteration for the linac
for {set j 0} {$j<$nm} {incr j} {
    Zero
    # deve diventare track_nominal
    puts "BEGIN LINAC electron "
    BeamlineUse -name linelectron  
     TestNoCorrection -beam beam_electron -emitt_file emitt_linelectron.$j.dat -survey Zero -machines 1
    #TestNoCorrection -beam beam_electron -emitt_file emitt_linelectron.$j.dat -survey my_survey -machines 1
    puts "BEGIN LINAC positron "
    BeamlineUse -name linpositron 
     TestNoCorrection -beam beam_positron -emitt_file emitt_linpositron.$j.dat -survey Zero -machines 1
    #TestNoCorrection -beam beam_positron -emitt_file emitt_linpositron.$j.dat -survey my_survey -machines 1    
}

proc save_bdselectron {} {
    global k nk
    BeamDump -file bds_electron.$k.$nk
    array set params [BeamMeasure]
    set beam_emix $params(emitt_x)
    set beam_emiy $params(emitt_y)
    set beam_betx $params(beta_x)
    set beam_bety $params(beta_y)
    set beam_alpx $params(alpha_x)
    set beam_alpy $params(alpha_y)
    set beam_s $params(s)
    puts "beam longitudinal position = $beam_s"
    puts "beam emix = $beam_emix"
    puts "beam emiy = $beam_emiy"
    puts "beam betax = $beam_betx"
    puts "beam betay = $beam_bety"
    puts "beam alpx = $beam_alpx"
    puts "beam alpy = $beam_alpy"
    set sig_x [expr sqrt($beam_betx*$beam_emix*1e-7/(3.0*1e6))]
    set sig_y [expr sqrt($beam_bety*$beam_emiy*1e-7/(3.0*1e6))]
    puts "beam sigx = $sig_x"
    puts "beam sigy = $sig_y"
}

proc save_bdspositron {} {
    global k nk
    BeamDump -file bds_positron.$k.$nk
    array set params [BeamMeasure]
    set beam_emix $params(emitt_x)
    set beam_emiy $params(emitt_y)
    set beam_betx $params(beta_x)
    set beam_bety $params(beta_y)
    set beam_alpx $params(alpha_x)
    set beam_alpy $params(alpha_y)
    set beam_s $params(s)
    puts "beam longitudinal position = $beam_s"
    puts "beam emix = $beam_emix"
    puts "beam emiy = $beam_emiy"
    puts "beam betax = $beam_betx"
    puts "beam betay = $beam_bety"
    puts "beam alpx = $beam_alpx"
    puts "beam alpy = $beam_alpy"
    set sig_x [expr sqrt($beam_betx*$beam_emix*1e-7/(3.0*1e6))]
    set sig_y [expr sqrt($beam_bety*$beam_emiy*1e-7/(3.0*1e6))]
    puts "beam sigx = $sig_x"
    puts "beam sigy = $sig_y"
}

# putting the BDS lattice

foreach line { bdselectron bdspositron } {
    puts "Putting BDS electron "
    BeamlineNew
    Girder
    SlicesToParticles -seed 0 -type 1 
    set quad_synrad 1
    set sbend_synrad 1
    set mult_synrad 1
    set e_initial 1500.0
    set e0 $e_initial
    source $script_dir/lattices/bds.match.linac4b
    ReferencePoint -sense -1
    FirstOrder 1
    TclCall -script "save_$line"
    BeamlineSet -name $line
}

array set gp_param "
    energy 1500.0
    particles [expr $match(charge)*1.e-10]
    sigmaz $match(sigma_z)
    cut_x 400.0
    cut_y 35.0
    n_x 128
    n_y 640
    do_coherent 1
    n_t 1
    charge_sign -1.0
    ecm_min 2970."

source $script_dir/scripts/clic_guinea.tcl
proc run_guinea {offx offy} {
    global gp_param k nk
    set res [exec grid]
    if [ expr $offy != 0.0 ] {
	set yoff [expr $offy-0.5*([lindex $res 2]+[lindex $res 3])]
    } else {
	set yoff [expr 0.5*([lindex $res 2]+[lindex $res 3])]	
    }
    if [ expr $offx != 0.0 ] {
	set xoff [expr $offx-0.5*([lindex $res 0]+[lindex $res 1])]
    } else {
	set xoff [expr 0.5*([lindex $res 0]+[lindex $res 1])]	
    }
    set tx $gp_param(cut_x)
    set ty $gp_param(cut_y)
    if {$offx+([lindex $res 1]-[lindex $res 0])>2.0*$tx} {
      set gp_param(cut_x) [expr 0.5*([lindex $res 1]-[lindex $res 0])+0.5*$offx]
    }
    if {$offy+([lindex $res 3]-[lindex $res 2])>2.0*$ty} {
      set gp_param(cut_y) [expr 0.5*([lindex $res 3]-[lindex $res 2])+0.5*$offy]
    }
    puts "yoff $yoff"
    puts "yoff $xoff"
    puts $res
    puts "cut_x $gp_param(cut_x)"
    puts "cut_y $gp_param(cut_y)"
    puts "cut_z $gp_param(sigmaz)"
    puts "ch $gp_param(particles)"	
    #
    # Modify this
    #
    #write_guinea_correct_angle_xy $xoff 0.0 $yoff 0.0
    write_guinea_correct $xoff $yoff

    exec guinea default default default
#     exec  cp photon.dat  photon.dat.$k.$nk
#     exec  cp hadron.dat  hadron.dat.$k.$nk
#     exec  cp beam1.dat   beam1.dat.$k.$nk
#     exec  cp beam2.dat   beam2.dat.$k.$nk
#     exec  cp coh1.dat    coh1.dat.$k.$nk
#     exec  cp coh2.dat    coh2.dat.$k.$nk
#     exec  cp pairs.dat   pairs.dat.$k.$nk
#     exec  cp lumi.ee.out lumi.ee.out.$k.$nk
#     exec  cp default     default.$k.$nk

    #exec  grep lumi_ee= default > lumi.$k.$nk
    #exec  grep lumi_ee_high= default > lumi_high.$k.$nk

    set gp_param(cut_x) $tx
    set gp_param(cut_y) $ty
    return [get_results default]
}

for {set k 0} {$k<$nm} {incr k} {
    for {set nk 0} {$nk<$n_bunches} {incr nk} {
	#    set nk 0
	BeamlineUse -name bdselectron
        if [ expr $n_bunches > 1 ] {
	 BeamLoadAll -beam beam_electron1 -file electron_linac.$k.ini.$nk
	 puts "Tracking BDS electron "
	 TestNoCorrection -beam beam_electron1 -emitt_file emitt.bdselectron.$k.$nk -machines 1 -survey Zero
        } else {       
         BeamLoadAll -beam beam_electron -file electron_linac.$k.ini.$nk
         puts "Tracking BDS electron "
         TestNoCorrection -beam beam_electron -emitt_file emitt.bdselectron.$k.$nk -machines 1 -survey Zero 
        }
	BeamlineUse -name bdspositron
        if [ expr $n_bunches > 1 ] {
         BeamLoadAll -beam beam_positron1 -file positron_linac.$k.ini.$nk
         puts "Tracking BDS positron "
         TestNoCorrection -beam beam_positron1 -emitt_file emitt.bdspositron.$k.$nk -machines 1 -survey Zero
        } else {
         BeamLoadAll -beam beam_positron -file positron_linac.$k.ini.$nk
        puts "Tracking BDS positron "
        TestNoCorrection -beam beam_positron -emitt_file emitt.bdspositron.$k.$nk -machines 1 -survey Zero
        }
	puts " RUNNING GUINEA-PIG "
	exec cp bds_electron.$k.$nk electron.ini
	exec cp bds_positron.$k.$nk positron.ini
        run_guinea 0.0 $off 
    }
}

exit

#}
