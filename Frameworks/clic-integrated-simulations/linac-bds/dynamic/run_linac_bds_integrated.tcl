# global variables
# directory structure
set script_dir [pwd]
source $script_dir/scripts/tcl_procs.tcl
source $script_dir/scripts/load_settings.tcl

#setting up beam, beamline
source $script_dir/scripts/accelerator_setup.tcl

Octave {

    %%%%%%%%%%%%%%%%%%%%%%%%%
    % Global initialization %
    %%%%%%%%%%%%%%%%%%%%%%%%%

    % load and calculate response matrices
    % calculate nominal values and system identification
    source "$script_dir/scripts/global_init.m"
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Go through all machines %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    for machine_index = 1:nr_machines
      
      fprintf(1, '\n Machine: %f of %f \n\n', machine_index, nr_machines);
 
      %%%%%%%%%%%%%%
      % Local Init %
      %%%%%%%%%%%%%%

      %% machine setup, reinitialisation of ground motion, dynamic imperfections, 
      source "$script_dir/scripts/machine_setup.m"
      time_step_index_long = 1;
      sim_mode = 1;

      %% Inperfections as misalignments and others
      source "$script_dir/scripts/machine_imperfections.m"

      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      % Go through all time steps %
      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
      for time_step_index = 1:nr_time_steps_short
	
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	% Apply ground motion and other disturbances %
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      
        sim_mode = 2;
	global_time = global_time + delta_T_short;
	fprintf(1, "Time: %fs of %fs\n", global_time, nr_time_steps_short*delta_T_short);
	time_step_index_short = time_step_index;

        source "$script_dir/scripts/dynamic_setup.m";
	
	%%%%%%%%%%%%%%%%%%%%%%%%%
	% System Identification %
	%%%%%%%%%%%%%%%%%%%%%%%%%
	
        % system identification setup
        source "$script_dir/scripts/sysident_setup.m"

	%%%%%%%%%%%%
	% Tracking %
	%%%%%%%%%%%%

        source "$script_dir/scripts/tracking.m"

	%%%%%%%%%%%%%%%%%
	% System Update %
	%%%%%%%%%%%%%%%%%

        % updating of the parameters (response matrix, etc.)
        source "$script_dir/scripts/sysident_update.m"
      
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	% Calculate control algorithm %
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
        source "$script_dir/scripts/controller_calc.m"

	%%%%%%%%%%%%%%%%%%%%%
	% Apply Corrections %
	%%%%%%%%%%%%%%%%%%%%%

        source "$script_dir/scripts/controller_apply.m"

	%%%%%%%%%%%%%%%%%%%%%
	% Store the results %
	%%%%%%%%%%%%%%%%%%%%%
	
        % store results and end process
        source "$script_dir/scripts/postprocess_time_step.m"
	
      end
      
      % Delete the useless files / Save some parameters 
      source "$script_dir/scripts/postprocess_machine.m"
      
    end
    
    % Delete the useless files / Save some parameters 
    source "$script_dir/scripts/postprocess_run.m"
}

exit
