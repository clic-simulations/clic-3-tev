# This script tracks the BDS+linac once until final focus
# After that the Final Focus (starting at QF1) is run independently
# Builtin Ground motion and possible misalignment. 
# It uses Annecy control method.

Octave {
    addpath("/usr/share/placet/octave"); #shouldn't be necessary, adjust it to your own location!
}

set script_dir [pwd]
# load my settings
source $script_dir/my_settings_FF_control.tcl

# load general tcl procedures
source $script_dir/scripts/tcl_procs.tcl
# export whole tcl environment into Octave:
export_env_2_octave ""
# load general tracking procedures
source $script_dir/scripts/tracking_procs.tcl
# load ground motion procedures
source $script_dir/scripts/ground_motion.tcl
# load IP Dipole procedures
source $script_dir/scripts/IPDipoleCorrection.tcl

array set params $argv
set seed $params(seed)
set niter $params(niter)
set distbpm $params(distbpm)
set gmotion $params(gmotion)
set gmseed $params(gmseed)
set time_step $params(time_step)
set delta_T $time_step

puts "Number of Iterations: $niter"

#
# Set output directory
#
set resultdir FF_control_bpm_error_${bpm_error}_time_step_$params(time_step)
if {$use_ground_motion} {append resultdir "_${gmseed}"}
if {$testdir} {set resultdir "test"}
exec rm -rf $resultdir
exec mkdir -p $resultdir
exec touch last_run
exec rm last_run
exec ln -fs $resultdir last_run
cd $resultdir

# save current settings:
save_state settings.dat

#
# General
#
#Random -type gaussian gcav
Random8 -type gaussian gcav

# main linac
source $script_dir/scripts/clic_basic_single.tcl
source $script_dir/scripts/clic_beam.tcl

set match(n_total) $n_total
set match(emitt_y) $params(emitt_y)
set match(sigma_z) $params(sigma_z)
set match(charge) $params(ch)
set charge $match(charge)
set match(emitt_x) $params(emitt_x)
set match(e_spread) $params(e_spread)

puts "charge $match(charge)"
puts "emitt_y $match(emitt_y)"

set off $params(offset)
set waist $params(waist_y)

set n_bunches 1

#
# Linacs lattice and functions
#

source $script_dir/lattices/lattice4b.tcl

# misalignment and error survey for Linac

proc my_survey { } {
    global params bpm_error
    set cav0 [CavityGetPhaseList]
    set grad0 [CavityGetGradientList]
    set bpm_res $bpm_error
    set phase_err 0.0
    SurveyErrorSet -quadrupole_y $params(quad_misalign) \
	-bpm_y 0.0 \
	-cavity_y 0.0 \
	-cavity_realign_y 0.0 \
	-cavity_yp 0.0

    # misalign beamline according to survey
    Clic
    #Zero
    set cav {}
    foreach c $cav0 g $grad0 {
	set cc [expr $c+[gcav]*$phase_err]
	lappend cav $cc
    }
    # close $fcav
    CavitySetPhaseList $cav
    puts "misalignment done"    
}

# iteration number
set time_step_index 0

foreach name { linelectron  linpositron } {

    puts "Putting Linac"
    #global n_total n_slice n_part
    BeamlineNew
    #Girder
    #ReferencePoint -sense 1
    set phase 8a
    set e_initial 9.0
    set e0 $e_initial
    Linac::choose_linac $script_dir/lattices/phase.def $phase
    Linac::put_linac
    TclCall -script "save_beam_$name"
    BeamlineSet -name $name
    BeamlinePrint -file "beamline_${name}.txt"
}

#
# set up the beam
#

foreach beamname { electron  positron } {
    #    Generate beam
    global n_slice n_part n_bunches
    puts "Generating the beam"
    puts "n. slices $n_slice "
    puts "n. particles $n_part "
    make_beam_slice_energy_gradient beam_${beamname} $n_slice $n_part 1.0 1.0
    #make_beam_slice_energy_gradient_train beam_${beamname} $n_slice $n_part 1.0 1.0 $n_bunches
    BeamSaveAll -file ${beamname}_start.dat
}

# this is needed only to set up the beam in the bds when one simulate the full train in the linac
#foreach beamname { electron1  positron1 } {
#    #    Generate beam
#    global n_slice n_part
#    puts "Generating the beam"
#    puts "n. slices $n_slice "
#    puts "n. particles $n_part "
#    make_beam_slice_energy_gradient beam_${beamname} $n_slice $n_part 1.0 1.0
#}

#
# track linacs
#
for {set machine_index 0} {$machine_index<$nm} {incr machine_index} {
    foreach name {electron positron} {
	Zero
	puts "BEGIN LINAC $name"
	BeamlineUse -name lin$name  
	TestNoCorrection -beam beam_$name -emitt_file emitt_lin${name}_${machine_index}.dat -survey my_survey -machines 1
    }
}

# free memory of linac beamline which is no longer used:
foreach beamname { linelectron linpositron } {
    BeamlineDelete -name $beamname
}

puts "The END of Linac"

#
# BDS lattice and functions
#

set sign -1
foreach name { bdselectron bdspositron} {
    puts "Putting BDS"
    BeamlineNew
    Girder
    ReferencePoint -sense 1
    SlicesToParticles -seed 0 -type 1
    set quad_synrad 1
    set sbend_synrad 1
    set mult_synrad 1
    set synrad 1
    set e_initial 1500.00
    set e0 $e_initial
    source $script_dir/lattices/bds.match.linac4b_noape
    #source $script_dir/lattices/bds.match.linac4b_noape_monitor
    ReferencePoint -sense -1

    TclCall -script "save_beam_$name"
    BeamlineSet -name $name
    BeamlinePrint -file "beamline_${name}.txt"

    Octave {
	$name.IPDIPOLE = placet_get_name_number_list("$name", "IPDIPOLE");
	$name.BPMIP = placet_get_name_number_list("$name", "BPMIP");
	placet_element_set_attribute("$name", $name.BPMIP, "resolution", $bpm_error_IP);
	QF1 = placet_get_name_number_list("$name", "QF1")
	Tcl_SetVar('start_cantilever',QF1(end));
	IPDipole = placet_get_name_number_list("$name", "IPDIPOLE")
	Tcl_SetVar('end_cantilever',IPDipole(end));
    }

    if {$use_ground_motion} {
	puts "NOW initializing GM"
	ground_motion_init $name $groundmotion(type) $groundmotion(model) $groundmotion(seed_nr) $sign
    }
    # flip sign
    set sign [expr $sign*-1]
}

#
# GUINEA-PIG related functions
#

source $script_dir/scripts/clic_guinea.tcl


# function for IPDipole correction:

#
# track bds: ground motion and orbit correction
#

for {set machine_index 0} {$machine_index<$nm} {incr machine_index} {
    set lumi 0
    set nk 0

    puts "**********************************";
    puts "********** Machine Nr $machine_index **********";
    puts "**********************************";

    # realign machine:
    Zero
    
    Octave {
	index=0;
	Lumi=zeros($niter,13);
	Anglex=0;
	Angley=0;
    }

    for {set time_step_index 0} {$time_step_index<$niter} {incr time_step_index} {
	Octave {index++;}
	puts "********** Iteration $time_step_index **********";
	
	##### Orbit correction applying correction #####
	
	if {$time_step_index>0} {
	    puts "NOW applying orbit correction $name"
	    Octave {
		IPdipole_strength = IPDipoleCorrection(Anglex,Angley)
		
		% set dipole kicker strength
		% this kick will be evenly distributed along the positron and electron line
		
		placet_element_vary_attribute("bdselectron", bdselectron.IPDIPOLE, "strength_x", IPdipole_strength(1) );
		placet_element_vary_attribute("bdselectron", bdselectron.IPDIPOLE, "strength_y", IPdipole_strength(2) );
		placet_element_vary_attribute("bdspositron", bdspositron.IPDIPOLE, "strength_x", -1.*IPdipole_strength(1) );
		placet_element_vary_attribute("bdspositron", bdspositron.IPDIPOLE, "strength_y", -1.*IPdipole_strength(2) );
		Lumi(index,10) = IPdipole_strength(1);
		Lumi(index,11) = IPdipole_strength(2);
		
	    }
	}
		
	
	#####################   APPLY  GM  ####################################
	
	foreach name {bdselectron bdspositron} {
	    if {$use_ground_motion} {
		if {$name == "bdselectron"} {
		    ground_motion $name $groundmotion(type)
		    set groundmotion_applied 1
		}
	    } else {
		puts "No GM applied $name"
		set groundmotion_applied 0
	    }
	}

	#####################   TRACK  BEAM  ###################################

	foreach name {electron positron} {
	    BeamlineUse -name bds$name
	    BeamLoadAll -beam beam_$name -file ${name}_linac_${machine_index}.dat
	    #BeamRead -beam beam_$name -file ${name}_linac_${machine_index}.dat
	    
	    Octave {
		[E, Beam_$name] = placet_test_no_correction("bds$name", "beam_$name", "None");
		save_beam("$name.ini", Beam_$name);
	    }
	    #track_no_correction bds$name
	    
	    #Octave {
	    # Save BPM readings
	    #	b=placet_get_bpm_readings("bds$name",bds$name.BPMIP,false);
	    #bpmIP_x = b(1,1)
	    #bpmIP_y = b(1,2)
	    #save -text bpm_reading_${name}_${machine_index}_${time_step_index}.out b;
	    #}
	}
	
	#####################   COLLISION!  ##################################
	    
	#set guinea_offx 0
	
	# we run guinea-pig
	set res [run_guinea 0.0 0.0]
	
	Octave {
	    
	    phspacep=load("positron.ini");
	    phspacee=load("electron.ini");
	    Tcl_Eval("set lumi [lindex \$res 0]");
	    Tcl_Eval("set lumipeak [lindex \$res 1]");
	    Tcl_Eval("set anglex [lindex \$res 3]");
	    Tcl_Eval("set angley [lindex \$res 4]");
	    Tcl_Eval("puts \"Lumi = \$lumi ; Angle(x,y) = \$anglex ; \$angley \"");
	    # IP position calculation
	    posy=mean(phspacep(:,3));
	    eley=mean(phspacee(:,3));
	    disp("positron y: "), disp(posy), disp("electron y: "), disp(eley);
	    # we read the results from Tcl
	    Anglex = str2num(Tcl_GetVar("anglex"));
	    Angley = str2num(Tcl_GetVar("angley"));
	    Lumi(index,1) = $time_step_index;
	    Lumi(index,2) = 0; #time_laten;
	    Lumi(index,3) = str2num(Tcl_GetVar("lumi"));
	    Lumi(index,4) = str2num(Tcl_GetVar("lumipeak"));
	    Lumi(index,5) = Anglex;
	    Lumi(index,6) = Angley;
	    Lumi(index,7) = eley;
	    Lumi(index,8) = posy;
	    Lumi(index,9) = 0;
	    %Lumi(index,10) = IPdipole_strength(1);
	    %Lumi(index,11) = IPdipole_strength(2);
	    Lumi(index,12) = 0;
	    Lumi(index,13) = $groundmotion_applied;

	    temp = Lumi(index,:);
	    save -ascii "lumi_test_${machine_index}_${niter}.res_${gmseed}_${time_step_index}" temp;

	    useless_file_name = sprintf('electron_bds_%d.dat', time_step_index);
	    delete(useless_file_name);
	    useless_file_name = sprintf('electron_bds_beampar_%d.dat', time_step_index);
	    delete(useless_file_name);
	    useless_file_name = sprintf('positron_bds_%d.dat', time_step_index);
	    delete(useless_file_name);
	    useless_file_name = sprintf('positron_bds_beampar_%d.dat', time_step_index);
	    delete(useless_file_name);
	    useless_file_name = sprintf('gm_bdselectron_%d.dat', time_step_index);
	    delete(useless_file_name);
	    useless_file_name = sprintf('gm_bdspositron_%d.dat', time_step_index);
	    delete(useless_file_name);
	}
    }
    
    Octave {
	save -ascii "lumi_test_${machine_index}_${niter}.res_${gmseed}" Lumi;
    }
}
puts "The END"