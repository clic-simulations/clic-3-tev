# This file loads the settings for the job and initialises the run environment

# Include general default settings:
source $script_dir/settings.tcl
# Include long term settings:
source $script_dir/settings_longterm.tcl

# Include private run-specific settings file (if given as command line option, else only default is used)
if {$argc >= 1} {
    set settings_name [lindex $argv 0]
    puts "load run settings: $settings_name"

    # check if private variables are existing and issue a warning if not
    check_file ${script_dir}/${settings_name}
    source ${script_dir}/${settings_name}
}

# export tcl variables to Octave and vice versa, create directories:
source $script_dir/scripts/prepare_environment.tcl