%% Script to run the IP feedback for some time steps to centre the beam
%%
%% Juergen Pfingstner
%% 17th of April 2012


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Center the beams approxiomately %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% dEBUG
%% Debug
corr_setting_ip_x_elec_print = placet_element_get_attribute("electron", ipdipole_index(electron,:), "strength_x");
corr_setting_ip_y_elec_print = placet_element_get_attribute("electron", ipdipole_index(electron,:), "strength_y");
corr_setting_ip_x_posi_print = placet_element_get_attribute("positron", ipdipole_index(electron,:), "strength_x");
corr_setting_ip_y_posi_print = placet_element_get_attribute("positron", ipdipole_index(electron,:), "strength_y");
fprintf(1,"Corr settings: elecx %g, posix %g, elecy %g, posiy %g\n", corr_setting_ip_x_elec_print, corr_setting_ip_x_posi_print, corr_setting_ip_y_elec_print, corr_setting_ip_y_posi_print);
tracking;
%delta_x = elex-posx;
%delta_y = eley-posy;

% Calculate the perfect offset
[ elec_opt, posi_opt, offset_x_opt, beam_beam_meas_init_x, beam_beam_meas_opt_x ] = ...
    max_beam_beam( Beam(:,:,electron), Beam(:,:,positron), 'x');
[ elec_opt, posi_opt, offset_y_opt, beam_beam_meas_init_y, beam_beam_meas_opt_y ] = ...
    max_beam_beam( elec_opt, posi_opt, 'y');

%offset_x_opt
%offset_y_opt
%keyboard;

%%if(use_optimal_offset == 0)
%%    ip_kick_x = -delta_x/2/r_kicker_ip_x;
%%    ip_kick_y = -delta_y/2/r_kicker_ip_y;
%%else
ip_kick_x = -offset_x_opt/2/r_kicker_ip_x;
ip_kick_y = -offset_y_opt/2/r_kicker_ip_y;
%%end

for i=electron:positron
    line_name = beamlinename(i,:);
    if(i == electron)
	sign_kick = 1;
    else
	sign_kick = -1;
    end
    placet_element_set_attribute(line_name, ipdipole_index(i,:), "strength_x", sign_kick*ip_kick_x);
    placet_element_set_attribute(line_name, ipdipole_index(i,:), "strength_y", sign_kick*ip_kick_y);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Save old values and apply new ones %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

nr_iter_centre_beam = 5;

use_controller_x_old = use_controller_x;
use_controller_y_old = use_controller_y;
use_ip_feedback_old = use_ip_feedback;
use_ip_feedback_x_old = use_ip_feedback_x;
use_ip_feedback_y_old = use_ip_feedback_y;
bpm_ip_res_old = bpm_ip_res; 
use_ip_feedback_linear_old = use_ip_feedback_linear;
use_ip_feedback_pid_old = use_ip_feedback_pid;
use_ip_feedback_annecy_old = use_ip_feedback_annecy;
gain_bb_x_old = gain_bb_x;
gain_bb_y_old = gain_bb_y; 

use_controller_x = 0;
use_controller_y = 0;
use_ip_feedback = 1;
use_ip_feedback_x = 1;
use_ip_feedback_y = 1;
bpm_ip_res = 0; 
use_ip_feedback_linear = 1;
use_ip_feedback_pid = 0;
use_ip_feedback_annecy = 0;
gain_bb_x = 0.5;
gain_bb_y = 0.5; 
%gain_bb_x = 1;
%gain_bb_y = 1; 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Centre the beams by running the IP feedback in a loop %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for index_centre_beam=1:nr_iter_centre_beam

    time_step_index = time_step_index+1;
    Tcl_SetVar("time_step_index", time_step_index);
    global_time = global_time + delta_T_short;

    %% Tracking
    tracking

    %% Debug
    fprintf(1,"Iteration %g\n", index_centre_beam);
    corr_setting_ip_x_elec_print = placet_element_get_attribute("electron", ipdipole_index(electron,:), "strength_x");
    corr_setting_ip_y_elec_print = placet_element_get_attribute("electron", ipdipole_index(electron,:), "strength_y");
    corr_setting_ip_x_posi_print = placet_element_get_attribute("positron", ipdipole_index(electron,:), "strength_x");
    corr_setting_ip_y_posi_print = placet_element_get_attribute("positron", ipdipole_index(electron,:), "strength_y");
    fprintf(1,"Corr settings: elecx %g, posix %g, elecy %g, posiy %g\n", corr_setting_ip_x_elec_print, corr_setting_ip_x_posi_print, corr_setting_ip_y_elec_print, corr_setting_ip_y_posi_print);
    %% dEBUG

    %% Calculate control algorithm
    controller_calc
		
    %% Apply Corrections 
    controller_apply

end
tracking

%%%%%%%%%%%%%%%%%%%%%%%
%% Restore old values %
%%%%%%%%%%%%%%%%%%%%%%%

use_ip_feedback = use_ip_feedback_old;
use_controller_x = use_controller_x_old;
use_controller_y = use_controller_y_old;
use_ip_feedback_x = use_ip_feedback_x_old;
use_ip_feedback_y = use_ip_feedback_y_old;
bpm_ip_res = bpm_ip_res_old; 
gain_bb_x = gain_bb_x_old;
gain_bb_y = gain_bb_y_old; 

use_ip_feedback_linear = use_ip_feedback_linear_old;
use_ip_feedback_pid = use_ip_feedback_pid_old;
use_ip_feedback_annecy = use_ip_feedback_annecy_old;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Update controller values %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if(use_ip_feedback_pid == 1)  
    volt_ipdipole_y = placet_element_get_attribute('electron', ipdipole_index(1,:), "strength_x");;
    volt_ipdipole_y_prev = volt_ipdipole_y;
    volt_ipdipole_y_new = volt_ipdipole_y;
    angley_volt_ipdipole_prev = 0;
end

if(use_ip_feedback_annecy == 1)
    IPdipole_strength_electron_y = placet_element_get_attribute('electron', ipdipole_index(1,:), "strength_x");
    IPdipole_strength_positron_y = -IPdipole_strength_electron_y;
end