proc sum_files {fn0 nm} {
    set f [open $fn0.0 r]
    set i 0
    gets $f line
    while {![eof $f]} {
	set ex($i) [lindex $line 1]
	set ey($i) [lindex $line 5]
	gets $f line
	incr i
    }
    close $f
    set n [expr $i-2]
    
    for {set j 1} {$j<$nm} {incr j} {
	set f [open $fn0.$j r]
	for {set i 0} {$i<$n} {incr i} {
	    gets $f line
	    set ex($i) [expr $ex($i)+[lindex $line 1]]
	    set ey($i) [expr $ey($i)+[lindex $line 5]]
	}
	close $f
    }
    
    set f [open $fn0.sum w]
    for {set i 0} {$i<$n} {incr i} {
	puts $f "$i [expr $ex($i)/$nm.0] [expr $ey($i)/$nm.0]"
    }
    close $f
}
