######################################
# Section to export/import variables #
######################################

# export whole tcl environment into Octave:
export_env_2_octave ""

# export individual parameters:
Octave {
    % Create directory:
    mkdir(dir_name);
    cd(dir_name);
}

# print time:
set timestamp [clock format [clock seconds]]
puts "$timestamp"

# save current settings:
save_state settings.dat
