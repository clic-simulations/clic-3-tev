Tcl_SetVar('time_step_index',time_step_index);
Tcl_SetVar('machine_index',machine_index);

for i=electron:positron
  beam_name = ['beam_',beamlinename(i,:)];
  Tcl_SetVar('name', beamlinename(i,:));
  Tcl_Eval("BeamlineUse -name $name");
  if (use_main_linac==1)
    % probably not needed
    %Tcl_Eval("BeamLoadAll -beam beam_$name -file ${name}_start.dat");
    Tcl_Eval("BeamDelete -beam beam_$name");
    Tcl_Eval("make_beam_slice_energy_gradient beam_$name $n_slice $n_part $energy_amplification 1.0");
  else
    Tcl_Eval("BeamLoadAll -beam beam_$name -file ${name}_linac_-1.dat");
    if(energy_amplification ~= 1)
	fprintf(1, "Constant initial energy jitter not yet implemented for only the BDS\n");
    end
  end

  if (use_bds==1)
    seed_dummy = placet_element_get_attribute(beamlinename(i,:),s2p_index(i,:),'seed');
    placet_element_set_attribute(beamlinename(i,:),s2p_index(i,:), 'seed',seed_dummy+123+i);
  end
  %[Emitt,Beam(:,:,i)]=placet_test_no_correction(beamlinename(i,:), beam_name, "None");
  [Emitt,Beam_temp]=placet_test_no_correction(beamlinename(i,:), beam_name, "None");
  if(isempty(Beam_temp))
      %% Do not store anything
      empty_beam(i) = 1;
  else
      Beam(:,:,i) = Beam_temp;
      empty_beam(i) = 0;
  end
  
  if (use_bds==1)
    filename = [beamlinename(i,:),'.ini'];
    save_beam(filename, Beam(:,:,i));
  end
end

% get BPM readings
for i=electron:positron
  if (bpm_noise == 1)
    bpm_readings(:,:,i)=placet_get_bpm_readings(beamlinename(i,:), bpm_index(i,:), false);
  else
    bpm_readings(:,:,i)=placet_get_bpm_readings(beamlinename(i,:), bpm_index(i,:), true);
  end

  if (use_bpm_failure == 1)
    % set bpm readings to 0 of failed bpms
    bpm_readings(:,1,i) = bpm_readings(:,1,i).*(1-bpm_failure(:,i));
    bpm_readings(:,2,i) = bpm_readings(:,2,i).*(1-bpm_failure(:,i));
  end

  if(bpm_scaling_error_x == 1)
      % At the moment also the BPM noise is scaled, this has to be considered
      bpm_readings(:,1,i) = bpm_readings(:,1,i).*bpm_scaling_values_x(i,:)'; 
  end
  if(bpm_scaling_error_y == 1)
      % At the moment also the BPM noise is scaled, this has to be considered
      bpm_readings(:,2,i) = bpm_readings(:,2,i).*bpm_scaling_values_y(i,:)'; 
  end
end

if (use_beam_beam == 1)
  % Check if beam is not empty:
  %%if (isempty(Beam(:,:,electron)) || isempty(Beam(:,:,positron)))
  if (empty_beam(electron) || empty_beam(positron))
    Lumi=0;
    Lumi_high=0;
    Anglex = 0;
    Angley = 0;
  else
    % Run Guinea-Pig
    Tcl_Eval("set res [run_guinea 0.0 0.0]");
    % Output:
    Tcl_Eval("set lumi [lindex $res 0]");
    Tcl_Eval("set lumipeak [lindex $res 1]");
    Tcl_Eval("set anglex [lindex $res 3]");
    Tcl_Eval("set angley [lindex $res 4]");
    % For corrector calculation (done in Octave):
    Anglex = str2num(Tcl_GetVar("anglex"));
    Angley = str2num(Tcl_GetVar("angley"));
    Lumi = str2num(Tcl_GetVar("lumi"));
    Lumi_high = str2num(Tcl_GetVar("lumipeak"));
    Tcl_Eval("puts \"Lumi = $lumi ; Angle = $anglex ; $angley \"");
    # IP position calculation
    phspacep=load("positron.ini");
    phspacee=load("electron.ini");
    posx=mean(phspacep(:,2));
    elex=mean(phspacee(:,2));
    posy=mean(phspacep(:,3));
    eley=mean(phspacee(:,3));
    disp("positron y: "), disp(posy), disp("electron y: "), disp(eley);
  end
end
