% Function to load the response matrix of the quadrupoles for the y
% direction.
%
% Juergen Pfingstner

function [R0_y U0_y s0_y V0_y] = load_R_y_qp(use_main_linac, use_bds, nr_corr_ml, use_bpm_scaling)

if( (use_main_linac == 1) & (use_bds == 0) )
	
  if(use_bpm_scaling == 1)
    R0_y = load('-ascii', '../scripts/R0_qp_y_scale.dat');
    R0_y = R0_y(1:nr_corr_ml,1:nr_corr_ml);
    U0_y = load('-ascii', '../scripts/U0_qp_ml_y_scale.dat');
    s0_y = load('-ascii', '../scripts/s0_qp_ml_y_scale.dat');
    V0_y = load('-ascii', '../scripts/V0_qp_ml_y_scale.dat');
  else
    R0_y = load('-ascii', '../scripts/R0_qp_y.dat');
    R0_y = R0_y(1:nr_corr_ml,1:nr_corr_ml);
    U0_y = load('-ascii', '../scripts/U0_qp_ml_y.dat');
    s0_y = load('-ascii', '../scripts/s0_qp_ml_y.dat');
    V0_y = load('-ascii', '../scripts/V0_qp_ml_y.dat');
  end
  
elseif( (use_main_linac == 0) & (use_bds == 1) )
  
  if(use_bpm_scaling == 1)
    R0_y = load('-ascii', '../scripts/R0_qp_y_scale.dat');
    R0_y = R0_y((nr_corr_ml+1):end, (nr_corr_ml+1):end);
    U0_y = load('-ascii', '../scripts/U0_qp_bds_y_scale.dat');
    s0_y = load('-ascii', '../scripts/s0_qp_bds_y_scale.dat');
    V0_y = load('-ascii', '../scripts/V0_qp_bds_y_scale.dat');
  else
    R0_y = load('-ascii', '../scripts/R0_qp_y.dat');
    R0_y = R0_y((nr_corr_ml+1):end, (nr_corr_ml+1):end);
    U0_y = load('-ascii', '../scripts/U0_qp_bds_y.dat');
    s0_y = load('-ascii', '../scripts/s0_qp_bds_y.dat');
    V0_y = load('-ascii', '../scripts/V0_qp_bds_y.dat');
  end

elseif( (use_main_linac == 1) & (use_bds == 1) )
  
  if(use_bpm_scaling == 1)
    R0_y = load('-ascii', '../scripts/R0_qp_y_scale.dat');
    U0_y = load('-ascii', '../scripts/U0_qp_y_scale.dat');
    s0_y = load('-ascii', '../scripts/s0_qp_y_scale.dat');
    V0_y = load('-ascii', '../scripts/V0_qp_y_scale.dat');
  else
    R0_y = load('-ascii', '../scripts/R0_qp_y.dat');
    U0_y = load('-ascii', '../scripts/U0_qp_y.dat');
    s0_y = load('-ascii', '../scripts/s0_qp_y.dat');
    V0_y = load('-ascii', '../scripts/V0_qp_y.dat');
  end
  
end


