namespace eval dynamic_effects {
    variable dec_cav_number
    variable cav_g0
    variable cav_ph0
    set dec_cav_number ""
    foreach c {
	2976
	2976
	2972
	2974
	2976
	2974
	2974
	2972
	2974
	2974
	2976
	2976
	2976
	2976
	2976
	2976
	2976
	2976
	2976
	2976
	2976
	2976
	2976
	2976
    } {
	lappend dec_cav_number [expr $c/2]
    }
    
    Random g_imperfections -type gaussian

    variable qs0
#    set qs0 [QuadropoleGetStrengthList]

    proc find_slope {} {
	variable dec_cav_number
	variable cav_g0
	variable cav_ph0
	set k 0
	set pi [expr acos(-1.0)]
	foreach nc $dec_cav_number {
	    set sc1 0.0
	    set sc2 0.0
	    for {set i 0} {$i<$nc} {incr i} {
		set sc1 [expr $sc1+[lindex $cav_g0 $k]*cos(([lindex $cav_ph0 $k]+0.1)/180.0*$pi)]
		set sc2 [expr $sc2+[lindex $cav_g0 $k]*cos(([lindex $cav_ph0 $k]-0.1)/180.0*$pi)]
		incr k
	    }
	    puts [expr ($sc1-$sc2)/(0.5*($sc1+$sc2))/0.2]
	}
    }

    proc cav_number {n} {
	variable dec_cav_number
	return [lindex $dec_cav_number $n]
    }

    proc set_nominal_cavities {} {
	variable cav_g0
	variable cav_ph0
	set cav_g0 [CavityGetGradientList]
	set cav_ph0 [CavityGetPhaseList]
    }

    proc rf_jitter {s_ph0_const s_ph0 s_ph s_g0_const s_g0 s_g} {
	variable dec_cav_number
	variable cav_g0
	variable cav_ph0
	
	set gl ""
	set phl ""
	set dg0 [expr [g_imperfections]*$s_g0 + $s_g0_const]
	set dph0 [expr [g_imperfections]*$s_ph0 + $s_ph0_const]
	set j 0
	foreach cn $dec_cav_number {
	    set dg [expr $dg0+[g_imperfections]*$s_g]
	    set dph [expr $dph0+[g_imperfections]*$s_ph]
	    for {set i 0} {$i<$cn} {incr i} {
		lappend gl [expr [lindex $cav_g0 $j]+$dg]
		lappend phl [expr [lindex $cav_ph0 $j]+$dph]
		incr j
	    }
	}

	CavitySetGradientList $gl
	CavitySetPhaseList $phl
	return "$dph0 $dg0"
    }

    proc rf_error {s_ph0 s_g0} {
	variable dec_cav_number
	variable cav_g0
	variable cav_ph0
	set gl ""
	set phl ""
	set dg0 [expr $s_g0]
	set dph0 [expr $s_ph0]
	set j 0
	foreach cn $dec_cav_number {
	    set dg [expr $dg0]
	    set dph [expr $dph0]
	    for {set i 0} {$i<$cn} {incr i} {
		lappend gl [expr [lindex $cav_g0 $j]+$dg]
		lappend phl [expr [lindex $cav_ph0 $j]+$dph]
		incr j
	    }
	}
	CavitySetGradientList $gl
	CavitySetPhaseList $phl
    }

    proc rf_error_sector {is s_ph0 s_g0} {
	variable dec_cav_number
	variable cav_g0
	variable cav_ph0
	set gl ""
	set phl ""
	set dg0 [expr $s_g0]
	set dph0 [expr $s_ph0]
	set j 0
	set dg [expr $dg0]
	set dph [expr $dph0]
	set ii 0
	foreach cn $dec_cav_number {
	    if {$ii==$is} {
		for {set i 0} {$i<$cn} {incr i} {
		    lappend gl [expr [lindex $cav_g0 $j]+$dg]
		    lappend phl [expr [lindex $cav_ph0 $j]+$dph]
		    incr j
		}
	    } {
		for {set i 0} {$i<$cn} {incr i} {
		    lappend gl [lindex $cav_g0 $j]
		    lappend phl [lindex $cav_ph0 $j]
		    incr j
		}
	    }
	    incr ii
	}
	CavitySetGradientList $gl
	CavitySetPhaseList $phl
    }

    proc quad_field_jitter {s0 s} {
	variable qs0
	set qs ""
	set dq [expr 1.0+$s0*[g_imperfections]]
	foreach q $qs0 {
	    lappend qs [expr $dq*(1.0+$s*[g_imperfections])*$q]
	}
	QuadrupoleSetStrengthList $qs
    }
}

namespace eval emittance {
    proc emitt {name} {
	set w_sum 0.0
	set y_sum 0.0
	set yp_sum 0.0
	set x_sum 0.0
	set xp_sum 0.0
	set y_sum2 0.0
	set yyp_sum2 0.0
	set yp_sum2 0.0
	set x_sum2 0.0
	set xxp_sum2 0.0
	set xp_sum2 0.0
	set e_sum 0.0
	set file [open $name r]
	gets $file line
	while {![eof $file]} {
	    if {$line!=""} {
		set wgt [lindex $line 1]
		set x [lindex $line 3]
		set xp [lindex $line 4]
		set y [lindex $line 5]
		set yp [lindex $line 6]
		set w_sum [expr $w_sum+$wgt]
		set e_sum [expr $e_sum+$wgt*[lindex $line 2]]
		set x_sum [expr $x_sum+$wgt*$x]
		set xp_sum [expr $xp_sum+$wgt*$xp]
		set y_sum [expr $y_sum+$wgt*$y]
		set yp_sum [expr $yp_sum+$wgt*$yp]
		set x_sum2 [expr $x_sum2+$wgt*($x*$x+[lindex $line 7])]
		set xxp_sum2 [expr $xxp_sum2+$wgt*($x*$xp+[lindex $line 8])]
		set xp_sum2 [expr $xp_sum2+$wgt*($xp*$xp+[lindex $line 9])]
		set y_sum2 [expr $y_sum2+$wgt*($y*$y+[lindex $line 10])]
		set yyp_sum2 [expr $yyp_sum2+$wgt*($yp*$y+[lindex $line 11])]
		set yp_sum2 [expr $yp_sum2+$wgt*($yp*$yp+[lindex $line 12])]
		}
	    gets $file line
	}
	close $file
	set e_sum [expr $e_sum/$w_sum]
	set y_sum [expr $y_sum/$w_sum]
	set yp_sum [expr $yp_sum/$w_sum]
	set x_sum [expr $x_sum/$w_sum]
	set xp_sum [expr $xp_sum/$w_sum]
	set y_sum2 [expr $y_sum2/$w_sum-$y_sum*$y_sum]
	set yyp_sum2 [expr $yyp_sum2/$w_sum-$y_sum*$yp_sum]
	set yp_sum2 [expr $yp_sum2/$w_sum-$yp_sum*$yp_sum]
	set x_sum2 [expr $x_sum2/$w_sum-$x_sum*$x_sum]
	set xp_sum2 [expr $xp_sum2/$w_sum-$xp_sum*$xp_sum]
	set xxp_sum2 [expr $xxp_sum2/$w_sum-$x_sum*$xp_sum]
	set emitt_x [expr sqrt(($x_sum2*$xp_sum2-$xxp_sum2*$xxp_sum2))*$e_sum/0.511e-3*1e-3]
	set emitt_y [expr sqrt(($y_sum2*$yp_sum2-$yyp_sum2*$yyp_sum2))*$e_sum/0.511e-3*1e-3]
	return "$emitt_x $emitt_y"
    }

    proc sum_emitt {name n} {
	set w_sum 0.0
	set y_sum 0.0
	set yp_sum 0.0
	set x_sum 0.0
	set xp_sum 0.0
	set y_sum2 0.0
	set yyp_sum2 0.0
	set yp_sum2 0.0
	set x_sum2 0.0
	set xxp_sum2 0.0
	set xp_sum2 0.0
	set e_sum 0.0
	for {set i 0} {$i<$n} {incr i} {
	set file [open $name.$i r]
	    gets $file line
	    while {![eof $file]} {
		if {$line!=""} {
		    set wgt [lindex $line 1]
		    set x [lindex $line 3]
		    set xp [lindex $line 4]
		    set y [lindex $line 5]
		    set yp [lindex $line 6]
		    set w_sum [expr $w_sum+$wgt]
		    set e_sum [expr $e_sum+$wgt*[lindex $line 2]]
		    set x_sum [expr $x_sum+$wgt*$x]
		    set xp_sum [expr $xp_sum+$wgt*$xp]
		    set y_sum [expr $y_sum+$wgt*$y]
		    set yp_sum [expr $yp_sum+$wgt*$yp]
		    set x_sum2 [expr $x_sum2+$wgt*($x*$x+[lindex $line 7])]
		    set xxp_sum2 [expr $xxp_sum2+$wgt*($x*$xp+[lindex $line 8])]
		    set xp_sum2 [expr $xp_sum2+$wgt*($xp*$xp+[lindex $line 9])]
		    set y_sum2 [expr $y_sum2+$wgt*($y*$y+[lindex $line 10])]
		    set yyp_sum2 [expr $yyp_sum2+$wgt*($yp*$y+[lindex $line 11])]
		    set yp_sum2 [expr $yp_sum2+$wgt*($yp*$yp+[lindex $line 12])]
		}
		gets $file line
	    }
	    close $file
	}
	set e_sum [expr $e_sum/$w_sum]
	set y_sum [expr $y_sum/$w_sum]
	set yp_sum [expr $yp_sum/$w_sum]
	set x_sum [expr $x_sum/$w_sum]
	set xp_sum [expr $xp_sum/$w_sum]
	set y_sum2 [expr $y_sum2/$w_sum-$y_sum*$y_sum]
	set yyp_sum2 [expr $yyp_sum2/$w_sum-$y_sum*$yp_sum]
	set yp_sum2 [expr $yp_sum2/$w_sum-$yp_sum*$yp_sum]
	set x_sum2 [expr $x_sum2/$w_sum-$x_sum*$x_sum]
	set xp_sum2 [expr $xp_sum2/$w_sum-$xp_sum*$xp_sum]
	set xxp_sum2 [expr $xxp_sum2/$w_sum-$x_sum*$xp_sum]
	set emitt_x [expr sqrt(($x_sum2*$xp_sum2-$xxp_sum2*$xxp_sum2))*$e_sum/0.511e-3*1e-3]
	set emitt_y [expr sqrt(($y_sum2*$yp_sum2-$yyp_sum2*$yyp_sum2))*$e_sum/0.511e-3*1e-3]
	return "$emitt_x $emitt_y"
    }

    proc sum_emitt_2 {name1 name2 n} {
	set w_sum 0.0
	set y_sum 0.0
	set yp_sum 0.0
	set x_sum 0.0
	set xp_sum 0.0
	set y_sum2 0.0
	set yyp_sum2 0.0
	set yp_sum2 0.0
	set x_sum2 0.0
	set xxp_sum2 0.0
	set xp_sum2 0.0
	set e_sum 0.0
	for {set i 0} {$i<$n} {incr i} {
	set file [open $name1.$i.$name2 r]
	    gets $file line
	    while {![eof $file]} {
		if {$line!=""} {
		    set wgt [lindex $line 1]
		    set x [lindex $line 3]
		    set xp [lindex $line 4]
		    set y [lindex $line 5]
		    set yp [lindex $line 6]
		    set w_sum [expr $w_sum+$wgt]
		    set e_sum [expr $e_sum+$wgt*[lindex $line 2]]
		    set x_sum [expr $x_sum+$wgt*$x]
		    set xp_sum [expr $xp_sum+$wgt*$xp]
		    set y_sum [expr $y_sum+$wgt*$y]
		    set yp_sum [expr $yp_sum+$wgt*$yp]
		    set x_sum2 [expr $x_sum2+$wgt*($x*$x+[lindex $line 7])]
		    set xxp_sum2 [expr $xxp_sum2+$wgt*($x*$xp+[lindex $line 8])]
		    set xp_sum2 [expr $xp_sum2+$wgt*($xp*$xp+[lindex $line 9])]
		    set y_sum2 [expr $y_sum2+$wgt*($y*$y+[lindex $line 10])]
		    set yyp_sum2 [expr $yyp_sum2+$wgt*($yp*$y+[lindex $line 11])]
		    set yp_sum2 [expr $yp_sum2+$wgt*($yp*$yp+[lindex $line 12])]
		}
		gets $file line
	    }
	    close $file
	}
	set e_sum [expr $e_sum/$w_sum]
	set y_sum [expr $y_sum/$w_sum]
	set yp_sum [expr $yp_sum/$w_sum]
	set x_sum [expr $x_sum/$w_sum]
	set xp_sum [expr $xp_sum/$w_sum]
	set y_sum2 [expr $y_sum2/$w_sum-$y_sum*$y_sum]
	set yyp_sum2 [expr $yyp_sum2/$w_sum-$y_sum*$yp_sum]
	set yp_sum2 [expr $yp_sum2/$w_sum-$yp_sum*$yp_sum]
	set x_sum2 [expr $x_sum2/$w_sum-$x_sum*$x_sum]
	set xp_sum2 [expr $xp_sum2/$w_sum-$xp_sum*$xp_sum]
	set xxp_sum2 [expr $xxp_sum2/$w_sum-$x_sum*$xp_sum]
	set emitt_x [expr sqrt(($x_sum2*$xp_sum2-$xxp_sum2*$xxp_sum2))*$e_sum/0.511e-3*1e-3]
	set emitt_y [expr sqrt(($y_sum2*$yp_sum2-$yyp_sum2*$yyp_sum2))*$e_sum/0.511e-3*1e-3]
	return "$emitt_x $emitt_y"
    }
    
    proc sum_emitt_scale {name n scale} {
	set w_sum 0.0
	set y_sum 0.0
	set yp_sum 0.0
	set x_sum 0.0
	set xp_sum 0.0
	set y_sum2 0.0
	set yyp_sum2 0.0
	set yp_sum2 0.0
	set x_sum2 0.0
	set xxp_sum2 0.0
	set xp_sum2 0.0
	set e_sum 0.0
	for {set i 0} {$i<$n} {incr i} {
	    set file [open $name.$i r]
	    gets $file line
	    while {![eof $file]} {
		if {$line!=""} {
		    set wgt [lindex $line 1]
		    set x [expr $scale*[lindex $line 3]]
		    set xp [expr $scale*[lindex $line 4]]
		    set y [expr $scale*[lindex $line 5]]
		    set yp [expr $scale*[lindex $line 6]]
		    set w_sum [expr $w_sum+$wgt]
		    set e_sum [expr $e_sum+$wgt*[lindex $line 2]]
		    set x_sum [expr $x_sum+$wgt*$x]
		    set xp_sum [expr $xp_sum+$wgt*$xp]
		    set y_sum [expr $y_sum+$wgt*$y]
		    set yp_sum [expr $yp_sum+$wgt*$yp]
		    set x_sum2 [expr $x_sum2+$wgt*($x*$x+[lindex $line 7])]
		    set xxp_sum2 [expr $xxp_sum2+$wgt*($x*$xp+[lindex $line 8])]
		    set xp_sum2 [expr $xp_sum2+$wgt*($xp*$xp+[lindex $line 9])]
		    set y_sum2 [expr $y_sum2+$wgt*($y*$y+[lindex $line 10])]
		    set yyp_sum2 [expr $yyp_sum2+$wgt*($yp*$y+[lindex $line 11])]
		    set yp_sum2 [expr $yp_sum2+$wgt*($yp*$yp+[lindex $line 12])]
		}
		gets $file line
	    }
	    close $file
	}
	set e_sum [expr $e_sum/$w_sum]
	set y_sum [expr $y_sum/$w_sum]
	set yp_sum [expr $yp_sum/$w_sum]
	set x_sum [expr $x_sum/$w_sum]
	set xp_sum [expr $xp_sum/$w_sum]
	set y_sum2 [expr $y_sum2/$w_sum-$y_sum*$y_sum]
	set yyp_sum2 [expr $yyp_sum2/$w_sum-$y_sum*$yp_sum]
	set yp_sum2 [expr $yp_sum2/$w_sum-$yp_sum*$yp_sum]
	set x_sum2 [expr $x_sum2/$w_sum-$x_sum*$x_sum]
	set xp_sum2 [expr $xp_sum2/$w_sum-$xp_sum*$xp_sum]
	set xxp_sum2 [expr $xxp_sum2/$w_sum-$x_sum*$xp_sum]
	set emitt_x [expr sqrt(($x_sum2*$xp_sum2-$xxp_sum2*$xxp_sum2))*$e_sum/0.511e-3*1e-3]
	set emitt_y [expr sqrt(($y_sum2*$yp_sum2-$yyp_sum2*$yyp_sum2))*$e_sum/0.511e-3*1e-3]
	return "$emitt_x $emitt_y"
    }
    
    proc sum_emitt_axis_scale {name n scale} {
	set w_sum 0.0
	set y_sum 0.0
	set yp_sum 0.0
	set x_sum 0.0
	set xp_sum 0.0
	set y_sum2 0.0
	set yyp_sum2 0.0
	set yp_sum2 0.0
	set x_sum2 0.0
	set xxp_sum2 0.0
	set xp_sum2 0.0
	set e_sum 0.0
	set i $n
	set file [open $name.$i r]
	gets $file line
	while {![eof $file]} {
	    if {$line!=""} {
		set wgt [lindex $line 1]
		set x [expr $scale*[lindex $line 3]]
		set xp [expr $scale*[lindex $line 4]]
		set y [expr $scale*[lindex $line 5]]
		set yp [expr $scale*[lindex $line 6]]
		set w_sum [expr $w_sum+$wgt]
		set e_sum [expr $e_sum+$wgt*[lindex $line 2]]
		set x_sum [expr $x_sum+$wgt*$x]
		set xp_sum [expr $xp_sum+$wgt*$xp]
		set y_sum [expr $y_sum+$wgt*$y]
		set yp_sum [expr $yp_sum+$wgt*$yp]
		set x_sum2 [expr $x_sum2+$wgt*($x*$x+[lindex $line 7])]
		set xxp_sum2 [expr $xxp_sum2+$wgt*($x*$xp+[lindex $line 8])]
		set xp_sum2 [expr $xp_sum2+$wgt*($xp*$xp+[lindex $line 9])]
		set y_sum2 [expr $y_sum2+$wgt*($y*$y+[lindex $line 10])]
		set yyp_sum2 [expr $yyp_sum2+$wgt*($yp*$y+[lindex $line 11])]
		set yp_sum2 [expr $yp_sum2+$wgt*($yp*$yp+[lindex $line 12])]
	    }
	    gets $file line
	}
	close $file
	
	set e_sum [expr $e_sum/$w_sum]
	set y_sum 0.0
	set yp_sum 0.0
	set x_sum 0.0
	set xp_sum 0.0
	set y_sum2 [expr $y_sum2/$w_sum-$y_sum*$y_sum]
	set yyp_sum2 [expr $yyp_sum2/$w_sum-$y_sum*$yp_sum]
	set yp_sum2 [expr $yp_sum2/$w_sum-$yp_sum*$yp_sum]
	set x_sum2 [expr $x_sum2/$w_sum-$x_sum*$x_sum]
	set xp_sum2 [expr $xp_sum2/$w_sum-$xp_sum*$xp_sum]
	set xxp_sum2 [expr $xxp_sum2/$w_sum-$x_sum*$xp_sum]
	set emitt_x [expr sqrt(($x_sum2*$xp_sum2-$xxp_sum2*$xxp_sum2))*$e_sum/0.511e-3*1e-3]
	set emitt_y [expr sqrt(($y_sum2*$yp_sum2-$yyp_sum2*$yyp_sum2))*$e_sum/0.511e-3*1e-3]
	return "$emitt_x $emitt_y"
    }
    
    proc emitt_scale_y {name n deps0} {
	set eps0 [lindex [sum_emitt_scale $name $n 0.0] 1]
	set x 1.0
	for {set i 0} {$i<3} {incr i} {
	    set eps [lindex [sum_emitt_scale $name $n $x] 1]
	    set deps [expr $eps-$eps0]
	    set x [expr $x*sqrt($deps0/$deps)]
	}
	return $x
    }
    
    proc emitt_axis_scale_y {name n deps0} {
	set eps0 [lindex [sum_emitt_axis_scale $name $n 0.0] 1]
	puts -nonewline $eps0
	set x 1.0
	for {set i 0} {$i<5} {incr i} {
	    set eps [lindex [sum_emitt_axis_scale $name $n $x] 1]
	    puts -nonewline " $eps"
	    set deps [expr $eps-$eps0]
	    set x [expr $x*sqrt($deps0/$deps)]
	}
	puts ""
	return $x
    }

}
