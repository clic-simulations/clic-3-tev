% Script to initialize all variables and structures, as well as the
% accelerator before a new run over the specified time steps
%
% Jochem Snuverink and Juergen Pfingstner

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Create file names for measurement stations 1,2 and 3 %
% Also delete old content.                             %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if(use_main_linac == 1)
  meas_station1_file_name = sprintf('meas_station_1%s_machine_%d.dat', save_string, machine_index);
  f_temp = fopen(meas_station1_file_name, 'wt');
  fclose(f_temp);
end

if(use_bds == 1)
  meas_station2_file_name = sprintf('meas_station_2%s_machine_%d.dat', save_string, machine_index);
  f_temp = fopen(meas_station2_file_name, 'wt');
  fclose(f_temp);
end

if(use_beam_beam == 1)
  meas_station3_file_name = sprintf('meas_station_3%s_machine_%d.dat', save_string, machine_index);
  f_temp = fopen(meas_station3_file_name, 'wt');
  fclose(f_temp);
end

if(use_bpm_storing == 1)
  bpm_meas_x_elec_file_name = sprintf('bpm_meas_x_elec_machine_%d.dat', machine_index);
  f_temp = fopen(bpm_meas_x_elec_file_name, 'wt');
  fclose(f_temp);
  
  bpm_meas_x_posi_file_name = sprintf('bpm_meas_x_posi_machine_%d.dat', machine_index);
  f_temp = fopen(bpm_meas_x_posi_file_name, 'wt');
  fclose(f_temp);
  
  bpm_meas_y_elec_file_name = sprintf('bpm_meas_y_elec_machine_%d.dat', machine_index);
  f_temp = fopen(bpm_meas_y_elec_file_name, 'wt');
  fclose(f_temp);
  
  bpm_meas_y_posi_file_name = sprintf('bpm_meas_y_posi_machine_%d.dat', machine_index);
  f_temp = fopen(bpm_meas_y_posi_file_name, 'wt');
  fclose(f_temp);
end

if(use_gm_storing == 1)
  gm_x_elec_file_name = sprintf('gm_x_elec_machine_%d.dat', machine_index);
  f_temp = fopen(gm_x_elec_file_name, 'wt');
  fclose(f_temp);
  
  gm_y_elec_file_name = sprintf('gm_y_elec_machine_%d.dat', machine_index);
  f_temp = fopen(gm_y_elec_file_name, 'wt');
  fclose(f_temp);
end

if(use_ip_corr_storing == 1)
  ip_corr_elec_file_name = sprintf("ip_corr_elec_machine_%d.dat", machine_index);
  f_temp = fopen(ip_corr_elec_file_name, 'wt');
  fclose(f_temp);
  
  ip_corr_posi_file_name = sprintf("ip_corr_posi_machine_%d.dat", machine_index);
  f_temp = fopen(ip_corr_posi_file_name, 'wt');
  fclose(f_temp);
end

if(use_ml_corr_storing == 1)
  ml_corr_elec_file_name = sprintf("ml_corr_elec_machine_%d.dat", machine_index);
  f_temp = fopen(ml_corr_elec_file_name, 'wt');
  fclose(f_temp);
  
  ml_corr_posi_file_name = sprintf("ml_corr_posi_machine_%d.dat", machine_index);
  f_temp = fopen(ml_corr_posi_file_name, 'wt');
  fclose(f_temp);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Create file names for the system identification %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


if(identification_switch == 1)
  r_file = sprintf('r%s_%d', save_string, machine_index);
  r_hat_file = sprintf('r_hat%s_%d', save_string, machine_index);
  covar_file =  sprintf('covar%s_%d', save_string, machine_index);
  result_file = sprintf('ident%s_%d', save_string, machine_index);
  column_file = sprintf('col_error%s_%d', save_string, machine_index);
  row_file = sprintf('row_error%s_%d', save_string, machine_index);
  scale_qp_file = sprintf('scale_qp%s_%d', save_string, machine_index);
  scale_bpm_file = sprintf('scale_bpm%s_%d', save_string, machine_index);

  f_temp = fopen(r_file, 'wt');
  fclose(f_temp);
  f_temp = fopen(r_hat_file, 'wt');
  fclose(f_temp);
  f_temp = fopen(covar_file, 'wt');
  fclose(f_temp);
  f_temp = fopen(result_file, 'wt');
  fclose(f_temp);
  f_temp = fopen(column_file, 'wt');
  fclose(f_temp);
  f_temp = fopen(row_file, 'wt');
  fclose(f_temp);
  f_temp = fopen(scale_qp_file, 'wt');
  fclose(f_temp);
  f_temp = fopen(scale_bpm_file, 'wt');
  fclose(f_temp);

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Reset the ground motion %
%%%%%%%%%%%%%%%%%%%%%%%%%%%

if ((ground_motion_x == 1 ) | (ground_motion_y == 1))
  Tcl_SetVar("machine_index", machine_index);
  Tcl_Eval("ground_motion_init electron $groundmotion(type) $groundmotion(model) [expr ($machine_index)*7 + $groundmotion(seed_nr)] 1");
  Tcl_Eval("ground_motion_init positron $groundmotion(type) $groundmotion(model) [expr ($machine_index)*7 + $groundmotion(seed_nr)] -1");

  % apply first ground motion (no moving as first motion is zero)
  if(groundmotion_type == 2)
      Tcl_Eval("ground_motion electron $groundmotion(type)");
      Tcl_Eval("ground_motion positron $groundmotion(type)");
  end
else
  fprintf(1,'No ground motion will be applied\n');
end

%%%%%%%%%%%%%%%%%%%%%%%
% Reset the beamlines %
%%%%%%%%%%%%%%%%%%%%%%%

Tcl_Eval("BeamlineUse -name electron");
Tcl_Eval("Zero");
Tcl_Eval("BeamlineUse -name positron");
Tcl_Eval("Zero");

%%%%%%%%%%%%%%%%%%%%
% Reset parameters %
%%%%%%%%%%%%%%%%%%%%

angley_volt_ipdipole_prev = 0.0;
volt_ipdipole_y_prev = 0.0;
volt_ipdipole_y = 0.0;
y_QD0_elec_prev=0;
y_QD0_elec=0;
y_QD0_posi_prev=0;
y_QD0_posi=0;
IPdipole_strength_positron_y=0;
IPdipole_strength_positron_x=0;
IPdipole_strength_electron_y=0;
IPdipole_strength_electron_x=0;
QF1_offset_elec_prev=0;
QD0_offset_elec_prev=0;
QF1_offset_posi_prev=0;
QD0_offset_posi_prev=0;
offset_elec_BBFB_IP_y_prev=0;
offset_posi_BBFB_IP_y_prev=0;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Reset the noise generators %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if(stabilization_noise_x == 1)
  corr_noise_x = zeros(length(corr_index),2);
  for i=electron:positron
    [stabilization_noise_data_x(:,i)] = reset_noise_data(stabilization_noise_data_x(:,i), stabilization_noise_type);
  end
end

if(stabilization_noise_y == 1)
  corr_noise_y = zeros(length(corr_index),2);
  for i=electron:positron
    [stabilization_noise_data_y(:,i)] = reset_noise_data(stabilization_noise_data_y(:,i), stabilization_noise_type);
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Reset controller parameters %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if(use_controller_x == 1)
  % spatial part
  contr_error_x = zeros(length(bpm_index), 2);
  spatial_filter_output_x = zeros(length(corr_index), 2);

  for j=electron:positron
    % frequency part
    if(controller_type_frequency_x == 1)
      for i=1:length(corr_index)
        contr_filter_x_data(i,j).output = 0;
        contr_filter_x_data(i,j).int_out_old = 0;
      end
    end
    
    if(controller_type_frequency_x == 2)
      for i=1:length(corr_index)
        contr_filter_x_data(i,j).output = 0;
        contr_filter_x_data(i,j).int_out_old = 0;
        contr_filter_x_data(i,j).low_out_old = 0;
      end
    end
    
    if(controller_type_frequency_x == 3)
      for i=1:length(corr_index)
        contr_filter_x_data(i,j).output = 0;
        
        if(orbit_filter_algo_x == 1)
          contr_filter_x_data(i,j).int_out_old = 0;
          contr_filter_x_data(i,j).low_out_old = 0;
          contr_filter_x_data(i,j).lead_out_old = 0;
          contr_filter_x_data(i,j).lead_in_old = 0;
          
          %contr_filter_x_data(i,j).peak_out_old = 0;
          %contr_filter_x_data(i,j).peak_in_old = 0;
          %contr_filter_x_data(i,j).peak_out_very_old = 0;
          %contr_filter_x_data(i,j).peak_in_very_old = 0;
        else
          contr_filter_x_data(i,j).in = zeros(5,1);
          contr_filter_x_data(i,j).out = zeros(5,1);
        end
      end
    end
  end
end

if(use_controller_y == 1)
  for j=electron:positron
    
    % spatial part
    contr_error_y = zeros(length(bpm_index), 2);
    spatial_filter_output_y = zeros(length(corr_index), 2);

    % frequency part
    if(controller_type_frequency_y == 1)
      for i=1:length(corr_index)
        contr_filter_y_data(i,j).output = 0;
        contr_filter_y_data(i,j).int_out_old = 0;
      end
    end
    
    if(controller_type_frequency_y == 2)
      for i=1:length(corr_index)
        contr_filter_y_data(i,j).output = 0;
        contr_filter_y_data(i,j).int_out_old = 0;
        contr_filter_y_data(i,j).low_out_old = 0;
      end
    end
    
    if(controller_type_frequency_y == 3)
      for i=1:length(corr_index)
        contr_filter_y_data(i,j).output = 0;
        
        if(orbit_filter_algo_y == 1)
          contr_filter_y_data(i,j).int_out_old = 0;
          contr_filter_y_data(i,j).low_out_old = 0;
          contr_filter_y_data(i,j).lead_out_old = 0;
          contr_filter_y_data(i,j).lead_in_old = 0;
          %contr_filter_y_data(i,j).peak_out_old = 0;
          %contr_filter_y_data(i,j).peak_in_old = 0;
          %contr_filter_y_data(i,j).peak_out_very_old = 0;
          %contr_filter_y_data(i,j).peak_in_very_old = 0;
        else
          contr_filter_y_data(i,j).in = zeros(5,1);
          contr_filter_y_data(i,j).out = zeros(5,1);
        end
      end
    end
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Reset the old corrector values %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if (use_controller_x | use_controller_y)

  corr_settings_old_x = zeros(length(corr_index), 2);
  corr_settings_new_x = zeros(length(corr_index), 2);
  corr_settings_old_y = zeros(length(corr_index), 2);
  corr_settings_new_y = zeros(length(corr_index), 2);

  corr_values_old_x = zeros(length(corr_index), 2);
  corr_values_new_x = zeros(length(corr_index), 2);
  corr_values_old_y = zeros(length(corr_index), 2);
  corr_values_new_y = zeros(length(corr_index), 2);

  corr_increment_x = zeros(length(corr_index), 2);
  corr_increment_y = zeros(length(corr_index), 2);

end

tic
toc2 = 0;
toc1 = 0;