namespace eval Feedback {

    proc define_bpm {nfeed} {
	set ql [Linac::quadrupole_match_list]
	set nquad [llength $ql]
	set nl 8
	set qc_all ""
	for {set i 0} {$i<$nfeed+1} {incr i} {
	    puts $i
	    set j [expr int($i.0*$nquad.0/$nfeed.0-2*$nl)]
	    while {([lindex $ql $j]==1)||($j<0)} {incr j}
	    if {$j%2==0} {
		incr j
	    }
	    if {$j>=$nquad} {exit}
	    set qc ""
	    for {set k 0} {$k<$nl} {incr k} {
		lappend qc [expr $j]
		incr j 2
	    }
	    lappend qc_all $qc
	}
	return $qc_all
    }

    proc define_corr {nfeed} {
	set ql [Linac::quadrupole_match_list]
	set nquad [llength $ql]
	set nl 2
	set qc_all ""
	for {set i 0} {$i<$nfeed} {incr i} {
	    puts $i
	    set j [expr int(($i.0+0.5)*$nquad.0/$nfeed.0-2*$nl)]
	    while {([lindex $ql $j]==1)||($j<0)} {incr j}
#
	    # changed from if {$j%2==0}
#
	    if {$j%2!=0} {
		incr j
	    }
	    if {$j>=$nquad} {exit}
	    set qc ""
	    for {set k 0} {$k<$nl} {incr k} {
		lappend qc $j
		incr j 2
	    }
	    lappend qc_all $qc
	}
	return $qc_all
    }

    proc measure_response {l_corr l_bpm} {
	set f [open response.tmp w]
	foreach q $l_corr {
	    Zero
	    ElementSetToOffset $q -y 1.0
	    TestNoCorrection -beam beam0 -emitt_file emitt.dynamic -machines 1 \
		-survey None
	    set tmp [BpmReadings]
	    set gg ""
	    foreach tt $l_bpm {
		lappend gg [lindex [lindex $tmp $tt] 2]
	    }
	    puts $gg
	    puts $f $gg
	    flush $f
	}
	close $f
    }

    proc correction_matrix {l_corr l_bpm} {
	measure_response $l_corr $l_bpm
	set f [open response.tmp r]
	set response ""
	gets $f line
	while {![eof $f]} {
	    lappend response $line
	    gets $f line
	}
	close $f
	MatrixCorrectionInit correction -res $response
    }
    
    proc prepare_feedback {nf} {
	global ql bl l_bpm l_corr l_quad
	
	set l_bpm [Feedback::define_bpm $nf]
	set l_corr [Feedback::define_corr $nf]
	
	set l_quad ""
	foreach q $l_corr {
	    set l_quad [concat $l_quad $q]
	}
	set l_corr ""
	foreach q $l_quad {
	    lappend l_corr [lindex $ql $q]
	}
	set l_tmp ""
	foreach q $l_bpm {
	    set l_tmp [concat $l_tmp $q]
	}
	set l_bpm $l_tmp
	correction_matrix $l_corr $l_bpm
    }
    
    namespace export define_bpm
    namespace export define_corr
#    namespace export measure_response
    namespace export prepare_feedback
}
