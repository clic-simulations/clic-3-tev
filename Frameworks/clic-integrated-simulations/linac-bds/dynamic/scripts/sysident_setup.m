% Implementation of the setup of the system identification which
% means basicly the bump setup


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Take care! Before the code can be used, also the second linac has %
% to be implemented -> to be implemented                            %
% variable have to be dublicated and names have to be changed       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if(identification_switch == 1)
  
  cycle_index = mod(time_step, nr_bumps);
  if(cycle_index == 0)
    cycle_index = nr_bumps;
  end
	  
  % Create excitation for e-
  source "../scripts/set_bump.m"
  
  % Create excitation for the e+
  % to be implemented
  
end
