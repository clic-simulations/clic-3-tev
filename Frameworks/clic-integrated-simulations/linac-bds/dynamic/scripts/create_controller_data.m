% Script to create the data needed to run the controller
%
% Juergen Pfingstner

%%%%%%%%%%%%%%%%%%
% Orbit feedback %
%%%%%%%%%%%%%%%%%%

if(use_controller_x == 1)
  
  % Init dispersive orbit

  if(strcmp(use_set_value, 'dispersion_init'))
    golden_orbit_x_elec = load('../scripts/golden_orbit_x_elec.dat');
    golden_orbit_x(:,:,electron) = golden_orbit_x_elec';
    golden_orbit_x_posi = load('../scripts/golden_orbit_x_posi.dat');
    golden_orbit_x(:,:,positron) = golden_orbit_x_posi';
    clear golden_orbit_x_elec golden_orbit_x_posi;
  end
  
  % Init the dispersion suppresion functionality

  if(use_disp_suppression_x == 1)
    disp_pattern_x_posi = load('../scripts/disp_x_posi.dat');
    disp_pattern_x(:,:,positron) = disp_pattern_x_posi';
    disp_pattern_x_elec = load('../scripts/disp_x_elec.dat');
    disp_pattern_x(:,:,electron) = disp_pattern_x_elec';
    clear disp_pattern_x_posi disp_pattern_x_elec;
  end
  
  % Init BPM scaling
  
  if(use_bpm_scaling == 1)
    bpm_scaling_factor_x = load('-ascii', 'bpm_scaling_factor_x.dat');
  end
  
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % Spatial part of the controller %
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  contr_error_x = zeros(length(bpm_index), 2);
  spatial_filter_output_x = zeros(length(corr_index), 2);
  
  [row_U_x col_U_x] = size(U0_x);
  [row_V_x col_V_x] = size(V0_x);
  if(controller_type_spatial_x == 1)
    % Full matrix
    sv0_contr_x = [1./s0_x];
  elseif(controller_type_spatial_x == 2)
    % Non-scaling spatial filter
    sv0_contr_x = [1./s0_x(1:16); 0.25./s0_x(17:300); 0.0001./s0_x(301:end)];
  elseif(controller_type_spatial_x == 3)
    % Scaling spatial filter
    sv0_contr_x = [1./s0_x(1:21); 0.05./s0_x(22:300); 0.0001./s0_x(301:end)];
    sv0_contr_x(51) = 1./s0_x(51);
    sv0_contr_x(52) = 1./s0_x(52);
    sv0_contr_x(53) = 1./s0_x(53);
  elseif(controller_type_spatial_x == 4)
    % Non-scaling or scaling with loaded coefficients
    gain_file_name_temp = sprintf('../scripts/%s', gain_file_name_x);
    mode_gains = load(gain_file_name_temp);
    sv0_contr_x = mode_gains./s0_x;
    for mode_index = 300:length(sv0_contr_x)
      if(sv0_contr_x(mode_index) > max_gain_x)
	sv0_contr_x(mode_index) = max_gain_x;
      end
    end
  elseif(controller_type_spatial_x == 5)
    % Experimental filter
    sv0_contr_x = [controller_gain1_x./s0_x(1:nr_sv1_x); controller_gain2_x./s0_x((nr_sv1_x+1):nr_sv2_x); controller_gain3_x./s0_x((nr_sv2_x+1):end)];
  end
  if(use_gain_error_x == 1)
    % Apply gain error to test the sensitivity of the singular values
    gain_error_vector_x = ones(size(sv0_contr_x))+gain_error_std_x.*randn(size(sv0_contr_x));
    for i = 1:length(gain_error_vector_x)
	if(gain_error_vector_x(i)<=0)
	    gain_error_vector_x(i) = 1e-8;
	end
    end 
    sv0_contr_x = sv0_contr_x.*gain_error_vector_x;
  end
  S_contr_x = diag(sv0_contr_x);
  if(row_U_x > row_V_x)
    % S_contr has to be a fat matrix
    S_contr_x = [S_contr_x, zeros(row_V_x, row_U_x-row_V_x)];
  elseif(row_U_x < row_V_x)
    % S_contr has to be a slim matrix
    S_contr_x = [S_contr_x; zeros(row_V_x-row_U_x, row_U_x)];
  end
  Contr_matrix_x = V0_x*S_contr_x*U0_x';
  clear U0_x s0_x sv0_contr_x S_contr_x V0_x;
  
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % frequency part of the controller %
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  if(controller_type_frequency_x == 1)
    contr_filter_x_data = struct('int_out_old', 0, 'output', 0);

    contr_filter_x_param = struct('dummy', 0);
    
    for i=1:length(corr_index)
      for j=electron:positron
        contr_filter_x_data(i,j).output = 0;
        contr_filter_x_data(i,j).int_out_old = 0;
      end
    end
    
  elseif(controller_type_frequency_x == 2)
    contr_filter_x_data = struct('int_out_old', 0, 'low_out_old', 0, 'output', 0);
    contr_filter_x_param = struct('low_a', 0);
    if(T_low <=0)
      contr_filter_x_param.low_a = 0;
    else
      contr_filter_x_param.low_a = exp(-delta_T_short/T_low);
    end
    
    for i=1:length(corr_index)
      for j=electron:positron
        contr_filter_x_data(i,j).output = 0;
        contr_filter_x_data(i,j).int_out_old = 0;
        contr_filter_x_data(i,j).low_out_old = 0;
      end
    end
    
  elseif(controller_type_frequency_x == 3)
    if(orbit_filter_algo_x == 1)
      %contr_filter_x_data = struct('int_out_old', 0, 'low_out_old', 0, 'output', 0, 'lead_out_old', 0, ...
      %	'lead_in_old', 0, 'peak_out_old', 0, 'peak_out_very_old', 0, 'peak_in_old', 0, 'peak_in_very_old', 0);
      %contr_filter_x_param = struct('low_a', 0, 'k_lead', 0, 'b_lead', 0, 'c_lead', 0, 'k_peak', 0, 'b1_peak', 0, 'b0_peak', 0, 'c1_peak', 0, 'c0_peak', 0);

      contr_filter_x_data = struct('int_out_old', 0, 'low_out_old', 0, 'output', 0, 'lead_out_old', 0, 'lead_in_old', 0);
      contr_filter_x_param = struct('low_a', 0, 'k_lead', 0, 'b_lead', 0, 'c_lead', 0);
      
      % low pass
      if(T_low <=0)
	contr_filter_x_param.low_a = 0;
      else
	contr_filter_x_param.low_a = exp(-delta_T_short/T_low);
      end
      
      % lead element
      n3 = exp(-38*delta_T_short);
      z3 = exp(-17*delta_T_short);
      k3 = (1-n3)/(1-z3);
      
      contr_filter_x_param.k_lead = k3;
      contr_filter_x_param.b_lead = -z3;
      contr_filter_x_param.c_lead = -n3;
      
      %% Peak at 0.3Hz
      %n1 = exp((-0.3+1i*2*pi*0.3)*delta_T_short);
      %n2 = exp((-0.3-1i*2*pi*0.3)*delta_T_short);
      %z1 = exp((-1.43+1i*2*pi*0.2)*delta_T_short);
      %z2 = exp((-1.43-1i*2*pi*0.2)*delta_T_short);
      %k1 = real( (1-n1)*(1-n2)/(1-z1)/(1-z2) );
      %b1 = real( -(z1+z2) );
      %b0 = real( z1*z2 );
      %c1 = real( -(n1+n2) );
      %c0 = real( n1*n2 );
      
      %contr_filter_x_param.k_peak = k1;
      %contr_filter_x_param.b1_peak = b1;
      %contr_filter_x_param.c1_peak = c1;
      %contr_filter_x_param.b0_peak = b0;
      %contr_filter_x_param.c0_peak = c0;
      
    else
      contr_filter_x_data = struct('in', zeros(5,1), 'out', zeros(5,1));
      contr_filter_x_param = struct('b', zeros(1,6), 'c', zeros(1, 5));
      
      contr_filter_x_param.b(1) = 0;
      contr_filter_x_param.b(2) = 0;
      contr_filter_x_param.b(3) = -0.231365793633025;
      contr_filter_x_param.b(4) = 0.801063214655195;
      contr_filter_x_param.b(5) = -0.913749779270672;
      contr_filter_x_param.b(6) = 0.344192116711065;
      
      contr_filter_x_param.c(1) = -0.378325629688077;
      contr_filter_x_param.c(2) = 2.410042251927293;
      contr_filter_x_param.c(3) = -5.958267927530270;
      contr_filter_x_param.c(4) = 7.199571859568403;
      contr_filter_x_param.c(5) = -4.273020554277349;
    end
    
    for i=1:length(corr_index)
      for j=electron:positron
        contr_filter_x_data(i,j).output = 0;
        
        if(orbit_filter_algo_x == 1)
          % Integrator
          contr_filter_x_data(i,j).int_out_old = 0;

          % Low-pass
          contr_filter_x_data(i,j).low_out_old = 0;
          
          % Lead element
          contr_filter_x_data(i,j).lead_out_old = 0;
          contr_filter_x_data(i,j).lead_in_old = 0;
          
          %% Peak at 0.3 Hz
          %contr_filter_x_data(i,j).peak_out_old = 0;
          %contr_filter_x_data(i,j).peak_in_old = 0;
          %contr_filter_x_data(i,j).peak_out_very_old = 0;
          %contr_filter_x_data(i,j).peak_in_very_old = 0;
          
        else
          contr_filter_x_data(i,j).in = zeros(5,1);
          contr_filter_x_data(i,j).out = zeros(5,1);
        end
      end
    end
  else
    % Unknown type
    fprintf(1, '\n\nThe specified controller type is unknown. Simulation terminated in create_noise_data!!!\n\n');
    exit;
  end
  
end

if(use_controller_y == 1)
  
  % Init dispersive orbit
  if(strcmp(use_set_value, 'dispersion_init'))
    golden_orbit_y_elec = load('../scripts/golden_orbit_y_elec.dat');
    golden_orbit_y(:,:,electron) = golden_orbit_y_elec';
    golden_orbit_y_posi = load('../scripts/golden_orbit_y_posi.dat');
    golden_orbit_y(:,:,positron) = golden_orbit_y_posi';
    clear golden_orbit_y_elec golden_orbit_y_posi;
  end
  
  % Init dispersion filter
  if(use_disp_suppression_y == 1)
    disp_pattern_y_posi = load('../scripts/disp_y_posi.dat');
    disp_pattern_y(:,:,positron) = disp_pattern_y_posi';
    disp_pattern_y_elec = load('../scripts/disp_y_elec.dat');
    disp_pattern_y(:,:,electron) = disp_pattern_y_elec';
    clear disp_pattern_y_posi disp_pattern_y_elec;
  end
  
  % Init BPM scaling
  
  if(use_bpm_scaling == 1)
    bpm_scaling_factor_y = load('-ascii', 'bpm_scaling_factor_y.dat');
  end
  
  % response matrix for QD0 
  R_QD0_y = R0_y(end,:);

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % Spatial part of the controller %
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
  contr_error_y = zeros(length(bpm_index), 2);
  spatial_filter_output_y = zeros(length(corr_index), 2);
  
  [row_U_y col_U_y] = size(U0_y);
  [row_V_y col_V_y] = size(V0_y);
  if(controller_type_spatial_y == 1)
    % Full matrix
    sv0_contr_y = [1./s0_y];
  elseif(controller_type_spatial_y == 2)
    % Non-scaling filter
    sv0_contr_y = [1./s0_y(1:16); 0.25./s0_y(17:300); 0.0001./s0_y(301:end)];
  elseif(controller_type_spatial_y == 3)
    % Scaling filter
    sv0_contr_y = [1./s0_y(1:17); 0.05./s0_y(18:300); 0.0001./s0_y(301:end)];
    sv0_contr_y(47) = 1./s0_y(47);
  elseif(controller_type_spatial_y == 4)
    % Non-scaling or scaling with loaded coefficients
    gain_file_name_temp = sprintf('../scripts/%s', gain_file_name_y);
    mode_gains = load(gain_file_name_temp);
    sv0_contr_y = mode_gains./s0_y;
     for mode_index = 300:length(sv0_contr_y)
      if(sv0_contr_y(mode_index) > max_gain_y)
	sv0_contr_y(mode_index) = max_gain_y;
      end
    end
  elseif(controller_type_spatial_y == 5)
    % Test filter
    sv0_contr_y = [controller_gain1_y./s0_y(1:nr_sv1_y); controller_gain2_y./s0_y((nr_sv1_y+1):nr_sv2_y); controller_gain3_y./s0_y((nr_sv2_y+1):end)];
  end
  if(use_gain_error_y == 1)
    % Apply gain error to test the sensitivity of the singular values
    gain_error_vector_y = ones(size(sv0_contr_y))+gain_error_std_y.*randn(size(sv0_contr_y));
    for i = 1:length(gain_error_vector_y)
	if(gain_error_vector_y(i)<=0)
	    gain_error_vector_y(i) = 1e-8;
	end
    end 
    sv0_contr_y = sv0_contr_y.*gain_error_vector_y;
  end
  S_contr_y = diag(sv0_contr_y);
  if(row_U_y > row_V_y)
    % S_contr has to be a fat matrix
    S_contr_y = [S_contr_y, zeros(row_V_y, row_U_y-row_V_y)];
  elseif(row_U_y < row_V_y)
    % S_contr has to be a slim matrix
    S_contr_y = [S_contr_y; zeros(row_V_y-row_U_y, row_U_y)];
  end
  Contr_matrix_y = V0_y*S_contr_y*U0_y';
  clear U0_y s0_y sv0_contr_y S_contr_y V0_y;
  
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % frequency part of the controller %
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
  if(controller_type_frequency_y == 1)
    contr_filter_y_param = struct('dummy', 0);
    contr_filter_y_data = struct('int_out_old', 0, 'output', 0);
    for i=1:length(corr_index)
      for j=electron:positron
        contr_filter_y_data(i,j).output = 0;
        contr_filter_y_data(i,j).int_out_old = 0;
      end
    end
  elseif(controller_type_frequency_y == 2)
    contr_filter_y_data = struct('int_out_old', 0, 'low_out_old', 0, 'output', 0);
    contr_filter_y_param = struct('low_a', 0);
    if(T_low <=0)
      contr_filter_y_param.low_a = 0;
    else
      contr_filter_y_param.low_a = exp(-delta_T_short/T_low);
    end

    for i=1:length(corr_index)
      for j=electron:positron
        contr_filter_y_data(i,j).output = 0;
        contr_filter_y_data(i,j).int_out_old = 0;
        contr_filter_y_data(i,j).low_out_old = 0;
      end
    end
  elseif(controller_type_frequency_y == 3)
    if(orbit_filter_algo_y == 1)
      %contr_filter_y_data = struct('int_out_old', 0, 'low_out_old', 0, 'output', 0, 'lead_out_old', 0, ...
      %	'lead_in_old', 0, 'peak_out_old', 0, 'peak_out_very_old', 0, 'peak_in_old', 0, 'peak_in_very_old', 0);
      %contr_filter_y_param = struct('low_a', 0, 'k_lead', 0, 'b_lead', 0, 'c_lead', 0, 'k_peak', 0, 'b1_peak', 0, 'b0_peak', 0, 'c1_peak', 0, 'c0_peak', 0);
      
      contr_filter_y_data = struct('int_out_old', 0, 'low_out_old', 0, 'output', 0, 'lead_out_old', 0, 'lead_in_old', 0);
      contr_filter_y_param = struct('low_a', 0, 'k_lead', 0, 'b_lead', 0, 'c_lead', 0);
      
      % low pass
      if(T_low <=0)
	contr_filter_y_param.low_a = 0;
      else
	contr_filter_y_param.low_a = exp(-delta_T_short/T_low);
      end
      
      % lead element
      n3 = exp(-38*delta_T_short);
      z3 = exp(-17*delta_T_short);
      k3 = (1-n3)/(1-z3);
      
      contr_filter_y_param.k_lead = k3;
      contr_filter_y_param.b_lead = -z3;
      contr_filter_y_param.c_lead = -n3;
      
      %% Peak at 0.3Hz
      %n1 = exp((-0.3+1i*2*pi*0.3)*delta_T_short);
      %n2 = exp((-0.3-1i*2*pi*0.3)*delta_T_short);
      %z1 = exp((-1.43+1i*2*pi*0.2)*delta_T_short);
      %z2 = exp((-1.43-1i*2*pi*0.2)*delta_T_short);
      %k1 = real( (1-n1)*(1-n2)/(1-z1)/(1-z2) );
      %b1 = real( -(z1+z2) );
      %b0 = real( z1*z2 );
      %c1 = real( -(n1+n2) );
      %c0 = real( n1*n2 );
      
      %contr_filter_y_param.k_peak = k1;
      %contr_filter_y_param.b1_peak = b1;
      %contr_filter_y_param.c1_peak = c1;
      %contr_filter_y_param.b0_peak = b0;
      %contr_filter_y_param.c0_peak = c0;
      
    else
      contr_filter_y_data = struct('in', zeros(5,1), 'out', zeros(5,1));
      contr_filter_y_param = struct('b', zeros(1,6), 'c', zeros(1, 5));
      
      contr_filter_y_param.b(1) = 0;
      contr_filter_y_param.b(2) = 0;
      contr_filter_y_param.b(3) = -0.231365793633025;
      contr_filter_y_param.b(4) = 0.801063214655195;
      contr_filter_y_param.b(5) = -0.913749779270672;
      contr_filter_y_param.b(6) = 0.344192116711065;
      
      contr_filter_y_param.c(1) = -0.378325629688077;
      contr_filter_y_param.c(2) = 2.410042251927293;
      contr_filter_y_param.c(3) = -5.958267927530270;
      contr_filter_y_param.c(4) = 7.199571859568403;
      contr_filter_y_param.c(5) = -4.273020554277349;
    end
    
    for i=1:length(corr_index)
      for j=electron:positron
        contr_filter_y_data(i,j).output = 0;
        
        if(orbit_filter_algo_y == 1)
          % Integrator
          contr_filter_y_data(i,j).int_out_old = 0;
          
          % Low-pass
          contr_filter_y_data(i,j).low_out_old = 0;
          
          % Lead element
          contr_filter_y_data(i,j).lead_out_old = 0;
          contr_filter_y_data(i,j).lead_in_old = 0;
          
          %% Peak at 0.3 Hz
          %contr_filter_y_data(i,j).peak_out_old = 0;
          %contr_filter_y_data(i,j).peak_in_old = 0;
          %contr_filter_y_data(i,j).peak_out_very_old = 0;
          %contr_filter_y_data(i,j).peak_in_very_old = 0;
          
        else
          contr_filter_y_data(i,j).in = zeros(5,1);
          contr_filter_y_data(i,j).out = zeros(5,1);
        end
      end
    end
  else
    % Unknown type
    fprintf(1, '\n\nThe specified controller type is unknown. Simulation terminated in create_noise_data!!!\n\n');
    exit;
  end
end

%%%%%%%%%%%%%%%
% IP feedback %
%%%%%%%%%%%%%%%

if(use_ip_feedback == 1)
    corr_setting_ip_x = 0;
    corr_setting_ip_y = 0;
end

if ( use_ip_feedback_annecy == 1 )
    load(white_noise_file)		% Enable  white noise
end
