% Script to perform one step of the long-term misalignment
%
% Juergen Pfingstner
% 12. April 2012

time_step_index = time_step_index+1;
Tcl_SetVar("time_step_index", time_step_index);
global_time = delta_T_long*time_step_index_long;

%% Apply long-term ground motion

if((ground_motion_x == 1) || (ground_motion_y == 1))
    for i=electron:positron
	line_name = beamlinename(i);

	if(i==electron)
	    Tcl_Eval("ground_motion_long electron $delta_T_long");
	else
	    Tcl_Eval("ground_motion_long positron $delta_T_long");
	end
    end

    %% Centre beam 
    if(use_beam_beam && use_bds)
	fprintf(1, "Centre beam ... ");
	centre_beam;
    else
	tracking;
    end

    %% Store results
    postprocess_time_step;
    fprintf(1, "done\n");
end
