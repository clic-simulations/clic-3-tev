% calc_contr_filter_step.m
%
% Juergen Pfingstner
% 21. March 2011

function [data] = calc_contr_filter_step(data, param, input, filter_type, algo_type)
% Function to calculate the frequency dependent output of the orbit
% feedback filter.
%
% Input: 
%     data  ... filter data to be updated
%     input ... input data to the filter
%     filter_type ... which filter should be applied
%                     1: integrator
%                     2: integrator + low-pass
%                     3: 2 + lead element + 0.3 Hz peak
%
% Output:
%     data  ... modified filter data
%

if(filter_type == 1)
  for i=1:length(input)

    % Read data
    int_out_old = data(i).int_out_old;

    % Filter
    int_out_new = int_out_old + input(i);
    
    % Write back data
    data(i).int_out_old = int_out_new;
    data(i).output = int_out_new;

  end
end

if(filter_type == 2)
  for i=1:length(input)

    % Read data
    int_out_old = data(i).int_out_old;
    low_out_old = data(i).low_out_old;

    % Filter
    int_out_new = int_out_old + input(i);
    low_out_new = param.low_a*low_out_old + (1-param.low_a)*int_out_new;

    % Write back data
    data(i).int_out_old = int_out_new;
    data(i).low_out_old = low_out_new;
    data(i).output = low_out_new;

  end
end

if(filter_type == 3)
  if(algo_type == 1)
    low_a = param.low_a;
    k_lead = param.k_lead;
    b_lead = param.b_lead;
    c_lead = param.c_lead;
    %k_peak = param.k_peak;
    %b1_peak = param.b1_peak;
    %b0_peak = param.b0_peak;
    %c1_peak = param.c1_peak;
    %c0_peak = param.c0_peak;
  else
    b = param.b;
    c = param.c;
    b6 = b(6);
    b = b(5:-1:1);
    c = c(end:-1:1);
  end
  for i=1:length(input)
    if(algo_type == 1)
      %%%%%%%%%%%%%%%%%%%%%%%%%%
      % Version 1: splitted up %
      %%%%%%%%%%%%%%%%%%%%%%%%%%
    
      % Read data
      int_out_old = data(i).int_out_old;
      
      low_out_old = data(i).low_out_old;
    
      lead_out_old = data(i).lead_out_old;
      lead_in_old = data(i).lead_in_old;
    
      %peak_out_old = data(i).peak_out_old;
      %peak_out_very_old = data(i).peak_out_very_old;
      %peak_in_old = data(i).peak_in_old;
      %peak_in_very_old = data(i).peak_in_very_old;
    
      % Filter
      int_out_new = int_out_old + input(i);
      low_out_new = low_a*low_out_old + (1-low_a)*int_out_new;
      lead_out_new = -c_lead*lead_out_old + k_lead*low_out_new + k_lead*b_lead*lead_in_old;
      %peak_out_new = -c1_peak*peak_out_old - c0_peak*peak_out_very_old ...
	%  + k_peak*(lead_out_new + b1_peak*peak_in_old + b0_peak*peak_in_very_old);
    
      % Write back data
      data(i).int_out_old = int_out_new;
    
      data(i).low_out_old = low_out_new;

      data(i).lead_out_old = lead_out_new;
      data(i).lead_in_old = low_out_new;
      
      %data(i).peak_out_very_old = peak_out_old;
      %data(i).peak_in_very_old = peak_in_old;
      %data(i).peak_out_old = peak_out_new;
      %data(i).peak_in_old = lead_out_new;
    
      %data(i).output = peak_out_new;
      data(i).output = lead_out_new;
    else
      
      %%%%%%%%%%%%%%%%%%%%%%%%%
      % Version 2: big filter %
      %%%%%%%%%%%%%%%%%%%%%%%%%

      % Read data
      out = data(i).out;
      in = data(i).in;
      input_temp = input(i);
      
      % Filter
      data(i).output = - c*out + b6*input_temp + b*in;
      %data(i).output = -c(5)*out(1)-c(4)*out(2)-c(3)*out(3)-c(2)*out(4)-c(1)*out(5)+ ...
             %b(6)*input(i)+b(5)*in(1)+b(4)*in(2)+b(3)*in(3)+b(2)*in(4)+b(1)*in(5);
      
      % Write back data
      data(i).out = [data(i).output; out(1:4)];
      data(i).in = [input(i); in(1:4)];
    end
    
  end
end






