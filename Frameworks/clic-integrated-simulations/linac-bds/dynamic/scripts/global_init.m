% Global initiatization
%
% Jochem Snuverink and Juergen Pfingstner

randn('seed', groundmotion_seed_nr);
warning("off", "Octave:assign-as-truth-value"); % turn off useless warning in controller_calc.m (IP Feedback)

% to be removed:
addpath('../scripts/');
addpath('/usr/share/placet/octave');
addpath('/home/jpfingst/Software/placet-development/scripts/placet/octave/');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Test for infeasible parameters %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if( (use_main_linac == 1) & (use_bds == 0) & (use_beam_beam == 1) )
    fprintf(1, 'Infeasible beamline configuration\n');
    exit;
elseif( (use_main_linac == 0) & (use_bds == 0) & (use_beam_beam == 1) )
    fprintf(1, 'Infeasible beamline configuration\n');
    exit;
elseif( (use_main_linac == 0) & (use_bds == 0) & (use_beam_beam == 0) )
    fprintf(1, 'Infeasible beamline configuration\n');
    exit;
else
    fprintf(1, 'Using beamline configuration:    ');
    if(use_main_linac == 1)
	fprintf(1, 'MainLinac    ');
    end
    if(use_bds == 1)
	fprintf(1, 'BDS    ');
    end
    if(use_beam_beam == 1)
	fprintf(1, 'BeamBeam');
    end
    fprintf(1,'\n');
end


%%%%%%%%%%%%%%%%%%%%%%%%%%
% General initialisation %
%%%%%%%%%%%%%%%%%%%%%%%%%%

output_precision(10);

Tcl_Eval('RandomReset -seed "$groundmotion(seed_nr) $groundmotion(seed_nr) $groundmotion(seed_nr) $groundmotion(seed_nr)"')

machine_index = -1;
Tcl_SetVar("machine_index", "-1");
time_step_index = -1;
Tcl_SetVar("time_step_index", "-1");
Tcl_SetVar("multipulse_nr", multipulse_nr);

% index of beamlines (used as enum), should be made const!
electron = 1;
positron = 2;
beamlinename = ['electron' ; 'positron'];
x = 1;
y = 2;
sim_mode = 0;
time_step_index_long = 0;
global_time = 0;
for i=electron:positron
    empty_beam(i) = 0;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Load BDS knobs procedures %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if (use_bds_knobs)
  Tcl_Eval("source $script_dir/scripts/multipole_knobs.tcl");
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Get the correctors and BPMs %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

source "../scripts/create_bpm_corr.m"

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Set up the BPM resolution                       %
% (not valid for the response matrix calculation) %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if( use_main_linac == 1 )
  for i=electron:positron
    bpm_index_ml(i,:) = bpm_index(i,1:nr_corr_ml(i));
    placet_element_set_attribute(beamlinename(i,:), bpm_index_ml(i,:), 'resolution', bpm_resolution_ml);
  end
end

if( use_bds == 1 )
  for i=electron:positron
    % bpm number at start of BDS, should ideally be determined differently.
    bds_start_bpm = nr_corr_ml(i)+1;
    bpm_index_bds(i,:) = bpm_index(i,bds_start_bpm:end);
    placet_element_set_attribute(beamlinename(i,:), bpm_index_bds(i,:), 'resolution', bpm_resolution_bds);
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Response matrix and controller calculation or loading %
%                                                       %
% Remark: electron and positron linac are completely    %
%         symmetric, so there is no need to calculate   %
%         the response matrix two times                 %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if (use_controller_x || use_controller_y)
  source "../scripts/create_load_matrices.m"
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Initialize and Calculate the controller %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if (use_controller_x || use_controller_y)
  source "../scripts/create_controller_data.m"
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Initialize the longterm and static methods %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

source "../scripts/create_longterm_methods_data.m"

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Get initial emittance and init variables %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if (save_meas_stations)
  source "../scripts/init_storage_variables.m"
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Setup the sytem identification %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if( use_main_linac == 1 )
  if(identification_switch == 1)
    cycle_index = 0;
    nr_bumps = 0;
    fprintf(1, 'Read bumps\n');
    source "../scripts/bump_data_3kick.m"
    fprintf(1, 'Create excitation\n');
    source "../scripts/calc_excitation_ls.m"
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Setup the corrector noise data %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

source "../scripts/create_noise_data.m"

nr_time_steps = nr_time_steps_long + nr_time_steps_long*nr_time_steps_short;
max_global_time = delta_T_long*nr_time_steps_long + delta_T_short*nr_time_steps_long*nr_time_steps_short;

