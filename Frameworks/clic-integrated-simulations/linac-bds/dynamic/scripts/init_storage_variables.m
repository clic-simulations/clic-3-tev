% Script to create the data needed for storing
%
% Juergen Pfingstner

% gm ... l2 of ground motion
% corr ... l2 of corrector settings
%
% spe ... single-pulse emittance [nm]
% mpe ... multi-pulse emittance

machine_index = -1;

% Get beam after linac, if linac not run:
if (use_main_linac==0)
  name = 'electron';
  Tcl_SetVar('name', name);
  Tcl_Eval("BeamlineUse -name linac_electron");
  Tcl_Eval("BeamLoadAll -beam beam_electron -file electron_start.dat");
  [Ee,Be]=placet_test_no_correction("linac_electron", "beam_electron", "None");
  Tcl_Eval("BeamlineDelete -name linac_electron");

  name = 'positron';
  Tcl_SetVar('name', name);
  Tcl_Eval("BeamlineUse -name linac_positron");
  Tcl_Eval("BeamLoadAll -beam beam_positron -file positron_start.dat");
  [Ep,Bp]=placet_test_no_correction("linac_positron", "beam_positron", "None");
  Tcl_Eval("BeamlineDelete -name linac_positron");
end

% Get initial emittance, needed for storing
fprintf(1, 'Get the initial emittance, luminosity, ... \n');
source "../scripts/tracking.m"

gm_x = 0;
corr_x = 0;
gm_y = 0;
gm_ml_y = 0;
gm_bds_y = 0;
corr_y = 0;
corr_ml_y = 0;
corr_bds_y = 0;
if(use_bds == 1);
  gm_diff1_y = 0;
  gm_diff2_y = 0;
end
  

if( use_main_linac == 1 )
  % Measurement station 1
  
  Tcl_SetVar('file_start_index', -1);
  Tcl_SetVar('file_stop_index', -1);
  Tcl_Eval('array set mpe_data [calc_emitt_ellipse electron_linac_ $file_start_index $file_stop_index $eval_beam_shift]')
  Tcl_Eval('export_array_2_octave mpe_data');

  Tcl_Eval('read_array electron_linac_beampar_-1');
  Tcl_Eval('export_array_2_octave electron_linac_beampar_-1');

  spe_m1_x = 0;  
  mpe_m1_x_old = 0;
  mpe_m1_x = 0;
  spe_m1_y = 0;
  mpe_m1_y_old = 0;
  mpe_m1_y = 0;
  e_m1 = 0;
  e_est_x_elec = 0;
  e_est_x_posi = 0;

end

if( use_bds == 1 )
  % Measurement station 2
  
  Tcl_SetVar('file_start_index', -1);
  Tcl_SetVar('file_stop_index', -1);
  Tcl_Eval('array set mpe_data [calc_emitt_particle electron_bds_ $file_start_index $file_stop_index $eval_beam_shift]')
  Tcl_Eval('export_array_2_octave mpe_data');
  
  Tcl_Eval('read_array electron_bds_beampar_-1');
  Tcl_Eval('export_array_2_octave electron_bds_beampar_-1');

  spe_m2_x = 0;
  mpe_m2_x_old = 0;
  mpe_m2_x = 0;
  spe_m2_y = 0;
  mpe_m2_y_old = 0;
  mpe_m2_y = 0;
end

if( use_beam_beam == 1 )

  % Measurement station 3
  
  Tcl_SetVar('file_start_index', -1);
  Tcl_SetVar('file_stop_index', -1);
  Tcl_Eval('array set mpe_data [calc_emitt_particle electron_ip_ $file_start_index $file_stop_index $eval_beam_shift]')
  Tcl_Eval('export_array_2_octave mpe_data');
  
  Tcl_Eval('read_array electron_ip_beampar_-1');
  Tcl_Eval('export_array_2_octave electron_ip_beampar_-1');

  spe_m3_x = 0;
  Lumi_buffer = Lumi_high .* ones(multipulse_nr, 1);
  delta_buffer_x = zeros(multipulse_nr, 1);
  delta_buffer_y = zeros(multipulse_nr, 1);  
  mpe_m3_x_old = 0;
  mpe_m3_x = 0;
  spe_m3_y = 0;
  mpe_m3_y_old = 0;
  mpe_m3_y = 0;
  
end
