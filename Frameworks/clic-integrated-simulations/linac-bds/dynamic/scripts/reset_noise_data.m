% reset_noise_data.m
%
% Juergen Pfingstner
% 14. Jan 2011

function [data] = reset_noise_data(data, noise_type)
% Function to reset the data needed to create noise of a certain type
%
% Input: 
%     data       ... data to be reseted
%     noise_type ... type of the noise the data are used for
%
% Output:
%     data ... reseted data
%

[temp, element_nr] = size(data);
for i = 1:element_nr
  if(strcmp(noise_type, 'white_gaussian'))
    data(i).actual_value = 0;
  elseif(strcmp(noise_type, 'white_uniform'))
    data(i).actual_value = 0;
  else
    % unknown type
    fprintf(1, '\n\nThe specified corrector noise type is unknown. Simulation terminated in reset_noise_data!!!\n\n');
    exit;
  end
end



