set fn [lindex $argv 0]
set f [open $fn r]
gets $f line
set zl ""
while {![eof $f]} {
     lappend zl $line
     gets $f line
}
close $f
set zmax [lindex $zl end]
set f [open [lindex $argv 1] w]
if {[lindex $argv 2]=="electron" || [lindex $argv 2]=="bdselectron" } {
     foreach z $zl {
	puts $f [expr $z-$zmax]
     }
} {
     foreach z $zl {
	puts $f [expr $zmax-$z]
     }
}
close $f
