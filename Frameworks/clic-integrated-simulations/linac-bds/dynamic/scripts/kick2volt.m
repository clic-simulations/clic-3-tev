% Function that converts a wanted kick of the beam to an equvalent
% voltage for the dipole kicker. The calculation allowes to
% approxiomately 
%create the same kicks along the beam line.
%
% Juergen Pfingstner
% 27th September 2010

function [volt]=kick2volt(kick, pos);
% Function that converts a wanted kick of the beam into an
% equivalent voltage for the dipole corrector.
%
% Input:
%        kick ... wanted kick [urad]
%        pos ... position of the kick in the beam line [m]
%
% Output
%        volt ... voltage to be applied to the kicker [kV]

E0 = 9;
f0 = 3.45;

if (pos < 21028)
  % In the main linac the energy is a linar function of the linac length
  E = (1500-E0)/21028*pos + E0;
else
  % In the BDS the thing is easy
  E = 1500;
end

volt = 1/f0 * sqrt(E0*E).*kick;

end
