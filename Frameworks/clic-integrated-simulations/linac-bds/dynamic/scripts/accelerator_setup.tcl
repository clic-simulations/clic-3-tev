#
# General
#
Random8 -type gaussian gcav

# load tcl procedures to setup beam and lattice
source $script_dir/scripts/clic_basic_single.tcl
source $script_dir/scripts/clic_beam.tcl
source $script_dir/scripts/clic_beam_pars.tcl
source $script_dir/lattices/lattice4b.tcl
# tcl procedures for imperfections
source $script_dir/scripts/imperfection.tcl

#
# GUINEA-PIG related functions
#
if {$use_beam_beam} {
    source $script_dir/scripts/clic_guinea.tcl
}

puts "charge $match(charge)"
puts "emitt_y $match(emitt_y)"

############################
# Setting up the beamlines #
############################

source $script_dir/scripts/tracking_procs.tcl
source $script_dir/scripts/ground_motion.tcl
source $script_dir/scripts/postprocess_procs.tcl

foreach name { electron positron } {
    # tracking parameters for BDS only! Not on for ML
    set e_initial $e_initial_linac
    set e0 $e_initial
    set quad_synrad 0
    set sbend_synrad 0
    set mult_synrad 0
    set synrad 0
    puts "Putting $name Linac"
    BeamlineNew
    Girder
    #if {$debug} {TclCall -script "print_beam_params"}
    
    ReferencePoint -sense 1
    TclCall -script "add_initial_jitter_$name"
    Linac::choose_linac $script_dir/lattices/phase.def $phase
    Linac::put_linac
    # Add corrector Dipoles after each Quadrupole
    if {$dipole_correctors} {
	AddDipolesToBeamline -before 0
    }
    # First dump after Linac
    ReferencePoint -sense -1
    if {$response_matrix_calc==0} {
	TclCall -name "ML_END" -script "save_beam_lin$name"
    }

    if {$use_main_linac == 0} {
	BeamlineSet -name linac_${name}
	# Start new beamline for BDS
	BeamlineNew
	Girder
    } elseif {$load_beam_bds} {
	TclCall -script "beamloadall"
    }

    if {$use_bds} { 
	puts "Putting BDS"
	ReferencePoint -name "START_BDS" -sense 1
	SlicesToParticles -seed $params(seed) -type 1 -name "S2P"
	set e_initial $e_initial_bds
	set e0 $e_initial
	# tracking parameters for BDS only! Not on for ML
	set quad_synrad 1
	set sbend_synrad 1
	set mult_synrad 1
	set synrad 1
	# Second dump just after QD0, included in special lattice
	if {$response_matrix_calc} {
	    if {$use_collimators} {
		source $script_dir/lattices/bds.match.linac4b_wakes
	    } else { 
		source $script_dir/lattices/bds.match.linac4b_noape
	    }
	} else {
	    if {$use_collimators} {
		source $script_dir/lattices/bds.match.linac4b_wakes_monitor
	    } else { 
		source $script_dir/lattices/bds.match.linac4b_noape_monitor
	    }
	}
	if {$external_ground_cms} {
	    TclCall -script "offset_beam_$name"
	}
	ReferencePoint -sense -1
	TclCall -script "save_beam_ip$name"
    }

    BeamlineSet -name $name
    BeamlinePrint -file "beamline_${name}.txt"
    WriteGirderLength -file "girder_${name}.dat" -beginning_only 1
    set nr_girders(${name}) [linecount girder_${name}.dat]
    # Initialises to zero offsets (probably not needed)
    Zero
}

# get some element numbers for later use
if {$use_bds} {
    Octave {
	index_start_bds = placet_get_name_number_list('$name','START_BDS');
	Tcl_SetVar('start_bds',index_start_bds(end));
	index_octdrift_bff = placet_get_name_number_list('$name', 'OCTDRIFT-BFF');
	Tcl_SetVar('start_cantilever',index_octdrift_bff(end));
	index_ip = placet_get_name_number_list('$name', 'IP');
	Tcl_SetVar('end_cantilever',index_ip(end));
	
	index_d2od_bff = placet_get_name_number_list('$name', 'D2OD-BFF');
	index_sf1_ff = placet_get_name_number_list('$name', 'SF1');
	index_octdrift_ff = placet_get_name_number_list('$name', 'OCTDRIFT-FF');
	index_qf1 = (placet_get_name_number_list('$name', 'QF1'))(:,end);
	index_qd0 = placet_get_name_number_list('$name', 'QD0');
    }
}

if {$response_matrix_calc==0 && $use_main_linac} {
    Octave {
	ML_END = placet_get_name_number_list('$name', 'ML_END');
	Tcl_SetVar('end_main_linac',ML_END(end));
    }
}

#######################
# Setting up the beam #
#######################

set energy_amplification 1.0
set e_initial $e_initial_linac
set e0 $e_initial

foreach beamname { electron  positron } {
    #    Generate beam
    #    global n_slice n_part n_bunches

    puts "Generating the beam"
    puts "n. slices $n_slice "
    puts "n. particles $n_part "
    make_beam_slice_energy_gradient beam_${beamname} $n_slice $n_part 1.0 1.0
    #make_beam_slice_energy_gradient_train beam_${beamname} $n_slice $n_part 1.0 1.0 $n_bunches
    BeamSaveAll -file ${beamname}_start.dat
}

# this is needed in order to overwrite the sliced beam info

BeamlineUse -name electron;
#set cav0_elec [CavityGetPhaseList]
#set grad0_elec [CavityGetGradientList]
set cav_init_elec [CavityGetPhaseList]
set grad_init_elec [CavityGetGradientList]
BeamlineUse -name positron;
#set cav0_posi [CavityGetPhaseList]
#set grad0_posi [CavityGetGradientList]
set cav_init_posi [CavityGetPhaseList]
set grad_init_posi [CavityGetGradientList]

