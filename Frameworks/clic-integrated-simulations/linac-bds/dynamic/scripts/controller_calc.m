% Script that contrains the controller calculations
%
% Jochem Snuverink and Juergen Pfingstner

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Beam-Based MIMO Feedback %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if( (use_main_linac == 1) | (use_bds == 1))
  
  %%%%%%%%%%%%%%%
  % In Software %
  %%%%%%%%%%%%%%%
  if(use_controller_x == 1)
    for i=electron:positron
      % Calculate the error
      if(strcmp(use_set_value, 'bpm_center'))
        contr_error_x(:,i) = -bpm_readings(:,x,i);
      elseif(strcmp(use_set_value, 'dispersion_init'))
        contr_error_x(:,i) = golden_orbit_x(:,1,i) - bpm_readings(:,x,i);
      else
        fprintf(1, '\n\nUnknown set value type!!!\n\n');
        exit;
      end
      
      % Remove the dispersion signature if wanted
      if(use_disp_suppression_x == 1)
        [contr_error_x(:,i), projection_x(i)] = remove_dispersion(contr_error_x(:,i), disp_pattern_x(:,i));
      else
        projection_x(i) = 0;
      end
      
      e_est_x(i) = 1.138e-4*projection_x(i);
      
      % BPM scaling
      % Remark: The BPM values could have been scaled on the first
      % hand. Then the golden_orbit and the dispersion pattern, would
      % have to be recalculated accordingly. To save this work I just
      % scale the remaining error, which is the same. 
      if(use_bpm_scaling == 1)
        contr_error_x(:,i) = contr_error_x(:,i).*bpm_scaling_factor_x;
      end
      
      % spatial filtering -> modified SVD
      spatial_filter_output_x(:,i) = Contr_matrix_x * contr_error_x(:,i);
      
      % frequency filtering
      contr_filter_x_data(:,i) = calc_contr_filter_step(contr_filter_x_data(:,i), contr_filter_x_param, spatial_filter_output_x(:,i), controller_type_frequency_x , orbit_filter_algo_x);
      for j=1:length(spatial_filter_output_x)
        corr_settings_new_x(j,i) = contr_filter_x_data(j,i).output;
      end
      
      % Calculate the increment
      corr_increment_x(:,i) = corr_settings_new_x(:,i) - corr_settings_old_x(:,i);
      
      % Store for next run
      corr_settings_old_x(:,i) = corr_settings_new_x(:,i);
    end
  end
  
  if(use_controller_y == 1)
    for i=electron:positron

      % Calculate the error
      if(strcmp(use_set_value, 'bpm_center'))
        contr_error_y(:,i) = -bpm_readings(:,y,i);
      elseif(strcmp(use_set_value, 'dispersion_init'))
        contr_error_y(:,i) = golden_orbit_y(:,1,i) - bpm_readings(:,y,i);
      else
        fprintf(1, '\n\nUnknown set value type!!!\n\n');
        exit;
      end
      
      % Remove the dispersion signature if wanted
      if(use_disp_suppression_y == 1)
        [contr_error_y(:,i), projection_y(i)] = remove_dispersion(contr_error_y(:,i), disp_pattern_y(:,i));
      end
      
      % BPM scaling
      % Remark: The BPM values could have been scaled on the first
      % hand. Then the golden_orbit and the dispersion pattern, would
      % have to be recalculated accordingly. To save this work I just
      % scale the remaining error, which is the same. 
      if(use_bpm_scaling == 1)
        contr_error_y(:,i) = contr_error_y(:,i).*bpm_scaling_factor_y;
      end
      
      % spatial filtering -> modified SVD
      spatial_filter_output_y(:,i) = Contr_matrix_y * contr_error_y(:,i);

      % frequency filtering
      contr_filter_y_data(:,i) = calc_contr_filter_step(contr_filter_y_data(:,i), contr_filter_y_param, spatial_filter_output_y(:,i), controller_type_frequency_y , orbit_filter_algo_y);
      for j=1:length(spatial_filter_output_y)
        corr_settings_new_y(j,i) = contr_filter_y_data(j,i).output;
      end
      
      % Calculate the increment
      corr_increment_y(:,i) = corr_settings_new_y(:,i) - corr_settings_old_y(:,i);
      
      % % Store for next run
      corr_settings_old_y(:,i) = corr_settings_new_y(:,i);
      
      % Expected beam position change at QD0:
      %disp("bpm readings QD0");
      bpm_reading_qd0(i) = bpm_readings(end,y,i);
      beam_pos_qd0_y(i) = R_QD0_y * corr_increment_y(:,i);
    end
  end
  
  %%%%%%%%%%%%%%%
  % In Hardware %
  %%%%%%%%%%%%%%%
  
  % Add corrector noise coming from the stabilization 
  % Remark: There is another noise source apart from stabilization
  %         noise, the positioning error. It acts exactly the same
  %         way as the stabilization error, but has possibly a
  %         different characteristics. It is a random walk (1/f
  %         noise). This has to be implemented in future. 
  
  for i=electron:positron
    if(stabilization_noise_x == 1)
      [stabilization_noise_data_x(:,i), stabilization_noise_seed] = ...
          calc_noise_time_step(stabilization_noise_data_x(:,i), stabilization_noise_type, stabilization_noise_seed);
      corr_noise_x(:,i) = [stabilization_noise_data_x(:,i).actual_value];
      corr_values_new_x(:,i) = corr_settings_new_x(:,i) + corr_noise_x(:,i);
    else
      corr_values_new_x(:,i) = corr_settings_new_x(:,i);
    end
    
    if(stabilization_noise_y == 1)
      [stabilization_noise_data_y(:,i), stabilization_noise_seed] = ...
          calc_noise_time_step(stabilization_noise_data_y(:,i), stabilization_noise_type, stabilization_noise_seed);
      corr_noise_y(:,i) = [stabilization_noise_data_y(:,i).actual_value];
      corr_values_new_y(:,i) = corr_settings_new_y(:,i) + corr_noise_y(:,i);
    else
      corr_values_new_y(:,i) = corr_settings_new_y(:,i);
    end

    if(stabilization_scaling_error_x == 1)
	corr_values_new_x(:,i) = corr_values_new_x(:,i).*stabilization_scaling_values_x(i,:)';
    end
    
    if(stabilization_scaling_error_y  == 1)
	corr_values_new_y(:,i) = corr_values_new_y(:,i).*stabilization_scaling_values_y(i,:)';
    end
    
    % corrector failures, does not work fully correctly when
    % stabilization noise is also on
    if (use_corr_failure==1)
        corr_values_new_x(:,i) = corr_values_new_x(:,i) - corr_increment_x(:,i).*corr_failure(:,i);
        corr_values_new_y(:,i) = corr_values_new_y(:,i) - corr_increment_y(:,i).*corr_failure(:,i);
    end
  end
end

%%%%%%%%%%%%
% FD model %
%%%%%%%%%%%%

% prediction of movement at the IP caused by the beam based feedback until the final doublet

%electron
pos_bff2_x = placet_element_get_attribute('electron', index_d2od_bff, 'x');
pos_bff2_y = placet_element_get_attribute('electron', index_d2od_bff, 'y');
% bds beampar is saved referenced to the beamline position (not angle)
dummy_string = strcat('read_array ','electron_bds_beampar_',num2str(time_step_index)); 
Tcl_Eval(dummy_string);
dummy_string = strcat('electron_bds_beampar_',num2str(time_step_index),'(x)');
beam_fd_x = str2num(Tcl_GetVar(dummy_string)) + pos_bff2_x;
dummy_string = strcat('electron_bds_beampar_',num2str(time_step_index),'(xp)'); 
beam_fd_xp = str2num(Tcl_GetVar(dummy_string));
dummy_string = strcat('electron_bds_beampar_',num2str(time_step_index),'(y)'); 
beam_fd_y = str2num(Tcl_GetVar(dummy_string)) + pos_bff2_y;
dummy_string = strcat('electron_bds_beampar_',num2str(time_step_index),'(yp)'); 
beam_fd_yp = str2num(Tcl_GetVar(dummy_string));
dummy_string = strcat('electron_bds_beampar_',num2str(time_step_index),'(e)'); 
beam_fd_E = str2num(Tcl_GetVar(dummy_string));

% old positions of QD0 and QF1 are used

[offset_elec_BBFB_IP_x, offset_elec_BBFB_IP_y] = FD_model(beam_fd_x,beam_fd_xp,beam_fd_y, ...
                                                  beam_fd_yp,QF1_offset_elec_prev,QD0_offset_elec_prev,beam_fd_E,1,0);

% expected change in electron beam position
IP_elec_BBFB_incr = offset_elec_BBFB_IP_y - offset_elec_BBFB_IP_y_prev;

offset_elec_BBFB_IP_y_prev=offset_elec_BBFB_IP_y;

%positron
pos_bff2_x = placet_element_get_attribute('positron', index_d2od_bff, 'x');
pos_bff2_y = placet_element_get_attribute('positron', index_d2od_bff, 'y');
dummy_string = strcat('read_array ','positron_bds_beampar_',num2str(time_step_index)); 
Tcl_Eval(dummy_string);
dummy_string = strcat('positron_bds_beampar_',num2str(time_step_index),'(x)'); 
beam_fd_x = str2num(Tcl_GetVar(dummy_string)) + pos_bff2_x;
dummy_string = strcat('positron_bds_beampar_',num2str(time_step_index),'(xp)'); 
beam_fd_xp = str2num(Tcl_GetVar(dummy_string));
dummy_string = strcat('positron_bds_beampar_',num2str(time_step_index),'(y)'); 
beam_fd_y = str2num(Tcl_GetVar(dummy_string)) + pos_bff2_y;
dummy_string = strcat('positron_bds_beampar_',num2str(time_step_index),'(yp)'); 
beam_fd_yp = str2num(Tcl_GetVar(dummy_string));
dummy_string = strcat('positron_bds_beampar_',num2str(time_step_index),'(e)'); 
beam_fd_E = str2num(Tcl_GetVar(dummy_string));

[offset_posi_BBFB_IP_x, offset_posi_BBFB_IP_y] = FD_model(beam_fd_x,beam_fd_xp,beam_fd_y, ...
                                                  beam_fd_yp,QF1_offset_posi_prev,QD0_offset_posi_prev,beam_fd_E,1,0);

IP_posi_BBFB_incr = offset_posi_BBFB_IP_y - offset_posi_BBFB_IP_y_prev;

% expected change in relative beam offset due to BBFB
IP_BBFB = offset_posi_BBFB_IP_y - offset_elec_BBFB_IP_y;
IP_BBFB_incr = IP_posi_BBFB_incr - IP_elec_BBFB_incr;

offset_posi_BBFB_IP_y_prev=offset_posi_BBFB_IP_y;

QF1_offset_elec_prev = placet_element_get_attribute('electron',index_qf1,'y');
QD0_offset_elec_prev = placet_element_get_attribute('electron',index_qd0,'y');
QF1_offset_posi_prev = placet_element_get_attribute('positron',index_qf1,'y');
QD0_offset_posi_prev = placet_element_get_attribute('positron',index_qd0,'y');

%%%%%%%%%%%%%%%
% IP Feedback %
%%%%%%%%%%%%%%%

if(use_beam_beam == 1) % That is the line where this stupid comment is comming from
    if(use_ip_feedback == 1)
    
    if(bpm_ip_noise == 1)
      % Angle in micro-rad
      % Formular is a first order approximation of the formular
      %   angle = arcsin((dist*sin(angle)+bpm_res)/dist)
      Anglex = Anglex + bpm_ip_res*randn(1,1)/bpm_ip_dist;
      Angley = Angley + bpm_ip_res*randn(1,1)/bpm_ip_dist;
    end

    OBS = [ Anglex; Angley ];

    volt_ipdipolex_m = [-20;-18;-16;-14;-12;-10;-8;-6;-5;-4;-3;-2;-1;0.0;1;2;3;4;5;6;8;10;12;14;16;18;20;22];
    volt_ipdipoley_m = [-2.0;-1.8;-1.6;-1.4;-1.2;-1.0;-0.8;-0.6;-0.5;-0.4;-0.3;-0.2;-0.1;0.0;0.1;0.2;0.3;0.4;0.5;0.6;0.8;1.0;1.2;1.4;1.6;1.8;2.0;2.2];
    anglex_m = [1e+05;9.08881900e+01;8.15702085e+01;7.38867630e+01;6.40323120e+01;5.74753835e+01;4.61513835e+01;3.45757075e+01;2.84579875e+01;2.32518140e+01;1.82468840e+01;1.23629895e+01;5.96171750e+00;0.0;-5.97734150e+00;-1.15559740e+01;-1.84385900e+01;-2.28725905e+01;-2.84784070e+01;-3.38430380e+01;-4.54901485e+01;-5.51351440e+01;-6.63522710e+01;-7.44190065e+01;-8.21657255e+01;-8.83701195e+01;-9.66601100e+01;-1e+05];
    angley_m = [1e+05;8.83471590e+01;8.83471590e+01;7.37509280e+01;6.52505075e+01;5.71941275e+01;4.79114320e+01;3.77751415e+01;3.29921015e+01;2.77325505e+01;2.17344410e+01;1.49464875e+01;7.48450700e+00;0.0;-7.45456350e+00;-1.50889565e+01;-2.16163065e+01;-2.76105855e+01;-3.28402695e+01;-3.81607225e+01;-4.80060980e+01;-5.69495650e+01;-6.57891865e+01;-7.35740490e+01;-8.08862530e+01;-8.77917365e+01;-9.47486510e+01;-1e+05];
    
    anglex_volt_ipdipole = interp1(anglex_m,volt_ipdipolex_m,Anglex);
    angley_volt_ipdipole = interp1(angley_m,volt_ipdipoley_m,Angley);

    if (use_ip_feedback_linear == 1)

      % straight line interpolation
      volt_ipdipole_x = -0.5*anglex_volt_ipdipole;
      volt_ipdipole_y = -0.5*angley_volt_ipdipole;

      corr_increment_ipdipole = [volt_ipdipole_x, -volt_ipdipole_x; volt_ipdipole_y, -volt_ipdipole_y];
    elseif (use_ip_feedback_pid == 1)
      gain_p = 0.3;
      gain_d = 0.5;
      volt_ipdipole_y_new = volt_ipdipole_y + gain_p*angley_volt_ipdipole + (volt_ipdipole_y - volt_ipdipole_y_prev) + gain_d*(angley_volt_ipdipole-angley_volt_ipdipole_prev);
      volt_ipdipole_y_prev = volt_ipdipole_y;
      volt_ipdipole_y = volt_ipdipole_y_new;
      angley_volt_ipdipole_prev = angley_volt_ipdipole;
      
      corr_increment_ipdipole = [-0.5*anglex_volt_ipdipole, 0.5*anglex_volt_ipdipole;-0.5*volt_ipdipole_y_new, 0.5*volt_ipdipole_y_new];
    elseif (use_ip_feedback_annecy == 1)
      # Compute next step correction
      IPdipole_strength_positron_x=0;
      
      if (use_defl_angle == 1)
        offset_pos_y = 0.5 * angley_volt_ipdipole*2.3 / 1e9 + white_noise(time_step_index+1)*1e6;
        offset_ele_y = -0.5 *  angley_volt_ipdipole*2.3 / 1e9;
      elseif
        offset_pos_y = posy + white_noise(time_step_index+1)*1e6; 		% Add a white noise [µm]
        offset_ele_y = eley;
      end

      if (external_ground_cms==0)
        switch (ip_feedback_annecy_choice)
         case 0
          corr_offset = feedbpm_PLACET_FB_preisolator(offset_pos_y*1e-6,offset_ele_y*1e-6,1); 		% posy: [µm]to [m]
         case 1
          corr_offset = feedbpm_PLACET_FBA_preisolator(offset_pos_y*1e-6,offset_ele_y*1e-6,1); 		% posy: [µm]to [m]
         case 2
          corr_offset = feedbpm_PLACET_PID_preisolator(offset_pos_y*1e-6,offset_ele_y*1e-6,1); 		% posy: [µm]to [m]
         case 3
          corr_offset = feedbpm_PLACET_PIDA_preisolator(offset_pos_y*1e-6,offset_ele_y*1e-6,1); 		% posy: [µm]to [m]
         otherwise
          disp("fbpm feedback not known!!")
        end
      else
        switch (ip_feedback_annecy_choice)
         case 0
          corr_offset = feedbpm_CMS_FB_preisolator(offset_pos_y*1e-6,offset_ele_y*1e-6,1); 		% posy: [µm]to [m]
         case 1
          corr_offset = feedbpm_CMS_FBA_preisolator(offset_pos_y*1e-6,offset_ele_y*1e-6,1); 		% posy: [µm]to [m]
         case 2
          corr_offset = feedbpm_CMS_PID_preisolator(offset_pos_y*1e-6,offset_ele_y*1e-6,1); 		% posy: [µm]to [m]
         case 3
          corr_offset = feedbpm_CMS_PIDA_preisolator(offset_pos_y*1e-6,offset_ele_y*1e-6,1); 		% posy: [µm]to [m]
         otherwise
          disp("fbpm feedback not known!!")
        end
      end

      IPdipole_strength_positron_y=-corr_offset*(1e9)/2.3 ; 	% feedbpm_preisolator [m] to [nm] 
      
      IPdipole_strength_electron_x=0;
      IPdipole_strength_electron_y=0;

    end
  end
end
