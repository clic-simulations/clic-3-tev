% Script to create the matrices needed for the orbit feedback
%
% Juergen Pfingstner

if(response_matrix_calc == 1)
  line_name = 'electron';
  beam_name = 'beam_electron';
  
  % Recalculate the response matrix (extremely heavy task)
  
  step_size_response = 0.4; % to be evaluated
  
  % Take care: We mainly want to calculate complete response
  % matrices. And for sure, big Rs should not be overwitten due to 
  % oversights. So at the moment we stop here if not the main linac
  % and the BDS are set up. This can be easily changed if needed,
  % but it should not be necessary and you better be sure what you
  % do.
  
  if( (use_main_linac == 0) | (use_bds == 0) )
    fprintf(1, 'Both MainLinac and BDS have to be activated for ');
    fprintf(1, 'response matrix calculation\n');
    fprintf(1, '      -> Simulation stopped\n\n\n');
    exit;
  end

  if(dipole_correctors == 1)

    [R0_x U0_x s0_x V0_x R0_y U0_y s0_y V0_y] = ...
	calc_matrices_dipole(use_main_linac, use_bds, nr_corr_ml(electron), ...
			     line_name, beam_name, bpm_index(electron,:), corr_index(electron,:), corr_center_x(electron,:), corr_center_y(electron,:), step_size_response);
  
  else
    [R0_x U0_x s0_x V0_x R0_y U0_y s0_y V0_y] = ...
	calc_matrices_qp(use_main_linac, use_bds, nr_corr_ml(electron), ...
			 line_name, beam_name, bpm_index(electron,:), corr_index(electron,:), step_size_response);
  end
  exit;

else
  
  % Load and cut the matrices
  
  fprintf(1, 'Load the needed matrices ... ');
  
  if(use_controller_x == 1)
  
      if(use_noisy_matrix_x == 1)
	  [R0_x U0_x s0_x V0_x] = load_R_noisy(R_file_name_x, use_main_linac, use_bds, nr_corr_ml(electron,:), use_bpm_scaling);
      else
	  if(dipole_correctors == 1)
	      [R0_x U0_x s0_x V0_x] = load_R_x_dipole(use_main_linac, use_bds, nr_corr_ml(electron,:), use_bpm_scaling);
	  else      
	      [R0_x U0_x s0_x V0_x] = load_R_x_qp(use_main_linac, use_bds, nr_corr_ml(electron,:), use_bpm_scaling);
	  end
      end
    
  end
    
  if(use_controller_y == 1)

      if(use_noisy_matrix_y)
	  [R0_y U0_y s0_y V0_y] = load_R_noisy(R_file_name_y, use_main_linac, use_bds, nr_corr_ml(electron,:), use_bpm_scaling);
      else
	  if(dipole_correctors == 1)
	      [R0_y U0_y s0_y V0_y] = load_R_y_dipole(use_main_linac, use_bds, nr_corr_ml(electron,:), use_bpm_scaling);
	  else
	      [R0_y U0_y s0_y V0_y] = load_R_y_qp(use_main_linac, use_bds, nr_corr_ml(electron,:), use_bpm_scaling);
	  end
      end
    
  end
  fprintf(1, 'finished\n');
end
