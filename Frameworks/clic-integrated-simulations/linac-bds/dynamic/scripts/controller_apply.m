% Script to apply the calculated controller values to the actuators
%
% Juergen Pfingstner
% 30th of September 2010

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Beam-Based MIMO Feedback %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%


if( (use_main_linac == 1) | (use_bds == 1))
  for i=electron:positron
    line_name = beamlinename(i,:);
    if(dipole_correctors == 1)
      % Use the dipole correctors
      corr_voltage = kick2volt(corr_values_new_x(i,:), corr_center_x(:,i));
      placet_element_set_attribute(line_name, corr_index(i,:), 'strength_x', corr_voltage);
      corr_voltage = kick2volt(corr_values_new_y(:,i), corr_center_y(:,i));
      placet_element_set_attribute(line_name, corr_index(i,:), 'strength_y', corr_voltage);
      
    else
      % Use the qp correctors
      
      % QP x
      corr_temp = placet_element_get_attribute(line_name, corr_index(i,:), 'x');
      corr_temp = corr_temp' - corr_values_old_x(:,i) + corr_values_new_x(:,i);
      placet_element_set_attribute(line_name, corr_index(i,:), 'x', corr_temp);
      % BPM x: move the BPM center back by the feedback increment
      bpm_temp = placet_element_get_attribute(line_name, bpm_move_index(i,:), 'x');
      bpm_temp = bpm_temp' - corr_values_old_x(2:end,i) + corr_values_new_x(2:end,i) ...
          - corr_increment_x(2:end,i);
      placet_element_set_attribute(line_name, bpm_move_index(i,:), 'x', bpm_temp);
      
      % QP y
      corr_temp = placet_element_get_attribute(line_name, corr_index(i,:), 'y');
      corr_temp = corr_temp' - corr_values_old_y(:,i) + corr_values_new_y(:,i);
      placet_element_set_attribute(line_name, corr_index(i,:), 'y', corr_temp);
      % BPM y: move the BPM center back by the feedback increment
      bpm_temp = placet_element_get_attribute(line_name, bpm_move_index(i,:), 'y');
      bpm_temp = bpm_temp' - corr_values_old_y(2:end,i) + corr_values_new_y(2:end,i) ...
          - corr_increment_y(2:end,i);
      placet_element_set_attribute(line_name, bpm_move_index(i,:), 'y', bpm_temp);
      
    end
    
    corr_values_old_x(:,i) = corr_values_new_x(:,i);
    corr_values_old_y(:,i) = corr_values_new_y(:,i);
  end    
end

%%%%%%%%%%%%%%%
% IP Feedback %
%%%%%%%%%%%%%%%

if(use_beam_beam == 1)
  if(use_ip_feedback == 1)

    % Here we vary the strength of the IPDIPOLE 
    if(use_ip_feedback_x == 1)
      for i=electron:positron
        line_name = beamlinename(i,:);
        placet_element_vary_attribute(line_name, ipdipole_index(i,:), "strength_x", gain_bb_x*corr_increment_ipdipole(x,i));
      end
    end
    if(use_ip_feedback_y == 1)
      if (use_ip_feedback_linear)
        for i=electron:positron
          line_name = beamlinename(i,:);
          placet_element_vary_attribute(line_name, ipdipole_index(i,:), ...
                                        "strength_y", gain_bb_y*corr_increment_ipdipole(y,i));
        end
      elseif (use_ip_feedback_pid)
        for i=electron:positron
          line_name = beamlinename(i,:);
          placet_element_set_attribute(line_name, ipdipole_index(i,:), ...
                                       "strength_y", corr_increment_ipdipole(y,i));
        end
      elseif (use_ip_feedback_annecy)
        placet_element_set_attribute("electron", ipdipole_index(electron,:), "strength_y", IPdipole_strength_electron_y);
        placet_element_set_attribute("positron", ipdipole_index(positron,:), "strength_y", -IPdipole_strength_positron_y);
      end
    end
    
    corr_setting_ip_x = placet_element_get_attribute("electron", ipdipole_index(electron,:), "strength_x");
    corr_setting_ip_y = placet_element_get_attribute("electron", ipdipole_index(electron,:), "strength_y");
    
    if (debug)
      str_ex=placet_element_get_attribute("electron", ipdipole_index, "strength_x");
      str_ey=placet_element_get_attribute("electron", ipdipole_index, "strength_y");
      str_px=placet_element_get_attribute("positron", ipdipole_index, "strength_x");
      str_py=placet_element_get_attribute("positron", ipdipole_index, "strength_y");
      disp("IPDipole strengths:");
      disp(str_ex)
      disp(str_ey)
      disp(str_px)
      disp(str_py)
    end
    % Latency time 
    %time_laten = time_laten + ${latency};
  else
    corr_setting_ip_x = 0;
    corr_setting_ip_y = 0;
  end
end
