% Function to calculate the response matrix of the dipoles.
%
% Remark: Based on the function placet_get_resonse_matrix.m 
%
% Juergen Pfingstner
% 29. September 2010

function [R_dipole] = calc_dipole_response_matrix(line_name, beam_name, bpm_index, corr_index, corr_pos, step_size, direction)
%
% direction: 1 ... y
%            0 ... x
%

R_dipole = zeros(length(bpm_index), length(corr_index));
placet_test_no_correction(line_name, beam_name, 'None');
B=placet_get_bpm_readings(line_name, bpm_index, true);
if(direction == 1)
  % y direction
  b0 = B(:,2);
  corr0 = placet_element_get_attribute(line_name, corr_index, 'strength_y');
else
  % x direction
  b0 = B(:,1);
  corr0 = placet_element_get_attribute(line_name, corr_index, 'strength_x');
end

for i=1:length(corr_index)
  if(direction == 1)
    step_volt = kick2volt(step_size, corr_pos(i));
    placet_element_set_attribute(line_name, corr_index(i), 'strength_y', corr0(i) + step_volt);
    placet_test_no_correction(line_name, beam_name, 'None');
    placet_element_set_attribute(line_name, corr_index(i), 'strength_y', corr0(i));
    B=placet_get_bpm_readings(line_name, bpm_index, true);
    b1 = B(:,2);
  else
    step_volt = kick2volt(step_size, corr_pos(i));
    placet_element_set_attribute(line_name, corr_index(i), 'strength_x', corr0(i) + step_volt);
    placet_test_no_correction(line_name, beam_name, 'None');
    placet_element_set_attribute(line_name, corr_index(i), 'strength_x', corr0(i));
    B=placet_get_bpm_readings(line_name, bpm_index, true);
    b1 = B(:,1);
  end
  R_dipole(:,i)=(b1-b0);
end

R_dipole = R_dipole ./ step_size; 


  
