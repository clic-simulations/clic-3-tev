% Function ot load the response matrix of the dipoles for the x
% direction.
%
% Juergen Pfingstner
% 26th of October 2010

function [R0_x U0_x s0_x V0_x R0_y U0_y s0_y V0_y] = ...
    calc_matrices_qp(use_main_linac, use_bds, nr_corr_ml, ...
		     line_name, beam_name, bpm_index, corr_index, step_size)

fprintf(1, 'Calculate the response matrix for x (take a day off) ... ');
[R0_qp_x] = calc_qp_response_matrix(line_name, beam_name, ...
				    bpm_index, corr_index, step_size, 0);
fprintf(1, 'finished\n');
save -ascii "R0_qp_x.dat" R0_qp_x;
    
% Perform the SVD x

fprintf(1, 'Perform the SVD ... ');
    
[U0_qp_x S0_qp_x V0_qp_x] = svd(R0_qp_x);
s0_qp_x = diag(S0_qp_x);

[U0_qp_ml_x S0_qp_ml_x V0_qp_ml_x] = svd(R0_qp_x(1:nr_corr_ml,1:nr_corr_ml));
s0_qp_ml_x = diag(S0_qp_ml_x);
    
[U0_qp_bds_x S0_qp_bds_x V0_qp_bds_x] = svd(R0_qp_x((nr_corr_ml+1):end,(nr_corr_ml+1):end));
s0_qp_bds_x = diag(S0_qp_bds_x);

fprintf(1, 'finished\n');
  
% Store the results x

fprintf(1, 'Store the matrices ... ');

save -ascii "U0_qp_x.dat" U0_qp_x;
save -ascii "s0_qp_x.dat" s0_qp_x;
save -ascii "V0_qp_x.dat" V0_qp_x;

save -ascii "U0_qp_ml_x.dat" U0_qp_ml_x;
save -ascii "s0_qp_ml_x.dat" s0_qp_ml_x;
save -ascii "V0_qp_ml_x.dat" V0_qp_ml_x;

save -ascii "U0_qp_bds_x.dat" U0_qp_bds_x;
save -ascii "s0_qp_bds_x.dat" s0_qp_bds_x;
save -ascii "V0_qp_bds_x.dat" V0_qp_bds_x;

fprintf(1, 'finished\n');
    
if( ( use_main_linac == 1 ) & ( use_bds == 0 ) )
  R0_x = R0_qp_x(1:nr_corr_ml, 1:nr_corr_ml);
  U0_x = U0_qp_ml_x;
  s0_x = s0_qp_ml_x;
  V0_x = V0_qp_ml_x;
elseif( ( use_main_linac == 0 ) & ( use_bds == 1 ) )
  R0_x = R0_qp_x((nr_corr_ml+1):end, (nr_corr_ml+1):end);
  U0_x = U0_qp_bds_x;
  s0_x = s0_qp_bds_x;
  V0_x = V0_qp_bds_x;
elseif( ( use_main_linac == 1 ) & ( use_bds == 1 ) )
  R0_x = R0_qp_x;
  U0_x = U0_qp_x;
  s0_x = s0_qp_x;
  V0_x = V0_qp_x;
end
    
clear R0_qp_x;
clear U0_qp_x S0_qp_x s0_qp_x V0_qp_x;
clear U0_qp_ml_x S0_qp_ml_x s0_qp_ml_x V0_qp_ml_x;
clear U0_qp_bds_x S0_qp_bds_x s0_qp_bds_x V0_qp_bds_x;
    
% Calculate the QP response matrix y
    
fprintf(1, 'Calculate the response matrix for y (take a day off) ... ');
[R0_qp_y] = calc_qp_response_matrix(line_name, beam_name, ...
				    bpm_index, corr_index, step_size, 1);
fprintf(1, 'finished\n');
save -ascii "R0_qp_y.dat" R0_qp_y;
    
% Perform the SVD y

fprintf(1, 'Perform the SVD for y ... ');
    
[U0_qp_y S0_qp_y V0_qp_y] = svd(R0_qp_y);
s0_qp_y = diag(S0_qp_y);
    
[U0_qp_ml_y S0_qp_ml_y V0_qp_ml_y] = svd(R0_qp_y(1:nr_corr_ml, 1:nr_corr_ml));
s0_qp_ml_y = diag(S0_qp_ml_y);
    
[U0_qp_bds_y S0_qp_bds_y V0_qp_bds_y] = svd(R0_qp_y((nr_corr_ml+1):end, (nr_corr_ml+1):end));
s0_qp_bds_y = diag(S0_qp_bds_y);
    
fprintf(1, 'finished\n');
  
% Store the results y
    
fprintf(1, 'Store the matrices for y ... ');
    
save -ascii "U0_qp_y.dat" U0_qp_y;
save -ascii "s0_qp_y.dat" s0_qp_y;
save -ascii "V0_qp_y.dat" V0_qp_y;

save -ascii "U0_qp_ml_y.dat" U0_qp_ml_y;
save -ascii "s0_qp_ml_y.dat" s0_qp_ml_y;
save -ascii "V0_qp_ml_y.dat" V0_qp_ml_y;

save -ascii "U0_qp_bds_y.dat" U0_qp_bds_y;
save -ascii "s0_qp_bds_y.dat" s0_qp_bds_y;
save -ascii "V0_qp_bds_y.dat" V0_qp_bds_y;

fprintf(1, 'finished\n');
    
if( ( use_main_linac == 1 ) & ( use_bds == 0 ) )
  R0_y = R0_qp_y(1:nr_corr_ml, 1:nr_corr_ml);
  U0_y = U0_qp_ml_y;
  s0_y = s0_qp_ml_y;
  V0_y = V0_qp_ml_y;
elseif( ( use_main_linac == 0 ) & ( use_bds == 1 ) )
  R0_y = R0_qp_y((nr_corr_ml+1):end, (nr_corr_ml+1):end);
  U0_y = U0_qp_bds_y;
  s0_y = s0_qp_bds_y;
  V0_y = V0_qp_bds_y;
elseif( ( use_main_linac == 1 ) & ( use_bds == 1 ) )
  R0_y = R0_qp_y;
  U0_y = U0_qp_y;
  s0_y = s0_qp_y;
  V0_y = V0_qp_y;
end
    
clear R0_qp_y;
clear U0_qp_y S0_qp_y s0_qp_y V0_qp_y;
clear R0_qp_ml_y U0_qp_ml_y S0_qp_ml_y s0_qp_ml_y V0_qp_ml_y;
clear R0_qp_bds_y U0_qp_bds_y S0_qp_bds_y s0_qp_bds_y V0_qp_bds_y;
