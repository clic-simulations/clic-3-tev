source $script_dir/scripts/tcl_procs.tcl

proc save_nominal {name beamlinename zero_align} {
    # Save nominal beam and bpm readings
    global target_x target_y machine_index use_ip_feedback
    BeamlineUse -name $beamlinename
    BeamLoadAll -beam beam_$name -file ${name}_linac_${machine_index}.dat ;# load linac beam
    Octave {
	if ($zero_align)
	[E, Beam_$name]=placet_test_no_correction("$beamlinename", "beam_$name", "Zero");
	else 
	[E, Beam_$name]=placet_test_no_correction("$beamlinename", "beam_$name", "None");
	endif
	save_beam("nom_$name.dat", Beam_$name);
	
	# Save nominal BPM readings (exact readings)
	bpms = placet_get_number_list("$beamlinename", "bpm");

	# Don't use last bpm in case of separate ip feedback
	if ($use_ip_feedback)
	bpms = bpms(1:end-1);
	endif

	${beamlinename}_nom_readings = placet_get_bpm_readings("$beamlinename",bpms,true);
	nom_read = ${beamlinename}_nom_readings;
	save -text bpm_nom_reading_${beamlinename}_${machine_index}.out nom_read;
    }
    set target_y($beamlinename) [read_bpm_y]
    set target_x($beamlinename) [read_bpm_x]
    print_total_nom_bpmreadings $beamlinename
}

proc beamloadall { } {
    global time_step_index name
    puts "BeamLoadAll $name"
    BeamLoadAll -beam beam_$name -file ${name}_linac_${time_step_index}.dat
}

proc figure_of_merit {name} {
    global target_y target_x
    set bpmy [read_bpm_y]
    set bpmx [read_bpm_x]
    set resy 0.0
    set resx 0.0
    #set f [open bpm.$name.$k.$ii w]
    foreach by $bpmy bx $bpmx ty $target_y($name) tx $target_x($name) {
	set resy [expr $resy+pow(($by-$ty),2)]
	set resx [expr $resx+pow(($bx-$tx),2)]
	#puts $f "[expr $bx-$tx] [expr $by-$ty] $tx $ty"
    }
    #close $f
    puts "Figure of Merit (x,y) $name: $resx $resy"
    set residual [list $resx $resy]
    return $residual
}

proc my_survey { } {
    global params ;#bpm_error
    set bpm_error 0

    set cav0 [CavityGetPhaseList]
    set grad0 [CavityGetGradientList]
    set bpm_res $bpm_error
#    set jitter 0.1
    set phase_err 0.0
    SurveyErrorSet -quadrupole_y $params(quad_misalign) \
	-bpm_y 0.0 \
	-cavity_y 0.0 \
	-cavity_realign_y 0.0 \
	-cavity_yp 0.0

    # misalign beamline according to survey
    Clic
    set cav {}
    #set grad {}
    #set fcav [open cav.dat.$i w]
    foreach c $cav0 g $grad0 {
	set cc [expr $c+[gcav]*$phase_err]
	#set gg [expr $g+[gcav]*$gradient_err]
	#puts $fcav "$gg $cc"
	lappend cav $cc
	#lappend grad $gg
    }
    # close $fcav
    CavitySetPhaseList $cav
    #CavitySetGradientList $grad
    #SaveAllPositions -vertical_only 0 -binary 0 -cav 1 -nodrift 1 -file buc.$i
    puts "misalignment done"    
}

proc track_no_correction {name beamlinename} {
    global response_matrix_calc use_beam_beam
    BeamlineUse -name $beamlinename
    #TestNoCorrection -beam beam_$name -emitt_file emitt_lin$name.$machine_index.dat -survey my_survey -machines 1

    Octave {
	# track with SVD correction
	[E, Beam_$name] = placet_test_no_correction("$beamlinename", "beam_$name", "None");
	if (($response_matrix_calc==0) & ($use_beam_beam!=0))
	  save_beam("$name.ini", Beam_$name);
	endif
	# store new bpm readings
	#$name.bsignal1=placet_get_bpm_readings("bds$name",bpms$name,true);
	#b1=$name.bsignal1;
	#save -text bpm_reading1_${name}_${ii}.out b1;
    }
}

proc print_total_nom_bpmreadings {name} {
    global target_y target_x
    set resy 0.0
    set resx 0.0
    foreach ty $target_y($name) tx $target_x($name) {
	set resy [expr $resy+pow($ty,2)]
	set resx [expr $resx+pow($tx,2)]
    }
    puts "nom_bpmreadings (x,y) $name: $resx $resy"
}

proc read_bpm_y {} {
    set bpm [BpmReadings -correct 1]
    set res ""
    foreach x $bpm {
	lappend res [lindex $x 2]
    }
    return $res
}

proc read_bpm_x {} {
    set bpm [BpmReadings -correct 1]
    set res ""
    foreach x $bpm {
	lappend res [lindex $x 1]
    }
    return $res
}

proc add_initial_jitter_electron { } {
    Octave {
	
	% Get the beam
	Be = placet_get_beam();
	twiss_matrix0 = placet_get_twiss_matrix(Be);

	% Modify the beam
	
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	% Initial beam position and angle jitter %
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

	if(initial_imperfection_type_x == 1)
	  beta_x0 = twiss_matrix0(1,1);
	  alpha_x0 = -twiss_matrix0(1,2);
	  action_x = initial_jitter_x^2/beta_x0;
	  phi_x = 2*pi*rand(1,1);
	  jitter_x = sqrt(2*action_x*beta_x0)*cos(phi_x);
	  jitter_xp = -sqrt(2*action_x/beta_x0)*(alpha_x0*cos(phi_x)+sin(phi_x));
	  fprintf(1,'Offset electron x: %g, xp: %g\n', jitter_x, jitter_xp);
	  Be(:,4) = Be(:,4) + jitter_x;
	  Be(:,5) = Be(:,5) + jitter_xp;
	end	
	
	if(initial_imperfection_type_y == 1)
	  beta_y0 = twiss_matrix0(3,3);
	  alpha_y0 = -twiss_matrix0(3,4);
	  action_y = initial_jitter_y^2/beta_y0;
	  phi_y = 2*pi*rand(1,1);
	  jitter_y = sqrt(2*action_y*beta_y0)*cos(phi_y);
	  jitter_yp = -sqrt(2*action_y/beta_y0)*(alpha_y0*cos(phi_y)+sin(phi_y));
	  fprintf(1,'Offset electron y: %g, yp: %g\n', jitter_y, jitter_yp);
	  Be(:,6) = Be(:,6) + jitter_y;
	  Be(:,7) = Be(:,7) + jitter_yp;
	end	

	%%%%%%%%%%%%%%%%%%%%%%%%%
	% Initial energy jitter %
	%%%%%%%%%%%%%%%%%%%%%%%%%

	if((initial_imperfection_type_x == 2) | (initial_imperfection_type_y == 2))
	  jitter_val_energy = initial_energy_jitter*randn(1,1);
	  fprintf(1,'Offset positron energy: %d\n', jitter_val_energy);
	  Be(:,3) = Be(:,3) + jitter_val_energy;
	end	

	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	% Simple offset of the beam at the entrance of the ML by 1 mircrometre %
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

	if(initial_imperfection_type_x == 11)
          % Simple offset to evaluate the offset trajectory
	  Be(:,4) = Be(:,4) + ones(size(Be(:,4))); % offset
	end
	
	if(initial_imperfection_type_y == 11)
          % Simple offset to evaluate the offset trajectory
	  Be(:,6) = Be(:,6) + ones(size(Be(:,6))); % offset
 	end
	
	% Get the beam to placet again
	placet_set_beam(Be);
    }
}

proc add_initial_jitter_positron { } {
    Octave {

	% Get the beam
	Be = placet_get_beam();
	twiss_matrix0 = placet_get_twiss_matrix(Be);

	% Modify the beam -> take care it is a 2nd order beam

	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	% Initial beam position and angle jitter %
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

	if(initial_imperfection_type_x == 1)
	  beta_x0 = twiss_matrix0(1,1);
	  alpha_x0 = -twiss_matrix0(1,2);
	  action_x = initial_jitter_x^2/beta_x0;
	  phi_x = 2*pi*rand(1,1);
	  jitter_x = sqrt(2*action_x*beta_x0)*cos(phi_x);
	  jitter_xp = -sqrt(2*action_x/beta_x0)*(alpha_x0*cos(phi_x)+sin(phi_x));
	  fprintf(1,'Offset electron x: %g, xp: %g\n', jitter_x, jitter_xp);
	  Be(:,4) = Be(:,4) + jitter_x;
	  Be(:,5) = Be(:,5) + jitter_xp;
	end	
	
	if(initial_imperfection_type_y == 1)
	  beta_y0 = twiss_matrix0(3,3);
	  alpha_y0 = -twiss_matrix0(3,4);
	  action_y = initial_jitter_y^2/beta_y0;
	  phi_y = 2*pi*rand(1,1);
	  jitter_y = sqrt(2*action_y*beta_y0)*cos(phi_y);
	  jitter_yp = -sqrt(2*action_y/beta_y0)*(alpha_y0*cos(phi_y)+sin(phi_y));
	  fprintf(1,'Offset electron y: %g, yp: %g\n', jitter_y, jitter_yp);
	  Be(:,6) = Be(:,6) + jitter_y;
	  Be(:,7) = Be(:,7) + jitter_yp;
	end

	%%%%%%%%%%%%%%%%%%%%%%%%%
	% Initial energy jitter %
	%%%%%%%%%%%%%%%%%%%%%%%%%

	if((initial_imperfection_type_x == 2) | (initial_imperfection_type_y == 2))
	  jitter_val_energy = initial_energy_jitter*randn(1,1);
	  fprintf(1,'Offset positron energy: %d\n', jitter_val_energy);
	  Be(:,3) = Be(:,3) + jitter_val_energy;
	end	
	

	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	% Simple offset of the beam at the entrance of the ML by 1 mircrometre %
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

	if(initial_imperfection_type_x == 11)
          % Simple offset to evaluate the offset trajectory
	  Be(:,4) = Be(:,4) + ones(size(Be(:,4))); % offset
	end	
	
	if(initial_imperfection_type_y == 11)
          % Simple offset to evaluate the offset trajectory
	  Be(:,6) = Be(:,6) + ones(size(Be(:,6))); % offset
	end
	
	% Get the beam to placet again
	placet_set_beam(Be);
    }
}

proc save_beam_linpositron { } {
    global time_step_index debug
    if {$debug} {puts "positron beam after linac"}
    BeamSaveAll -file positron_linac_$time_step_index.dat 
    array set beamparams [BeamMeasure]
    save_array beamparams "positron_linac_beampar_$time_step_index"
    if {$debug} {print_beam_params beamparams}
}

proc save_beam_linelectron { } {
    global time_step_index debug
    if {$debug} {puts "electron beam after linac"}
    BeamSaveAll -file electron_linac_$time_step_index.dat 
    array set beamparams [BeamMeasure]
    save_array beamparams "electron_linac_beampar_$time_step_index"
    if {$debug} {print_beam_params beamparams}
}

proc save_beam_bdspositron { } {
    global time_step_index debug
    if {$debug} {puts "positron beam after bds"}
    BeamDump -file positron_bds_$time_step_index.dat
    array set beamparams [BeamMeasure]
    save_array beamparams "positron_bds_beampar_$time_step_index"
    if {$debug} {print_beam_params beamparams}
}

proc save_beam_bdselectron { } {
    global time_step_index debug
    if {$debug} {puts "electron beam after bds"}
    BeamDump -file electron_bds_$time_step_index.dat
    array set beamparams [BeamMeasure]
    save_array beamparams "electron_bds_beampar_$time_step_index"
    if {$debug} {print_beam_params beamparams}
}

proc save_beam_ippositron { } {
    global time_step_index debug
    if {$debug} {puts "positron beam after ip"}
    BeamDump -file positron_ip_$time_step_index.dat
    array set beamparams [BeamMeasure]
    save_array beamparams "positron_ip_beampar_$time_step_index"
    if {$debug} {print_beam_params beamparams}
}

proc save_beam_ipelectron { } {
    global time_step_index debug
    if {$debug} {puts "electron beam after ip"}
    BeamDump -file electron_ip_$time_step_index.dat
    array set beamparams [BeamMeasure]
    save_array beamparams "electron_ip_beampar_$time_step_index"
    if {$debug} {print_beam_params beamparams}
}

proc print_beam_params {{beamparams {}}} {
    upvar $beamparams params
    if {[llength beamparams]<=1} {
	array set params [BeamMeasure]
    }
    set beam_ener $params(e)
    set beam_emix $params(emitt_x)
    set beam_emiy $params(emitt_y)
    set beam_betx $params(beta_x)
    set beam_bety $params(beta_y)
    set beam_alpx $params(alpha_x)
    set beam_alpy $params(alpha_y)
    set beam_s $params(s)
    puts "beam longitudinal position = $beam_s"
    puts "beam energy = $beam_ener"
    puts "beam emix = $beam_emix"
    puts "beam emiy = $beam_emiy"
    puts "beam betax = $beam_betx"
    puts "beam betay = $beam_bety"
    puts "beam alpx = $beam_alpx"
    puts "beam alpy = $beam_alpy"
    set sig_x [expr sqrt($beam_betx*$beam_emix*1e-7/(3.0*1e6))]
    set sig_y [expr sqrt($beam_bety*$beam_emiy*1e-7/(3.0*1e6))]
    puts "beam sigx = $sig_x"
    puts "beam sigy = $sig_y"
}

proc plot_beam {name} {
    global script_dir
    exec root -b -q -l "$script_dir/scripts/beamplot.C(\"$name\")"
}

proc reset_e_initial_linac {} {
    set e_initial $e_initial_linac
    set e0 $e_initial
}

proc reset_e_initial_bds {} {
    set e_initial $e_initial_bds
    set e0 $e_initial
}

proc offset_beam_bdselectron {} {
	#BeamAddOffset -y 0
}
proc offset_beam_bdspositron {} {
	# add offset to beam (um)
    global time_step_index

    Octave {
	gaelground=Temporal_Displacement_Signal_CMS($time_step_index+1)*1e6
	corrector_offset=corr_offset
	Tcl_SetVar("gael_gm",Temporal_Displacement_Signal_CMS($time_step_index+1)*1e6);
    }
	global gael_gm
	BeamAddOffset -y $gael_gm
	puts "add offset to beam manually: $gael_gm"
}
