% Implementation of the second part of the system identification,
% which is everything after the excitation
%
% Juergen Pfingstner
% 23th September 2010

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Take care! Before the code can be used, also the second linac has %
% to be implemented -> to be implemented                            %
% variable have to be dublicated and names have to be changed       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if(identification_switch == 1)

  %%%%%%%%%%%
  % e- part %
  %%%%%%%%%%%
  
  source "../scripts_private/reset_bump.m";
  
  %% Feed the RLS algorithms
  source "../../scripts_private/manage_ident.m"
	  
  % If one excitation cycle is over form the newly estimated
  % matrix and calculate the controller
	  
  if(cycle_index == nr_bumps)
    %if(cycle_index == 1)
	     
    fprintf(1,'Cycle finished\n');
    
    % Update the matrix
    source "../../scripts_private/update_R_hat.m"
	    
    % Recalculate the bumps for the next run
    source "../../scripts_private/calc_excitation_ls.m"
	    
    % Evaluate the results
    source "../../scripts_private/eval_results.m"
	    
    % Update the controller
    if((controller_switch == 1) & (perfect_controller_matrix == 0))
      R_hat_trunc_inv = calc_controller(R_hat, nr_sv);
    end
  end
  
  %%%%%%%%%%%
  % e+ part %
  %%%%%%%%%%%
  
  % To be implemented
  
end
