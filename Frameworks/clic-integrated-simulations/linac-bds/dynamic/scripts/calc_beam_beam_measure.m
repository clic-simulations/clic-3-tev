function beam_beam_measure = calc_beam_beam_measure(elec, posi, offset)
% Function beam_beam_measure calculates the beam measure for a given
% offset.
%
% Input:
%       elec   ... electron beam
%       posi   ... positron beam
%       offset ... offset of the two beams (elec_pos-posi_pos)
%
% Output:
%       beam_beam_measure ... approx. measure of the luminosity
%

% Shift the beams to the wanted position
elec = elec - offset/2;
posi = posi + offset/2;

% Create curves

y = -0.15:0.0001:0.15;
curve_elec= hist(elec, y);
curve_posi= hist(posi, y);

% Debug
%figure(10);
%plot(y, curve_elec, 'b');
%hold on;
%plot(y, curve_posi, 'r');
%hold off;
%keyboard;
% dEBUG

% Calculate value
beam_beam_measure = curve_elec*curve_posi';
end
