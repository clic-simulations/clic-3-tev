
function beam_correction=feedbpm_CMS_FBA_preisolator(y1,y2,Adapt_switch)

%beam_correction=feedbpm(y1,y2,Adapt_switch)
%y1: measure of the position of the beam 1 at the IP
%y2: measure of the position of the beam 2 at the IP
%1= with adaptive control 0 = without

persistent h
persistent ha
persistent bpm
persistent Dim_Den_H
persistent Dim_Num_H
persistent Num_H
persistent Den_H
persistent Adapt_Gain;



%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Initialisation %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if isempty(h)
    	disp('Initialisation of the controller');
	Adapt_Gain=10^8;                        % In order to avoid zero machine face to too small signals at the input of the controller
	
end

y=y1-y2;
y=y*Adapt_Gain;

if isempty(h)
       
%%% Parameters of the non adaptive control
    a1=-2;                       	
    a2=1;
    b0=1.56;							
    b1=-1.44;						
    b2=0.2;				

    Num_H=[b0 b1 b2];						% Num_H contain the parameters of the Numerator of H [b0 b1 ... bn]
    Den_H=[1 a1 a2];						% Den_H contain the parameters of the Denominator of H [a1 a2 ... an]

    Dim_Den_H=max(size(Den_H));				% Order of the denominator of the controller
    Dim_Num_H=max(size(Num_H));				% Order of the numerator of the controller
    
%%% Do not modify after this point

    ha=0;
    bpm(1:max([Dim_Den_H Dim_Num_H]))=y;    % Initial conditions for the controller
    h(1:Dim_Den_H)=y;
    
end

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Beam correction %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

bpm(1)=y;
h(1,2:end)=h(1,1:(end-1));
h(1,1)=0;

    for k=1:Dim_Num_H
        h(1,1)=h(1,1)+Num_H(k)*bpm(k);
    end

    for k=2:Dim_Den_H
        h(1,1)=h(1,1)-Den_H(k)*h(1,k);
    end

    if Adapt_switch==1
        ha=adaptiv_ctrl(-bpm(1),h(1,1));
    end

beam_correction=(-h(1,1)+ha)/Adapt_Gain;
bpm(2:end)=bpm(1:(end-1));
bpm(1)=0;

end                                                     % End of function
