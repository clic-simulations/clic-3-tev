% Script to evaluate and store the results of the last time steps
%
% Jochem Snuverink and Juergen Pfingstner

gm_x = placet_element_get_attribute('electron', corr_index(electron,:), 'x')';
gm_x = gm_x - (corr_settings_new_x(:,electron));
gm_x = sqrt(var(gm_x));
corr_x = sqrt(var(corr_settings_new_x(:,electron)));

gm_y = placet_element_get_attribute('electron', corr_index(electron,:), 'y')';
gm_y = gm_y - corr_settings_new_y(:,electron);
gm_full_y = sqrt(var(gm_y));

if( (use_main_linac == 0) & (use_bds == 1) )
  gm_ml_y = 0;
  gm_bds_y = gm_full_y;
elseif( (use_main_linac == 1) & (use_bds == 0) )
  gm_ml_y = sqrt(var(gm_y(1:nr_corr_ml(electron))));
  gm_bds_y = 0;
elseif( (use_main_linac == 1) & (use_bds == 1) )
  gm_ml_y = sqrt(var(gm_y(1:nr_corr_ml(electron))));
  gm_bds_y = sqrt(var(gm_y(nr_corr_ml(electron)+1:end)));
end

corr_full_y = sqrt(var(corr_settings_new_y(:,electron)));

if( (use_main_linac == 0) & (use_bds == 1) )
  corr_ml_y = 0;
  corr_bds_y = corr_full_y;
elseif( (use_main_linac == 1) & (use_bds == 0) )
  corr_ml_y = sqrt(var(corr_settings_new_y(1:nr_corr_ml(electron),electron)));
  corr_bds_y = 0;
elseif( (use_main_linac == 1) & (use_bds == 1) )
  corr_ml_y = sqrt(var(corr_settings_new_y(1:nr_corr_ml(electron),electron)));
  corr_bds_y = sqrt(var(corr_settings_new_y(nr_corr_ml(electron)+1:end,electron)));
end

if(use_bds == 1)
  % not octdift since that will be in middle of preisolator and rest of machine 
  pos_bff2 = placet_element_get_attribute('electron', index_d2od_bff, 'y');
  pos_ff = placet_element_get_attribute('electron', index_sf1_ff, 'y');
  offset_bff_bff = pos_bff2;
  offset_bff_ff = pos_ff;
else
  offset_bff_bff = 0;
  offset_bff_ff = 0;
end

file_stop_index = time_step_index;
if(sim_mode == 2)
    if( (time_step_index_short - multipulse_nr) < 0)
	%% Not enough points
	file_start_index = file_stop_index - time_step_index_short + 1;
    else
	%% Enough beam data for a multipulse calculation in the current dynamic sim.
	file_start_index = file_stop_index - multipulse_nr + 1;
    end
else
    %% For static and long-term simulations multi-pulse emitance is not calculated,
    %% but set to the single pulse emmitance
    file_start_index = time_step_index;
end
Tcl_SetVar('file_stop_index', file_stop_index);
Tcl_SetVar('file_start_index', file_start_index);

%%%%%%%%%%%%%%%%%%%%%%%%%
% Measurement station 1 %
%%%%%%%%%%%%%%%%%%%%%%%%%

if(use_main_linac == 1)

    if (save_meas_stations == 1)
	%% Load the single pulse results
    
	Tcl_Eval('read_array electron_linac_beampar_$time_step_index');
	Tcl_Eval('export_array_2_octave electron_linac_beampar_$time_step_index');
	
	spe_m1_x = emitt_x_tcl * 100;
	spe_m1_y = emitt_y_tcl * 100;
	
	sxx_m1 = sxx_axis_tcl;
	syy_m1 = syy_axis_tcl;
	szz_m1 = szz_axis_tcl;
    
	sps_m1_x = sqrt(sxx_m1)*1000;
	sps_m1_y = sqrt(syy_m1)*1000;
	%%sps_m1_z = sqrt(szz_m1)*1000;
	sps_m1_z = 0;
	
	disp_m1_x = disp_x_tcl*1000*e_spread_tcl;
	disp_m1_y = disp_y_tcl*1000*e_spread_tcl;
	%%disp_m1_z = disp_z_tcl*1000*e_spread_tcl;
	disp_m1_z = 0;
    
	delta_m1_x = x_tcl*1000;
	delta_m1_y = y_tcl*1000;
	
	e_m1 = e_tcl;
    
	%% Calculate the real multipulse emittance if the time is right 
    
	%% This is not very costy (only 375 particles) and can be done
	%% in every step time
	
	Tcl_Eval('array set mpe_data [calc_emitt_ellipse electron_linac_ $file_start_index $file_stop_index $eval_beam_shift]')
	Tcl_Eval('export_array_2_octave mpe_data');
    
	mpe_m1_x = emitt_x_tcl;
	mpe_m1_y = emitt_y_tcl;
    
	mpe_m1_x_old = mpe_m1_x;
	mpe_m1_y_old = mpe_m1_y;
    
	%% Store the results
    
	fid1 = fopen(meas_station1_file_name, 'at');
	fprintf(fid1, "%d %d %g   %g   %g   %g   %g    ", time_step_index_long, sim_mode, global_time, gm_x, gm_full_y, gm_ml_y, gm_bds_y);
	fprintf(fid1, "%g   %g   %g   %g   ", corr_x, corr_full_y, corr_ml_y, corr_bds_y);
	fprintf(fid1, "%g   %g   %g   %g   ", spe_m1_x, spe_m1_y, mpe_m1_x, mpe_m1_y);
	fprintf(fid1, "%g   %g   %g   %g   %g    ", sps_m1_x, sps_m1_y, sps_m1_z, delta_m1_x, delta_m1_y);
	fprintf(fid1, "%g   %g   %g   ", disp_m1_x, disp_m1_y, disp_m1_z);
	fprintf(fid1, "%g   %g   %g\n", e_m1, e_est_x_elec, e_est_x_posi);
	fclose(fid1);
    end
  
    %% Delete unused files
  
    if( time_step_index <= multipulse_nr)
	%% nothing to do yet
    else 
	useless_file_index = time_step_index - multipulse_nr;
	useless_file_name = sprintf("positron_linac_%d.dat", useless_file_index);
	delete(useless_file_name);
	useless_file_name = sprintf("positron_linac_beampar_%d.dat", useless_file_index);
	delete(useless_file_name);
	useless_file_index = time_step_index - multipulse_nr;
	useless_file_name = sprintf("electron_linac_%d.dat", useless_file_index);
	delete(useless_file_name);
	useless_file_name = sprintf("electron_linac_beampar_%d.dat", useless_file_index);
	delete(useless_file_name);
	%%eval exec rm -f [glob -nocomplain beam_$file_index.dat]
    end
  
end

%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Measurement station 2 %
%%%%%%%%%%%%%%%%%%%%%%%%%%

if(use_bds == 1)
    if ((save_meas_stations == 1) && (empty_beam(electron)== 0))
	
	%% Load the single pulse results
	
	Tcl_Eval('read_array electron_bds_beampar_$time_step_index');
	Tcl_Eval('export_array_2_octave electron_bds_beampar_$time_step_index');

	spe_m2_x = emitt_x_tcl * 100;
	spe_m2_y = emitt_y_tcl * 100;
	
	sxx_m2 = sxx_axis_tcl;
	syy_m2 = syy_axis_tcl;
	szz_m2 = szz_axis_tcl;
    
	sps_m2_x = sqrt(sxx_m2)*1000;
	sps_m2_y = sqrt(syy_m2)*1000;
	%%sps_m2_z = sqrt(szz_m2)*1000;
	sps_m2_z = 0;
    
	disp_m2_x = disp_x_tcl*1000*e_spread_tcl;
	disp_m2_y = disp_y_tcl*1000*e_spread_tcl;
	disp_m2_z = disp_z_tcl*1000*e_spread_tcl;
    
	delta_m2_x = x_tcl*1000;
	delta_m2_y = y_tcl*1000;
	e_m2 = e_tcl;

	%% Calculate the real multipulse emittance if the time is right 

	if(sim_mode == 2)
	    %% Only calculated for dynamic simulations 
	    if(mod(time_step_index_short, multipulse_nr)==0)
      
		Tcl_Eval('array set mpe_data [calc_emitt_particle electron_bds_ $file_start_index $file_stop_index $eval_beam_shift]')
		Tcl_Eval('export_array_2_octave mpe_data');
		
		mpe_m2_x = emitt_x_tcl;
		mpe_m2_y = emitt_y_tcl;
		
		mpe_m2_x_old = mpe_m2_x;
		mpe_m2_y_old = mpe_m2_y;
	    end
	end
    
	%% Store the results
	
	fid2 = fopen(meas_station2_file_name, 'at');
	fprintf(fid2, "%d   %d   %g   %g   %g   %g   %g    ", time_step_index_long, sim_mode, global_time, gm_x, gm_full_y, gm_ml_y, gm_bds_y);
	fprintf(fid2, "%g   %g   %g   %g   ", corr_x, corr_full_y, corr_ml_y, corr_bds_y);
	fprintf(fid2, "%g   %g   %g   %g   ", spe_m2_x, spe_m2_y, mpe_m2_x, mpe_m2_y);
	fprintf(fid2, "%g   %g   %g   %g   %g   ", sps_m2_x, sps_m2_y, sps_m2_z, delta_m2_x, delta_m2_y);
	fprintf(fid2, "%g   %g   %g   %g   %g    ", disp_m2_x, disp_m2_y, disp_m2_z, offset_bff_bff, offset_bff_ff);
	fprintf(fid2, "%g\n", e_m2);
	fclose(fid2);
    end
  
    %% Delete unused files
  
    if( time_step_index <= multipulse_nr)
	%% nothing to do yet
    else 
	useless_file_index = time_step_index - multipulse_nr;
	useless_file_name = sprintf("electron_bds_%d.dat", useless_file_index);
	delete(useless_file_name);
	useless_file_name = sprintf("electron_bds_beampar_%d.dat", useless_file_index);
	delete(useless_file_name);
	useless_file_name = sprintf("positron_bds_%d.dat", useless_file_index);
	delete(useless_file_name);
	useless_file_name = sprintf("positron_bds_beampar_%d.dat", useless_file_index);
	delete(useless_file_name);
	%%eval exec rm -f [glob -nocomplain beam_$file_index.dat]
    end 
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Measurement station 3 (IP) %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if(use_beam_beam == 1)
    if ((save_meas_stations == 1) && (empty_beam(electron)== 0))

	%% Load the single pulse results
	
	Tcl_Eval('read_array electron_ip_beampar_$time_step_index');
	Tcl_Eval('export_array_2_octave electron_ip_beampar_$time_step_index');
	
	spe_m3_x = emitt_x_tcl * 100;
	spe_m3_y = emitt_y_tcl * 100;
    
	sxx_temp = sxx_axis_tcl;
	syy_temp = syy_axis_tcl;
	szz_temp = szz_axis_tcl;
	
	disp_x = disp_x_tcl*e_spread_tcl;
	disp_y = disp_y_tcl*e_spread_tcl;
	disp_z = disp_z_tcl*e_spread_tcl;
    
	x_temp = x_tcl;
	y_temp = y_tcl;
    
	e_m3 = e_tcl;
    
	Tcl_Eval('read_array positron_ip_beampar_$time_step_index');
	Tcl_Eval('export_array_2_octave positron_ip_beampar_$time_step_index');
    
	delta_m3_x = (x_temp - x_tcl) * 1000;
	delta_m3_y = (y_temp - y_tcl) * 1000;
    
	sxx_m3 = (sxx_axis_tcl + sxx_temp)/2;
	syy_m3 = (syy_axis_tcl + syy_temp)/2;
	szz_m3 = (szz_axis_tcl + szz_temp)/2;
    
	disp_m3_x = (disp_x_tcl*e_spread_tcl + disp_x) / 2 * 1000;
	disp_m3_y = (disp_y_tcl*e_spread_tcl + disp_y) / 2 * 1000;
	disp_m3_z = (disp_z_tcl*e_spread_tcl + disp_z) / 2 * 1000;
	
	sps_m3_x_avg = sqrt(sxx_m3) * 1000;
	sps_m3_y_avg = sqrt(syy_m3) * 1000;
	%%sps_m3_z_avg = sqrt(szz_m3) * 1000;
	sps_m3_z_avg = 0;
    
	%% Calculate the real multipulse emittance if the time is right 

	if(sim_mode == 2)
	    if(mod(time_step_index_short, multipulse_nr)==0)
      
		Tcl_Eval('array set mpe_data [calc_emitt_particle electron_ip_ $file_start_index $file_stop_index $eval_beam_shift]')
		Tcl_Eval('export_array_2_octave mpe_data');
		
		mpe_m3_x = emitt_x_tcl;
		mpe_m3_y = emitt_y_tcl;
      
		mpe_m3_x_old = mpe_m3_x;
		mpe_m3_y_old = mpe_m3_y;
	    end
	end

	%% Store the results

	fid3 = fopen(meas_station3_file_name, 'at');
	fprintf(fid3, "%d   %d   %g   %g   %g    ", time_step_index_long, sim_mode, global_time, gm_x, gm_full_y, gm_ml_y, gm_bds_y);
	fprintf(fid3, "%g   %g   %g   %g   ", corr_x, corr_full_y, corr_ml_y, corr_bds_y);
	fprintf(fid3, "%g   %g   %g   %g   ", spe_m3_x, spe_m3_y, mpe_m3_x, mpe_m3_y);
	fprintf(fid3, "%g   %g   %g   %g   %g   ", sps_m3_x_avg, sps_m3_y_avg, sps_m3_z_avg, delta_m3_x, delta_m3_y);
	fprintf(fid3, "%g   %g   %g    ", disp_m3_x, disp_m3_y, disp_m3_z);
    
	%% specific IP parameters
	fprintf(fid3, "%g   %g   %g   %g   ", Anglex, Angley, Lumi, Lumi_high);
	fprintf(fid3, "%g   %g   ", eley, posy);
	if(use_ip_feedback == 1)
	    fprintf(fid3, "%g   %g   ", corr_setting_ip_x, corr_setting_ip_y);
	end
	if (use_controller_y == 1)
	    fprintf(fid3, "%g  %g  %g  %g   ", bpm_reading_qd0(electron), bpm_reading_qd0(positron), beam_pos_qd0_y(electron), beam_pos_qd0_y(positron));
	end
	fprintf(fid3, "%g\n", e_m3);

	fclose(fid3);
    end
  
    %% Copy files when need to be stored:
    if(sim_mode == 2)
	if ( save_beam_file > 0 && 0 == mod(time_step_index_short,save_beam_file_freq) )
	    Tcl_Eval("eval exec cp electron.ini meas_station_3_beam_$time_step_index.dat");
	end
    end
  
    %% Delete unused files
  
    if( time_step_index <= multipulse_nr)
	%% nothing to do yet
    else 
	useless_file_index = time_step_index - multipulse_nr;
	useless_file_name = sprintf("electron_ip_%d.dat", useless_file_index);
	delete(useless_file_name);
	useless_file_name = sprintf("electron_ip_beampar_%d.dat", useless_file_index);
	delete(useless_file_name);
	useless_file_name = sprintf("positron_ip_%d.dat", useless_file_index);
	delete(useless_file_name);
	useless_file_name = sprintf("positron_ip_beampar_%d.dat", useless_file_index);
	delete(useless_file_name);
	%%eval exec rm -f [glob -nocomplain beam_$file_index.dat]
    end

    useless_file_name = sprintf("default.%d.%d", machine_index, time_step_index);
    delete(useless_file_name);

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% Annecy controller storage %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
    y_QD0_elec_prev = y_QD0_elec;
    y_QD0_elec = placet_element_get_attribute("electron",index_qd0,'y');
    y_incr_QD0_elec = (y_QD0_elec-y_QD0_elec_prev);
    y_QD0_posi_prev = y_QD0_posi;
    y_QD0_posi = placet_element_get_attribute("positron",index_qd0,'y');
    y_incr_QD0_posi = (y_QD0_posi-y_QD0_posi_prev);

    if(sim_mode == 2)
	if (time_step_index_short==1)
	    Beam_correction_log_id=fopen(sprintf("%s.txt",outputfile_ip_controller),'w');
	    fdisp(Beam_correction_log_id," ");
	    fprintf(Beam_correction_log_id,'Positron position [nm]:\t Electron position [nm]:\t Delta y [nm]:\t\t Positron correction [nm]:\t\t Electron correction [nm]:\t Luminosity:\t Noise [nm]:\n');
	    fclose(Beam_correction_log_id);
	end
    end
  
    %% Save beam corrections and positron positions in a file
    if (use_ip_feedback_annecy == 1)
	white_noise_annecy = white_noise(time_step_index+1)*1e9;
    else
	white_noise_annecy = 0;
    end
    Beam_correction_log_id=fopen(sprintf("%s.txt",outputfile_ip_controller),'a');
    fprintf(Beam_correction_log_id,"%1.2e \t\t %1.2e \t\t\t %1.2e \t\t %1.2e \t\t\t\t %1.2e \t\t\t %2.2e\t\t\t %1.2e\n",posy*1000,eley*1000,(posy-eley)*1000,IPdipole_strength_positron_y*2.3,IPdipole_strength_electron_y*2.3,str2num(Tcl_GetVar("lumipeak")),white_noise_annecy);
    fclose(Beam_correction_log_id);
end

%%%%%%%%%%%%%%%%%%%%%%
%% BPM value storage %
%%%%%%%%%%%%%%%%%%%%%%

if(use_bpm_storing == 1)
  
  % Readings without noise:
  if (bpm_storing_no_noise == 1)
    for i=electron:positron
      bpm_readings(:,:,i)=placet_get_bpm_readings(beamlinename(i,:), bpm_index(i,:), true);
    end
  end
  
  fid4 = fopen(bpm_meas_x_elec_file_name, "at");
  for i = (1:length(bpm_readings)-1)
    fprintf(fid4, "%g   ", bpm_readings(i,x,electron));
  end
  fprintf(fid4, "%g\n", bpm_readings(end,x,electron));
  fclose(fid4);
  
  fid4 = fopen(bpm_meas_x_posi_file_name, "at");
  for i = (1:length(bpm_readings)-1)
    fprintf(fid4, "%g   ", bpm_readings(i,x,positron));
  end
  fprintf(fid4, "%g\n", bpm_readings(end,x,positron));
  fclose(fid4);
  
  fid4 = fopen(bpm_meas_y_elec_file_name, "at");
  for i = (1:length(bpm_readings)-1)
      fprintf(fid4, "%g   ", bpm_readings(i,y,electron));
  end
  fprintf(fid4, "%g\n", bpm_readings(end,y,electron));
  fclose(fid4);
  
  fid4 = fopen(bpm_meas_y_posi_file_name, "at");
  for i = (1:length(bpm_readings)-1)
      fprintf(fid4, "%g   ", bpm_readings(i,y,positron));
  end
  fprintf(fid4, "%g\n", bpm_readings(end,y,positron));
  fclose(fid4);
end

%%%%%%%%%%%%%%%%%%%%%
%% GM value storage %
%%%%%%%%%%%%%%%%%%%%%

if(use_gm_storing == 1)
  gm_savings_x = placet_element_get_attribute("electron", corr_index(electron,:), "x");
  gm_savings_y = placet_element_get_attribute("electron", corr_index(electron,:), "y");
	
  fid5 = fopen(gm_x_elec_file_name, "at");
  for i = (1:(length(gm_savings_x)-1))
    fprintf(fid5, "%g   ", gm_savings_x(i));
  end
  fprintf(fid5, "%g\n", gm_savings_x(end));
  fclose(fid5);
  
  fid5 = fopen(gm_y_elec_file_name, "at");
  for i = (1:(length(gm_savings_y)-1))
    fprintf(fid5, "%g   ", gm_savings_y(i));
  end
  fprintf(fid5, "%g\n", gm_savings_y(end));
  fclose(fid5);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% IP beam correlation storage %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if((use_ml_corr_storing == 1) & (use_main_linac == 1))
    
    %%%%%%%%%%%%%%%%%%%
    %% electron linac %
    %%%%%%%%%%%%%%%%%%%

    %% Load and prepare the data
    Tcl_Eval('read_array electron_linac_beampar_$time_step_index');
    Tcl_Eval('export_array_2_octave electron_linac_beampar_$time_step_index');

    %% Write data in file
    fid6 = fopen(ml_corr_elec_file_name, "at");
    fprintf(fid6,"%d   %d   %g   %g   ", time_step_index_long, sim_mode, global_time);
    fprintf(fid6,"%g   %g   %g   %g   ", sxx_axis_tcl, sxxp_axis_tcl, sxe_axis_tcl, sxpe_axis_tcl);
    fprintf(fid6,"%g   %g   %g   %g   ", syy_axis_tcl, syyp_axis_tcl, sye_axis_tcl, sype_axis_tcl);
    fprintf(fid6,"%g   %g   %g   %g   ", sxy_axis_tcl, sxyp_axis_tcl, sxpy_axis_tcl, sxpyp_axis_tcl);
    fprintf(fid6,"\n");
    fclose(fid6);

    %%%%%%%%%%%%%%%%%%%
    %% positron linac %
    %%%%%%%%%%%%%%%%%%%

    %% Load and prepare the data
    Tcl_Eval('read_array positron_linac_beampar_$time_step_index');
    Tcl_Eval('export_array_2_octave positron_linac_beampar_$time_step_index');

    %% Write data in file
    fid6 = fopen(ml_corr_posi_file_name, "at");
    fprintf(fid6,"%d   %d   %g   %g   ", time_step_index_long, sim_mode, global_time);
    fprintf(fid6,"%g   %g   %g   %g   ", sxx_axis_tcl, sxxp_axis_tcl, sxe_axis_tcl, sxpe_axis_tcl);
    fprintf(fid6,"%g   %g   %g   %g   ", syy_axis_tcl, syyp_axis_tcl, sye_axis_tcl, sype_axis_tcl);
    fprintf(fid6,"%g   %g   %g   %g   ", sxy_axis_tcl, sxyp_axis_tcl, sxpy_axis_tcl, sxpyp_axis_tcl);
    fprintf(fid6,"\n");
    fclose(fid6);

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% IP beam correlation storage %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if((use_ip_corr_storing == 1) & (use_bds == 1))
    
    %%%%%%%%%%%%%%%%%%%
    %% electron linac %
    %%%%%%%%%%%%%%%%%%%

    %% Load and prepare the data
    Tcl_Eval('read_array electron_ip_beampar_$time_step_index');
    Tcl_Eval('export_array_2_octave electron_ip_beampar_$time_step_index');

    %% Write data in file
    fid6 = fopen(ip_corr_elec_file_name, "at");
    fprintf(fid6,"%d   %d   %g   %g   %g   %g   ", time_step_index_long, sim_mode, global_time, elex-posx, eley-posy);
    fprintf(fid6,"%g   %g   %g   %g   ", sxx_axis_tcl, sxxp_axis_tcl, sxe_axis_tcl, sxpe_axis_tcl);
    fprintf(fid6,"%g   %g   %g   %g   ", syy_axis_tcl, syyp_axis_tcl, sye_axis_tcl, sype_axis_tcl);
    fprintf(fid6,"%g   %g   %g   %g   ", sxy_axis_tcl, sxyp_axis_tcl, sxpy_axis_tcl, sxpyp_axis_tcl);
    if(use_beam_beam == 1)
	      fprintf(fid6,"%g   ", Lumi_high);
    end
    fprintf(fid6,"\n");
    fclose(fid6);

    %%%%%%%%%%%%%%%%%%%
    %% positron linac %
    %%%%%%%%%%%%%%%%%%%

    %% Load and prepare the data
    Tcl_Eval('read_array positron_ip_beampar_$time_step_index');
    Tcl_Eval('export_array_2_octave positron_ip_beampar_$time_step_index');

    %% Write data in file
    fid6 = fopen(ip_corr_posi_file_name, "at");
    fprintf(fid6,"%d   %d   %g   %g   %g   %g   ", time_step_index_long, sim_mode, global_time, elex-posx, eley-posy);
    fprintf(fid6,"%g   %g   %g   %g   ", sxx_axis_tcl, sxxp_axis_tcl, sxe_axis_tcl, sxpe_axis_tcl);
    fprintf(fid6,"%g   %g   %g   %g   ", syy_axis_tcl, syyp_axis_tcl, sye_axis_tcl, sype_axis_tcl);
    fprintf(fid6,"%g   %g   %g   %g   ", sxy_axis_tcl, sxyp_axis_tcl, sxpy_axis_tcl, sxpyp_axis_tcl);
    if(use_beam_beam == 1)
	      fprintf(fid6,"%g   ", Lumi_high);
    end
    fprintf(fid6,"\n");
    fclose(fid6);

end