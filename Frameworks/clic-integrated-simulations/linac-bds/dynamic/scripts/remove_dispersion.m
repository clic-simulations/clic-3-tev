% remove_dispersion.m

function [ bpm_readings_mod factor] = remove_dispersion( bpm_readings, disp_pattern )
% Function to remove a certain vector direction, from an other one. In this
% case we remove from the bpm_reading, the components that origin from
% dispersion due to energy jitter. 
%
% Input: 
%      bpm_readings ... BPM reading from which the dispersion should be
%                       removed
%      disp_pattern ... pattern that should be removed from bpm_readings
%
%      bpm_readings_mod ... Modified BPM readings
%      factor           ... Projection amplitude, which can be used
%                           for energy calculation

factor = bpm_readings'*disp_pattern;
bpm_readings_mod = bpm_readings - factor.*disp_pattern;

% Due to some reason the sign of the disp pattern is wrong. But it
% does not matter if we invert the sign of the factor for the
% energy calculation
factor = -factor;

end

