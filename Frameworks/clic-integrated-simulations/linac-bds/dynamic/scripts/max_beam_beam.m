function [ elec_opt, posi_opt, offset, beam_beam_meas_init, beam_beam_meas_opt ] = max_beam_beam( elec, posi, x_or_y)
% Function max_beam_beam takes two beams and shifts them vers each other
% till a approx. luminosity measure is maximized. At the moment this
% functionallity is just implemented in y direction.
%
% Input:
%       elec ... electron beam
%       posi ... positron beam
%       x_or_y ... optimize in x or y direction
%
% Output:
%       elec_opt ... electron beam shifted by (-offset/2)
%       posi_opt ... positron beam shifted by offset/2
%       beam_beam_meas_init ... beam beam measure of the initial passed beams
%       beam_beam_meas_opt ... beam beam measure of the optimized and returend beams
%       offset ... optimal offset of the two initial beams (elec_pos-posi_pos)
%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculate initial measure %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if(strcmp(x_or_y, 'x'))
  data_elec = elec(:,2);
  data_posi = posi(:,2);
else
  data_elec = elec(:,3);
  data_posi = posi(:,3);
end
beam_beam_meas_init = calc_beam_beam_measure(data_elec, data_posi, 0);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% First we center the beams, such that the search region is not too big %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

mean_elec = mean(data_elec);
data_elec = data_elec - mean_elec;
mean_posi = mean(data_posi);
data_posi = data_posi - mean_posi;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Center the maxima on a larger scale to get more robust %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if(strcmp(x_or_y, 'x'))
    offset = -2:0.001:2;
else
    offset = -0.2:0.0001:0.2;
end

curve_elec = hist(data_elec, offset);
curve_posi = hist(data_posi, offset);
%% Cut the outer point to get rid of stong halo effects that is collected in 2 bins
curve_elec = curve_elec(2:end-1);
curve_posi = curve_posi(2:end-1);
offset = offset(2:end-1);
[temp,max_index1] = max(curve_elec);
[temp,max_index2] = max(curve_posi);
offset2 = offset(max_index1)-offset(max_index2);
data_elec = data_elec - offset(max_index1);
data_posi = data_posi - offset(max_index2);

%%%% Debug 
%%curve_elec = hist(data_elec, offset);
%%$curve_posi = hist(data_posi, offset);
%%figure(3);
%%plot(offset, curve_elec, 'r');
%%hold on;
%%grid on;
%%plot(offset, curve_posi, 'b');
%%hold off;
%%figure(100);
%%keyboard;
%%%% dEBUG

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Optimize the beam overlap %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if(strcmp(x_or_y, 'x'))
  offset = -0.05:0.0005:0.05;
else
  offset = -0.005:0.00005:0.005;
end

beam_beam_measure = zeros(size(offset));
for i=1:length(beam_beam_measure)
    beam_beam_measure(i) = calc_beam_beam_measure(data_elec, data_posi, offset(i));
end
[beam_beam_meas_opt, index_max] = max(beam_beam_measure);
%keyboard;
offset1 = offset(index_max);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Create the shifte beams %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%

elec_opt = elec;
posi_opt = posi;
if(beam_beam_meas_init > beam_beam_meas_opt)
  % Do not change anything
  offset1 = 0;
  beam_beam_meas_opt = beam_beam_meas_init;
end

offset = mean_elec - mean_posi + offset1 + offset2;
%offset = mean_elec - mean_posi + offset2;
if(strcmp(x_or_y, 'x'))
    elec_opt(:,2) = elec_opt(:,2) - offset/2;
    posi_opt(:,2) = posi_opt(:,2) + offset/2;
else
    elec_opt(:,3) = elec_opt(:,3) - offset/2;
    posi_opt(:,3) = posi_opt(:,3) + offset/2;
end
%keyboard;
end

