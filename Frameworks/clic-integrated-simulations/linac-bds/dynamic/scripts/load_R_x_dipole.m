% Function to load the response matrix of the dipoles for the x
% direction.
%
% Juergen Pfingstner

function [R0_x U0_x s0_x V0_x] = load_R_x_dipole(use_main_linac,  use_bds, nr_corr_ml, used_bpm_scaling)

if( (use_main_linac == 1) & (use_bds == 0) )
	
  if(use_bpm_scaling == 1)
    R0_x = load('-ascii', '../scripts/R0_dipole_x_scale.dat');
    R0_x = R0_x(1:nr_corr_ml,1:nr_corr_ml);
    U0_x = load('-ascii', '../scripts/U0_dipole_ml_x_scale.dat');
    s0_x = load('-ascii', '../scripts/s0_dipole_ml_x_scale.dat');
    V0_x = load('-ascii', '../scripts/V0_dipole_ml_x_scale.dat');
  else
    R0_x = load('-ascii', '../scripts/R0_dipole_x.dat');
    R0_x = R0_x(1:nr_corr_ml,1:nr_corr_ml);
    U0_x = load('-ascii', '../scripts/U0_dipole_ml_x.dat');
    s0_x = load('-ascii', '../scripts/s0_dipole_ml_x.dat');
    V0_x = load('-ascii', '../scripts/V0_dipole_ml_x.dat');
  end
  
elseif( (use_main_linac == 0) & (use_bds == 1) )
  
  if(use_bpm_scaling == 1)
    R0_x = load('-ascii', '../scripts/R0_dipole_x_scale.dat');
    R0_x = R0_x((nr_corr_ml+1):end, (nr_corr_ml+1):end);
    U0_x = load('-ascii', '../scripts/U0_dipole_bds_x_scale.dat');
    s0_x = load('-ascii', '../scripts/s0_dipole_bds_x_scale.dat');
    V0_x = load('-ascii', '../scripts/V0_dipole_bds_x_scale.dat');
  else
    R0_x = load('-ascii', '../scripts/R0_dipole_x.dat');
    R0_x = R0_x((nr_corr_ml+1):end, (nr_corr_ml+1):end);
    U0_x = load('-ascii', '../scripts/U0_dipole_bds_x.dat');
    s0_x = load('-ascii', '../scripts/s0_dipole_bds_x.dat');
    V0_x = load('-ascii', '../scripts/V0_dipole_bds_x.dat');
  end

elseif( (use_main_linac == 1) & (use_bds == 1) )
  
  if(use_bpm_scaling == 1)
    R0_x = load('-ascii', '../scripts/R0_dipole_x_scale.dat');
    U0_x = load('-ascii', '../scripts/U0_dipole_x_scale.dat');
    s0_x = load('-ascii', '../scripts/s0_dipole_x_scale.dat');
    V0_x = load('-ascii', '../scripts/V0_dipole_x_scale.dat');
  else
    R0_x = load('-ascii', '../scripts/R0_dipole_x.dat');
    U0_x = load('-ascii', '../scripts/U0_dipole_x.dat');
    s0_x = load('-ascii', '../scripts/s0_dipole_x.dat');
    V0_x = load('-ascii', '../scripts/V0_dipole_x.dat');
  end
  
end

