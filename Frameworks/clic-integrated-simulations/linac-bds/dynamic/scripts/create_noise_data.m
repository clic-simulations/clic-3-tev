% Script to create the data needed to create and store various
% corrector noise sources (coming from the stabilization system). 
%
% Juergen Pfingstner

%%%%%%%%%%%%%%%%%%%%
% For x correctors %
%%%%%%%%%%%%%%%%%%%%

if(stabilization_noise_x == 1) 
  
  corr_noise_x = zeros(length(corr_index),2);
  
  if(strcmp(stabilization_noise_type, 'white_gaussian'))
    
    % White gaussian noise
    
    stabilization_noise_data_x = struct('actual_value', 0, 'std', 0.001);
    for j=electron:positron
      for i = 1:length(corr_index)
        stabilization_noise_data_x(i,j).actual_value = 0;
        stabilization_noise_data_x(i,j).std = stabilization_noise_param;
      end
    end
    
    % Set std to 0 if section is not selected
    if(strcmp(stabilization_noise_section, 'ML'))
      for j=electron:positron
        for i=(nr_corr_ml(j)+1):length(corr_index)
          stabilization_noise_data_x(i,j).std = 0;
        end
      end
    end
    if(strcmp(stabilization_noise_section, 'BDS'))
      for j=electron:positron
        for i=1:nr_corr_ml(j)
          stabilization_noise_data_x(i,j).std = 0;
        end
      end
    end
    
  elseif(strcmp(stabilization_noise_type, 'white_uniform'))
    
    % White uniform noise
    
    stabilization_noise_data_x = struct('actual_value', 0, 'interval', 0.001);
    for j=electron:positron
      for i = 1:length(corr_index)
        stabilization_noise_data_x(i,j).actual_value = 0;
        stabilization_noise_data_x(i,j).interval = stabilization_noise_param;
      end
    end
    
    % Set interval to 0 if section is not selected
    if(strcmp(stabilization_noise_section, 'ML'))
      for j=electron:positron
        for i=(nr_corr_ml(j)+1):length(corr_index)
          stabilization_noise_data_x(i,j).interval = 0;
        end
      end
    end
    if(strcmp(stabilization_noise_section, 'BDS'))
      for j=electron:positron
        for i=1:nr_corr_ml(j)
          stabilization_noise_data_x(i,j).interval = 0;
        end
      end
    end
    
  else
    
    % Unknown type
    
    fprintf(1,'\n\nThe specified corrector noise type is unknown. Simulation terminated!!!\n\n');
    exit;
  end
end

%%%%%%%%%%%%%%%%%%%%
% For y correctors %
%%%%%%%%%%%%%%%%%%%%

if(stabilization_noise_y == 1)
  
  corr_noise_y = zeros(length(corr_index),2);
  
  if(strcmp(stabilization_noise_type, 'white_gaussian'))
    
    % White gaussian noise
    
    stabilization_noise_data_y = struct('actual_value', 0, 'std', 0.001);
    for j=electron:positron
      for i = 1:length(corr_index)
        stabilization_noise_data_y(i,j).actual_value = 0;
        stabilization_noise_data_y(i,j).std = stabilization_noise_param;
      end
    end
    
    % Set std to 0 if section is not selected
    if(strcmp(stabilization_noise_section, 'ML'))
      for j=electron:positron
        for i=(nr_corr_ml(j)+1):length(corr_index)
          stabilization_noise_data_y(i,j).std = 0;
        end
      end
    end
    if(strcmp(stabilization_noise_section, 'BDS'))
      for j=electron:positron
        for i=1:nr_corr_ml(j)
          stabilization_noise_data_y(i,j).std = 0;
        end
      end
    end
    
  elseif(strcmp(stabilization_noise_type, 'white_uniform'))
    
    % White uniform noise
    stabilization_noise_data_y = struct('actual_value', 0, 'interval', 0.001);
    for j=electron:positron
      for i = 1:length(corr_index)
        stabilization_noise_data_y(i,j).actual_value = 0;
        stabilization_noise_data_y(i,j).interval = stabilization_noise_param;
      end
    end
        
    % Set interval to 0 if section is not selected
    if(strcmp(stabilization_noise_section, 'ML'))
      for j=electron:positron
        for i=(nr_corr_ml(j)+1):length(corr_index)
          stabilization_noise_data_y(i,j).interval = 0;
        end
      end
    end
    if(strcmp(stabilization_noise_section, 'BDS'))
      for j=electron:positron
        for i=1:nr_corr_ml(j)
          stabilization_noise_data_y(i,j).interval = 0;
        end
      end
    end
    
  else
    
    % Unknown type
    
    fprintf(1, '\n\nThe specified corrector noise type is unknown. Simulation terminated in create_noise_data!!!\n\n');
    exit;
  end
end