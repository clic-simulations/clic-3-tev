
Octave {
    function [volt]= IPDipoleCorrection(anglex,angley);
    % Function that calculates the strength of the IP Dipole in x,y for electron line
    %
    % Input:
    %        anglex ... measured deflection angle in x [urad]
    %        angley ... measured deflection angle in y [urad]
    %
    % Output
    %        volt ... voltage to be applied to the kicker [kV]
    volt = IPDipoleCorrection_linear(anglex,angley);
    end
}

Octave {
    function [volt]= IPDipoleCorrection_dummy(anglex,angley);
    % dummy feedback
    volt = [0.0; 0.0];
    end
}

Octave {
    function [volt]= IPDipoleCorrection_linear(anglex,angley);
    % linear feedback
    % load linear IP feedback
    eval (['load -text ' "../scripts/IPfeedback.dat"]);

    Angle = [anglex; angley];
    volt = -0.5*(Angle) ./ IPfeedback.Rangle;
    end
}

Octave {
    function [volt]= IPDipoleCorrection_linear_corrected(anglex,angley);
    % NOT WORKING, mixing quad offset and dipole strengths..

    % linear feedback
    % corrected for beam based feedback
    % load linear IP feedback
    eval (['load -text ' "../scripts/IPfeedback.dat"]);

    % calculate expected correction:
    if (use_controller_y) 
      Q_off_svd_y_elec = R0_y(end:end,1:end) * corr_settings_new_y_elec;
      Q_off_svd_y_posi = R0_y(end:end,1:end) * corr_settings_new_y_posi;
    else 
      Q_off_svd_y_elec = 0;
      Q_off_svd_y_posi = 0;
    end
    if (use_controller_x) 
      Q_off_svd_x_elec = R0_x(end:end,1:end) * corr_settings_new_x_elec;
      Q_off_svd_x_posi = R0_x(end:end,1:end) * corr_settings_new_x_posi;
    else
      Q_off_svd_x_elec = 0;
      Q_off_svd_x_posi = 0;
    end

    Q_off_svd_posi = [Q_off_svd_x_posi; Q_off_svd_y_posi];
    Q_off_svd_elec = [Q_off_svd_x_elec; Q_off_svd_y_elec];
    Angle = [anglex; angley];
    volt = -0.5*(Angle) ./ IPfeedback.Rangle - (Q_off_svd_elec+Q_off_svd_posi) / 2;
    end
}

Octave {
    function [volt]= IPDipoleCorrection_lookup(anglex,angley);
    % feedback based on lookup table

    voltx_m = [-20;-18;-16;-14;-12;-10;-8;-6;-5;-4;-3;-2;-1;0.0;1;2;3;4;5;6;8;10;12;14;16;18;20;22];
    volty_m = [-2.0;-1.8;-1.6;-1.4;-1.2;-1.0;-0.8;-0.6;-0.5;-0.4;-0.3;-0.2;-0.1;0.0;0.1;0.2;0.3;0.4;0.5;0.6;0.8;1.0;1.2;1.4;1.6;1.8;2.0;2.2];
    anglex_m = [1e+05;9.08881900e+01;8.15702085e+01;7.38867630e+01;6.40323120e+01;5.74753835e+01;4.61513835e+01;3.45757075e+01;2.84579875e+01;2.32518140e+01;1.82468840e+01;1.23629895e+01;5.96171750e+00;0.0;-5.97734150e+00;-1.15559740e+01;-1.84385900e+01;-2.28725905e+01;-2.84784070e+01;-3.38430380e+01;-4.54901485e+01;-5.51351440e+01;-6.63522710e+01;-7.44190065e+01;-8.21657255e+01;-8.83701195e+01;-9.66601100e+01;-1e+05];
    angley_m = [1e+05;8.83471590e+01;8.83471590e+01;7.37509280e+01;6.52505075e+01;5.71941275e+01;4.79114320e+01;3.77751415e+01;3.29921015e+01;2.77325505e+01;2.17344410e+01;1.49464875e+01;7.48450700e+00;0.0;-7.45456350e+00;-1.50889565e+01;-2.16163065e+01;-2.76105855e+01;-3.28402695e+01;-3.81607225e+01;-4.80060980e+01;-5.69495650e+01;-6.57891865e+01;-7.35740490e+01;-8.08862530e+01;-8.77917365e+01;-9.47486510e+01;-1e+05];
    % straight line interpolation
    volt_x = -0.5*interp1(anglex_m,voltx_m,anglex);
    volt_y = -0.5*interp1(angley_m,volty_m,angley);
    volt = [volt_x; volt_y];
    end
}

Octave {
    function [volt_y]= IPDipoleCorrection_pid_y(angley,corry_prev,angley_prev,corry_prev2);
    % feedback by D. Schulte
    %
    % load linear IP feedback
    eval (['load -text ' "../scripts/IPfeedback.dat"]);
    %
    % a = 1.0;
    b = 0.15;
    c = 1.0;
    d = 1.5;
    % a*corry (old correction, added in controller_apply)
    volt_y = b*angley/IPfeedback.Rangle(2,1) + c*(angley - angley_prev)/IPfeedback.Rangle(2,1) + d*(corry_prev-corry_prev2);
    end
}

Octave {
    function [volt_y]= IPDipoleCorrection_pid2_y(angley,corry_prev,angley_prev,corry_prev2,angley_rec);
    % feedback by D. Schulte, based on pid2.m
    %
    % load linear IP feedback
    eval (['load -text ' "../scripts/IPfeedback.dat"]);
    %
    % a = 1.0;
    nm = 3.0;
    b = 0.0;
    c = 1.0;
    d = 1.0;
    % a*corry (old correction, added in controller_apply)
    volt_y = b*angley/IPfeedback.Rangle(2,1) + c*(angley - angley_prev)/IPfeedback.Rangle(2,1) + d*(corry_prev-corry_prev2) + angley_rec;
    end
}
