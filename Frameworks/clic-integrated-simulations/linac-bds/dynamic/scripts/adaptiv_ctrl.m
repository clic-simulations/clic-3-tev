
function y = adaptiv_ctrl(u1,u2)

format long

persistent park;        				% Parameter set of estimated filter
persistent phik;        				% Input/output signals of the estimated filter
persistent Fk;          				% Adaptation gain of the estimator
persistent lbd1;        				% Forgetting factor: 0.95<lbd1<=0.99 
persistent lbd2;        				% If lbd2=1 => Constant Forgetting factor if 0<lbd2<2 => variable Forgetting factor
persistent feed;        				% Adaptive command
persistent Dim_Num_Estimated_dynamic;	% Order of the numerator of the estimated dynamic (0 if no feedforward)
persistent Dim_Den_Estimated_dynamic;	% Order of the denominator of the estimated dynamic (i.e. Number of 
persistent TraceMax;					% Control of the trace of the Kalman Gain 
persistent TraceMin;					% Control of the trace of the Kalman Gain 
persistent Hc;							% Two latest controller output
persistent Process_Output;				% Latest value of the beam displacmeent
persistent W_Estimated;					% Latest estimation of the perturbation

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Initialisation %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if isempty(park) 

%%% Parameters of the adaptive control 
    park=-1.000398;%[Den_Estimated_dynamic(1:Dim_Den_Estimated_dynamic)'; Num_Estimated_dynamic(1:Dim_Num_Estimated_dynamic)'];
   
%%% Adaptive control parameters

    lbd1=0.99;    					% Forgetting factor: 0.95<lbd1<=0.99              
    lbd2=1;							% If lbd2=1 => Constant Forgetting factor if 0<lbd2<2 => variable Forgetting factor
    TraceMax=0.0001;                % Control of the trace of the Kalman Gain     
    TraceMin=0.000001;				% Control of the trace of the Kalman Gain  
    gFk =0.000001;       			% Initial Kalman gain

%%% Do not modify after this point    

    Num_Estimated_dynamic=[];       % Is empty if no feed-forward (i.e. empty = no real measure of the ground motion disturbance)  
    Den_Estimated_dynamic=0.1;      % Only 1 parameter: gives the best results

    Dim_Den_Estimated_dynamic=max(size(Den_Estimated_dynamic));    % Order of the denominator of the mechanical estimator
    Dim_Num_Estimated_dynamic=max(size(Num_Estimated_dynamic));    % Order of the numerator of the mechanical estimator

    phik=zeros(Dim_Den_Estimated_dynamic+Dim_Num_Estimated_dynamic,1);
    Fk=eye(Dim_Den_Estimated_dynamic+Dim_Num_Estimated_dynamic);
    Fk=Fk*gFk;
    feed=zeros(2,1);
    Hc=zeros(2,1);
    Process_Output=zeros(Dim_Num_Estimated_dynamic,1);
    W_Estimated=zeros(Dim_Den_Estimated_dynamic,1);

end

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Adaptive controller %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% Inputs (step k)

    yk=u1;    % Input 3 = process output
    Hc(1)=u2; % Input 3 = controller output

%%% Adapted parameters for this step (k)

    v0= yk-Hc(2);                                                   % Estimation error at time k-1
    vk=v0/(1+phik'*Fk*phik);                                        % Compute filtered error

    park=park+Fk*phik*vk;                                           % Adaptation of parameter set
    Fk=(Fk-(Fk*(phik)*(phik')*Fk)/(lbd1/lbd2+phik'*Fk*phik))/lbd1;  % Computation the gain for next step new adaptive gain

%%% Limitation of the trace of the Kalman Gain

    tra = trace(Fk);
    if tra<TraceMin
        Fk=Fk*TraceMin/tra;											
    end
    if tra>TraceMax
        Fk=Fk*TraceMax/tra;											
    end

%%% Shift for next time (k+1) for the reconstrHcted disturbance input of the process 

    for i=Dim_Num_Estimated_dynamic:-1:2;                  	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    Process_Output(i)=Process_Output(i-1);                  % Memorization of the beam displacement  %          
    end                                                     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    Process_Output(1)=yk;                                

    for i=Dim_Den_Estimated_dynamic:-1:2;                 	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
    W_Estimated(i)=W_Estimated(i-1);                        % Memorization of Estimated disturbances %
    end                                                     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    W_Estimated(1)=yk-Hc(2)+feed(1);                       	%                Next step               %

    phik(1:Dim_Den_Estimated_dynamic)=-W_Estimated;        	% Memorization of estimation of real disturbance at time k+1 
    phik(Dim_Den_Estimated_dynamic+1:Dim_Den_Estimated_dynamic+Dim_Num_Estimated_dynamic)=Process_Output;

%%% Predictor for real disturbance 

    Hc(2)=Hc(1);                	% Shifts for the next step
    feed(2)=feed(1);
    feed(1)=park'*phik;          	% Use most recent parameter set and predict real disturbance at time k

%%% Outputs

    y(1)=feed(1);

end
