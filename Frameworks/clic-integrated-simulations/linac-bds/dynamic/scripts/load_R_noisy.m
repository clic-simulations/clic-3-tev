% Function to load a noisy response matrix and calculate its SVD
%
% Juergen Pfingstner
% 18th of October 2011

function [R U s V] = load_R_noisy(R_file_name, use_main_linac, use_bds, nr_corr_ml, use_bpm_scaling)

R = load(R_file_name);
R = R.R;

if( (use_main_linac == 1) & (use_bds == 0) )
    R = R(1:nr_corr_ml,1:nr_corr_ml);
elseif( (use_main_linac == 0) & (use_bds == 1) )
    R = R((nr_corr_ml+1):end, (nr_corr_ml+1):end);
end

fprintf(1, 'Perform the SVD ... ');
[U, S, V] = svd(R);
s=diag(S);
fprintf(1, 'finished\n');


