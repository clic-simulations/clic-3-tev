% Script to perform one step of the long-term alignment
%
% Juergen Pfingstner
% 12. April 2012

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Recalculate the full response matrix %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if(response_matrix_calc_longterm == 1)
    time_step_index = time_step_index+1;
    Tcl_SetVar("time_step_index", time_step_index);
    global_time = global_time + delta_T_short;

    %% At the moment only for the electron linac
    line_name = beamlinename(1,:);
    beam_name = 'beam_R';
    Tcl_Eval("make_beam_slice_energy_gradient beam_R $n_slice $n_part $energy_amplification 1.0");
    step_size_response = 0.4;
    positron_old = positron;
    positron = 1;
  
    fprintf(1, "Calculate the long-term response matrix for x (take a day off) ... ");
    [R0_qp_x] = calc_qp_response_matrix(line_name, beam_name, bpm_index(electron,:), qp_index(electron,:), step_size_response, 0);
    fprintf(1, "finished\n");
    save -ascii "R0_qp_x.dat" R0_qp_x;

    fprintf(1, "Calculate the long-term response matrix for y (take a day off) ... ");
    [R0_qp_y] = calc_qp_response_matrix(line_name, beam_name, bpm_index(electron,:), qp_index(electron,:), step_size_response, 1);
    fprintf(1, "finished\n");
    save -ascii "R0_qp_y.dat" R0_qp_y;   

    positron = positron_old;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Basic steering: 1-2-1, all-2-all, ... % 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


if(use_basic_steering == 1)

    time_step_index = time_step_index+1;
    Tcl_SetVar("time_step_index", time_step_index);
    global_time = global_time + delta_T_short;

    %% Get the initial beam orbit
    tracking
    
    for i=electron:positron
	if(basic_steering_method == 1)
	    %% 1-to-1 steering
	    %% to be implemented
	elseif(basic_steering_method == 2)
	    %% all-to-all steering
	    fprintf(1, "All-to-all steering ... ");

	    if(basic_steering_option == 3)
		bpm_readings_steering = bpm_readings(1:nr_corr_ml(i),:,i);
	    else
		bpm_readings_steering = bpm_readings(:,:,i);
	    end

	    %% Calculate correction 
	    basic_steering_corrections_x(i,:) = (BS_x*[bpm_readings_steering(:,1); zeros(nr_corr_steering_x,1)])';
	    basic_steering_corrections_y(i,:) = (BS_y*[bpm_readings_steering(:,2); zeros(nr_corr_steering_y,1)])';
	else
	    fprintf(1,"Unknown basic steering method")
	end
    end

    %% Apply corrections
    one_time_corr_apply(beamlinename, corr_index_basic_steering, basic_steering_corrections_x, basic_steering_corrections_y, electron, positron);

    %% Centre beam 
    if(use_beam_beam && use_bds)
	fprintf(1, "centre beam ... ");
	centre_beam;
    else
	tracking;
    end

    %% Store results
    postprocess_time_step;
    fprintf(1, "done\n");


end

%%%%%%%%%%%%%%
%% BDS Knobs %
%%%%%%%%%%%%%%

% Apply knobs in one go, to be changed. 

if (use_beam_beam && use_bds && use_bds_knobs)

    time_step_index = time_step_index+1;
    Tcl_SetVar("time_step_index", time_step_index);
    global_time = global_time + delta_T_short;

    %% Centre beam 
    centre_beam

    %% apply all knobs
    Tcl_Eval("multipole_knobs 10.0");
    
    %%Tcl_Eval('SaveAllPositions -vertical_only 0 -cav_grad_phas 1 -binary 0 -file "machine.dat" -cav_bpm 1 -nodrift 1');
    %% apply individual knobs:
    %%for i=1:10
    %%  Tcl_Eval('ReadAllPositions -vertical_only 0  -cav_grad_phas 1 -binary 0 -file "machine.dat" -cav_bpm 1 -nodrift 1	');
    %%  Tcl_SetVar("knobnumber",i);
    %%  Tcl_Eval("multipole_knobs_single $knobnumber 3.0");
    %%end

    %% Store results
    postprocess_time_step;
end


%%%%%%%%%%%%%%%%%%%%%%
%% Individual script %
%%%%%%%%%%%%%%%%%%%%%%

if(use_user_long_term_alignment_script == 1)
    eval(user_long_term_alignment_script_name);
end

