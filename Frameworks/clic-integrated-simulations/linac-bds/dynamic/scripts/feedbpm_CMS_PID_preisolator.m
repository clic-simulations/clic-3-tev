
function beam_correction=feedbpm_CMS_PID_preisolator(y1,y2,Adapt_switch)

%beam_correction=feedbpm(y1,y2,Adapt_switch)
%y1: measure of the position of the beam 1 at the IP
%y2: measure of the position of the beam 2 at the IP
%1= with adaptive control 0 = without
%feedbpm integrates a numerical PID which is discretised using p => Ts/(1-Z^-1)

persistent h
persistent ha
persistent bpm
persistent Dim_Den_H
persistent Dim_Num_H
persistent Num_H
persistent Den_H
persistent Adapt_Gain;

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Initialisation %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if isempty(h)
    Adapt_Gain=10^8;                        % In order to avoid zero machine face to too small signals at the input of the controller
end

y=y1-y2;
y=y*Adapt_Gain;

if isempty(h)
       
    %% parameters of the non adaptive control

    %Tp=1.980;
    %Ti=0.02;   %Unstable with PLACET
    %Td=-0.02;

    Tp=-0.96;
    Ti=-0.04;
    Td=0;
    Te=1/50;
   
    %% Laplace transfrom

   if Ti==0 && Td==0 % Particular case where we have only a P controller
        a1= 0;                       	
        a2= 0;
        b0= Tp;							
        b1=  0;						
        b2= 0;
    else
        if Ti==0 && Td~=0       % Particular case where we have a PD
            a=0.01;
            a1=-a/(Te+a);                     	
            a2= 0;
            b0=	Tp/(Te+a) *	(Te+a+Td);					
            b1=-Tp/(Te+a) *	(a+Td);				
            b2= 0;
        else
            if Ti~=0 && Td==0   % Particular case where we have a PI
            a1=-1;                       	
            a2= 0;
            b0= (Tp/Ti) * (Te+Ti);							
            b1=-Tp;						
            b2= 0;
            else                % Particular case where we have a PID
                a=0.01;
                a1=-Te/(Te+a) * (1+2*a/Te);                       	
                a2= Te/(Te+a) * (a*Ti/Te^2);
                b0= Te/(Te+a) * (Tp/Ti) * (Te+a+Ti+(Ti*Td+a*Ti)/Te);							
                b1=-Te/(Te+a) * (Tp/Ti) * (a+Ti+2*(Ti*Td+a*Ti)/Te);						
                b2= Te/(Te+a) * (Tp/Ti) * (Ti*Td+a*Ti)/Te;
            end
        end
    end

    
%%% Controller
    
    Num_H=[b0 b1 b2];						% Num_H contain the parameters of the Numerator of H [b0 b1 ... bn]
    Den_H=[1 a1 a2];						% Den_H contain the parameters of the Denominator of H [a1 a2 ... an]
   

    Dim_Den_H=max(size(Den_H));				% Order of the denominator of the controller
    Dim_Num_H=max(size(Num_H));				% Order of the numerator of the controller

    %%% System


    SYSnz=[1 0];
    SYSdz=[1 0 0];
    SYSz=tf(SYSnz,SYSdz,Te);
    PIDz=tf(Num_H,Den_H,Te);
    
    FTBF=feedback(SYSz,PIDz);
    step(FTBF)
    hold on
    
    
%%% Do not modify after this point

    ha=0;
    bpm(1:max([Dim_Den_H Dim_Num_H]))=y;    % Initial conditions for the controller
    h(1:Dim_Den_H)=y;
    
end

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Beam correction %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

bpm(1)=y;
h(1,2:end)=h(1,1:(end-1));
h(1,1)=0;

    for k=1:Dim_Num_H
        h(1,1)=h(1,1)+Num_H(k)*bpm(k);
    end

    for k=2:Dim_Den_H
        h(1,1)=h(1,1)-Den_H(k)*h(1,k);
    end

    if Adapt_switch==1
        ha=adaptiv_ctrl(-bpm(1),h(1,1));
    end

beam_correction=(-h(1,1)+ha)/Adapt_Gain;
bpm(2:end)=bpm(1:(end-1));
bpm(1)=0;

end                                                     % End of function
