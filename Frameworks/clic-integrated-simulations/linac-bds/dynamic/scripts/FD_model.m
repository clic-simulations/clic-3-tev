function [ offset_x, offset_y ] = FD_model( x_init, xp_init, y_init, yp_init, y_qf1, y_qd0, E , sync_rad, use_mp)
% calc_disp_sigma calculated the beam size growth in the the final focus
% due to dispersion (so far implemented only in y).
%
% Inputs: y0 ... initial beam offset
%         y_p0 ... initial beam angle
%         y_qf1 ... offset of QF1
%         y_qd0 ... offset of QD1
%         E ... energy of the beam in [GeV]
%         sync_rad ... consider syncrotron radiation or not
%
% Outputs: offset_x ... offset of the particle in x direction
%          offset_y ... offset of the particle in y direction
%
% Jurgen Pfingstner

% Test if the simplifications were correct
variant = 2;
%use_macroparticle = 1;

%Input
%x_init
%xp_init
%y_init
%yp_init
%y_qf1
%y_qd0
%E

% Parameters
%pos_girder_end = 2.3735+2+11.2889+11.838;
%pos_qd0 = 4.869;
%pos_qf1 = 11.838;
%L0 = pos_girder_end-pos_qf1;
%L1 = pos_qf1-pos_qd0;
%L2 = pos_qd0;
L_D0 = 3.5026092;
L_QD0 = 2.732532;
L_D1 = 0.248412;
L_S0 = 0.496824;
L_D2 = 0.248412+0.496824+2+0.48412;
L_QF1 = 3.25668132;
L_D3 = 0.248412;
L_S1 = 0.496824;
L_D4 = 2+11.28893905;


% This values can be gathered from the lattice files. There 1/f*e0 is
% specified, which is equal to K*L*e0. In the program the specified value
% is /e and /L, which results in K*eo/e, which is exactly the wanted energy
% scaling. 
f_qf1 = 1/0.13177400282;
%f_qf1 = 1/0.1317762219;
f_qd0 = 1/0.3175473258;
%f_qd0 = 1/0.3175470482;
% I define g in a way that 1/f = g/E0 => g = E0/f
if(sync_rad == 1)
   E_ref = 1499.74151812;
else
   E_ref = 1500.0386663374468;
end
g_qd0 = E_ref/f_qd0;
g_qf1 = E_ref/f_qf1;

if(variant == 1)

   T1 = (1-L2*g_qd0/E) * (1+L1*g_qf1/E) + L2*g_qf1/E;
   T2 = L1*(1-L2*g_qd0/E) + L2;
   T3 = -(1-L2*g_qd0/E)*(L1*g_qf1/E) - L2*g_qf1/E;
   T4 = L2*g_qd0/E;

   offset_y = T1*y_init + T2*yp_init + T3*y_qf1 + T4*y_qd0;

else


   %%%%%%%%%%%%%%
   % y directon %
   %%%%%%%%%%%%%%

   % Debug
   %E = 1500;
   %y_qd0 = 0;
   %y_qf1 = 0;
   %L_qf1 = 3.25668132;
   %L_qd0 = 2.732532;

   % Quadrupole parameters
   K_qf1 = g_qf1/E;
   K_qd0 = g_qd0/E;
   k_qd0 = K_qd0/L_QD0;   
   k_qf1 = K_qf1/L_QF1; 


   % Sextupole parameters
   N_lenses = 5;

   k_s0 = 21.78589514*E_ref*10^-6/factorial(3-1);
   k_s1 = -6.053493698*E_ref*10^-6/factorial(3-1);

   d_x_s0_begin = -10.0103017;
   d_x_s0_end = -8.6880332;
   d_x_s0 = (d_x_s0_begin+d_x_s0_end)/2;

   d_x_s1_begin = -22.69426;
   d_x_s1_end = -22.777409;
   d_x_s1 = (d_x_s1_begin+d_x_s1_end)/2;

   %sigma_x_s1_before = sqrt(3.2847e4);
   %sigma_x_s1_after = sqrt(3.30904e4);
   %sigma_x_s1 = (sigma_x_s1_before + sigma_x_s1_after)/2;

   %sigma_y_s1_before = sqrt(5.99767e2);
   %sigma_y_s1_after = sqrt(6.03824e2);
   %sigma_y_s1 = (sigma_y_s1_before + sigma_y_s1_after)/2; 

   %sigma_x_s0_before = sqrt(6.39495e3);
   %sigma_x_s0_after = sqrt(4.8169844e3);
   %sigma_x_s0 = (sigma_x_s0_before + sigma_x_s0_after)/2;

   %sigma_y_s0_before = sqrt(1.8118e3);
   %sigma_y_s0_after = sqrt(1.971161e3);
   %sigma_y_s0 = (sigma_y_s0_before + sigma_y_s0_after)/2;  

   % Define matrices
   R_D4 = [1 L_D4; 0 1];
   R_D3 = [1 L_D3; 0 1];
   R_D2 = [1 L_D2; 0 1];
   R_D1 = [1 L_D1; 0 1];
   R_D0 = [1 L_D0; 0 1];    
   R_S1_half = [1 L_S1/2; 0 1];
   R_S0_half = [1 L_S0/2; 0 1];

   %R_QF1_x = [cos(sqrt(k_qf1)*L_QF1) 1/sqrt(k_qf1)*sin(sqrt(k_qf1)*L_QF1); -sqrt(k_qf1)*sin(sqrt(k_qf1)*L_QF1) cos(sqrt(k_qf1)*L_QF1)];
   %R_QD0_x = [cosh(sqrt(k_qd0)*L_QD0) 1/sqrt(k_qd0)*sinh(sqrt(k_qd0)*L_QD0); sqrt(k_qd0)*sinh(sqrt(k_qd0)*L_QD0) cosh(sqrt(k_qd0)*L_QD0)];
   R_QD0_y = [cos(sqrt(k_qd0)*L_QD0) 1/sqrt(k_qd0)*sin(sqrt(k_qd0)*L_QD0); -sqrt(k_qd0)*sin(sqrt(k_qd0)*L_QD0) cos(sqrt(k_qd0)*L_QD0)];
   R_QF1_y = [cosh(sqrt(k_qf1)*L_QF1) 1/sqrt(k_qf1)*sinh(sqrt(k_qf1)*L_QF1); sqrt(k_qf1)*sinh(sqrt(k_qf1)*L_QF1) cosh(sqrt(k_qf1)*L_QF1)];

   %if(use_mp == 1)
   %    %R_S0 = [1 L_S0/2; 0 1]*[1 0; (k_s0/E)*2*(d_x_s0*(E-E_ref)) 1]*[1 L_S0/2; 0 1];
   %    R_S0_y = create_sextupole_matrix(L_S0, k_s0, E, E_ref, d_x_s0_begin, d_x_s0_end, N_lenses);
   %else
   %    R_S0_y = [1 L_S0; 0 1];
   %end
   %if(use_mp == 1)
   %    %R_S1 = [1 L_S1/2; 0 1]*[1 0; (k_s1/E)*2*(d_x_s1*(E-E_ref)) 1]*[1 L_S1/2; 0 1];
   %    R_S1_y = create_sextupole_matrix(L_S1, k_s1, E, E_ref, d_x_s1_begin, d_x_s1_end, N_lenses);
   %else
   %    R_S1_y = [1 L_S1; 0 1];
   %end

   % Intit beam
   %if(use_macroparticle == 1)
   %    x0 = [x_m1 + d_x_m1*(E-E_ref); xp_m1 + d_xp_m1*(E-E_ref)];
   %    y0 = [0; 0];
   %else
       x0 = [x_init; xp_init];
       y0 = [y_init; yp_init];
   %end

   % Track to center of S1

   %x1 = R_D4*x0;
   %x2_1 = R_S1_half*x1;
   y1 = R_D4*y0;
   %y2_1 = R_S1_half*y1;

   % Apply S1 kick

   %if(use_mp==1)
   %    % Sextupole thin lense kick
   %    if(use_macroparticle == 1)
   %        sigma_x_2 = sigma_x_s1^2 + x2_1(1)^2;
   %        sigma_y_2 = sigma_y_s1^2 + y2_1(1)^2;
   %    else
   %        sigma_x_2 = x2_1(1)^2;
   %        sigma_y_2 = y2_1(1)^2;
   %    end
   %    x2_2 = [x2_1(1); x2_1(2) + (k_s1/E)*(sigma_y_2 - sigma_x_2)];
   %    x2 = R_S1_half*x2_2;
   %else
   %    x2 = R_S1_half*x2_1;
   %end
   if(use_mp==1)
       % Sextupole thin lense kick
       %if(use_macroparticle == 1)
           sigma_x = d_x_s1*(E-E_ref);
       %else
       %sigma_x = x2_1(1);
       %end
       y2 = R_S1_half*y1;   
       y2 = [y2(1); y2(2) + 2*(k_s1/E)*sigma_x*y2(1)];
       y2 = R_S1_half*y2;
       %R_S1_y = create_sextupole_matrix(L_S1, k_s1, E, E_ref, d_x_s1_begin, d_x_s1_end, N_lenses);
       %y2 = R_S1_y*y1;
   else
       %y2 = R_S1_half*y2_1;
       y2 = R_S1_half*R_S1_half*y1;
   end

   % Track till the center of S0

   %x3 = R_D3*x2;
   %x4 = R_QF1_x*x3;
   %x5 = R_D2*x4;
   %x6_1 = R_S0_half*x5;
   y3 = R_D3*y2;
   y4 = R_QF1_y*(y3 + [-y_qf1; 0]) - [-y_qf1; 0];
   y5 = R_D2*y4;
   %y6_1 = R_S0_half*y5;

   % Apply S0 kick

   %if(use_mp == 1)
   %    % Sextupole thin lense kick
   %    if(use_macroparticle == 1)
   %        sigma_x_2 = sigma_x_s0^2 + x6_1(1)^2;
   %        sigma_y_2 = sigma_y_s0^2 + y6_1(1)^2;
   %    else
   %        sigma_x_2 = x6_1(1)^2;
   %        sigma_y_2 = y6_1(1)^2;
   %    end
   %    %sigma_x_2 = x6_1(1)^2;
   %    %sigma_y_2 = y6_1(1)^2;
   %    x6_2 = [x6_1(1); x6_1(2) + (k_s0/E)*(sigma_y_2 - sigma_x_2)];
   %    x6 = R_S0_half*x6_2;
   %else
   %    x6 = R_S0_half*x6_1;
   %end
   if(use_mp == 1)
       %if(use_macroparticle == 1)
          sigma_x = d_x_s0*(E-E_ref);
       %else
       %sigma_x = x6_1(1);
       %end
       y6 = R_S0_half*y5;
       y6 = [y6(1); y6(2) + 2*(k_s0/E)*sigma_x*y6(1)];
       y6 = R_S0_half*y6;
       %R_S0_y = create_sextupole_matrix(L_S0, k_s0, E, E_ref, d_x_s0_begin, d_x_s0_end, N_lenses); 
       %y6 = R_S0_y*y5;
   else
       y6 = R_S0_half*R_S0_half*y5;
   end

   % Track to the IP

   %x7 = R_D1*x6;
   %x8 = R_QD0_x*x7;
   %x9 = R_D0*x8;
   y7 = R_D1*y6;
   y8 = R_QD0_y*(y7 + [-y_qd0; 0]) - [-y_qd0; 0];
   y9 = R_D0*y8;

   %offset_x = x9(1);
   offset_x = 0;
   offset_y = y9(1);

   % variante 2
   %R = R_D0*R_QD0_y*R_D1*R_S0_half
   %offset_x = a_1 * r_11^2 * y_qf1^2 / E;


end

end