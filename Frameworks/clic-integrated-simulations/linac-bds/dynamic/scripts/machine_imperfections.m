% Script to add imperfections to the accelerator
% before a new run over the specified time steps
%
% Juergen Pfingstner

time_step_index = 0;
Tcl_SetVar("time_step_index", time_step_index);
global_time = 0;

%Start from non-perfect machine:
if (aligned_machine == 0) 
  % add ATL ground motion (both in x and y)
  Tcl_SetVar("atl_time", atl_time_initial);
  Tcl_Eval("ground_motion_long electron");
  Tcl_Eval("ground_motion_long positron");
  Tcl_SetVar("atl_time", delta_T_short);
  if (ground_motion_x == 0 )
    for i=electron:positron
      placet_element_set_attribute(beamlinename(i,:),all_index(:,i),"x",0);
      placet_element_set_attribute(beamlinename(i,:),all_index(:,i),"xp",0);
    end
  end
  if (ground_motion_y == 0 )
    for i=electron:positron
      placet_element_set_attribute(beamlinename(i,:),all_index(:,i),"y",0);
      placet_element_set_attribute(beamlinename(i,:),all_index(:,i),"yp",0);
    end
  end
  
  % add misalignment (to be done)
end

if (use_bpm_drift == 1)
  disp('Apply BPM misalignments (y only)')
  for i=electron:positron
    line_name = beamlinename(i,:);
    placet_element_set_attribute(line_name,bpm_index_ml(i,:),"y",bpm_drift_sigma_ml*normrnd(0,1,1,length(bpm_index_ml(i,:))));
    placet_element_set_attribute(line_name,bpm_index_bds(i,1:(end)),"y",bpm_drift_sigma_bds*normrnd(0,1,1,length(bpm_index_bds(i,:))));
    placet_element_set_attribute(line_name,bpm_index_bds(i,(end-25):end),"y",bpm_drift_sigma_ff*normrnd(0,1,1,26));
    placet_element_set_attribute(line_name,bpm_index_bds(i,(end-1):end),"y",bpm_drift_sigma_fd*normrnd(0,1,1,2));
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Add inital beam errors % 
%%%%%%%%%%%%%%%%%%%%%%%%%%%

if(use_initial_beam_variation == 1)
    energy_amplification = 1.0+initial_energy_var_const;
    Tcl_SetVar("energy_amplification", energy_amplification);
end

%%%%%%%%%%%%%%%%%%%%%%%%
% Add static RF errors %
%%%%%%%%%%%%%%%%%%%%%%%%

if(use_rf_jitter == 1)
  Tcl_Eval('BeamlineUse -name electron');
  Tcl_Eval('set dynamic_effects::cav_g0 $grad_init_elec');
  Tcl_Eval('set dynamic_effects::cav_ph0 $cav_init_elec');
  Tcl_Eval('dynamic_effects::rf_jitter $phi_linac_machine_const $phi_linac_machine $phi_decel_machine $grad_linac_machine_const $grad_linac_machine $grad_decel_machine');
  Tcl_Eval('set cav0_elec [CavityGetPhaseList]');
  Tcl_Eval('set grad0_elec [CavityGetGradientList]');

  Tcl_Eval('BeamlineUse -name positron');
  Tcl_Eval('set dynamic_effects::cav_g0 $grad_init_posi');
  Tcl_Eval('set dynamic_effects::cav_ph0 $cav_init_posi');
  Tcl_Eval('dynamic_effects::rf_jitter $phi_linac_machine_const $phi_linac_machine $phi_decel_machine $grad_linac_machine_const $grad_linac_machine $grad_decel_machine');
  Tcl_Eval('set cav0_posi [CavityGetPhaseList]');
  Tcl_Eval('set grad0_posi [CavityGetGradientList]');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Apply Quadrupole strength jitter %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if (use_quad_strength_jitter==1)

  disp('Apply Quad Strength jitter permanent')
  for i=electron:positron
    line_name = beamlinename(i,:);
	
    qp_strength_ml(i,:) = qp_strength_ml_init(i,:).*(1+quad_strength_sigma_ml_machine*normrnd(0,1,1,length(qp_index_ml(i,:))));
    qp_strength_bds(i,:) = qp_strength_bds_init(i,:).*(1+quad_strength_sigma_bds_machine*normrnd(0,1,1,length(qp_index_bds(i,:))));
    qp_strength_ff(i,:) = qp_strength_ff_init(i,:).*(1+quad_strength_sigma_ff_machine*normrnd(0,1,1,length(qp_index_ff(i,:))));
    qp_strength_fd(i,:) = qp_strength_fd_init(i,:).*(1+quad_strength_sigma_fd_machine*normrnd(0,1,1,length(qp_index_fd(i,:))));

    placet_element_set_attribute(line_name,qp_index_ml(i,:),"strength",qp_strength_ml(i,:));
    placet_element_set_attribute(line_name,qp_index_bds(i,:),"strength",qp_strength_bds(i,:));
    placet_element_set_attribute(line_name,qp_index_ff(i,:),"strength",qp_strength_ff(i,:));
    placet_element_set_attribute(line_name,qp_index_fd(i,:),"strength",qp_strength_fd(i,:));
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Create corrector scaling errors %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if(stabilization_scaling_error_x == 1) 
    stabilization_scaling_values_x = ones(size(corr_index)); 
    if((strcmp(stabilization_scaling_section, 'ML')) | (strcmp(stabilization_scaling_section, 'ALL')))
      fprintf(1,'Init stabilization scaling errors: x, ML\n');
      for j=electron:positron
        for i=1:nr_corr_ml(j) 
          stabilization_scaling_values_x(j,i) = 1+stabilization_scaling_sigma*randn(1,1);
        end
      end
    end
    if(strcmp(stabilization_scaling_section, 'BDS') | (strcmp(stabilization_scaling_section, 'ALL')))
      fprintf(1,'Init stabilization scaling errors: x, BDS\n');
      for j=electron:positron
        for i=(nr_corr_ml(j)+1):length(corr_index)
          stabilization_scaling_values_x(j,i) = 1+stabilization_scaling_sigma*randn(1,1);
        end
      end
    end    
end

if(stabilization_scaling_error_y == 1) 
    stabilization_scaling_values_y = ones(size(corr_index)); 
    if((strcmp(stabilization_scaling_section, 'ML')) | (strcmp(stabilization_scaling_section, 'ALL')))
      fprintf(1,'Init stabilization scaling errors: y, ML\n');
      for j=electron:positron
        for i=1:nr_corr_ml(j) 
          stabilization_scaling_values_y(j,i) = 1+stabilization_scaling_sigma*randn(1,1);
        end
      end
    end
    if(strcmp(stabilization_scaling_section, 'BDS') | (strcmp(stabilization_scaling_section, 'ALL')))
      fprintf(1,'Init stabilization scaling errors: y, BDS\n');
      for j=electron:positron
        for i=(nr_corr_ml(j)+1):length(corr_index)
          stabilization_scaling_values_y(j,i) = 1+stabilization_scaling_sigma*randn(1,1);
        end
      end
    end    
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Create bpm scaling errors %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if(bpm_scaling_error_x == 1) 
    bpm_scaling_values_x = ones(size(bpm_index)); 
    if((strcmp(bpm_scaling_section, 'ML')) | (strcmp(bpm_scaling_section, 'ALL')))
      fprintf(1,'Init BPM scaling errors: x, ML\n');
      for j=electron:positron
        for i=1:(nr_corr_ml(j)-1)
          bpm_scaling_values_x(j,i) = 1+bpm_scaling_sigma*randn(1,1);
        end
      end
    end
    if(strcmp(bpm_scaling_section, 'BDS') | (strcmp(bpm_scaling_section, 'ALL')))
      fprintf(1,'Init BPM scaling errors: BDS\n');
      for j=electron:positron
        for i=nr_corr_ml(j):length(bpm_index)
          bpm_scaling_values_x(j,i) = 1+bpm_scaling_sigma*randn(1,1);
        end
      end
    end    
end

if(bpm_scaling_error_y == 1) 
    bpm_scaling_values_y = ones(size(bpm_index)); 
    if((strcmp(bpm_scaling_section, 'ML')) | (strcmp(bpm_scaling_section, 'ALL')))
      for j=electron:positron
        for i=1:(nr_corr_ml(j)-1) 
          bpm_scaling_values_y(j,i) = 1+bpm_scaling_sigma*randn(1,1);
        end
      end
    end
    if(strcmp(bpm_scaling_section, 'BDS') | (strcmp(bpm_scaling_section, 'ALL')))
      for j=electron:positron
        for i=(nr_corr_ml(j)):length(bpm_index)
          bpm_scaling_values_y(j,i)= 1+bpm_scaling_sigma*randn(1,1);
        end
      end
    end    
end


%%%%%%%%%%%%%%%%%
% Failure Modes %
%%%%%%%%%%%%%%%%%

if (use_bpm_failure)
  bpm_failure = zeros(length(bpm_index),2);
  for i=electron:positron
    % sets bpm failure to 1 if failing
    puts("BPM failure: nr of failed bpms: ");
    nr_failed_bpms = round(bpm_failure_prob*nr_bpm(i))
    bpm_failure_distribution
    % add protection against 0 failed
    if (strcmp(bpm_failure_distribution,'random'))
      bpm_failure(:,i) = floor(rand(length(bpm_index(i,:)),1)+bpm_failure_prob);
    elseif (strcmp(bpm_failure_distribution,'block_begin'))
      bpm_failure(1:nr_failed_bpms,i) = 1;
    elseif (strcmp(bpm_failure_distribution,'block_end'))
      bpm_failure(end+1-nr_failed_bpms:end,i) = 1;
    elseif (strcmp(bpm_failure_distribution,'block_end_ml'))
      bpm_failure(nr_corr_ml(i)+1-nr_failed_bpms:nr_corr_ml(i),i) = 1;
    elseif (strcmp(bpm_failure_distribution,'block_begin_bds'))
      bpm_failure(nr_corr_ml(i)+1:nr_corr_ml(i)+nr_failed_bpms,i) = 1;
    elseif (strcmp(bpm_failure_distribution,'list'))
      for j=1:length(bpm_failure_list)
        bpm_failure(bpm_failure_list(j)) = 1;
      end
    else
      puts("BPM failure distribution is not implemented yet");
      exit(1);
    end
  end
end

if (use_corr_failure)
  corr_failure = zeros(length(corr_index),2);
  for i=electron:positron
    % sets corr failure to 1 if failing
    puts("Corrector failure: nr of failed correctors: ");
    nr_failed_corrs = round(corr_failure_prob*nr_corr(i))
    corr_failure_distribution
    if (strcmp(corr_failure_distribution,'random'))
      corr_failure(:,i) = floor(rand(length(corr_index(i,:)),1)+corr_failure_prob);
    elseif (strcmp(corr_failure_distribution,'block_begin'))
      corr_failure(1:nr_failed_corrs,i) = 1;
    elseif (strcmp(corr_failure_distribution,'block_end'))
      corr_failure(end+1-nr_failed_corrs:end,i) = 1;
    elseif (strcmp(corr_failure_distribution,'block_end_ml'))
      corr_failure(nr_corr_ml(i)+1-nr_failed_corrs:nr_corr_ml(i),i) = 1;
    elseif (strcmp(corr_failure_distribution,'block_begin_bds'))
      corr_failure(nr_corr_ml(i)+1:nr_corr_ml(i)+nr_failed_corrs,i) = 1;
    elseif (strcmp(corr_failure_distribution,'list'))
      for j=1:length(corr_failure_list)
        corr_failure(corr_failure_list(j)) = 1;
      end
    else
      puts("CORR failure distribution is not implemented yet");
      exit(1);
    end
  end
end