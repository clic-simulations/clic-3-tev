%% Script to apply one-time corrections 
%%
%% Juergen Pfingstner
%% 17th of April 2012

function []=one_time_corr_apply(beamlinename, corr_index, corrections_x, corrections_y, electron, positron);

%% At the moment only implemented for QP correctors
for i=electron:positron 
    line_name = beamlinename(i,:);
      
    %% QP x
    corr_temp = placet_element_get_attribute(line_name, corr_index, 'x');
    corr_temp = corr_temp + corrections_x(i,:);
    placet_element_set_attribute(line_name, corr_index, 'x', corr_temp);
      
    %% QP y
    corr_temp = placet_element_get_attribute(line_name, corr_index, 'y');
    corr_temp = corr_temp + corrections_y(i,:);
    placet_element_set_attribute(line_name, corr_index, 'y', corr_temp);
end    

