% Script to create the BPMs and correctors
%
% Juergen Pfingstner

sffknobname = [ "SF6"; "SF5"; "SD4"; "SF1"; "SD0" ];

for i=electron:positron
  line_name = beamlinename(i,:);
  all_index(i,:) = placet_get_name_number_list(line_name,'*');
  bpm_index(i,:) = placet_get_number_list(line_name, 'bpm');
  dipole_index(i,:) = placet_get_number_list(line_name, 'dipole');
  qp_index(i,:) = placet_get_number_list(line_name, 'quadrupole');
  ipdipole_index(i,:) = placet_get_name_number_list(line_name, "IPDIPOLE");
  s2p_index(i,:) = placet_get_name_number_list(line_name, "S2P");
  bpmsknob_index(i,:) = placet_get_name_number_list(line_name, 'BPMSFF');
  for j=1:length(sffknobname)
    sffknob_index(i,j) = placet_get_name_number_list(line_name, sffknobname(j,:));
  end
  nr_bpm(i) = length(bpm_index(i,:));
  nr_quad(i) = length(qp_index(i,:));
  if (use_main_linac)
    nr_corr_ml(i) = 2010;
    nr_quad_ml(i) = 2010;
  else
    nr_corr_ml(i) = 0;
    nr_quad_ml(i) = 0;
  end

  qp_index_ml(i,:) = qp_index(i,1:nr_corr_ml(i));
  qp_index_bds(i,:) = qp_index(i,nr_corr_ml(i)+1:end);
  qp_index_ff(i,:) = qp_index(i,end-14:end);
  qp_index_fd(i,:) = qp_index(i,end-1:end);
  qp_index_qf1(i,:) = qp_index(i,end-1);
  qp_index_qd0(i,:) = qp_index(i,end);

  qp_strength_ml_init(i,:) = placet_element_get_attribute(line_name, qp_index_ml(i,:), 'strength');
  qp_strength_bds_init(i,:) = placet_element_get_attribute(line_name, qp_index_bds(i,:), 'strength');
  qp_strength_ff_init(i,:) = placet_element_get_attribute(line_name, qp_index_ff(i,:), 'strength');
  qp_strength_fd_init(i,:) = placet_element_get_attribute(line_name, qp_index_fd(i,:), 'strength');

  if( use_beam_beam == 1)
    ip_kicker_index(i,:) = dipole_index(i,end);
  end

  if (dipole_correctors == 1)
    % Dipoles
    if( (use_main_linac == 1) & (use_bds == 0) )
      corr_index(i,:) = dipole_index(i,1:nr_corr_ml(i));
      bpm_index(i,:) = bpm_index(i,1:nr_corr_ml(i));
    elseif( (use_main_linac == 0) & (use_bds == 1) )
      corr_index(i,:) = dipole_index(i,1:(end-2));
      bpm_index(i,:) = bpm_index(i,1:end);
    elseif( (use_main_linac == 1) & (use_bds == 1) )
      corr_index(i,:) = dipole_index(i,1:(end-2));
      bpm_index(i,:) = bpm_index(i,1:end);
    end
  else
    % QP
    if( (use_main_linac == 1) & (use_bds == 0) )
      corr_index(i,:) = qp_index(i,1:nr_corr_ml(i));
      bpm_index(i,:) = bpm_index(i,1:nr_corr_ml(i));
    elseif( (use_main_linac == 0) & (use_bds == 1) )
      corr_index(i,:) = qp_index(i,1:(end-2));
      bpm_index(i,:) = bpm_index(i,1:end);
    elseif( (use_main_linac == 1) & (use_bds == 1) )
      corr_index(i,:) = qp_index(i,1:(end-2));
      %% Debug -> only for the R calculation
      %%corr_index(i,:) = qp_index(i,1:end);
      %% dEBUG
      bpm_index(i,:) = bpm_index(i,1:end);
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% Create correspondance between QPs and BPMs if they are mounted %
    %% to each other (BPMQUAD). Every QP has a BPM (apart from the    %
    %% first), but not vice versa.                                    %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    % Main linac
    bpm_quad(:,1) = 1:length(qp_index(i,:));
    bpm_quad(2:2010, 2) = 1:2009;
    
    % BDS
    bpm_name = placet_element_get_attribute(line_name, bpm_index(i,:), 'name')';
    qp_counter = nr_corr_ml(i)+1;
    for bpm_counter = (nr_corr_ml(i)+1):nr_bpm(i)
      if(strcmp(bpm_name(:,bpm_counter)', 'BPMQUAD'))
        % BPM-QUAD (not a single standing one)
        bpm_quad(qp_counter, 2) = bpm_counter;
        qp_counter = qp_counter + 1;
      end
    end
    
    % The first QP has no BPM and QF1 and QD0 are not used
    bpm_quad_nofd = bpm_quad(2:(end-2), :);
    bpm_move_index(i,:) = bpm_index(i,bpm_quad_nofd(:,2));
    
    clear bpm_quad_nofd bpm_quad qp_counter bpm_counter;
  end
  nr_corr(i) = length(corr_index(i,:));
  % Get s-position in middle of correctors.
  corr_pos(i,:) = placet_element_get_attribute(line_name, corr_index(i,:), 's')';
  corr_length(i,:) = placet_element_get_attribute(line_name, corr_index(i,:), 'length')';
  corr_center_x(i,:) = corr_pos(i,:) - corr_length(i,:)/2;
  corr_center_y(i,:) = corr_center_x(i,:);
  clear corr_length;
end

corr_settings_old_x = zeros(length(corr_index), 2);
corr_settings_new_x = zeros(length(corr_index), 2);
corr_settings_old_y = zeros(length(corr_index), 2);
corr_settings_new_y = zeros(length(corr_index), 2);

corr_values_old_x = zeros(length(corr_index), 2);
corr_values_new_x = zeros(length(corr_index), 2);
corr_values_old_y = zeros(length(corr_index), 2);
corr_values_new_y = zeros(length(corr_index), 2);

corr_increment_x = zeros(length(corr_index), 2);
corr_increment_y = zeros(length(corr_index), 2);

gm_hat_estimated_x = zeros(length(corr_index), 2);
gm_hat_estimated_y = zeros(length(corr_index), 2);

if(stabilization_scaling_error_x == 1)
    stabilization_scaling_values_x = ones(size(corr_index)); 
end

if(stabilization_scaling_error_y == 1)
    stabilization_scaling_values_y = ones(size(corr_index)); 
end

if(bpm_scaling_error_x == 1)
    bpm_scaling_values_x = ones(size(bpm_index)); 
end

if(bpm_scaling_error_y == 1)
    bpm_scaling_values_y = ones(size(bpm_index)); 
end

if (use_bpm_failure == 1)
  bpm_failure = zeros(length(bpm_index),2);
end
