% Script to adjust settings, properties or imperfections specific
% for a certain time step
%
% Juergen Pfingstner

%%%%%%%%%%%%%%%%%%%%%%%
% Apply ground motion %
%%%%%%%%%%%%%%%%%%%%%%%

if((ground_motion_x == 1) | (ground_motion_y == 1))
  Tcl_Eval("ground_motion electron $groundmotion(type)");
  Tcl_Eval("ground_motion positron $groundmotion(type)");
end

%%%%%%%%%%%%%%%%%%%
% Apply RF jitter %
%%%%%%%%%%%%%%%%%%%

if(use_rf_jitter == 1)
  Tcl_Eval('BeamlineUse -name electron');
  Tcl_Eval('set dynamic_effects::cav_g0 $grad0_elec');
  Tcl_Eval('set dynamic_effects::cav_ph0 $cav0_elec');
  Tcl_Eval('dynamic_effects::rf_jitter 0 $phi_linac $phi_decel 0 $grad_linac $grad_decel');

  Tcl_Eval('BeamlineUse -name positron');
  Tcl_Eval('set dynamic_effects::cav_g0 $grad0_posi');
  Tcl_Eval('set dynamic_effects::cav_ph0 $cav0_posi');
  Tcl_Eval('dynamic_effects::rf_jitter 0 $phi_linac $phi_decel 0 $grad_linac $grad_decel');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Apply Quadrupole strength jitter %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if (use_quad_strength_jitter==1)

  disp('Apply Quad Strength jitter')
  for i=electron:positron
    line_name = beamlinename(i,:);

    placet_element_set_attribute(line_name,qp_index_ml(i,:),"strength",qp_strength_ml(i,:).*(1+quad_strength_sigma_ml*normrnd(0,1,1,length(qp_index_ml(i,:)))));
    placet_element_set_attribute(line_name,qp_index_bds(i,:),"strength",qp_strength_bds(i,:).*(1+quad_strength_sigma_bds*normrnd(0,1,1,length(qp_index_bds(i,:)))));
    placet_element_set_attribute(line_name,qp_index_ff(i,:),"strength",qp_strength_ff(i,:).*(1+quad_strength_sigma_ff*normrnd(0,1,1,length(qp_index_ff(i,:)))));
    placet_element_set_attribute(line_name,qp_index_fd(i,:),"strength",qp_strength_fd(i,:).*(1+quad_strength_sigma_fd*normrnd(0,1,1,length(qp_index_fd(i,:)))));
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Apply Quadrupole position jitter %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if (use_quad_position_jitter==1)

  disp('Apply Quad position jitter (y only)')
  for i=electron:positron
    line_name = beamlinename(i,:);
    placet_element_set_attribute(line_name,qp_index_ml(i,:),"y",quad_position_sigma_ml*normrnd(0,1,1,length(qp_index_ml(i,:))));
    placet_element_set_attribute(line_name,qp_index_bds(i,:),"y",quad_position_sigma_bds*normrnd(0,1,1,length(qp_index_bds(i,:))));
    placet_element_set_attribute(line_name,qp_index_ff(i,:),"y",quad_position_sigma_ff*normrnd(0,1,1,length(qp_index_ff(i,:))));
    placet_element_set_attribute(line_name,qp_index_fd(i,:),"y",quad_position_sigma_fd*normrnd(0,1,1,length(qp_index_fd(i,:))));
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Apply other imperfections ... (to be added) %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
