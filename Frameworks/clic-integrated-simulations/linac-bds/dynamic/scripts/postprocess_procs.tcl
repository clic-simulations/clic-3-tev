## Workaround for a Tcl - Octave problem
##
## Juergen Pfingstner
## 14th of December 2009

#############################################
# Implementation of the different functions #
#############################################


# Calculates the multipulse emittance for 2nd order beam
# shift: 1 ... shift combined bunch by its center
#        0 ... no shift of the combined bunch
proc calc_emitt_ellipse {name n_start n_stop shift} {

	set w_sum 0.0
	set y_sum 0.0
	set yp_sum 0.0
	set x_sum 0.0
	set xp_sum 0.0
	set y_sum2 0.0
	set yyp_sum2 0.0
	set yp_sum2 0.0
	set x_sum2 0.0
	set xxp_sum2 0.0
	set xp_sum2 0.0
	set e_sum 0.0
	for {set i $n_start} {$i<=$n_stop} {incr i} {
	    set file [open $name$i.dat r]
	    while {[gets $file line] >= 0} {
		if {$line!=""} {
		    set wgt [lindex $line 1]
		    set x [lindex $line 3]
		    set xp [lindex $line 4]
		    set y [lindex $line 5]
		    set yp [lindex $line 6]
		    set w_sum [expr $w_sum+$wgt]
		    set e_sum [expr $e_sum+$wgt*[lindex $line 2]]
		    set x_sum [expr $x_sum+$wgt*$x]
		    set xp_sum [expr $xp_sum+$wgt*$xp]
		    set y_sum [expr $y_sum+$wgt*$y]
		    set yp_sum [expr $yp_sum+$wgt*$yp]
		    set x_sum2 [expr $x_sum2+$wgt*($x*$x+[lindex $line 7])]
		    set xxp_sum2 [expr $xxp_sum2+$wgt*($x*$xp+[lindex $line 8])]
		    set xp_sum2 [expr $xp_sum2+$wgt*($xp*$xp+[lindex $line 9])]
		    set y_sum2 [expr $y_sum2+$wgt*($y*$y+[lindex $line 10])]
		    set yyp_sum2 [expr $yyp_sum2+$wgt*($yp*$y+[lindex $line 11])]
		    set yp_sum2 [expr $yp_sum2+$wgt*($yp*$yp+[lindex $line 12])]
		}
	    }
	    close $file
	}
	set e_sum [expr $e_sum/$w_sum]
        if {$shift == 0} {
            set y_sum 0.0
	    set yp_sum 0.0
	    set x_sum 0.0
	    set xp_sum 0.0
	} else {
	    set y_sum [expr $y_sum/$w_sum]
	    set yp_sum [expr $yp_sum/$w_sum]
	    set x_sum [expr $x_sum/$w_sum]
	    set xp_sum [expr $xp_sum/$w_sum]
	}
    
	set y_sum2 [expr $y_sum2/$w_sum-$y_sum*$y_sum]
	set yyp_sum2 [expr $yyp_sum2/$w_sum-$y_sum*$yp_sum]
	set yp_sum2 [expr $yp_sum2/$w_sum-$yp_sum*$yp_sum]
	set x_sum2 [expr $x_sum2/$w_sum-$x_sum*$x_sum]
	set xp_sum2 [expr $xp_sum2/$w_sum-$xp_sum*$xp_sum]
	set xxp_sum2 [expr $xxp_sum2/$w_sum-$x_sum*$xp_sum]
	set emitt_x [expr sqrt(abs($x_sum2*$xp_sum2-$xxp_sum2*$xxp_sum2))*$e_sum/0.511e-3*1e-3]
	set emitt_y [expr sqrt(abs($y_sum2*$yp_sum2-$yyp_sum2*$yyp_sum2))*$e_sum/0.511e-3*1e-3]
	
        array set emitt_data {
	    emitt_x
	    emitt_y
	}

	set emitt_data(emitt_x) $emitt_x
	set emitt_data(emitt_y) $emitt_y

	array get emitt_data
}

# Calculates the approximated multi-pulse emittance using some intermediate emitt.
# shift: 1 ... shift combined bunch by its center
#        0 ... no shift of the combined bunch

proc calc_emitt_approx {name n_start n_stop shift} {
    

    ##################
    # Init variables #
    ##################

    set y_sum 0.0
    set yp_sum 0.0
    set x_sum 0.0
    set xp_sum 0.0
    set y_sum2 0.0
    set yyp_sum2 0.0
    set yp_sum2 0.0
    set x_sum2 0.0
    set xxp_sum2 0.0
    set xp_sum2 0.0
    set e_sum 0.0
    set emitt_s_x_sum 0.0
    set emitt_s_y_sum 0.0
    set emitt_s_x 0.0
    set emitt_s_y 0.0
    set xx_s_sum 0.0
    set xxp_s_sum 0.0
    set xpxp_s_sum 0.0
    set yy_s_sum 0.0
    set yyp_s_sum 0.0
    set ypyp_s_sum 0.0
    set nr_part 0

    ####################################################
    # Load the data from files                         #
    #                                                  #
    # At the same time average single pulse parameters #
    # and calc the jitter parameters                   #
    ####################################################

    for {set i $n_start} {$i<=$n_stop} {incr i} {

	read_array $name$i

	set nr_part [expr $nr_part + 1]
	set emitt_x_s [expr $[format $name$i](emitt_x)] 
	set emitt_y_s [expr $[format $name$i](emitt_y)] 
	set emitt_x_s [expr $emitt_x_s * 100]
	set emitt_y_s [expr $emitt_y_s * 100]
	set e [expr $[format $name$i](e)]
	set xx_s [expr $[format $name$i](sxx)] 
	set xxp_s [expr $[format $name$i](sxxp)]
	set xpxp_s [expr $[format $name$i](sxpxp)] 
	set yy_s [expr $[format $name$i](syy)] 
	set yyp_s [expr $[format $name$i](syyp)]
	set ypyp_s [expr $[format $name$i](sypyp)] 
	set x [expr $[format $name$i](x)] 
	set y [expr $[format $name$i](y)] 
	set xp [expr $[format $name$i](xp)] 
	set yp [expr $[format $name$i](yp)] 
	
	set e_sum [expr $e_sum+$e]
	set emitt_s_x_sum [expr $emitt_s_x_sum+$emitt_x_s]
	set emitt_s_y_sum [expr $emitt_s_y_sum+$emitt_y_s]
	set xx_s_sum [expr $xx_s_sum+$xx_s]
	set xxp_s_sum [expr $xxp_s_sum+$xxp_s]
	set xpxp_s_sum [expr $xpxp_s_sum+$xpxp_s]
	set yy_s_sum [expr $yy_s_sum+$yy_s]
	set yyp_s_sum [expr $yyp_s_sum+$yyp_s]
	set ypyp_s_sum [expr $ypyp_s_sum+$ypyp_s]
	set x_sum [expr $x_sum+$x]
	set xp_sum [expr $xp_sum+$xp]
	set y_sum [expr $y_sum+$y]
	set yp_sum [expr $yp_sum+$yp]

	set x_sum2 [expr $x_sum2+$x*$x]
	set xxp_sum2 [expr $xxp_sum2+$x*$xp]
	set xp_sum2 [expr $xp_sum2+$xp*$xp]
	set y_sum2 [expr $y_sum2+$y*$y]
	set yyp_sum2 [expr $yyp_sum2+$yp*$y]
	set yp_sum2 [expr $yp_sum2+$yp*$yp]
    }

    ##################################################################
    # Subtract the multi-beam center and average single-pulse values #
    ##################################################################
    
    set e_sum [expr $e_sum/$nr_part]
    set emitt_s_x [expr $emitt_s_x_sum/$nr_part]
    set emitt_s_y [expr $emitt_s_y_sum/$nr_part]
    set xx_s_sum [expr $xx_s_sum/$nr_part]
    set xxp_s_sum [expr $xxp_s_sum/$nr_part]
    set xpxp_s_sum [expr $xpxp_s_sum/$nr_part]
    set yy_s_sum [expr $yy_s_sum/$nr_part]
    set yyp_s_sum [expr $yyp_s_sum/$nr_part]
    set ypyp_s_sum [expr $ypyp_s_sum/$nr_part]

    if {$shift == 0} {
	set y_sum 0.0
	set yp_sum 0.0
	set x_sum 0.0
	set xp_sum 0.0
    } else {
	set y_sum [expr $y_sum/$nr_part]
	set yp_sum [expr $yp_sum/$nr_part]
	set x_sum [expr $x_sum/$nr_part]
	set xp_sum [expr $xp_sum/$nr_part]
    }

    set y_sum2 [expr $y_sum2/$nr_part-$y_sum*$y_sum]
    set yyp_sum2 [expr $yyp_sum2/$nr_part-$y_sum*$yp_sum]
    set yp_sum2 [expr $yp_sum2/$nr_part-$yp_sum*$yp_sum]
    set x_sum2 [expr $x_sum2/$nr_part-$x_sum*$x_sum]
    set xp_sum2 [expr $xp_sum2/$nr_part-$xp_sum*$xp_sum]
    set xxp_sum2 [expr $xxp_sum2/$nr_part-$x_sum*$xp_sum]

    
    ############################
    # Calculate the emittances #
    ############################

    set emitt_j0_x [expr sqrt(abs( $x_sum2*$xp_sum2 - $xxp_sum2*$xxp_sum2 ))*$e_sum/0.511e-3*1e-3]
    set emitt_j0_y [expr sqrt(abs( $y_sum2*$yp_sum2 - $yyp_sum2*$yyp_sum2 ))*$e_sum/0.511e-3*1e-3]
    set emitt_c_x [expr sqrt(abs( $xx_s_sum*$xp_sum2 + $xpxp_s_sum*$x_sum2 - 2*$xxp_s_sum*$xxp_sum2 ))*$e_sum/0.511e-3*1e-3]
    set emitt_c_y [expr sqrt(abs( $yy_s_sum*$yp_sum2 + $ypyp_s_sum*$y_sum2 - 2*$yyp_s_sum*$yyp_sum2 ))*$e_sum/0.511e-3*1e-3]
    set emitt_m_x [expr sqrt( $emitt_s_x*$emitt_s_x + $emitt_j0_x*$emitt_j0_x + $emitt_c_x*$emitt_c_x )]
    set emitt_m_y [expr sqrt( $emitt_s_y*$emitt_s_y + $emitt_j0_y*$emitt_j0_y + $emitt_c_y*$emitt_c_y )]
    set emitt_j_x [expr sqrt( $emitt_j0_x*$emitt_j0_x + $emitt_c_x*$emitt_c_x )]
    set emitt_j_y [expr sqrt( $emitt_j0_y*$emitt_j0_y + $emitt_c_y*$emitt_c_y )]

    array set emitt_data {
	emitt_m_x
	emitt_m_y
	emitt_s_x 
	emitt_s_y
	emitt_j_x
	emitt_j_y
	emitt_j0_x
	emitt_j0_y
	emitt_c_x
	emitt_c_y
    }

    set emitt_data(emitt_s_x) $emitt_s_x
    set emitt_data(emitt_s_y) $emitt_s_y
    set emitt_data(emitt_j_x) $emitt_j_x
    set emitt_data(emitt_j_y) $emitt_j_y
    set emitt_data(emitt_j0_x) $emitt_j0_x
    set emitt_data(emitt_j0_y) $emitt_j0_y
    set emitt_data(emitt_c_x) $emitt_c_x
    set emitt_data(emitt_c_y) $emitt_c_y

    array get emitt_data
}

# Calculates the multipulse emittance for a particle beam
# shift: 1 ... shift combined bunch by its center
#        0 ... no shift of the combined bunch
proc calc_emitt_particle {name n_start n_stop shift} {

	set w_sum 0.0
	set y_sum 0.0
	set yp_sum 0.0
	set x_sum 0.0
	set xp_sum 0.0
	set y_sum2 0.0
	set yyp_sum2 0.0
	set yp_sum2 0.0
	set x_sum2 0.0
	set xxp_sum2 0.0
	set xp_sum2 0.0
	set e_sum 0.0
	set nr_part 0
	for {set i $n_start} {$i<=$n_stop} {incr i} {
	    set file [open $name$i.dat r]
	    while {[gets $file line] >= 0} {
		if {$line!=""} {
		    set nr_part [expr $nr_part + 1]
		    set e [lindex $line 0]
		    set x [lindex $line 1]
		    set y [lindex $line 2]
		    set z [lindex $line 3]
		    set xp [lindex $line 4]
		    set yp [lindex $line 5]
      
		    set e_sum [expr $e_sum+$e]
		    set x_sum [expr $x_sum+$x]
		    set xp_sum [expr $xp_sum+$xp]
		    set y_sum [expr $y_sum+$y]
		    set yp_sum [expr $yp_sum+$yp]

		    set x_sum2 [expr $x_sum2+$x*$x]
		    set xxp_sum2 [expr $xxp_sum2+$x*$xp]
		    set xp_sum2 [expr $xp_sum2+$xp*$xp]
		    set y_sum2 [expr $y_sum2+$y*$y]
		    set yyp_sum2 [expr $yyp_sum2+$yp*$y]
		    set yp_sum2 [expr $yp_sum2+$yp*$yp]
		}
	    }
	    close $file
	}
	set e_sum [expr $e_sum/$nr_part]
        if {$shift == 0} {
            set y_sum 0.0
	    set yp_sum 0.0
	    set x_sum 0.0
	    set xp_sum 0.0
	} else {
	    set y_sum [expr $y_sum/$nr_part]
	    set yp_sum [expr $yp_sum/$nr_part]
	    set x_sum [expr $x_sum/$nr_part]
	    set xp_sum [expr $xp_sum/$nr_part]
	}
	set y_sum2 [expr $y_sum2/$nr_part-$y_sum*$y_sum]
	set yyp_sum2 [expr $yyp_sum2/$nr_part-$y_sum*$yp_sum]
	set yp_sum2 [expr $yp_sum2/$nr_part-$yp_sum*$yp_sum]
	set x_sum2 [expr $x_sum2/$nr_part-$x_sum*$x_sum]
	set xp_sum2 [expr $xp_sum2/$nr_part-$xp_sum*$xp_sum]
	set xxp_sum2 [expr $xxp_sum2/$nr_part-$x_sum*$xp_sum]
	set emitt_x [expr sqrt(($x_sum2*$xp_sum2-$xxp_sum2*$xxp_sum2))*$e_sum/0.511e-3*1e-3]
	set emitt_y [expr sqrt(abs($y_sum2*$yp_sum2-$yyp_sum2*$yyp_sum2))*$e_sum/0.511e-3*1e-3]

	array set emitt_data {
	    emitt_x
	    emitt_y
	}

	set emitt_data(emitt_x) $emitt_x
	set emitt_data(emitt_y) $emitt_y

	array get emitt_data
}



