% calc_noise_time_step.m
%
% Juergen Pfingstner
% 14. Jan 2011

function [data, noise_seed] = calc_noise_time_step(data, noise_type, noise_seed)
% Function to calculate the noise for the next time step of the simulation
%
% Input: 
%     data       ... data to be reseted
%     noise_type ... type of the noise the data are used for
%
% Output:
%     data ... reseted data
%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Set noise generator to the right seed %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

old_seed = 0;
if(strcmp(noise_type, 'white_gaussian'))
  old_seed = randn('seed');
  randn('seed', noise_seed);
elseif(strcmp(noise_type, 'white_uniform'))
  old_seed = rand('seed');
  rand('seed', noise_seed);
end

%%%%%%%%%%%%%%%%%%%%
% Create the noise %
%%%%%%%%%%%%%%%%%%%%

[element_nr, temp] = size(data);
for i = 1:element_nr
  
  if(strcmp(noise_type, 'white_gaussian'))
    
    std = data(i).std;
    data(i).actual_value = std*randn(1,1);
    
  elseif(strcmp(noise_type, 'white_uniform'))
   
    interval = data(i).interval;
    data(i).actual_value = -interval/2 + interval*rand(1,1);
    
  else
    
    % unknown type
    fprintf(1, '\n\nThe specified corrector noise type is unknown. Simulation terminated in calc_noise_time_step!!!\n\n');
    exit;
    
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Set noise generator back to the original seed %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if(strcmp(noise_type, 'white_gaussian'))
  noise_seed = randn('seed');
  randn('seed', old_seed);
elseif(strcmp(noise_type, 'white_uniform'))
  noise_seed = rand('seed');
  rand('seed', old_seed);
end

