% Function to calculate the response matrix of the QP.
%
% Remark: Based on the function placet_get_resonse_matrix.m 
%
% Juergen Pfingstner
% 29. September 2010

function [R_qp] = calc_qp_response_matrix(line_name, beam_name, bpm_index, qp_index, step_size, direction)
%
% direction: 1 ... y
%            0 ... x
%

R_qp = zeros(length(bpm_index), length(qp_index));
placet_test_no_correction(line_name, beam_name, 'None');
B=placet_get_bpm_readings(line_name, bpm_index, true);
if(direction == 1)
  % y direction
  b0 = B(:,2);
  qp0 = placet_element_get_attribute(line_name, qp_index, 'y');
else
  % x direction
  b0 = B(:,1);
  qp0 = placet_element_get_attribute(line_name, qp_index, 'x');
end

for i=1:length(qp_index)
  if(direction == 1)

    placet_element_set_attribute(line_name, qp_index(i), 'y', qp0(i) + step_size);
    placet_test_no_correction(line_name, beam_name, 'None');
    placet_element_set_attribute(line_name, qp_index(i), 'y', qp0(i));
    B=placet_get_bpm_readings(line_name, bpm_index, true);
    b1 = B(:,2);
  else
 
    qp_pos_local = placet_element_get_attribute(line_name, qp_index(i), 'x');
    placet_element_set_attribute(line_name, qp_index(i), 'x', qp_pos_local + step_size);
    placet_test_no_correction(line_name, beam_name, 'None');
    placet_element_set_attribute(line_name, qp_index(i), 'x', qp_pos_local);
    B=placet_get_bpm_readings(line_name, bpm_index, true);
    b1 = B(:,1);
    
  end
  R_qp(:,i)=(b1-b0);
end

R_qp = R_qp ./ step_size; 

  
