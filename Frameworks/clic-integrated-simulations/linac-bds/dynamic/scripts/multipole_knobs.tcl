array set gp_param "
    energy 1500.0
    particles [expr $match(charge)*1e-10]
    sigmaz $match(sigma_z)
    cut_x 200.0
    cut_y 25.0
    n_x 64
    n_y 320
    do_coherent 1
    n_t 1
    charge_sign -1.0"

#source $script_dir/scripts/clic_guinea.tcl

proc run_guinea_alatina {off angle} {
    global gp_param
    set res [exec grid]
    set yoff [expr -0.5*([lindex $res 2]+[lindex $res 3])]
    set xoff [expr -0.5*([lindex $res 0]+[lindex $res 1])] 
    set tx $gp_param(cut_x)
    set ty $gp_param(cut_y)
    if {[lindex $res 1]-[lindex $res 0]>2.0*$tx} {
        set gp_param(cut_x) [expr 0.5*([lindex $res 1]-[lindex $res 0])]
    }
    if {[lindex $res 3]-[lindex $res 2]>2.0*$ty} {
        set gp_param(cut_y) [expr 0.5*([lindex $res 3]-[lindex $res 2])]
    }
    #puts "yoff $yoff"
    #puts "xoff $xoff"
    write_guinea_correct $xoff $yoff 
    #write_guinea_correct $xoff $yoff 0 0 0
    exec guinea default_clic default result.out
    set gp_param(cut_x) $tx
    set gp_param(cut_y) $ty
    return [get_results result.out]
}

Octave {
  function L = get_lumi(B)
    if nargin==0
      %IP = placet_get_name_number_list("electron", "IP");
      [E,B] = placet_test_no_correction("electron", "beam_electron", "None");
    end
    save_beam("electron.ini", B);
    save_beam("positron.ini", B);
    Tcl_Eval("set res [run_guinea 0.0 0.0]");
    Tcl_Eval("set lumi_total [lindex \$res 0]");
    Tcl_Eval("set lumi_peak [lindex \$res 1]");
    L = str2num(Tcl_GetVar("lumi_total"))
    L_peak = str2num(Tcl_GetVar("lumi_peak"))
  end
}

Octave {
    global Knobs;
    load '../scripts/Knobs_sr_${synrad}.dat';

    NAME = [ "SF6"; "SF5"; "SD4"; "SF1"; "SD0" ];
    for i=1:length(Knobs.Ix)
      Knobs.I(i,:)=placet_get_name_number_list("electron",NAME(i,:));
      Knobs.Ix(i,:) = Knobs.I(i,:);
      Knobs.I(i+length(Knobs.Ix),:)=placet_get_name_number_list("electron",NAME(i,:));
      Knobs.Iy(i,:) = Knobs.I(i+length(Knobs.Ix),:);
    end
}


for {set knob 1 } { $knob <= 10 } { incr knob } {
  Octave {
    function vary_knob${knob}(x)
      global Knobs;
      knobs = x * Knobs.V(:,$knob);
      placet_element_vary_attribute("electron", Knobs.I, Knobs.L, +knobs);
    end
    function L = test_knob${knob}(x)
      vary_knob${knob}(+x);
      L = -get_lumi();
      vary_knob${knob}(-x);
      printf("test_knob${knob}(%g) =  %g\n", x, -L);
    end
  }
}

Octave {
  function [x_min, f_min] = fmin_inverse_parabola(func, a, b, c)
    X = [ a b c ];
    F = [ feval(func, a) feval(func, b) feval(func, c) ];
    [f_min, i_min] = min(F);
    x_min = X(i_min);
    # hold on;
    #for iter = 1:10
    for iter = 1:7
      P = polyfit(X, F, 2);
      # _X = linspace(min(X), max(X), 80);
      # _F = polyval(P, _X);
      # if P(1) > 0 
      #   plot(X,-F,'*',_X,-_F);
      # end
      # drawnow;
      if P(1) == 0
        disp("flat function!");
        break
      end
      while P(1) < 0
        disp("bracketing");
        [F, I] = sort(F);
        X = X(I);
        X(3) = X(1) + 1.6180 * (X(1) - X(3));
        F(3) = feval(func, X(3));
        P = polyfit(X, F, 2);
      end
      x_vertex = -P(2) / P(1) / 2;
      f_vertex = feval(func, x_vertex);
      X = [ X x_vertex ];
      F = [ F f_vertex ];	
      [F, I] = sort(F);
      X = X(I(1:3));
      F = F(1:3);
      x_min = X(1);
      f_min = F(1);
      X(3) = X(1) + 0.38197 * sign(X(1) - X(2)) * abs(X(1) - X(3));
      F(3) = feval(func, X(3));
    end
  end
}

proc multipole_knobs { range } {
  foreach iter { 1 2 } {
    foreach knob { 1 2 3 4 5 6 7 8 9 10 } {
      Octave {
        x = fmin_inverse_parabola("test_knob${knob}", -$range, 0.0, $range);
        vary_knob${knob}(x);
      }
    }
    set range [expr $range / 10.0]
  }
}

proc multipole_knobs_single { knobnumber range } {
  foreach iter { 1 } {
      Octave {
	  x = fmin_inverse_parabola("test_knob${knobnumber}", -$range, 0.0, $range);
	  #vary_knob${knobnumber}(x);
      }
    set range [expr $range / 10.0]
  }
}
