# calculate the power of base**p
proc power {base p} { 
    set result 1
    while {$p > 0} { 
	set result [expr $result*$base] 
	set p [expr $p-1]
    }
    return $result
}

# count lines in a file
proc linecount {file} {
    set i 0
    set fh [open $file r]
    while {[gets $fh line] > -1} {incr i}
    close $fh
    return $i
}

# save array to file
proc save_array {array0 arrayname} {
    upvar 1 $array0 array1
    set fh [open $arrayname.dat w]
    puts $fh [list array set $arrayname [array get array1]]
    close $fh
}

# read array from file
# uplevel should be fixed!
proc read_array {arrayname} {
    uplevel 1 "set arrayname $arrayname"
    uplevel 1 {
	source $arrayname.dat
    }
}

# print elements of an array
proc show_array {arrayName arrayList} {
    upvar $arrayName myArrayName
    array set myArray $arrayList

    foreach element [array names myArray] {
	puts "${arrayName}($element) =  $myArray($element)"
    }
}

# export tcl array into octave variables
proc export_array_2_octave { arrayname {extension _tcl} } {
    upvar $arrayname array1
    foreach {key} [array names array1] {
	Octave {
	    ${key}${extension}=str2double(Tcl_GetVar('array1','$key'));
	    if (isnan(${key}${extension}))
	      ${key}${extension}=Tcl_GetVar('array1','$key');
	    end
	}
    }
}

# export tcl array into octave variables 
# for exporting of environment to octave, which somehow does not work with above function
proc export_array_2_octave_env { arrayname arraylist {extension _tcl} } {
    upvar 1 $arrayname myarrayname
    array set array1 $arraylist
    foreach {key} [array names array1] {
	Octave {
	    ${myarrayname}_${key}${extension}=str2double(Tcl_GetVar('array1','$key'));
	    if (isnan(${myarrayname}_${key}${extension}))
	    ${myarrayname}_${key}${extension}=Tcl_GetVar('array1','$key');
	    end
	}
    }
}

# export tcl environment into octave variables
proc export_env_2_octave { {extension "_tcl"} } {
    if {$extension==""} {
	uplevel 1 "set extension \"\" "
    } else {
	uplevel 1 "set extension $extension"
    }
    uplevel 1 {
	foreach v [info vars] {
	    if [array exists $v] {
		export_array_2_octave_env v [array get $v] $extension
	    } else {
		Octave {
		    ${v}${extension} = str2double(Tcl_GetVar('$v'));
		    if (isnan(${v}${extension}))
		      ${v}${extension} = Tcl_GetVar('$v');
		    end
		}
	    }
	}
    }
}

# save all vars and arrays
proc save_state {filename} {
    uplevel 1 "set filename $filename"
    uplevel 1 {
        set fh [open $filename w]
	set not_saved_vars {auto_index auto_path env fh not_saved_vars script_dir tcl_interactive tcl_libPath tcl_library tcl_pkgPath tcl_patchLevel tcl_platform tcl_version temp}
        foreach v [info vars] {
	    if {[lsearch $not_saved_vars $v] < 0} {
		if [array exists $v] {
		    puts $fh [list array set $v [array get $v]]
		} else {
		    puts $fh [list set $v [set $v]]
		}
            }
        }
        close $fh
    }
}

# restore variables
proc restore_state {filename} {
    uplevel 1 "set filename $filename"
    uplevel 1 {
        source $filename
    }
}

# source file and issue warning if variable does not exist
# known issue: not able to deal with lists
proc check_file {filename} {
    uplevel 1 "set filename $filename"
    uplevel 1 {
	puts "$filename"
	set fh [open $filename r]
	while {[gets $fh line] > -1} {
	    # skip empty lines or comments
	    if {[string is space $line] || [string index $line 0]== "#"} {
		continue
	    }
	    # split line into a tcl list:
	    set tcl_list [split $line " "]
	    # compare first argument to "set"
	    # don't do anything for now as more elaborate tcl code might be in the file
	    if {[llength $tcl_list]<3 || [string compare "set" [lindex $tcl_list 0]] != 0} {
		#puts "WARNING input $tcl_list has not the correct format in $filename"
		#exit;
		continue
	    }
	    set temp [lindex $tcl_list 1]
	    if {[info exists $temp]==0} {
		puts "WARNING new variable introduced: $temp"
	    }
	    #set $temp [lindex $tcl_list 2]
	}
    }
}
