% File to postprocess all results of one machine run
%
% Juergen Pfingstner
% 11th of October 2010

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Delete the useless files %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Matlab was not capable of deleting more than one file (do not ask
% why). Therefore the task is performed in Tcl
%useless_file_name = sprintf('electron_linac_*.dat');
%delete(useless_file_name); Did not work due to some strange reason

% Debug
Tcl_Eval("eval exec rm -f [glob -nocomplain electron_linac*]");
Tcl_Eval("eval exec rm -f [glob -nocomplain electron_bds*]");
Tcl_Eval("eval exec rm -f [glob -nocomplain electron_ip*]");
Tcl_Eval("eval exec rm -f [glob -nocomplain positron_linac*]");
Tcl_Eval("eval exec rm -f [glob -nocomplain positron_bds*]");
Tcl_Eval("eval exec rm -f [glob -nocomplain positron_ip*]");
