%% Script to run the IP feedback for some time steps to centre the beam
%%
%% Juergen Pfingstner
%% 17th of April 2012

nr_iter_centre_beam == 20;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Save old values and apply new ones %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

use_controller_x_old = use_controller_x;
use_controller_y_old = use_controller_y;
use_ip_feedback_old = use_ip_feedback;
use_ip_feedback_x_old = use_ip_feedback_x;
use_ip_feedback_y_old = use_ip_feedback_y;
bpm_ip_res_old = bpm_ip_res; 
use_ip_feedback_linear_old = use_ip_feedback_linear;
use_ip_feedback_pid_old = use_ip_feedback_pid;
use_ip_feedback_annecy_old = use_ip_feedback_annecy;
gain_bb_x_old = gain_bb_x;
gain_bb_y_old = gain_bb_y; 

use_controller_x = 1;
use_controller_y = 1;
use_ip_feedback = 1;
use_ip_feedback_x = 1;
use_ip_feedback_y = 1;
bpm_ip_res = 0; 
use_ip_feedback_linear = 1;
use_ip_feedback_pid = 0;
use_ip_feedback_annecy = 0;
gain_bb_x = 1;
gain_bb_y = 1; 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Centre the beams by running the IP feedback in a loop %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for index_centre_beam=1:nr_iter_centre_beam

    %% Tracking
    tracking

    %% Calculate control algorithm
    controller_calc
		
    %% Apply Corrections
    controller_apply

end

%%%%%%%%%%%%%%%%%%%%%%%
%% Restore old values %
%%%%%%%%%%%%%%%%%%%%%%%

use_ip_feedback = use_ip_feedback_old;
use_controller_x = use_controller_x_old;
use_controller_y = use_controller_y_old;
use_ip_feedback_x = use_ip_feedback_x_old;
use_ip_feedback_y = use_ip_feedback_y_old;
bpm_ip_res = bpm_ip_res_old; 
gain_bb_x = gain_bb_x_old;
gain_bb_y = gain_bb_y_old; 

use_ip_feedback_linear = use_ip_feedback_linear_old;
use_ip_feedback_pid = use_ip_feedback_pid_old;
use_ip_feedback_annecy = use_ip_feedback_annecy_old;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Update controller values %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if(use_ip_feedback_pid == 1)  
    volt_ipdipole_y = placet_element_get_attribute('electron', ipdipole_index(1,:), "strength_x");;
    volt_ipdipole_y_prev = volt_ipdipole_y;
    volt_ipdipole_y_new = volt_ipdipole_y;
    angley_volt_ipdipole_prev = 0;
end

if(use_ip_feedback_annecy == 1)
    IPdipole_strength_electron_y = placet_element_get_attribute('electron', ipdipole_index(1,:), "strength_x");
    IPdipole_strength_positron_y = -IPdipole_strength_electron_y;
end


