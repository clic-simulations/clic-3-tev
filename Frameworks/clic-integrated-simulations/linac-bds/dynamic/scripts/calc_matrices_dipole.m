% Function ot load the response matrix of the dipoles for the x
% direction.
%
% Juergen Pfingstner
% 26th of October 2010

function [R0_x U0_x s0_x V0_x R0_y U0_y s0_y V0_y] = ...
    calc_matrices_dipole(use_main_linac, use_bds, nr_corr_ml, ...
			 line_name, beam_name, bpm_index, corr_index, ...
			 corr_center_x, corr_center_y, step_size)
      

fprintf(1, 'Calculate the response matrix for x (take a day off) ... ');
[R0_dipole_x] = calc_dipole_response_matrix(line_name, beam_name, ...
					    bpm_index, corr_index, corr_center_x, step_size, 0);
fprintf(1, 'finished\n');
save -ascii "R0_dipole_x.dat" R0_dipole_x;
    
% Perform the SVD x

fprintf(1, 'Perform the SVD for x ... ');
    
[U0_dipole_x S0_dipole_x V0_dipole_x] = svd(R0_dipole_x);
s0_dipole_x = diag(S0_dipole_x);

[U0_dipole_ml_x S0_dipole_ml_x V0_dipole_ml_x] = svd(R0_dipole_x(1:nr_corr_ml,1:nr_corr_ml));
s0_dipole_ml_x = diag(S0_dipole_ml_x);
    
[U0_dipole_bds_x S0_dipole_bds_x V0_dipole_bds_x] = svd(R0_dipole_x((nr_corr_ml+1):end,(nr_corr_ml+1):end));
s0_dipole_bds_x = diag(S0_dipole_bds_x);
    
fprintf(1, 'finished\n');

% Store the results x
    
fprintf(1, 'Store the matrices for x ... ');
 
save -ascii "U0_dipole_x.dat" U0_dipole_x;
save -ascii "s0_dipole_x.dat" s0_dipole_x;
save -ascii "V0_dipole_x.dat" V0_dipole_x;

save -ascii "U0_dipole_ml_x.dat" U0_dipole_ml_x;
save -ascii "s0_dipole_ml_x.dat" s0_dipole_ml_x;
save -ascii "V0_dipole_ml_x.dat" V0_dipole_ml_x;

save -ascii "U0_dipole_bds_x.dat" U0_dipole_bds_x;
save -ascii "s0_dipole_bds_x.dat" s0_dipole_bds_x;
save -ascii "V0_dipole_bds_x.dat" V0_dipole_bds_x;

fprintf(1, 'finished\n');
    
if( ( use_main_linac == 1 ) & ( use_bds == 0 ) )
  R0_x = R0_dipole_x(1:nr_corr_ml, 1:nr_corr_ml);
  U0_x = U0_dipole_ml_x;
  s0_x = s0_dipole_ml_x;
  V0_x = V0_dipole_ml_x;
elseif( ( use_main_linac == 0 ) & ( use_bds == 1 ) )
  R0_x = R0_dipole_x((nr_corr_ml+1):end, (nr_corr_ml+1):end);
  U0_x = U0_dipole_bds_x;
  s0_x = s0_dipole_bds_x;
  V0_x = V0_dipole_bds_x;
elseif( ( use_main_linac == 1 ) & ( use_bds == 1 ) )
  R0_x = R0_dipole_x;
  U0_x = U0_dipole_x;
  s0_x = s0_dipole_x;
  V0_x = V0_dipole_x;
end
    
clear R0_dipole_x;
clear U0_dipole_x S0_dipole_x V0_dipole_x;
clear R0_dipole_ml_x U0_dipole_ml_x S0_dipole_ml_x V0_dipole_ml_x;
clear R0_dipole_bds_x U0_dipole_bds_x S0_dipole_bds_x V0_dipole_bds_x;

    
% Calculate the dipole response matrix for y
 
fprintf(1, 'Calculate the response matrix for y (take a day off) ... ');
[R0_dipole_y] = calc_dipole_response_matrix(line_name, beam_name, ...
					    bpm_index, corr_index, corr_center_y, step_size, 1);
fprintf(1, 'finished\n');
save -ascii "R0_dipole_y.dat" R0_dipole_y;
    
% Perform the SVD y

fprintf(1, 'Perform the SVD for y ... ');

[U0_dipole_y S0_dipole_y V0_dipole_y] = svd(R0_dipole_y);
s0_dipole_y = diag(S0_dipole_y);

[U0_dipole_ml_y S0_dipole_ml_y V0_dipole_ml_y] = svd(R0_dipole_y(1:nr_corr_ml,1:nr_corr_ml));
s0_dipole_ml_y = diag(S0_dipole_ml_y);
    
[U0_dipole_bds_y S0_dipole_bds_y V0_dipole_bds_y] = svd(R0_dipole_y((nr_corr_ml+1):end,(nr_corr_ml+1):end));
s0_dipole_bds_y = diag(S0_dipole_bds_y);
    
fprintf(1, 'finished\n');
  
% Store the results y
    
fprintf(1, 'Store the matrices for y ... ');

save -ascii "U0_dipole_y.dat" U0_dipole_y;
save -ascii "s0_dipole_y.dat" s0_dipole_y;
save -ascii "V0_dipole_y.dat" V0_dipole_y;

save -ascii "U0_dipole_ml_y.dat" U0_dipole_ml_y;
save -ascii "s0_dipole_ml_y.dat" s0_dipole_ml_y;
save -ascii "V0_dipole_ml_y.dat" V0_dipole_ml_y;

save -ascii "U0_dipole_bds_y.dat" U0_dipole_bds_y;
save -ascii "s0_dipole_bds_y.dat" s0_dipole_bds_y;
save -ascii "V0_dipole_bds_y.dat" V0_dipole_bds_y;

fprintf(1, 'finished\n');
    
if( ( use_main_linac == 1 ) & ( use_bds == 0 ) )
  R0_y = R0_dipole_y(1:nr_corr_ml, 1:nr_corr_ml);
  U0_y = U0_dipole_ml_y;
  s0_y = s0_dipole_ml_y;
  V0_y = V0_dipole_ml_y;
elseif( ( use_main_linac == 0 ) & ( use_bds == 1 ) )
  R0_y = R0_dipole_y((nr_corr_ml+1):end, (nr_corr_ml+1):end);
  U0_y = U0_dipole_bds_y;
  s0_y = s0_dipole_bds_y;
  V0_y = V0_dipole_bds_y;
elseif( ( use_main_linac == 1 ) & ( use_bds == 1 ) )
  R0_y = R0_dipole_y;
  U0_y = U0_dipole_y;
  s0_y = s0_dipole_y;
  V0_y = V0_dipole_y;
end
    
clear R0_dipole_y;
clear U0_dipole_y S0_dipole_y V0_dipole_y;
clear R0_dipole_ml_y U0_dipole_ml_y S0_dipole_ml_y V0_dipole_ml_y;
clear R0_dipole_bds_y U0_dipole_bds_y S0_dipole_bds_y V0_dipole_bds_y;
