%% Script to run the IP feedback for some time steps to centre the beam
%%
%% Juergen Pfingstner
%% 17th of April 2012

nr_iter_centre_beam == 20;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Save old values and apply new ones %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

use_controller_x_old = use_controller_x;
use_controller_y_old = use_controller_y;
use_ip_feedback_x_old = use_ip_feedback_x;
use_ip_feedback_y_old = use_ip_feedback_y;
bpm_ip_res_old = bpm_ip_res; 

use_controller_x = 1;
use_controller_y = 1;
use_ip_feedback_x = 0;
use_ip_feedback_y = 0;
bpm_ip_res = 0; 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Centre the beams by running the IP feedback in a loop %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for index_centre_beam=1:nr_iter_centre_beam

    %% Tracking
    tracking

    %% Calculate control algorithm
    controller_calc
		
    %% Apply Corrections
    controller_apply

end

%%%%%%%%%%%%%%%%%%%%%%%
%% Restore old values %
%%%%%%%%%%%%%%%%%%%%%%%

use_controller_x = use_controller_x_old;
use_controller_y = use_controller_y_old;
use_ip_feedback_x = use_ip_feedback_x_old;
use_ip_feedback_y = use_ip_feedback_y_old;
bpm_ip_res = bpm_ip_res_old; 
