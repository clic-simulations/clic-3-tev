%% Script to create the data needed for the long-term alignment and tuning methods
%%
%% Juergen Pfingstner
%% 17. April 2012

%%%%%%%%%%%%%%%%%%%
%% Basic steering %
%%%%%%%%%%%%%%%%%%%

if(use_basic_steering == 1)
  
    if(basic_steering_method == 2)

	%%%%%%%%%%%%%%%%
	%% x direction %
	%%%%%%%%%%%%%%%%
    
	%% Load matrix
	R_x_steering = load(basic_steering_matrix_name_x);

	%% Create indices and the matrices
	if(basic_steering_option == 1)
	    %% Use correctors everywhere
	    corr_index_basic_steering = qp_index(1,1:basic_steering_modulo:end);
	    bpm_index_basic_steering = bpm_index(i,:);
	    R_x_steering = R_x_steering(:,1:basic_steering_modulo:end);
        elseif(basic_steering_option == 2)
	    %% Use correctors only in the ML
	    corr_index_basic_steering = qp_index_ml(1,1:basic_steering_modulo:end);
	    bpm_index_basic_steering = bpm_index(i,:);
	    R_x_steering = R_x_steering(:,1:nr_corr_ml(1));
	    R_x_steering = R_x_steering(:,1:basic_steering_modulo:end);
	elseif(basic_steering_option == 3)
	    %% Use correctors only in the ML and also only BPMs in the ML
	    corr_index_basic_steering = qp_index_ml(1,1:basic_steering_modulo:end);
	    bpm_index_basic_steering = bpm_index(i,1:nr_corr_ml(1));
	    R_x_steering = R_x_steering(:,1:nr_corr_ml(1));
	    R_x_steering = R_x_steering(:,1:basic_steering_modulo:end);
	    R_x_steering = R_x_steering(1:nr_corr_ml(1),:);
	else
	    fprintf(1,"Unknown basis steering option!\n");
	    exit;
	end
	
	%% Create final matrix and invert it
	[nr_bpm_steering, nr_corr_steering_x] = size(R_x_steering);
	fprintf(1,"Calculation of the all-to-all matrix x ... ");
	BS_x = -pinv([R_x_steering; basic_steering_beta.*eye(nr_corr_steering_x)]);
	fprintf(1,"done\n");

	%%%%%%%%%%%%%%%%
	%% y direction %
	%%%%%%%%%%%%%%%%
    
	%% Load matrix
	R_y_steering = load(basic_steering_matrix_name_y);

	%% Create indices and the matrices
	if(basic_steering_option == 1)
	    %% Use correctors everywhere
	    corr_index_basic_steering = qp_index(1,1:basic_steering_modulo:end);
	    bpm_index_basic_steering = bpm_index(i,:);
	    R_y_steering = R_y_steering(:,1:basic_steering_modulo:end);
        elseif(basic_steering_option == 2)
	    %% Use correctors only in the ML
	    corr_index_basic_steering = qp_index_ml(1,1:basic_steering_modulo:end);
	    bpm_index_basic_steering = bpm_index(i,:);
	    R_y_steering = R_y_steering(:,1:nr_corr_ml(1));
	    R_y_steering = R_y_steering(:,1:basic_steering_modulo:end);
	elseif(basic_steering_option == 3)
	    %% Use correctors only in the ML and also only BPMs in the ML
	    corr_index_basic_steering = qp_index_ml(1,1:basic_steering_modulo:end);
	    bpm_index_basic_steering = bpm_index(i,1:nr_corr_ml(1));
	    R_y_steering = R_y_steering(:,1:nr_corr_ml(1));
	    R_y_steering = R_y_steering(:,1:basic_steering_modulo:end);
	    R_y_steering = R_y_steering(1:nr_corr_ml(1),:);
	else
	    fprintf(1,"Unknown basis steering option!\n");
	    exit;
	end
	
	%% Create final matrix and invert it
	[nr_bpm_steering, nr_corr_steering_y] = size(R_y_steering);
	fprintf(1,"Calculation of the all-to-all matrix y ... ");
	BS_y = -pinv([R_y_steering; basic_steering_beta.*eye(nr_corr_steering_y)]);
	fprintf(1,"done\n");

    end

    basic_steering_corrections_x = zeros(2,nr_corr_steering_x);
    basic_steering_corrections_y = zeros(2,nr_corr_steering_y);
end

%% Variables for initially centering the beams
if(use_beam_beam == 1)
    r_kicker_ip_x = 0.0023;
    r_kicker_ip_y = 0.0023;
end