proc ground_motion_init {beamlinename type model gmseed sign} {
    global script_dir delta_T_short ground_motion_x ground_motion_y groundmotion use_main_linac use_bds
    
    if {$type==2} {
	puts -nonewline "Init ground motion generator in "
	puts $beamlinename
	BeamlineUse -name $beamlinename
	
	GroundMotionInit -file $script_dir/scripts/harm.$model.300 -x $ground_motion_x -y $ground_motion_y -t_step $delta_T_short -seed $gmseed -s_sign $sign -s_abs 0 -last_start 1 -s_start 0 -t_start $groundmotion(t_start)
	# stabilisation for the machine
	if {$groundmotion(filtertype)} {
	    global use_quad_stab_failure
	    if {$use_quad_stab_failure} {
		global quad_stab_failure
		# global nr_girders
		set quad_list [QuadrupoleNumberList]
		if {[string compare $quad_stab_failure(distribution) "random"]==0 } {
		    AddGMFilter -file $script_dir/scripts/harm.$model.300 -filter_x $script_dir/scripts/$groundmotion(filtertype_x) -filter_y $script_dir/scripts/$groundmotion(filtertype_y)
		    # put no filter one by one..
		    foreach i $quad_list {
			set broken [expr rand() + $quad_stab_failure(prob)]
			# if broken (>1 then no filter is added)
			if {$broken > 1} {
			    AddGMFilter -file $script_dir/scripts/harm.$model.300 -filter_x $script_dir/scripts/no_filter.dat -filter_y $script_dir/scripts/no_filter.dat -start_element $i -end_element $i
			}
		    }
		} elseif {[string compare $quad_stab_failure(distribution) "list"]==0 } {
		    AddGMFilter -file $script_dir/scripts/harm.$model.300 -filter_x $script_dir/scripts/$groundmotion(filtertype_x) -filter_y $script_dir/scripts/$groundmotion(filtertype_y)
		    # put no filter one by one..
		    foreach i $quad_stab_failure(list) {
			set element_nr [lindex $quad_list $i] 
			AddGMFilter -file $script_dir/scripts/harm.$model.300 -filter_x $script_dir/scripts/no_filter.dat -filter_y $script_dir/scripts/no_filter.dat -start_element $element_nr -end_element $element_nr
		    }
		} else {
		    # failure in block mode, decide start and end point of filter
		    set start_element 0
		    set end_element 10000000
		    # calculate nr of broken quads, todo: protect against 0!
		    set nr_quad_stab_failures [expr round($quad_stab_failure(prob)*[llength $quad_list])]
		    puts "nr of quadrupole stabilisation failures: $nr_quad_stab_failures"
		    if {[string compare $quad_stab_failure(distribution) "block_begin"]==0 } {
			set start_element [lindex $quad_list [expr $nr_quad_stab_failures + 1]]
		    } elseif {[string compare $quad_stab_failure(distribution) "block_end"]==0 } {
			set end_element [lindex $quad_list [expr [llength $quad_list] - $nr_quad_stab_failures]]
		    } elseif {[string compare $quad_stab_failure(distribution) "block_end_ml"]==0 } {
			set quad_begin_bds 1
			if {$use_main_linac} {
			    set nr_quads_ml 2010
			    set quad_begin_bds [lindex $quad_list [expr $nr_quads_ml+1]]
			    # add BDS filter
			    AddGMFilter -file $script_dir/scripts/harm.$model.300 -filter_x $script_dir/scripts/$groundmotion(filtertype_x) -filter_y $script_dir/scripts/$groundmotion(filtertype_y) -start_element $quad_begin_bds
			}
			set end_element [lindex $quad_list [expr $nr_quads_ml - $nr_quad_stab_failures]]
		    } elseif {[string compare $quad_stab_failure(distribution) "block_begin_bds"]==0 } {
			# get element nr of end of ML
			set quad_end_ml 0
			set nr_quads_ml 0
			if {$use_main_linac} {
			    set nr_quads_ml 2010
			    set quad_end_ml [lindex $quad_list $nr_quads_ml]
			    # add ML filter
			    AddGMFilter -file $script_dir/scripts/harm.$model.300 -filter_x $script_dir/scripts/$groundmotion(filtertype_x) -filter_y $script_dir/scripts/$groundmotion(filtertype_y) -end_element $quad_end_ml
			}
			set start_element [lindex $quad_list [expr $nr_quad_stab_failures + $nr_quads_ml]]
		    } else {
			puts "Quad stabilisation failure mode $quad_stab_failure(distribution) is not known!"
			exit;
		    }
		    AddGMFilter -file $script_dir/scripts/harm.$model.300 -filter_x $script_dir/scripts/$groundmotion(filtertype_x) -filter_y $script_dir/scripts/$groundmotion(filtertype_y) -start_element $start_element -end_element $end_element
		}
	    } else {
		# add all filters at once
		AddGMFilter -file $script_dir/scripts/harm.$model.300 -filter_x $script_dir/scripts/$groundmotion(filtertype_x) -filter_y $script_dir/scripts/$groundmotion(filtertype_y)
	    }
	}

	# special stabilisation for final focus, overwrites other filter:
	if {($groundmotion(preisolator)==1) && $use_bds} {
	    global start_cantilever end_cantilever
	    # pre-isolator (passive, from F. Ramos et al.) , add x direction
	    AddGMFilter -file $script_dir/scripts/harm.$model.300 -filter $script_dir/scripts/filter_preisolatorFF.dat -start_element $start_cantilever -end_element $end_cantilever
	} elseif {($groundmotion(preisolator)==2) && $use_bds} {
	    # mechanical feedback (active, from B. Caron et al.)
	    AddGMFilter -file $script_dir/scripts/harm.$model.300 -filter $script_dir/scripts/filter_mechanical_fb_FF.dat -start_element $start_cantilever -end_element $end_cantilever
	} elseif {($groundmotion(preisolator)==3) && $use_bds} {
	    # filter from stabilisation groups
	    # pre-isolator (passive, from F. Ramos et al.)
	    AddPreIsolator -file_mnt1QD0 $script_dir/scripts/preisolatorTF_mount1_QD0.dat -file_mnt2QD0 $script_dir/scripts/preisolatorTF_mount2_QD0.dat -file_mnt1QF1 $script_dir/scripts/preisolatorTF_mount1_QF1.dat -file_mnt2QF1 $script_dir/scripts/preisolatorTF_mount2_QF1.dat -file_mnt1QD0_x $script_dir/scripts/preisolatorTF_mount1_QD0_x.dat -file_mnt2QD0_x $script_dir/scripts/preisolatorTF_mount2_QD0_x.dat -file_mnt1QF1_x $script_dir/scripts/preisolatorTF_mount1_QF1_x.dat -file_mnt2QF1_x $script_dir/scripts/preisolatorTF_mount2_QF1_x.dat -pos_mnt1 8.306 -pos_mnt2 10.946
	}
    } elseif {$type==1} {
	# ATL motion
	puts -nonewline "Init ATL motion in "
	puts $beamlinename
    } elseif {$type==0} {
	puts -nonewline "No ground motion applied "
	puts $beamlinename
    } else {
	puts -nonewline "Unknown ground motion type "
	puts $beamlinename
	exit
    }
}

proc ground_motion {beamlinename type } {
    global delta_T_short
    if {$type==1} {
	ground_motion_long $beamlinename $delta_T_short
    } elseif {$type==2} {
	ground_motion_short $beamlinename
    }
}

proc ground_motion_short {beamlinename} {
    global debug time_step_index gm_ip0 gm_cav_stab
    BeamlineUse -name $beamlinename
    if {$debug} {
	GroundMotion -cav_stab $gm_cav_stab -ip0 $gm_ip0 -output_file gm_${beamlinename}_${time_step_index}.dat  -output_file2 gm2_${beamlinename}_${time_step_index}.dat
    } else {
	GroundMotion -cav_stab $gm_cav_stab -ip0 $gm_ip0
    }
}

proc ground_motion_long {beamlinename {time_step 0} } {
    global ground_motion_x ground_motion_y atl_time groundmotion_long start_bds use_bds use_main_linac end_main_linac

    # ATL motion, time is given by tcl variable atl_time

    if {$time_step == 0} {
	set time_step $atl_time
    }

    puts "ATL motion applied $time_step"
    
    # determine which part to move:
    set start_element -1
    set end_element -1
    if {[info exists groundmotion_long] && $use_bds} {
	if {$groundmotion_long(ml)==0} {
	    set start_element $start_bds
	}
	if {$groundmotion_long(bds)==0 && $use_main_linac} {
	    set end_element $end_main_linac
	}
    }

    # move girders
    BeamlineUse -name $beamlinename
    GroundMotionATL -t $time_step -x $ground_motion_x -y $ground_motion_y -end_fixed 1 -start_element $start_element -end_element $end_element

}
