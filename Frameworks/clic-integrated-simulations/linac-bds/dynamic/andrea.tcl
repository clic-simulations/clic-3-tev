# Demo version of a personal settings file.

# Overwrite default settings:

set params(seed) 2

set debug 1
set ground_motion_x 0
set ground_motion_y 0

set dipole_correctors 0

set nr_machines 1
set nr_time_steps [expr 3]
set use_main_linac 1
set use_bds 1
set use_beam_beam 1

set use_controller_x 0
set use_controller_y 0
set use_ip_feedback 0
set use_ip_feedback_pid 0
set use_ip_feedback_linear 0

set dir_name "test_andrea"

#set main_linac_stab 0