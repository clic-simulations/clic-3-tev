# global variables
# directory structure
set script_dir [pwd]
source $script_dir/scripts/tcl_procs.tcl
source $script_dir/scripts/load_settings.tcl

#setting up beam, beamline
source $script_dir/scripts/accelerator_setup.tcl

Octave {

%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Global initialization %
%%%%%%%%%%%%%%%%%%%%%%%%%%
 
%%% load and calculate response matrices
%%% calculate nominal values and system identification
source "$script_dir/scripts/global_init.m"
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Go through all machines %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
for machine_index = 1:nr_machines
      
    fprintf(1, "\n Machine: %f of %f \n\n", machine_index, nr_machines);
 
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% Static alignment and initialisation %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    sim_mode = 1;

    %% machine setup, reinitialisation of ground motion, dynamic imperfections, 
    source "$script_dir/scripts/machine_setup.m"
    tracking
    postprocess_time_step

    %% Imperfections as misalignments and others
    source "$script_dir/scripts/machine_imperfections.m"

    %% Alignment
    source "$script_dir/scripts/static_alignment.m"

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% Go through all long term time steps %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    for time_step_index_long = 1:nr_time_steps_long

	sim_mode = 3;

	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	%% Apply long-term misalignment %
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

	fprintf(1,"\n\nLong-term step: %d of %d at time %gs of %gs\n", time_step_index_long, nr_time_steps_long, global_time, max_global_time);
	source "$script_dir/scripts/long_term_misalignment.m"

	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	%% Go through all time steps in real time %
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
	for time_step_index_short = 1:nr_time_steps_short
	    
	    sim_mode = 2; 
	    fprintf(1, "    Real-time step: %d of %d at time %gs of %gs\n", time_step_index_short, nr_time_steps_short, global_time, max_global_time);
	    time_step_index = time_step_index+1;
	    Tcl_SetVar("time_step_index", time_step_index);
	    global_time = global_time + delta_T_short;

	    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	    %% Apply ground motion and other disturbances %
	    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

	    source "$script_dir/scripts/dynamic_setup.m";
	
            %%%%%%%%%%%%%%%%%%%%%%%%%%
            %% System Identification %
            %%%%%%%%%%%%%%%%%%%%%%%%%%

            source "$script_dir/scripts/sysident_setup.m"

	    %%%%%%%%%%%%%
	    %% Tracking %
	    %%%%%%%%%%%%%

	    source "$script_dir/scripts/tracking.m"

	    %%%%%%%%%%%%%%%%%%
	    %% System Update %
	    %%%%%%%%%%%%%%%%%%

	    %% updating of the parameters (response matrix, etc.)
	    source "$script_dir/scripts/sysident_update.m"
		
	    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	    %% Calculate control algorithm %
	    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

	    source "$script_dir/scripts/controller_calc.m"
		
	    %%%%%%%%%%%%%%%%%%%%%%
	    %% Apply Corrections %
	    %%%%%%%%%%%%%%%%%%%%%%

	    source "$script_dir/scripts/controller_apply.m"
	    
	    %%%%%%%%%%%%%%%%%%%%%%
	    %% Store the results %
	    %%%%%%%%%%%%%%%%%%%%%%
	
	    source "$script_dir/scripts/postprocess_time_step.m"

	    fprintf(1,"    Time per real-time iteration: %2.2fs, ",toc-toc1);
	    fprintf(1,"Time remaining: %2.2fs\n",(nr_time_steps_short-time_step_index_short)*(toc-toc1));
	    toc1=toc;
	
	end
	
	sim_mode = 3;

	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	%% Apply long-term or static alignment %
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

	source "$script_dir/scripts/long_term_alignment.m"

	fprintf(1,"Time per long time iteration: %2.2fs, ",toc-toc2);
	fprintf(1,"Time remaining: %2.2fs\n\n",(nr_time_steps_long-time_step_index_long)*(toc-toc2));
	toc2=toc;

    end

    %% Delete the useless files / Save some parameters 
    source "$script_dir/scripts/postprocess_machine.m"
    
end


%% Delete the useless files / Save some parameters 
source "$script_dir/scripts/postprocess_run.m"
}

exit
