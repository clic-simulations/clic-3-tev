###############################
# Beam and Lattice Parameters #
###############################

array set params {
    ch 3.72e9
    emitt_x 6.6
    emitt_y 0.2
    sigma_z 44.0
    e_spread 1.3
    offset 0.0
    waist_y 0.0
    linerr everything
    wgt 1000.0
    quad_misalign 0.000
    seed 1
    qjitter 0.0
}

set phase 8a
set e_initial_linac 9.0
set e_initial_bds 1500.0
set e_initial $e_initial_linac
set e0 $e_initial

set n_slice 25
set n_part 15
set frac_lambda 0.25
set n_total 50000
set n_bunches 1
set n [ expr $n_total/$n_slice ]
set scale 1.0

#set quad_synrad 1
#set mult_synrad 1
#set sbend_synrad 1

# Collimators
set use_collimators 0
### taperl defines the length of the tapered part of the spoiler (here it is defined to conform 88 mrad taper angle) 
set wmetode 0
set taperl 0.09

#################################
# GUINEA-PIG related parameters #
#################################

array set gp_param {
    energy 1500.0
    cut_x 400.0
    cut_y 15.0
    n_x 128
    n_y 256
    do_coherent 1
    n_t 1
    charge_sign -1.0
}
# charge_sign 0: no beam-beam force

# declare expressions outside array set declaration!
set gp_param(waist_y) $params(waist_y)
set gp_param(particles) [expr $params(ch)*1e-10]
set gp_param(sigmaz) $params(sigma_z)
set gp_param(emitt_x) [expr $params(emitt_x)/10.]
# Set the minimum energy for lumi_high to 99% of the nominal centre-of-mass
# energy
set gp_param(ecm_min) [expr 2.0*$gp_param(energy)*0.99]

#################################
# General Simulation Parameters # 
#################################

# number of machines to simulate
# number of iterations (time_steps) per machine
# directory name, if empty autogenerate one
# delta_T_short in [s]
set delta_T_short 0.02
set nr_machines 1
set nr_time_steps_short 10
set dir_name "default"
set save_string ""
set use_main_linac 1
set use_bds 1
set use_beam_beam 1

# at the moment ground motion in x direction can not be run without the ground
# motion in y direction.
set ground_motion_x 0
set ground_motion_y 0

# type: 0: none, 1: ATL, 2: ground motion generator
# filtertype: 0: no filter, 1: use filters specified in x and y for the whole beamline
# preisolator: 0: no preisolator, 1: simple version F. Ramos et al, 2: mech. fdbck B. Caron et al., 3: F. Ramos et al. including tilt motion
# t_start: start after perfect beamline in s
array set groundmotion {
    type 2
    model "B"
    seed_nr 3
    filtertype 1
    filtertype_x "local_FB_lateral_v2.dat"
    filtertype_y "local_FB_vertical_v2.dat"
    preisolator 3
    t_start 0
}

# produce additional files and printout
set debug 0
# move ground with respect to IP
set gm_ip0 1
# move cavities with stabilisation
set gm_cav_stab 0
# mode for calculating response matrix
set response_matrix_calc 0
# dipole correctors or quadrupole offsets as orbit correctors
set dipole_correctors 0

##################
# Orbit feedback #
##################

set multipulse_nr 10
set error_mode 0

# Controller Parameters
set use_controller_x 1
set use_controller_y 1

# controller type 1: full matrix, 2: non-scaling case, 3: scaling case, 4: loaded coefficients, 5: experimental case
set controller_type_spatial_x 4
set controller_type_spatial_y 4
#set controller_type_ip 1
set gain_file_name_x "gains_x.dat"
set gain_file_name_y "gains_y.dat"
set max_gain_x 0.01
set max_gain_y 0.01

set nr_sv1_x 16
set nr_sv1_y 16
set nr_sv2_x 300
set nr_sv2_y 300
set controller_gain1_y 1
set controller_gain1_x 1
set controller_gain2_y 0.05
set controller_gain2_x 0.05
set controller_gain3_y 0.0001
set controller_gain3_x 0.0001

# controller type 1: integrator, 2: integrator, low-pass (no lead), 3: complex controller (see below)
set controller_type_frequency_x 3
set controller_type_frequency_y 3
# time constant for the low-pass
set T_low 0.1;
# orbit_filter_algo 1: explicit but complicated implementation (at the moment just integrator, low-pass and lead);
#                   2: short and fast implementation (integrator, low-pass, lead element and 0.3 Hz peak)
set orbit_filter_algo_x 2
set orbit_filter_algo_y 2

# value can be "dispersion_init" oder "bpm_center"
set use_set_value "dispersion_init"

# Use a scaling of the BPM readings for the controller
set use_bpm_scaling 0

#################
# Imperfections #
#################

# use the bpm_resolution, if 0 then 0.0 resolution
set bpm_noise 1
# in micrometer
set bpm_resolution_ml 0.1
set bpm_resolution_bds 0.05

set stabilization_noise_x 0
set stabilization_noise_y 0
set stabilization_noise_seed 1
set stabilization_noise_type "white_uniform"
set stabilization_noise_param 0.001
# parameter could be 'ALL', 'ML', 'BDS' 
set stabilization_noise_section "ALL"

set stabilization_scaling_error_x 0
set stabilization_scaling_error_y 0
set stabilization_scaling_sigma 0.001
# parameter could be 'ALL', 'ML', 'BDS' 
set stabilization_scaling_section "ALL"

set bpm_scaling_error_x 0
set bpm_scaling_error_y 0
set bpm_scaling_sigma 0.001
# parameter could be 'ALL', 'ML', 'BDS' 
set bpm_scaling_section "ALL"

# RF jitter variables. Each default value corresponds to 1% lumi loss
# phi is in degree and grad in GV/m
set use_rf_jitter 0 
# for random train to train jitter
#set phi_linac 0.2
#set phi_decel 0.6
#set grad_linac 0.000075
#set grad_decel 0.0002
set phi_linac 0.0
set phi_decel 0.0
set grad_linac 0.00000
set grad_decel 0.0000
# for random jitter contant for one machine
set phi_linac_machine 0.0
set phi_decel_machine 0.0
set grad_linac_machine 0.0
set grad_decel_machine 0.0
# for a deterministic acceleration change for one machine
set phi_linac_machine_const 0.0
set grad_linac_machine_const 0.0

# Procedure to remove the orbit created by dispersion
set use_disp_suppression_x 0
set use_disp_suppression_y 0

# Initial jitter: 1 ... beam offset/angle jitter, 2 ... energy jitter
set initial_imperfection_type_x 0
set initial_imperfection_type_y 0
# in [micro metre]
set initial_jitter_x 0
set initial_jitter_y 0
# in [Gev]
set initial_energy_jitter 0

# Constant initial beam energy variation
set use_initial_beam_variation 0
set initial_energy_var_const 0.0

# Quadrupole strength jitter
set use_quad_strength_jitter 0
set quad_strength_sigma_ml 0;# [relative] 
set quad_strength_sigma_bds 0 ;# 
set quad_strength_sigma_ff 0 ;# 
set quad_strength_sigma_fd 0 ;# 
set quad_strength_sigma_ml_machine 0;# [relative] 
set quad_strength_sigma_bds_machine 0 ;# 
set quad_strength_sigma_ff_machine 0 ;# 
set quad_strength_sigma_fd_machine 0 ;# 

# Quadrupole position jitter
set use_quad_position_jitter 0
set quad_position_sigma_ml 0;# [um]
set quad_position_sigma_bds 0 ;# 
set quad_position_sigma_ff 0 ;# 
set quad_position_sigma_fd 0 ;# 

# System Identification parameters
set identification_switch 0
set perfect_controller_matrix 0

# BPM drifts
set use_bpm_drift 0
set bpm_drift_sigma_ml 0.44 ;# [um]
set bpm_drift_sigma_bds 0.44 ;# [um]
set bpm_drift_sigma_ff 0 ;# [um]
set bpm_drift_sigma_fd 0 ;# [um]

# component failures (static)
# possible distributions, "random", "block_begin", "block_end", "block_end_ml", "block_begin_bds", "list"
# 
set use_bpm_failure 0
set bpm_failure(prob) 0 ;# chance of a failed bpm
set bpm_failure(list) {} ;# element numbers of failed bpms (needs distribution list)
set bpm_failure(distribution) "random"
# std deviation of BPM response (random reading with mean 0)
# set bpm_failure(std) 0

set use_corr_failure 0
set corr_failure(prob) 0
set corr_failure(list) {} ;# element numbers of failed correctors
set corr_failure(distribution) "random"

set use_quad_stab_failure 0
set quad_stab_failure(prob) 0
set quad_stab_failure(list) {} ;# element numbers of failed stabilisation
# quadrupole stab failure distribution, "random", "block"
set quad_stab_failure(distribution) "random"

# add noise to response matrix
set use_noisy_matrix_x 0
set use_noisy_matrix_y 0
set R_file_name_x "R_noisy_x.dat"
set R_file_name_y "R_noisy_y.dat"

# L-FB gain errors
set use_gain_error_x 0
set use_gain_error_y 0
set gain_error_std_x 0.1
set gain_error_std_y 0.1

###########################
# IP feedback Parameters: #
###########################

# ip feedback correction independent of orbit correction 	 
set use_ip_feedback 0
set use_ip_feedback_x 1
set use_ip_feedback_y 1
# different ip feedback: linear ip feedback, second order (pid) or annecy (LAPP) optimised
set use_ip_feedback_linear 0
# gain factors for beam based feedback (IP DIPOLE), for linear feedback
set gain_bb_x 0.5
set gain_bb_y 0.5

set use_ip_feedback_pid 1

set use_ip_feedback_annecy 0
# 0: fb 1:fba 2:pid 3:pida
set ip_feedback_annecy_choice 1

# Properties of the BPM after the IP
# distance BPM-IP
set bpm_ip_dist 3.0
set bpm_ip_noise 0
# resolution of BPM at IP [um]
set bpm_ip_res 0.0

##############################
# Postprocessing Parameters: #
##############################

# store data at the measurement stations (after ML, before FD and at IP), 
# switching off will also remove the first initial tracking step.
set save_meas_stations 1

# save beam files and how often (once every x iterations) (careful 10 MB per iteration!):
set save_beam_file 0
set save_beam_file_freq 50

# Logging
set log_column_error 1
# include beam shift to calculate multipulse emittance
set eval_beam_shift 1

###################################
# Specific Simulation Parameters: #
###################################

# start from (non-)perfect machine
set aligned_machine 1
set atl_time_initial 300

# reload beam after bds
set load_beam_bds 0

# Some options for storing data
set use_bpm_storing 0
set bpm_storing_no_noise 0
set use_gm_storing 0
set use_ip_corr_storing 0
set use_ml_corr_storing 0

# Gael options
set external_ground_cms 0
set white_noise_file "white_noise_00pm" ;# [pm]
set use_defl_angle 1
set outputfile_ip_controller "Beam_correction_log_4"

