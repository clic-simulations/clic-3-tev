#no special directory:
set testdir 0

# print/output additional debug information:
set debug 1

# track BDS (should be always on):
set use_bds 1

# apply ground motion:
set use_ground_motion 1

# type: 1: ATL, 2: ground motion generator
# filtertype: 0: no filter 1: use filters specified in x and y for the whole beamline
array set groundmotion {
    type 2
    model "B"
    seed_nr 3
    filtertype 0
    filtertype_x "local_FB_vertical_v2.dat"
    filtertype_y "local_FB_vertical_v2.dat"
}

# apply ground motion in x/y direction
set ground_motion_x 0
set ground_motion_y 1

# long motion (ATL-law) time
set atl_time 10 ;# seconds

# filter options for QF1,QD0
set use_cantilever_filter 0
# default is pre-isolator from F.Ramos et al.
# perfect stabilization instead:
set use_ff_stab 0
# active mechanical feedback from B.Caron et al.:
set use_mech_feedback 0

set bpm_error_IP 0.0 ;# resolution of BPM at IP [um]

# number of machines to simulate
set nm 1

# number of slices and macroparticles for main linac
set n_slice 25
set n_part 15

set frac_lambda 0.25

# number of particles for BDS tracking
set n_total 50000 ;# 150000
set n [ expr $n_total/$n_slice ]
set scale 1.0

# beam variables
array set params {
    ch 3.72e9
    emitt_x 6.6
    emitt_y 0.2
    offset 0.0
    waist_y 0.0
    quad_misalign 0.000
    seed 2
    qjitter 0.0
    niter 10
    distbpm 3.0 ;# distbpm is the distance BPM-IP
    gmotion 2
    gmseed 1142
    time_step 0.02
    wgt 1000.0
    linerr everything
    sigma_z 44.0
    e_spread 1.3
}

#
# GUINEA-PIG related parameters
#

array set gp_param {
    energy 1500.0
    particles [expr $match(charge)*1e-10]
    sigmaz $match(sigma_z)
    cut_x 400.0
    cut_y 35.0
    n_x 128
    n_y 320
    do_coherent 1
    waist_y $params(waist_y)
    emitt_x [expr $match(emitt_x)/10.]
    n_t 1
    charge_sign 0
}