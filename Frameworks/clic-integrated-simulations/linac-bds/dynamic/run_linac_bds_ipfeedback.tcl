# Script to apply ground motion and SVD correction
#
# This script tracks the linac once
# After that the BDS is run independently
# Builtin Ground motion and possible misalignment. It uses an SVD and IP correction method to correct for.

set script_dir [pwd]

# variables
array set params {
    ch 3.72e9
    emitt_x 6.6
    emitt_y 0.2
    offset 0.0
    waist_y 0.0
    quad_misalign 0.000
    seed 1
    qjitter 0.0
    niter 5
    gain_bb_x 0.0
    gain_bb_y 1.0
    distbpm 3.0 
    gmotion 2
    gmseed 1117
    svdbpm 0.00
    time_step 0.02
}

array set params $argv
set seed $params(seed)
set qjitter $params(qjitter)
set niter $params(niter)
set gain_bb_x $params(gain_bb_x)
set gain_bb_y $params(gain_bb_y)
set distbpm $params(distbpm)
set gmotion $params(gmotion)
set gmseed $params(gmseed)
set svdbpm $params(svdbpm)
set time_step $params(time_step)

set resultdir linac_bds_feedback_svdbpm_$params(svdbpm)_time_step_$params(time_step)
exec mkdir -p $resultdir
exec ln -fs $resultdir last_run
cd $resultdir


#
# General
#
Random -type gaussian gcav

# number of machine to simulate
set nm 1

set n_slice 25
set n_part 15

set frac_lambda 0.25

set n_total 150000
set n [ expr $n_total/$n_slice ]
set scale 1.0
# distbpm is the distance BPM-IP
# set time-of-flight kicker-IP/BPM-IP in nanoseconds
set timef [expr $distbpm/3e8*1e9]
# latency time adding the reducible time (from the electronics: 10 ns)
set latency [expr 2*$timef + 17]

# Set BPM resolution in micrometers
set bpmreso 1.0


source $script_dir/scripts/clic_basic_single.tcl
source $script_dir/scripts/clic_beam.tcl


set match(n_total) $n_total

set match(emitt_y) $params(emitt_y)

set match(sigma_z) 44.0

set match(charge) $params(ch)

set charge $match(charge)

set match(emitt_x) $params(emitt_x)

set match(e_spread) 1.3

puts "charge $match(charge)"
puts "emitt_y $match(emitt_y)"

set off $params(offset)
set waist $params(waist_y)

set n_bunches 1

#
# Linacs lattice and functions
#

source $script_dir/lattices/lattice4b.tcl


foreach name { linelectron  linpositron } {

    puts "Putting Linac"
    global n_total n_slice n_part
    BeamlineNew
    set phase 8a
    set e_initial 9.0
    set e0 $e_initial
    Linac::choose_linac $script_dir/lattices/phase.def $phase
    Linac::put_linac
    TclCall -script "save_beam_$name"
    BeamlineSet -name $name
}

proc my_survey { } {
    global params
    set cav0 [CavityGetPhaseList]
    set grad0 [CavityGetGradientList]
    set bpm_res 0.0
#    set jitter 0.1
    set phase_err 0.0
    SurveyErrorSet -quadrupole_y $params(quad_misalign) \
	-bpm_y 0.0 \
	-cavity_y 0.0 \
	-cavity_realign_y 0.0 \
	-cavity_yp 0.0

    # misalign beamline according to survey
    Clic
    set cav {}
    #set grad {}
    #set fcav [open cav.dat.$i w]
    foreach c $cav0 g $grad0 {
	set cc [expr $c+[gcav]*$phase_err]
	#set gg [expr $g+[gcav]*$gradient_err]
	#puts $fcav "$gg $cc"
	lappend cav $cc
	#lappend grad $gg
    }
    # close $fcav
    CavitySetPhaseList $cav
    #CavitySetGradientList $grad
    #SaveAllPositions -vertical_only 0 -binary 0 -cav 1 -nodrift 1 -file buc.$i
    puts "misalignment done"    
}

proc print_beam_params name {
    upvar $name params
    set beam_ener $params(e)
    set beam_emix $params(emitt_x)
    set beam_emiy $params(emitt_y)
    set beam_betx $params(beta_x)
    set beam_bety $params(beta_y)
    set beam_alpx $params(alpha_x)
    set beam_alpy $params(alpha_y)
    set beam_s $params(s)
    puts "beam longitudinal position = $beam_s"
    puts "beam energy = $beam_ener"
    puts "beam emix = $beam_emix"
    puts "beam emiy = $beam_emiy"
    puts "beam betax = $beam_betx"
    puts "beam betay = $beam_bety"
    puts "beam alpx = $beam_alpx"
    puts "beam alpy = $beam_alpy"
    set sig_x [expr sqrt($beam_betx*$beam_emix*1e-7/(3.0*1e6))]
    set sig_y [expr sqrt($beam_bety*$beam_emiy*1e-7/(3.0*1e6))]
    puts "beam sigx = $sig_x"
    puts "beam sigy = $sig_y"
}

proc save_beam_linpositron { } {
    global j
    BeamSaveAll -file positron_linac.$j.ini 
    #     BeamDump -file positron_linac.$j -type 1
    array set params [BeamMeasure]
    print_beam_params params
}

proc save_beam_linelectron { } {
    global j
    BeamSaveAll -file electron_linac.$j.ini 
    #    BeamDump -file electron_linac.$j
    array set params [BeamMeasure]
    print_beam_params params
}

#
# set up the beam
#

foreach beamname { electron  positron } {
    #    Generate beam
    global n_slice n_part n_bunches
    puts "Generating the beam"
    puts "n. slices $n_slice "
    puts "n. particles $n_part "
    make_beam_slice_energy_gradient beam_${beamname} $n_slice $n_part 1.0 1.0
    #make_beam_slice_energy_gradient_train beam_${beamname} $n_slice $n_part 1.0 1.0 $n_bunches
}

# this is needed only to set up the beam in the bds when one simulate the full train in the linac
#foreach beamname { electron1  positron1 } {
#    #    Generate beam
#    global n_slice n_part
#    puts "Generating the beam"
#    puts "n. slices $n_slice "
#    puts "n. particles $n_part "
#    make_beam_slice_energy_gradient beam_${beamname} $n_slice $n_part 1.0 1.0
#}

#
# track linacs
#
for {set j 0} {$j<$nm} {incr j} {
    Zero
    puts "BEGIN LINAC electron "
    BeamlineUse -name linelectron  
    TestNoCorrection -beam beam_electron -emitt_file emitt_linelectron.$j.dat -survey my_survey -machines 1
    puts "BEGIN LINAC positron "
    BeamlineUse -name linpositron 
    TestNoCorrection -beam beam_positron -emitt_file emitt_linpositron.$j.dat -survey my_survey -machines 1
}
#
# putting the BDS lattice and functions
#
proc save_bdselectron {} {
    global k nk
    BeamDump -file bds_electron.$k.$nk
}

proc save_bdspositron {} {
    global k nk
    BeamDump -file bds_positron.$k.$nk
}

foreach line { bdselectron bdspositron } {

    puts "Putting BDS $line "
    BeamlineNew
    Girder
    SlicesToParticles -seed 0 -type 1
    set quad_synrad 1
    set sbend_synrad 1
    set mult_synrad 1
    set synrad 1
    set e_initial 1500.00
    set e0 $e_initial
    #ReferencePoint -sense 1
    source $script_dir/lattices/bds.match.linac4b
    ReferencePoint -sense -1
    FirstOrder 1
    TclCall -script "save_$line"
    BeamlineSet -name $line
    BeamlinePrint -file "bds_beamline_$line.txt"
    # write the grider positions along the beamline in a file
    WriteGirderLength -file tmp.$line -absolute 1 -beginning_only 1
    # change the values of the girder positions in order to have zero at the IP
    # electron are coming from negative s and positron coming from positive s
    exec tclsh $script_dir/scripts/prepare_ground.tcl tmp.$line girder.$line $line

}
#
# GUINEA-PIG related functions
#

array set gp_param {
    energy 1500.0
    particles [expr $match(charge)*1e-10]
    sigmaz $match(sigma_z)
    cut_x 400.0
    cut_y 35.0
    n_x 128
    n_y 640
    do_coherent 1
    n_t 1
    charge_sign -1.0
    waist_y $params(waist_y)
    emitt_x [expr $match(emitt_x)/10.]
}

source $script_dir/scripts/clic_guinea.tcl

proc run_guinea {offx offy} {
    global k nk
    set res [exec grid]
    set yoff [expr $offy-0.5*([lindex $res 2]+[lindex $res 3])]
    set xoff [expr $offx-0.5*([lindex $res 0]+[lindex $res 1])]
    puts " xoff $xoff "
    puts " yoff $yoff "
    
    write_guinea_correct $xoff $yoff
    
    exec  guinea default default default.$k.$nk
    return [get_results default.$k.$nk]
}

#
# track bds: response matrix, ground motion and orbit correction
#

#reset response matrix after each run
set reset_matrix 0
if {$reset_matrix>0} {
    foreach name {electron positron} {
	exec rm R$name.dat
    }
}

for {set k 0} {$k<$nm} {incr k} {
    for {set nk 0} {$nk<$n_bunches} {incr nk} {
	################ Response Matrix #####################################
	puts "NOW obtaining Response matrix"
	foreach name {electron positron} {
	    BeamlineUse -name bds$name
	    BeamLoadAll -beam beam_$name -file ${name}_linac.${k}.ini ;# load linac beam
	    Octave {
		# Here we calculate the "response matrix" for the beam-beam feedback (ie. one number).
		if exist("R$name.dat", "file")
		load R$name.dat;
		disp("Load existing response matrix file");

		else
		# Orbit Correction
		#$name.B = placet_get_name_number_list("$name", "BPMQUAD", "BPMSEXT", "BPMIP");
		$name.B = placet_get_number_list("bds$name", "bpm");
		$name.C = placet_get_number_list("bds$name", "dipole");
		[$name.Rx, $name.Tx] = placet_get_response_matrix_attribute("$name", "beam_$name", $name.B, "x", $name.C, "strength_x", "Zero");
		[$name.Ry, $name.Ty] = placet_get_response_matrix_attribute("$name", "beam_$name", $name.B, "y", $name.C, "strength_y", "Zero");
		[U,S,V] = svd($name.Rx); $name.iRx = -V*pinv(S)*U';
		[U,S,V] = svd($name.Ry); $name.iRy = -V*pinv(S)*U';

		# This selects the beamline to use
		Tcl_Eval("BeamlineUse -name bdspositron");
		Tcl_Eval("BeamLoadAll -beam beam_positron -file positron_linac.\$k.ini");		
		[E, Beam_positron]=placet_test_no_correction("bdspositron", "beam_positron", "Zero");
		save_beam("positron.ini", Beam_positron);
		# This selects the beamline to use
		Tcl_Eval("BeamlineUse -name bdselectron");
		Tcl_Eval("BeamLoadAll -beam beam_electron -file electron_linac.\$k.ini");
		[E, Beam_electron]=placet_test_no_correction("bdselectron", "beam_electron", "Zero");
		save_beam("electron.ini", Beam_electron);

		Tcl_Eval("set res [run_guinea 0.0 0.0]");
		Tcl_Eval("set anglex [lindex \$res 3]");
		Tcl_Eval("set angley [lindex \$res 4]");
		Anglex = str2num(Tcl_GetVar("anglex"));
		Angley = str2num(Tcl_GetVar("angley"));
		$name.Tangle = [ Anglex; Angley ];

		# Calculate response matrix for IPDIPOLE
		$name.Cbb = placet_get_name_number_list("bds$name", "IPDIPOLE");
	
		# This selects the beamline to use
		Tcl_Eval("BeamlineUse -name bds\${name}");
		Tcl_Eval("BeamLoadAll -beam beam_\${name} -file \${name}_linac.\$k.ini");	
		placet_element_set_attribute("bds$name", $name.Cbb, "strength_y", 1);
		[E, Beam_$name] = placet_test_no_correction("bds$name", "beam_$name", "None");
		placet_element_set_attribute("bds$name", $name.Cbb, "strength_y", 0);
		save_beam("$name.ini", Beam_$name);
		Tcl_Eval("set res [run_guinea 0.0 0.0]");
		Tcl_Eval("set angley [lindex \$res 4]");
		Angley = str2num(Tcl_GetVar("angley"));
		placet_element_set_attribute("bds$name", $name.Cbb, "strength_x", 1);
		[E, Beam_$name] = placet_test_no_correction("bds$name", "beam_$name", "None");
		placet_element_set_attribute("bds$name", $name.Cbb, "strength_x", 0);
		Tcl_Eval("set res [run_guinea 0.0 0.0]");
		Tcl_Eval("set anglex [lindex \$res 3]");
		Anglex = str2num(Tcl_GetVar("anglex"));
		$name.Rangle = [ Anglex; Angley ] - $name.Tangle;
		save -text R$name.dat $name

		endif

		disp("The dimensions of the SVD matrix are:")
		matrixsize = size($name.Rx);
		disp(matrixsize);

		S = svd($name.Rx); $name.iRx = -pinv($name.Rx, S(1)*1e-4);
		S = svd($name.Ry); $name.iRy = -pinv($name.Ry, S(1)*1e-4);
	    }
	}
	#####################   APPLY  GM  ####################################
	puts "NOW applying GM"
	
	if {$gmotion == 1} {
	    set gmot "A"
	} elseif {$gmotion == 2} {
	    set gmot "B"
	} elseif {$gmotion == 3} {
	    set gmot "C"
	} elseif {$gmotion == 4} {
	    set gmot "K"
	}
	if {$gmotion != 0} {
	    foreach name {electron positron} {
		exec ground $script_dir/harm.$gmot.300 girder.bds$name 2 $time_step 0 0 0 $gmseed
		BeamlineUse -name bds$name
		Zero
		MoveGirder -vertical_only 1 -file position.1
	    }
	    
#	    # we save the position of the elements after ground motion (to be read in bds.tcl before the tracking)
#	    
#	    foreach name {electron positron} {
#		BeamlineUse -name bds$name
#		SaveAllPositions -file bds_$name.pos -binary 1
#	    }
#	    
#	    # here we read the position file from bds_init.tcl
#	    
#	    foreach name {electron positron} {
#		BeamlineUse -name bds$name
#		ReadAllPositions -file bds_$name.pos -binary 1
#	    }
	} elseif {$gmotion == 0} {	    
	    puts "NO GM applied"	    
	}
	
	Octave {
	    Lumi=zeros($niter,8);
	}
	
	foreach name {electron positron} {
	    BeamlineUse -name bds$name
            BeamLoadAll -beam beam_$name -file ${name}_linac.${k}.ini

	    Octave {
		placet_element_set_attribute("bds$name", $name.B, "resolution", $svdbpm);
		placet_element_set_attribute("bds$name", $name.C, "strength_x", 0);
		placet_element_set_attribute("bds$name", $name.C, "strength_y", 0);
	    }
	}
	##### Orbit correction applying SVD #####
	puts "NOW applying orbit correction"
	if {true} {
	    foreach name {electron positron} {
                BeamlineUse -name bds$name
                BeamLoadAll -beam beam_$name -file ${name}_linac.${k}.ini

		Octave {
		    for i=1:$seed
		    placet_test_no_correction("bds$name", "beam_$name", "None");
		    endfor
		    
		    $name.bsignal=placet_get_bpm_readings("bds$name",$name.B);
		    b=$name.bsignal;
		    save -text bpm_reading_$name.out b;
		    ## Strength corrector calculation
		    # $name.corrx=-pinv($name.Rx)*$name.bsignal(:,1);
		    $name.corry=$name.iRy*$name.bsignal(:,2);
		    
		    #placet_element_set_attribute("bds$name", $name.C, "strength_x", $name.corrx);
		    placet_element_set_attribute("bds$name", $name.C, "strength_y", $name.corry);
		    
		    placet_test_no_correction("bds$name", "beam_$name", "None");
		    $name.bsignal1=placet_get_bpm_readings("bds$name",$name.B);
		    b1=$name.bsignal1;
		    save -text bpm_reading1_$name.out b1;
		}
	    }
	}
	puts "Bringing beams into collision... ";
	
	Octave { 
	    index=1;
	}
	
	#####  HERE THE CONTROL LOOP FOR IP-FB STARTS ####################### 
	
	Octave {
	    time_laten=0.0;
	}
	for {set ii 0} {$ii<$niter} {incr ii} {
	    Octave {
		# here we track the particles
		Tcl_Eval("BeamlineUse -name bdselectron");
                Tcl_Eval("BeamLoadAll -beam beam_electron -file electron_linac.\$k.ini");
		[E, Beam_electron] = placet_test_no_correction("bdselectron", "beam_electron", "None");
		Tcl_Eval("BeamlineUse -name bdspositron");
                Tcl_Eval("BeamLoadAll -beam beam_positron -file positron_linac.\$k.ini");
		[E, Beam_positron] = placet_test_no_correction("bdspositron", "beam_positron", "None");
		save_beam("positron.ini", Beam_positron);
		save_beam("electron.ini", Beam_electron);
		phspacep=load("positron.ini");
		phspacee=load("electron.ini");
		# we run guinea-pig
		Tcl_Eval("set res [run_guinea 0.0 0.0]");
		Tcl_Eval("set lumi [lindex \$res 0]");
		Tcl_Eval("set lumipeak [lindex \$res 1]");
		Tcl_Eval("set anglex [lindex \$res 3]");
		Tcl_Eval("set angley [lindex \$res 4]");
		Tcl_Eval("puts \"Lumi = \$lumi ; Angle = \$anglex ; \$angley \"");
		# IP position calculation
		posy=mean(phspacep(:,3));
		eley=mean(phspacee(:,3));
		# we read the results from Tcl
		Anglex = str2num(Tcl_GetVar("anglex"));
		Angley = str2num(Tcl_GetVar("angley"));
		Lumi(index,1) += index - 1;
		Lumi(index,2) += time_laten;
		Lumi(index,3) += str2num(Tcl_GetVar("lumi"));
		Lumi(index,4) += str2num(Tcl_GetVar("lumipeak"));
		Lumi(index,5) += Anglex;
		Lumi(index,6) += Angley; 
		Lumi(index,7) += posy;
		Lumi(index,8) += eley; 
		index++;
		# we apply the beam-beam feedback # Angle in microrad
		#############  BPM resolution ###################
		Angley=Angley+${bpmreso}*randn(size(Angley))/${distbpm};
		#################################################
		OBS = [ Anglex; Angley ];
		VARYE = -(OBS - electron.Tangle) ./ electron.Rangle; 
		VARYP = -(OBS - positron.Tangle) ./ positron.Rangle;
		# why we vary the strength_x of the IPDIPOLE ????? 
		placet_element_vary_attribute("bdselectron", electron.Cbb, "strength_x", $gain_bb_x * VARYE(1));
		placet_element_vary_attribute("bdselectron", electron.Cbb, "strength_y", $gain_bb_y * VARYE(2));
		placet_element_vary_attribute("bdspositron", positron.Cbb, "strength_x", $gain_bb_x * VARYP(1));
		placet_element_vary_attribute("bdspositron", positron.Cbb, "strength_y", $gain_bb_y * VARYP(2));
		
		# Latency time 
		time_laten = time_laten + ${latency};
	    }
	}
	
	Octave {
	    save -text "lumi_test_${gain_bb_y}.res_${gmseed}" Lumi;
	}
	
	# Save data without comment lines (!)
	set fr [open lumi_test_$gain_bb_y.res_$gmseed r];
	set fres [open resulta.out w];
	
	for {set i 0} {$i<5} {incr i} {
	    gets $fr line
	}
	gets $fr line
	while {![eof $fr]} {
	    puts $fres $line
	    gets $fr line
	}
	close $fr
	close $fres
    }
}
puts "The END"
exit

