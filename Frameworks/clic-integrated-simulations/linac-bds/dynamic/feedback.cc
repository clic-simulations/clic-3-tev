#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#define N 100000

main()
{
  double s[N],kx[N],xs[N],ky[N],ys[N],dx[N],dy[N],xso[N],yso[N],dummy,ktmpx,ktmpy;

  double gi=1.0;gp=0.25,gd=0.5,gd2=1.0;

  int i,j,n=0,m=20;
  FILE *f;
  char *point,buffer[1024];

  f=fopen("position.0","r");
  while (point=fgets(buffer,1024,f)) {
    s[n]=strtod(point,&point);
    kx[n]=0.0;
    ky[n]=0.0;
    kxo[n]=0.0;
    kyo[n]=0.0;
    xso[n]=0.0;
    yso[n]=0.0;
    xs[n]=strtod(point,&point);
    ys[n++]=strtod(point,&point);
  }
  fclose(f);
  for (j=1;j<m;++j) {
    sprintf(buffer,"position.%d",j);
    f=fopen(buffer,"r");
    for (i=0;i<n;++i) {
      point=fgets(buffer,1024,f);
      dummy=strtod(point,&point);
      dx[i]=strtod(point,&point);
      dy[i]=strtod(point,&point);
      ktmpx=kx[i];
      ktmpy=ky[i];
      kx[i]=gi*kx[i]+xs[i]*gp+gd*(xs[i]-xso[i])+gd2*(kx[i]-kxo[i]);
      ky[i]=gi*ky[i]+ys[i]*gp+gd*(ys[i]-yso[i])+gd2*(ky[i]-kyo[i]);
      kxo=ktmpx;
      kyo=ktmpy;
      xs[i]=xs[i]+dx[i]-kx[i];
      ys[i]=ys[i]+dy[i]-ky[i];
    }
    fclose(f);
  }
  f=fopen("position.feed","w");
  for (i=0;i<n;++i) {
    fprintf(f,"%g %g %g\n",s[i],xs[i],ys[i]);
  }
  fclose(f);
}
