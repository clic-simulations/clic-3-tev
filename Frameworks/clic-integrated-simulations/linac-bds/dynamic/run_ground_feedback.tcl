set gmtype [lindex $argv 1]
set qstab [lindex $argv 2]
switch $qstab {
    0 {
	set name test_feed_nocorr_$gmtype
    }
    1 {
	set name test_feed_qstab_$gmtype
    }
    2 {
	set name test_feed_all_$gmtype
    }
}

exec mkdir -p $name
set script_dir [pwd]
cd $name

Random -type gaussian gcav

#
# Define beam parameters
#

set nm 50
set n_slice 21
set n_part 7

set phase 8a

set frac_lambda 0.25

set n_total 30000

set e_initial 9.0
set e0 $e_initial

set scale 1.0

source $script_dir/scripts/clic_basic_single.tcl
source $script_dir/scripts/clic_beam.tcl
set charge $match(charge)

foreach name {electron positron} {
    BeamlineNew
    source $script_dir/lattices/lattice4b.tcl
    Linac::choose_linac $script_dir/lattices/phase.def $phase
    Girder
    if {$qstab<2} {
	ReferencePoint -sens 1
    }
    Linac::put_linac
    SlicesToParticles -seed 0
    set quad_synrad 1
    set sbend_synrad 1
    set mult_synrad 1
    set e0 1500.0
    source $script_dir/lattice/bds.match.linac4b
    if {$qstab} {

    } {
	ReferencePoint -sens -1
    }
    TclCall -script save_$name
    BeamlineSet -name $name
    WriteGirderLength -file girder.$name
}
source $script_dir/scripts/imperfection.tcl
source $script_dir/scripts/define_feedback.tcl
source $script_dir/scripts/overlay.tcl
source $script_dir/scripts/guinea.tcl

set gp_param(cut_y) 30.0
set gp_param(n_y) 320

#
# Adjust longitudinal positions
#

set f [open girder.electron r]
set l1 ""
gets $f l
while {![eof $f]} {
    lappend l1 $l
    gets $f l
}
close $f
set shift [lindex $l1 end]
set f [open girder.electron w]
foreach x $l1 {
    puts $f [expr $x-$shift]
}
close $f

set f [open girder.positron r]
set l1 ""
gets $f l
while {![eof $f]} {
    lappend l1 $l
    gets $f l
}
close $f
set shift [lindex $l1 end]
set f [open girder.positron w]
foreach x $l1 {
    puts $f [expr $shift-$x]
}
close $f

#
# Identify final doublet
#

set ql [QuadrupoleNumberList]
set ml [MultipoleNumberList]
if {$qstab<2} {
    set qfd [lrange $ql end-3 end]
    set qfd [concat $qfd [lrange $ml end-6 end]]
} {
    set qfd [concat $ql $ml]
}

#
# Generate beam
#

set match(n_total) $n_total

source $script_dir/scripts/clic_beam.tcl
set match(emitt_y) 0.2
set match(emitt_x) 6.6

make_beam_slice_energy_gradient beam0 $n_slice $n_part 1.0 1.0

proc save_electron {} {
    BeamDump -file electron.ini
}

proc save_positron {} {
    BeamDump -file positron.ini
}

set nstep 21
set dt [lindex $argv 0]

set f [open result.$dt w]

for {set i 0} {$i<$nm} {incr i} {
    BeamlineUse -name electron
    Zero
    BeamlineUse -name positron
    Zero

    set offset_x 0.0
    set offset_y 0.0

    exec $script_dir/ground2 $script_dir/harm_$gmtype.300 girder.electron $nstep $dt 0.0 $i
    exec $script_dir/feedback
    exec cp position.feed position_electron.feed

    exec $script_dir/ground2 $script_dir/harm_$gmtype.300 girder.positron $nstep $dt 0.0 $i
    exec $script_dir/feedback
    exec cp position.feed position_positron.feed

    BeamlineUse -name electron
    MoveGirder -file position_electron.feed
    if {$qstab} {
	foreach x $qfd {
	    ElementSetToOffset $x -y 0.0 -x 0.0 -angle_x 0.0 -angle_y 0.0
	}
    }
    TestNoCorrection -beam beam0 -emitt_file emitt.dynamic \
	-machines 1 -survey None

    BeamlineUse -name positron
    MoveGirder -file position_positron.feed
    if {$qstab} {
	foreach x $qfd {
	    ElementSetToOffset $x -y 0.0 -x 0.0 -angle_x 0.0 -angle_y 0.0
	}
    }
    TestNoCorrection -beam beam0 -emitt_file emitt.dynamic \
	-machines 1 -survey None
    
    write_guinea_correct $offset_x $offset_y
    exec $script_dir/guinea default default step.feed
    set res [get_results step.feed]
    #	set offset_y [expr $offset_y+[lindex $res 4]*0.2/13.6]
    puts $f "$i 0 $res"
    flush $f
}
close $f
