set script_dir [pwd]
# global variables
array set params {
    ch 3.72e9
    emitt_x 6.6
    emitt_y 0.1
    offset 0.0
    waist_y 0.0
    linerr everything
    gp_options default_bkg_opt
    atl 0
}

array set params $argv
exec mkdir -p bkg_3TeV_emix_$params(emitt_x)_emiy_$params(emitt_y)_off_$params(offset)_linerr_$params(linerr)_gp_$params(gp_options)
cd bkg_3TeV_emix_$params(emitt_x)_emiy_$params(emitt_y)_off_$params(offset)_linerr_$params(linerr)_gp_$params(gp_options)
#
# General
#
Random -type gaussian gcav

# number of machine to simulate
set nm 50

set n_slice 25
set n_part 15

set frac_lambda 0.25

set n_total 150000
set n [ expr $n_total/$n_slice ]
set scale 1.0


source $script_dir/clic_basic_single.tcl
source $script_dir/clic_beam.tcl


set match(n_total) $n_total

set match(emitt_y) $params(emitt_y)

set match(sigma_z) 44.0

set match(charge) $params(ch)

set charge $match(charge)

set match(emitt_x) $params(emitt_x)

#set match(e_spread) -1.0

puts "charge $match(charge)"
puts "emitt_y $match(emitt_y)"

set off $params(offset)
set waist $params(waist_y)
set tatl  $params(atl)

#####################################################
# Linacs beamlines
#

source $script_dir/lattice4b.tcl

foreach name { linelectron  linpositron } {
    puts "Putting Linac"
    global n_total n_slice n_part
    BeamlineNew
    set phase 8a
    set e_initial 9.0
    set e0 $e_initial
    Linac::choose_linac $script_dir/phase.def $phase
    Linac::put_linac
    TclCall -script "save_beam"
    BeamlineSet -name $name
    # this is needed in order to overwrite the sliced beam info
}
set cav0 [CavityGetPhaseList]
set grad0 [CavityGetGradientList]
###########################################################
# Beam definitions
#
#set n_bunches 312
set n_bunches 1
if [ expr $n_bunches > 1 ] {
 foreach beamname { electron  positron } {
    #    Generate beam: multibunch 
    global n_slice n_part n_bunches
    puts "Generating the beam"
    puts "n. slices $n_slice "
    puts "n. particles $n_part "
    #    make_beam_slice_energy_gradient beam_${beamname} $n_slice $n_part 1.0 1.0
    make_beam_slice_energy_gradient_train beam_${beamname} $n_slice $n_part 1.0 1.0 $n_bunches
 }
 foreach beamname { electron1  positron1 } {
    #    Generate beam
    global n_slice n_part
    puts "Generating the beam"
    puts "n. slices $n_slice "
    puts "n. particles $n_part "
    #    make_beam_slice_energy_gradient beam_${beamname} $n_slice $n_part 1.0 1.0
    make_beam_slice_energy_gradient beam_${beamname} $n_slice $n_part 1.0 1.0 
 }
} else {
 foreach beamname { electron  positron } {
    #    Generate beam: single bunch
    global n_slice n_part n_bunches
    puts "Generating the beam"
    puts "n. slices $n_slice "
    puts "n. particles $n_part "
    make_beam_slice_energy_gradient beam_${beamname} $n_slice $n_part 1.0 1.0
 }
}
# test beams
make_beam_slice_energy_gradient beam0a 1 1 1.0 1.0
set match(sigma_z) 60.0
make_beam_slice_energy_gradient beam3 $n_slice $n_part 0.95 0.9
make_beam_slice_energy_gradient beam3a 1 1 0.95 0.9
#####################################################################
# Misalignments and tracking
# 
# choose and set the errors
#set err jitter01
set err $params(linerr)
source $script_dir/errors.tcl
set mm my.$err
set bpm_scale 0.0
# select the method
set method dfs
set wgt 1000
set bin_iter 3

#set atl_time 1e6
foreach line {electron positron} {
# initial misalignment    
    proc my_survey {} {
	global j err line
	global cav0 grad0
	global phase_err gradient_err bpm_scale
	Clic
	if {$err=="full"} {
	    InterGirderMove -scatter_y 12.0 -flo_y 5.0
	}
	if {$err=="everything"} {
	    InterGirderMove -scatter_y 12.0 -flo_y 5.0
	}
	if {$err=="girder"} {
	    InterGirderMove -scatter_y 12.0 -flo_y 5.0
	}
	if {$err=="girder2"} {
	    InterGirderMove -scatter_y 12.0 -flo_y 0.0
	}
	if {$err=="girder3"} {
	    InterGirderMove -scatter_y 0.0 -flo_y 5.0
	}
	#	    apply_survey_data_switch girder.data    
#	set cav {}
#	set grad {}
#	set fcav [open cav.$line.dat.$j w]
#	foreach c $cav0 g $grad0 {
#	    set cc [expr $c+[gcav]*$phase_err]
#	    set gg [expr $g+[gcav]*$gradient_err]
#	    puts $fcav "$gg $cc"
#	    lappend cav $cc
#	    lappend grad $gg
#	}
#	close $fcav
#	CavitySetPhaseList $cav
#	CavitySetGradientList $grad
	SaveAllPositions -vertical_only 0 -cav_grad_phas 1 -binary 0 -cav_bpm 1 -nodrift 1 -file $line.buc.$j
	puts "misalignment done"
    }
# 1-to-1 correction
    proc save_beam { } {
	global j line
	if {[EmittanceRun]} {
	    SaveAllPositions -vertical_only 0 -cav_grad_phas 1 -binary 0 -file $line.b121.$j -cav_bpm 1 -nodrift 1
	    incr j
	}
    }
    set j 0
    Zero
    CavitySetPhaseList $cav0
    CavitySetGradientList $grad0
    puts "BEGIN LINACs $line"
    BeamlineUse -name lin$line  
    TestSimpleCorrection -beam beam_$line -emitt_file $line.$mm.121 \
	-survey my_survey -machines $nm
# dfs correction
    proc my_survey {} {
	global j line
	global cav0 bpm_scale
	ReadAllPositions -file $line.b121.$j -cav_grad_phas 1 -vertical_only 0 -binary 0 -cav_bpm 1 -nodrift 1
	SetBpmScaleError -sigma_y $bpm_scale
    }
    proc save_beam { } {
	global j line
	if {[EmittanceRun]} {
	    BpmRealign
	    SaveAllPositions -vertical_only 0 -cav_grad_phas 1 -binary 0 -file $line.bl.$j -cav_bpm 1 -nodrift 1
	    incr j
	}
    }
    set j 0
    switch $method {
	nlc {
	    TestSimpleAlignment -beam beam_$line -emitt_file $line.$mm \
		-survey Nlc -jitter_y $jitter \
		-machines $nm -position_wgt 0.01 -binlength 36 \
		-binoverlap 18 -bpm_res $bpm_res
	}
	ball {
	    TestBallisticCorrection -beam beam_$line -emitt_file $mm \
		-survey Nlc -jitter_y $jitter \
		-machines $nm -binlength 12 -bpm_res $bpm_res
	}
	dfs {
	    set mm $mm.$wgt
	    Zero
	    CavitySetPhaseList $cav0
	    CavitySetGradientList $grad0
	    TestMeasuredCorrection \
		-beam0 beam_$line -beam1 beam3 -cbeam0 beam0a -cbeam1 beam3a\
		-jitter_y [expr 1.0*$jitter] \
		-emitt_file $line.$mm.$err -survey my_survey -machines $nm \
		-wgt1 $wgt -bin_iteration $bin_iter -correct_full_bin 1 \
		-binlength 36 -binoverlap 18 -bpm_res $bpm_res
	}
	dfs2 {
	    set mm $method.$wgt
	    Zero
	    CavitySetPhaseList $cav0
	    CavitySetGradientList $grad0
	    set qtmp0 [QuadrupoleGetStrengthList]
	    set qtmp1 ""
	    foreach qq $qtmp0 {
		lappend qtmp1 $qq
	    }
	    TestMeasuredCorrection \
		-beam0 beam_$line -beam1 beam_$line \
		-jitter_y [expr 1.0*$jitter] \
		-quad0 $qtmp0 -quad1 -$qtmp1 \
		-emitt_file $line.$mm.$err -survey my_survey -machines $nm \
		-wgt1 10000 -bin_iteration $bin_iter -correct_full_bin 1 \
		-binlength 36 -binoverlap 18 -bpm_res $bpm_res
	}
	default {
	    puts "I do not know correction method $method"
	    exit
	}
    }
# Rf align    
    proc my_survey {} {
	global j line
	global cav0
	ReadAllPositions -file $line.bl.$j -cav_grad_phas 1 -vertical_only 0 -binary 0 -cav_bpm 1 -nodrift 1	
    }
    proc save_beam { } {
	global j line
	if {[EmittanceRun]} {
	    SaveAllPositions -vertical_only 0 -cav_grad_phas 1 -binary 0 -file $line.bx.$j -cav_bpm 1 -nodrift 1
	    BeamSaveAll -file linac_beam$line.$j -bunches 1 -axis 1
	    BeamDump -file beam$line.$j 
	    incr j
	}
    }
    set j 0
    Zero
    CavitySetPhaseList $cav0
    CavitySetGradientList $grad0
    TestRfAlignment -beam beam_$line -emitt_file $line.$mm.rf -machines $nm \
	-survey my_survey -testbeam beam_$line

# now introduce dispersion in the machine 
# adding ground motion from ATL low
    proc my_survey {} {
	global j line
	global cav0
	ReadAllPositions -file $line.bx.$j -cav_grad_phas 1 -vertical_only 0 -binary 0 -cav_bpm 1 -nodrift 1	
	Atl
    }
    proc save_beam { } {
	global j line
	if {[EmittanceRun]} {
	    SaveAllPositions -vertical_only 0 -cav_grad_phas 1 -binary 0 -file $line.atl.bx.$j -cav_bpm 1 -nodrift 1
	    BeamSaveAll -file linac_beam$line.atl.$j -bunches 1 -axis 1
	    BeamDump -file beam$line.atl.$j
	    incr j     
	}
    }
    set j 0
    set atl_time $tatl
    TestSimpleCorrection -beam beam_$line -emitt_file $line.atl.121 \
	        -survey my_survey -machines $nm
}

puts " END of LINACs "
puts " START of BDSs "
# reset sigma_z value for the GUINEA-PIG calculation
set match(sigma_z) 44.0
# proc save_bdselectron {} {
#     global k nk line
#     BeamDump -file bds_electron.$k.$nk
#     array set params [BeamMeasure]
#     set beam_emix $params(emitt_x)
#     set beam_emiy $params(emitt_y)
#     set beam_betx $params(beta_x)
#     set beam_bety $params(beta_y)
#     set beam_alpx $params(alpha_x)
#     set beam_alpy $params(alpha_y)
#     set beam_s $params(s)
#     puts "beam longitudinal position = $beam_s"
#     puts "beam emix = $beam_emix"
#     puts "beam emiy = $beam_emiy"
#     puts "beam betax = $beam_betx"
#     puts "beam betay = $beam_bety"
#     puts "beam alpx = $beam_alpx"
#     puts "beam alpy = $beam_alpy"
#     set sig_x [expr sqrt($beam_betx*$beam_emix*1e-7/(3.0*1e6))]
#     set sig_y [expr sqrt($beam_bety*$beam_emiy*1e-7/(3.0*1e6))]
#     puts "beam sigx = $sig_x"
#     puts "beam sigy = $sig_y"
# }

proc save_beam {} {
    global k nk line
    BeamDump -file bds_$line.$k.$nk
    array set params [BeamMeasure]
    set beam_emix $params(emitt_x)
    set beam_emiy $params(emitt_y)
    set beam_betx $params(beta_x)
    set beam_bety $params(beta_y)
    set beam_alpx $params(alpha_x)
    set beam_alpy $params(alpha_y)
    set beam_s $params(s)
    puts "beam longitudinal position = $beam_s"
    puts "beam emix = $beam_emix"
    puts "beam emiy = $beam_emiy"
    puts "beam betax = $beam_betx"
    puts "beam betay = $beam_bety"
    puts "beam alpx = $beam_alpx"
    puts "beam alpy = $beam_alpy"
    set sig_x [expr sqrt($beam_betx*$beam_emix*1e-7/(3.0*1e6))]
    set sig_y [expr sqrt($beam_bety*$beam_emiy*1e-7/(3.0*1e6))]
    puts "beam sigx = $sig_x"
    puts "beam sigy = $sig_y"
}

# putting the BDS lattice

foreach line { bdselectron bdspositron } {
    puts "Putting BDS $line "
    BeamlineNew
    Girder
    SlicesToParticles -seed 0 -type 1 
    set quad_synrad 1
    set sbend_synrad 1
    set mult_synrad 1
    set e_initial 1500.0
    set e0 $e_initial
    source $script_dir/bds.match.linac4b
    ReferencePoint -sense -1
    FirstOrder 1
    TclCall -script "save_beam"
    BeamlineSet -name $line
}

# first set the gp_param array and then source the clic_guinea.tcl script

array set gp_param "
    energy 1500.0
    particles 0.372
    sigmaz $match(sigma_z)
    cut_x 1500.0
    cut_y 36.0
    n_x 256
    n_y 640
    do_coherent 1
    n_t 1
    charge_sign -1.0
    ecm_min 2970."


source $script_dir/clic_guinea.tcl

proc run_guinea {offx offy} {
    global gp_param k nk params 
    set res [exec grid]
    set yoff [expr $offy-0.5*([lindex $res 2]+[lindex $res 3])]
    set xoff [expr $offx-0.5*([lindex $res 0]+[lindex $res 1])]
    set tx $gp_param(cut_x)
    set ty $gp_param(cut_y)
    if {[lindex $res 1]-[lindex $res 0]>2.0*$tx} {
      set gp_param(cut_x) [expr 0.5*([lindex $res 1]-[lindex $res 0])]
    }
    if {[lindex $res 3]-[lindex $res 2]>2.0*$ty} {
      set gp_param(cut_y) [expr 0.5*([lindex $res 3]-[lindex $res 2])]
    }
    puts "yoff $yoff"
    puts "yoff $xoff"
    puts $res
    #
    # Modify this
    #
    #write_guinea_correct_angle_xy $xoff 0.0 $yoff 0.0
    write_guinea_correct $xoff $yoff
    exec guinea default default_bkg_opt default
    #exec guinea default default default
    exec  cp photon.dat  photon.dat.$k.$nk
    exec  cp hadron.dat  hadron.dat.$k.$nk
    exec  cp beam1.dat   beam1.dat.$k.$nk
    exec  cp beam2.dat   beam2.dat.$k.$nk
    exec  cp coh1.dat    coh1.dat.$k.$nk
    exec  cp coh2.dat    coh2.dat.$k.$nk
    exec  cp pairs.dat   pairs.dat.$k.$nk
    exec  cp lumi.ee.out lumi.ee.out.$k.$nk
    exec  cp default     default.$k.$nk

    #exec  grep lumi_ee= default > lumi.$k.$nk
    #exec  grep lumi_ee_high= default > lumi_high.$k.$nk
    puts " guinea output copy done"
    set gp_param(cut_x) $tx
    set gp_param(cut_y) $ty
    return [get_results default]
}

for {set k 0} {$k<$nm} {incr k} {
    puts " machine $k / $nm "
    for {set nk 0} {$nk<$n_bunches} {incr nk} {
	#    set nk 0
	foreach line {electron positron} {
	    BeamlineUse -name bds$line
 	    # now track
	    if [ expr $n_bunches > 1 ] {
		BeamLoadAll -beam beam_$line -file linac_beam$line.atl.$k.$nk
		puts "Tracking BDS $line "
		TestNoCorrection -beam beam_$line1 -emitt_file emitt.bds$line.$k.$nk -machines 1 -survey Zero
	    } else {       
		BeamLoadAll -beam beam_$line -file linac_beam$line.atl.$k.$nk
		puts "Tracking BDS $line "
		TestNoCorrection -beam beam_$line -emitt_file emitt.bds$line.$k.$nk -machines 1 -survey Zero 
	    }
	}
	# 	BeamlineUse -name bdspositron
	#         if [ expr $n_bunches > 1 ] {
	#          BeamLoadAll -beam beam_positron1 -file positron_linac.$k.ini.$nk
	#          puts "Tracking BDS positron "
	#          TestNoCorrection -beam beam_positron1 -emitt_file emitt.bdspositron.$k.$nk -machines 1 -survey Zero
	#         } else {
	#          BeamLoadAll -beam beam_positron -file positron_linac.$k.ini.$nk
	#         puts "Tracking BDS positron "
	#         TestNoCorrection -beam beam_positron -emitt_file emitt.bdspositron.$k.$nk -machines 1 -survey Zero
	#         }
	puts " RUNNING GUINEA-PIG "
	exec cp bds_electron.$k.$nk electron.ini
	exec cp bds_positron.$k.$nk positron.ini
        run_guinea 0.0 $off 
    }
}

exit


