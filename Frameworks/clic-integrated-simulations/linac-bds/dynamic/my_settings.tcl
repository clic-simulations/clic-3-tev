# Demo version of a personal settings file.

# Overwrite default settings:

set debug 0
set ground_motion_x 1
set ground_motion_y 1
set groundmotion(type) 2
#set groundmotion(seed_nr) 5

# nr of particles:
#set n_total 150000

set dipole_correctors 0

set nr_machines 1
set nr_time_steps 10
set use_main_linac 1
set use_bds 1
set use_beam_beam 1

set use_controller_x 1
set use_controller_y 1
set use_ip_feedback 1

set dir_name "test_my_settings"
set multipulse_nr 3
