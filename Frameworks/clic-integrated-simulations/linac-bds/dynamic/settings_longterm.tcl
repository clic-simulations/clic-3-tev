set delta_T_long [expr 60*60]
set nr_time_steps_long 2

array set groundmotion_long {
    ml 1
    bds 1
    time_step $delta_T_long
}

##############################
# Long-term alignment/tuning #
##############################

set use_user_long_term_alignment_script 0
set user_long_term_alignment_script_name "empty_script"
set response_matrix_calc_longterm 0

# 1 ... whole linac, 2 ... BDS only, 3 ... ML only
set long_term_atl_section 1

set response_matrix_calc_longterm 0

set use_basic_steering 0
# 2 ... all-to_all steering
set basic_steering_method 2
# 1 ... corr everywhere, 2 ... corr ML, 3 ... corr ML - BPM ML
set basic_steering_option 1
set basic_steering_matrix_name_x "R0_x_complete.dat"
set basic_steering_matrix_name_y "R0_y_complete.dat"
set basic_steering_modulo 1
set basic_steering_beta 1

#set dfs_matrix_name_x "R_delta_E_x_complete.dat"
#set dfs_matrix_name_y "R_delta_E_y_complete.dat"
set dfs_delta_grad 0.01
set dfs_delta_phi 0.00
set use_theoretical_dfs_omega 1
set dfs_omega_value 1

set use_bds_knobs 0