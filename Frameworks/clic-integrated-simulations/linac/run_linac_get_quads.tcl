set script_dir [pwd] 
# global variables
array set params {
    ch 3.72e9
    emitt_x 5.5
    emitt_y 0.1
    offset 0.0
    waist_y 0.0
    linerr none
    wgt 1000.0
}

array set params $argv
exec mkdir -p dfs_linac_emix_$params(emitt_x)_emiy_$params(emitt_y)_wgt_$params(wgt)_linerr_$params(linerr)
cd dfs_linac_emix_$params(emitt_x)_emiy_$params(emitt_y)_wgt_$params(wgt)_linerr_$params(linerr)
#
# General
#
Random -type gaussian gcav

# number of machine to simulate
set nm 100 

set n_slice 25
set n_part 15

set frac_lambda 0.25

set n_total 15000
set n [ expr $n_total/$n_slice ]
set scale 1.0


source $script_dir/scripts/clic_basic_single.tcl
source $script_dir/scripts/clic_beam.tcl


set match(n_total) $n_total
set match(emitt_x) $params(emitt_x)
set match(emitt_y) $params(emitt_y)
set match(sigma_z) 44.0
set match(charge) $params(ch)
set charge $match(charge)

set off $params(offset)
set waist $params(waist_y)

#####################################################
# Linacs beamlines
#

source $script_dir/lattices/lattice4b.tcl

foreach name { linelectron } {
    puts "Putting Linac"
    global n_total n_slice n_part
    BeamlineNew
    set phase 8a
    set e_initial 9.0
    set e0 $e_initial
    Linac::choose_linac $script_dir/lattices/phase.def $phase
    Linac::put_linac
    TclCall -script "save_beam"
    BeamlineSet -name $name
    # this is needed in order to overwrite the sliced beam info
}

Octave {
    QI = placet_get_number_list("linelectron", "quadrupole");
    QS = placet_element_get_attribute("linelectron", QI, "strength");
    QZ = placet_element_get_attribute("linelectron", QI, "s");
    QE = placet_element_get_attribute("linelectron", QI, "e0");
    QL = placet_element_get_attribute("linelectron", QI, "length");
    QG = (QS ./ QL * 3.335641);
    Radius = 5e-3 * ones(size(QI));
    T = [ QL' QG' Radius' * 1e3 abs(QG' .* Radius') ];
    save -text ../linac_quads.dat T
}

Octave
