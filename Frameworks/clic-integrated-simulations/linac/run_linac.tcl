set script_dir [pwd] 
# global variables
array set params {
    ch 3.72e9
    emitt_x 6
    emitt_y 0.1
    offset 0.0
    waist_y 0.0
    linerr none
}

array set params $argv

#
# General
#

source $script_dir/scripts/clic_basic_single.tcl
source $script_dir/scripts/clic_beam.tcl

##############################################

source $script_dir/lattices/lattice4b.tcl

puts "Putting Linac"
global n_total n_slice n_part
BeamlineNew
set phase 8a
set e_initial 9.0
set e0 $e_initial
Linac::choose_linac $script_dir/lattices/phase.def $phase
Linac::put_linac
BeamlineSet -name linelectron

##############################################

set n_total 15000
set n_slice 25
set n 31

set scale 1.0

set e_initial 9.0
set e0 $e_initial

set match(n_total) $n_total
set match(beta_x) 2.02111
set match(beta_y) 6.68204
set match(alpha_x) 0.0
set match(alpha_y) 0.0
set match(e_spread) 2.0
set match(emitt_x) $params(emitt_x)
set match(emitt_y) $params(emitt_y)
set match(sigma_z) 44.0
set match(charge) $params(ch)
set charge $match(charge)

make_beam_slice beam0 $n_slice $n

##############################################

BeamlineList -file linac_list.txt

TestNoCorrection -beam beam0 -emitt_file emitt.dat

SetReferenceEnergy -beamline linelectron -beam beam0 -iter 1



Octave {

    global COLS_NAME % preceeded by "NAME" and "KEYWORD" and followed by "Type"
    COLS_NAME = { "S" ; "L" ; "E0" ; "K1L" ; "K1SL" ; "K2L" ; "K2SL" ; "KS" ; "ANGLE" ; "E1" ; "E2" ; "Strength" };
    ALL = placet_element_get_attributes("linelectron");
    TABLE_NAME = [];
    TABLE_KEY  = [];
    TABLE_DATA = [];
    TABLE_TYPE = [];
    function T = append_to_row(T, name, value)
        global COLS_NAME
        T(find(strcmp(COLS_NAME, name))) = value;
    end

    %% element by element
    sigma_quad = 10; % um, rms misalignment of quadrupoles
    last_quad_K1L = 0.0; % 1/m
    
    index = 1;    
    for i=1:size(ALL,1);
        THIS = ALL{i};
        T = zeros(1, size(COLS_NAME,1));
        T = append_to_row(T, "S", THIS.s);
        T = append_to_row(T, "L", THIS.length);
        T = append_to_row(T, "E0", THIS.e0);
        switch THIS.type_name
         case "quadrupole"
            strength = abs(THIS.strength / 0.299792458);
            T = append_to_row(T, "K1L", THIS.strength / THIS.e0);
            T = append_to_row(T, "Strength", strength); % GV/c/m = (1 / 0.299792458) T
            last_quad_K1L = THIS.strength / THIS.e0; % 1/m
            %if THIS.length == 0.3
              if     strength <= 0.57 ; THIS.comment = "Q5" ;
              elseif strength <= 1.2  ; THIS.comment = "Q4T1" ;
              elseif strength <= 2.43 ; THIS.comment = "Q4T2" ;
              elseif strength <= 3.6  ; THIS.comment = "Q4T3" ;
              elseif strength <= 5.5  ; THIS.comment = "Q3T1" ;
              elseif strength <= 7.8  ; THIS.comment = "Q3T2" ;
              elseif strength <= 11.1 ; THIS.comment = "Q3T3" ;
              elseif strength <= 15   ; THIS.comment = "Q2" ;
              elseif strength <= 21   ; THIS.comment = "Q1" ;
            endif
           %endif
         case "sbend"
            T = append_to_row(T, "ANGLE", THIS.angle);
            T = append_to_row(T, "E1", THIS.E1);
            T = append_to_row(T, "E2", THIS.E2);
            T = append_to_row(T, "Strength", THIS.e0 * THIS.angle / 0.299792458); % GV/c = (1 / 0.299792458) T*m
         case "multipole"
            THIS.type_name = "sextupole";
            strength = abs(THIS.strength / 0.299792458);
            T = append_to_row(T, "K2L", THIS.strength / THIS.e0);
            T = append_to_row(T, "Strength", strength); % GV/c/m^2 = (1 / 0.299792458) T/m
            if     strength <= 300 ; THIS.comment = "SX2" ;
            elseif strength <= 600 ; THIS.comment = "SX1" ;
            endif
          case "bpm"
          case "dipole"
            THIS.type_name = "kicker";
            max_strength = 3*sigma_quad*abs(last_quad_K1L)*THIS.e0*1e3; % V, 3 * sigma_quad * last_quad_K1L * THIS.e0
            T = append_to_row(T, "Strength", max_strength / 299792458); % T*m
          case "cavity"
            T = append_to_row(T, "Strength", 1e3 * THIS.gradient * THIS.length); % MV
            T = append_to_row(T, "ANGLE", THIS.phase / 180.0 * pi); % rad
          case "solenoid"
            T = append_to_row(T, "KS", 0.29979246 * THIS.bz); % solenoid strength from T/(GeV/c) to 1/m
            T = append_to_row(T, "Strength", 2.9); % T
          case "drift"
            if strfind(THIS.name, "MARKER")
              THIS.type_name = "marker";
            endif
        end
        
        %% for all types
        switch THIS.type_name
          case { "quadrupole" , "sbend", "sextupole", "solenoid", "bpm" , "kicker" , "cavity", "marker" }
            TABLE_DATA(index,:) = T;
            TABLE_NAME{index} = THIS.name;
            TABLE_KEY {index} = toupper(THIS.type_name);
            TABLE_TYPE{index} = THIS.comment;
            index++;
        end
    end
    
    %% save on disk
    fid = fopen("elements_table_elinac.txt", 'w');
    if fid != -1
        fprintf(fid, '* NAME\tKEYWORD\t%s\n', strjoin (COLS_NAME, '\t'));
        fprintf(fid, '* <STR> <STR> [m] [m] [GeV] [m^-1] [m^-1] [m^-2] [m^-2] [m^-1] [rad] [rad] [rad] <STR>\n');
        for i=1:size(TABLE_DATA,1)
            fprintf(fid, [ '\"%s\"\t\"%s\"\t%.3f\t' repmat('%g\t', 1, size(TABLE_DATA,2)-1) '\n' ], TABLE_NAME{i}, TABLE_KEY{i}, TABLE_DATA(i,:));
        end
        fclose(fid);
    end
}
