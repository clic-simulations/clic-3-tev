set name run10_new2_8_nochange_sign
exec mkdir -p $name
set script_dir [pwd]
cd $name

source $script_dir/survey.tcl

set atl_time 1e6

Random -type gaussian gcav

set nm 50
set n_slice 25
set n_part 9

#set nm 1
#set n_slice 3
#set n_part 1

set phase 8a

set frac_lambda 0.25

set n_total 500

set e_initial 9.0
set e0 $e_initial

set scale 1.0

source $script_dir/clic_basic_single.tcl
source $script_dir/clic_beam.tcl
set charge $match(charge)
#set match(e_spread) [expr 9.0/8.0*$match(e_spread)]

source $script_dir/lattice4a.tcl

Linac::choose_linac $script_dir/phase.def $phase
Linac::put_linac
TclCall -script {save_beam}
BeamlineSet -name test

proc change_sign {} {
    global match
    set ql0 [QuadrupoleGetStrengthList]
    set ql {}
    foreach x $ql0 {
	lappend ql [expr -1.0*$x]
    }
    QuadrupoleSetStrengthList $ql
    set tmp $match(beta_x)
    set match(beta_x) $match(beta_y)
    set match(beta_y) $tmp
    puts "$match(beta_x) $match(beta_y) [lindex $ql 0]"
}

WriteGirderLength -file girder.data -beginning_only 1

set cav0 [CavityGetPhaseList]
set grad0 [CavityGetGradientList]

#
# Generate beam
#

set match(n_total) $n_total

source $script_dir/clic_beam.tcl
set match(emitt_y) 0.1
#change_sign

make_beam_slice_energy_gradient beam0 $n_slice $n_part 1.0 1.0
make_beam_slice_energy_gradient beam0a 1 1 1.0 1.0
make_beam_slice_energy_gradient beam1 $n_slice $n_part 1.0 0.9
make_beam_slice_energy_gradient beam2a $n_slice $n_part 0.95 1.0
set match(sigma_z) 60.0
make_beam_slice_energy_gradient beam2 $n_slice $n_part 0.95 1.0
make_beam_slice_energy_gradient beam3 $n_slice $n_part 0.95 0.9
make_beam_slice_energy_gradient beam3a 1 1 0.95 0.9
make_beam_slice_energy_gradient beam4 $n_slice $n_part 0.95 0.8

set method dfs
set wgt 1000
set bin_iter 3
foreach {sname} {
    10_07_1
} {

#    load_survey_data $script_dir/survey.10_07_1 $nm

    foreach err {jitter01} {

	set mm $sname.$err
	set jitter 0.0
	set phase_err 0.0
	set gradient_err 0.0
	set bpm_scale 0.0
#	survey_data_reset
#	set survey_scale 1.0
#	set survey_scale 0.0
	switch $err {
	    straight {
		set bpm_res 0.0
	    }
	    cav {
		set bpm_res 0.0
		SurveyErrorSet -quadrupole_y 0.0 \
		    -bpm_y 0.0 \
		    -cavity_y [expr 20.0/sqrt(2.0)] \
		    -cavity_realign_y 0 \
		    -cavity_yp 0.0 \
		}
	    real {
		set bpm_res 0.0
		SurveyErrorSet -quadrupole_y 0.0 \
		    -bpm_y 0.0 \
		    -cavity_y 0.0 \
		    -cavity_realign_y [expr 5.0/sqrt(2.0)] \
		    -cavity_yp 0.0 \
		}
	    quad {
		set bpm_res 0.0
		SurveyErrorSet -quadrupole_y 20.0 \
		    -bpm_y 0.0 \
		    -cavity_y 0.0 \
		    -cavity_realign_y 0 \
		    -cavity_yp 0.0 \
		}
	    bpm {
		set bpm_res 0.0
		SurveyErrorSet -quadrupole_y 0.0 \
		    -bpm_y 200.0 \
		    -cavity_y 0.0 \
		    -cavity_realign_y 0 \
		    -cavity_yp 0.0 \
		}
	    res {
		set bpm_res 0.1
		SurveyErrorSet -quadrupole_y 0.0 \
		-bpm_y 0.0 \
		-cavity_y 0.0 \
		-cavity_realign_y 0 \
		-cavity_yp 0.0 \
	    }
	    tilt {
		set bpm_res 0.0
		SurveyErrorSet -quadrupole_y 0.0 \
		    -bpm_y 0.0 \
		    -cavity_y 0.0 \
		    -cavity_realign_y 0 \
		    -cavity_yp [expr 200.0/sqrt(2.0)]
	    }
	    all {
		set bpm_res 0.1
		SurveyErrorSet -quadrupole_y 20.0 \
		    -bpm_y 20.0 \
		    -cavity_y [expr 20.0/sqrt(2.0)] \
		    -cavity_realign_y [expr 5.0/sqrt(2.0)] \
		    -cavity_yp [expr 200.0/sqrt(2.0)]
	    }
	    everything {
		set bpm_res 0.1
		set jitter 0.1
		SurveyErrorSet -quadrupole_y 17.0 \
		    -bpm_y 14.0 \
                    -quadrupole_roll 100.0 \
		    -cavity_y [expr 10.0/sqrt(2.0)] \
		    -cavity_realign_y [expr 5.0/sqrt(2.0)] \
		    -cavity_yp [expr 200.0/sqrt(2.0)]
	    }
	    girder {
		set bpm_res 0.0
		set jitter 0.0
		SurveyErrorSet -quadrupole_y 0.0 \
		    -bpm_y 0.0 \
                    -quadrupole_roll 0.0 \
		    -cavity_y [expr 20.0/sqrt(2.0)] \
		    -cavity_realign_y 0.0 \
		    -cavity_yp 0.0
	    }
	    girder2 {
		set bpm_res 0.0
		set jitter 0.0
		SurveyErrorSet -quadrupole_y 0.0 \
		    -bpm_y 0.0 \
                    -quadrupole_roll 0.0 \
		    -cavity_y 0.0 \
		    -cavity_realign_y 0.0 \
		    -cavity_yp 0.0
	    }
	    girder3 {
		set bpm_res 0.0
		set jitter 0.0
		SurveyErrorSet -quadrupole_y 0.0 \
		    -bpm_y 0.0 \
                    -quadrupole_roll 0.0 \
		    -cavity_y 0.0 \
		    -cavity_realign_y 0.0 \
		    -cavity_yp 0.0
	    }
	    everything_scale {
		set bpm_res 0.1
		set jitter 0.1
		set bpm_scale 0.01
		SurveyErrorSet -quadrupole_y 17.0 \
		    -bpm_y 14.0 \
                    -quadrupole_roll 100 \
		    -cavity_y [expr 20.0/sqrt(2.0)] \
		    -cavity_realign_y [expr 5.0/sqrt(2.0)] \
		    -cavity_yp [expr 200.0/sqrt(2.0)]
	    }
	    everything_scale2 {
		set bpm_res 0.1
		set jitter 0.1
		set bpm_scale 0.02
		SurveyErrorSet -quadrupole_y 17.0 \
		    -bpm_y 14.0 \
                    -quadrupole_roll 100 \
		    -cavity_y [expr 20.0/sqrt(2.0)] \
		    -cavity_realign_y [expr 5.0/sqrt(2.0)] \
		    -cavity_yp [expr 200.0/sqrt(2.0)]
	    }
	    full {
		set bpm_res 0.1
		set jitter 0.1
		SurveyErrorSet -quadrupole_y 17.0 \
		    -quadrupole_roll 100.0 \
		    -bpm_y 14.0 \
		    -cavity_y [expr 7.0/sqrt(2.0)] \
		    -cavity_realign_y [expr 5.0/sqrt(2.0)] \
		    -cavity_yp [expr 200.0/sqrt(2.0)]
	    }
	    roll {
		set bpm_res 0.0
		set jitter 0.0
		SurveyErrorSet -quadrupole_y 0.0 \
		    -quadrupole_roll 100.0 \
		    -bpm_y 0.0 \
		    -cavity_y 0.0 \
		    -cavity_realign_y 0.0 \
		    -cavity_yp 0.0
	    }
	    tous_null {
		set bpm_res 0.1
		set jitter 0.1
		SurveyErrorSet -quadrupole_y 20.0 \
		    -bpm_y 20.0 \
		    -cavity_y [expr 20.0/sqrt(2.0)] \
		    -cavity_realign_y [expr 5.0/sqrt(2.0)] \
		    -cavity_yp [expr 200.0/sqrt(2.0)]
	    }
	    tous_phase {
		set bpm_res 0.1
		set jitter 0.1
		set phase_err 6.0
		SurveyErrorSet -quadrupole_y 20.0 \
		    -bpm_y 20.0 \
		    -cavity_y [expr 20.0/sqrt(2.0)] \
		    -cavity_realign_y [expr 5.0/sqrt(2.0)] \
		    -cavity_yp [expr 200.0/sqrt(2.0)]
	    }
	    tous_phase1 {
		set bpm_res 0.1
		set jitter 0.1
		set phase_err 1.0
		SurveyErrorSet -quadrupole_y 20.0 \
		    -bpm_y 20.0 \
		    -cavity_y [expr 20.0/sqrt(2.0)] \
		    -cavity_realign_y [expr 5.0/sqrt(2.0)] \
		    -cavity_yp [expr 200.0/sqrt(2.0)]
	    }
	    tous_phase2 {
		set bpm_res 0.1
		set jitter 0.1
		set phase_err 2.0
		SurveyErrorSet -quadrupole_y 20.0 \
		    -bpm_y 20.0 \
		    -cavity_y [expr 20.0/sqrt(2.0)] \
		    -cavity_realign_y [expr 5.0/sqrt(2.0)] \
		    -cavity_yp [expr 200.0/sqrt(2.0)]
	    }
	    tous_gradient {
		set bpm_res 0.1
		set jitter 0.1
		set gradient_err 0.002
		SurveyErrorSet -quadrupole_y 20.0 \
		    -bpm_y 20.0 \
		    -cavity_y [expr 20.0/sqrt(2.0)] \
		    -cavity_realign_y [expr 5.0/sqrt(2.0)] \
		    -cavity_yp [expr 200.0/sqrt(2.0)]
	    }
	    tous {
		set bpm_res 0.1
		set jitter 0.1
		set phase_err 6.0
		set gradient_err 0.002
		SurveyErrorSet -quadrupole_y 20.0 \
		    -bpm_y 20.0 \
		    -cavity_y [expr 20.0/sqrt(2.0)] \
		    -cavity_realign_y [expr 5.0/sqrt(2.0)] \
		    -cavity_yp [expr 200.0/sqrt(2.0)]
	    }
	    jitter {
		set bpm_res 0.0
		set jitter 1.0
		SurveyErrorSet -quadrupole_y 0.0 \
		    -bpm_y 0.0 \
		    -cavity_y 0.0 \
		    -cavity_realign_y 0 \
		    -cavity_yp 0.0
	    }
	    jitter01 {
		set bpm_res 0.0
		set jitter 0.1
		SurveyErrorSet -quadrupole_y 0.0 \
		    -bpm_y 0.0 \
		    -cavity_y 0.0 \
		    -cavity_realign_y 0 \
		    -cavity_yp 0.0
	    }
	    jitter2 {
		set bpm_res 0.0
		set jitter -1.0
		SurveyErrorSet -quadrupole_y 0.0 \
		    -bpm_y 0.0 \
		    -cavity_y 0.0 \
		    -cavity_realign_y 0 \
		    -cavity_yp 0.0
	    }
	    default {
		puts "I do not know misalignment method $err"
		exit
	    }
	}
	
	proc my_survey {} {
	    global i err
	    global cav0 grad0
	    global phase_err gradient_err bpm_scale

	    Clic
	    if {$err=="full"} {
		InterGirderMove -scatter_y 12.0 -flo_y 5.0
	    }
	    if {$err=="everything"} {
		InterGirderMove -scatter_y 12.0 -flo_y 5.0
	    }
	    if {$err=="girder"} {
		InterGirderMove -scatter_y 12.0 -flo_y 5.0
	    }
	    if {$err=="girder2"} {
		InterGirderMove -scatter_y 12.0 -flo_y 0.0
	    }
	    if {$err=="girder3"} {
		InterGirderMove -scatter_y 0.0 -flo_y 5.0
	    }

#	    apply_survey_data_switch girder.data

	    set cav {}
	    set grad {}
	    set fcav [open cav.dat.$i w]
	    foreach c $cav0 g $grad0 {
		set cc [expr $c+[gcav]*$phase_err]
		set gg [expr $g+[gcav]*$gradient_err]
		puts $fcav "$gg $cc"
		lappend cav $cc
		lappend grad $gg
	    }
	    close $fcav
	    CavitySetPhaseList $cav
	    CavitySetGradientList $grad
	    SaveAllPositions -vertical_only 0 -binary 0 -cav 1 -nodrift 1 -file buc.$i
	    puts "misalignment done"
	}

	proc save_beam {} {
	    global i
	    if {[EmittanceRun]} {
		SaveAllPositions -vertical_only 0 -binary 1 -file b121.$i -cav 1 -nodrift 1
		incr i
	    }
	}

	set i 0
	Zero
	CavitySetPhaseList $cav0
	CavitySetGradientList $grad0
	TestSimpleCorrection -beam beam0 -emitt_file $mm.121 \
	    -survey my_survey -machines $nm

	proc save_beam {} {
	    global i
	    if {[EmittanceRun]} {
		BpmRealign
		SaveAllPositions -vertical_only 0 -binary 1 -file bl.$i -cav 1 -nodrift 1
		incr i
	    }
	}
	
	proc my_survey {} {
	    global i
	    global cav0 bpm_scale
	    ReadAllPositions -file b121.$i -vertical_only 0 -binary 1 -cav 1 -nodrift 1

#	    return

	    SetBpmScaleError -sigma_y $bpm_scale
	    set fcav [open cav.dat.$i r]
	    set cav ""
	    set grad ""
	    foreach c $cav0 {
		gets $fcav cc
		lappend cav [lindex $cc 1]
		lappend grad [lindex $cc 0]
	    }
	    close $fcav
	    CavitySetPhaseList $cav
	    CavitySetGradientList $grad
	}
	
	set i 0
	switch $method {
	    nlc {
		TestSimpleAlignment -beam beam0 -emitt_file $mm \
		    -survey Nlc -jitter_y $jitter \
		    -machines $nm -position_wgt 0.01 -binlength 36 \
		    -binoverlap 18 -bpm_res $bpm_res
	    }
	    ball {
		TestBallisticCorrection -beam beam0 -emitt_file $mm \
		    -survey Nlc -jitter_y $jitter \
		    -machines $nm -binlength 12 -bpm_res $bpm_res
	    }
	    dfs {
		set mm $mm.$wgt
		Zero
		CavitySetPhaseList $cav0
		CavitySetGradientList $grad0
		TestMeasuredCorrection \
		    -beam0 beam0 -beam1 beam3 -cbeam0 beam0a -cbeam1 beam3a\
		    -jitter_y [expr 1.0*$jitter] \
		    -emitt_file $mm.$err -survey my_survey -machines $nm \
		    -wgt1 $wgt -bin_iteration $bin_iter -correct_full_bin 1 \
		    -binlength 36 -binoverlap 18 -bpm_res $bpm_res
	    }
	    dfs2 {
		set mm $method.$wgt
		Zero
		CavitySetPhaseList $cav0
		CavitySetGradientList $grad0
		set qtmp0 [QuadrupoleGetStrengthList]
		set qtmp1 ""
		foreach qq $qtmp0 {
		    lappend qtmp1 $qq
		}
		TestMeasuredCorrection \
		    -beam0 beam0 -beam1 beam0 \
		    -jitter_y [expr 1.0*$jitter] \
		    -quad0 $qtmp0 -quad1 -$qtmp1 \
		    -emitt_file $mm.$err -survey my_survey -machines $nm \
		    -wgt1 10000 -bin_iteration $bin_iter -correct_full_bin 1 \
		    -binlength 36 -binoverlap 18 -bpm_res $bpm_res
	    }
	    default {
		puts "I do not know correction method $method"
		exit
	    }
	}
	
	proc save_beam {} {
	    global i
	    if {[EmittanceRun]} {
		SaveAllPositions -file bx.$i -vertical_only 0 -binary 0 -cav 1 -nodrift 1
		BeamSaveAll -file beam.$i
		incr i
	    }
	}
	
	proc my_survey {} {
	    global i
	    global cav0
	    ReadAllPositions -file bl.$i -vertical_only 0 -binary 1 -cav 1 -nodrift 1

	    set fcav [open cav.dat.$i r]
	    set cav ""
	    set grad ""
	    foreach c $cav0 {
		gets $fcav cc
		lappend cav [lindex $cc 1]
		lappend grad [lindex $cc 0]
	    }
	    close $fcav
	    CavitySetPhaseList $cav
	    CavitySetGradientList $grad
	}
	
	Zero

	CavitySetPhaseList $cav0
	CavitySetGradientList $grad0
	set i 0
	TestRfAlignment -beam beam0 -emitt_file $mm.rf -machines $nm \
	    -survey my_survey -testbeam beam0
	
    }
}
