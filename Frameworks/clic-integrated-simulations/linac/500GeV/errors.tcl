switch $err {
    straight {
	set bpm_res 0.0
    }
    cav {
	set bpm_res 0.0
	SurveyErrorSet -quadrupole_y 0.0 \
	    -bpm_y 0.0 \
	    -cavity_y [expr 20.0/sqrt(2.0)] \
	    -cavity_realign_y 0 \
	    -cavity_yp 0.0 \
	}
    real {
	set bpm_res 0.0
	SurveyErrorSet -quadrupole_y 0.0 \
	    -bpm_y 0.0 \
	    -cavity_y 0.0 \
	    -cavity_realign_y [expr 5.0/sqrt(2.0)] \
	    -cavity_yp 0.0 \
	}
    quad {
	set bpm_res 0.0
	SurveyErrorSet -quadrupole_y 20.0 \
	    -bpm_y 0.0 \
	    -cavity_y 0.0 \
	    -cavity_realign_y 0 \
	    -cavity_yp 0.0 \
	}
    bpm {
	set bpm_res 0.0
	SurveyErrorSet -quadrupole_y 0.0 \
	    -bpm_y 14.0 \
	    -cavity_y 0.0 \
	    -cavity_realign_y 0 \
	    -cavity_yp 0.0 \
	}
    res {
	set bpm_res 0.1
	SurveyErrorSet -quadrupole_y 0.0 \
	    -bpm_y 0.0 \
	    -cavity_y 0.0 \
	    -cavity_realign_y 0 \
	    -cavity_yp 0.0 \
	}
    tilt {
	set bpm_res 0.0
	SurveyErrorSet -quadrupole_y 0.0 \
	    -bpm_y 0.0 \
	    -cavity_y 0.0 \
	    -cavity_realign_y 0 \
	    -cavity_yp [expr 200.0/sqrt(2.0)]
    }
    all {
	set bpm_res 0.1
	SurveyErrorSet -quadrupole_y 20.0 \
	    -bpm_y 20.0 \
	    -cavity_y [expr 20.0/sqrt(2.0)] \
	    -cavity_realign_y [expr 5.0/sqrt(2.0)] \
	    -cavity_yp [expr 200.0/sqrt(2.0)]
    }
    everything {
	set bpm_res 0.1
	set jitter 0.1
	SurveyErrorSet -quadrupole_y 17.0 \
	    -bpm_y 14.0 \
	    -quadrupole_roll 100.0 \
	    -cavity_y [expr 10.0/sqrt(2.0)] \
	    -cavity_realign_y [expr 5.0/sqrt(2.0)] \
	    -cavity_yp [expr 200.0/sqrt(2.0)]
    }
    girder {
	set bpm_res 0.0
	set jitter 0.0
	SurveyErrorSet -quadrupole_y 0.0 \
	    -bpm_y 0.0 \
	    -quadrupole_roll 0.0 \
	    -cavity_y [expr 20.0/sqrt(2.0)] \
	    -cavity_realign_y 0.0 \
	    -cavity_yp 0.0
    }
    girder2 {
	set bpm_res 0.0
	set jitter 0.0
	SurveyErrorSet -quadrupole_y 0.0 \
	    -bpm_y 0.0 \
	    -quadrupole_roll 0.0 \
	    -cavity_y 0.0 \
	    -cavity_realign_y 0.0 \
	    -cavity_yp 0.0
    }
    girder3 {
	set bpm_res 0.0
	set jitter 0.0
	SurveyErrorSet -quadrupole_y 0.0 \
	    -bpm_y 0.0 \
	    -quadrupole_roll 0.0 \
	    -cavity_y 0.0 \
	    -cavity_realign_y 0.0 \
	    -cavity_yp 0.0
    }
    everything_scale {
	set bpm_res 0.1
	set jitter 0.1
	set bpm_scale 0.01
	SurveyErrorSet -quadrupole_y 17.0 \
	    -bpm_y 14.0 \
	    -quadrupole_roll 100 \
	    -cavity_y [expr 20.0/sqrt(2.0)] \
	    -cavity_realign_y [expr 5.0/sqrt(2.0)] \
	    -cavity_yp [expr 200.0/sqrt(2.0)]
    }
    everything_scale2 {
	set bpm_res 0.1
	set jitter 0.1
	set bpm_scale 0.02
	SurveyErrorSet -quadrupole_y 17.0 \
	    -bpm_y 14.0 \
	    -quadrupole_roll 100 \
	    -cavity_y [expr 20.0/sqrt(2.0)] \
	    -cavity_realign_y [expr 5.0/sqrt(2.0)] \
	    -cavity_yp [expr 200.0/sqrt(2.0)]
    }
    full {
	set bpm_res 0.1
	set jitter 0.1
	SurveyErrorSet -quadrupole_y 17.0 \
	    -quadrupole_roll 100.0 \
	    -bpm_y 14.0 \
	    -cavity_y [expr 7.0/sqrt(2.0)] \
	    -cavity_realign_y [expr 5.0/sqrt(2.0)] \
	    -cavity_yp [expr 200.0/sqrt(2.0)]
    }
    roll {
	set bpm_res 0.0
	set jitter 0.0
	SurveyErrorSet -quadrupole_y 0.0 \
	    -quadrupole_roll 100.0 \
	    -bpm_y 0.0 \
	    -cavity_y 0.0 \
	    -cavity_realign_y 0.0 \
	    -cavity_yp 0.0
    }
    tous_null {
	set bpm_res 0.1
	set jitter 0.1
	SurveyErrorSet -quadrupole_y 20.0 \
	    -bpm_y 20.0 \
	    -cavity_y [expr 20.0/sqrt(2.0)] \
	    -cavity_realign_y [expr 5.0/sqrt(2.0)] \
	    -cavity_yp [expr 200.0/sqrt(2.0)]
    }
    tous_phase {
	set bpm_res 0.1
	set jitter 0.1
	set phase_err 6.0
	SurveyErrorSet -quadrupole_y 20.0 \
	    -bpm_y 20.0 \
	    -cavity_y [expr 20.0/sqrt(2.0)] \
	    -cavity_realign_y [expr 5.0/sqrt(2.0)] \
	    -cavity_yp [expr 200.0/sqrt(2.0)]
    }
    tous_phase1 {
	set bpm_res 0.1
	set jitter 0.1
	set phase_err 1.0
	SurveyErrorSet -quadrupole_y 20.0 \
	    -bpm_y 20.0 \
	    -cavity_y [expr 20.0/sqrt(2.0)] \
	    -cavity_realign_y [expr 5.0/sqrt(2.0)] \
	    -cavity_yp [expr 200.0/sqrt(2.0)]
    }
    tous_phase2 {
	set bpm_res 0.1
	set jitter 0.1
	set phase_err 2.0
	SurveyErrorSet -quadrupole_y 20.0 \
	    -bpm_y 20.0 \
	    -cavity_y [expr 20.0/sqrt(2.0)] \
	    -cavity_realign_y [expr 5.0/sqrt(2.0)] \
	    -cavity_yp [expr 200.0/sqrt(2.0)]
    }
    tous_gradient {
	set bpm_res 0.1
	set jitter 0.1
	set gradient_err 0.002
	SurveyErrorSet -quadrupole_y 20.0 \
	    -bpm_y 20.0 \
	    -cavity_y [expr 20.0/sqrt(2.0)] \
	    -cavity_realign_y [expr 5.0/sqrt(2.0)] \
	    -cavity_yp [expr 200.0/sqrt(2.0)]
    }
    tous {
	set bpm_res 0.1
	set jitter 0.1
	set phase_err 6.0
	set gradient_err 0.002
	SurveyErrorSet -quadrupole_y 20.0 \
	    -bpm_y 20.0 \
	    -cavity_y [expr 20.0/sqrt(2.0)] \
	    -cavity_realign_y [expr 5.0/sqrt(2.0)] \
	    -cavity_yp [expr 200.0/sqrt(2.0)]
    }
    jitter {
	set bpm_res 0.0
	set jitter 1.0
	SurveyErrorSet -quadrupole_y 0.0 \
	    -bpm_y 0.0 \
	    -cavity_y 0.0 \
	    -cavity_realign_y 0 \
	    -cavity_yp 0.0
    }
    jitter01 {
	set bpm_res 0.0
	set jitter 0.1
	SurveyErrorSet -quadrupole_y 0.0 \
	    -bpm_y 0.0 \
	    -cavity_y 0.0 \
	    -cavity_realign_y 0 \
	    -cavity_yp 0.0
    }
    jitter2 {
	set bpm_res 0.0
	set jitter -1.0
	SurveyErrorSet -quadrupole_y 0.0 \
	    -bpm_y 0.0 \
	    -cavity_y 0.0 \
	    -cavity_realign_y 0 \
	    -cavity_yp 0.0
    }
    default {
	puts "I do not know misalignment method $err"
	exit
    }
}
