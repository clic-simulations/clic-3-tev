set ch {1.0}

#
# Choice of Structure
#

array set structure {a 3.625e-3 g 8.5417e-3 l 10.417e-3 delta 0.095 delta_g 0.205e-3}

#
# Choice of beam parameters
#

array set match {
    emitt_x 20.0
    emitt_y 0.1
    e_spread 2.0
    charge 6.8e9
    sigma_z 72
    phase 0.0
}

#
# transverse wakefield
# s is given in micro metres
# return value is in V/pCm^2
#

proc w_transv {s} {
    global structure
    set l $structure(l)
    set a $structure(a)

    set g $structure(g)
    set s0 [expr 0.169*pow($a,1.79)*pow($g,0.38)*pow($l,-1.17)]
    set tmp [expr 4.0*377.0*3e8*$s0*1e-12/\
		 (acos(-1.0)*pow($a,4))\
		 *(1.0-(1.0+sqrt($s*1e-6/$s0))*exp(-sqrt($s*1e-6/$s0)))]

    set g [expr $structure(g)+0.5*$structure(delta_g)]
    set s0 [expr 0.169*pow($a*(1.0+0.5*$structure(delta)),1.79)*pow($g,0.38)*pow($l,-1.17)]
    set tmp [expr $tmp+4.0*377.0*3e8*$s0*1e-12/\
		 (acos(-1.0)*pow($a*(1.0+0.5*$structure(delta)),4))\
		 *(1.0-(1.0+sqrt($s*1e-6/$s0))*exp(-sqrt($s*1e-6/$s0)))]

    set g [expr $structure(g)-0.5*$structure(delta_g)]
    set s0 [expr 0.169*pow($a*(1.0-0.5*$structure(delta)),1.79)*pow($g,0.38)*pow($l,-1.17)]
    set tmp [expr $tmp+4.0*377.0*3e8*$s0*1e-12/\
		 (acos(-1.0)*pow($a*(1.0-0.5*$structure(delta)),4))\
		 *(1.0-(1.0+sqrt($s*1e-6/$s0))*exp(-sqrt($s*1e-6/$s0)))]

    set g [expr $structure(g)-$structure(delta_g)]
    set s0 [expr 0.169*pow($a*(1.0-$structure(delta)),1.79)*pow($g,0.38)*pow($l,-1.17)]
    set tmp [expr $tmp+0.5*4.0*377.0*3e8*$s0*1e-12/\
		 (acos(-1.0)*pow($a*(1.0-$structure(delta)),4))\
		 *(1.0-(1.0+sqrt($s*1e-6/$s0))*exp(-sqrt($s*1e-6/$s0)))]

    set g [expr $structure(g)+$structure(delta_g)]
    set s0 [expr 0.169*pow($a*(1.0+$structure(delta)),1.79)*pow($g,0.38)*pow($l,-1.17)]
    set tmp [expr $tmp+0.5*4.0*377.0*3e8*$s0*1e-12/\
		 (acos(-1.0)*pow($a*(1.0+$structure(delta)),4))\
		 *(1.0-(1.0+sqrt($s*1e-6/$s0))*exp(-sqrt($s*1e-6/$s0)))]

    set tmp [expr 0.25*$tmp]

    return $tmp
}

#
# longitudinal wakefield
# s is given in micro metres
# return value is in V/pCm
#

proc w_long {s} {
    global structure
    set l $structure(l)

    set a $structure(a)
    set g $structure(g)
    set s0 [expr 0.41*pow($a,1.8)*pow($g,1.6)*pow($l,-2.4)]
    set tmp [expr 377.0*3e8*1e-12/(acos(-1.0)*$a*$a)*exp(-sqrt($s*1e-6/$s0))]

    set a [expr $structure(a)*(1.0-0.5*$structure(delta))]
    set g [expr $structure(g)-0.5*$structure(delta_g)]
    set s0 [expr 0.41*pow($a,1.8)*pow($g,1.6)*pow($l,-2.4)]
    set tmp [expr $tmp+377.0*3e8*1e-12/(acos(-1.0)*$a*$a)*exp(-sqrt($s*1e-6/$s0))]

    set a [expr $structure(a)*(1.0+0.5*$structure(delta))]
    set g [expr $structure(g)+0.5*$structure(delta_g)]
    set s0 [expr 0.41*pow($a,1.8)*pow($g,1.6)*pow($l,-2.4)]
    set tmp [expr $tmp+377.0*3e8*1e-12/(acos(-1.0)*$a*$a)*exp(-sqrt($s*1e-6/$s0))]

    set a [expr $structure(a)*(1.0-$structure(delta))]
    set g [expr $structure(g)-$structure(delta_g)]
    set s0 [expr 0.41*pow($a,1.8)*pow($g,1.6)*pow($l,-2.4)]
    set tmp [expr $tmp+0.5*377.0*3e8*1e-12/(acos(-1.0)*$a*$a)*exp(-sqrt($s*1e-6/$s0))]

    set a [expr $structure(a)*(1.0+$structure(delta))]
    set g [expr $structure(g)+$structure(delta_g)]
    set s0 [expr 0.41*pow($a,1.8)*pow($g,1.6)*pow($l,-2.4)]
    set tmp [expr $tmp+0.5*377.0*3e8*1e-12/(acos(-1.0)*$a*$a)*exp(-sqrt($s*1e-6/$s0))]

    return [expr $tmp/4.0]
}

#
# Routine to prepare file for wakefields
# it creates a file with name f_name that can be read when a beam is created
# z_list is of longitudinal position and weight of the slices
#

proc calc_new_transv {charge z_list} {
    set tmp {}
#
# unit to store in the file is MV/pCm^2
#

#
# multiply charge with factor for pC and MV
#
    set z_l {}
    foreach l $z_list {
	lappend z_l $l
	set z0 [lindex $l 0]
	foreach j $z_l {
	    set z [lindex $j 0]
	    lappend tmp [expr 1.6e-7*1e-6*$charge*[w_transv [expr $z0-$z]]]
	}
    }
    return $tmp
}

proc calc_new_long {charge z_list} {
    set z_l {}
    set n [llength $z_list]
    set tmp {}
    foreach l $z_list {
	set z0 [lindex $l 0]
	set wgt0 [expr $charge*[lindex $l 1]]
	set sum 0.0
	foreach j $z_l {
	    set z [lindex $j 0]
	    set wgt [expr [lindex $j 1]*$charge]
	    set sum [expr $sum+$wgt*[w_long [expr $z0-$z]]]
	}
	set sum [expr $sum+0.5*$wgt0*[w_long 0.0]]
	lappend z_l $l
#
# multiply with factor for pC and MV
#
	set sum [expr -($sum*1.6e-7*1e-6)]
	lappend tmp "$z0 $sum"
    }
    return $tmp
}

proc calc {f_name charge a b sigma n} {
    set file [open $f_name w]
    puts $file $n
    set long [calc_new_long $charge [GaussList -min $a -max $b -sigma $sigma -charge 1.0 -n_slices [expr 5*$n]]]
    set z_list [GaussList -min $a -max $b -sigma $sigma -charge 1.0 -n_slices [expr $n]]
    set transv [calc_new_transv $charge $z_list]
    set x [lindex $long [expr int($n/2)*5+2]]
    puts $file [lindex $x 1]
    for {set i 0} {$i<$n} {incr i} {
	set x [lindex $long [expr 2+5*$i]]
	puts $file "$x [lindex [lindex $z_list $i] 1]"
    }
    foreach x $transv {
	puts $file $x
    }
    close $file
}

#
# Define the beams
#

source $script_dir/make_beam.tcl
