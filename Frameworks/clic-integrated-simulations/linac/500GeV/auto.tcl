set name run4
exec mkdir -p $name
set script_dir [pwd]
cd $name

set n_slice 21
set n_part 1

set phase [lindex $argv 0]

set frac_lambda 0.25

set n_total 500

set e_initial 9.0
set e0 $e_initial

set scale 1.0

source $script_dir/clic_basic_single.tcl
source $script_dir/clic_beam.tcl
set charge $match(charge)

source $script_dir/lattice4b.tcl

Linac::log_start module.list
Linac::choose_linac $script_dir/phase_real.def $phase
Linac::put_linac
Linac::log_stop
TclCall -script {BeamSaveAll -file beam.save}

BeamlineSet -name test

#Octave {

#QI = placet_get_number_list("test", "cavity")

#}
#exit
#
# Generate beam
#

set match(n_total) $n_total

set match(emitt_y) 0.1

set match(emitt_x) 24.0

make_beam_slice_energy_gradient beam0 $n_slice $n_part 1.0 1.0

#puts [llength [CavityGetPhaseList]]
#exit

EnergySpreadPlot -beam beam0 -file e0.dat
TwissPlot -beam beam0 -file twiss.dat
#BeamSetToOffset -beam beam0 -y 1.0
TestNoCorrection -beam beam0 -emitt_file auto.$phase -survey Zero
set emitt_axis 1
TestNoCorrection -beam beam0 -emitt_file auto_x.$phase -survey Zero

puts [llength [CavityNumberList]]
