set f [open run4/module.list r]
array set m {
0 0
1 0
2 0
3 0
4 0
0n 1
1n 1
2n 1
3n 1
4n 1
}
array set c {
0 8
1 6
2 4
3 2
4 0
0n 0
1n 0
2n 0
3n 0
4n 0
}

array set nm {0 474 1 418 2 418 3 413 4 1000}
set sum 0
for {set i 0} {$i<5} {incr i} {
    set nsum($i) 0
    set ng 0
    for {set j 0} {$j<$nm($i)} {incr j} {
	gets $f line
	if {[eof $f]} {
	    close $f
	    break
	}
	incr ng
	set nsum($i) [expr $nsum($i)+$c($line)]
    }
    set sum [expr $sum+$nsum($i)]
    puts "$ng $sum $nsum($i)"
}
