set script_dir /afs/cern.ch/user/d/dalena/public/clic-integrated-simulations/linac/barbara  
# global variables
array set params {
    ch 6.8e9
    emitt_x 24.0
    emitt_y 0.1
    offset 0.0
    waist_y 0.0
    linerr everything
    wgt 1000.0
}

array set params $argv
exec mkdir -p dfs_linac_500GeV_emix_$params(emitt_x)_emiy_$params(emitt_y)_wgt_$params(wgt)_linerr_$params(linerr)
cd dfs_linac_500GeV_emix_$params(emitt_x)_emiy_$params(emitt_y)_wgt_$params(wgt)_linerr_$params(linerr)
#
# General
#
Random -type gaussian gcav

# number of machine to simulate
set nm 100 

set n_slice 25
set n_part 15

set frac_lambda 0.25

set n_total 150000
set n [ expr $n_total/$n_slice ]
set scale 1.0


source $script_dir/clic_basic_single.tcl
source $script_dir/clic_beam.tcl


set match(n_total) $n_total

set match(emitt_y) $params(emitt_y)

#set match(sigma_z) 44.0
puts "$match(sigma_z)"
set match(charge) $params(ch)

set charge $match(charge)

set match(emitt_x) $params(emitt_x)

#set match(e_spread) -1.0

puts "charge $match(charge)"
puts "emitt_y $match(emitt_y)"

set off $params(offset)
set waist $params(waist_y)

#####################################################
# Linacs beamlines
#

source $script_dir/lattice4b.tcl

foreach name { linelectron  linpositron } {
    puts "Putting Linac"
    global n_total n_slice n_part
    BeamlineNew
    set phase 8a
    set e_initial 9.0
    set e0 $e_initial
    Linac::choose_linac $script_dir/phase_real.def $phase
    Linac::put_linac
    TclCall -script "save_beam"
    BeamlineSet -name $name
    # this is needed in order to overwrite the sliced beam info
}
set cav0 [CavityGetPhaseList]
set grad0 [CavityGetGradientList]
###########################################################
# Beam definitions
#
#set n_bunches 312
set n_bunches 1
if [ expr $n_bunches > 1 ] {
 foreach beamname { electron  positron } {
    #    Generate beam: multibunch 
    global n_slice n_part n_bunches
    puts "Generating the beam"
    puts "n. slices $n_slice "
    puts "n. particles $n_part "
    #    make_beam_slice_energy_gradient beam_${beamname} $n_slice $n_part 1.0 1.0
    make_beam_slice_energy_gradient_train beam_${beamname} $n_slice $n_part 1.0 1.0 $n_bunches
 }
 foreach beamname { electron1  positron1 } {
    #    Generate beam
    global n_slice n_part
    puts "Generating the beam"
    puts "n. slices $n_slice "
    puts "n. particles $n_part "
    #    make_beam_slice_energy_gradient beam_${beamname} $n_slice $n_part 1.0 1.0
    make_beam_slice_energy_gradient beam_${beamname} $n_slice $n_part 1.0 1.0 
 }
} else {
 foreach beamname { electron  positron } {
    #    Generate beam: single bunch
    global n_slice n_part n_bunches
    puts "Generating the beam"
    puts "n. slices $n_slice "
    puts "n. particles $n_part "
    make_beam_slice_energy_gradient beam_${beamname} $n_slice $n_part 1.0 1.0
 }
}
# test beams
make_beam_slice_energy_gradient beam0a 1 1 1.0 1.0
#set match(sigma_z) 60.0
make_beam_slice_energy_gradient beam3 $n_slice $n_part 0.95 0.9
make_beam_slice_energy_gradient beam3a 1 1 0.95 0.9
#####################################################################
# Misalignments and tracking
# 
# choose and set the errors
#set err jitter01
set err $params(linerr)
source $script_dir/errors.tcl
set mm my.$err
set bpm_scale 0.0
set jitter 0.0
set phase_err 0.0
set gradient_err 0.0
# select the method
set method dfs
#set wgt 1000
set wgt $params(wgt)
set bin_iter 3

#set atl_time 1e6
foreach line {electron } {
# initial misalignment    
    proc my_survey {} {
	global j err line
	global cav0 grad0
	global phase_err gradient_err bpm_scale
	Clic
	if {$err=="full"} {
	    InterGirderMove -scatter_y 12.0 -flo_y 5.0
	}
	if {$err=="everything"} {
	    InterGirderMove -scatter_y 12.0 -flo_y 5.0
	}
	if {$err=="girder"} {
	    InterGirderMove -scatter_y 12.0 -flo_y 5.0
	}
	if {$err=="girder2"} {
	    InterGirderMove -scatter_y 12.0 -flo_y 0.0
	}
	if {$err=="girder3"} {
	    InterGirderMove -scatter_y 0.0 -flo_y 5.0
	}
	#	    apply_survey_data_switch girder.data    
	set cav {}
	set grad {}
	#set fcav [open cav.$line.dat.$j w]
	foreach c $cav0 g $grad0 {
	    set cc [expr $c+[gcav]*$phase_err]
	    set gg [expr $g+[gcav]*$gradient_err]
	    #puts $fcav "$gg $cc"
	    lappend cav $cc
	    lappend grad $gg
	}
	#close $fcav
	CavitySetPhaseList $cav
	CavitySetGradientList $grad
	SaveAllPositions -vertical_only 0 -cav_grad_phas 1 -binary 0 -cav_bpm 1 -nodrift 1 -file $line.buc.$j
	puts "misalignment done"
    }
# 1-to-1 correction
    proc save_beam { } {
	global j line
       if {[EmittanceRun]} {
	SaveAllPositions -vertical_only 0 -cav_grad_phas 1 -binary 0 -file $line.b121.$j -cav_bpm 1 -nodrift 1
	incr j
       }
    }
    set j 0
    Zero
    CavitySetPhaseList $cav0
    CavitySetGradientList $grad0
    puts "BEGIN LINACs $line"
    BeamlineUse -name lin$line  
    TestSimpleCorrection -beam beam_$line -emitt_file $line.$mm.121 \
	-survey my_survey -machines $nm
# dfs correction
    proc my_survey {} {
	global j line
	global cav0 bpm_scale
	ReadAllPositions -file $line.b121.$j -cav_grad_phas 1 -vertical_only 0 -binary 0 -cav_bpm 1 -nodrift 1
	SetBpmScaleError -sigma_y $bpm_scale
    }
    proc save_beam { } {
	global j line
       if {[EmittanceRun]} {
	BpmRealign
	SaveAllPositions -vertical_only 0 -cav_grad_phas 1 -binary 0 -file $line.bl.$j -cav_bpm 1 -nodrift 1
	incr j
       }
    }
    set j 0
    switch $method {
	nlc {
	    TestSimpleAlignment -beam beam_$line -emitt_file $line.$mm \
		-survey Nlc -jitter_y $jitter \
		-machines $nm -position_wgt 0.01 -binlength 36 \
		-binoverlap 18 -bpm_res $bpm_res
	}
	ball {
	    TestBallisticCorrection -beam beam_$line -emitt_file $mm \
		-survey Nlc -jitter_y $jitter \
		-machines $nm -binlength 12 -bpm_res $bpm_res
	}
	dfs {
	    set mm $mm.$wgt
	    Zero
	    CavitySetPhaseList $cav0
	    CavitySetGradientList $grad0
	    TestMeasuredCorrection \
		-beam0 beam_$line -beam1 beam3 -cbeam0 beam0a -cbeam1 beam3a\
		-jitter_y [expr 1.0*$jitter] \
		-emitt_file $line.$mm.$err -survey my_survey -machines $nm \
		-wgt1 $wgt -bin_iteration $bin_iter -correct_full_bin 1 \
		-binlength 36 -binoverlap 18 -bpm_res $bpm_res
	}
	dfs2 {
	    set mm $method.$wgt
	    Zero
	    CavitySetPhaseList $cav0
	    CavitySetGradientList $grad0
	    set qtmp0 [QuadrupoleGetStrengthList]
	    set qtmp1 ""
	    foreach qq $qtmp0 {
		lappend qtmp1 $qq
	    }
	    TestMeasuredCorrection \
		-beam0 beam_$line -beam1 beam_$line \
		-jitter_y [expr 1.0*$jitter] \
		-quad0 $qtmp0 -quad1 -$qtmp1 \
		-emitt_file $line.$mm.$err -survey my_survey -machines $nm \
		-wgt1 10000 -bin_iteration $bin_iter -correct_full_bin 1 \
		-binlength 36 -binoverlap 18 -bpm_res $bpm_res
	}
	default {
	    puts "I do not know correction method $method"
	    exit
	}
    }
# Rf align    
    proc my_survey {} {
	global j line
	global cav0
	ReadAllPositions -file $line.bl.$j -cav_grad_phas 1 -vertical_only 0 -binary 0 -cav_bpm 1 -nodrift 1	
    }
    proc save_beam { } {
	global j line
       if {[EmittanceRun]} {
	SaveAllPositions -vertical_only 0 -cav_grad_phas 1 -binary 0 -file $line.bx.$j -cav_bpm 1 -nodrift 1
#	BeamSaveAll -file linac_beam$line.$j -bunches 1 -axis 1
	BeamSaveAll -file linac_beam$line.$j 
	BeamDump -file beam$line.$j -seed 0 -type 1 
	incr j
       } 
    }

    set j 0
    Zero
    CavitySetPhaseList $cav0
    CavitySetGradientList $grad0
    TestRfAlignment -beam beam_$line -emitt_file $line.$mm.rf -machines $nm \
	-survey my_survey -testbeam beam_$line

}
puts " END of LINACs "

exit


