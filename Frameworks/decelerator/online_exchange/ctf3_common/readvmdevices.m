function readvalues = readvmdevices(devdescr)


n = length(devdescr);

fprintf(1,'There is %d devices to Read \n',n);

vmdb = importdata('virtual.currents',' ',0);
vmndevs = length(vmdb.textdata);

for i=1:n
  devicename = [devdescr(i).prefix '.' devdescr(i).name];

  n=0;
  for j=1:vmndevs
    vmname = char(vmdb.textdata(j));
    if (strcmpi(devicename,vmname))
      n = j;
      break
    end
  end

  if (n == 0)
    error('Can not find device %s in the list of devices. Java Control Error.\n',devicename);
    readvalues(i) = 1/n;
  end;

  readvalues(i) = vmdb.data(n);
  fprintf(1,'%s = %f in DB at n=%d\n', devicename, readvalues(i), n ); 
  
end



