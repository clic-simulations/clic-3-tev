% returns the phase-advance for a periodic transfer map [rad]
function [mu_x_per, mu_y_per] = placet_calc_phase_advance(M)

mu_x_per = acos(trace(M(1:2,1:2))/2);
mu_y_per = acos(trace(M(3:4,3:4))/2);

return;

