# manual loop-script for "standard mode test" : amplification of jitter on individual frequencies
#   in order to use: 
# param1: mode #
# param2: Q-scaling   (set Q_t_scaling_factor to $param2)
# ext_param1: w-scaling, 1e-20: wakes off   (set w_t_scaling_factor to $ext_param)
#   (to reproduce thesis results, n_bunches should be set to 200 as well)
set g_placet_bin /Users/eadli/work/myPlacetDev/placet-development/bin/placet-octave

# clear and date output file
set cmd "date"
exec bash << $cmd > placet_output.txt

set n_modes_loop 9

for {set loop_modes 0} {$loop_modes < $n_modes_loop} {incr loop_modes} {
set cmd "$g_placet_bin ../dec_scripts/main.tcl .. $loop_modes $loop_modes 1 1 1e-20 .."
puts $cmd
exec bash << $cmd >>& placet_output.txt 
set cmd "$g_placet_bin ../dec_scripts/main.tcl .. $loop_modes $loop_modes 1 2 1 .."
puts $cmd
exec bash << $cmd >>& placet_output.txt 
set cmd "$g_placet_bin ../dec_scripts/main.tcl .. $loop_modes $loop_modes 2 3 1 .."
puts $cmd
exec bash << $cmd >>& placet_output.txt 
set cmd "$g_placet_bin ../dec_scripts/main.tcl .. $loop_modes $loop_modes 3 4 1 .."
puts $cmd
exec bash << $cmd >>& placet_output.txt 
}

#
exit


#
# Octave plotting routines for standard Q scaling graph
#
#qpole_sist = [0:1.005:(1.005*1049)];
plot(env_NC_max(:,2) ./ (env_NC_max(:,1))  , '-xr;Q=Q_0;', "markersize", 15);
hold on;
plot(env_NC_max(:,3) ./ (env_NC_max(:,1))  , '-ob;Q=2 x Q_0;', "markersize", 15);
plot(env_NC_max(:,4) ./ (env_NC_max(:,1))  , '-xm;Q=3 x Q_0;', "markersize", 15);
%hold off;
%axis([1 6 1 2]);
xlabel('Mode number [-]');  ylabel('Jitter amplification [-]');
grid on;
