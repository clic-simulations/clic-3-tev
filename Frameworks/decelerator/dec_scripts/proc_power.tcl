# calculate form factor
proc form_factor { slice_dist   lambda_l } {
    global SI_c
    global SI_e
    global SI_pi

    set form_factor 0
    foreach slice $slice_dist {
	set pos [lindex $slice 0]/1e6
	set weight [lindex $slice 1]
#	puts "pow is: $pos"
#	puts "weight is: $weight"
	set form_factor [expr cos( 2*$SI_pi * $pos / $lambda_l )*$weight + $form_factor]
    }
    return $form_factor
}
	
   


# calculate steady-state Power at the end of the structure in Time-Domain
# TODO: implement second harmonic
proc power_td { beta_l   cavitylength  n_harmonic   lambda_l   R_Q   bunchlength   charge   distance    ignore_damp} {
    global SI_c
    global SI_e
    global SI_pi
    # extra cavity input parameters
    if { $ignore_damp } {
	set Q 1e10 
    } else {
	set Q 7210
    }
    set N_cell 39

    # conversion to SI
    set bunchlength [expr $bunchlength/1e6]
    # conversion to LINAC units
    set R_Q [expr $R_Q*2]

    # cell length
    set l_cell [expr $cavitylength / $N_cell]


    # bunch frequency
    set omega_bunch [expr 2*$SI_pi*($SI_c / $distance) ]

    # set power extraction frequency [rad/s] (equal to bunch frequency if 1st harmonic)
    set omega_ext [expr $omega_bunch * $n_harmonic]

    # TODO!!: implement second harmonic


    # resonance frequency (inc. detuning)
    set omega_c [expr 2*$SI_pi*($SI_c / $lambda_l) ] 
    
    # Form factor
    set F_sigma [expr exp(-(2*pow($SI_pi,2)*pow($bunchlength,2))/(pow($distance,2)))]
    
    # Current
    set I [expr $charge*$SI_e*$SI_c / $distance]

    # Power produced in single cell
    set P_cell [expr $R_Q * pow($I,2) * pow($l_cell,2) * pow($F_sigma,2) * $omega_c / (4*$beta_l*$SI_c)]

    # Field profile at the end of the structure
    set Et 0
    for {set k 0} {$k < $N_cell} {incr k } {
      set damp_factor 0
      set det_factor 0
      # calc of damp and detuning factor.  NB: in our case the sum simply sums the cell lengths to the total remaining length6
      for {set jj $k} {$jj < $N_cell} {incr jj } { 
	  set damp_factor [expr $damp_factor -2*$omega_ext*$l_cell / (2*$Q*$beta_l*$SI_c)]
          set det_factor [expr $det_factor + $l_cell*(1-$beta_l) / ($beta_l*$SI_c)]
      }
	set Et [expr $Et + pow(($P_cell*exp($damp_factor) ),0.5) * pow(($R_Q*$omega_ext) / ($beta_l*$SI_c),0.5) * cos( (($omega_c - $omega_ext)/2) * $det_factor)]
    }

 # Steady-state Power at the end of the structure
set P_end [expr pow($Et,2) * pow(($R_Q * $omega_ext/($beta_l*$SI_c)),-1)]

return $P_end
}

