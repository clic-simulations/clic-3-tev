proc my_calc_betas_all_particles {} {
#
# calculates beta functions along the lattice for all particles in beam
# - used e.g. for wake amplification calculations
# - NB: current version of script will only work for TBL due to n_Q_before_quad    
global loop_param1_n loop_param2_n param1_n param2_n run mysimname e0 n_pets_installed twiss_match

Octave {
    Q_cb = placet_get_number_list("$mysimname", "quadrupole");
    Qe0_nominal_cb = placet_element_get_attribute("$mysimname", Q_cb, "e0");
    n_Q_before_dec = 5+1; % the +1 is due to two-half matching quads 
    beam = placet_get_beam();
    E = placet_get_name_number_list("$mysimname", "*");
    n_E = size(E,2);
    n_mp = length(beam)

% loop over all macroparticles       
 for n_cb=1:n_mp,
e_mp = beam(n_cb, 3);
de_mp = ($e0-e_mp) / $n_pets_installed;

% loop over all PETS
for(m_cb=1:$n_pets_installed),
  % adjust e0 for this and all trailing quadrupoles
  placet_element_set_attribute("$mysimname", Q((n_Q_before_dec+m_cb):end), "e0", $e0-de_mp*m_cb);
end% for

% calculate betas for this macroparticle
[s, beta_x, beta_y, alpha_x, alpha_y, mu_x, mu_y] = placet_evolve_beta_function($twiss_match(beta_x), $twiss_match(alpha_x), $twiss_match(beta_y), $twiss_match(alpha_y), 0, n_E-1);
beta_arr(n_cb, :, :) = [s; beta_x'; beta_y'; alpha_x'; alpha_y'];
beta_inits(n_cb, :) = [beta_x(1); beta_y(1); alpha_x(1); alpha_y(1)];
beta_finals(n_cb, :) = [beta_x(end); beta_y(end); alpha_x(end); alpha_y(end)];
 end% for
 
% save betas for all macroparticles
% save also a smaller file with only init and final betas for all particles 
save -text particles_beta.$loop_param1_n.$loop_param2_n.$param1_n.$param2_n.$run beta_arr
      save -text particles_beta_light.$loop_param1_n.$loop_param2_n.$param1_n.$param2_n.$run beta_inits  beta_finals

% reset quad strengths      
placet_element_set_attribute("$mysimname", Q_cb, "e0", Qe0_nominal_cb); 
}
}
