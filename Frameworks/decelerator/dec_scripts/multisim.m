#
# main placet-octave multi-purpose simulation script, EA
#

Octave {
    disp('Starting multisim');
    rand("seed", 42)

    % if TBL do matching (temp)
    if ( strcmp("$mysimname", "tbl12"))
       %source "$deceleratorrootpath/dec_scripts/def_matching.m"
    end% if

Dy_temp_NC = 0;
Dy_tot2 = 0;
y0_tot2 = 0;
x0_tot2 = 0;
y1_tot2 = 0;
Dy_tot2_SC = 0;
y0_tot2_SC = 0;
x0_tot2_SC = 0;
y1_tot2_SC = 0;
Dy_tot2_DFS = 0;
y0_tot2_DFS = 0;
x0_tot2_DFS = 0;
y1_tot2_DFS = 0;
y0_NC = 0;
y0_SC = 0;
y0_DFS = 0;
x0_NC = 0;
x0_SC = 0;
x0_DFS = 0;
env2_NC = 0;
env2_SC = 0;
env2_DFS = 0;
emitty_NC = 0;
emitty_SC = 0;
emitty_DFS = 0;
emittx_NC = 0;
emittx_SC = 0;
emittx_DFS = 0;
losses_NC = 0;
losses_SC = 0;
losses_DFS = 0;
BEAM0_NC = 0;    
BEAM2_NC = 0;    
BEAM2_SC = 0;    
BEAM2_DFS = 0;    
dp_p = 0;
Qy_max_after_NC = 0; 
Qy_rms_after_NC = 0;
Qy_max_after_SC = 0;
Qy_rms_after_SC = 0;
Qy_max_after_DFS = 0;
Qy_rms_after_DFS = 0;
Qy_after_NC = 0;
Qy_after_SC = 0;
Qy_after_DFS = 0;
Dsy_after_NC = 0;
Dsy_after_SC = 0;
Dsy_after_DFS = 0;


#
# GLOBAL OPTIONS, MAGNET SCALING, OR TEST BEAM
#
dfs_scale_magnets = 0; # perform dfs with magnet scaling (instead of testbeam) - NB: NOT IMPLEMENTED IN X
dp_p_dfs = 0.2;
quad_dfs_scaling = 1/(1+dp_p_dfs);

disp_scale_magnets = 1; # calc disp with magnet scaling (instead of testbeam)
dp_p = ($energyfraction2-100) / 100;
quad_disp_scaling = 1/(1-dp_p);

dipole_correctors = str2num(Tcl_GetVar("dipole_correctors"));
    
nm=$n_machines;

    
    
#
# Retrieve initial values of data
#
Q = placet_get_number_list("$mysimname", "quadrupole");
D = placet_get_number_list("$mysimname", "dipole");
# remove initial and final observation quads
# Q = Q(2:(end-2));
#placet_element_get_attribute("$mysimname", Q(56:58), "e0")
#placet_element_set_attribute("$mysimname", Q(2), "y", 20);
#placet_element_get_attribute("$mysimname", Q(57), "s")
B = placet_get_number_list_real_bpm("$mysimname");

# do not include final observation half-quads + BPM in correction (beacause last BPM is 0 meters after last quad, in order to save beam in middle of quad)
elements_corr_cut = 1;    
Q = Q(1:(end-elements_corr_cut));
B = B(1:(end-elements_corr_cut));

Qstrength_nominal = placet_element_get_attribute("$mysimname", Q, "strength");
Qs = placet_element_get_attribute("$mysimname", Q, "s");
Bs = placet_element_get_attribute("$mysimname", B, "s");

%TEMP
%plot(abs(Qstrength_nominal));
%pause;
    
%
% SUBSET OF BPM / Q's used for correction
%
disp(['  QB_BBA_interval: ' num2str($QB_BBA_interval)])
disp(['  Q_BBA_interval: ' num2str($Q_BBA_interval)])
disp(['  B_BBA_interval: ' num2str($B_BBA_interval)])
disp(['  BBA_interval_start2: ' num2str($BBA_interval_start2)])
disp(['  B_BBA_interval_missing: ' num2str($B_BBA_interval_missing)])
if( $QB_BBA_interval == 1 )
  if( $BBA_interval_start2 == 1)
      n_B_selected = [1 2:${B_BBA_interval}:length(B)];
      n_Q_selected = [1 2:${Q_BBA_interval}:length(Q)];
  else
      n_B_selected = [1:${B_BBA_interval}:length(B)];
      n_Q_selected = [1:${Q_BBA_interval}:length(Q)];
  end;
else
  if( $BBA_interval_start2 == 1)
     n_B_selected = [1 2:${QB_BBA_interval}:length(B)];
     n_Q_selected = [1 2:${QB_BBA_interval}:length(Q)];
  else
     n_B_selected = [1:${QB_BBA_interval}:length(B)];
     n_Q_selected = [1:${QB_BBA_interval}:length(Q)];
  end;
end

% new batch of sub-set BPMs
%   every n'th BPM missing (here: keep correctors)
if( $B_BBA_interval_missing != 1 )
  n_B_selected = [];
 for n=1:length(B),
   if ( mod(n, $B_BBA_interval_missing) != 0 )
     n_B_selected = [n_B_selected   n];
   end%if
 end% for
end% if
%   grouping, n present, then n missing
if( $B_BBA_grouping != 1 )
  B_BBA = B;
  n_B_selected = [];
  for n=2:length(B),
    if ( mod(round(n/$B_BBA_grouping),2) == 1 ),
      n_B_selected = [n_B_selected   n];
    end% if
  end% for
end% if

% select Qs for BBA
Q_BBA = Q(n_Q_selected);
% select Qs for BBA
B_BBA = B(n_B_selected);

if ( strcmp("$mysimname", "tbl12"))
  % for TBL: quad on movers are defined by their names
  Q_BBA = placet_get_name_number_list("$mysimname", "CB.Q*R*");
end% if


% select correctors
if( dipole_correctors )
  C = D;
  C_variable_x = "strength_x";
  C_variable_y = "strength_y";
else
  C = Q_BBA;
  C_variable_x = "x";
  C_variable_y = "y";
end% if

%
disp(['Total number of BPMs in line: ' num2str(length(B_BBA))]);
disp(['Total number of correctors in line: ' num2str(length(C))]);




%
%  SET QUAD STRINGING
%    (N quadrupoles of the same family on the same string)
if( $use_stepped_gradient_strings ),
n_quad_stringing = $quad_per_string;
  Qstrength_stringed = Qstrength_nominal;
  n = 2; % skip first quad, special case
  while(n < ((length(Q)-1-n_quad_stringing*2))*($lattice_string_cut)),
    for(m=2:n_quad_stringing),
      Qstrength_stringed(n+(m-1)*2) = Qstrength_nominal(n); 
      Qstrength_stringed(n+1+(m-1)*2) = Qstrength_nominal(n+1);
    end% for
    n = n + n_quad_stringing*2;
  end% while
  %plot((Qstrength_stringed-Qstrength_nominal)./Qstrength_nominal);
  %pause;
  Qstrength_nominal = Qstrength_stringed;
  placet_element_set_attribute("$mysimname", Q, "strength", Qstrength_nominal);
end% n_quad_stringing





%
% load or recalc response matrix
%
R0_loaded = 0;
R1_loaded = 0;
R0_x_loaded = 0;
R1_x_loaded = 0;
placet_element_vary_attribute("$mysimname", 0, "y", 0); % workaround for for dir-bug
if( ~$do_multisim_load_mach )
 # R0_y  (NB: it is called R0 for "backward compability"
if( $track_1to1_correction || $track_DFS_correction || $saveresponsematrix_active ),    
        if( exist("R0") )
      disp('Using already calculated R0');
    else
    if ( $do_multisim_load_save_R )
      if (exist("$deceleratorrootpath/precalc/responsematrices/R0_${mysimname}_${Rsuffix}.dat", "file")) 
        disp('Loading previously calculated R0...');
        load $deceleratorrootpath/precalc/responsematrices/R0_${mysimname}_${Rsuffix}.dat;
        R0_loaded = 1;
      else
        disp('Recalculating R0...');
        R0 = placet_get_response_matrix_attribute("$mysimname", "$beamname1", B_BBA, "y", C, C_variable_y, "None");
        save -text R0_${mysimname}_${Rsuffix}.dat R0
      end% if
    else
      disp('Recalculating R0...');
       R0 = placet_get_response_matrix_attribute("$mysimname", "$beamname1", B_BBA, "y", C, C_variable_y, "None");
    end% if
  end% if
end% if
# R1_y
if( $track_DFS_correction ),    
    if( exist("R1") )
      disp('Using already calculated R1');
    else
    if ( $do_multisim_load_save_R )
      if (exist("$deceleratorrootpath/precalc/responsematrices/R1_${mysimname}_${Rsuffix}.dat", "file")) 
        disp('Loading previously calculated R1...');
        load $deceleratorrootpath/precalc/responsematrices/R1_${mysimname}_${Rsuffix}.dat;
        R1_loaded = 1;
      else
        disp('Recalculating R1...');
            if( dfs_scale_magnets )
              k_Q = placet_element_get_attribute("$mysimname", Q, "strength");
              placet_element_set_attribute("$mysimname", Q, "strength", k_Q*quad_dfs_scaling);
              if( length(D)>0 ),
                a_y = placet_element_get_attribute("$mysimname", D, "strength_y");
                placet_element_set_attribute("$mysimname", D, "strength_y", a_y*quad_dfs_scaling);
              end% if
              if( dipole_correctors)
                # if we use dipole correctors R_dp_p must be scaled because of the way placet calc's the responsel
                R1 = placet_get_response_matrix_attribute("$mysimname", "$beamname1", B_BBA, "y", C, C_variable_y, "None")*quad_dfs_scaling - R0;
              else
                R1 = placet_get_response_matrix_attribute("$mysimname", "$beamname1", B_BBA, "y", C, C_variable_y, "None") - R0;
              end
              placet_element_set_attribute("$mysimname", Q, "strength", k_Q);
              if( length(D)>0 ),
                placet_element_set_attribute("$mysimname", D, "strength_y", a_y);
              end% if
            else
              R1 = placet_get_response_matrix_attribute("$mysimname", "$testbeam1", B_BBA, "y", C, C_variable_y, "None") - R0;
        end% if
        save -text R1_${mysimname}_${Rsuffix}.dat R1
      end% if
    else
      disp('Recalculating R1...');
            if( dfs_scale_magnets )
              k_Q = placet_element_get_attribute("$mysimname", Q, "strength");
              placet_element_set_attribute("$mysimname", Q, "strength", k_Q*quad_dfs_scaling);
              if( length(D)>0 ),
                a_y = placet_element_get_attribute("$mysimname", D, "strength_y");
                placet_element_set_attribute("$mysimname", D, "strength_y", a_y*quad_dfs_scaling);
              end% if
              # NB: need to scale R1 because of how placet response is calculated
              if( dipole_correctors)
                # if we use dipole correctors R_dp_p must be scaled because of the way placet calc's the responsel
                R1 = placet_get_response_matrix_attribute("$mysimname", "$beamname1", B_BBA, "y", C, C_variable_y, "None")*quad_dfs_scaling - R0;
              else
                R1 = placet_get_response_matrix_attribute("$mysimname", "$beamname1", B_BBA, "y", C, C_variable_y, "None") - R0;
              end
              placet_element_set_attribute("$mysimname", Q, "strength", k_Q);
              if( length(D)>0 ),
                placet_element_set_attribute("$mysimname", D, "strength_y", a_y);
              end% if
            else
              R1 = placet_get_response_matrix_attribute("$mysimname", "$testbeam1", B_BBA, "y", C, C_variable_y, "None") - R0;
        end% if
    end% if
    end% if
end% if
if( $do_x_alignment )
# R0_x
if( $track_1to1_correction || $track_DFS_correction || $saveresponsematrix_active ),    
        if( exist("R0_x") )
      disp('Using already calculated R0_x');
    else
    if ( $do_multisim_load_save_R )
      if (exist("$deceleratorrootpath/precalc/responsematrices/R0_x_${mysimname}_${Rsuffix}.dat", "file")) 
        disp('Loading previously calculated R0_x...');
        load $deceleratorrootpath/precalc/responsematrices/R0_x_${mysimname}_${Rsuffix}.dat;
        R0_x_loaded = 1;
      else
        disp('Recalculating R0_x...');
         R0_x = placet_get_response_matrix_attribute("$mysimname", "$beamname1", B_BBA, "x", C,  C_variable_x, "None");
       save -text R0_x_${mysimname}_${Rsuffix}.dat R0_x
      end% if
    else
      disp('Recalculating R0_x...');
         R0_x = placet_get_response_matrix_attribute("$mysimname", "$beamname1", B_BBA, "x", C,  C_variable_x, "None");
  end% if
  end% if
end% if
# R1_x
if( $track_DFS_correction ),    
    if( exist("R1_x") )
      disp('Using already calculated R1_x');
    else
    if ( $do_multisim_load_save_R )
      if (exist("$deceleratorrootpath/precalc/responsematrices/R1_x_${mysimname}_${Rsuffix}.dat", "file")) 
        disp('Loading previously calculated R1_x...');
        load $deceleratorrootpath/precalc/responsematrices/R1_x_${mysimname}_${Rsuffix}.dat;
        R1_x_loaded = 1;
      else
        disp('Recalculating R1_x...');
        R1_x = placet_get_response_matrix_attribute("$mysimname", "$testbeam1", B_BBA, "x", C,  C_variable_x, "None") - R0_x;
        save -text R1_x_${mysimname}_${Rsuffix}.dat R1_x
      end% if
    else
      disp('Recalculating R1_x...');
      R1_x = placet_get_response_matrix_attribute("$mysimname", "$testbeam1", B_BBA, "x", C,  C_variable_x, "None") - R0_x;
    end% if
    end% if
end% if
end% if  
    
% select R row and columns for BBA
if( R0_loaded )
  R0 = R0(n_B_selected, n_Q_selected);
end% if
if( R1_loaded )
  R1 = R1(n_B_selected, n_Q_selected);
end% if
if( R0_x_loaded )
  R0_x = R0_x(n_B_selected, n_Q_selected);
end% if
if( R1_x_loaded )
  R1_x = R1_x(n_B_selected, n_Q_selected);
end% if

end% if( ~$do_multisim_load_mach )


    % take corrector positions before correction
    By_before = placet_element_get_attribute("$mysimname", B, "y" );

envmax_NC = zeros(length(Q)+elements_corr_cut, 1);
envmax_SC = zeros(length(Q)+elements_corr_cut, 1);
envmax_DFS = zeros(length(Q)+elements_corr_cut, 1);
envmax_DFS_testbeam = zeros(length(Q)+elements_corr_cut, 1);

envmaxmach_NC = 0;    
envmaxmach_SC = 0;    
envmaxmach_DFS = 0;    
    
placet_element_set_attribute("$mysimname", B, "resolution", $bpm_resolution);


%
% calculating correction bins
%

n_corr = length(C);
n_corrperbin = $corr_per_bin;
n_overlap = $bin_overlap;
n_bins = ceil(n_corr / (n_corrperbin - n_overlap));
for n=1:n_bins,
  bin_start(n) = 1 + (n-1)*(n_corrperbin - n_overlap);
  bin_end(n) = n*(n_corrperbin - n_overlap);
end
bin_end(n_bins) = length(C);

%
%
%  MAIN LOOP
%

    for i=1:nm,
 
%
%  SET QUAD FAILURE
%    Important: when sim. one plane fail only F quads (start of string)
n_failure_quad = str2num(Tcl_GetVar("n_failure_quad"));
Q_failed_2 = sort(genrandlist(floor((length(Q)-1)/2), n_failure_quad));
Q_failed = sort(Q_failed_2 * 2)+1; % ensure failed quad is F
if( length(Q_failed) > 0 )
  series_fault =  str2num(Tcl_GetVar("quadfail_in_series"));
  % FF and DD pair: set series_step 2,  if not, set 1 
  series_step=2;
  %Q_failed_pad = [Q_failed
  %ones(1,length(series_fault*series_step))];
  if (series_fault > 1),
    Q_failed_strings = [];
    for n=1:(series_fault-1),
      Q_failed_strings = [Q_failed_strings (Q_failed+n*series_step)]; % somewhat sloppy for serie fault
      series_fault -= 1;
    end% for
    Q_failed = [Q_failed Q_failed_strings];
  end% if
  % get rid of Q_failed that are out-of-range after stringing
  Q_failed = sort(Q_failed);
  [val,ind] = find(Q_failed > (length(Q)-1));
  ind = min(ind)-1;
  if(ind > 0),
    Q_failed = Q_failed(1:ind)
  end% if
end% if
Q_failed

%  SET BPM FAILURE
n_failure_BPM_percent = str2num(Tcl_GetVar("n_failure_BPM_percent"))
BPM_failed = sort(genrandlist(floor((length(B_BBA))), n_failure_BPM_percent*length(B_BBA) ));
% n_failure_BPM = str2num(Tcl_GetVar("n_failure_BPM"));  % before absolute number
% BPM_failed = sort(genrandlist(floor((length(B_BBA))), n_failure_BPM))


%  SET QUAD STRENGTH JITTER
scale_Q = 1+randn(length(Q),1)*$quad_strength_jitter;
Q_strength_jitter_rms = $quad_strength_jitter

%  SET QUAD Y JITTER
jitter_Qy = randn(length(Q),1)*$quad_y_jitter;
Q_y_jitter_rms = $quad_y_jitter

% SET PETS INHIBITION
P = placet_get_number_list("$mysimname", "cavity_pets");
if( length(P) > 0 )
n_failure_PETS = str2num(Tcl_GetVar("n_failure_PETS"));
P_failed = sort(genrandlist(length(P), n_failure_PETS))
PETS_nominal = placet_element_get_attribute("$mysimname", P, "e0"); % we (ab)use the parameter "e0" to induce PETS failure
end% if


% loop to arbitrary random machine
n_rand_mach = 1 %  set to arbitrary number for a different random machine

% Apply initial misalignments to the machine (survey) 
%    if not disabled by user
if( ~($skip_inital_survey) )
% Survey resets the dipoles, so they must be stored and set back
% after the survey
disp("Performing initial machine survey");
if( length(D) > 0),
    Dsy_init = placet_element_get_attribute("$mysimname", D, "strength_y")
  end% if
  for (i_rand=1:n_rand_mach)
    Tcl_Eval("my_survey");
  end% for
  if( length(D) > 0),
    placet_element_set_attribute("$mysimname", D, "strength_y", Dsy_init);
  end% if
end% if

    #
    # DO NC
    #
    
if( $track_no_correction )
    machinename = sprintf("$deceleratorrootpath/precalc/machines/M_${mysimname}_${Rsuffix}_${QB_BBA_interval}_${BBA_interval_start2}_NC_%s.dat", num2str(i) );
    if ( exist(machinename, "file") && $do_multisim_load_mach  ) 
      machinename
       disp('Load NC machine');
          load(machinename);
          placet_element_set_attribute("$mysimname", Q, "x", Qx);
          placet_element_set_attribute("$mysimname", Q, "y", Qy);
else      
  disp('OCT NC - using surveyed machine');
end% if
    
Qy_nominal = placet_element_get_attribute("$mysimname", Q, "y");
if( length(P(P_failed)) > 0 ), placet_element_set_attribute("$mysimname", P(P_failed), "e0", -1.0); end; %  drive wedges PETS
placet_element_set_attribute("$mysimname", Q, "y", Qy_nominal.+jitter_Qy); % y jitter
placet_element_set_attribute("$mysimname", Q, "strength", Qstrength_nominal.*scale_Q); % strength jitter
if( length(Q(Q_failed)) > 0 ), placet_element_set_attribute("$mysimname", Q(Q_failed), "strength", Qstrength_nominal(Q_failed)*$quad_fail_current_reduction); end;
placet_element_set_attribute("$mysimname", B, "resolution", 0);
[E0, BEAM] = placet_test_no_correction("$mysimname", "$beamname1", "None", 1);  % final beam in centre quad
y0 = placet_element_get_attribute("$mysimname", B, "reading_y") + placet_element_get_attribute("$mysimname", B, "y");
x0 = placet_element_get_attribute("$mysimname", B, "reading_x") + placet_element_get_attribute("$mysimname", B, "x");
losses_NC += placet_element_get_attribute("$mysimname", Q, "aperture_losses");
if ( $do_calc_disp )
placet_element_set_attribute("$mysimname", B, "resolution", 0);
   if( disp_scale_magnets )
     k_Q = placet_element_get_attribute("$mysimname", Q, "strength");
     placet_element_set_attribute("$mysimname", Q, "strength", k_Q*quad_disp_scaling);
     if( length(D)>0 ),
          a_y = placet_element_get_attribute("$mysimname", D, "strength_y");
          placet_element_set_attribute("$mysimname", D, "strength_y", a_y*quad_disp_scaling);
     end% if
     E1 = placet_test_no_correction("$mysimname", "$beamname1", "None", 1);
     placet_element_set_attribute("$mysimname", Q, "strength", k_Q);
     if( length(D)>0 ),
       placet_element_set_attribute("$mysimname", D, "strength_y", a_y);
     end% if
     else
     E1 = placet_test_no_correction("$mysimname", "$testbeamE2", "None", 1);
   end% if
   y1 = placet_element_get_attribute("$mysimname", B, "reading_y") + placet_element_get_attribute("$mysimname", B, "y");
   Dy = (y0-y1) / dp_p;
   y1_tot2 += y1.^2;
   Dy_temp_NC += Dy;
   Dy_tot2 += Dy.^2;
placet_element_set_attribute("$mysimname", B, "resolution", $bpm_resolution);
end% if
placet_element_set_attribute("$mysimname", B, "resolution", $bpm_resolution);
placet_element_set_attribute("$mysimname", Q, "strength", Qstrength_nominal ); % restore quads
placet_element_set_attribute("$mysimname", Q, "y", Qy_nominal ); % restore quads
if( length(P) > 0 ), placet_element_set_attribute("$mysimname", P, "e0", PETS_nominal); end; %  restore PETS
    y0_NC += y0;
    x0_NC += x0;
    y0_tot2 += y0.^2;
    x0_tot2 += x0.^2;
    env2_NC += (E0(1:(end-elements_corr_cut),8)'.^2);
    envmax_NC(:,i) =  E0(1:(end-elements_corr_cut),8); 
    envmaxmach_NC(i) = max(E0(1:(end-elements_corr_cut),8));
    emitty_NC += (E0(1:(end-elements_corr_cut),6)');
    emittx_NC += (E0(1:(end-elements_corr_cut),2)');
    if( $savebeam_multisim_active )
       BEAM2_NC += BEAM.^2;
       BEAM0_NC += BEAM;
    end% if
Qy_after_NC = placet_element_get_attribute("$mysimname", Q, "y" );
Qy_max_after_NC = max( max(abs(Qy_after_NC)), Qy_max_after_NC );
Qy_rms_after_NC +=std( Qy_after_NC );
if( length(D)>0 ); Dsy_after_NC = placet_element_get_attribute("$mysimname", D, "strength_y" ); end; 
end% if

Breading_y_after_NC = placet_element_get_attribute("$mysimname", B, "reading_y" );

if ( $do_multisim_save_mach  ) 
      disp('Save NC machine');
      Qx = placet_element_get_attribute("$mysimname", Q, "x");
      Qy = placet_element_get_attribute("$mysimname", Q, "y");
      save(machinename, 'Qx', 'Qy');
   end% if




    #
    # DO SIMPLE CORRECTION
    #
    #Q_a0_type = placet_element_get_attribute("$mysimname", Q, "aperture_shape");
    Q_a0x = placet_element_get_attribute("$mysimname", Q, "aperture_x");
    Q_a0y = placet_element_get_attribute("$mysimname", Q, "aperture_y");
    if( $track_1to1_correction )
    machinename = sprintf("$deceleratorrootpath/precalc/machines/M_${mysimname}_${Rsuffix}_${QB_BBA_interval}_${BBA_interval_start2}_SC_%s.dat", num2str(i) );
    if ( exist(machinename, "file") && $do_multisim_load_mach   ) 
      machinename
       disp('Load SC machine');
          load(machinename);
          placet_element_set_attribute("$mysimname", Q, "x", Qx);
          placet_element_set_attribute("$mysimname", Q, "y", Qy);
    else
    disp(['OCT SC - correcting surveyed machine, in steps of '  num2str(n_bins) ' bins.']);
    tB = B_BBA;
    tC = C;
    placet_element_set_attribute("$mysimname", Q, "aperture_x", Q_a0x*1e6);
    placet_element_set_attribute("$mysimname", Q, "aperture_y", Q_a0y*1e6);
    placet_test_no_correction("$mysimname", "$beamname1", "None", 1, 0, tB(end));
    placet_element_set_attribute("$mysimname", Q, "aperture_x", Q_a0x);
    placet_element_set_attribute("$mysimname", Q, "aperture_y", Q_a0y);
if( $do_x_alignment )
  disp("WARNING: BPM failure not implemented for x motion.  All BPMs are correct.");
  if( n_bins > 1)
    disp("WARNING: binning not implemented for x motion.  Correcting in a single bin.");
  end% if
    # x correction
    tb0_x = placet_element_get_attribute("$mysimname", tB, "reading_x")';
    tR_pinv_x = calc_pinv(R0_x, $svd_cut_SC);
    tc_x = -tR_pinv_x * tb0_x;
    if($corrector_step_size != 0), tc_x = round(tc_x / $corrector_step_size) * $corrector_step_size; end% % corr steps
    placet_element_vary_attribute("$mysimname", C, C_variable_x, tc_x);
end% if
  # y correction
 n_iters = 1;
%placet_element_set_attribute("$mysimname", Q, "strength", Qstrength_nominal.*scale_Q); % TEMP, perturb machine for correction
 for( n_iter=1:n_iters ),
   for n_bin=1:n_bins,
     if( n_bins > 1 )
       tB_bin = tB( bin_start(n_bin):bin_end(n_bin) );
       R0_bin = R0(bin_start(n_bin):bin_end(n_bin), bin_start(n_bin):bin_end(n_bin)); % NB: this only works for square R0
       C_bin = C( bin_start(n_bin):bin_end(n_bin) );
     else
       tB_bin = tB;
       R0_bin = R0;
       C_bin = C;
     end% if
     placet_test_no_correction("$mysimname", "$beamname1", "None", 1, 0, tB_bin(end)); # re-calc in case disp has been calc (not optim for speed)
    tb0 = placet_element_get_attribute("$mysimname", tB_bin, "reading_y")';
    # BPM failure: take out signal and row in R (assume known fail)
    if( n_failure_BPM_percent > 0)
      tb0(BPM_failed) = [];
      R0_bin(BPM_failed, :) = [];
    end% if
    tR_pinv_y = calc_pinv(R0_bin, $svd_cut_SC);
    tc = -tR_pinv_y * tb0;
    if($corrector_step_size != 0), tc = round(tc / $corrector_step_size) * $corrector_step_size; end; % corr steps
    placet_element_vary_attribute("$mysimname", C_bin, C_variable_y, tc);
    end% for bins
    %temp = placet_test_no_correction("$mysimname", "$beamname1", "None", 1, 0, tB(end)); % TEMP
    %temp_y0_after_it = placet_element_get_attribute("$mysimname", tB, "reading_y"); % TEMP
 end% for
    %envmax_it_SC(i) =  max(abs(temp_y0_after_it));  % TEMP
    %save -text it.dat envmax_it_SC; % TEMP 
    disp('Finishing OCTSC current machine routine');
%placet_element_set_attribute("$mysimname", Q, "strength", Qstrength_nominal ); % TEMP restore machine after correction
end% if

if ( $do_multisim_save_mach  ) 
      disp('Save SC machine');
      Qx = placet_element_get_attribute("$mysimname", Q, "x");
      Qy = placet_element_get_attribute("$mysimname", Q, "y");
      save(machinename, 'Qx', 'Qy');
   end% if
 

Qy_nominal = placet_element_get_attribute("$mysimname", Q, "y");
if( length(P) > 0 ), placet_element_set_attribute("$mysimname", P(P_failed), "e0", -1.0); end; %  drive wedges PETS
placet_element_set_attribute("$mysimname", Q, "y", Qy_nominal.+jitter_Qy); % y jitter
placet_element_set_attribute("$mysimname", Q, "strength", Qstrength_nominal.*scale_Q); % strength jitter
placet_element_set_attribute("$mysimname", Q(Q_failed), "strength", Qstrength_nominal(Q_failed)*$quad_fail_current_reduction);
placet_element_set_attribute("$mysimname", B, "resolution", 0);
[E0, BEAM] = placet_test_no_correction("$mysimname", "$beamname1", "None", 1);  % final beam in centre quad
y0 = placet_element_get_attribute("$mysimname", B, "reading_y") + placet_element_get_attribute("$mysimname", B, "y");
x0 = placet_element_get_attribute("$mysimname", B, "reading_x") + placet_element_get_attribute("$mysimname", B, "x");
losses_SC += placet_element_get_attribute("$mysimname", Q, "aperture_losses");
if ( $do_calc_disp )
placet_element_set_attribute("$mysimname", B, "resolution", 0);
   if( disp_scale_magnets )
     k_Q = placet_element_get_attribute("$mysimname", Q, "strength");
     placet_element_set_attribute("$mysimname", Q, "strength", k_Q*quad_disp_scaling);
     if( length(D)>0 ),
       a_y = placet_element_get_attribute("$mysimname", D, "strength_y");
       placet_element_set_attribute("$mysimname", D, "strength_y", a_y*quad_disp_scaling);
     end% if
     E1 = placet_test_no_correction("$mysimname", "$beamname1", "None", 1); % final beam in centre quad
     placet_element_set_attribute("$mysimname", Q, "strength", k_Q);
     if( length(D)>0 ),
       placet_element_set_attribute("$mysimname", D, "strength_y", a_y);
     end% if
   else
     E1 = placet_test_no_correction("$mysimname", "$testbeamE2", "None", 1); % final beam in centre quad
   end% if
   y1 = placet_element_get_attribute("$mysimname", B, "reading_y") + placet_element_get_attribute("$mysimname", B, "y");
   Dy = (y0-y1) / dp_p;
   y1_tot2_SC += y1.^2;
   Dy_tot2_SC += Dy.^2;
placet_element_set_attribute("$mysimname", B, "resolution", $bpm_resolution);
end% if
placet_element_set_attribute("$mysimname", B, "resolution", $bpm_resolution);
placet_element_set_attribute("$mysimname", Q, "strength", Qstrength_nominal );
placet_element_set_attribute("$mysimname", Q, "y", Qy_nominal ); % restore quads
if( length(P) > 0 ), placet_element_set_attribute("$mysimname", P, "e0", PETS_nominal); end; %  restore PETS

    y0_SC += y0;
    x0_SC += x0;
    y0_tot2_SC += y0.^2;
    x0_tot2_SC += x0.^2;
    env2_SC += (E0(1:(end-elements_corr_cut),8)'.^2);
    envmax_SC(:,i) =  E0(1:(end-elements_corr_cut),8);
    envmaxmach_SC(i) = max(E0(1:(end-elements_corr_cut),8));
    emitty_SC += (E0(1:(end-elements_corr_cut),6)');
    emittx_SC += (E0(1:(end-elements_corr_cut),2)');
    if( $savebeam_multisim_active )
      BEAM2_SC += BEAM.^2;
    end
    % take corrector positions
    Qy_after_SC = placet_element_get_attribute("$mysimname", Q, "y" );
    Qy_max_after_SC = max( max(abs(Qy_after_SC)), Qy_max_after_SC );
    Qy_rms_after_SC += std( Qy_after_SC );
    if( length(D)>0 ); Dsy_after_SC = placet_element_get_attribute("$mysimname", D, "strength_y" ); end; 

end

 Breading_y_after_SC = placet_element_get_attribute("$mysimname", B, "reading_y" );





    #
    # DO DFS
    #
    if( $track_DFS_correction )
    machinename = sprintf("$deceleratorrootpath/precalc/machines/M_${mysimname}_${Rsuffix}_${QB_BBA_interval}_${BBA_interval_start2}_DFS_%s.dat", num2str(i) );
    if ( exist(machinename, "file") && $do_multisim_load_mach  ) 
      machinename
       disp('Load DFS machine');
          load(machinename);
          placet_element_set_attribute("$mysimname", Q, "x", Qx);
          placet_element_set_attribute("$mysimname", Q, "y", Qy);
    else
    disp(['OCT DFS - correcting surveyed machine, in steps of '  num2str(n_bins) ' bins.']);
    placet_element_set_attribute("$mysimname", B, "resolution", $bpm_resolution);
    tB = B_BBA;
    tC = C;
if( $do_x_alignment )
  disp("WARNING: BPM failure not implemented for x motion.  All BPMs are correct.");
    # x correction
  if( n_bins > 1)
    disp("WARNING: binning not implemented for x motion.  Correcting in a single bin.");
  end% if
    placet_element_set_attribute("$mysimname", Q, "aperture_x", Q_a0x*1e6);
    placet_element_set_attribute("$mysimname", Q, "aperture_y", Q_a0y*1e6);
   placet_test_no_correction("$mysimname", "$beamname1", "None", 1, 0, tB(end));
    tb0 = placet_element_get_attribute("$mysimname", tB, "reading_x")';
    placet_test_no_correction("$mysimname", "$testbeam1", "None", 1, 0, tB(end));
    tb1 = placet_element_get_attribute("$mysimname", tB, "reading_x")' - tb0;
    placet_element_set_attribute("$mysimname", Q, "aperture_x", Q_a0x);
    placet_element_set_attribute("$mysimname", Q, "aperture_y", Q_a0y);
    tb = [1.0*tb0 ;  $DFS_w1_w0*tb1];
    tR=[1.0*R0_x ; $DFS_w1_w0*R1_x];
    tR_pinv = calc_pinv(tR, $svd_cut_DFS);
    tc_x = -tR_pinv * tb;
    if($corrector_step_size != 0), tc_x = round(tc_x / $corrector_step_size) * $corrector_step_size; end; % corr steps
    placet_element_vary_attribute("$mysimname", C, C_variable_x, tc_x);
 end% if
 n_iters = 1;
 # y correction
 for( n_iter=1:n_iters ),
    for n_bin=1:n_bins,
     if( n_bins > 1 )
       tB_bin = tB( bin_start(n_bin):bin_end(n_bin) );
       R0_bin = R0(bin_start(n_bin):bin_end(n_bin), bin_start(n_bin):bin_end(n_bin)); % NB: this only works for square R0
       R1_bin = R1(bin_start(n_bin):bin_end(n_bin), bin_start(n_bin):bin_end(n_bin)); % NB: this only works for square R1
       C_bin = C( bin_start(n_bin):bin_end(n_bin) );
     else
       tB_bin = tB;
       R0_bin = R0;
       R1_bin = R1;
       C_bin = C;
     end% if
    placet_test_no_correction("$mysimname", "$beamname1", "None", 1, 0, tB_bin(end));
    tb0 = placet_element_get_attribute("$mysimname", tB_bin, "reading_y")';
    if( dfs_scale_magnets )
      k_Q = placet_element_get_attribute("$mysimname", Q, "strength");
      placet_element_set_attribute("$mysimname", Q, "strength", k_Q*quad_dfs_scaling);
      if( length(D)>0 ),
        a_y = placet_element_get_attribute("$mysimname", D, "strength_y");
        placet_element_set_attribute("$mysimname", D, "strength_y", a_y*quad_dfs_scaling);
      end% if
      E1_ver = placet_test_no_correction("$mysimname", "$beamname1", "None", 1); 
      envmax_DFS_testbeam(:,i) =  E1_ver(1:(end-elements_corr_cut),8); % verification
      placet_element_set_attribute("$mysimname", Q, "strength", k_Q);
      if( length(D)>0 ),
        placet_element_set_attribute("$mysimname", D, "strength_y", a_y);
      end% if
    else
      E1_ver = placet_test_no_correction("$mysimname", "$testbeam1", "None", 1);
      envmax_DFS_testbeam(:,i) =  E1_ver(1:(end-elements_corr_cut),8); % verification
    end% if
    tb1 = placet_element_get_attribute("$mysimname", tB_bin, "reading_y")' - tb0;
    # BPM failure: take out signal and row in R (assume known fail)
    if( n_failure_BPM_percent > 0)
      tb0(BPM_failed) = [];
      tb1(BPM_failed) = [];
      R0_bin(BPM_failed, :) = [];
      R1_bin(BPM_failed, :) = [];
    end% if
    tb = [1.0*tb0 ;  $DFS_w1_w0*tb1];
    tR=[1.0*R0_bin ; $DFS_w1_w0*R1_bin];
    tR_pinv = calc_pinv(tR, $svd_cut_DFS);
    tc_y = -tR_pinv * tb;
    if($corrector_step_size != 0), tc_y = round(tc_y / $corrector_step_size) * $corrector_step_size; end; % corr steps
    placet_element_vary_attribute("$mysimname", C_bin, C_variable_y, tc_y);
    end% for bins
    end% for iters
    disp('Finishing DFS current machine routine');
    end% if
    
   if ( $do_multisim_save_mach  ) 
      disp('Save DFS machine');
      Qx = placet_element_get_attribute("$mysimname", Q, "x");
      Qy = placet_element_get_attribute("$mysimname", Q, "y");
      save(machinename, 'Qx', 'Qy');
   end% if

    
Qy_nominal = placet_element_get_attribute("$mysimname", Q, "y");
if( length(P) > 0 ), placet_element_set_attribute("$mysimname", P(P_failed), "e0", -1.0); end; %  drive wedges PETS
placet_element_set_attribute("$mysimname", Q, "y", Qy_nominal.+jitter_Qy); % y jitter
placet_element_set_attribute("$mysimname", Q, "strength", Qstrength_nominal.*scale_Q); % strength jitter
placet_element_set_attribute("$mysimname", Q(Q_failed), "strength", Qstrength_nominal(Q_failed)*$quad_fail_current_reduction);
placet_element_set_attribute("$mysimname", B, "resolution", 0);
[E0, BEAM] = placet_test_no_correction("$mysimname", "$beamname1", "None", 1); % final beam in centre quad
y0 = placet_element_get_attribute("$mysimname", B, "reading_y") + placet_element_get_attribute("$mysimname", B, "y");
x0 = placet_element_get_attribute("$mysimname", B, "reading_x") + placet_element_get_attribute("$mysimname", B, "x");
losses_DFS += placet_element_get_attribute("$mysimname", Q, "aperture_losses");
if ( $do_calc_disp )
placet_element_set_attribute("$mysimname", B, "resolution", 0);
   if( disp_scale_magnets )
     k_Q = placet_element_get_attribute("$mysimname", Q, "strength");
     placet_element_set_attribute("$mysimname", Q, "strength", k_Q*quad_disp_scaling);
     if( length(D)>0 ),
       a_y = placet_element_get_attribute("$mysimname", D, "strength_y");
       placet_element_set_attribute("$mysimname", D, "strength_y", a_y*quad_disp_scaling);
     end% if
     E1 = placet_test_no_correction("$mysimname", "$beamname1", "None", 1);
     placet_element_set_attribute("$mysimname", Q, "strength", k_Q);
     if( length(D)>0 ),
       placet_element_set_attribute("$mysimname", D, "strength_y", a_y);
     end% if
   else
     E1 = placet_test_no_correction("$mysimname", "$testbeamE2", "None", 1);
   end% if
   y1 = placet_element_get_attribute("$mysimname", B, "reading_y") + placet_element_get_attribute("$mysimname", B, "y");
   Dy = (y0-y1) / dp_p;
   y1_tot2_DFS += y1.^2;
   Dy_tot2_DFS += Dy.^2;
placet_element_set_attribute("$mysimname", B, "resolution", $bpm_resolution);
end% if
placet_element_set_attribute("$mysimname", B, "resolution", $bpm_resolution);
placet_element_set_attribute("$mysimname", Q, "strength", Qstrength_nominal );
placet_element_set_attribute("$mysimname", Q, "y", Qy_nominal ); % restore quads
if( length(P) > 0 ), placet_element_set_attribute("$mysimname", P, "e0", PETS_nominal); end; %  restore PETS
    y0_DFS += y0;
    x0_DFS += y0;
    y0_tot2_DFS += y0.^2;
    x0_tot2_DFS += x0.^2;
    env2_DFS += (E0(1:(end-elements_corr_cut),8)'.^2);
    envmax_DFS(:,i) =  E0(1:(end-elements_corr_cut),8);
    envmaxmach_DFS(i) = max(E0(1:(end-elements_corr_cut),8));
    emitty_DFS += (E0(1:(end-elements_corr_cut),6)');
    emittx_DFS += (E0(1:(end-elements_corr_cut),2)');
    if( $savebeam_multisim_active )
    BEAM2_DFS += BEAM.^2;
end
    Qy_after_DFS = placet_element_get_attribute("$mysimname", Q, "y" );
    Qy_max_after_DFS = max( max(abs(Qy_after_DFS)), Qy_max_after_DFS );
    Qy_rms_after_DFS += std( Qy_after_DFS );
    if( length(D)>0 ); Dsy_after_DFS = placet_element_get_attribute("$mysimname", D, "strength_y" ); end; 
end
Breading_y_after_DFS = placet_element_get_attribute("$mysimname", B, "reading_y" );
end

    



%
% POST PROCESSING
%
    Dy_tot_NC = sqrt(Dy_tot2 / nm);
    y0_tot_NC = sqrt(y0_tot2 / nm);
    x0_tot_NC = sqrt(x0_tot2 / nm);
    y1_tot_NC = sqrt(y1_tot2 / nm);
    Dy_tot_SC = sqrt(Dy_tot2_SC / nm);
    y0_tot_SC = sqrt(y0_tot2_SC / nm);
    x0_tot_SC = sqrt(x0_tot2_SC / nm);
    y1_tot_SC = sqrt(y1_tot2_SC / nm);
    Dy_tot_DFS = sqrt(Dy_tot2_DFS / nm);
    y0_tot_DFS = sqrt(y0_tot2_DFS / nm);
    x0_tot_DFS = sqrt(x0_tot2_DFS / nm);
    y1_tot_DFS = sqrt(y1_tot2_DFS / nm);
    y0_NC = (y0_NC / nm);
    x0_NC = (x0_NC / nm);
    y0_SC = (y0_SC / nm);
    x0_SC = (x0_SC / nm);
    y0_DFS = (y0_DFS / nm);
    x0_DFS = (x0_DFS / nm);
    env_NC = sqrt(env2_NC / nm);
    env_SC = sqrt(env2_SC / nm);
    env_DFS = sqrt(env2_DFS / nm);
    emitty_NC = emitty_NC / nm;
    emitty_SC = emitty_SC / nm;
    emitty_DFS = emitty_DFS / nm;
    emittx_NC = emittx_NC / nm;
    emittx_SC = emittx_SC / nm;
    emittx_DFS = emittx_DFS / nm;
    losses_NC /= nm;
    losses_SC /= nm;
    losses_DFS /= nm;
    Dy_temp_NC /= nm;
    BEAM0_NC = BEAM0_NC / nm;
    BEAM_NC = sqrt(BEAM2_NC / nm);
    BEAM_SC = sqrt(BEAM2_SC / nm);
    BEAM_DFS = sqrt(BEAM2_DFS / nm);
    envmax_NC_sort = sort(envmax_NC',1)';
    envmax_NC = envmax_NC_sort(:, round(size(envmax_NC_sort, 2)*$beam_envelope_percentile))';
    envmax_SC_sort = sort(envmax_SC',1)';
    envmax_SC = envmax_SC_sort(:, round(size(envmax_SC_sort, 2)*$beam_envelope_percentile))';
    envmax_DFS_sort = sort(envmax_DFS',1)';
    envmax_DFS = envmax_DFS_sort(:, round(size(envmax_DFS_sort, 2)*$beam_envelope_percentile))';
    envmax_DFS_testbeam_sort = sort(envmax_DFS_testbeam',1)';
    envmax_DFS_testbeam = envmax_DFS_testbeam_sort(:, round(size(envmax_DFS_testbeam_sort, 2)*$beam_envelope_percentile))';
    Qy_max_after_NC
    Qy_rms_after_NC /= nm
    Qy_max_after_SC 
    Qy_rms_after_SC /= nm
    Qy_max_after_DFS 
    Qy_rms_after_DFS /= nm
    
    % NB, NB, NB
    % SCALE ALL ENVLOPES IF SIMULATION IS Y-ONLY (NB: this is done in multisim only)
    if( $activate_envelope_scaling)
      disp('   scaling all envelopes by factor (beta_max+beta_min)/beta_max');
      beta_max = max( $twiss_match(beta_x), $twiss_match(beta_y) );
      beta_min = min( $twiss_match(beta_x), $twiss_match(beta_y) );
      % scaling factor of minimum envelope
      % scale everything except minimum envelope by factor sqrt(2)
      y_ad_scale_factor = sqrt((beta_max + beta_min) / beta_max)
      y_mean_max = max(env_NC);
      %y_max_max = max(envmax_NC)
      r0 = min(pvar_r_min*1e6, y_mean_max*y_ad_scale_factor);
      r_mean_max = r0 + (y_mean_max*y_ad_scale_factor - r0)*sqrt(2)/y_ad_scale_factor;
      if(y_mean_max > 0 )
        y_total_scale_factor_NC = r_mean_max / y_mean_max
      else
        y_total_scale_factor_NC = 1;
      end% if
      y_mean_max = max(envmax_SC);
      r0 = min(pvar_r_min*1e6, y_mean_max*y_ad_scale_factor);
      r_mean_max = r0 + (y_mean_max*y_ad_scale_factor - r0)*sqrt(2)/y_ad_scale_factor;
      if(y_mean_max > 0 )
        y_total_scale_factor_SC = r_mean_max / y_mean_max
      else
        y_total_scale_factor_SC = 1;
      end% if
      y_mean_max = max(envmax_DFS);
      r0 = min(pvar_r_min*1e6, y_mean_max*y_ad_scale_factor);
      r_mean_max = r0 + (y_mean_max*y_ad_scale_factor - r0)*sqrt(2)/y_ad_scale_factor;
      if(y_mean_max > 0 )
        y_total_scale_factor_DFS = r_mean_max / y_mean_max
      else
        y_total_scale_factor_DFS = 1;
      end% if
      y_mean_max = max(envmax_DFS_testbeam);
      r0 = min(pvar_r_min*1e6, y_mean_max*y_ad_scale_factor);
      r_mean_max = r0 + (y_mean_max*y_ad_scale_factor - r0)*sqrt(2)/y_ad_scale_factor;
      if(y_mean_max > 0 )
        y_total_scale_factor_DFS_testbeam = r_mean_max / y_mean_max
      else
        y_total_scale_factor_DFS_testbeam = 1;
      end% if

      env_NC *= y_total_scale_factor_NC;
      env_SC *= y_total_scale_factor_SC;  
      env_DFS *= y_total_scale_factor_DFS; 
      envmax_NC *= y_total_scale_factor_NC;  
      envmax_SC *= y_total_scale_factor_SC;  
      envmax_DFS *= y_total_scale_factor_DFS;
      envmax_DFS_testbeam *= y_total_scale_factor_DFS_testbeam; 
      envmaxmach_NC *= y_total_scale_factor_NC; 
      envmaxmach_SC *= y_total_scale_factor_SC; 
      envmaxmach_DFS *= y_total_scale_factor_DFS; 
    else
      % do not scale
      y_ad_scale_factor  = 1;
      y_total_scale_factor_NC = 1;
      y_total_scale_factor_SC = 1;
      y_total_scale_factor_DFS = 1;
      y_total_scale_factor_testbeam = 1;
    end

    % defining and saving generic post-processing info
    pvar_n_slices = $n_slices;
    pvar_n_bunches = $n_bunches;
    pvar_n_macros = $n_macros;
    pvar_d_bunch = $d_bunch;
    pvar_n_bunch_steady_state = $n_bunch_steady_state;
    pvar_L_PETS = $cavitylength;
    pvar_n_PETS = $n_PETS;
    pvar_L_FODO = $unitlength*2;
    pvar_n_PETSperFODO = $n_PETSperFODO;
    pvar_L_sector = pvar_n_PETS/pvar_n_PETSperFODO*pvar_L_FODO;
    pvar_L_QUAD = $quadrupolelength;
    pvar_k_QUAD = $k_qpole;
    pvar_N_bunch = $charge;
    pvar_sigma_z = $sigma_bunch;
    pvar_S_requested = $requested_E_spread;
    pvar_P_requested = $requested_PETS_power;
    pvar_corrector_step_size = $corrector_step_size;
    pvar_corr_per_bin = $corr_per_bin;
    pvar_corr_bin_overlap = $bin_overlap;
    pvar_beta_x = $twiss_match(beta_x);
    pvar_beta_y = $twiss_match(beta_y);

    if( $savebeta_multisim_active )
      disp('   Saving lattice beta functions...');
      E = placet_get_name_number_list("$mysimname", "*");
      n_E = size(E,2);
      [s, beta_x, beta_y, alpha_x, alpha_y, mu_x, mu_y] = placet_evolve_beta_function("$mysimname", $twiss_match(beta_x), $twiss_match(alpha_x), $twiss_match(beta_y), $twiss_match(alpha_y), 0, n_E-1);
      save -text latticebeta.$loop_param1_n.$loop_param2_n.$param1_n.$param2_n.$run.dat s beta_x beta_y mu_x mu_y  alpha_x  alpha_y
    end% if

    if( $savetransfermatrices_active )
      disp('   Saving lattice transfer matrices...');
      E_matrix_points = placet_get_name_number_list("$mysimname", "MATRIXPOINT*");
      n_E = size(E_matrix_points,2);
      M_transfer = 0;
      if(n_E > 1)
        M_transfer = zeros(6,6,n_E-1);
        for( n=1:(n_E-1) ),
          M_transfer(:,:,n) = placet_get_transfer_matrix("$mysimname", E_matrix_points(n),E_matrix_points(n+1));
        end% for
      end% if
      save -text transfermatrices.$loop_param1_n.$loop_param2_n.$param1_n.$param2_n.$run.dat E_matrix_points M_transfer 
    end% if

    %
    % save EASIER names for beam...
    %
    bnc = BEAM_NC;
    b0nc = BEAM0_NC;
    bsc = BEAM_SC;
    bdfs = BEAM_DFS;
     
    if( $savebeam_multisim_active )
save -mat decsimdata.$loop_param1_n.$loop_param2_n.$param1_n.$param2_n.$run.dat Dy_tot_NC  y0_tot_NC  y0_tot_SC y0_tot_DFS   x0_tot_NC  x0_tot_SC x0_tot_DFS  y1_tot_NC  Dy_tot_SC Dy_tot_DFS  y1_tot_SC y1_tot_DFS  dp_p  env_NC  env_SC  env_DFS envmax_NC  envmax_SC  envmax_DFS  emitty_NC  emitty_SC  emitty_DFS   emittx_NC  emittx_SC  emittx_DFS   losses_NC  losses_SC  losses_DFS  Dy_temp_NC b0nc bnc  bsc bdfs envmax_DFS_testbeam envmaxmach_NC envmaxmach_SC envmaxmach_DFS    Qy_after_NC  Qy_after_SC  Qy_after_DFS  Qy_max_after_NC  Qy_rms_after_NC   Qy_max_after_SC  Qy_rms_after_SC   Qy_max_after_DFS    Qy_rms_after_DFS By_before   Breading_y_after_NC   Breading_y_after_SC  Breading_y_after_DFS   Dsy_after_NC  Dsy_after_SC  Dsy_after_DFS y0_NC y0_SC y0_DFS  x0_NC x0_SC x0_DFS pvar_n_slices pvar_n_bunches pvar_n_macros pvar_d_bunch pvar_n_bunch_steady_state    pvar_L_PETS pvar_n_PETS  pvar_L_FODO   pvar_n_PETSperFODO   pvar_L_sector    pvar_L_QUAD   pvar_k_QUAD   pvar_f_T   pvar_w_T   pvar_Q_T   pvar_beta_T   pvar_N_bunch   pvar_sigma_z   pvar_n_modes_T  pvar_e0 pvar_de0 pvar_e_min_final pvar_gamma_final pvar_r_min pvar_gamma_initial pvar_r_initial y_ad_scale_factor    y_total_scale_factor_NC    y_total_scale_factor_SC   y_total_scale_factor_DFS  Qs Bs   pvar_S_requested  pvar_P_requested  pvar_corrector_step_size  pvar_corr_per_bin  pvar_corr_bin_overlap  pvar_E_beam_ana  pvar_P_ana   pvar_U_max_ana  pvar_U_mean_ana   pvar_P_fd  pvar_beta_x  pvar_beta_y
    else
save -mat decsimdata.$loop_param1_n.$loop_param2_n.$param1_n.$param2_n.$run.dat Dy_tot_NC   y0_tot_NC  y0_tot_SC y0_tot_DFS   x0_tot_NC  x0_tot_SC x0_tot_DFS y1_tot_NC  Dy_tot_SC Dy_tot_DFS  y1_tot_SC y1_tot_DFS  dp_p  env_NC  env_SC  env_DFS envmax_NC  envmax_SC  envmax_DFS  emitty_NC  emitty_SC  emitty_DFS   emittx_NC  emittx_SC  emittx_DFS    losses_NC  losses_SC  losses_DFS  Dy_temp_NC envmax_DFS_testbeam envmaxmach_NC envmaxmach_SC envmaxmach_DFS     Qy_max_after_NC  Qy_rms_after_NC   Qy_max_after_SC  Qy_rms_after_SC   Qy_max_after_DFS    Qy_rms_after_DFS   y0_NC y0_SC y0_DFS    x0_NC x0_SC x0_DFS  pvar_n_slices pvar_n_bunches pvar_n_macros pvar_d_bunch pvar_n_bunch_steady_state    pvar_L_PETS pvar_n_PETS  pvar_L_FODO   pvar_n_PETSperFODO   pvar_L_sector    pvar_L_QUAD   pvar_k_QUAD   pvar_f_T   pvar_w_T   pvar_Q_T   pvar_beta_T   pvar_N_bunch   pvar_sigma_z   pvar_n_modes_T    pvar_e0 pvar_de0 pvar_e_min_final pvar_gamma_final pvar_r_min  pvar_gamma_initial  pvar_r_initial  y_ad_scale_factor   y_total_scale_factor_NC    y_total_scale_factor_SC   y_total_scale_factor_DFS   Qs Bs  pvar_S_requested  pvar_P_requested  pvar_corrector_step_size    pvar_corr_per_bin  pvar_corr_bin_overlap   pvar_E_beam_ana  pvar_P_ana   pvar_U_max_ana  pvar_U_mean_ana   pvar_P_fd  pvar_beta_x  pvar_beta_y
    end% if
    
  max_NC_env = max(envmax_NC)
  max_SC_env = max(envmax_SC)
  max_DFS_env = max(envmax_DFS)
  avg_NC_env = max(env_NC)
  avg_SC_env = max(env_SC)
  avg_DFS_env = max(env_DFS)
 
if ( $savelattice_active )
      disp('   Saving lattice...');
     filename = "lattice.txt";
     fid = fopen (filename, "wt");
     # Do the actual I/O here...
   E = placet_get_name_number_list("$mysimname", "*");
   n_E = length(E);
   #lattice = zeros(n_E, 1);
   for n=0:(n_E-1)
    fprintf(fid, "%5s", num2str(n));
    fprintf(fid, "%15s", placet_element_get_attribute("$mysimname", n, "type_name"));
    fprintf(fid, "%10s", num2str(placet_element_get_attribute("$mysimname", n, "s")));
    fprintf(fid, "%10s", num2str(placet_element_get_attribute("$mysimname", n, "length")));
    fprintf(fid, "%10s\n", num2str(placet_element_get_attribute("$mysimname", n, "y")));
   end% for
    #
    fclose (fid);
end% if


% example how to deal with envelope together with losses
%B_nolosses = placet_remove_lost_particles(B);
%env_final = placet_get_envelope(B)
%env_final_nolosses = placet_get_envelope(B_nolosses);

% clear reponses
if exist("R0"), clear R0, end%
if exist("R0_x"), clear R0_x, end%
if exist("R1"), clear R1, end%
if exist("R1_x"), clear R1_x, end%

disp('Finished Multisim');
}
